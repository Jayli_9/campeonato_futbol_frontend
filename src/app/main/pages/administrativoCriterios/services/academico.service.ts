import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { environment } from 'environments/environment';
import { ResponseGenerico } from '../interfaces/response-generico';
import { habilidadesInterface } from "../interfaces/habilidades";
import { criteriosInterface } from '../interfaces/criterios';
import { catHabilidadesInterface } from "../interfaces/catHabilidad";


@Injectable({
  providedIn: 'root'
})
export class AcademicoService {

    private readonly URL_REST = environment.url_academico;

constructor(private _http: HttpClient) { }

listarHabilidades(): Promise<ResponseGenerico<habilidadesInterface>>{
    return new Promise((resolve, reject) => {
      this._http.get(`${environment.url_academico}/private/listarHabilidades`).subscribe((response: ResponseGenerico<habilidadesInterface>) => {
        resolve(response);
      }, reject);
    })
  }


  listarCriterios(): Promise<ResponseGenerico<criteriosInterface>>{
    return new Promise((resolve, reject) => {
      this._http.get(`${environment.url_academico}/private/listarCriterios`).subscribe((response: ResponseGenerico<criteriosInterface>) => {
        resolve(response);
      }, reject);
    })
  }


  listarCatHabilidades(): Promise<ResponseGenerico<catHabilidadesInterface>>{
    return new Promise((resolve, reject) => {
      this._http.get(`${environment.url_academico}/private/listaCatHabilidades`).subscribe((response: ResponseGenerico<catHabilidadesInterface>) => {
        resolve(response);
      }, reject);
    })
  } 


  guardarCriterios(parametro: any){
    return this._http.post<criteriosInterface>(`${environment.url_academico}/private/registrarCriterios`, parametro);
  }


  editarCriterios(parametro: any){
    return this._http.post<criteriosInterface>(`${environment.url_academico}/private/actualizarCriterios`, parametro);
  }


  buscarListaCriteriosPorCodigo(cod): Promise<ResponseGenerico<criteriosInterface>>{
    return new Promise((resolve, reject) => {
      this._http.get(`${environment.url_academico}/private/buscarCriteriosPorCodigo/`+cod).subscribe((response: ResponseGenerico<criteriosInterface>) => {
        resolve(response);
      }, reject);
    })
  }


  eliminarCriterio(cod): Promise<ResponseGenerico<criteriosInterface>>{
    return new Promise((resolve, reject) => {
      this._http.delete(`${environment.url_academico}/private/eliminarCriterio/`+cod).subscribe((response: ResponseGenerico<criteriosInterface>) => {
        resolve(response);
      }, reject);
    })
  }

  
  

}
