import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class ProgramaStoreService {

  private realmCodigo: number;
  private regCodigo: number;

  private codigoCriterio: number;
  private codigoNiv: number;
  private codigoHabilidad: number;
  private codigoServicio: number;


  get getCodigoServicio(){
    return this.codigoServicio;
  }

  set setCodigoServicio(codigoServicio){
    this.codigoServicio = codigoServicio;
  }

  get getCodigoCriterio(){
    return this.codigoCriterio;
  }

  set setCodigoCriterio(codigoCriterio){
    this.codigoCriterio = codigoCriterio;
  }

  get getCodigoNiv(){
    return this.codigoNiv;
  }

  set setCodigoNiv(codigoNiv){
    this.codigoNiv = codigoNiv;
  }

  get getCodigoHabilidad(){
    return this.codigoHabilidad;
  }

  set setCodigoHabilidad(codigoHabilidad){
    this.codigoHabilidad = codigoHabilidad;
  }


  
  get getRealmCodigo(){
    return this.realmCodigo;
  }

  set setRealmCodigo(realmCodigo){
    this.realmCodigo = realmCodigo;
  }

  get getRegCodigo(){
    return this.regCodigo;
  }

  set setRegCodigo(regCodigo){
    this.regCodigo = regCodigo;
  }

}
