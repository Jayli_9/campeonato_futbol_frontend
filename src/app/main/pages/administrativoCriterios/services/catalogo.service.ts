import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { environment } from 'environments/environment';
import { ResponseGenerico } from '../interfaces/response-generico';
import { nivelInterface } from "../interfaces/nivel";
import { regimenAniolecInterface } from "../interfaces/regimen-anio-leg";
import { institucionEducativaInterface } from "../interfaces/intitucionEducativa";
import { modalidadServicioInterface } from "../interfaces/modalidadServicio";
import { servicioEducativoInterface } from "../interfaces/servicioEducativo";

@Injectable({
  providedIn: 'root'
})
export class CatalogoService {

  private readonly URL_REST = environment.url_catalogo;

constructor(private _http: HttpClient) { }

ListarNivelPorCodigo(cod): Promise<ResponseGenerico<nivelInterface>>{
  return new Promise((resolve, reject) => {
    this._http.get(`${environment.url_catalogo}/private/buscarNivelPorCodigo/`+cod).subscribe((response: ResponseGenerico<nivelInterface>) => {
      resolve(response);
    }, reject);
  })
}


listarNiveles(): Promise<ResponseGenerico<nivelInterface>>{
  return new Promise((resolve, reject) => {
    this._http.get(`${environment.url_catalogo}/private/listarNiveles`).subscribe((response: ResponseGenerico<nivelInterface>) => {
      resolve(response);
    }, reject);
  })
}


buscarReanlePorCodigo(cod): Promise<ResponseGenerico<regimenAniolecInterface>>{
  return new Promise((resolve, reject) => {
    this._http.get(`${environment.url_catalogo}/private/buscarRegimenAnioLectivoPorCodigo/`+cod).subscribe((response: ResponseGenerico<regimenAniolecInterface>) => {
      resolve(response);
    }, reject);
  })
}


buscarInstitucionPorAmie(cod): Promise<ResponseGenerico<institucionEducativaInterface>>{
  return new Promise((resolve, reject) => {
    this._http.get(`${environment.url_catalogo}/private/buscarInstitucionEducativaPorAmie/`+cod).subscribe((response: ResponseGenerico<institucionEducativaInterface>) => {
      resolve(response);
    }, reject);
  })
}

listarModalidadServicio(): Promise<ResponseGenerico<modalidadServicioInterface>>{
  return new Promise((resolve, reject) => {
    this._http.get(`${environment.url_catalogo}/private/listaModalidadesTipoEducacion`).subscribe((response: ResponseGenerico<modalidadServicioInterface>) => {
      resolve(response);
    }, reject);
  })
}


listaServiciosEducativos(): Promise<ResponseGenerico<servicioEducativoInterface>>{
  return new Promise((resolve, reject) => {
    this._http.get(`${environment.url_catalogo}/private/listarServicioEducativos`).subscribe((response: ResponseGenerico<servicioEducativoInterface>) => {
      resolve(response);
    }, reject);
  })
}


buscarMotiedPorCodigo(cod): Promise<ResponseGenerico<modalidadServicioInterface>>{
  return new Promise((resolve, reject) => {
    this._http.get(`${environment.url_catalogo}/private/buscarModalidadTipoEducacionPorId/`+cod).subscribe((response: ResponseGenerico<modalidadServicioInterface>) => {
      resolve(response);
    }, reject);
  })
}


buscarRegAnioElectivo(cod): Promise<ResponseGenerico<regimenAniolecInterface>>{
  return new Promise((resolve, reject) => {
    this._http.get(`${environment.url_catalogo}/private/listarRegimenAnioLectivoPorCodigoRegimen/`+cod).subscribe((response: ResponseGenerico<regimenAniolecInterface>) => {
      resolve(response);
    }, reject);
  })
}


}
