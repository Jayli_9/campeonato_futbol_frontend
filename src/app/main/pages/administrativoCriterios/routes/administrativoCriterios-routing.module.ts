import { Routes } from "@angular/router";
import { AuthGuard } from "app/auth/helpers/auth.guards";
import { PrincipalComponent } from "../components/principal/principal.component";

export const RUTA_ADMINISTRATIVOCRITERIOS:Routes=[
    {
        path:'administrativoCriterios',
        component:PrincipalComponent,
        canActivate:[AuthGuard]
    }
]