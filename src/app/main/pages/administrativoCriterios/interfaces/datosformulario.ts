export interface datosFormularioInterface{
          idHabilidad: number;
          nombreHabilidad: string;
          idCriterios: number;
          nombreCriterios: string;
          idNivel: number;
          nombreNivel: string;
}