export interface servicioEducativoInterface{
       sereduCodigo: number;
       sereduEstado: number;
       motiedCodigo: number;
       sereduVigente: string;
       reanleCodigo: number;
       codigoRegimen: number;
       codigoAnioLectivo: number;
       codigoModalidadTipoEducacion: null;
       codigoModalidad: null;
       codigoTipoEducacion: null;
       codigoPrograma: null;
       nombrePrograma: null;
       tipeduNombre: null;
       modalidad: null;
       modCodigo: number;
       impClasificacion: null;
       impDescripcion: null;
}