export interface regimenAniolecInterface{
    reanleCodigo: number;
    reanleEstado: number;
    reanleFechaCreacion: Date;
    reanleTipo: string;
    regCodigo: number;
    anilecCodigo: number;
    descripcionAnioLectivo: string;
}