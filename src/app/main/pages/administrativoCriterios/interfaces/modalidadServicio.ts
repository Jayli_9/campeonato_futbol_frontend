export interface modalidadServicioInterface{
       motiedCodigo: number;
       motiedDescripcion: string;
       motiedEstado: number;
       modCodigo: number;
       tipeduCodigo: number;
       impCodigo: number;
       temCodigo: number;
       escCodigo: number;
       pbojCodigo: number;
}