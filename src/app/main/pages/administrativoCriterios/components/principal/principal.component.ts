import { Component, OnInit } from '@angular/core';
import { AcademicoService } from "../../services/academico.service";
import { CatalogoService } from "../../services/catalogo.service";
import { User } from 'app/auth/models';
import { datosFormularioInterface } from "../../interfaces/datosformulario";
import { ModalCriteriosComponent } from "../modal-criterios/modal-criterios.component";
import { EdicionCriteriosComponent } from "../edicion-criterios/edicion-criterios.component";
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { ProgramaStoreService } from "../../services/programa-store.service";
import Swal from 'sweetalert2';

@Component({
  selector: 'app-principal',
  templateUrl: './principal.component.html',
  styleUrls: ['./principal.component.scss']
})
export class PrincipalComponent implements OnInit {
  contentHeader: object;

  currentUser: User;
  datosInstitucion = [];
  codigoRegimen;

  page = 1;
  pageSize = 4;
  datoEvent;


  listaCriterios;

  habilidadSeleccionada;
  listaHabilidades = [];
  listaNivel;


  //datos de objeto tabla
  idHabilidad;
  nombreHabilidad;
  idCriterio;
  nombreCriterio;
  idNivel;
  nombreNivel;


  datosObjeto;
  listaDatosFormulario = [];

  seleccionado: { id: number, nombre: string } | null = null;
  seleccionadoNivel: { idNivel: number, nombreNivel: string } | null = null;

  constructor(
    private readonly academicoService: AcademicoService,
    private readonly catalogoService: CatalogoService,
    private readonly programaStore: ProgramaStoreService,
    private modalService: NgbModal
  ) {
    this.currentUser = JSON.parse(localStorage.getItem("currentUser"))
   }

  ngOnInit() {

    this.datosInstitucion.push(this.currentUser.sede);

    this.catalogoService.buscarInstitucionPorAmie(this.datosInstitucion[0].nemonico).then(data => {
      let lista = data;

      if(lista[0].descripcionRegimen == "SIERRA"){

        this.codigoRegimen = 1;
        this.programaStore.setRegCodigo=this.codigoRegimen

      }else if(lista[0].descripcionRegimen == "COSTA"){

        this.codigoRegimen = 2;
        this.programaStore.setRegCodigo=this.codigoRegimen

      }

    })

    this.contentHeader = {
      headerTitle: 'Administrar Criterios',
      actionButton: false,
      breadcrumb: {
          type: '',
          links: [
              {
                name: 'Inicio',
                isLink: true,
                link: '/'
              },
              {
                name: 'administrativoCriterios',
                isLink: false
              }
          ]
      }
    };

    this.academicoService.listarCriterios().then(data => {
      this.listaCriterios = data.listado
    })


    this.academicoService.listarHabilidades().then(data => {
      let lista = data.listado;

      for(let i = 0; i < lista.length; i++){

        if(lista[i].habEstado == 1){

          this.listaHabilidades.push(lista[i])
        }

      }

    })

  }


  openModalHabilidad(){
    const dialog = this.modalService.open(ModalCriteriosComponent,{
      scrollable: true,
      size: 'lg',
      windowClass: 'myCustomModalClass',
      // keyboard: false,
      //backdrop: 'static'
    });

    dialog.result.then((result) => {

      this.academicoService.listarCriterios().then(data => {
        this.listaCriterios = data.listado
      })

      setTimeout(() => {



        this.listaDatosFormulario = [];

          this.listaCriterios.forEach(element => {
            if(element.habCodigo == this.idHabilidad){
      
              if(element.nivCodigo == this.idNivel){
      
                if(element.criEstado == 1){
      
                  this.catalogoService.buscarReanlePorCodigo(element.reanleCodigo).then(data => {
      
                    let lista = data.objeto;
        
                    if(lista.reanleTipo = "A"){
        
                      if(lista.regCodigo == this.codigoRegimen){
                        
                          this.idCriterio = element.criCodigo;
                          this.nombreCriterio = element.criDescripcion;
        
                          setTimeout(() => {
                                let objetoDatos = {
                                       idHabilidad: this.idHabilidad,
                                       nombreHabilidad: this.nombreHabilidad,
                                       idCriterios: this.idCriterio,
                                       nombreCriterios: this.nombreCriterio,
                                       idNivel: this.idNivel,
                                       nombreNivel: this.nombreNivel,
                                  }
        
                                this.listaDatosFormulario.push(objetoDatos);
                            }, 1000);
                          }
                         }
                  })
                }   
              }
            }
          });

      }, 1000);

      
    })

  }


  abrirEditar(est){
    //console.log(est);
    this.programaStore.setCodigoCriterio=est.idCriterios;
    this.programaStore.setCodigoHabilidad = est.idHabilidad;
    this.programaStore.setCodigoNiv = est.idNivel;
    this.programaStore.setCodigoServicio = est.servicioCodigo;

    const dialog = this.modalService.open(EdicionCriteriosComponent,{
      scrollable: true,
      size: 'lg',
      windowClass: 'myCustomModalClass',
      // keyboard: false,
      //backdrop: 'static'
    });

    dialog.result.then((result) => {

      this.academicoService.listarCriterios().then(data => {
        this.listaCriterios = data.listado
      })


      setTimeout(() => {
        this.listaDatosFormulario = [];

          this.listaCriterios.forEach(element => {
            if(element.habCodigo == this.idHabilidad){
      
              if(element.nivCodigo == this.idNivel){
      
                if(element.criEstado == 1){
      
                  this.catalogoService.buscarReanlePorCodigo(element.reanleCodigo).then(data => {
      
                    let lista = data.objeto;
        
                    if(lista.reanleTipo = "A"){
        
                      if(lista.regCodigo == this.codigoRegimen){
                        
                          this.idCriterio = element.criCodigo;
                          this.nombreCriterio = element.criDescripcion;
        
                          setTimeout(() => {
                                let objetoDatos = {
                                       idHabilidad: this.idHabilidad,
                                       nombreHabilidad: this.nombreHabilidad,
                                       idCriterios: this.idCriterio,
                                       nombreCriterios: this.nombreCriterio,
                                       idNivel: this.idNivel,
                                       nombreNivel: this.nombreNivel,
                                  }
        
                                this.listaDatosFormulario.push(objetoDatos);
                            }, 1000);
                          }
                         }
                  })
                }   
              }
            }
          });

      }, 1000);
    })
  }


  openEliminarModal(est: any){
    Swal.fire({
      title: '¿Está seguro que desea eliminar este Criterio?',
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Si',
      cancelButtonText: 'No'
    }).then((result) => {
      if(result.isConfirmed){
        this.academicoService.eliminarCriterio(est.idCriterios).then(data => {

          Swal.fire({
            title: 'Criterio eliminado',
            text: "Datos guardados",
            icon: 'success',
            confirmButtonColor:  '#3085d6',
            confirmButtonText: 'ok'
          })

          setTimeout(() => {

            this.academicoService.listarCriterios().then(data => {
              this.listaCriterios = data.listado
            })
      
      
            setTimeout(() => {
              this.listaDatosFormulario = [];
      
                this.listaCriterios.forEach(element => {
                  if(element.habCodigo == this.idHabilidad){
            
                    if(element.nivCodigo == this.idNivel){
            
                      if(element.criEstado == 1){
            
                        this.catalogoService.buscarReanlePorCodigo(element.reanleCodigo).then(data => {
            
                          let lista = data.objeto;
              
                          if(lista.reanleTipo = "A"){
              
                            if(lista.regCodigo == this.codigoRegimen){
                              
                                this.idCriterio = element.criCodigo;
                                this.nombreCriterio = element.criDescripcion;
              
                                setTimeout(() => {
                                      let objetoDatos = {
                                             idHabilidad: this.idHabilidad,
                                             nombreHabilidad: this.nombreHabilidad,
                                             idCriterios: this.idCriterio,
                                             nombreCriterios: this.nombreCriterio,
                                             idNivel: this.idNivel,
                                             nombreNivel: this.nombreNivel,
                                        }
              
                                      this.listaDatosFormulario.push(objetoDatos);
                                  }, 1000);
                                }
                               }
                        })
                      }   
                    }
                  }
                });
      
            }, 1000);
            
          }, 1000);

        })
      }
    })
  }




  seleccionarHabilidad(){

    this.listaNivel = null;

    this.listaDatosFormulario = [];

    this.nombreHabilidad = this.seleccionado?.nombre;
    this.idHabilidad = this.seleccionado?.id;

    setTimeout(() => {
      this.catalogoService.listarNiveles().then(data => {
        this.listaNivel = data.listado;
      })
    }, 500);

  }


  seleccionarNivel(){

    this.listaDatosFormulario = [];


    this.idNivel = this.seleccionadoNivel?.idNivel;
    this.nombreNivel = this.seleccionadoNivel?.nombreNivel;


    this.listaCriterios.forEach(element => {

      if(element.habCodigo == this.idHabilidad){

        if(element.nivCodigo == this.idNivel){

          if(element.criEstado == 1){

            this.catalogoService.buscarReanlePorCodigo(element.reanleCodigo).then(data => {

              let lista = data.objeto;
  
              if(lista.reanleTipo = "A"){
  
                if(lista.regCodigo == this.codigoRegimen){
                  
                    this.idCriterio = element.criCodigo;
                    this.nombreCriterio = element.criDescripcion;
  
                    setTimeout(() => {
                          let objetoDatos = {
                                 idHabilidad: this.idHabilidad,
                                 nombreHabilidad: this.nombreHabilidad,
                                 idCriterios: this.idCriterio,
                                 nombreCriterios: this.nombreCriterio,
                                 idNivel: this.idNivel,
                                 nombreNivel: this.nombreNivel,
                                 servicioCodigo: element.sereduCodigo
                            }
  
                          this.listaDatosFormulario.push(objetoDatos);
                      }, 1000);
  
                    }
  
                   }
  
            })

          }

          

        }

      }
    });
  }


}
