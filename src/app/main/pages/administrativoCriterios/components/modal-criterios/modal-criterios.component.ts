import { Component, EventEmitter, Input, OnInit, Output, ViewChild  } from '@angular/core';
import { User } from 'app/auth/models';
import { AcademicoService } from "../../services/academico.service";
import { CatalogoService } from "../../services/catalogo.service";
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import Swal from 'sweetalert2/dist/sweetalert2.js';
import { Router } from '@angular/router';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import {Location} from '@angular/common';

@Component({
  selector: 'app-modal-criterios',
  templateUrl: './modal-criterios.component.html',
  styleUrls: ['./modal-criterios.component.scss']
})
export class ModalCriteriosComponent implements OnInit {

  //datos para el filtro
  listaCriterios;
  codigoNivel;
  currentUser: User;
  datosInstitucion = [];
  codigoRegimen;
  realmCodigo;
  anieLect = 25;
  descripcion = null;

  //datos de categorias
  codigoCategoria;
  listaCatHabilidades;

  //datos de habilidad
  codigoHabilidad;
  listaHabilidades=[];

  //datos de nivel
  nivel;
  listaNivel;

  //datos de modalidad
  codigoModalidad;
  listaModalidad = [];
  listaServicios = [];


  @Input() fromParent;
  @Output() cambiosGuardados = new EventEmitter<void>();

  constructor(
    private readonly adecademicoService: AcademicoService,
    private readonly catalogoService: CatalogoService,
    public activeModal: NgbActiveModal
  ) {
    this.currentUser = JSON.parse(localStorage.getItem("currentUser"))
   }

  ngOnInit() {

    this.datosInstitucion.push(this.currentUser.sede);

    this.catalogoService.buscarInstitucionPorAmie(this.datosInstitucion[0].nemonico).then(data => {
      let lista = data;

      if(lista[0].descripcionRegimen == "SIERRA"){

        this.codigoRegimen = 1;

      }else if(lista[0].descripcionRegimen == "COSTA"){

        this.codigoRegimen = 2;

      }
    })

    setTimeout(() => {
      this.catalogoService.buscarRegAnioElectivo(this.codigoRegimen).then(data => {
        let listado = data.listado;

        for(let i = 0; i < listado.length; i++){
          if(listado[i].reanleTipo = "A"){
            if(listado[i].anilecCodigo == this.anieLect){

              this.realmCodigo =  listado[i].reanleCodigo;

            }
            
          }
        }
        
        
      })
    }, 1000);


    this.adecademicoService.listarCatHabilidades().then(data => {
      this.listaCatHabilidades = data.listado;
    })

  }

  seleccionCategoria(event){
    this.codigoCategoria = event;


    this.nivel = null;
    this.codigoHabilidad = null;
    this.codigoModalidad = null;
    this.listaHabilidades = [];
    this.listaNivel = [];
    this.listaModalidad = [];

    this.adecademicoService.listarHabilidades().then(data => {
      let listado = data.listado
      for(let i = 0; i < listado.length; i++){

        if(listado[i].cthCodigo == event){

          this.listaHabilidades.push(listado[i])

        }
      }
    })
  }


  seleccionHabilidad(event){

    this.codigoHabilidad = event;

    this.nivel = null;
    this.codigoModalidad = null;
    this.listaNivel = [];
    this.listaModalidad = [];

    this.catalogoService.listarNiveles().then(data => {
      this.listaNivel = data.listado;
    })
  }


  seleccionNivel(event){

    this.nivel = event;
    this.codigoModalidad = null;
    this.listaModalidad = [];


    setTimeout(() => {

      this.catalogoService.listaServiciosEducativos().then(data => {
        let lista = data.listado;
        for(let i = 0; i < lista.length; i++){

          if(lista[i].reanleCodigo == this.realmCodigo){
            if(lista[i].reanleCodigo !== null){

              this.catalogoService.buscarMotiedPorCodigo(lista[i].motiedCodigo).then(data => {
                let listaMotied = data.objeto;
              
                //limpiar de los repetidos

                let opcionDatos = {
                  codigoServicio: lista[i].sereduCodigo ,
                  codigoModTied: listaMotied.motiedCodigo,
                  descripcionMotied: listaMotied.motiedDescripcion
                }

                this.listaModalidad.push(opcionDatos);

              })

            }
          }

        }

        setTimeout(() => {

          let result = this.listaModalidad.filter((item,index)=>{
            return this.listaModalidad.indexOf(item) === index;
          })

          result.forEach(element => {

            this.listaServicios.push(element);
          });

        }, 500);
        
      })

    }, 1000);

    


  }


  seleccionServicio(event){

    this.codigoModalidad = event;

  }


  closeModal(sendData) {
    if(sendData==='ok'){
      Swal.fire({
        title: 'Confirmar',
        text: '¿Desea Guardar la Información?',
        icon: 'question',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Guardar',
        cancelButtonText: 'Cancelar'
      }).then((result) => {
        if (result.isConfirmed) {

          let datos ={
              "habCodigo" : parseInt(this.codigoHabilidad),
              "criDescripcion" : this.descripcion,
              "nivCodigo": parseInt(this.nivel),
              "reanleCodigo": parseInt(this.realmCodigo),
              "sereduCodigo": parseInt(this.codigoModalidad),
              "criEstado": 1
           }

           this.adecademicoService.guardarCriterios(datos).subscribe({
            next: (Response) => {
              Swal.fire({
                title: "Datos Guardados",
                text: "El Criterio ha sido guardado",
                icon: "success"
              });
              this.activeModal.close(this.fromParent);
            },
            error: (error) => {
              Swal.fire({
                icon: 'error',
                title: 'Atención!',
                text: 'Error en guardar los datos'
              })
            }
           })

        }
      });


    }else if(sendData==='cancel'){
      this.activeModal.close(this.fromParent);
    }else if(sendData==='dismiss'){
      this.activeModal.close(this.fromParent);
    }
  }
  

}
