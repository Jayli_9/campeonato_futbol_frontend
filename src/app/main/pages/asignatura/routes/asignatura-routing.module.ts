import { Routes } from "@angular/router";
import { AuthGuard } from "app/auth/helpers/auth.guards";
import { AsignaturaComponent } from "../components/asignatura/asignatura.component";

export const RUTA_ASIGNATURA:Routes=[
    {
        path:'asignatura',
        component:AsignaturaComponent
    }
]