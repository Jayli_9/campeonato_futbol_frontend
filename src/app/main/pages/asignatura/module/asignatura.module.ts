import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AsignaturaComponent } from '../components/asignatura/asignatura.component';
import { RUTA_ASIGNATURA } from '../routes/asignatura-routing.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { CoreCommonModule } from '@core/common.module';
import { ContentHeaderModule } from 'app/layout/components/content-header/content-header.module';
import { MaterialModule } from 'app/main/shared/material/material.module';



@NgModule({
  declarations: [AsignaturaComponent],
  imports: [
    CommonModule,
    RouterModule.forChild(RUTA_ASIGNATURA),
    FormsModule,
    ReactiveFormsModule,
    MaterialModule,
    ContentHeaderModule,
    CoreCommonModule
  ]
})
export class AsignaturaModule { }