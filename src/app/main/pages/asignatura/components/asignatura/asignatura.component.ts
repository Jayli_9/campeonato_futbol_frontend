import { Component, OnInit, ViewChild } from '@angular/core';
import { UntypedFormBuilder, UntypedFormGroup, Validators } from '@angular/forms';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { RespuestaTipoAsignaturaInterfaz } from 'app/main/pages/tipo-asignatura/interfaces/respuesta-tipo-asignatura-interfaz';
import { NgxSpinnerService } from 'ngx-spinner';
import Swal from 'sweetalert2';
import { Agrupacion } from '../../interfaces/agrupacion';
import { AreaConocimiento } from '../../interfaces/area-conocimiento';
import { Asignatura } from '../../interfaces/asignatura';
import { Especialidad } from '../../interfaces/especialidad';
import { Grado } from '../../interfaces/grado';
import { RespuestaAgrupacionInterfaz } from '../../interfaces/respuesta-agrupacion-interfaz';
import { RespuestaAreaConocimientoInterfaz } from '../../interfaces/respuesta-area-conocimiento-interfaz';
import { RespuestaAsignaturaInterfaz } from '../../interfaces/respuesta-asignatura-interfaz';
import { RespuestaEspecialidadInterfaz } from '../../interfaces/respuesta-especialidad-interfaz';
import { RespuestaGradoInterfaz } from '../../interfaces/respuesta-grado-interfaz';
import { RespuestaTipoValoracionInterfaz } from '../../interfaces/respuesta-tipo-valoracion-interfaz';
import { TipoAsignatura } from '../../interfaces/tipo-asignatura';
import { TipoValoracion } from '../../interfaces/tipo-valoracion';
import { AsignaturaService } from '../../services/asignatura.service';
import { GradosParametros } from '../../interfaces/grados-parametros';

@Component({
    selector: 'app-asignatura',
    templateUrl: './asignatura.component.html',
    styleUrls: ['./asignatura.component.scss']
})
export class AsignaturaComponent implements OnInit {

    public contentHeader: object;
    public frmAsignaturaCreateEdit: UntypedFormGroup;
    public submitted = false;
    public codigoVariable;
    public selectTipoValoracion;
    public asiNemonico;
    public selectTipoValoracionAc;

    listaAsignatura:Asignatura[] = null;
    listaTipoAsignatura: TipoAsignatura[] = null;
    listaTipoValoracion: TipoValoracion[] = null;
    listaAreaConocimiento: AreaConocimiento[] = null;
    listaAgrupacion: Agrupacion[] = null;
    listaAgrupacionfiltro: Agrupacion[] = null;
    listaGrado: Grado[] = null;
    listaEspecialidad: Especialidad[] = null;

    columnasAsignarura=['num', 'asignatura', 'grados', 'estado', 'acciones'];
    dataSource: MatTableDataSource<Asignatura>;

    @ViewChild(MatPaginator) paginator: MatPaginator;
    @ViewChild(MatSort) sort: MatSort;
    constructor(private readonly fb: UntypedFormBuilder,
        private modalService: NgbModal,
        public spinner: NgxSpinnerService,
        private _asignaturaService: AsignaturaService
        ){
    }
    
    ngOnInit(): void {
        this.contentHeader = {
            headerTitle: 'Asignatura',
            actionButton: false,
            breadcrumb: {
                type: '',
                links: [
                    {
                      name: 'Inicio',
                      isLink: true,
                      link: '/pages/inicio'
                    },
                    {
                      name: 'Asignatura',
                      isLink: false
                    },
                ]
            }
          };
          this.listarTodasLasAsignaturas();
          this.listaTodosLosTiposAsignaturas();
          this.listaTodosLosTiposValoraciones();
          this.listaTodasLasAreasConocimiento();
          this.listaTodasLasAgrupaciones();
          this.listaTodosLosGrados();
    }

    codigoDeTiipoValoracion(event){
        this.codigoVariable=event;
    }


    listarTodasLasAsignaturas(){
        this.spinner.show();
        this._asignaturaService.listarTodasLasAsignaturas().subscribe(
            (respuesta:RespuestaAsignaturaInterfaz)=>{
                this.listaAsignatura = respuesta.listado;
                this.listarGradosPorListaAsignatura();
                this.dataSource = new MatTableDataSource(this.listaAsignatura);
                this.dataSource.paginator = this.paginator;
                this.dataSource.paginator._intl.itemsPerPageLabel="Asignaturas por página";
                this.dataSource.paginator._intl.nextPageLabel="Siguiente";
                this.dataSource.paginator._intl.previousPageLabel="Anterior";
                this.dataSource.sort = this.sort;
                this.spinner.hide();
            },
            (error:any)=>{
                this.spinner.hide();
                Swal.fire('Ups! ocurrió un error al cargar Asignaturas','','error'); 
            }
        );
    }

    listarGradosPorListaAsignatura() {
        let graCodigo = this.listaAsignatura.map((asignatura) => asignatura.graCodigo);
        let graCodigoLimpio = Array.from(new Set(graCodigo));
        let listaDesGrados:Grado[];
        let gradosParametros:GradosParametros = {listaGraCodigo:graCodigoLimpio};
        this._asignaturaService.listarGradosPorListaGrados(gradosParametros).subscribe(
            (respuesta:RespuestaGradoInterfaz)=>{
                listaDesGrados=respuesta.listado;
                for(let i = 0; i<this.listaAsignatura.length;i++){
                    let desGrado:Grado = listaDesGrados.find((desGrado)=>desGrado.graCodigo==this.listaAsignatura[i].graCodigo);
                    if (desGrado != undefined) {
                        this.listaAsignatura[i].graDescripcion = desGrado.graDescripcion;
                    }
                    
                }
            },
            (error:any)=>{
                this.spinner.hide();
                Swal.fire('Ups! ocurrió un error la cargar la litas de grados','','error'); 
            }
        );
    }

    listaTodosLosTiposAsignaturas(){
        this.spinner.show();
        this.listaTipoAsignatura = [];
        this._asignaturaService.listarTodosLosTipoAsignatura().subscribe(
            (respuesta:RespuestaTipoAsignaturaInterfaz)=>{
                this.listaTipoAsignatura = respuesta.listado;
                this.spinner.hide();
            },
            (error:any)=>{
                this.spinner.hide();
                Swal.fire('Ups! ocurrió un error al cargar Tipo Asignatura','','error'); 
            }
        );
    }

    listaTodosLosTiposValoraciones(){
        this.spinner.show();
        this.listaTipoValoracion = [];
        this._asignaturaService.listarTodosLosTiposValoracion().subscribe(
            (respuesta:RespuestaTipoValoracionInterfaz)=>{
                this.listaTipoValoracion = respuesta.listado;
                this.spinner.hide();
            },
            (error:any)=>{
                this.spinner.hide();
                Swal.fire('Ups! ocurrió un error al cargar Tipo Valoración','','error'); 
            }
        );
    }

    listaTodasLasAreasConocimiento(){
        this.spinner.show();
        this.listaAreaConocimiento = [];
        this._asignaturaService.listarTodasLasAreasConocimiento().subscribe(
            (respuesta:RespuestaAreaConocimientoInterfaz)=>{
                this.listaAreaConocimiento = respuesta.listado;
                this.spinner.hide();
            },
            (error:any)=>{
                this.spinner.hide();
                Swal.fire('Ups! ocurrió un error al cargar Area Conocimiento','','error'); 
            }
        );
    }

    getListaGrupos(): any[] {
        return this.listaAgrupacion.filter(codigo => codigo.agrCodigo === 3);
    }

    listaTodasLasAgrupaciones(){
        this.spinner.show();
        this.listaAgrupacion = [];
        this.listaAgrupacionfiltro = [];
        this._asignaturaService.listarTodasLasAgrupaciones().subscribe(
            (respuesta:RespuestaAgrupacionInterfaz)=>{
                this.listaAgrupacion = respuesta.listado;
                this.listaAgrupacionfiltro = this.listaAgrupacion.slice(0, 1);
                this.spinner.hide();
            },
            (error:any)=>{
                this.spinner.hide();
                Swal.fire('Ups! ocurrió un error al cargar Agrupaciones','','error'); 
            }
        );
    }

    listaTodosLosGrados(){
        this.spinner.show();
        this.listaGrado=[];
        this._asignaturaService.listarTodosLosGrados().subscribe(
            (respuesta:RespuestaGradoInterfaz)=>{
                this.listaGrado = respuesta.listado;
                this.spinner.hide();
            },
            (error:any)=>{
                this.spinner.hide();
                Swal.fire('Ups! ocurrió un error al cargar Grados','','error'); 
            }
        );
    }

    listaTodasLasEspecialidadesPorNivel(codGrado:any){
        let nivel = null;
        for (let i = 0; i < this.listaGrado.length; i++) {
            if (this.listaGrado[i].graCodigo == codGrado) {
                nivel = this.listaGrado[i].nivCodigo;
            }
        }
        
        this.spinner.show();
        this.listaEspecialidad = null;

        if (nivel == 8) {
            this._asignaturaService.listarLasEspecialidadesPorNivel(nivel).subscribe(
                (respuesta:RespuestaEspecialidadInterfaz)=>{
                    this.listaEspecialidad = respuesta.listado;
                    this.spinner.hide();
                },
                (error:any)=>{
                    this.spinner.hide();
                    Swal.fire('Ups! ocurrió un error al cargar Especialidades','','error'); 
                }
            );
        } else {
            this.spinner.hide();
        }
    }

    nuevaAsignatura(modalForm) {
        this.submitted=false;
        this.selectTipoValoracion='';
        this.asiNemonico='';
        this.frmAsignaturaCreateEdit=this.fb.group({
            asiDescripcion: ['', [Validators.required]],
            asiHoras: ['', [Validators.required]],
            asiNemonico: ['', [Validators.required]],
            acaAgrupacion: [null, [Validators.required]],
            acaAreaConocimiento: [null, [Validators.required]],
            acaTipoAsignatura: [null, [Validators.required]],
            acaTipoValoracion: [null, [Validators.required]],
            graCodigo: [null, [Validators.required]],
            espCodigo: [null]
        });
        this.modalService.open(modalForm);
    }

    limpiarValoresAntiguos(cor){
        if(cor == "tipoValoracion"){
            this.asiNemonico=null
            this.frmAsignaturaCreateEdit.get('asiNemonico').reset();
            this.frmAsignaturaCreateEdit.get('acaAreaConocimiento').reset();
            this.frmAsignaturaCreateEdit.get('asiDescripcion').reset();
            this.frmAsignaturaCreateEdit.get('asiHoras').reset();
            this.frmAsignaturaCreateEdit.get('acaAgrupacion').reset();
            this.frmAsignaturaCreateEdit.get('graCodigo').reset();
            this.frmAsignaturaCreateEdit.get('espCodigo').reset();
        }else if(cor == "nemonico"){
            this.frmAsignaturaCreateEdit.get('acaAreaConocimiento').reset();
            this.frmAsignaturaCreateEdit.get('asiDescripcion').reset();
            this.frmAsignaturaCreateEdit.get('asiHoras').reset();
            this.frmAsignaturaCreateEdit.get('acaAgrupacion').reset();
            this.frmAsignaturaCreateEdit.get('graCodigo').reset();
            this.frmAsignaturaCreateEdit.get('espCodigo').reset();
        }else if(cor == "areaConocimineto"){
            this.frmAsignaturaCreateEdit.get('asiDescripcion').reset();
            this.frmAsignaturaCreateEdit.get('asiHoras').reset();
            this.frmAsignaturaCreateEdit.get('acaAgrupacion').reset();
            this.frmAsignaturaCreateEdit.get('graCodigo').reset();
            this.frmAsignaturaCreateEdit.get('espCodigo').reset();
        }else if(cor == "descripcion"){
            this.frmAsignaturaCreateEdit.get('asiHoras').reset();
            this.frmAsignaturaCreateEdit.get('acaAgrupacion').reset();
            this.frmAsignaturaCreateEdit.get('graCodigo').reset();
            this.frmAsignaturaCreateEdit.get('espCodigo').reset();
        }else if(cor == "horas"){
            this.frmAsignaturaCreateEdit.get('acaAgrupacion').reset();
            this.frmAsignaturaCreateEdit.get('graCodigo').reset();
            this.frmAsignaturaCreateEdit.get('espCodigo').reset();
        }else if(cor == "agrupacion"){
            this.frmAsignaturaCreateEdit.get('graCodigo').reset();
            this.frmAsignaturaCreateEdit.get('espCodigo').reset();
        }else if(cor == "graCodigo"){
            this.frmAsignaturaCreateEdit.get('espCodigo').reset();
        }
    }

    refrescarValues() {
        this.asiNemonico=null;
        this.frmAsignaturaCreateEdit.get('asiDescripcion').reset();
        this.frmAsignaturaCreateEdit.get('asiHoras').reset();
        this.frmAsignaturaCreateEdit.get('asiNemonico').reset();
        this.frmAsignaturaCreateEdit.get('acaAgrupacion').reset();
        this.frmAsignaturaCreateEdit.get('acaAreaConocimiento').reset();
        this.frmAsignaturaCreateEdit.get('acaTipoValoracion').reset();
        this.frmAsignaturaCreateEdit.get('graCodigo').reset();
        this.frmAsignaturaCreateEdit.get('espCodigo').reset();
    }

    refrescaEditValues(){
        this.asiNemonico=null;
        this.frmAsignaturaCreateEdit.get('asiDescripcion').reset();
        this.frmAsignaturaCreateEdit.get('asiEstado').reset();
        this.frmAsignaturaCreateEdit.get('asiHoras').reset();
        this.frmAsignaturaCreateEdit.get('asiNemonico').reset();
        this.frmAsignaturaCreateEdit.get('acaAgrupacion').reset();
        this.frmAsignaturaCreateEdit.get('acaAreaConocimiento').reset();
        this.frmAsignaturaCreateEdit.get('acaTipoValoracion').reset();
        this.frmAsignaturaCreateEdit.get('graCodigo').reset();
        this.frmAsignaturaCreateEdit.get('espCodigo').reset();
    }

    editAsignatura(modalForm,asignaturaEdit:Asignatura) {

        if (asignaturaEdit.asiEstado==0) {
            Swal.fire(`Debe activar la asignatura!`,'','success');
            return;
        }


        this.selectTipoValoracionAc=asignaturaEdit.acaTipoValoracion;
        this.asiNemonico=asignaturaEdit.asiNemonico;
        this.submitted=false;
        this.frmAsignaturaCreateEdit=this.fb.group({
            asiCodigo:[asignaturaEdit.asiCodigo],
            asiDescripcion: [asignaturaEdit.asiDescripcion],
            asiEstado:[asignaturaEdit.asiEstado],
            asiHoras: [asignaturaEdit.asiHoras],
            asiNemonico: [asignaturaEdit.asiNemonico],
            acaAgrupacion: [asignaturaEdit.acaAgrupacion],
            acaAreaConocimiento: [asignaturaEdit.acaAreaConocimiento],
            acaTipoAsignatura: [asignaturaEdit.acaTipoAsignatura],
            acaTipoValoracion: [asignaturaEdit.acaTipoValoracion],
            graCodigo: [asignaturaEdit.graCodigo],
            espCodigo: [asignaturaEdit.espCodigo]
        });
        this.modalService.open(modalForm);
        this.listaTodasLasEspecialidadesPorNivel(asignaturaEdit.graCodigo);
    }

    guardarAsignatura(modalForm){

        if(this.frmAsignaturaCreateEdit.get('acaAgrupacion').value != 3){

            Swal.fire({
                icon: 'error',
                title: 'Atención',
                text: 'Agrupación y Escala no coinciden'
              })

        }else{

            this.spinner.show();
            this.submitted = true;

            /**
             * if (this.frmAsignaturaCreateEdit.invalid) {
                   this.spinner.hide();
                     return;
        }
             */
        

            let asignaturaNueva:Asignatura={
                asiDescripcion:this.frmAsignaturaCreateEdit.get('asiDescripcion').value,
                asiEstado:1,
                asiHoras:this.frmAsignaturaCreateEdit.get('asiHoras').value,
                asiNemonico:this.asiNemonico,
                acaAgrupacion:this.frmAsignaturaCreateEdit.get('acaAgrupacion').value,
                acaAreaConocimiento:this.frmAsignaturaCreateEdit.get('acaAreaConocimiento').value,
                acaTipoAsignatura:this.frmAsignaturaCreateEdit.get('acaTipoAsignatura').value,
                acaTipoValoracion:this.frmAsignaturaCreateEdit.get('acaTipoValoracion').value,
                graCodigo:this.frmAsignaturaCreateEdit.get('graCodigo').value,
                espCodigo:this.frmAsignaturaCreateEdit.get('espCodigo').value
            }

            //console.log(asignaturaNueva);

            this._asignaturaService.guardarActualizarAsignatura(asignaturaNueva).subscribe(
                (respuesta:any)=>{
                    Swal.fire(`Asignatura Creada!`,'','success');
                    this.spinner.hide();
                    this.listarTodasLasAsignaturas();
                },
                (error:any)=>{
                    Swal.fire(`No se pudo crear la Asignatura `,'','error');
                    this.spinner.hide();
                    this.listarTodasLasAsignaturas();
                }
            );

            modalForm.close('Accept click');
        }
    }

    activarInactivar(asignaturaActualizar:Asignatura){
        this.spinner.show();

        
        if (asignaturaActualizar.asiEstado==1) {
            this._asignaturaService.activarInactivarAsignatura(asignaturaActualizar.asiCodigo).subscribe(
                (respuesta:any)=>{
                    this.spinner.hide();
                    Swal.fire('Asignatura Inactivada!','','success'); 
                    this.listarTodasLasAsignaturas();
                },
                (error:any)=>{
                    this.spinner.hide();
                    Swal.fire('Ups! ocurrió un error','','error'); 
                    this.listarTodasLasAsignaturas();
                }
            );
        } else {
            asignaturaActualizar.asiEstado = 1;
            this._asignaturaService.guardarActualizarAsignatura(asignaturaActualizar).subscribe(
                (respuesta:any)=>{
                    Swal.fire('Asignatura Activada!','','success');
                    this.spinner.hide();
                    this.listarTodasLasAsignaturas();
                },
                (error:any)=>{
                    Swal.fire('Ups! ocurrió un error','','error');
                    this.spinner.hide();
                    this.listarTodasLasAsignaturas();
                }
            );
        }
    }

    actualizarAsignatura(modalForm){


        if(this.frmAsignaturaCreateEdit.get('acaTipoValoracion').value == 3){

            Swal.fire({
                icon: 'error',
                title: 'Atención',
                text: 'Agrupación y Escala no coinciden'
              })

        }else{

            this.spinner.show();
        this.submitted = true;

        if (this.frmAsignaturaCreateEdit.invalid) {
            this.spinner.hide();
            return;
        }

        this.asiNemonico=this.frmAsignaturaCreateEdit.get('asiNemonico').value;

        let asignaturaGuardar:Asignatura={
            asiCodigo:this.frmAsignaturaCreateEdit.get('asiCodigo').value,
            asiDescripcion:this.frmAsignaturaCreateEdit.get('asiDescripcion').value,
            asiEstado:1,
            asiHoras:this.frmAsignaturaCreateEdit.get('asiHoras').value,
            asiNemonico:this.asiNemonico,
            acaAgrupacion:this.frmAsignaturaCreateEdit.get('acaAgrupacion').value,
            acaAreaConocimiento:this.frmAsignaturaCreateEdit.get('acaAreaConocimiento').value,
            acaTipoAsignatura:this.frmAsignaturaCreateEdit.get('acaTipoAsignatura').value,
            acaTipoValoracion:this.frmAsignaturaCreateEdit.get('acaTipoValoracion').value,
            graCodigo:this.frmAsignaturaCreateEdit.get('graCodigo').value,
            espCodigo:this.frmAsignaturaCreateEdit.get('espCodigo').value
        }

        this._asignaturaService.guardarActualizarAsignatura(asignaturaGuardar).subscribe(
            (respuesta:any)=>{
                Swal.fire(`Asignatura Guardada!`,'','success');
                this.spinner.hide();
                this.listarTodasLasAsignaturas();
            },
            (error:any)=>{
                Swal.fire(`No se pudo guardar la Asignatura `,'','error');
                this.spinner.hide();
                this.listarTodasLasAsignaturas();
            },
        );
        modalForm.close('Accept click');

        }

        
    }

    applyFilter(event: Event) {
        const filterValue = (event.target as HTMLInputElement).value;
        this.dataSource.filter = filterValue.trim().toLowerCase();
      
        if (this.dataSource.paginator) {
          this.dataSource.paginator.firstPage();
        }
    }

    get ReactiveFrmAsignaturaCreateEdit() {
        return this.frmAsignaturaCreateEdit.controls;
    }

}