import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { environment } from "environments/environment";
import { Asignatura } from "../interfaces/asignatura";
import { RespuestaAgrupacionInterfaz } from "../interfaces/respuesta-agrupacion-interfaz";
import { RespuestaAreaConocimientoInterfaz } from "../interfaces/respuesta-area-conocimiento-interfaz";
import { RespuestaAsignaturaInterfaz } from "../interfaces/respuesta-asignatura-interfaz";
import { RespuestaEspecialidadInterfaz } from "../interfaces/respuesta-especialidad-interfaz";
import { RespuestaGradoInterfaz } from "../interfaces/respuesta-grado-interfaz";
import { RespuestaTipoAsignaturaInterfaz } from "../interfaces/respuesta-tipo-asignatura-interfaz";
import { RespuestaTipoValoracionInterfaz } from "../interfaces/respuesta-tipo-valoracion-interfaz";
import { GradosParametros } from "../interfaces/grados-parametros";

@Injectable({
    providedIn: 'root'
})
export class AsignaturaService {

    url_academico=environment.url_academico;
    url_catalogo=environment.url_catalogo;

    constructor(
        public _http:HttpClient
    ) { }

    guardarActualizarAsignatura(asignatura: Asignatura) {
        let url_ws=`${this.url_academico}/private/guardarAsignatura`;
        return this._http.post(url_ws,asignatura);
    }

    activarInactivarAsignatura(codigo:number) {
        let url_ws=`${this.url_academico}/private/eliminarAsignaturaPorId/${codigo}`;
        return this._http.delete(url_ws);
    }

    listarTodasLasAsignaturas() {
        let url_ws=`${this.url_academico}/private/listarTodasLasAsignaturasDTO`;
        return this._http.get<RespuestaAsignaturaInterfaz>(url_ws);
    }

    listarTodosLosTipoAsignatura() {
        let url_ws=`${this.url_academico}/private/listarTodosLosTiposDeAsignatura`;
        return this._http.get<RespuestaTipoAsignaturaInterfaz>(url_ws);
    }

    listarTodosLosTiposValoracion() {
        let url_ws=`${this.url_academico}/private/listarTodosLosTiposDeValoracion`;
        return this._http.get<RespuestaTipoValoracionInterfaz>(url_ws);
    }

    listarTodasLasAreasConocimiento() {
        let url_ws=`${this.url_academico}/private/listarTodasLasAreasDeConocimiento`;
        return this._http.get<RespuestaAreaConocimientoInterfaz>(url_ws);
    }

    listarTodasLasAgrupaciones() {
        let url_ws=`${this.url_academico}/private/listarTodasLasAgrupaciones`;
        return this._http.get<RespuestaAgrupacionInterfaz>(url_ws);
    }

    listarTodosLosGrados() {
        let url_ws=`${this.url_catalogo}/private/listarCatalogoGrados`;
        return this._http.get<RespuestaGradoInterfaz>(url_ws);
    }

    listarGradosPorListaGrados(gradosParametros:GradosParametros) {
        let url_ws=`${this.url_catalogo}/private/listarGradosPorListaGraCodigoYEstado`;
        return this._http.post<RespuestaGradoInterfaz>(url_ws,gradosParametros);
    }

    listarLasEspecialidadesPorNivel(nivel: Number) {
        let url_ws=`${this.url_catalogo}/private/buscarEspecialidadPorNivelYEstado/${nivel}`;
        return this._http.get<RespuestaEspecialidadInterfaz>(url_ws);
    }

}