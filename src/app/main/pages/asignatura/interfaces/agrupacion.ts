export interface Agrupacion {
    agrCodigo?: number;
    agrDescripcion: string;
    agrEstado: number;
    agrNemonico: string;
}
