import { RespuestaGeneralInterfaz } from "app/main/shared/interfaces/respuesta-general-interfaz";
import { Asignatura } from "./asignatura";

export interface RespuestaAsignaturaInterfaz extends RespuestaGeneralInterfaz{
    listado: Asignatura[];
    objeto: Asignatura;
}