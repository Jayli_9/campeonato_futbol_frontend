export interface Grado {
    graCodigo?: number;
    nivCodigo: number;
    graDescripcion: string;
    graEstado: number;
    graNemonico: string;
}