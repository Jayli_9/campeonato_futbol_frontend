import { RespuestaGeneralInterfaz } from "app/main/shared/interfaces/respuesta-general-interfaz";
import { TipoAsignatura } from "./tipo-asignatura";

export interface RespuestaTipoAsignaturaInterfaz extends RespuestaGeneralInterfaz{
    listado: TipoAsignatura[];
    objeto: TipoAsignatura;
}
