export interface AreaConocimiento {
    arcoCodigo?: number;
    arcoDescripcion: string;
    arcoEstado: number;
    arcoNemonico: string;
}
