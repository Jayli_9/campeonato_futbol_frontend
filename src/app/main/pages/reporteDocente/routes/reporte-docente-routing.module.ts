import { Routes } from "@angular/router";
import { AuthGuard } from "app/auth/helpers";
import { PrincipalComponent } from "../components/principal/principal.component";

export const RUTA_REPORTE_DOCENTE:Routes=[
    {
        path:'reporteDocentes',
        component:PrincipalComponent
    }
]