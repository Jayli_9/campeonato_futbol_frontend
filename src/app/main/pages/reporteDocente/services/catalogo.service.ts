import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { environment } from 'environments/environment';
import { ResponseGenerico } from '../interfaces/response-generico';
import { jornadaInterface } from "../interfaces/jornada";
import { nivelesInterface } from "../interfaces/niveles";
import { institucionEducativaInterface } from "../interfaces/institucionEducativa";
import { gradoInterface } from "../interfaces/grado";

@Injectable({
  providedIn: 'root'
})
export class CatalogoService {

  private readonly URL_REST = environment.url_catalogo;

constructor(private _http: HttpClient) { }

ListarJornadas(): Promise<ResponseGenerico<jornadaInterface>>{
  return new Promise((resolve, reject) => {
    this._http.get(`${environment.url_catalogo}/private/listarJornadas`).subscribe((response: ResponseGenerico<jornadaInterface>) => {
      resolve(response);
    }, reject);
  })
}


listarNiveles(): Promise<ResponseGenerico<nivelesInterface>>{
  return new Promise((resolve, reject) => {
    this._http.get(`${environment.url_catalogo}/private/listarNiveles`).subscribe((response: ResponseGenerico<nivelesInterface>) => {
      resolve(response);
    }, reject);
  })
}

buscarInstitucionPorAmie(cod): Promise<ResponseGenerico<institucionEducativaInterface>>{
  return new Promise((resolve, reject) => {
    this._http.get(`${environment.url_catalogo}/private/buscarInstitucionEducativaPorAmie/`+cod).subscribe((response: ResponseGenerico<institucionEducativaInterface>) => {
      resolve(response);
    }, reject);
  })
}


buscarGradosPorCodigo(cod): Promise<ResponseGenerico<gradoInterface>>{
  return new Promise((resolve, reject) => {
    this._http.get(`${environment.url_catalogo}/private/buscarCatalogoGradosPorCodigo/`+cod).subscribe((response: ResponseGenerico<gradoInterface>) => {
      resolve(response);
    }, reject);
  })
}


buscarJornadaPorCodigo(cod): Promise<ResponseGenerico<jornadaInterface>>{
  return new Promise((resolve, reject) => {
    this._http.get(`${environment.url_catalogo}/private/buscarJornadaPorCodigo/`+cod).subscribe((response: ResponseGenerico<jornadaInterface>) => {
      resolve(response);
    }, reject);
  })
}


buscarNivelesPorCodigo(cod): Promise<ResponseGenerico<nivelesInterface>>{
  return new Promise((resolve, reject) => {
    this._http.get(`${environment.url_catalogo}/private/buscarNivelPorCodigo/`+cod).subscribe((response: ResponseGenerico<nivelesInterface>) => {
      resolve(response);
    }, reject);
  })
}

}
