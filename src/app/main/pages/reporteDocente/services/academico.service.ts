import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { environment } from 'environments/environment';
import { ResponseGenerico } from '../interfaces/response-generico';
import { asignacionDocenteInterface } from "../interfaces/asignacionDocentes";

@Injectable({
  providedIn: 'root'
})
export class AcademicoService {

  private readonly URL_REST = environment.url_academico;

constructor(private _http: HttpClient) { }


listarTodosLosDistributivos(): Promise<ResponseGenerico<asignacionDocenteInterface>>{
  return new Promise((resolve, reject) => {
    this._http.get(`${environment.url_academico}/private/listarTodosLosDistributivos`).subscribe((response: ResponseGenerico<asignacionDocenteInterface>) => {
      resolve(response);
    }, reject);
  })
}










}
