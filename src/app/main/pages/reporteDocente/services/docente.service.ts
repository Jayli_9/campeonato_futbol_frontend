import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { environment } from 'environments/environment';
import { ResponseGenerico } from '../interfaces/response-generico';
import { docenteInterface } from "../interfaces/docentehispana";

@Injectable({
  providedIn: 'root'
})
export class DocenteService {

  private readonly URL_REST = environment.url_docente;

constructor(private _http: HttpClient) { }

buscarDocentePorCod(cod): Promise<ResponseGenerico<docenteInterface>>{
  return new Promise((resolve, reject) => {
    this._http.get(`${environment.url_docente}/private/obtenerDocenteHispanaPorCodigo/`+cod).subscribe((response: ResponseGenerico<docenteInterface>) => {
      resolve(response);
    }, reject);
  })
}

//obtener docente hispano por Amie
getListaDocentesHispanoPorCodAmie(codAmie): Promise<ResponseGenerico<docenteInterface>> {
  return new Promise((resolve, reject) => {
    this._http.get(`${environment.url_docente}/private/listarDocentesHispanaPorCodigoAmie/${codAmie}`).subscribe((response: ResponseGenerico<docenteInterface>) => {
      resolve(response);
    }, reject);
  })
}


}
