import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { environment } from 'environments/environment';
import { ResponseGenerico } from '../interfaces/response-generico';
import { cursoInterface } from "../interfaces/curso";
import { ofertaIntitucionInterface } from "../interfaces/ofertaInstitucion";
import { cursoParaleloInterface } from "../interfaces/cursoParalelo";
import { paraleloInterface } from "../interfaces/paralelo";

@Injectable({
  providedIn: 'root'
})
export class OfertaService {

  private readonly URL_REST = environment.url_oferta;

constructor(private _http: HttpClient) { }

buscarCursoPorCodigo(cod): Promise<ResponseGenerico<cursoInterface>>{
  return new Promise((resolve, reject) => {
    this._http.get(`${environment.url_oferta}/private/buscarCursoPorCodigo/`+cod).subscribe((response: ResponseGenerico<cursoInterface>) => {
      resolve(response);
    }, reject);
  })
}


buscarIntitucionCodigo(cod): Promise<ResponseGenerico<ofertaIntitucionInterface>>{
  return new Promise((resolve, reject) => {
    this._http.get(`${environment.url_oferta}/private/listaCursoPorInsestCodigo/`+cod).subscribe((response: ResponseGenerico<ofertaIntitucionInterface>) => {
      resolve(response);
    }, reject);
  })
}


buscarCursoParaleloPorCodigo(cod): Promise<ResponseGenerico<cursoParaleloInterface>>{
  return new Promise((resolve, reject) => {
    this._http.get(`${environment.url_oferta}/private/buscarCursoParaleloPorCodigo/`+cod).subscribe((response: ResponseGenerico<cursoParaleloInterface>) => {
      resolve(response);
    }, reject);
  })
}

buscarParaleloPorCodigo(cod):  Promise<ResponseGenerico<paraleloInterface>>{
  return new Promise((resolve, reject) => {
    this._http.get(`${environment.url_oferta}/private/buscarParaleloPorCodigo/`+cod).subscribe((response: ResponseGenerico<paraleloInterface>) => {
      resolve(response);
    }, reject);
  })
}


}
