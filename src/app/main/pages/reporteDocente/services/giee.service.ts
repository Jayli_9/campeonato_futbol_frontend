import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { environment } from 'environments/environment';
import { ResponseGenerico } from '../interfaces/response-generico';
import { institucionEstablecimientoInterface } from "../interfaces/institucionEstablecimiento";
import { ofertaIntitucionInterface } from "../interfaces/ofertaInstitucion";
import { permisosNivelInterface } from "../interfaces/permisosNivel";
import { institucionInterface } from "../interfaces/institucionAmie";


@Injectable({
  providedIn: 'root'
})
export class GieeService {

  private readonly URL_REST = environment.url_institucion;

constructor(private _http: HttpClient) { }

buscarInstitucionEstablecimientoPorAmie(cod): Promise<ResponseGenerico<institucionEstablecimientoInterface>>{
  return new Promise((resolve, reject) => {
    this._http.get(`${environment.url_institucion}/private/listarEstablecimientosPorAmie/`+cod).subscribe((response: ResponseGenerico<institucionEstablecimientoInterface>) => {
      resolve(response);
    }, reject);
  })
}

buscarOfertaPorInstitucion(cod):  Promise<ResponseGenerico<ofertaIntitucionInterface>>{
  return new Promise((resolve, reject) => {
this._http.get(`${environment.url_institucion}/private/listaCursoPorInsestCodigo/` + cod).subscribe((response: ResponseGenerico<ofertaIntitucionInterface>) => {
   resolve(response);
 }, reject);
})
}

buscarPermisosNivelPorInstitucion(cod): Promise<ResponseGenerico<permisosNivelInterface>>{
  return new Promise((resolve, reject) => {
this._http.get(`${environment.url_institucion}/private/listarNivelPermisoPorCodigoInstitucionEstadoYVigente/` + cod).subscribe((response: ResponseGenerico<permisosNivelInterface>) => {
   resolve(response);
 }, reject);
})
}

buscarInsitucionPorAmie(cod): Promise<ResponseGenerico<institucionInterface>>{
  return new Promise((resolve, reject) => {
this._http.get(`${environment.url_institucion}/private/buscarInstitucionPorAmie/` + cod).subscribe((response: ResponseGenerico<institucionInterface>) => {
   resolve(response);
 }, reject);
})
}

}
