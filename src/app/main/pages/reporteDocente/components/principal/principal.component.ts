import { Component, OnInit } from '@angular/core';
import { User } from 'app/auth/models';
import { CompleterService, CompleterData } from 'ng2-completer';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { CatalogoService } from "../../services/catalogo.service";
import { AcademicoService } from "../../services/academico.service";
import { OfertaService } from "../../services/oferta.service";
import { DocenteService } from "../../services/docente.service";
import { GieeService } from "../../services/giee.service";
import { MatAutocompleteModule } from '@angular/material/autocomplete';
import { docenteInterface } from "../../interfaces/docentehispana";
import { reporteListaInterface } from "../../interfaces/listaReporte";
import { map, startWith } from 'rxjs/operators';
import Swal from 'sweetalert2';


import pdfMake from 'pdfmake/build/pdfmake';
import pdfFonts from 'pdfmake/build/vfs_fonts';
import { Observable } from 'rxjs';
import { UntypedFormControl } from '@angular/forms';
pdfMake.vfs = pdfFonts.pdfMake.vfs;



@Component({
  selector: 'app-principal',
  templateUrl: './principal.component.html',
  styleUrls: ['./principal.component.scss']
})
export class PrincipalComponent implements OnInit {

  //datos de establecimiento
  codigoEstablecimiento = 0;
  listaEstablecimiento;
  listaIntitucion = [];

  //sacar jornadas con ofertas
  objetoJornadas = [];
  objetoLimpioJornadas = [];



  //docente
  nombreDocente;
  listaCodigoNiveles = [];
  listaCodigoNivelesFiltros = [];


  //datos de institucion
  dataInstitucion;
  idInstitucion;

  contentHeader: object;
  datosInstitucion = [];

  codigoInstitucion;


  nombreJornada;
  nombreNivel;

  currentUser: User;

  //datos de jornada
  codigoJornada;
  listaJornada = [];


  //datos de niveles
  codigoNivel = 0;
  listaNiveles = [];
  listaNivelesLimpios = [];


  //listado de la tabla
  listaTabla = [];
  listaSistematico = [];
  listaCompleta = [];
  listaFiltradoSistematico = [];

  page = 1;
  pageSize = 4;
  datoEvent;


  //datos del pdf
  nombreInstitucion;

  // Configura el servicio y datos para el autocompletar
  dataService: CompleterData;


  myControl = new UntypedFormControl;
  opcionesDocentes: Observable<reporteListaInterface[]>;
  displayFn(docente: reporteListaInterface): string {
    return docente && docente.nombreDocente ? docente.nombreDocente : '';
  }

  constructor(
    private readonly catalogoService: CatalogoService,
    private readonly academicoService: AcademicoService,
    private readonly ofertaService: OfertaService,
    private readonly docenteService: DocenteService,
    private readonly gieeService: GieeService,
    public readonly completerService: CompleterService
    ) { 
      this.currentUser = JSON.parse(localStorage.getItem("currentUser"))
    }


    async sacarInstitucion(cod){
      this.gieeService.buscarInsitucionPorAmie(cod).then(data => {
        let lista = data.objeto;
        this.idInstitucion = lista.insCodigo;
      })
    }

  ngOnInit(){

    this.datosInstitucion.push(this.currentUser.sede);

    this.nombreInstitucion = this.datosInstitucion[0].nombre;

    this.sacarInstitucion(this.datosInstitucion[0].nemonico);

    this.gieeService.buscarInstitucionEstablecimientoPorAmie(this.datosInstitucion[0].nemonico).then(data => {
      let datosInstitucion = data.listado;
      this.dataInstitucion = datosInstitucion[0].insestCodigo;
      let insTipoEstablecimiento = datosInstitucion[0].insTipoEstablecimiento;

      let datos = {
        nombreEstablecimiento: insTipoEstablecimiento.tipEstDescripcion,
        idCodigo: datosInstitucion[0].insestCodigo
      }

      this.listaIntitucion.push(datos);

    })

    setTimeout(() => {

      this.ofertaService.buscarIntitucionCodigo(this.dataInstitucion).then(data => {
        let listadoOferta = data.listado;

        for(let i = 0; i < listadoOferta.length; i++){

          this.objetoJornadas.push(listadoOferta[i].jorCodigo);
        }
      })


      this.gieeService.buscarPermisosNivelPorInstitucion(this.idInstitucion).then(data => {
        let listaPermiso = data.listado;
        for(let i = 0; i < listaPermiso.length; i++){
          this.listaCodigoNiveles.push(listaPermiso[i].nivCodigo);
        }
      })

    }, 500);



    setTimeout(() => {

      let objetoSinRepetidos = {};

      let nivelesRepetidos = {};


      this.objetoJornadas.forEach(function (elemento) {
        objetoSinRepetidos[elemento] = elemento;
      });
      
      this.objetoLimpioJornadas = Object.values(objetoSinRepetidos);

      this.objetoLimpioJornadas.forEach(element => {
        this.catalogoService.buscarJornadaPorCodigo(element).then(data => {
          let datosJornada = data.objeto;

          this.listaJornada.push(datosJornada);
        })
      } )


      this.academicoService.listarTodosLosDistributivos().then(data => {
        let datos = data.listado;
  
        for(let i = 0; i < datos.length; i++){
  
          if(datos[i].insestCodigo == this.dataInstitucion){

            if(datos[i].acaMallaAsignatura.acaAsignatura != null){


              let numero = datos[i].acaMallaAsignatura.acaAsignatura.graCodigo;

            this.catalogoService.buscarGradosPorCodigo(numero).then(grados => {

              let lista = grados.objeto;

              this.ofertaService.buscarCursoPorCodigo(datos[i].curCodigo).then(oferta => {

                let datosOferta = oferta.objeto;

                this.ofertaService.buscarCursoParaleloPorCodigo(datos[i].curparCodigo).then(cursoParalelo => {
                  let cursoParaleloDato = cursoParalelo.objeto;
                
                  this.ofertaService.buscarParaleloPorCodigo(cursoParaleloDato.parCodigo).then(curso => {
                    let cursoData = curso.objeto;

                    this.docenteService.buscarDocentePorCod(datos[i].docCodigo).then(docente => {
                      let docenteObjeto = docente.objeto;
    
                      let opcionalesTabla ={
                        asiCodigo: datos[i].acaMallaAsignatura.acaAsignatura.asiCodigo,
                        asiDescripcion: datos[i].acaMallaAsignatura.acaAsignatura.asiDescripcion,
                        graCodigo: lista.graCodigo,
                        codigoParalelo: cursoData.parCodigo,
                        paralelo: cursoData.parDescripcion,
                        graDescripcion: lista.graDescripcion,
                        graNemonico: lista.graNemonico,
                        nivCodigo: lista.nivCodigo,
                        curCodigo: datosOferta.curCodigo,
                        jorCodigo: datosOferta.jorCodigo,
                        codPersona: docenteObjeto.codPersona,
                        cedula: docenteObjeto.numIdentificacion,
                        nombreDocente: docenteObjeto.nomPersona,
                        regimenLaboral: docenteObjeto.nomRegimenLaboral,
                        nom_deno_puesto: docenteObjeto.nom_deno_puesto
                      }
    
                      this.listaTabla.push(opcionalesTabla);
                    })

                  })
                })
              })
            }) 
            }else{
              let numero = datos[i].acaMallaAsignatura.acaAsignaturaPrepa.graCodigo;

            this.catalogoService.buscarGradosPorCodigo(numero).then(grados => {

              let lista = grados.objeto;

              this.ofertaService.buscarCursoPorCodigo(datos[i].curCodigo).then(oferta => {

                let datosOferta = oferta.objeto;

                this.ofertaService.buscarCursoParaleloPorCodigo(datos[i].curparCodigo).then(cursoParalelo => {
                  let cursoParaleloData = cursoParalelo.objeto;

                  this.ofertaService.buscarParaleloPorCodigo(cursoParaleloData.parCodigo).then(curso => {
                    let cursoData = curso.objeto;

                    this.docenteService.buscarDocentePorCod(datos[i].docCodigo).then(docente => {
                      let docenteObjeto = docente.objeto;
    
                      let opcionalesTabla ={
                        asiCodigo: datos[i].acaMallaAsignatura.acaAsignaturaPrepa.apreCodigo,
                        asiDescripcion: datos[i].acaMallaAsignatura.acaAsignaturaPrepa.apreDescripcion,
                        graCodigo: lista.graCodigo,
                        graDescripcion: lista.graDescripcion,
                        graNemonico: lista.graNemonico,
                        codigoParalelo: cursoData.parCodigo,
                        paralelo: cursoData.parDescripcion,
                        nivCodigo: lista.nivCodigo,
                        curCodigo: datosOferta.curCodigo,
                        jorCodigo: datosOferta.jorCodigo,
                        codPersona: docenteObjeto.codPersona,
                        cedula: docenteObjeto.numIdentificacion,
                        nombreDocente: docenteObjeto.nomPersona,
                        regimenLaboral: docenteObjeto.nomRegimenLaboral,
                        nom_deno_puesto: docenteObjeto.nom_deno_puesto
                      }
    
                      this.listaTabla.push(opcionalesTabla);
                    })


                  })

                })

                
              })
            }) 
            } 
          }
        }
      })
    }, 1500);

    


    this.contentHeader = {
      headerTitle: 'Reporte Docentes',
      actionButton: false,
      breadcrumb: {
          type: '',
          links: [
              {
                name: 'Inicio',
                isLink: true,
                link: '/'
              },
              {
                name: 'reporteDocentes',
                isLink: false
              }
          ]
      }
    };


    this.catalogoService.listarNiveles().then(data => {
      this.listaNiveles = data.listado;
    })

  }


  generarReporte(){

    const bodyData = this.listaSistematico.map((empleado, index) => [index + 1, empleado.cedula, empleado.nombreDocente, empleado.graDescripcion, empleado.paralelo ,empleado.asiDescripcion, empleado.regimenLaboral, empleado.nom_deno_puesto]);


    const pdfDefinition: any = {
      content: [
        { text: this.nombreInstitucion, style: 'datoTituloGeneral'},
        { text: 'Reporte Docente Asignatura', style: 'datoTituloGeneral'},
        { text: 'Nivel: '+this.nombreNivel, style: 'datoSubtitulo'},
        { text: 'Jornada: '+this.nombreJornada, style: 'datoSubtitulo'},
        {
          table: {
            body: [
              ['#', 'Cedula', 'Nombre', 'Grado', 'Paralelo', 'Materia', 'Regimen Laboral', 'Puesto'],
              ...bodyData
            ],
          },
          style: 'datosTabla'
        },
      ],
      styles: {
        datosTabla: {
          fontSize: 6,
          margin: [0, 0, 0, 0], // Margen inferior para separar la tabla de otros elementos
          fillColor: '#F2F2F2', // Color de fondo de la tabla
        },
        datoTitulo: {
          fontSize: 10
        },
        datoTituloGeneral: {
          fontSize: 16,
          bold: true,
          alignment: 'center',
          margin: [0, 0, 0, 10], // Puedes ajustar el margen según tus preferencias
        },
        datoSubtitulo: {
          fontSize: 10,
          bold: true,
          alignment: 'left', // Alineado a la izquierda
          margin: [0, 0, 0, 10], // Ajusta el margen según tus preferencias
        }
      },
    }
        const pdf = pdfMake.createPdf(pdfDefinition);
        pdf.open();
        location.reload();
  }


  
  filtrarDocentes(){
    // Filtra los docentes según el nombre ingresado
    
    if (this.nombreDocente.trim() === ''){

      this.listaSistematico = [];

      for(let i = 0; i < this.listaTabla.length; i++){

        if(this.listaTabla[i].nivCodigo == this.codigoNivel){
  
          if(this.listaTabla[i].jorCodigo == this.codigoJornada){
  
            this.listaSistematico.push(this.listaTabla[i]);
            this.listaCompleta.push(this.listaTabla[i]);
  
          }
  
        }
  
      }

    }else{

      this.listaSistematico = this.listaSistematico.filter(docente =>{
        const nombreDocenteLowerCase = this.nombreDocente.toLowerCase().trim();
        const nombreCompletoLowerCase = docente.nombreDocente.toLowerCase().trim();

        let indexNombreDocente = 0;

        // Verifica que las letras estén en el orden correcto y consecutivo
        for (let i = 0; i < nombreCompletoLowerCase.length; i++) {
          if (nombreCompletoLowerCase[i] === nombreDocenteLowerCase[indexNombreDocente]) {
            indexNombreDocente++;
          }
        }

        // Si se ha recorrido toda la cadena de búsqueda, se incluye el docente
        return indexNombreDocente === nombreDocenteLowerCase.length;
      }
      );

    }    
  }

  seleccionJornada(event, item){

    for(let i = 0; i < item.length; i++){

      if(event == item[i].jorCodigo){

        this.nombreJornada = item[i].jorNombre;

      }

    }

    this.codigoJornada = event;

    this.listaSistematico = [];

    this.listaNivelesLimpios = [];

    this.codigoNivel = 0;

    let result = this.listaCodigoNiveles.filter((item,index)=>{
      return this.listaCodigoNiveles.indexOf(item) === index;
    })

    result.forEach(element => {
      this.catalogoService.buscarNivelesPorCodigo(element).then(data => {
        let datosNiveles = data.objeto;

        this.listaNivelesLimpios.push(datosNiveles);

      })

    })


    setTimeout(() => {
      const listaOrdenada = [...this.listaNivelesLimpios];
      // Ordena la lista en función del código del nivel de menor a mayor.
      listaOrdenada.sort((a, b) => (a.nivCodigo || 0) - (b.nivCodigo || 0));
      // Asigna la lista ordenada de nuevo a la lista original.
      this.listaNivelesLimpios = listaOrdenada;
    }, 500);

  }


  seleccionNiveles(event, item){

    this.listaSistematico = [];

    for(let i = 0; i < item.length; i++){

      if(event == item[i].nivCodigo){

        this.nombreNivel = item[i].nivDescripcion;

      }


    }


    this.codigoNivel = event;

    for(let i = 0; i < this.listaTabla.length; i++){

      if(this.listaTabla[i].nivCodigo == this.codigoNivel){

        if(this.listaTabla[i].jorCodigo == this.codigoJornada){

          this.listaSistematico.push(this.listaTabla[i]);

        }

      }

    }

    this.opcionesDocentes = this.myControl.valueChanges.pipe(
      startWith(''),
      map(value => {
        const name = typeof value === 'string' ? value : value?.name;
        const filteredList = name ? this._filter(name as string) : this.listaSistematico.slice();
        
        // Filtrar nombres únicos
        const uniqueNames = this.getUniqueNames(filteredList);
    
        return uniqueNames;
      }),
    );

  }

  private _filter(name: string): reporteListaInterface[] {
    const filterValue = name.toLowerCase();
    return this.listaTabla.filter(option => option.nombreDocente.toLowerCase().includes(filterValue));
  }

  private getUniqueNames(list: reporteListaInterface[]): reporteListaInterface[] {
    const uniqueNamesMap = new Map<string, reporteListaInterface>();
    list.forEach(option => {
      const lowerCaseName = option.nombreDocente.toLowerCase();
      if (!uniqueNamesMap.has(lowerCaseName)) {
        uniqueNamesMap.set(lowerCaseName, option);
      }
    });
    return Array.from(uniqueNamesMap.values());
  }


  seleccionEstablecimiento(event){

  }

  
  onInputChange(event: Event) {

    this.listaSistematico = [];

    this.opcionesDocentes;
    
    for(let i = 0; i < this.listaTabla.length; i++){

      if(this.listaTabla[i].nivCodigo == this.codigoNivel){

        if(this.listaTabla[i].jorCodigo == this.codigoJornada){

          this.listaSistematico.push(this.listaTabla[i]);

        }

      }

    }

    this.opcionesDocentes = this.myControl.valueChanges.pipe(
      startWith(''),
      map(value => {
        const name = typeof value === 'string' ? value : value?.name;
        const filteredList = name ? this._filter(name as string) : this.listaSistematico.slice();
        
        // Filtrar nombres únicos
        const uniqueNames = this.getUniqueNames(filteredList);
    
        return uniqueNames;
      }),
    );


  }

  reiniciarDatosTabla(docente: any): void {

    if(!this.nombreDocente){
      this.listaSistematico = [...this.listaCompleta];
    }else{
      this.listaSistematico = this.listaSistematico.filter(docente => docente.nombreDocente === this.nombreDocente.nombreDocente);
    }

}

}
