import { CommonModule } from "@angular/common";
import { NgModule } from "@angular/core";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { RouterModule } from "@angular/router";
import { CoreCommonModule } from "@core/common.module";
import { ContentHeaderModule } from "app/layout/components/content-header/content-header.module";
import { MaterialModule } from "app/main/shared/material/material.module";
import { PrincipalComponent } from "../components/principal/principal.component";
import { RUTA_REPORTE_DOCENTE } from "../routes/reporte-docente-routing.module";
import { Ng2CompleterModule } from 'ng2-completer';

@NgModule({
    declarations: [PrincipalComponent],
    imports: [
      CommonModule,
      RouterModule.forChild(RUTA_REPORTE_DOCENTE),
      FormsModule,
      ReactiveFormsModule,
      MaterialModule,
      ContentHeaderModule,
      CoreCommonModule,
      Ng2CompleterModule
    ]
  })

  export class ModeloReporteDocentes {

  }

