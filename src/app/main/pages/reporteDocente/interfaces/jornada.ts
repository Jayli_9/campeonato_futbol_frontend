export interface jornadaInterface{
       jorCodigo: number;
       jorNombre: string;
       jorEstado: number;
       jorFechaCreacion: Date;
       jorNemonico: string;
       jorEsBilingue: number;
}