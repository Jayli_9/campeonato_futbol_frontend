export interface reporteListaInterface{
                    asiCodigo: number;
                    asiDescripcion: string;
                    graCodigo: number;
                    graDescripcion: string;
                    graNemonico: string;
                    nivCodigo: number;
                    curCodigo: number;
                    jorCodigo: number;
                    codPersona: number;
                    cedula: number;
                    nombreDocente: string;
                    regimenLaboral: string;
                    nom_deno_puesto: string;
}