export interface ofertaIntitucionInterface{
    curCodigo: number;
    aulbloCodigo: number;
    curAforo: number;
    curCuposDis: number;
    curEstado: number;
    curFechaCreacion: Date
    espCodigo: number;
    graCodigo: number;
    insestCodigo: number;
    jorCodigo: number;
    modCodigo: number;
    reanleCodigo: number;
    sereduCodigo: number;
    tipeduCodigo: number;
    uniCodigo: number;
    arecieCodigo:  number;
}