export interface permisosNivelInterface{
       nipeinCodigo: number;
       insCodigo: number;
       sereduCodigo: number;
       nivCodigo: number;
       figprobtCodigo: number;
       espbtCodigo: number;
       figprobcCodigo: number;
       arecieCodigo: number;
       nisediCodigo: number;
       jorCodigo: number;
       nipeinEstado: number;
       nipeinVigente: string;
}