export interface institucionEducativaInterface{
    codInstitucionEducativa: number;
    codAmie: string;
    nomInstitucionEducativa: string;
    nomEstadoIe: string;
    nomSostenimiento: string;
    descripcionRegimen: string;
    descripcionJurisdiccion: string;
    nomZona: string;
    nomDistrito: string;
    codAdDistrito: string;
    nomCircuito: string;
    codAdCircuito: string;
    codAdZona: string;
    nomProvincia: string;
    nomCanton: string;
    codDpaParroquia: string;
    nomParroquia: string;
    zonaParroquia: string;
    coordenadaX: string;
    coordenadaY: string;
    fechaCreacion: Date;
}