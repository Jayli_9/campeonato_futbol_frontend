import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';

//importacion material
import { MaterialModule } from "app/main/pages/material/material.module";
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatAutocompleteModule } from '@angular/material/autocomplete';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { MatIconModule } from '@angular/material/icon';
import { MatDividerModule } from '@angular/material/divider';
import { MatSelectModule } from '@angular/material/select';
import { MatFormFieldModule } from '@angular/material/form-field';
import { RUTA_CALIFICACION_ORDINARIO } from '../rutas/calificacioinOrdinarioa.routing';
import { CalificacionPrincipalComponent } from '../componentes/calificacion-principal/calificacion-principal.component';
import { NotaPrincipalComponent } from '../componentes/calificacion-principal/nota-principal/nota-principal.component';
import { PrimeraNotaComponent } from '../componentes/calificacion-principal/primera-nota/primera-nota.component';
import { MatTabsModule } from '@angular/material/tabs';
import { SegundaNotaComponent } from '../componentes/calificacion-principal/segunda-nota/segunda-nota.component';
import { MatPaginatorModule } from '@angular/material/paginator';
import { InicialComponent } from '../componentes/calificacion-principal/inicial/inicial.component';
import {PagesModule} from '../../pages.module';
import {NotaOvpComponent} from '../componentes/calificacion-principal/nota-ovp/nota-ovp.component';
import {NgxSpinnerModule} from 'ngx-spinner';
import { NgxPaginationModule } from 'ngx-pagination';
import { CaliHabilidadesComponent } from "../componentes/calificacion-principal/cali-habilidades/cali-habilidades.component";
import { ListaEstudiantesHabilidadesComponent } from "../componentes/calificacion-principal/lista-estudiantes-habilidades/lista-estudiantes-habilidades.component";


@NgModule({
  declarations: [
    CalificacionPrincipalComponent,
    PrimeraNotaComponent,
    NotaPrincipalComponent,
    SegundaNotaComponent,
    InicialComponent,
    NotaOvpComponent,
    CaliHabilidadesComponent,
    ListaEstudiantesHabilidadesComponent
  ],
    imports: [
        CommonModule,
        RouterModule.forChild(RUTA_CALIFICACION_ORDINARIO),
        MaterialModule,
        FormsModule,
        ReactiveFormsModule,
        MatAutocompleteModule,
        MatCheckboxModule,
        NgxPaginationModule,
        MatIconModule,
        MatDividerModule,
        MatSelectModule,
        MatFormFieldModule,
        MatTabsModule,
        MatPaginatorModule,
        NgxSpinnerModule,
        MatPaginatorModule
        // ContentHeaderModule
    ],
  schemas: [
    CUSTOM_ELEMENTS_SCHEMA
  ]

})
export class calificacionOrdinariaModule { }
