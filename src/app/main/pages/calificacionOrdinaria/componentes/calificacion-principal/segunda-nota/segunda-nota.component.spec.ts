import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SegundaNotaComponent } from './segunda-nota.component';

describe('SegundaNotaComponent', () => {
  let component: SegundaNotaComponent;
  let fixture: ComponentFixture<SegundaNotaComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SegundaNotaComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SegundaNotaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
