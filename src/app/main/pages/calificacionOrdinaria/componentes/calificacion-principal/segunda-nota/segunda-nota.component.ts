import { Component, EventEmitter, Input, OnInit, Output, ViewChild } from '@angular/core';
import { MatPaginator, MatPaginatorIntl } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { Router } from '@angular/router';
import { MensajesService } from 'app/main/pages/asignacionDocentesOrdinaria/servicios/mensajes.service';
import { IacaCalificacionDTO } from '../../../interfaces/IacaCalificacionDTO';
import { CalificacionOrdinariaService } from '../../../servicios/calificacionOrdinaria.service';
import { orderBy } from 'lodash-es';
import { NgxSpinnerService } from 'ngx-spinner';
import { IacaRangoEscala } from '../../../interfaces/IacaRangoEscala';
import { IcatRangoNota } from '../../../interfaces/IcatRangoNota';
import { OperacionesService } from '../../../servicios/operacionesService.service';
import { constanteCalificacion } from '../../../constantes/constanteCalificacion';
import { IacaRangoSupletorio } from '../../../interfaces/IacaRangoSupletorio';
import { AreaConocimiento } from '../../../../asignatura/interfaces/area-conocimiento';
@Component({
  selector: 'app-segunda-nota',
  templateUrl: './segunda-nota.component.html',
  styleUrls: ['./segunda-nota.component.scss']
})
export class SegundaNotaComponent implements OnInit {
  //parametros tabla
  columnasEncabezado: string[] = ['identificacion', 'estudiante'];
  dataSource: MatTableDataSource<any>;
  //variables
  public tipoInput: string = "text";
  public rangoNota: IcatRangoNota;
  public areaConocimientoSeleccionado: AreaConocimiento;


  //variables paginación y orden
  @ViewChild(MatPaginator) paginator!: MatPaginator;
  @ViewChild(MatSort) sort!: MatSort;
  //Inputs
  @Input() nivelCodigoChild;
  @Input() cursoSeleccionadoChild;
  @Input() modgenCodigoChild;
  @Input() listaCalificacionChild: any[];
  @Input() listaEstudiantesNotasChild: any[];
  @Input() acaTipoValoracionChild;
  @Input() rangoNotaChild;
  @Input() asiNemonicoChild;
  @Input() activoModeloChild:number;

  //outputs
  @Output() porcentajeNotas: EventEmitter<number>

  //lista
  public listaEstudiantesNotas: IacaCalificacionDTO[] = [];
  public listaRangoEscala: IacaRangoEscala[] = [];
  public listaRangoSupletorio: IacaRangoSupletorio[] = [];
  public constantes;
  public graCodigo: number;

  constructor(private router: Router,
    private matPaginatorIntl: MatPaginatorIntl,
    private calificacionOrdinariaService: CalificacionOrdinariaService,
    private operacionService: OperacionesService,
    private mensajeService: MensajesService,
    private spinnerService: NgxSpinnerService,) {
    this.porcentajeNotas = new EventEmitter<number>()
  }

  async ngOnInit(): Promise<void> {
    // this.valorMinimoSupletorio = constanteCalificacion.valorminimoSupletorio;
    this.constantes = constanteCalificacion;
    try {
      this.spinnerService.show();
      if (this.acaTipoValoracionChild.tivaDescripcion === "CUANTITATIVA") {
        this.tipoInput = "number";
        await this.consultarValidacionCuantitativa(this.cursoSeleccionadoChild.reanleCodigo);
      } else {
        this.graCodigo = this.cursoSeleccionadoChild.ofeCurso.graCodigo;
        await this.consultarRangoSupletorios(this.cursoSeleccionadoChild.ofeCurso.reanleCodigo, this.acaTipoValoracionChild.tivaCodigo);
        await this.consultarValidacionCualitativa(this.asiNemonicoChild, this.cursoSeleccionadoChild.ofeCurso.graCodigo);
      }
      await this.listarEstudaintesMatriculados(this.listaEstudiantesNotasChild);
      this.agregarColunasTabla(this.listaCalificacionChild);
      this.areaConocimientoSeleccionado = this.cursoSeleccionadoChild.acaMallaAsignatura.acaAsignatura.acaAreaConocimiento;

    } finally {
      // setTimeout(() => {
      this.spinnerService.hide();
      // }, 2000);
    }

  }

  agregarColunasTabla(listaCalificacion) {
    for (const iterator of listaCalificacion) {
      this.columnasEncabezado.push(iterator.calDescripcion)
    }
    this.columnasEncabezado.push("acciones")
  }
  async listarEstudaintesMatriculados(listaEstudiantes): Promise<void> {
    this.spinnerService.show();
    try {


      for (const objeto of listaEstudiantes) {
        await this.calificacionOrdinariaService.getNotaAsignaturaObjetoConsulta(objeto.maasCodigo, objeto.matCodigo)
          .then(async data => {
            if (data.objeto != null) {
              const listaCombinada = await this.combinarListas(objeto.listaModeloGeneral, JSON.parse(data.objeto.noasCalNot));
              objeto.listaModeloGeneral = listaCombinada;
              objeto.noasCodigo = data.objeto.noasCodigo;

            }
          });


      }

      this.listaEstudiantesNotas = orderBy(listaEstudiantes, ['estudiante.estNombres'], ['asc']);
      this.dataSource = new MatTableDataSource(this.listaEstudiantesNotas);
      this.dataSource.paginator = this.paginator;
      this.dataSource.sort = this.sort;
      this.dataSource.paginator = this.paginator;
      this.matPaginatorIntl.itemsPerPageLabel = 'Elementos por página:';
      this.matPaginatorIntl.nextPageLabel = 'Siguiente:';
      this.matPaginatorIntl.previousPageLabel = 'Anterior:';
    } finally {
      setTimeout(() => {
        this.spinnerService.hide();
      }, 1000);
    }
  }
  validarNota(elemento) {

    if (this.tipoInput === "number") {
      if (elemento.noasCalNot > this.rangoNota.ranotMaximo || elemento.noasCalNot < this.rangoNota.ranotMinimo) {
        elemento.noasCalNot = elemento.notaPresentacion;
        this.mensajeService.mensajeError("Error", "Nota no se encuentra en el dentro del rango entre " + this.rangoNota.ranotMinimo + " - " + this.rangoNota.ranotMaximo)
      } else {
        elemento.notaPresentacion = elemento.noasCalNot;
      }
    }
    if (this.tipoInput === "text") {
      let existe = this.listaRangoEscala.find(obj => obj.resEscala === elemento.noasCalNot?.toUpperCase())

      if (!existe) {
        let texto = "Con la Siguiente Escala: ";
        this.listaRangoEscala.forEach((objeto, indice) => {
          texto += `${objeto.resEscala}`;
          if (indice < this.listaRangoEscala.length - 1) {
            texto += ", ";
          }
        });
        this.mensajeService.mensajeError("Error", "Solo permite la calificación " + texto)
        elemento.noasCalNot = elemento.notaPresentacion;
      } else {
        elemento.notaPresentacion = elemento.noasCalNot;
      }
    }

  }

  async guardarNotaIndividual(elemento) {
    let notaPonderada = 0;
    await this.calificacionOrdinariaService.getNotaAsignaturaObjetoConsulta(elemento.maasCodigo, elemento.matCodigo)
      .then(async data => {
        if (data.objeto != null) {
          elemento.noasCodigo = data.objeto.noasCodigo; // Asignar valor si la condición se cumple
        }
      });
    try {
      if (this.nivelCodigoChild === 4 && this.graCodigo !== 6) {
        this.guardarPromedioDirecto(elemento)
      } else {
        this.spinnerService.show();
        // let suma = 0;
        // Recorrer listaModeloGeneral de elemento        
        for (const objetoModeloGeneral of elemento.listaModeloGeneral) {
          if (objetoModeloGeneral.modgenCodigo === this.modgenCodigoChild) {
            // Realizar operaciones en listaCalificacion de objetoModeloGeneral            
            for (const calificacion of objetoModeloGeneral.listaCalificacion) {
              //Se transforma en mayuscula la nota
              calificacion.noasCalNot = calificacion.noasCalNot?.toUpperCase();
              //aqui capturo la nota en letras y verificao su equivalencia;
              let equivalenteCuantitativo = this.listaRangoEscala.find(obj => obj.resEscala === calificacion.noasCalNot);

              if (equivalenteCuantitativo) {
                // suma += Number(equivalenteCuantitativo.vmax);
                var notaPorcentaje = (equivalenteCuantitativo.vmax * calificacion.calPorcentaje) / 100;

              }
              // if (notaPorcentaje < 0.5) {
              //   notaPorcentaje = Math.ceil(notaPorcentaje);
              // }
              // let equivalenteCualitativo = this.listaRangoEscala.find(obj => obj.vmax === Math.round(notaPorcentaje));
              calificacion.porcentaje = notaPorcentaje;

              // calificacion.porcentaje = equivalenteCualitativo.resEscala;
              calificacion.notaPresentacion = calificacion.noasCalNot;
            }
          }
        }


        ///Actualizar promerdio de aceurdo con el servicio 
        let sumaTotalTrimestres = 0;
        let sumaTotal = 0;
        let valorNuevofinal: IacaRangoSupletorio;
        for (const nota of elemento.listaModeloGeneral) {
          if (nota.modgenNivel === constanteCalificacion.modgenNivelParciales) {
            // Verificar si listaCalificacion está definido
            if (nota.listaCalificacion && Array.isArray(nota.listaCalificacion)) {
              for (const calificacion of nota.listaCalificacion) {
                // Sumar solo si 'noasCalNot' es un número válido
                if (calificacion.porcentaje !== null && calificacion.porcentaje) {
                  //cambiar equivalencia a numeros 
                  // let equivalenteCuantitativo = this.listaRangoEscala.find(obj => obj.resEscala === calificacion.porcentaje?.toUpperCase());
                  sumaTotalTrimestres += Number(calificacion.porcentaje);
                  // sumaTotalTrimestres += Number(equivalenteCuantitativo.vmax);

                }
              }
            } else {
              console.error("La lista de calificaciones no está definida o no es un array.");
            }
          }

          if (nota.modgenNemonico === constanteCalificacion.nemonicoNotaTrimestral) {
            if (sumaTotalTrimestres != 0) {
              // let equivalenteCuanlitativo = this.listaRangoEscala.find(obj => obj.vmax === sumaTotalTrimestres);
              // nota.listaCalificacion[0].noasCalNot = (equivalenteCuanlitativo.resEscala);
              nota.listaCalificacion[0].porcentaje = sumaTotalTrimestres;
            }

          }

          ///Suma con la evaluación subnivel
          if (nota.modgenNivel === constanteCalificacion.modgenNivelPromedios) {
            // Verificar si listaCalificacion está definido
            if (nota.listaCalificacion && Array.isArray(nota.listaCalificacion)) {
              for (const calificacion of nota.listaCalificacion) {
                // Sumar solo si 'noasCalNot' es un número válido
                if (calificacion.porcentaje !== null && calificacion.porcentaje) {
                  //cambiar equivalencia a numeros 
                  // let equivalenteCuantitativo = this.listaRangoEscala.find(obj => obj.resEscala === calificacion.porcentaje?.toUpperCase());
                  sumaTotal += Number(calificacion.porcentaje);
                }
              }
            } else {
              console.error("La lista de calificaciones no está definida o no es un array.");
            }

          }
          if (nota.modgenNemonico === constanteCalificacion.nemonicoNotaFinal) {
            let equivalenteCuanlitativo = this.listaRangoEscala.find(obj => obj.vmax === Math.round(sumaTotal));
            nota.listaCalificacion[0].noasCalNot = (equivalenteCuanlitativo.resEscala);
          }
          if (nota.modgenNemonico === constanteCalificacion.nemonicoSupletorio) {
            // nota.listaCalificacion[0].notaPromedio = Number((sumaTotal).toFixed(2));
            let equivalenteCuanlitativo = this.listaRangoEscala.find(obj => obj.vmax === Math.round(sumaTotal));
            nota.listaCalificacion[0].notaPromedio = (equivalenteCuanlitativo.resEscala);
            //cambiar nota final 
            if (nota.listaCalificacion[0].noasCalNot) {
              valorNuevofinal = this.buscarEquivalenciaSupletorio(this.listaRangoSupletorio, nota.listaCalificacion[0].noasCalNot)
              if (valorNuevofinal) {
                nota.listaCalificacion[0].notaPromedio = valorNuevofinal.rasuPromedioFinal;
              }
            } else {
              nota.listaCalificacion[0].notaPromedio = (equivalenteCuanlitativo.resEscala);
            }

          }
          if (valorNuevofinal) {
            for (const nota of elemento.listaModeloGeneral) {
              if (nota.modgenNemonico === constanteCalificacion.nemonicoNotaFinal) {
                nota.listaCalificacion[0].noasCalNot = valorNuevofinal.rasuPromedioFinal;
                nota.listaCalificacion[0].notaPromedio = valorNuevofinal.rasuPromedioFinal;
              }
            }
          }
          if (nota.modgenNemonico === constanteCalificacion.nemonicoNotaPonderada) {
            let coeficiente: number;
            let notaFinalEscala = buscarNotafinal(elemento.listaModeloGeneral)
            let notaEquivalenteEscala = buscarValorEscala(this.listaRangoEscala, notaFinalEscala)
            let arcoCodigo = this.areaConocimientoSeleccionado.arcoCodigo;
            try {
              const respuesta = await this.calificacionOrdinariaService.getObtenerCoeficientePorAreaConocimientoPorReanleCodigo(arcoCodigo, elemento.reanleCodigo).toPromise();
              if (respuesta.objeto) {
                coeficiente = respuesta.objeto.coeValor;
                notaPonderada = (notaEquivalenteEscala * coeficiente) / 10;
                nota.listaCalificacion[0].noasCalNot = Math.trunc(notaPonderada * 100) / 100;
              }
            } catch (error) {
              console.error('Coeficiente erroneo')
            }
            this.calificacionOrdinariaService.getObtenerCoeficientePorAreaConocimientoPorReanleCodigo(arcoCodigo, elemento.reanleCodigo)
              .subscribe(respuesta => {
                if (respuesta.objeto) {
                  coeficiente = respuesta.objeto.coeValor
                  notaPonderada = (notaEquivalenteEscala * coeficiente) / 10;
                  nota.listaCalificacion[0].noasCalNot = (Math.trunc(notaPonderada * 100) / 100);
                }
              })

          }

        }

        //guardar nota json 
        let valorIngreso = JSON.parse(JSON.stringify(elemento));
        // Recorrer listaModeloGeneral de valorIngreso (copia independiente con copia profunda)
        for (const objetoModeloGeneral of valorIngreso.listaModeloGeneral) {
          // Eliminar la propiedad notaPresentacion de listaCalificacion de objetoModeloGeneral
          for (const calificacion of objetoModeloGeneral.listaCalificacion) {
            // calificacion.noasCalNot = calificacion.noasCalNot?.toUpperCase()
            delete calificacion.notaPresentacion;
          }
        }
        // let sumaCualitativo = this.listaRangoEscala.find(obj => obj.vmax === suma);
        // if (sumaCualitativo) {
        //   elemento.promedioNota = sumaCualitativo.resEscala;
        // }
        //poner el promedio transformado 
        let notaAsignaturaDTO = {
          acaMallaAsignatura: elemento.maasCodigo,
          matCodigo: elemento.matCodigo,
          noasCalNot: JSON.stringify(valorIngreso.listaModeloGeneral),
          noasCodigo: elemento.noasCodigo,
          noasEstado: 1
        }

        this.calificacionOrdinariaService.getGuardarNotaAsignaturaObjetoConsulta(notaAsignaturaDTO).then(data => {
          elemento.noasCodigo = data.objeto.noasCodigo;
          this.mensajeService.mensajeCorrecto("", "Notas de la asignatura grabada!")
        })
      }


    } finally {
      this.spinnerService.hide();
    }
    // this.operacionService.listaEstudiantesPrimertrimestre$.next(this.listaEstudiantesNotas);
  }
  calcularPorcentaje(listaCalcular) {
    let totalDeCalificacionesIngresar = (listaCalcular.length) * (this.listaCalificacionChild.length);
    // Utiliza reduce para contar objetos con atributoEspecifico en null en la lista anidada
    const cantidadDeNullEnListaAnidada = listaCalcular.reduce((count, objeto) => {
      const listaAnidada = objeto.listaCalificacion || [];
      const nullsEnListaAnidada = listaAnidada.filter(item => item.noasCalNot === null);
      return count + nullsEnListaAnidada.length;
    }, 0);
    let porcentajeTotal;
    let porcentajeFaltante = (cantidadDeNullEnListaAnidada * 100) / (totalDeCalificacionesIngresar);
    this.porcentajeNotas.emit(porcentajeFaltante)
  }
  async guardarListaNotas(listaEstudiantesNotas) {
    for (const objeto of listaEstudiantesNotas) {
      await this.guardarNotaIndividual(objeto);
    }
    // this.operacionService.listaEstudiantesPrimertrimestre$.next(this.listaEstudiantesNotas);
  }
  consultarValidacionCualitativa(resAgrupacion, graCodigo) {
    this.calificacionOrdinariaService.getConsultarRangoEscalasObjetoConsulta(resAgrupacion, graCodigo).then(data => {
      this.listaRangoEscala = data.listado;
    })
  }
  consultarValidacionCuantitativa(reanleCodigo) {
    this.calificacionOrdinariaService.getRangoNotaPorReanleCodigo(reanleCodigo).then(data => {
      this.rangoNota = data?.listado[0]
    })
  }
  combinarListas(listaModeloGeneral: any[], listaNotasConsulta: any[]): any[] {
    return listaModeloGeneral.map((elementoListaModeloGeneral) => {
      const elementoListaNotasConsulta = listaNotasConsulta.find((item) => item.modgenCodigo === elementoListaModeloGeneral.modgenCodigo);

      if (elementoListaNotasConsulta) {
        // Combinar las listas de calificación si hay coincidencia en modgenCodigo
        const listaCalificacionCombinada = elementoListaModeloGeneral.listaCalificacion.map((objetoModelo) => {
          const objetoConsulta = elementoListaNotasConsulta.listaCalificacion.find((calificacion) => calificacion.calCodigo === objetoModelo.calCodigo);
          if (objetoConsulta) {
            // Asignar el valor de noasCalNot si existe en la segunda lista
            objetoModelo.noasCalNot = objetoConsulta.noasCalNot;
            objetoModelo.notaPresentacion = objetoConsulta.noasCalNot;
            // Si hay coincidencia y hay un valor en porcentaje en la segunda lista, combinar los campos
            if (objetoConsulta.porcentaje !== undefined) {
              objetoModelo.porcentaje = objetoConsulta.porcentaje;
            }
          }

          return objetoModelo;
        });

        return { ...elementoListaModeloGeneral, listaCalificacion: listaCalificacionCombinada };
      } else {
        // Si no hay coincidencia en modgenCodigo, mantener el elemento de la primera lista sin cambios
        return elementoListaModeloGeneral;
      }
    });
  }
  guardarPromedioDirecto(elemento) {
    this.spinnerService.show();
    // Recorrer listaModeloGeneral de elemento
    for (const objetoModeloGeneral of elemento.listaModeloGeneral) {
      if (objetoModeloGeneral.modgenCodigo === this.modgenCodigoChild) {
        // Realizar operaciones en listaCalificacion de objetoModeloGeneral
        for (const calificacion of objetoModeloGeneral.listaCalificacion) {
          calificacion.notaPresentacion = calificacion.noasCalNot;
        }
      }
    }

    ///Actualizar promerdio de aceurdo con el servicio 
    let sumaTotalTrimestres = 0;
    let contarPromedios = 0;
    for (const nota of elemento.listaModeloGeneral) {
      if (nota.modgenNivel === constanteCalificacion.modgenNivelParciales) {
        // Verificar si listaCalificacion está definido
        contarPromedios++;
        if (nota.listaCalificacion && Array.isArray(nota.listaCalificacion)) {
          for (const calificacion of nota.listaCalificacion) {
            // Sumar solo si 'noasCalNot' es un número válido
            //cambiar equivalencia a numeros 
            if (calificacion.noasCalNot) {
              let equivalenteCuantitativo = this.listaRangoEscala.find(obj => obj.resEscala === calificacion.noasCalNot?.toUpperCase());
              sumaTotalTrimestres += Number(equivalenteCuantitativo.vmax);
            }
          }
        } else {
          console.error("La lista de calificaciones no está definida o no es un array.");
        }

      }


    }
    sumaTotalTrimestres = (sumaTotalTrimestres / contarPromedios);
    for (const nota of elemento.listaModeloGeneral) {
      if (nota.modgenNemonico === constanteCalificacion.nemonicoNotaTrimestral) {
        if (sumaTotalTrimestres < 0.5) {
          sumaTotalTrimestres = Math.ceil(sumaTotalTrimestres);
        } else {
          sumaTotalTrimestres = Math.round(sumaTotalTrimestres);
        }
        if (sumaTotalTrimestres != 0) {
          let equivalenteCuanlitativo = this.listaRangoEscala.find(obj => obj.vmax === sumaTotalTrimestres);
          nota.listaCalificacion[0].noasCalNot = (equivalenteCuanlitativo.resEscala);
        }
      }
    }
    let valorIngreso = JSON.parse(JSON.stringify(elemento));

    // Recorrer listaModeloGeneral de valorIngreso (copia independiente con copia profunda)
    for (const objetoModeloGeneral of valorIngreso.listaModeloGeneral) {
      // Eliminar la propiedad notaPresentacion de listaCalificacion de objetoModeloGeneral
      for (const calificacion of objetoModeloGeneral.listaCalificacion) {
        calificacion.noasCalNot = calificacion.noasCalNot?.toUpperCase()
        delete calificacion.notaPresentacion;
      }
    }
    let notaAsignaturaDTO = {
      acaMallaAsignatura: elemento.maasCodigo,
      matCodigo: elemento.matCodigo,
      // noasCalNot: `{"notas": ${JSON.stringify(valorIngreso.listaModeloGeneral)}}`,
      noasCalNot: JSON.stringify(valorIngreso.listaModeloGeneral),
      noasCodigo: elemento.noasCodigo,
      noasEstado: 1
    }
    this.calificacionOrdinariaService.getGuardarNotaAsignaturaObjetoConsulta(notaAsignaturaDTO).then(data => {
      elemento.noasCodigo = data.objeto.noasCodigo;
      this.mensajeService.mensajeCorrecto("", "Notas de la asignatura grabada!")
    })
  }
  consultarRangoSupletorios(reanleCodigo, tipoAsignaturaCodigo) {
    this.calificacionOrdinariaService.getRangoSupletorioPorTivaCodigo(tipoAsignaturaCodigo).then(data => {
      this.listaRangoSupletorio = data.listado;
      this.listaRangoSupletorio = this.listaRangoSupletorio.filter(obj => obj.reanleCodigo === reanleCodigo)
    })
  }
  buscarEquivalenciaSupletorio(listaRangoSupletorio: IacaRangoSupletorio[], letraBuscar) {
    const rangoEncontrado = listaRangoSupletorio.find((rango: IacaRangoSupletorio) => {
      return letraBuscar.toUpperCase() === rango.rasuMax
    });
    return rangoEncontrado;
  }
}
function buscarNotafinal(listaModeloEvaGeneral: any): number {
  let modelo = listaModeloEvaGeneral.find(modeloEvaluacion => modeloEvaluacion.modgenNemonico === constanteCalificacion.nemonicoNotaFinal)
  if (modelo == null) return null;
  else {
    return modelo.listaCalificacion[0].noasCalNot
  }
}
function buscarValorEscala(listaRangoEsc: IacaRangoEscala[], resEscalaBuscar): number {
  let rangoEscala = listaRangoEsc.find(rangoEscala => rangoEscala.resEscala === resEscalaBuscar);
  if (rangoEscala === null) return null;
  else {
    return rangoEscala.vmax
  }
}
