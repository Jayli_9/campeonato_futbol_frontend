import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PrimeraNotaComponent } from './primera-nota.component';

describe('PrimeraNotaComponent', () => {
  let component: PrimeraNotaComponent;
  let fixture: ComponentFixture<PrimeraNotaComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PrimeraNotaComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PrimeraNotaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
