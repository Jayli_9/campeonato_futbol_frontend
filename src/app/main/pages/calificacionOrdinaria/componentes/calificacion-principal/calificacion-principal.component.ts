import { Component, OnInit, ViewChild } from '@angular/core';
import { CatalogoService } from 'app/main/pages/asignacionDocentes/servicios/catalogo.service';
import { GieeService } from 'app/main/pages/asignacionDocentes/servicios/giee.service';
import { NgxSpinnerService } from 'ngx-spinner';
import { CalificacionOrdinariaService } from '../../servicios/calificacionOrdinaria.service';
import { IacaDistributivo } from '../../interfaces/IacaDistributivo';
import { DocenteOrdinariaService } from 'app/main/pages/asignacionDocentesOrdinaria/servicios/docenteOrdinaria.service';
import { IinsInstitucion } from 'app/main/pages/asignacionDocentesOrdinaria/interfaces/IinsInstitucion';
import { IcatDocenteHispana } from 'app/main/pages/asignacionDocentesOrdinaria/interfaces/IcatDocenteHispana';
import { MensajesService } from 'app/main/pages/asignacionDocentesOrdinaria/servicios/mensajes.service';
import { orderBy } from 'lodash-es';
import { MatTableDataSource } from '@angular/material/table';
import { MatPaginator, MatPaginatorIntl } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { IacaModevaGen } from '../../interfaces/IacaModevaGen';
import { IacaParametrizacionCalpre } from '../../interfaces/IacaParametrizacionCalpre';
import dayjs from "dayjs"
declare var $: any; // Declaración de jQuery
@Component({
  selector: 'app-calificacion-principal',
  templateUrl: './calificacion-principal.component.html',
  styleUrls: ['./calificacion-principal.component.scss']
})
export class CalificacionPrincipalComponent implements OnInit {
  //paginacion
  numeros: number[] = []
  currentPage: number = 0;
  pageSize: number = 10;
  totalPages: number;
  //variables
  public listaInstitucion: any;
  public institucionObjeto: IinsInstitucion;
  public informacionSede: any;
  public nombreInstitucion:string;
  public informacionInstitucion;
  public objetoRegimen:any;
  public insestCodigo:number;
  public codInstitucion:number;
  public codRegimen:number;
  public informacionEstablecimiento: any;
  public codAmie:string;
  public regimen:number;
  public listaAnio:any[];
  public reanleCodigo:number;
  public anioElectivoCodigo:number;
  public anioElectivoDescripcion:string;
  //listas
  public listaDistributivo: IacaDistributivo[] = [];
  public listaDistributivoConsulta: IacaDistributivo[] = [];
  public listaParametrizacionCalPre: IacaParametrizacionCalpre[] = []
  public listaCruceValidacion: any[] = []
  public listaModEvaGen: IacaModevaGen[] = []
  //objetos
  public docenteObjeto: IcatDocenteHispana;
  public activarNotaPrincipal = false;
  public cursoSeleccionado;
  dataSource: MatTableDataSource<any>;
  columnasEncabezado: string[] = ['descripcion', 'grado', 'paralelo', 'jornada', 'acciones'];
  //variables paginación y orden
  @ViewChild(MatPaginator) paginator!: MatPaginator;
  @ViewChild(MatSort) sort!: MatSort;

  constructor(private spinnerService: NgxSpinnerService,
    private GieeService: GieeService,
    private CatalogoService: CatalogoService,
    private asignacionOrdinariaService: DocenteOrdinariaService,
    private calificacionOrdinariaService: CalificacionOrdinariaService,
    private mensajeService: MensajesService, private matPaginatorIntl: MatPaginatorIntl,
  ) { }

  async ngOnInit(): Promise<void> {
    try {
      this.spinnerService.show();
      /////////////////////////////////////
      //asignacion codigo amie
      this.listaInstitucion = JSON.parse(localStorage.getItem('currentUser'));
      this.informacionSede = this.listaInstitucion.sede;
      this.nombreInstitucion = this.informacionSede.descripcion;
      this.codAmie = this.informacionSede.nemonico;

      await this.consultarInstitucionPorAmie(this.codAmie);
      //buscar la institucion y regimen
      await this.buscarRegiment(this.codAmie);
      //buscar Establecimiento de la institucion
      await this.obtenerEstablecimiento(this.codAmie);
      ///
      await this.consultarDocentePorcedula(this.listaInstitucion.identificacion);

      await this.obtenerListadoDistributivo(this.docenteObjeto?.codPersona, this.reanleCodigo, this.insestCodigo, this.currentPage, this.pageSize);
      // setTimeout(() => {

      // }, 10000);

      ////////////SACAR MODELO GENERAL PARA FILTRAR POR GRADO DESPUES  ////////////////////
      await this.consultarModevaGenPorReanleCodigo(this.reanleCodigo)

      ////////////////////////////////
    } finally {

      this.spinnerService.hide();
    }

  }

  async consultarParametrizacionCalPre(reanleCodigo, sereduCodigo) {
    await this.calificacionOrdinariaService
      .getParametrizacionCalPrePorReanleCodigoYSereduCodigo(reanleCodigo, sereduCodigo)
      .then(
        (data) => {
          this.listaParametrizacionCalPre = data.listado
        },
        (error) => {
          this.spinnerService.hide()
        }
      )
    await this.realizarCruceValidacion(this.listaParametrizacionCalPre)
  }
  realizarCruceValidacion(listaParametrizacionCalPre) {
    this.listaCruceValidacion = this.listaModEvaGen
      .flatMap((obj1) => {
        const objetosCoincidentes = listaParametrizacionCalPre.filter(
          (obj2) => obj2.acaModevaGen.modgenCodigo === obj1.modgenCodigo
        )
        if (objetosCoincidentes.length > 0) {
          // Combinar obj1 con todos los objetos coincidentes de listaOferta
          return objetosCoincidentes.map((obj2) => ({ ...obj1, ...obj2 }))
        }
        // Si no se encontraron coincidencias, devuelve null
        return null
      })
      .filter((elemento) => elemento !== null)
    var fechaActual = new Date(dayjs(new Date()).format("YYYY-MM-DD"))
    for (const objeto of this.listaCruceValidacion) {
      var diaInicio = new Date(dayjs(objeto.papreInicio).format("YYYY-MM-DD"))
      var diaFin = new Date(dayjs(objeto.papreFin).format("YYYY-MM-DD"))
      if (fechaActual >= diaInicio && fechaActual <= diaFin) {
        objeto.activarNota = false
      } else {
        objeto.activarNota = true
      }
    }
  }
  async consultarModevaGenPorReanleCodigo(reanleCodigo) {
    await this.calificacionOrdinariaService
      .getObtenerModelosEvaluacionGeneralPorReanlecYEstado(reanleCodigo)
      .then(
        async (data) => {
          this.listaModEvaGen = data.listado
          for (const objeto of this.listaModEvaGen) {
            await this.calificacionOrdinariaService
              .getCalificacionPorModgenCodigo(objeto.modgenCodigo)
              .then((data) => { objeto.listaCalificacion = data.listado })
          }
        },
        (error) => {
          this.spinnerService.hide()
        }
      )
    const [{ listaCalificacion: [{ sereduCodigo }] }] = this.listaModEvaGen;
    await this.consultarParametrizacionCalPre(this.reanleCodigo, sereduCodigo);

  }
  //buscar los datos de la institucion y regimen
  async buscarRegiment(codAmie) {
    //buscar la institucion
    await this.GieeService.getDatosInstitucion(codAmie).then(async data => {
      this.informacionInstitucion = data.objeto;
      this.codInstitucion = this.informacionInstitucion.insCodigo;
      this.codRegimen = this.informacionInstitucion.regimenes[0].regCodigo;
      await this.CatalogoService.getRegimen(this.codRegimen).then(data => {
        this.objetoRegimen = data.objeto;
        //exportar el objeto
        this.regimen = this.objetoRegimen?.regDescripcion
      }, error => {
        this.mensajeService.mensajeError("", "Error al consultar catálogo");
        this.spinnerService.hide();
      })
      //listar regimenAñolectivo 
      await this.CatalogoService.getAnioElectivo(this.codRegimen).then(data => {
        this.listaAnio = data.listado;
        for (let dato of this.listaAnio) {
          if (dato.reanleTipo === "A") {
            this.reanleCodigo = dato.reanleCodigo;
            this.anioElectivoCodigo = dato.anilecCodigo;
            this.CatalogoService.getAnioLectivoPorAnilecCodigo(this.anioElectivoCodigo).then(data => {
              let anioLectivo = data.objeto
              this.anioElectivoDescripcion = anioLectivo.anilecAnioInicio + " - " + anioLectivo.anilecAnioFin
            })
          }
        }
      })
    }, error => {
      this.mensajeService.mensajeError("", "Error al consultar servicios Giee");
      this.spinnerService.hide();
    })
  }

  //sacar los establecimientos de la institucion
  async obtenerEstablecimiento(cod) {
    await this.GieeService.getEstablecimientoPorAmie(cod).then(data => {
      this.informacionEstablecimiento = data.listado;
      //Buscar el establecimientto matriz
      let matriz = this.informacionEstablecimiento.find(obj => obj.insTipoEstablecimiento.tipEstCodigo === 1)
      this.insestCodigo = matriz.insestCodigo

    }, error => {
      this.mensajeService.mensajeError("", "Error al consultar servicios Giee");
      this.spinnerService.hide();
    })
  }

  async obtenerListadoDistributivo(docCodigo, reanleCodigo, insestCodigo, page, pageSize) {
    try {
      this.spinnerService.show();
      const dataDistributivo = await this.calificacionOrdinariaService.getDistributivoPorDtoPaginado(docCodigo, reanleCodigo, insestCodigo, pageSize, page);
      this.listaDistributivoConsulta = dataDistributivo.listado;
      this.totalPages = dataDistributivo.totalPaginas;
      this.numeros = Array.from({ length: this.totalPages }, (_, i) => i + 1);
      for (const objeto of this.listaDistributivoConsulta) {
        const dataCursoParalelo = await this.calificacionOrdinariaService.getCursoParaleloPorCurparCodigo(objeto.curparCodigo);
        objeto.ofeCursoParalelo = dataCursoParalelo.objeto;
        const dataParalelo = await this.calificacionOrdinariaService.getParaleloPorParCodigo(objeto.ofeCursoParalelo.parCodigo);
        objeto.ofeCursoParalelo.nombreParalelo = dataParalelo.objeto.parDescripcion;

        const dataCurso = await this.calificacionOrdinariaService.getCursoPorCurCodigo(objeto.curCodigo);
        objeto.ofeCurso = dataCurso.objeto;

        const dataGrado = await this.calificacionOrdinariaService.getGradoPorGraCodigo(objeto.ofeCurso.graCodigo);
        objeto.ofeCurso.nombreGrado = dataGrado.objeto.graDescripcion;
        objeto.ofeCurso.nivCodigo = dataGrado.objeto.nivCodigo;

        const dataJornada = await this.asignacionOrdinariaService.getJornadaPorJorCodigo(objeto.ofeCurso.jorCodigo);
        objeto.ofeCurso.nombreJornada = dataJornada.objeto.jorNombre;
      }

      this.listaDistributivo = this.listaDistributivoConsulta.sort((a, b) => a.ofeCurso.graCodigo - b.ofeCurso.graCodigo);
      this.dataSource = new MatTableDataSource(this.listaDistributivo);
      this.dataSource.paginator = this.paginator;
      this.dataSource.sort = this.sort;
      this.dataSource.paginator = this.paginator;
      this.matPaginatorIntl.itemsPerPageLabel = 'Elementos por página:';
      this.matPaginatorIntl.nextPageLabel = 'Siguiente:';
      this.matPaginatorIntl.previousPageLabel = 'Anterior:';
    } catch (error) {
      this.mensajeService.mensajeError("", `Error al consultar servicios: ${error}`);
      this.spinnerService.hide();
    } finally {
      this.spinnerService.hide();
    }
  }
  async consultarInstitucionPorAmie(codAmie) {
    try {
      const data = await this.calificacionOrdinariaService.getIntitucionPorAmie(codAmie);
      this.institucionObjeto = data.objeto;
    } catch (error) {
      this.mensajeService.mensajeError("", `Error al consultar servicios instituciones app: ${error}`);
      this.spinnerService.hide();
    }
  }

  async consultarDocentePorcedula(identificacion) {
    await this.asignacionOrdinariaService.getDocentePorIdentificacion(identificacion).then(data => {
      this.docenteObjeto = data.objeto;
    }, error => {
      console.log("error");
      this.spinnerService.hide();
    });
  }

  ingresarNotas(elemento) {
    this.activarNotaPrincipal = true;
    this.cursoSeleccionado = elemento;
    
  }

  actualizarNotasPrincipal(event) {
    this.activarNotaPrincipal = event;
    this.ngOnInit()
  }
  async onPageChange(page: number): Promise<void> {
    this.currentPage = page;
    await this.obtenerListadoDistributivo(this.docenteObjeto?.codPersona, this.reanleCodigo, this.insestCodigo, this.currentPage, this.pageSize);
  }
}
