import { Component, Input, OnInit } from '@angular/core';
import { User } from 'app/auth/models';
import { CalificacionHabilidadService } from "../../../servicios/calificacionHabilidad.service";
import Swal from 'sweetalert2/dist/sweetalert2.js'

@Component({
  selector: 'app-cali-habilidades',
  templateUrl: './cali-habilidades.component.html',
  styleUrls: ['./cali-habilidades.component.scss']
})
export class CaliHabilidadesComponent implements OnInit {

   //Inputs
   @Input() cursoSeleccionadoChild;
   @Input() estudianteSeleccionado;
   @Input() codigoMatricula;
   @Input() codigoAnioElectivo;


  contentHeader: object;

  //datos institucion
  currentUser: User;
  datosInstitucion = [];
  nombreInstitucion;
  nemonico;
  amie;

  //datos de guardado

  //datos de docente y paralelo
  identificacion;
  curso;
  nombresDocente;
  nombreAsignatura;
  codigoPersona;

  //selector de asignatura
  listaCatHabilidades;
  selectorAsignatura;


  //tabla de habInst y habilidad
  habilidadesInstitucion = [];
  habLifDatosLimpios = [];
  habDescipcion = [];
  graCodigo;
  codigoEstablemiento;
  nombreEstudiante;


  //lista de criterios
  objetoCriterios = [];
  datosIdentificador = [];


  //datos de seleccion cod docente
  codigoDocente;
  codigoCurso;


  //mogeHib
  mogeHibPrueba;



  //datos para sacar malla del docente;
  datosSeleccionDocente = [];
  nombreDeAsignacion;


  //datos de nemonico
  datosPrincipales = [];
  datosTotales = [];
  gradoNemonico;
  listaSeleccionTabla;
  codigoNivel;

  curCodigo;
  curparCodigo;

  reanleCodigo;
  codigoMalla;


  //ponderacion;
  listaPonderacion;

  //cantidadCriterios
  cantidadCriterios;


  //codigos para la insercion

  codigoCursoInsercion;
  codigoParalelo;
  datoHib;

  listaEvaluacion;
  listaEvaluacionFiltrada;

  listaHabIst;

  ponderacionSeleccionada;

  codigoTrimestral;


  datosGuardar = [];

  listaHabilidades = [];

  listaHabilidadesSociales = [];


  //variable de objetos
  datosNotas= [];
  datosNotaComportamiento= [];

  constructor(
    private readonly calificacionHabilidadService: CalificacionHabilidadService
  ) {
    this.currentUser = JSON.parse(localStorage.getItem("currentUser"))
   }


   async sacarDocentesPorCodigo(cod) {
    try {
      const data = await this.calificacionHabilidadService.buscarCodigoDatosDocente(cod);
      let listadoDocente = data.objeto;
      if (listadoDocente != null) {
        this.codigoDocente = listadoDocente.codPersona;
        this.nombresDocente = listadoDocente.nomPersona;
      }
    } catch (error) {
      console.error('Error en sacarDocentesPorCodigo:', error);
    }
  }


  async sacarHabilidadPorCodigo(cod){
    try{
      const data = await this.calificacionHabilidadService.buscarHabilidadesPorCodigo(cod);
      let objeto = data.objeto;

      this.listaHabilidades.push(objeto);
    } catch (error){
      console.error('Error en ', error)
    }
  }


  async sacarHabilidadesInstitucionPorCodigo(cod){
    try{
      this.listaHabilidades = [];
      const data = await this.calificacionHabilidadService.buscarHabilidadesinstitucionPorEstablecimiento(cod);
      let listado = data.listado;
      let codigosUnicos = new Set();
      listado.forEach(element => {
        if(element.hinEstado === 1 && !codigosUnicos.has(element.habCodigo)){
          codigosUnicos.add(element.habCodigo);
        }
        
      });

      this.listaSeleccionTabla = listado
      await codigosUnicos.forEach(element => {
        this.sacarHabilidadPorCodigo(element);
      });

    } catch (error){
      console.error('Error en ', error)
    }

  }

  async sacarEstableciminetoPorAmie(cod){
    try{
      const data = await this.calificacionHabilidadService.buscarEstablecimientoPorCodigo(cod);
      let listaEstablecimiento = data.listado;
      listaEstablecimiento.forEach(element => {
        this.codigoEstablemiento = element.insestCodigo;
      });

     await this.sacarHabilidadesInstitucionPorCodigo(this.codigoEstablemiento)

    } catch (error) {
      console.error('Error en sacarDocentesPorCodigo:', error)
    }
  }

   
   async sacarGrados(cod){
    this.calificacionHabilidadService.buscarGradoPorCodigo(cod).then(data => {
      let listaGrado = data.objeto;
      this.gradoNemonico=listaGrado.graNemonico;
    })
   }

  async ngOnInit() {
    this.codigoMalla = this.cursoSeleccionadoChild.acaMallaAsignatura.maasCodigo;

    this.curCodigo = this.cursoSeleccionadoChild.curCodigo;
    this.curparCodigo = this.cursoSeleccionadoChild.curparCodigo;

    this.nombreEstudiante = this.estudianteSeleccionado.estudiante.estNombres;

    this.codigoNivel = this.cursoSeleccionadoChild.ofeCurso.nivCodigo;

    this.graCodigo = this.cursoSeleccionadoChild.ofeCurso.graCodigo;

    this.curso = this.cursoSeleccionadoChild.ofeCursoParalelo.nombreParalelo;

    this.datosInstitucion.push(this.currentUser.sede);

    this.identificacion = this.currentUser.identificacion;

    this.amie = this.datosInstitucion[0].nemonico;

    this.nombreInstitucion = this.datosInstitucion[0].descripcion;
    this.nemonico = this.datosInstitucion[0].nemonico;

    this.sacarDocentesPorCodigo(this.identificacion);

    this.sacarGrados(this.graCodigo);

    this.sacarEstableciminetoPorAmie(this.amie);

    //lista de modulo evaluacion
    let listaEvaluacionOber = await this.calificacionHabilidadService.listarModelaEvaluacion();

    let listaEvaluacionData = listaEvaluacionOber.listado;

    this.listaEvaluacion = listaEvaluacionData.filter(item => item.reanleCodigo === this.codigoAnioElectivo && (item.modgenDescripcion === "TRIMESTRE 1" ||
    item.modgenDescripcion === "TRIMESTRE 2" ||
    item.modgenDescripcion === "TRIMESTRE 3"))

    let lista = await this.calificacionHabilidadService.listaPonderacion();

    let listaData = lista.listado;

    this.listaPonderacion = listaData.filter(item => item.reanleCodigo === this.codigoAnioElectivo && item.reanleCodigo === this.codigoAnioElectivo);

  }

  
  async seleccionAsignatura(event){

    this.datosTotales = null;

    this.codigoTrimestral = parseInt(event);

   const obje =  await this.calificacionHabilidadService.listarCriteriorHabilidades();

   let lista = obje.listado;

   await Promise.all(lista.map(async (element) => {

    this.listaHabilidades.forEach(habilidad => {
      if(element.habCodigo === habilidad.habCodigo){
        element.codigoIdentificador = habilidad.cthCodigo;
        element.codigoHabilidad = habilidad.habCodigo;
      element.nombreHabilidad = habilidad.habDescipcion;
      element.codigoTrimestre = this.codigoTrimestral;
      element.seleccion = false;
      }
    });
  }));


   this.datosTotales = lista.filter(item => item.nivCodigo === this.codigoNivel && item.reanleCodigo === this.codigoAnioElectivo && item.nombreHabilidad !== undefined);

   await this.sacarDatosYaInsertados(this.datosTotales);

  }

  async sacarDatosYaInsertados(listaTotales){

    this.cantidadCriterios = listaTotales.length;

    this.calificacionHabilidadService.buscarCalificacionHabilidadPorMatricula(this.codigoMatricula).then(data => {
      try {
        const listadoFiltrado = data.listado.filter(item => item.modgenCodigo === this.codigoTrimestral);
        
        listaTotales = listaTotales.map(listaTotal => {
          const elementoCorrespondiente = listadoFiltrado.find(element => element.criCodigo === listaTotal.criCodigo);
          if (elementoCorrespondiente) {
            listaTotal.ponderacionSeleccionada = elementoCorrespondiente.ponCodigo;
            listaTotal.codigoCalificacion = elementoCorrespondiente.ccriCodigo;
            listaTotal.hinCodigo = elementoCorrespondiente.hinCodigo;
          }
          return listaTotal;
        });
      } catch (error) {
        console.error("Error al procesar los datos:", error);
      }
    })
  }


  async seleccionHabilidad(paral, ponderacion){

    let datalit = this.listaSeleccionTabla.filter(item => item.insestCodigo === this.codigoEstablemiento && item.habCodigo === paral.codigoHabilidad && item.reanleCodigo === this.codigoAnioElectivo);

    let hinCodigo;

    datalit.forEach(element => {
      hinCodigo = element.hinCodigo;
      paral.hinCodigo = hinCodigo;
    });

    try{

      if(paral.ponderacionSeleccionada !== undefined){

        if(paral.ponderacionSeleccionada !== parseInt(ponderacion)){
          paral.ponderacionSeleccionada = parseInt(ponderacion)
        }
  
      }else{
        paral.ponderacionSeleccionada = parseInt(ponderacion);
      }
    } catch{

    }

  }

  guardarCalificacionHabilidades(){

    this.generarDatos(this.datosTotales);

  }


  async generarNotaComportamiento (mat, acumulador){
    let nivelModeloGeneral = 3;
    try{
      this.datosNotaComportamiento=[];
      const data = await this.calificacionHabilidadService.buscarNotaComportamientoPorMatricula(mat);
      let notaComportamiento = data.objeto;

      if(notaComportamiento === null){

        let datosListaCalificacion = {
          calCodigo: this.codigoTrimestral,
          noasCalNot: acumulador
     }

     let datosCalificaciones ={
       listaCalificacion: [datosListaCalificacion],
       modgenCodigo: this.codigoTrimestral,
       Nivel: this.codigoNivel,
       modgenNivel: nivelModeloGeneral
     }

     let datosAGuardar ={
       comCodigo: 1,
       matCodigo: mat,
       nocoCalNota: "["+JSON.stringify(datosCalificaciones)+"]",
       nocoEstado: 1
     }

     this.calificacionHabilidadService.guardarNotaComportamiento(datosAGuardar).subscribe({
      next: (response) => {

      },
      error: (error) => {
        Swal.fire({
          icon: "error",
          title: "Advertencia",
          text: "Los datos no se pudieron guardar"
        });
      }
    })
      }else{
        if (typeof notaComportamiento.nocoCalNota === 'string'){
          //editar nota comportamiento
          let resultado = true;
          JSON.parse(notaComportamiento.nocoCalNota).forEach(cod => {
            try{
              if(cod.modgenCodigo === this.codigoTrimestral){
                resultado = false;
                  cod.listaCalificacion.forEach(calif => {
                    calif.noasCalNot = acumulador;
                  });
              }
              this.datosNotaComportamiento.push(cod);
            } catch{

            }
          });

          if(resultado){
            let datosListaCalificacion = {
              calCodigo: this.codigoTrimestral,
              noasCalNot: acumulador
         }
    
         let datosCalificaciones ={
           listaCalificacion: [datosListaCalificacion],
           modgenCodigo: this.codigoTrimestral,
           Nivel: this.codigoNivel,
           modgenNivel: nivelModeloGeneral
         }

         this.datosNotaComportamiento.push(datosCalificaciones);
          }

          notaComportamiento.nocoCalNota =JSON.stringify(this.datosNotaComportamiento);

          let datosAGuardar ={
            nocoCodigo: notaComportamiento.nocoCodigo,
            comCodigo: notaComportamiento.comCodigo,
            matCodigo: notaComportamiento.matCodigo,
            nocoCalNota: notaComportamiento.nocoCalNota,
            nocoEstado: notaComportamiento.nocoEstado
          }

          this.calificacionHabilidadService.editarNotaComportamiento(datosAGuardar).subscribe({
            next: (response) => {
      
            },
            error: (error) => {
              Swal.fire({
                icon: "error",
                title: "Advertencia",
                text: "Los datos no se pudieron guardar"
              });
            }
          })

        }
      }


    }catch (error) {
      // Manejar el error, por ejemplo, lanzando una excepción o devolviendo un valor predeterminado
      console.error("Error en generarNotaComportamiento:", error);
      return null;
    }


  }


  async generarCalificacionAsignatura(mat, nota) {
    let nivelModeloGeneral = 3;
    try {
      this.datosNotas= [];
      const data = await this.calificacionHabilidadService.buscarNotaAsignaturaPorMatricula(mat);
      let notaAsignatura = data.listado;
  
      if (notaAsignatura.length === 0) {

        let datosListaCalificacion = {
             calCodigo: this.codigoTrimestral,
             calMaximo: 10,
             calMinimo: 1,
             calPorcentaje: 30,
             noasCalNot: nota
        }

        let datosCalificaciones ={
          listaCalificacion: [datosListaCalificacion],
          modgenCodigo: this.codigoTrimestral,
          modgenNivel: nivelModeloGeneral
        }

        let datosAGuardar ={
           acaMallaAsignatura: this.codigoMalla,
           matCodigo: mat,
          noasCalNot: "["+JSON.stringify(datosCalificaciones)+"]",
          noasEstado: 1
        }

        this.calificacionHabilidadService.guardarNotaAsignatura(datosAGuardar).subscribe({
          next: (response) => {

          },
          error: (error) => {
            Swal.fire({
              icon: "error",
              title: "Advertencia",
              text: "Los datos no se pudieron guardar"
            });
          }
        })


      } else {
        notaAsignatura.forEach(element => {
          if (typeof element.noasCalNot === 'string') {
            let resultado = true;
            JSON.parse(element.noasCalNot).forEach(cod => {
              try{
                if (cod.modgenCodigo === this.codigoTrimestral) {
                  resultado = false;
                  cod.listaCalificacion.forEach(calif => {
                    calif.noasCalNot = nota;
                  });
                } 

                this.datosNotas.push(cod);
              } catch{

              }
              
              
            });
            
            if(resultado){
              let datosListaCalificacion = {
                calCodigo: this.codigoTrimestral,
                calMaximo: 10,
                calMinimo: 1,
                calPorcentaje: 30,
                noasCalNot: nota
              }

              let datosCalificaciones ={
                listaCalificacion: [datosListaCalificacion],
                modgenCodigo: this.codigoTrimestral,
                modgenNivel: nivelModeloGeneral
              }

              this.datosNotas.push(datosCalificaciones);
            }

            
            element.noasCalNot = JSON.stringify(this.datosNotas)
            // Guardar los cambios en el objeto element

            const datos ={
              "acaMallaAsignatura": element.acaMallaAsignatura.maasCodigo,
              "matCodigo": element.matCodigo,
              "noasCalNot": element.noasCalNot,
              "noasCodigo": element.noasCodigo,
              "noasEstado": element.noasEstado
            }

            this.calificacionHabilidadService.guardarNotaAsignatura(datos).subscribe({
              next: (response) => {
  
              },
              error: (error) => {
                Swal.fire({
                  icon: "error",
                  title: "Advertencia",
                  text: "Los datos no se pudieron guardar"
                });
              }
            })

          }
        });
      }
      
      return notaAsignatura; // Devolver la lista modificada
    } catch (error) {
      // Manejar el error, por ejemplo, lanzando una excepción o devolviendo un valor predeterminado
      console.error("Error en generarCalificacionAsignatura:", error);
      return null;
    }
  }


  async sacarIntervalos(mat, acumulador, grupo) {
    try {
      const data = await this.calificacionHabilidadService.listarIntervalos();
      const lista = data.listado;
  
      let nota = null;

      if(grupo == 1){
        const listaFiltro = lista.filter(obj => obj.intCodigo >= 1 && obj.intCodigo <= 4);
        listaFiltro.forEach(element => {
          if (acumulador >= element.intInicio && acumulador <= element.intFin) {
            nota = element.intNemonico;
          }
        });
      }else if(grupo == 2){
        const listaFiltro = lista.filter(obj => obj.intCodigo === 5);
        listaFiltro.forEach(element => {
          if (acumulador >= element.intInicio && acumulador <= element.intFin) {
            nota = element.intNemonico;
          }
        });

      }else if(grupo == 3){
        const listaFiltro = lista.filter(obj => obj.intCodigo >= 6);
        listaFiltro.forEach(element => {
          if (acumulador >= element.intInicio && acumulador <= element.intFin) {
            nota = element.intNemonico;
          }
        });
      }
  
      await this.generarCalificacionAsignatura(mat, nota);

    } catch (error) {
      console.error(error);
    }
  }

  async sacarValorPonderado(mat) {
    try {
      let grupo = 0
      let seleccion = false;
      let seleccion2 = false;
      const data = await this.calificacionHabilidadService.buscarCalificacionHabilidadPorMatricula(mat);
      const listado = data.listado; 
      const dataFiltro = listado.filter(item => item.modgenCodigo === this.codigoTrimestral);
  
      let acumulador = 0;
      let acumulador2 = 0;
  
      for (const element of dataFiltro) {
        const ponderacionData = await this.calificacionHabilidadService.buscarPonderacionPorCodigo(element.ponCodigo);
        const aplicado = ponderacionData.objeto;

        if(aplicado.ponDescripcion ==="SIEMPRE" || aplicado.ponDescripcion === "FRECUENTEMENTE"){
          grupo = 1;
        }else if(aplicado.ponDescripcion === "OCASIONALMENTE"){
          seleccion = true
        }else if(aplicado.ponDescripcion === "NUNCA"){
          seleccion2 = true;
        }

        acumulador += aplicado.ponValor;
      }

      for(const element of dataFiltro){
        for(const ident of this.datosIdentificador){
          if(element.hinCodigo === ident){
            const ponderacionData = await this.calificacionHabilidadService.buscarPonderacionPorCodigo(element.ponCodigo);
            const aplicado = ponderacionData.objeto;
            acumulador2 += aplicado.ponValor;
          }
        }
      }

      if(seleccion && !seleccion2){
        grupo = 2;
      }

      if(!seleccion && seleccion2){
        grupo = 3;
      }

      if(seleccion && seleccion2){
        grupo = 3;
      }

      await this.generarNotaComportamiento(mat, acumulador2);
      await this.sacarIntervalos(mat, acumulador, grupo);
    } catch (error) {
      console.error(error);
    }
  }



  generarDatos(obj){

    const totalCriterios = obj.length

    const numerosIngresados = obj.filter(estudiante => estudiante.ponderacionSeleccionada !== undefined).length;

    if(numerosIngresados === totalCriterios){

      Swal.fire({
        title: 'Confirmar',
        text: '¿Desea Guardar la información?',
        icon: 'question',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Guardar',
        cancelButtonText: 'Cancelar'
      }).then((result) =>{
        if (result.isConfirmed){
          let valor = 0;
          let valorTotal = obj.length
          this.datosIdentificador = [];

          obj.forEach(element => {
            if(element.codigoIdentificador === 2){
              this.datosIdentificador.push(element.hinCodigo);
            }



            if(element.codigoCalificacion !== undefined){

              let datos ={
                ccriCodigo: element.codigoCalificacion,
                ccriEstado: 1,
                criCodigo: element.criCodigo,
                curCodigo: this.curCodigo,
                curparCodigo: this.curparCodigo,
                hinCodigo:  element.hinCodigo,
                matCodigo: this.codigoMatricula,
                modgenCodigo: parseInt(this.codigoTrimestral),
                ponCodigo: parseInt(element.ponderacionSeleccionada),
                reanleCodigo: this.codigoAnioElectivo
              }

              this.calificacionHabilidadService.editarCriteriorHabilidad(datos).subscribe({
                next: (response) => {
    
                  valor = valor + 1;
    
                  if(valor === valorTotal){
                    Swal.fire({
                      title: "Datos Guardados",
                      text: "Todos los datos han sido guardados correctamente",
                      icon: "success"
                    });

                    this.sacarValorPonderado(this.codigoMatricula);
                    this.datosTotales = null;
                    this.selectorAsignatura = null;
                  }
    
                },
                error: (error) => {
                  Swal.fire({
                    icon: "error",
                    title: "Advertencia",
                    text: "Los datos no se pudieron guardar"
                  });
                }
              })

            }else{

              let datos ={
                ccriEstado: 1,
                criCodigo: element.criCodigo,
                curCodigo: this.curCodigo,
                curparCodigo: this.curparCodigo,
                hinCodigo:  element.hinCodigo,
                matCodigo: this.codigoMatricula,
                modgenCodigo: parseInt(this.codigoTrimestral),
                ponCodigo: parseInt(element.ponderacionSeleccionada),
                reanleCodigo: this.codigoAnioElectivo
              }

              this.calificacionHabilidadService.guardarCriteriosHabilidad(datos).subscribe({
                next: (response) => {
    
                  valor = valor + 1;
    
                  if(valor === valorTotal){
                    Swal.fire({
                      title: "Datos Guardados",
                      text: "Todos los datos han sido guardados correctamente",
                      icon: "success"
                    });
                    this.sacarValorPonderado(this.codigoMatricula);
                    this.datosTotales = null;
                    this.selectorAsignatura = null;
                  }
    
                },
                error: (error) => {
                  Swal.fire({
                    icon: "error",
                    title: "Advertencia",
                    text: "Los datos no se pudieron guardar"
                  });
                }
              })

            }

          });





          for(const element of obj){
            
          }
  
        }
      })

    }else{
      Swal.fire({
        title: "ATENCIÓN",
        text: "Debe completar el formulario",
        icon: "error"
      });
    }

  }




}
