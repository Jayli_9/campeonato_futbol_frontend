import { Component, Input, OnInit, ViewChild } from '@angular/core';
import { CalificacionHabilidadService } from '../../../servicios/calificacionHabilidad.service';
import { ResponseGenerico } from '../../../interfaces/response-generico';
import { MatriculaOrdinaria } from '../../../interfaces/matricula-ordinaria';
import { MatTableDataSource } from '@angular/material/table';
import { MatPaginator } from '@angular/material/paginator';
import { orderBy } from 'lodash-es';

@Component({
  selector: 'app-lista-estudiantes-habilidades',
  templateUrl: './lista-estudiantes-habilidades.component.html',
  styleUrls: ['./lista-estudiantes-habilidades.component.scss']
})
export class ListaEstudiantesHabilidadesComponent implements OnInit {
  columns: string[] = ['Identificador','Nombre', 'Accion'];
  @Input() listaEstudiantesNotasChild: any[];
  @Input() cursoSeleccionadoChild;
  dataSource = new MatTableDataSource<Object>();
  

  @ViewChild(MatPaginator) paginator: MatPaginator;

  //listaMatricula
  listaMatriculas;

  codigoMatricula;
  estudianteSeleccionado;
  codigoAnioElectivo;

  datoSeleccion = 0;

  page: number = 1;
  totalItems: number;

  constructor(
    private readonly calificacionHabilidadService: CalificacionHabilidadService
  ) { }

  calcularTotalItems(): void {
    this.totalItems = this.listaMatriculas.length; // Por ejemplo, si listaMatriculas es un array, podrías usar su longitud
  }


  ingresarNotas(paral){
    this.estudianteSeleccionado = paral;
    this.codigoMatricula = paral.matCodigo
    this.codigoAnioElectivo = paral.reanleCodigo;
    this.datoSeleccion = 1; 
  }

  ngOnInit(): void {

    let codigoRegimen = this.cursoSeleccionadoChild.ofeCurso.reanleCodigo;
    let codigoParalelo = this.cursoSeleccionadoChild.ofeCursoParalelo.curparCodigo;

    this.calificacionHabilidadService.listaMatriculasPorRegimenAnioLectivoYParalelo(codigoRegimen, codigoParalelo).subscribe(
      (respuesta: ResponseGenerico<MatriculaOrdinaria>) => {
        this.listaMatriculas = respuesta.listado;
        this.listaMatriculas = orderBy(this.listaMatriculas, ['estudiante.estNombres'], ['asc']);
      })
  }

}
