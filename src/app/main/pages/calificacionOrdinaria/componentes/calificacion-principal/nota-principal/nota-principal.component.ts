import {
  ChangeDetectorRef,
  Component,
  EventEmitter,
  Input,
  OnInit,
  Output,
  QueryList,
  ViewChild,
  ViewChildren,
} from "@angular/core"
import { CalificacionOrdinariaService } from "../../../servicios/calificacionOrdinaria.service"
import { IacaModevaGen } from "../../../interfaces/IacaModevaGen"
import { IacaParametrizacionCalpre } from "../../../interfaces/IacaParametrizacionCalpre"
import dayjs from "dayjs"
import { NgxSpinnerService } from "ngx-spinner"
import { MensajesService } from "app/main/pages/asignacionDocentesOrdinaria/servicios/mensajes.service"
import { IcatRangoNota } from "../../../interfaces/IcatRangoNota"
import { IacaCalificacionDTO } from "../../../interfaces/IacaCalificacionDTO"
import { MatTab, MatTabChangeEvent, MatTabGroup } from "@angular/material/tabs"
import { InicialService } from "../../../servicios/inicial.service"
import { IAcaParamCalPreDTO } from "../../../interfaces/IAcaParamCalPreDTO"
import { IParametrizacionDTO } from "../../../interfaces/IParametrizacionDTO"
import { constanteCalificacion } from "../../../constantes/constanteCalificacion"
import { NotaOvpComponent } from "../nota-ovp/nota-ovp.component"
import { IacaCalifNivel } from "../../../interfaces/IacaCalifNivel"
import { IAcaAmbito } from "../../../interfaces/IacaAmbito"
import { IacaDistributivo } from "../../../interfaces/IacaDistributivo"
import { MatSelectChange } from "@angular/material/select"
import { IAlumnosMateriaAmbitoModeloDTO } from "../../../interfaces/IAlumnosMateriaAmbitoModeloDTO"
import { IAcaDestreza } from "../../../interfaces/IacaDestreza"
import { IAcaNotaDestreza } from "../../../interfaces/IAcaNotaDestreza"
import { IDataPrepaDTO } from "../../../interfaces/IDataPrepaDTO"
import { Idestreza } from "../../../interfaces/Idestreza"

@Component({
  selector: "app-nota-principal",
  templateUrl: "./nota-principal.component.html",
  styleUrls: ["./nota-principal.component.scss"],
})
export class NotaPrincipalComponent implements OnInit {
  @ViewChild(NotaOvpComponent) contenidoTabComponent: NotaOvpComponent
  @ViewChildren(MatTabGroup) tabGroups: QueryList<MatTabGroup>
  //Inputs
  @Input() cursoSeleccionadoChild
  @Input() listaCruceValidacionChild
  //Outputs
  @Output() deshabilitarNotaPrincipalOutput: EventEmitter<boolean>
  //listas
  public listaModEvaGen: IacaModevaGen[] = []
  public listaReanleCodigo: any[] = []
  public listaCruceValidacion: any[] = []
  public listaCalifNivel: IacaCalifNivel[] = []
  public listaParametrizacionCalPre: IacaParametrizacionCalpre[] = []
  public listaEstudiantesNotas: IacaCalificacionDTO[] = []
  public listaModeloGeneralPresentacion: any[] = []
  public tipoValoracion: string
  public nivelCodigo: number
  public graCodigo: number
  public selectedTabIndex: number = 0
  public constantes
  public listaAmbitos: IAcaAmbito[]
  public listaDestrezas: IAcaDestreza[]
  headerStatic: string[] = ["estudiante"]
  public listaAlumnosParent: IDataPrepaDTO[]
  //objeto
  public rangoNota: IcatRangoNota
  public listaParamsCalPre: IAcaParamCalPreDTO[]
  public modeloActualInicialPrepa: number
  public asiNemonico: string
  public nombreMateria: string;
  public ambitoSelected: number
  displayedColumns: string[] = []
  public showtable: boolean

  constructor(
    private calificacionOrdinariaService: CalificacionOrdinariaService,
    private spinnerService: NgxSpinnerService,
    private cdr: ChangeDetectorRef,
    private mensajeService: MensajesService,
    private inicialService: InicialService
  ) {
    this.deshabilitarNotaPrincipalOutput = new EventEmitter<boolean>()
    this.listaAlumnosParent = []
    this.showtable = false
    this.ambitoSelected = 0
  }


  sacarComparadorTecnico(lista){
    try{
      this.graCodigo = lista.ofeCurso.graCodigo
      if(this.graCodigo < 4){
        this.nombreMateria = lista.acaMallaAsignatura.acaAsignaturaPrepa.apreDescripcion;
      }else{
        this.nombreMateria = lista.acaMallaAsignatura.acaAsignatura.asiDescripcion;
      }
    } catch{

    }
  }

  async ngOnInit(): Promise<void> {
    this.spinnerService.show()
    if (
      !!this.cursoSeleccionadoChild.acaMallaAsignatura.acaAsignaturaPrepa?.apreCodigo
    ) {
      await this.getAmbitos(this.cursoSeleccionadoChild)
    }    this.sacarComparadorTecnico(this.cursoSeleccionadoChild);
    this.constantes = constanteCalificacion

    try {
      // this.listaCruceValidacion = this.listaCruceValidacionChild;
      //filtro de acuerdo con cada curso o nivel
      this.graCodigo = this.cursoSeleccionadoChild.ofeCurso.graCodigo
      this.extraerModeloGeneralPorCurso(
        this.listaCruceValidacionChild,
        this.graCodigo,
        this.cursoSeleccionadoChild.reanleCodigo
      )
      //Fin filtro de acuerdo con cada curso o nivel
      this.nivelCodigo = this.cursoSeleccionadoChild.ofeCurso.nivCodigo
      if (this.nivelCodigo === 6)
        this.asiNemonico =
          this.cursoSeleccionadoChild.acaMallaAsignatura.acaAsignatura.asiNemonico
      else this.asiNemonico = null

      await this.consultarRangoCuantitativo(
        this.cursoSeleccionadoChild.reanleCodigo
      )
      await this.listarEstudaintesMatriculados(
        this.cursoSeleccionadoChild.reanleCodigo,
        this.cursoSeleccionadoChild.curCodigo,
        this.cursoSeleccionadoChild.curparCodigo,
        this.cursoSeleccionadoChild.ofeCurso.sereduCodigo
      )
      await this.getParamCalPre()
    } finally {
      this.spinnerService.hide()
    }
  }
  async extraerModeloGeneralPorCurso(
    listaModeloGeneralPadre,
    graCodigo,
    reanleCodigo
  ) {
    await this.calificacionOrdinariaService
      .getCalifNivelPorGraCodigoAndReanleCodigo(graCodigo, reanleCodigo)
      .then(
        (data) => {
          this.listaCalifNivel = data.listado
        },
        (error) => {
          this.mensajeService.mensajeError(
            "",
            "Error al consultar servicios academico califnivel" + error
          )
        }
      )
    // this.listaCruceValidacion = listaModeloGeneralPadre.filter(item => this.listaCalifNivel.some(elem => elem.modgenCodigo === item.modgenCodigo));
    this.listaCruceValidacion = listaModeloGeneralPadre.filter((item) =>
      this.listaCalifNivel.some((elem) => {
        if (elem.modgenCodigo === item.modgenCodigo) {
          item.calnivActivo = elem.calnivActivo
          return true
        }
        return false
      })
    )
  }
  volverCalificacionPrincipal() {
    this.deshabilitarNotaPrincipalOutput.emit(false)
  }

  async consultarRangoCuantitativo(reanleCodigo) {
    await this.calificacionOrdinariaService
      .getRangoNotaPorReanleCodigo(reanleCodigo)
      .then(
        (data) => {
          this.rangoNota = data.listado[0]
        },
        (error) => {
          this.mensajeService.mensajeError(
            "",
            "Error al consultar servicios catalgo rango de nota" + error
          )
        }
      )
  }

  async listarEstudaintesMatriculados(
    reanleCodigo,
    curCodigo,
    curparCodigo,
    sereduCodigo
  ) {
    await this.calificacionOrdinariaService
      .getMatriculaOrdinariaObjetoConsulta(
        reanleCodigo,
        curCodigo,
        curparCodigo,
        sereduCodigo
      )
      .then((data) => {
        this.listaEstudiantesNotas = data.listado
        for (const objeto of this.listaEstudiantesNotas) {
          objeto.maasCodigo =
            this.cursoSeleccionadoChild.acaMallaAsignatura.maasCodigo
          objeto.listaModeloGeneral = this.listaCruceValidacion.map((obj1) => ({
            modgenCodigo: obj1.modgenCodigo,
            modgenNivel: obj1.modgenNivel,
            modgenNemonico: obj1.modgenNemonico,
            listaCalificacion: obj1.listaCalificacion.map((obj2) => ({
              calCodigo: obj2.calCodigo,
              noasCalNot: null,
              calMaximo: obj2.calMaximo,
              calMinimo: obj2.calMinimo,
              calPorcentaje: obj2.calPorcentaje,
            })),
          }))
        }
      })

    this.tipoValoracion =
      this.cursoSeleccionadoChild?.acaMallaAsignatura?.acaAsignatura?.acaTipoValoracion?.tivaDescripcion
    this.listaModeloGeneralPresentacion = this.listaCruceValidacion

    for (const iterator of this.listaModeloGeneralPresentacion) {
      const index = this.listaCruceValidacion.indexOf(iterator)
      if (!iterator.activarNota) {
        this.activarPestana(index)
        break
      }
    }
    let fechaActual = new Date()
    this.listaModeloGeneralPresentacion.forEach((modelo) => {
      modelo.activarModelo = !(
        fechaActual >= this.getFecha(modelo.papreInicio) &&
        fechaActual <= this.getFecha(modelo.papreFin)
      )
    })
  }
  activarPestana(indice: number): void {
    this.selectedTabIndex = indice
  }

  async getParamCalPre() {
    let modelos: number[] = []
    this.listaModeloGeneralPresentacion.forEach((modelo) => {
      if (modelo.modgenNivel === 3) {
        modelos.push(modelo.modgenCodigo)
      }
    })
    let data: IParametrizacionDTO = {
      reanleCodigo: this.cursoSeleccionadoChild.reanleCodigo,
      modgenCodigo: modelos,
    }
    await this.inicialService
      .getParamCalPre(data)
      .then((respuesta) => {
        this.listaParamsCalPre = respuesta.listado
      })
      .catch((error) => {
        console.log("error")
      })
  }

  getFecha(fecha: Date) {
    return new Date(fecha)
  }

  onLinkClick($event: MatTabChangeEvent) {
    if (
      this.asiNemonico === "AI" &&
      this.nivelCodigo === 6 &&
      $event.index === 3
    ) {
      //const nuevosDatos = this.contenidoTabComponent.actualizarDatos();
      this.ngOnInit()
      //$event.tab.ngOnInit()
    }
  }

  async getAmbitos(distributivo: IacaDistributivo) {
    await this.inicialService
      .getAmbitosPorApreCodigo(
        distributivo.acaMallaAsignatura.acaAsignaturaPrepa?.apreCodigo
      )
      .then((res) => {
        this.listaAmbitos = res.listado
        this.spinnerService.hide()
      })
      .catch((error) => {
        console.log(error)
        this.spinnerService.hide()
      })
  }

  async handlerAmbito(evento: MatSelectChange) {
    if (!!evento && !!evento.value) {
      this.showtable = false
      // this.spinnerService.show()
      this.ambitoSelected = evento.value
      await this.analizarExisteInformacionNotasDestrezas(
        this.ambitoSelected,
        this.listaEstudiantesNotas
      )
    }
  }

  async analizarExisteInformacionNotasDestrezas(
    ambito: number,
    estudiantes: any[]
  ) {
    let listaMatCodigoAlumnos: number[] = []

    if (estudiantes.length > 0) {
      await this.getDestrezas(ambito)

      estudiantes.forEach((estudiante: any) => {
        listaMatCodigoAlumnos.push(estudiante.matCodigo)
      })

      let data: IAlumnosMateriaAmbitoModeloDTO = {
        ambCodigo: ambito,
        modgenCodigo: null,
        maatCodigo: this.cursoSeleccionadoChild.acaMallaAsignatura.maasCodigo,
        estudiantes: listaMatCodigoAlumnos,
      }
      await this.inicialService
        .getAlumnosNotasPrepa(data)
        .then(async (res) => {
          if (res?.listado.length > 0) {
            await this.setearNotasEstudiantes(
              res.listado,
              this.listaEstudiantesNotas,
              this.listaDestrezas
            )
          } else {
            await this.setTableData(
              this.listaDestrezas,
              this.listaEstudiantesNotas
            )
          }
        })
        .catch((error) => {
          this.spinnerService.hide()
          console.error(error)
        })
    } else {
      // Swal.fire(
      //     "Error",
      //     "No existen alumnos matriculados en este curso",
      //     "info"
      // )
      this.spinnerService.hide()
    }
  }

  async getDestrezas(ambCodigo: number) {
    await this.inicialService
      .getDestrezasPorAmbito(ambCodigo)
      .then(async (res) => {
        this.listaDestrezas = res.listado
        // this.spinnerService.hide()
      })
      .catch((error) => {
        console.log(error)
        this.spinnerService.hide()
      })
  }

  async setearNotasEstudiantes(
    estNotas: IAcaNotaDestreza[],
    estLista: any[],
    destrezas: any
  ) {
    // await this.setHeaderColumns(destrezas)
    let data: IDataPrepaDTO[] = []
    estNotas.forEach(async (estNota, i) => {
      let estudianteFind = estLista.find(
        (est) => est.matCodigo === estNota.matCodigo
      )
      let nodeCalNotaDes = JSON.parse(estNota.nodeCalNotaDes)

      data.push({
        nodeCodigo: estNota.nodeCodigo,
        estCodigo: estudianteFind.estudiante.estCodigo,
        matCodigo: estNota.matCodigo,
        nombre: estudianteFind.estudiante.estNombres,
        modgenCodigo: nodeCalNotaDes.modelo,
        destrezas: nodeCalNotaDes.destrezas.map(
          (destreza: Idestreza, i: number) => {
            return {
              desCodigo: destreza.desCodigo,
              resCodigo: null,
              resEscala: destreza.resEscala,
            }
          }
        ),
      })
    })
    this.listaAlumnosParent = data
    this.showtable = true
    // this.dataFinalPrepa = data
    // this.setTableNotas(this.dataFinalPrepa)
  }

  async setTableData(
    destrezas: IAcaDestreza[],
    listaEstudiantes: IacaCalificacionDTO[]
  ) {
    await this.setHeaderColumns(destrezas)
    await this.setBodyData(listaEstudiantes, destrezas)
  }

  async setHeaderColumns(destrezas: IAcaDestreza[]) {
    this.displayedColumns = []
    this.displayedColumns.push(this.headerStatic[0])
    destrezas.forEach((_destreza, i) => {
      this.displayedColumns.push(`des_${i}`)
    })
  }

  async setBodyData(listaEstudiantes: any[], destrezas: IAcaDestreza[]) {
    let data: IDataPrepaDTO[] = []
    listaEstudiantes.forEach(async (item, i) => {
      data.push({
        nodeCodigo: null,
        estCodigo: item.estudiante.estCodigo,
        matCodigo: item.matCodigo,
        nombre: item.estudiante.estNombres,
        destrezas: destrezas.map((destreza, i) => {
          return {
            desCodigo: destreza.desCodigo,
            resCodigo: null,
            resEscala: null,
          }
        }),
      })
    })

    // this.activarGuardar()
    //this.setTableNotas(data)
    this.listaAlumnosParent = data
    this.showtable = true
  }
}
