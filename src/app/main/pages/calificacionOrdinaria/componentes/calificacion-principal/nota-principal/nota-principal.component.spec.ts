import { ComponentFixture, TestBed } from '@angular/core/testing';

import { NotaPrincipalComponent } from './nota-principal.component';

describe('NotaPrincipalComponent', () => {
  let component: NotaPrincipalComponent;
  let fixture: ComponentFixture<NotaPrincipalComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ NotaPrincipalComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(NotaPrincipalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
