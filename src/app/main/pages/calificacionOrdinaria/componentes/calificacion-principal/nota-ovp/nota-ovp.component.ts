import {ChangeDetectorRef, Component, Input, OnInit, ViewChild} from '@angular/core';
import {MatSelectChange} from '@angular/material/select';
import {NgxSpinnerService} from 'ngx-spinner';
import {IcatRangoNota} from '../../../interfaces/IcatRangoNota';
import {IacaRangoEscala} from '../../../interfaces/IacaRangoEscala';
import {MatTableDataSource} from '@angular/material/table';
import {MatPaginator} from '@angular/material/paginator';
import {MatSort} from '@angular/material/sort';
import Swal from 'sweetalert2';
import {MensajesService} from '../../../../asignacionDocentesOrdinaria/servicios/mensajes.service';
import {CalificacionOrdinariaService} from '../../../servicios/calificacionOrdinaria.service';
import {DomSanitizer} from '@angular/platform-browser';
import {IacaDistributivo} from '../../../interfaces/IacaDistributivo';
import {IacaCalificacionDTO} from '../../../interfaces/IacaCalificacionDTO';
import {EjeGrado} from '../../../../eje-grado/interfaces/eje-grado';
import {OvpService} from '../../../servicios/ovp.service';
import {IAcaNotaOvp} from '../../../interfaces/IAcaNotaOvp';
import {IDataClobOvpDto} from '../../../interfaces/IDataClobOvpDto';
import {AcaEjeGrado} from '../../../../eje-grado/interfaces/aca-eje-grado';
import {IAlumnoMeteriaEjeModeloDTO} from '../../../interfaces/IAlumnoMeteriaEjeModeloDTO';
import {IDataOvpDTO} from '../../../interfaces/IDataOvpDTO';
import {IEjeGrado} from '../../../interfaces/IEjeGrado';
import {IDataEjeGradoDTO} from '../../../interfaces/IDataEjeGradoDTO';

@Component({
    selector: 'app-nota-ovp',
    templateUrl: './nota-ovp.component.html',
    styleUrls: ['./nota-ovp.component.scss']
})
export class NotaOvpComponent implements OnInit {
    public tipoInput: string = 'text';
    public rangoNota: IcatRangoNota;
    private rangoEscalas: IacaRangoEscala[]
    public showTable: boolean;
    //Tabla y dataSource
    dataSource: MatTableDataSource<any> = new MatTableDataSource();
    headerStatic: string[] = ['estudiante'];
    displayedColumns: string[] = [];
    dataFinalOvp: IDataOvpDTO[];
    @ViewChild(MatPaginator) paginator: MatPaginator;
    @ViewChild(MatSort) sort!: MatSort;
    //Inputs
    @Input() cursoSeleccionadoChild: IacaDistributivo;
    @Input() listaEstudiantesNotasChild: IacaCalificacionDTO[];
    @Input() modgenCodigoChild: any;
    public listaEstudiantesNotas: IacaCalificacionDTO[] = [];
    public listaEjePorGrado: AcaEjeGrado[];
    public listaEjeGrado: EjeGrado[];
    public constantes: any;
    public enableGuardar: boolean = true;
    //Tabla
    private curCodigo: number;
    private curparCodigo: number;
    private reanleCodigo: number;
    private insestCodigo: any;
    ejgrSelected: number;
    tieneRegistros: boolean = false;
    listaRangoEscala: IacaRangoEscala[] = [];


    constructor(
        private mensajeService: MensajesService,
        private spinnerService: NgxSpinnerService,
        private ovpService: OvpService,
        private calificacionOrdinariaService: CalificacionOrdinariaService,
        private sanitizer: DomSanitizer,
        private changeDetectorRefs: ChangeDetectorRef
    ) {
        this.ejgrSelected = 0;
        this.dataFinalOvp = [];
        this.showTable = false;
    }

    applyFilter(filterValue: string) {
        filterValue = filterValue.trim(); // Remove whitespace
        filterValue = filterValue.toLowerCase(); // MatTableDataSource defaults to lowercase matches
        this.dataSource.filter = filterValue;
    }

    async ngOnInit(): Promise<void> {

        this.spinnerService.show();
        await this.getEjeGradoPorGradoReanleCodigo(this.cursoSeleccionadoChild);
        await this.getRangoEscalas();
        this.activarBotonGuardar();
        this.curCodigo = this.cursoSeleccionadoChild.curCodigo;
        this.curparCodigo = this.cursoSeleccionadoChild.curparCodigo;
        this.insestCodigo = this.cursoSeleccionadoChild.ofeCurso.insestCodigo;
        this.reanleCodigo = this.cursoSeleccionadoChild.reanleCodigo;
        await this.cargarRangoEscala(this.cursoSeleccionadoChild.acaMallaAsignatura.acaAsignatura.asiNemonico, this.cursoSeleccionadoChild.ofeCurso.graCodigo);
    }

    async handlerEje(evento: MatSelectChange) {
        if (!!evento && !!evento.value) {
            this.showTable = false;
            this.spinnerService.show();
            this.ejgrSelected = evento.value;
            await this.manejarCargaNotas();
            this.tieneRegistros = this.dataSource.data.length > 0;
            /*await this.verificarInformacionNotasOvpExistente(this.ejgrSelected, this.listaEstudiantesNotasChild);*/
        }
    }

    async getEjeGradoPorGradoReanleCodigo(distributivo: IacaDistributivo) {
        await this.ovpService
            .getEjeGradosPorGradoPorReanleCodigo(
                distributivo.acaMallaAsignatura.acaAsignatura.graCodigo, distributivo.reanleCodigo
            )
            .then((res) => {
                this.listaEjePorGrado = res.listado;

                this.spinnerService.hide();
            })
            .catch((error) => {
                console.error("Error" +
                    "Ejes Erroreos");
                this.spinnerService.hide();
            });
    }

    async getEjeGradosColumna(ejgrCod: number) {
        this.listaEjeGrado=[]
        await this.ovpService
            .getEjeGradoPorEjeSelecionado(ejgrCod)
            .then(async (res) => {

                this.listaEjeGrado.push(res.objeto);
                // this.spinnerService.hide()
            })
            .catch((error) => {
                console.error(error);
                this.spinnerService.hide();
            });
    }

    async getRangoEscalas() {

        await this.calificacionOrdinariaService
            .getConsultarRangoEscalasObjetoConsulta(
                this.cursoSeleccionadoChild.acaMallaAsignatura.acaAsignatura.asiNemonico,
                this.cursoSeleccionadoChild.acaMallaAsignatura.acaAsignatura.graCodigo
            )
            .then((respuesta) => {
                this.rangoEscalas = respuesta.listado;
                this.spinnerService.hide();
            })
            .catch((error) => {
                console.error('error en la consulta de rango escalas: ', error);
                this.spinnerService.hide();
            });
    }

    async setTableData(ejeGrados: EjeGrado[],listaEstudiantes: IacaCalificacionDTO[]) {
        this.showTable = true;
        await this.setHeaderColumns(ejeGrados);
        await this.setBodyData(listaEstudiantes, ejeGrados);
    }

    async setHeaderColumns(ejeGrados: EjeGrado[]) {
        this.displayedColumns = [];
        this.displayedColumns.push(this.headerStatic[0]);
        ejeGrados.forEach((_ejeGrado, i) => {
            this.displayedColumns.push(`eje_${i}`);
        });
    }

    async setBodyData(listaEstudiantes: any[], ejeGrados: EjeGrado[]) {
        let data: IDataOvpDTO[] = [];

        for (const item of listaEstudiantes) {
            const i = listaEstudiantes.indexOf(item);
            data.push({
                ovpCodigo: null,
                estCodigo: item.estudiante.estCodigo,
                matCodigo: item.matCodigo,
                nombre: item.estudiante.estNombres,
                jsonActual:null,
                calificacion: ejeGrados.map((ejeGrado, i) => {
                    return {
                        ejeCodigo: ejeGrado.ejeCodigo,
                        resEscala: null,
                    };
                }),
            });
        }

        this.dataFinalOvp = data;
        this.activarBotonGuardar();
        this.setTableNotas(this.dataFinalOvp);
    }

    setTableNotas(data: IDataOvpDTO[]) {
        this.paginador();
        this.dataSource.data = this.sortTableData(data);
        this.dataSource.paginator = this.paginator;
        this.changeDetectorRefs.detectChanges();
        this.spinnerService.hide();
    }

    paginador() {
        this.paginator.pageSize = 5;
        this.paginator._intl.itemsPerPageLabel = 'Filas por página';
        this.paginator._intl.lastPageLabel = 'última página';
        this.paginator._intl.nextPageLabel = 'Siguiente página';
        this.paginator._intl.previousPageLabel = 'Página anterior';
    }

    handlerEscala(event: any, elemento: any) {


    }

    activarBotonGuardar() {
        let isNull: boolean = false;
        if (this.dataFinalOvp.length !== 0) {
            this.dataFinalOvp.some((data) => {
                return data.calificacion.some((ejeGrado, i) => {
                    if (!!!ejeGrado.resEscala) {
                        this.enableGuardar = isNull = true;
                        return true;
                    }
                    return false;
                });
            });
            if (!isNull) {
                this.enableGuardar = false;
            }
        }
    }
    async  calcularNotaTrimestral(listaJSON:IDataClobOvpDto) {

    }
    async guardarOvp() {
        try {
            this.spinnerService.show();

            const notasOvp: IAcaNotaOvp[] = await this.prepararNotasOvp();
            await this.ovpService.guardarNotasOvp(notasOvp);
            await this.manejarCargaNotas()
            this.showTable = true;

            Swal.fire('Éxito', 'Información almacenada correctamente', 'success');
            this.spinnerService.hide();
        } catch (error) {
console.log('error al guardr',error);
            Swal.fire('Error', 'Hubo un error al guardar la información', 'error');
            this.spinnerService.hide();
        }

    }

    async prepararNotasOvp(): Promise<IAcaNotaOvp[]> {
        const resultado: IAcaNotaOvp[] = [];

        const elementosConCalificacionValida = this.dataFinalOvp.filter(item =>
            item.calificacion.some(calificacion => calificacion.resEscala !== null)
        );

        for (const estudiante of elementosConCalificacionValida) {
            let listaDataClob: IDataClobOvpDto[] = [];

            if (estudiante.jsonActual === null) {
                listaDataClob = this.crearEstructuraCalificacionVacia();
            } else {
                listaDataClob = this.parseJsonActual(estudiante.jsonActual);
            }

            const calificacionNuevo: IDataClobOvpDto = {
                modelo: this.modgenCodigoChild,
                ejgrCodigo: this.ejgrSelected,
                calificacion: estudiante.calificacion,
            };

            const indiceExistente = listaDataClob.findIndex(item =>
                item.modelo === calificacionNuevo.modelo && item.ejgrCodigo === calificacionNuevo.ejgrCodigo
            );

            if (indiceExistente !== -1) {
                listaDataClob[indiceExistente].calificacion = calificacionNuevo.calificacion;
            } else {
                listaDataClob.push(calificacionNuevo);
            }

            const listaDataCalculos = await this.sumarYActualizarCalificaciones(listaDataClob);

            resultado.push({
                curCodigo: this.curCodigo,
                curParCodigo: this.curparCodigo,
                insEstCodigo: this.insestCodigo,
                reanleCodigo: this.reanleCodigo,
                ovpCodigo: estudiante.ovpCodigo,
                ovpCalNot: JSON.stringify(listaDataCalculos),
                matCodigo: estudiante.matCodigo,
                ovpEstado: 1,
                maasCodigo: this.cursoSeleccionadoChild.acaMallaAsignatura.maasCodigo
            });
        }

        return resultado;
    }


    async manejarCargaNotas() {
        const [listadoExistente, existenteEnBase] = await this.verificarNotasOvpExistente(this.ejgrSelected, this.listaEstudiantesNotasChild);

        if (existenteEnBase) {
            await this.setearNotasEstudiantes(listadoExistente,this.listaEstudiantesNotasChild,this.listaEjeGrado);
        } else {
            await this.setTableData(this.listaEjeGrado,this.listaEstudiantesNotasChild);
        }
    }

    async manejarPreGuardadoNotas() {
        const [listadoExistente, existenteEnBase] = await this.verificarNotasOvpExistente(this.ejgrSelected, this.listaEstudiantesNotasChild);

        if (existenteEnBase) {
            await this.setearNotasEstudiantes(listadoExistente,this.listaEstudiantesNotasChild,this.listaEjeGrado);
        } else {
            await this.setTableData(this.listaEjeGrado,this.listaEstudiantesNotasChild);
        }
    }


    private parseJsonActual(jsonActual: string): IDataClobOvpDto[] {
        return jsonActual !== null ? JSON.parse(jsonActual) : [];
    }
    async verificarNotasOvpExistente(ejgrCodigo: number, estudiantes: any[]) :Promise<[IAcaNotaOvp[], boolean]>{
    let listaMatCodigoAlumnos: number[] = [];
        let listadoExistente: IAcaNotaOvp[]=[];
        let existenteEnBase: boolean = false;

        if (estudiantes.length > 0) {

            await this.getEjeGradosColumna(this.ejgrSelected);

            estudiantes.forEach((estudiante: any) => {
                listaMatCodigoAlumnos.push(estudiante.matCodigo);
            });

            let data: IAlumnoMeteriaEjeModeloDTO = {
                curCodigo:this.curCodigo,
                curParCodigo:this.cursoSeleccionadoChild.curparCodigo,
                insestCodigo:this.insestCodigo,
                reanleCodigo:this.reanleCodigo,
                maasCodigo:this.cursoSeleccionadoChild.acaMallaAsignatura.maasCodigo,
                ejgrCodigo:this.ejgrSelected,
                estudiantes: listaMatCodigoAlumnos,
            };

            await this.ovpService
                .getAlumnosNotasOvp(data)
                .then(async (res) => {

                    if (res.listado.length > 0) {
                        listadoExistente=res.listado
                        existenteEnBase = true;
                       /* await this.setearNotasEstudiantes(res.listado,this.listaEstudiantesNotasChild,this.listaEjeGrado);*/
                    } else {
                        listadoExistente=null
                        existenteEnBase=false
                        /*await this.setTableData(this.listaEjeGrado,this.listaEstudiantesNotasChild);*/
                    }
                })
                .catch((error) => {
                    this.spinnerService.hide();
                    console.error(error);
                });
        } else {
            Swal.fire(
                'Error',
                'No existen alumnos matriculados en este curso',
                'info'
            );
            this.spinnerService.hide();
        }
        return [ listadoExistente, existenteEnBase ]
    }

    async setearNotasEstudiantes(estNotas: IAcaNotaOvp[],estLista: any[],ejeGrados: any ){
        this.showTable = true;
        await this.setHeaderColumns(ejeGrados);
        let data: IDataOvpDTO[] = [];
        for(const itemEst of estLista){
            const i = estNotas.indexOf(itemEst);
            let estudianteOvp = estNotas.find((est) => est.matCodigo === itemEst.matCodigo);

            if(estudianteOvp){//si hay estudiante con nota
                let ovpCalNota1 = JSON.parse(estudianteOvp.ovpCalNot);
                let listaOvpCalNota: IDataClobOvpDto[] = [];
                listaOvpCalNota = JSON.parse(estudianteOvp.ovpCalNot);
                const elementoEncontrado: IDataClobOvpDto | undefined = listaOvpCalNota.find(item => item.modelo === this.modgenCodigoChild && item.calificacion!==null);

                if(elementoEncontrado!== undefined ) {// hay estudiante con notas con el modelo actual
                    data.push({
                        ovpCodigo: estudianteOvp.ovpCodigo,
                        estCodigo: itemEst.estudiante.estCodigo,
                        matCodigo: itemEst.matCodigo,
                        nombre: itemEst.estudiante.estNombres,
                        jsonActual: estudianteOvp.ovpCalNot,
                        calificacion: elementoEncontrado.calificacion.map((ejeGrado: IEjeGrado, i: number) => {
                                return {
                                    ejeCodigo: ejeGrado.ejeCodigo,
                                    resEscala: ejeGrado.resEscala,
                                };
                            }
                        ),
                    });
                }
                else{// hay estudiante con otras notas pero no del modelo actual
                    data.push({
                        ovpCodigo: estudianteOvp.ovpCodigo,
                        estCodigo: itemEst.estudiante.estCodigo,
                        matCodigo: itemEst.matCodigo,
                        jsonActual: estudianteOvp.ovpCalNot,
                        nombre: itemEst.estudiante.estNombres,
                        calificacion: ejeGrados.map((ejeGrado: IEjeGrado, i: number) => {
                                return {
                                    ejeCodigo: ejeGrado.ejeCodigo,
                                    resEscala: null,
                                };
                            }
                        ),
                    });
                    }

            }else{// no hay estudiante en ovp
                data.push({
                    ovpCodigo: null,
                    estCodigo: itemEst.estudiante.estCodigo,
                    matCodigo: itemEst.matCodigo,
                    nombre: itemEst.estudiante.estNombres,
                    jsonActual: null,
                    calificacion: ejeGrados.map((ejeGrado: IEjeGrado, i: number) => {
                            return {
                                ejeCodigo: ejeGrado.ejeCodigo,
                                resEscala: null,
                            };
                        }
                    ),
                });

            }
        }

        this.dataFinalOvp = data;
        this.setTableNotas(this.dataFinalOvp);
    }

    sortTableData(data: any[]) {
        return data.sort((a, b) => a.nombre.localeCompare(b.nombre));
    }

    crearEstructuraCalificacionVacia():IDataClobOvpDto[]{
        const listaModelo: number[] = [8, 9, 10, 11]; // codigos de cada modelo general de evaluacion
        let listaCalificacion:IDataClobOvpDto[]=[]
        listaModelo.forEach(numModelo =>{
            const calificacion: IDataClobOvpDto = {
                modelo: numModelo,
                ejgrCodigo: this.ejgrSelected,
                calificacion: null,
            };
            listaCalificacion.push(calificacion)
        })
        return listaCalificacion
    }
    async sumarYActualizarCalificaciones(listaCalificaciones: IDataClobOvpDto[]): Promise<IDataClobOvpDto[]>  {
        const sumaCalificaciones = listaCalificaciones
            .filter(c => c.modelo === 8 || c.modelo === 9 || c.modelo === 10)
            .reduce((acum, curr) => {
                if (!curr.calificacion || !Array.isArray(curr.calificacion)) {
                    return acum;
                }
                const suma = curr.calificacion.reduce((sum, item) => {

                    const rango = this.listaRangoEscala.find(r => r.resEscala === item.resEscala);
                    const valor = rango ? rango.vmin : null;
                    return sum + (valor !== null ? valor : 0);
                }, 0);
                return acum + suma;
            }, 0);
        let sumatoria:number
        sumatoria=sumaCalificaciones
        let valorCualitativo

        valorCualitativo= await this.convertirValorCualitativo(sumaCalificaciones)

        const indiceModeloSumatorio = listaCalificaciones.findIndex(c => c.modelo === 11);
        const ejeGrado = this.listaEjeGrado.find(ejeGrado => ejeGrado.ejgrCodigo === 5);
        const nuevoEjeGradoDTO: IDataEjeGradoDTO = {
            ejeCodigo: ejeGrado ? ejeGrado.ejeCodigo : null,
            resEscala: valorCualitativo,
        };

        if (indiceModeloSumatorio !== -1) {
            listaCalificaciones[indiceModeloSumatorio].calificacion = [nuevoEjeGradoDTO];
        } else {
            listaCalificaciones.push({
                modelo: 11,
                ejgrCodigo: this.ejgrSelected,
                calificacion: [nuevoEjeGradoDTO],
            });
        }

        return listaCalificaciones;
    }


    cargarRangoEscala(resAgrupacion: string, graCodigo) {
        this.calificacionOrdinariaService.getConsultarRangoEscalasObjetoConsulta(resAgrupacion, graCodigo).then(data => {
            this.listaRangoEscala = data.listado;
        })
    }
    async convertirValorCualitativo(sumatoria:number):Promise<string>{
    let valorCualitativo=null
        try {
            const respuesta = await this.calificacionOrdinariaService.getObtenerValorCualitativoOVP(this.reanleCodigo,sumatoria).toPromise();
            if (respuesta.objeto) {
                valorCualitativo = respuesta.objeto.ranValor

            }
        } catch (error) {
            valorCualitativo=null
            console.error('Coeficiente erroneo')
        }

        return valorCualitativo
    }


    actualizarDatos(): void {
        this.ngOnInit()

    }
}
