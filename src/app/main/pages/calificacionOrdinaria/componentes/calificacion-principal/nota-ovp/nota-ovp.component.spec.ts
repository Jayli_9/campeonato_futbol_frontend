import { ComponentFixture, TestBed } from '@angular/core/testing';

import { NotaOvpComponent } from './nota-ovp.component';

describe('NotaOvpComponent', () => {
  let component: NotaOvpComponent;
  let fixture: ComponentFixture<NotaOvpComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ NotaOvpComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(NotaOvpComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
