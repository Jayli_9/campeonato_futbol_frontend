import {
  ChangeDetectorRef,
  Component,
  EventEmitter,
  Input,
  OnInit,
  Output,
  ViewChild,
} from "@angular/core"
import { MatTableDataSource } from "@angular/material/table"
import { MatPaginator } from "@angular/material/paginator"
import { IacaCalificacionDTO } from "../../../interfaces/IacaCalificacionDTO"
import { MensajesService } from "app/main/pages/asignacionDocentesOrdinaria/servicios/mensajes.service"
import { IcatRangoNota } from "../../../interfaces/IcatRangoNota"
import { NgxSpinnerService } from "ngx-spinner"
import { InicialService } from "../../../servicios/inicial.service"
import { IacaDistributivo } from "../../../interfaces/IacaDistributivo"
import { IAcaAmbito } from "../../../interfaces/IacaAmbito"
import { MatSelectChange } from "@angular/material/select"
import { IAcaDestreza } from "../../../interfaces/IacaDestreza"
import { MatSort } from "@angular/material/sort"
import { DomSanitizer, SafeHtml } from "@angular/platform-browser"
import { CalificacionOrdinariaService } from "../../../servicios/calificacionOrdinaria.service"
import { IacaRangoEscala } from "../../../interfaces/IacaRangoEscala"
import { IDataPrepaDTO } from "../../../interfaces/IDataPrepaDTO"
import { IAcaNotaDestreza } from "../../../interfaces/IAcaNotaDestreza"
import { IDataClobPrepaDTO } from "../../../interfaces/IDataClobPrepaDTO"
import { IAlumnosMateriaAmbitoModeloDTO } from "../../../interfaces/IAlumnosMateriaAmbitoModeloDTO"
import Swal from "sweetalert2"
import { Idestreza } from "../../../interfaces/Idestreza"

@Component({
  selector: "app-inicial",
  templateUrl: "./inicial.component.html",
  styleUrls: ["./inicial.component.scss"],
})
export class InicialComponent implements OnInit {
  //parametros tabla
  //variables
  public tipoInput: string = "text"
  public rangoNota: IcatRangoNota
  private rangoEscalas: IacaRangoEscala[]
  public showTable: boolean

  //Tabla y dataSource
  dataSource: MatTableDataSource<any> = new MatTableDataSource()
  headerStatic: string[] = ["estudiante"]
  displayedColumns: string[] = []
  dataFinalPrepa: IDataPrepaDTO[]

  @ViewChild(MatPaginator) paginator: MatPaginator
  @ViewChild(MatSort) sort!: MatSort
  //Tabla
  applyFilter(filterValue: string) {
    filterValue = filterValue.trim() // Remove whitespace
    filterValue = filterValue.toLowerCase() // MatTableDataSource defaults to lowercase matches
    this.dataSource.filter = filterValue
  }
  //Inputs
  @Input() cursoSeleccionadoChild: IacaDistributivo
  @Input() listaEstudiantesNotasChild: IacaCalificacionDTO[]
  @Input() modgenCodigoChild: any
  @Input() listaCompletaEstudiantesChild: any
  @Input() listaDestrezasChild: any
  @Input() ambitoChild: number

  public listaEstudiantesNotas: IacaCalificacionDTO[] = []
  public listaAmbitos: IAcaAmbito[]
  public listaDestrezas: IAcaDestreza[]
  public constantes: any
  private ambitoSelected: number
  public enableGuardar: boolean = true

  constructor(
    private mensajeService: MensajesService,
    private spinnerService: NgxSpinnerService,
    private inicialService: InicialService,
    private calificacionOrdinariaService: CalificacionOrdinariaService,
    private sanitizer: DomSanitizer,
    private changeDetectorRefs: ChangeDetectorRef
  ) {
    this.ambitoSelected = 0
    this.dataFinalPrepa = []
    this.showTable = false
  }

  async ngOnInit(): Promise<void> {
    this.spinnerService.show()
    this.showTable = true
    await this.getRangoEscalas()
    await this.setHeaderColumns(this.listaDestrezasChild)
    this.filtrarModelo(this.listaCompletaEstudiantesChild)

    this.activarGuardar()
  }

  async getRangoEscalas() {
    await this.calificacionOrdinariaService
      .getConsultarRangoEscalasObjetoConsulta(
        this.cursoSeleccionadoChild.acaMallaAsignatura.acaAsignaturaPrepa
          .resAgrupacion,
        this.cursoSeleccionadoChild.acaMallaAsignatura.acaAsignaturaPrepa
          .graCodigo
      )
      .then((respuesta) => {
        this.rangoEscalas = respuesta.listado
        this.spinnerService.hide()
      })
      .catch((error) => {
        console.log("error en la consulta de rango escalas: ", error)
        this.spinnerService.hide()
      })
  }

  async setTableData(
    destrezas: IAcaDestreza[],
    listaEstudiantes: IacaCalificacionDTO[]
  ) {
    this.showTable = true
    await this.setHeaderColumns(destrezas)
    await this.setBodyData(listaEstudiantes, destrezas)
  }

  async setHeaderColumns(destrezas: IAcaDestreza[]) {
    this.displayedColumns = []
    this.displayedColumns.push(this.headerStatic[0])
    destrezas.forEach((_destreza, i) => {
      this.displayedColumns.push(`des_${i}`)
    })
  }

  async setBodyData(listaEstudiantes: any[], destrezas: IAcaDestreza[]) {
    let data: IDataPrepaDTO[] = []
    listaEstudiantes.forEach(async (item, i) => {
      data.push({
        nodeCodigo: null,
        estCodigo: item.estudiante.estCodigo,
        matCodigo: item.matCodigo,
        nombre: item.estudiante.estNombres,
        destrezas: destrezas.map((destreza, i) => {
          return {
            desCodigo: destreza.desCodigo,
            resCodigo: null,
            resEscala: null,
          }
        }),
      })
    })
    this.dataFinalPrepa = data
    this.activarGuardar()
    this.setTableNotas(this.dataFinalPrepa)
  }

  setTableNotas(data: IDataPrepaDTO[]) {
    this.paginador()
    this.dataSource.data = this.sortTableData(data)
    this.dataSource.paginator = this.paginator
    this.changeDetectorRefs.detectChanges()
    this.spinnerService.hide()
  }

  paginador() {
    this.paginator.pageSize = 5
    this.paginator._intl.itemsPerPageLabel = "Filas por página"
    this.paginator._intl.lastPageLabel = "última página"
    this.paginator._intl.nextPageLabel = "Siguiente página"
    this.paginator._intl.previousPageLabel = "Página anterior"
  }

  handlerEscala(event: any, elemento: any) {
    if (!!event.value) {
      // this.activarGuardar()
    }
  }

  activarGuardar() {
    let isNull: boolean = false
    if (this.dataFinalPrepa.length !== 0) {
      this.dataFinalPrepa.some((data) => {
        return data.destrezas.some((destreza, i) => {
          if (!!!destreza.resEscala) {
            this.enableGuardar = isNull = true
            return true
          }
          return false
        })
      })
      if (!isNull) {
        this.enableGuardar = false
      }
    }
  }

  async guardar() {
    this.spinnerService.show()
    let notasDestrezas: IAcaNotaDestreza[] = []

    this.dataFinalPrepa.forEach((estudiante) => {
      let dataClob: IDataClobPrepaDTO = {
        ambito: this.ambitoChild,
        modelo: this.modgenCodigoChild,
        destrezas: estudiante.destrezas,
      }

      notasDestrezas.push({
        nodeCodigo: estudiante.nodeCodigo,
        nodeCalNotaDes: JSON.stringify(dataClob),
        matCodigo: estudiante.matCodigo,
        nodeEstado: 1,
        maasCodigo: this.cursoSeleccionadoChild.acaMallaAsignatura.maasCodigo,
      })
    })

    this.inicialService
      .guardarNotasDestrezas(notasDestrezas)
      .then(async (respuesta) => {
        this.showTable = false
        await this.analizarExisteInformacionNotasDestrezas(
          this.ambitoChild,
          this.listaEstudiantesNotasChild
        )
        Swal.fire("Éxito", "Información almacenada correctamente", "success")
      })
      .catch((error) => {
        console.error(error)
      })
  }

  async analizarExisteInformacionNotasDestrezas(
    ambito: number,
    estudiantes: any[]
  ) {
    let listaMatCodigoAlumnos: number[] = []

    if (estudiantes.length > 0) {

      estudiantes.forEach((estudiante: any) => {
        listaMatCodigoAlumnos.push(estudiante.matCodigo)
      })

      let data: IAlumnosMateriaAmbitoModeloDTO = {
        ambCodigo: ambito,
        modgenCodigo: this.modgenCodigoChild,
        maatCodigo: this.cursoSeleccionadoChild.acaMallaAsignatura.maasCodigo,
        estudiantes: listaMatCodigoAlumnos,
      }

      await this.inicialService
        .getAlumnosNotasPrepaModelo(data)
        .then(async (res) => {
          if (res.listado.length > 0) {
            await this.setearNotasEstudiantes(
              res.listado,
              this.listaEstudiantesNotasChild,
              this.listaDestrezasChild
            )
          } else {
            await this.setTableData(
              this.listaDestrezasChild,
              this.listaEstudiantesNotasChild
            )
          }
        })
        .catch((error) => {
          this.spinnerService.hide()
          console.error(error)
        })
    } else {
      Swal.fire(
        "Error",
        "No existen alumnos matriculados en este curso",
        "info"
      )
      this.spinnerService.hide()
    }
  }

  async setearNotasEstudiantes(
    estNotas: IAcaNotaDestreza[],
    estLista: any[],
    destrezas: any
  ) {
    this.showTable = true
    await this.setHeaderColumns(destrezas)
    let data: IDataPrepaDTO[] = []

    estNotas.forEach(async (estNota) => {
      let estudianteFind = estLista.find(
        (est) => est.matCodigo === estNota.matCodigo
      )
      let nodeCalNotaDes = JSON.parse(estNota.nodeCalNotaDes)

      data.push({
        nodeCodigo: estNota.nodeCodigo,
        estCodigo: estudianteFind?.estudiante.estCodigo,
        matCodigo: estNota.matCodigo,
        nombre: estudianteFind.estudiante.estNombres,
        destrezas: nodeCalNotaDes.destrezas.map(
          (destreza: Idestreza) => {
            return {
              desCodigo: destreza.desCodigo,
              resCodigo: null,
              resEscala: destreza.resEscala,
            }
          }
        ),
      })
    })
    this.dataFinalPrepa = data
    this.setTableNotas(this.dataFinalPrepa)
  }

  sortTableData(data: any[]) {
    return data.sort((a, b) => a.nombre.localeCompare(b.nombre))
  }

  filtrarModelo(data: IDataPrepaDTO[]) {
    const dataTemp = data.filter((d) => d.modgenCodigo === this.modgenCodigoChild)
    if (dataTemp.length > 0) {
      this.dataFinalPrepa = dataTemp
      this.setTableNotas(this.dataFinalPrepa)
    } else {
      this.dataFinalPrepa = data
      this.setBodyData(this.listaEstudiantesNotasChild, this.listaDestrezasChild)
    }
  }
}
