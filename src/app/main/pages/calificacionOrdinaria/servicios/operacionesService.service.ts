import { EventEmitter, Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class OperacionesService {

  
  public listaEstudiantesPrimertrimestre$ = new BehaviorSubject<any[]>([]);
  public listaEstudiantesSegundotrimestre$ = new BehaviorSubject<any[]>([]);
  public listaEstudiantesTercertrimestre$ = new BehaviorSubject<any[]>([]);

  
  constructor() { }


  ////Evento activar 
  public activarOninit = new EventEmitter<void>();
  
}