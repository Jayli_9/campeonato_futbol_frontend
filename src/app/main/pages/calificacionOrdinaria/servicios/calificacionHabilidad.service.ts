import { Injectable } from "@angular/core"
import { HttpClient } from "@angular/common/http"
import { environment } from "environments/environment"
import { AuthenticationService } from "../../../../auth/service/authentication.service"
import { ResponseGenerico } from "../interfaces/response-generico";

import { asginacionInterface } from "../interfaces/asignacionExtra";
import { asignacionModuloInterface } from "../interfaces/asignacionModulo";
import { distributivoInterface } from "../interfaces/distributivo";
import { distributivoListadoInterface } from "../interfaces/listadoDistributivo";
import { CatHabilidadInterface } from "../interfaces/CatHabilidad";
import { habilidadesInterface } from "../interfaces/habilidad";
import { HabInstInterface } from "../interfaces/habInst";
import { criteriosInterface } from "../interfaces/criterios";
import { ponderacionInterface } from '../interfaces/ponderacion';
import { CalifHabilidadesInterface } from '../interfaces/calificacionHabilidades';


import { servicioEducativoInterface } from "../interfaces/servicioEducativo";
import { MotiedInterface } from '../interfaces/motied';
import { gradoCatalogoInterface } from '../interfaces/gradoCatalogo';


import { informacionDocente } from "../interfaces/informacionDocente";


import { paraleloInterface } from "../interfaces/paralelo";
import { datosParalelosInterface } from "../interfaces/datosParalelo";
import { ofertaCursoInterface } from '../interfaces/ofertaCurso';
import { establecimientoInterface } from "../../reporteEstudiantes/interfaces/establecimineto";
import { habInstInterface } from "../../acomIntegralAula/interfaces/habInst";
import { Observable } from "rxjs";
import { MatriculaOrdinaria } from "../interfaces/matricula-ordinaria";
import { modeloEvaluacionInterface } from "../interfaces/modeloEvaluacion";
import { notaAsignaturaInterface } from "../interfaces/nota-Asignatura";

import { intervalosInterface } from "../interfaces/intervalos";

import { editNotaAsignaturaInterface } from "../interfaces/notaAsignaturaGuardar";
import { notaComportamientoInterface } from "../interfaces/notaComportamiento";


@Injectable({
    providedIn: 'root'
  })
  export class CalificacionHabilidadService{

    private readonly URL_REST = environment.url_academico;
    private readonly URL_MATRI = environment.url_matricula;

constructor(private _http: HttpClient) { }

buscarDistributivoExtraPorDocente(cod): Promise<ResponseGenerico<asginacionInterface>>{
  return new Promise((resolve, reject) => {
    this._http.get(`${environment.url_academico}/private/buscarDistributivoExtraPorDocente/`+cod).subscribe((response: ResponseGenerico<asginacionInterface>) => {
      resolve(response);
    }, reject);
  })
}

buscarDistributivoModuloPorDocente(cod): Promise<ResponseGenerico<asignacionModuloInterface>>{
  return new Promise((resolve, reject) => {
    this._http.get(`${environment.url_academico}/private/buscarDistributivoModuloPorDocente/`+cod).subscribe((response: ResponseGenerico<asignacionModuloInterface>) => {
      resolve(response);
    }, reject);
  })
}

buscarDistributivoPorCodIdentificacion(cod): Promise<ResponseGenerico<distributivoInterface>>{
  return new Promise((resolve, reject) => {
    this._http.get(`${environment.url_academico}/private/buscarDistributivoModuloPorDocente/`+cod).subscribe((response: ResponseGenerico<distributivoInterface>) => {
      resolve(response);
    }, reject);
  })
}


listarDistributivos(): Promise<ResponseGenerico<distributivoListadoInterface>>{
  return new Promise((resolve, reject) => {
    this._http.get(`${environment.url_academico}/private/listarTodosLosDistributivos`).subscribe((response: ResponseGenerico<distributivoListadoInterface>) => {
      resolve(response);
    }, reject);
  })
}


listarCatHabilidades(): Promise<ResponseGenerico<CatHabilidadInterface>>{
  return new Promise((resolve, reject) => {
    this._http.get(`${environment.url_academico}/private/listaCatHabilidades`).subscribe((response: ResponseGenerico<CatHabilidadInterface>) => {
      resolve(response);
    }, reject);
  })
}


BuscarHabilidadesPorCatHabilidades(cod): Promise<ResponseGenerico<habilidadesInterface>>{
  return new Promise((resolve, reject) => {
    this._http.get(`${environment.url_academico}/private/buscarHabilidadesPorCodigoCatHabilidad/`+cod).subscribe((response: ResponseGenerico<habilidadesInterface>) => {
      resolve(response);
    }, reject);
  })
}


listarHabilidades():Promise<ResponseGenerico<habilidadesInterface>>{
  return new Promise((resolve, reject) => {
    this._http.get(`${environment.url_academico}/private/listarHabilidades`).subscribe((response: ResponseGenerico<habilidadesInterface>) => {
      resolve(response);
    }, reject);
  })
}

buscarHabilidadesPorCodigo(cod): Promise<ResponseGenerico<habilidadesInterface>>{
  return new Promise((resolve, reject) => {
    this._http.get(`${environment.url_academico}/private/buscarHabilidadesPorCodigo/`+cod).subscribe((response: ResponseGenerico<habilidadesInterface>) => {
      resolve(response);
    }, reject);
  })
}


buscarHabInstPorCodigoHabilidad(cod): Promise<ResponseGenerico<HabInstInterface>>{
  return new Promise((resolve, reject) => {
    this._http.get(`${environment.url_academico}/private/buscarHabInstPorCodigoHabilidad/`+cod).subscribe((response: ResponseGenerico<HabInstInterface>) => {
      resolve(response);
    }, reject);
  })
}


listarCriterios(): Promise<ResponseGenerico<criteriosInterface>>{
  return new Promise((resolve, reject) => {
    this._http.get(`${environment.url_academico}/private/listarCriterios`).subscribe((response: ResponseGenerico<criteriosInterface>) => {
      resolve(response);
    }, reject);
  })
}




listarDistributivo(): Promise<ResponseGenerico<distributivoInterface>>{
  return new Promise((resolve, reject) => {
    this._http.get(`${environment.url_academico}/private/listarDistributivosActivos`).subscribe((response: ResponseGenerico<distributivoInterface>) => {
      resolve(response);
    }, reject);
  })
}


listaPonderacion(): Promise<ResponseGenerico<ponderacionInterface>>{
  return new Promise((resolve, reject) => {
    this._http.get(`${environment.url_academico}/private/listarPonderacion`).subscribe((response: ResponseGenerico<ponderacionInterface>) => {
      resolve(response);
    }, reject);
  })
}

buscarPonderacionPorCodigo(cod): Promise<ResponseGenerico<ponderacionInterface>>{
  return new Promise((resolve, reject) => {
    this._http.get(`${environment.url_academico}/private/buscarPonderacionPorCodigo/`+cod).subscribe((response: ResponseGenerico<ponderacionInterface>) => {
      resolve(response);
    }, reject);
  })
}


listaHabInst(): Promise<ResponseGenerico<HabInstInterface>>{
  return new Promise((resolve, reject) => {
    this._http.get(`${environment.url_academico}/private/listarHabInst`).subscribe((response: ResponseGenerico<HabInstInterface>) => {
      resolve(response);
    }, reject);
  })
}


guardarCriteriosHabilidad(parametro: any){
  return this._http.post<CalifHabilidadesInterface>(`${environment.url_academico}/private/registrarCalifCriterios`, parametro);
}

editarCriteriorHabilidad(parametro: any){
  return this._http.post<CalifHabilidadesInterface>(`${environment.url_academico}/private/actualizarCalifCriterios`, parametro);
}


guardarNotaAsignatura(parametro: any){
  return this._http.post<editNotaAsignaturaInterface>(`${environment.url_academico}/private/guardarNotaAsignatura`, parametro);
}



buscarServicioEducativoPorCodigo(cod): Promise<ResponseGenerico<servicioEducativoInterface>>{
    return new Promise((resolve, reject) => {
      this._http.get(`${environment.url_catalogo}/private/buscarServicioEducativoPorCodigo/`+cod).subscribe((response: ResponseGenerico<servicioEducativoInterface>) => {
        resolve(response);
      }, reject);
    })
  }
  
  buscarMotiedPorCod(cod): Promise<ResponseGenerico<MotiedInterface>>{
    return new Promise((resolve, reject) => {
  this._http.get(`${environment.url_catalogo}/private/buscarModalidadTipoEducacionPorId/`+ cod).subscribe((response: ResponseGenerico<MotiedInterface>) => {
     resolve(response);
   }, reject);
  })
  }
  
  
  buscarGradoPorCodigo(cod): Promise<ResponseGenerico<gradoCatalogoInterface>>{
    return new Promise((resolve, reject) => {
  this._http.get(`${environment.url_catalogo}/private/buscarCatalogoGradosPorCodigo/`+ cod).subscribe((response: ResponseGenerico<gradoCatalogoInterface>) => {
     resolve(response);
   }, reject);
  })
  }


  buscarCodigoDatosDocente(cod): Promise<ResponseGenerico<informacionDocente>>{
    return new Promise((resolve, reject) => {
      this._http.get(`${environment.url_docente}/private/docenteHispanaPorIdentificacionYEstado/`+cod).subscribe((response: ResponseGenerico<informacionDocente>) => {
        resolve(response);
      }, reject);
    })
  }


  buscarCodigoCursoPorCodigo(cod): Promise<ResponseGenerico<paraleloInterface>>{
    return new Promise((resolve, reject) => {
      this._http.get(`${environment.url_oferta}/private/buscarCursoParaleloPorCodigo/`+cod).subscribe((response: ResponseGenerico<paraleloInterface>) => {
        resolve(response);
      }, reject);
    })
  }
  
  
  buscarCodigoDatosParalelos(cod): Promise<ResponseGenerico<datosParalelosInterface>>{
    return new Promise((resolve, reject) => {
      this._http.get(`${environment.url_oferta}/private/buscarParaleloPorCodigo/`+cod).subscribe((response: ResponseGenerico<datosParalelosInterface>) => {
        resolve(response);
      }, reject);
    })
  }
  
  
  buscarCursoPorCodigo(cod): Promise<ResponseGenerico<ofertaCursoInterface>>{
    return new Promise((resolve, reject) => {
      this._http.get(`${environment.url_oferta}/private/buscarCursoPorCodigo/`+cod).subscribe((response: ResponseGenerico<ofertaCursoInterface>) => {
        resolve(response);
      }, reject);
    })
  }


  buscarEstablecimientoPorCodigo(cod):  Promise<ResponseGenerico<establecimientoInterface>>{
    return new Promise((resolve, reject) => {
      this._http.get(`${environment.url_institucion}/private/listarEstablecimientosPorAmie/`+cod).subscribe((response: ResponseGenerico<establecimientoInterface>) => {
        resolve(response);
      }, reject);
    })
  }


  buscarHabilidadesinstitucionPorEstablecimiento(cod): Promise<ResponseGenerico<habInstInterface>>{
    return new Promise((resolve, reject) => {
      this._http.get(`${environment.url_academico}/private/buscarHabInstPorInsestCodgio/`+cod).subscribe((response: ResponseGenerico<habInstInterface>) => {
        resolve(response);
      }, reject);
    })
  }

  buscarCriteriorPorHabilidad(cod): Promise<ResponseGenerico<criteriosInterface>>{
    return new Promise((resolve, reject) => {
      this._http.get(`${environment.url_academico}/private/buscarCriteriosHabCodigo/`+cod).subscribe((response: ResponseGenerico<criteriosInterface>) => {
        resolve(response);
      }, reject);
    })
  }

  listarCriteriorHabilidades(): Promise<ResponseGenerico<criteriosInterface>>{
    return new Promise((resolve, reject) => {
      this._http.get(`${environment.url_academico}/private/listarCriterios`).subscribe((response: ResponseGenerico<criteriosInterface>) => {
        resolve(response);
      }, reject);
    })
  }


  listaMatriculasPorRegimenAnioLectivoYParalelo(reanleCodigo:number,paraleloCodigo:number): Observable<any> | undefined {
    let url_ws=`${this.URL_MATRI}/private/listaMatriculasPorRegimenAnioLectivoYParalelo/${reanleCodigo}/${paraleloCodigo}`;
  return this._http.get<ResponseGenerico<MatriculaOrdinaria>>(url_ws);
  }


  listarModelaEvaluacion(): Promise<ResponseGenerico<modeloEvaluacionInterface>>{
    return new Promise((resolve, reject) => {
      this._http.get(`${environment.url_academico}/private/listarTodosModevaGen`).subscribe((response: ResponseGenerico<modeloEvaluacionInterface>) => {
        resolve(response);
      }, reject);
    })
  }


  buscarCalificacionHabilidadPorMatricula(cod): Promise<ResponseGenerico<CalifHabilidadesInterface>>{
    return new Promise((resolve, reject) => {
      this._http.get(`${environment.url_academico}/private/buscarCalifCriteriosPorMatCodigo/`+cod).subscribe((response: ResponseGenerico<CalifHabilidadesInterface>) => {
        resolve(response);
      }, reject);
    })
  }


  buscarNotaAsignaturaPorMatricula(cod): Promise<ResponseGenerico<notaAsignaturaInterface>>{
    return new Promise((resolve, reject) => {
      this._http.get(`${environment.url_academico}/private/buscarNotaAsignaturaPorMatricula/`+cod).subscribe((response: ResponseGenerico<notaAsignaturaInterface>) => {
        resolve(response);
      }, reject);
    })
  }


  listarIntervalos():  Promise<ResponseGenerico<intervalosInterface>>{
    return new Promise((resolve, reject) => {
      this._http.get(`${environment.url_academico}/private/listarIntervalos`).subscribe((response: ResponseGenerico<intervalosInterface>) => {
        resolve(response);
      }, reject);
    })
  }


  buscarNotaComportamientoPorMatricula(cod): Promise<ResponseGenerico<notaComportamientoInterface>>{
    return new Promise((resolve, reject) => {
      this._http.get(`${environment.url_academico}/private/buscarNotaComportamientoPorMatricula/`+cod).subscribe((response: ResponseGenerico<notaComportamientoInterface>) => {
        resolve(response);
      }, reject);
    })
  }


  editarNotaComportamiento(parametro: any){
    return this._http.post<notaComportamientoInterface>(`${environment.url_academico}/private/actualizarNotaComportamientoTotal`, parametro);
  }
  
  
  guardarNotaComportamiento(parametro: any){
    return this._http.post<notaComportamientoInterface>(`${environment.url_academico}/private/guardarNotaComportamientoTotal`, parametro);
  }


}