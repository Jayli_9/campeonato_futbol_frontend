import { Injectable } from "@angular/core"
import { HttpClient } from "@angular/common/http"
import { environment } from "environments/environment"
import { AuthenticationService } from "../../../../auth/service/authentication.service"

//exportacion interfaces
import { ResponseGenerico } from "../../asignacionDocentes/interface/response-generico"
import { IAcaDestreza } from "../interfaces/IacaDestreza"
import { IAcaAmbito } from "../interfaces/IacaAmbito"
import { IAcaNotaDestreza } from "../interfaces/IAcaNotaDestreza"
import { IParametrizacionDTO } from "../interfaces/IParametrizacionDTO"
import { IAcaParamCalPreDTO } from "../interfaces/IAcaParamCalPreDTO"
import { IAlumnosMateriaAmbitoModeloDTO } from "../interfaces/IAlumnosMateriaAmbitoModeloDTO"

@Injectable({
  providedIn: "root",
})
export class InicialService {
  constructor(
      private _http: HttpClient,
      private _authService: AuthenticationService
  ) {}

  //obtener lista de destrezas por Ambito;
  getDestrezasPorAmbito(
      ambCodigo: number
  ): Promise<ResponseGenerico<IAcaDestreza>> {
    return new Promise((resolve, reject) => {
      this._http
          .get(
              `${environment.url_academico}/private/listarDestrezasPorAmbCodigo/${ambCodigo}`
          )
          .subscribe((response: ResponseGenerico<IAcaDestreza>) => {
            resolve(response)
          }, reject)
    })
  }

  //obtener lista de Ambitos por Apre codigo;
  getAmbitosPorApreCodigo(
      apreCodigo: number
  ): Promise<ResponseGenerico<IAcaAmbito>> {
    return new Promise((resolve, reject) => {
      this._http
          .get(
              `${environment.url_academico}/private/listarAmbitosPorApreCodigo/${apreCodigo}`
          )
          .subscribe((response: ResponseGenerico<IAcaAmbito>) => {
            resolve(response)
          }, reject)
    })
  }

  //guardar notas destrezas
  guardarNotasDestrezas(
      notasDestrezas: IAcaNotaDestreza[]
  ): Promise<ResponseGenerico<string>> {
    return new Promise((resolve, reject) => {
      this._http
          .post(
              `${environment.url_academico}/private/guardarNotasDestrezas`,
              notasDestrezas
          )
          .subscribe((response: ResponseGenerico<string>) => {
            resolve(response)
          }, reject)
    })
  }

  getParamCalPre(
      parametros: IParametrizacionDTO
  ): Promise<ResponseGenerico<IAcaParamCalPreDTO>> {
    return new Promise((resolve, reject) => {
      this._http
          .post(
              `${environment.url_academico}/private/listarParamCalPreReanleCodigoYModgenCodigo`,
              parametros
          )
          .subscribe((response: ResponseGenerico<IAcaParamCalPreDTO>) => {
            resolve(response)
          })
    })
  }

  async getAlumnosNotasPrepa(
      data: IAlumnosMateriaAmbitoModeloDTO
  ): Promise<ResponseGenerico<IAcaNotaDestreza>> {
    return new Promise((resolve, reject) => {
      this._http
          .post(
              `${environment.url_academico}/private/listarNotasDestrezasPorAlumnosYAmbitoSinModelo`,
              data
          )
          .subscribe((response: ResponseGenerico<IAcaNotaDestreza>) => {
            resolve(response)
          })
    })
  }

  async getAlumnosNotasPrepaModelo(
      data: IAlumnosMateriaAmbitoModeloDTO
  ): Promise<ResponseGenerico<IAcaNotaDestreza>> {
    return new Promise((resolve, reject) => {
      this._http
          .post(
              `${environment.url_academico}/private/listarNotasDestrezasPorAlumnosYAmbito`,
              data
          )
          .subscribe((response: ResponseGenerico<IAcaNotaDestreza>) => {
            resolve(response)
          })
    })
  }
}
