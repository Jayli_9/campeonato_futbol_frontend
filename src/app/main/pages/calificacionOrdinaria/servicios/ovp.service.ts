import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {AuthenticationService} from '../../../../auth/service';
import {ResponseGenerico} from '../../asignacionDocentes/interface/response-generico';
import {environment} from '../../../../../environments/environment';
import {IAcaNotaOvp} from '../interfaces/IAcaNotaOvp';
import {AcaEjeGrado} from '../../eje-grado/interfaces/aca-eje-grado';
import {IAlumnoMeteriaEjeModeloDTO} from '../interfaces/IAlumnoMeteriaEjeModeloDTO';
import {EjeGrado} from '../../eje-grado/interfaces/eje-grado';

@Injectable({
    providedIn: 'root'
})
export class OvpService {
    constructor(
        private _http: HttpClient,
        private _authService: AuthenticationService
    ) {
    }

    //obtener lista de eje grado por eje
    getEjeGradoPorEjeSelecionado(
        ejgrCodigo: number
    ): Promise<ResponseGenerico<EjeGrado>> {
        return new Promise((resolve, reject) => {
            this._http
                .get(
                    `${environment.url_academico}/private/buscarEjeGradoDTOPorCodigo/${ejgrCodigo}` // hacer back
                )
                .subscribe((response: ResponseGenerico<EjeGrado>) => {
                    resolve(response);
                }, reject);
        });
    }

    getEjeGradosPorGradoPorReanleCodigo(graCodigo: number, reanleCodigo: number): Promise<ResponseGenerico<AcaEjeGrado>> {
        return new Promise((resolve, reject) => {
            this._http
                .get(
                    `${environment.url_academico}/private/listarEjeGradosPorGradoPorReanleCodigo/${graCodigo}/${reanleCodigo}`
                )
                .subscribe((response: ResponseGenerico<AcaEjeGrado>) => {
                    resolve(response);
                }, reject);
        });
    }

    //guardar notas ovp
    guardarNotasOvp(
        notasOvp: IAcaNotaOvp[]
    ): Promise<ResponseGenerico<string>> {
        return new Promise((resolve, reject) => {
            this._http
                .post(
                    `${environment.url_academico}/private/guardarNotasOvp`,
                    notasOvp
                )
                .subscribe((response: ResponseGenerico<string>) => {
                    resolve(response);
                }, reject);
        });
    }

    async getAlumnosNotasOvp(
        data: IAlumnoMeteriaEjeModeloDTO
    ): Promise<ResponseGenerico<IAcaNotaOvp>> {
        return new Promise((resolve, reject) => {
            this._http
                .post(
                    `${environment.url_academico}/private/listarNotasEjeGradoPorAlumnosYEje`,
                    data
                )
                .subscribe((response: ResponseGenerico<IAcaNotaOvp>) => {
                    resolve(response);
                });
        });
    }
}
