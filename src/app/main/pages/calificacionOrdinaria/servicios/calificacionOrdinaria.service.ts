import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'environments/environment';
import { AuthenticationService } from '../../../../auth/service/authentication.service';

//exportacion interfaces
import { ResponseGenerico } from '../../asignacionDocentes/interface/response-generico';
import { IacaDistributivo } from '../interfaces/IacaDistributivo';
import { IofeCursoParalelo } from '../../asignacionDocentesOrdinaria/interfaces/IofeCursoParalelo';
import { IofeCurso } from '../../asignacionDocentesOrdinaria/interfaces/IofeCurso';
import { IofeParalelo } from '../interfaces/IofeParalelo';
import { IcatGrado } from '../interfaces/IcatGrado';
import { IinsInstitucion } from '../interfaces/IinsInstitucion';
import { IacaModevaGen } from '../interfaces/IacaModevaGen';
import { IacaCalificacion } from '../interfaces/IacaCalificacion';
import { IacaParametrizacionCalpre } from '../interfaces/IacaParametrizacionCalpre';
import { IcatRangoNota } from '../interfaces/IcatRangoNota';
import { IacaRangoSupletorio } from '../interfaces/IacaRangoSupletorio';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { RangoOvp } from '../interfaces/Rango-Ovp';
import { IacaCalifNivel } from '../interfaces/IacaCalifNivel';

@Injectable({
  providedIn: 'root'
})
export class CalificacionOrdinariaService {

  constructor(private _http: HttpClient, private _authService: AuthenticationService) { }


  //obtener lista de distributivo;
  getDistributivo(): Promise<ResponseGenerico<IacaDistributivo>> {
    return new Promise((resolve, reject) => {
      this._http.get(`${environment.url_academico}/private/listarTodosLosDistributivos`).subscribe((response: ResponseGenerico<IacaDistributivo>) => {
        resolve(response);
      }, reject);
    })
  }

  //obtener lista de distributivo;
  getDistributivoPorDto(docCodigo, reanleCodigo, insestCodigo): Promise<ResponseGenerico<IacaDistributivo>> {
    let objetoConsulta = {
      docCodigo: docCodigo,
      reanleCodigo: reanleCodigo,
      insestCodigo: insestCodigo
    }
    return new Promise((resolve, reject) => {
      this._http.post(`${environment.url_academico}/private/listarDistributivosPorDocenteRegimenAnioLectivoYInstitucionEstablecimiento`, objetoConsulta).subscribe((response: ResponseGenerico<IacaDistributivo>) => {
        resolve(response);
      }, reject);
    })
  }

  //obtener lista de distributivo;
  getDistributivoPorDtoPaginado(docCodigo, reanleCodigo, insestCodigo, itemsPagina, numPagina): Promise<ResponseGenerico<IacaDistributivo>> {
    let objetoConsulta = {
      docCodigo: docCodigo,
      reanleCodigo: reanleCodigo,
      insestCodigo: insestCodigo,
      numPagina: numPagina,
      itemsPagina: itemsPagina
    }
    return new Promise((resolve, reject) => {
      this._http.post(`${environment.url_academico}/private/listarDistributivosPorDocenteRegimenAnioLectivoYInstitucionEstablecimientoPaginado`, objetoConsulta).subscribe((response: ResponseGenerico<IacaDistributivo>) => {
        resolve(response);
      }, reject);
    })
  }

  //obtener modelo evaluación ´por lista reanleCodigo
  getObtenerModelosEvaluacionGeneralPorLitaReganlecYEstado(listaReanleCodigo: []): Promise<ResponseGenerico<IacaModevaGen>> {
    let objetoConsulta = {
      listaReanleCodigo: listaReanleCodigo
    }
    return new Promise((resolve, reject) => {
      this._http.post(`${environment.url_academico}/private/listarModevaGenPorListaReganlecYEstado`, objetoConsulta).subscribe((response: ResponseGenerico<IacaModevaGen>) => {
        resolve(response);
      }, reject);
    })
  }
  //obtener modelo evaluación por  reanleCodigo
  getObtenerModelosEvaluacionGeneralPorReanlecYEstado(reanleCodigo: number): Promise<ResponseGenerico<IacaModevaGen>> {
    return new Promise((resolve, reject) => {
      this._http.get(`${environment.url_academico}/private/obtienerModevaGenPorReanleCodigo/${reanleCodigo}`).subscribe((response: ResponseGenerico<IacaModevaGen>) => {
        resolve(response);
      }, reject);
    })
  }

  //obtener  curso Paralelo por curparCodigo
  getCursoParaleloPorCurparCodigo(curparCodigo): Promise<ResponseGenerico<IofeCursoParalelo>> {
    return new Promise((resolve, reject) => {
      this._http.get(`${environment.url_oferta}/private/buscarCursoParaleloPorCodigo/` + curparCodigo).subscribe((response: ResponseGenerico<IofeCursoParalelo>) => {
        resolve(response);
      }, reject);
    })
  }
  //obtener   Paralelo por parCodigo
  getParaleloPorParCodigo(parCodigo): Promise<ResponseGenerico<IofeParalelo>> {
    return new Promise((resolve, reject) => {
      this._http.get(`${environment.url_oferta}/private/buscarParaleloPorCodigo/` + parCodigo).subscribe((response: ResponseGenerico<IofeParalelo>) => {
        resolve(response);
      }, reject);
    })
  }
  //obtener  curso  por curCodigo
  getCursoPorCurCodigo(curCodigo): Promise<ResponseGenerico<IofeCurso>> {
    return new Promise((resolve, reject) => {
      this._http.get(`${environment.url_oferta}/private/buscarCursoPorCodigo/` + curCodigo).subscribe((response: ResponseGenerico<IofeCurso>) => {
        resolve(response);
      }, reject);
    })
  }
  //obtener  grado  por graCodigo
  getGradoPorGraCodigo(graCodigo): Promise<ResponseGenerico<IcatGrado>> {
    return new Promise((resolve, reject) => {
      this._http.get(`${environment.url_catalogo}/private/buscarCatalogoGradosPorCodigo/` + graCodigo).subscribe((response: ResponseGenerico<IcatGrado>) => {
        resolve(response);
      }, reject);
    })
  }
  //obtener  institucion por Amie
  getIntitucionPorAmie(codAmie): Promise<ResponseGenerico<IinsInstitucion>> {
    return new Promise((resolve, reject) => {
      this._http.get(`${environment.url_institucion}/private/buscarInstitucionPorAmie/` + codAmie).subscribe((response: ResponseGenerico<IinsInstitucion>) => {
        resolve(response);
      }, reject);
    })
  }
  //obtener  calificaciones por modgenCodigo
  getCalificacionPorModgenCodigo(modgenCodigo): Promise<ResponseGenerico<IacaCalificacion>> {
    return new Promise((resolve, reject) => {
      this._http.get(`${environment.url_academico}/private/listarCalificacionPorEstadoAndModgenCodigo/` + modgenCodigo).subscribe((response: ResponseGenerico<IacaCalificacion>) => {
        resolve(response);
      }, reject);
    })
  }
  //obtener  rango supletorio
  getRangoSupletorio(): Promise<ResponseGenerico<IacaRangoSupletorio>> {
    return new Promise((resolve, reject) => {
      this._http.get(`${environment.url_academico}/private/listarRangoSupletorioActivos`).subscribe((response: ResponseGenerico<IacaRangoSupletorio>) => {
        resolve(response);
      }, reject);
    })
  }
  //obtener  rango supletorio por tivaCodigo
  getRangoSupletorioPorTivaCodigo(tivaCodigo: number): Promise<ResponseGenerico<IacaRangoSupletorio>> {
    return new Promise((resolve, reject) => {
      this._http.get(`${environment.url_academico}/private/listarRangoSupletorioActivosPorCodigoTipoValoracion/${tivaCodigo}`).subscribe((response: ResponseGenerico<IacaRangoSupletorio>) => {
        resolve(response);
      }, reject);
    })
  }
  //obtener  getParametrizacionCalPreActivos
  getParametrizacionCalPreActivos(): Promise<ResponseGenerico<IacaParametrizacionCalpre>> {
    return new Promise((resolve, reject) => {
      this._http.get(`${environment.url_academico}/private/listarParametrizacionCalPreActivos`).subscribe((response: ResponseGenerico<IacaParametrizacionCalpre>) => {
        resolve(response);
      }, reject);
    })
  }
  //obtener  matricula estudiantes ordinario
  getParametrizacionCalPrePorReanleCodigoYSereduCodigo(reanleCodigo: number, sereduCodigo: number): Promise<ResponseGenerico<IacaParametrizacionCalpre>> {
    let objetoConsulta = {
      reanleCodigo: reanleCodigo,
      sereduCodigo: sereduCodigo
    }
    return new Promise((resolve, reject) => {
      this._http.post(`${environment.url_academico}/private/listarParamCalPreReanleCodigoYSereduCodigo`, objetoConsulta).subscribe((response: ResponseGenerico<IacaParametrizacionCalpre>) => {
        resolve(response);
      }, reject);
    })
  }
  //obtener  getParametrizacionCalPreActivos
  getMatriculaOrdinariaPorReanleCodigoAndCurCodigo(reanleCodigo, curCodigo): Promise<ResponseGenerico<any>> {
    return new Promise((resolve, reject) => {
      this._http.get(`${environment.url_matricula}/private/listaMatriculaOrdinariaEstudiante/${reanleCodigo}/${curCodigo}`).subscribe((response: ResponseGenerico<any>) => {
        resolve(response);
      }, reject);
    })
  }
  //obtener  matricula estudiantes ordinario
  getMatriculaOrdinariaObjetoConsulta(reanleCodigo, curCodigo, curparCodigo, sereduCodigo): Promise<ResponseGenerico<any>> {
    let objetoConsulta = {
      curCodigo: curCodigo,
      curparCodigo: curparCodigo,
      reanleCodigo: reanleCodigo,
      sereduCodigo: sereduCodigo
    }
    return new Promise((resolve, reject) => {
      this._http.post(`${environment.url_matricula}/private/listarMatriculaOrdinariaPorCursoYCursoParaleloYRegimenAnioLectivoyServicioEducativo`, objetoConsulta).subscribe((response: ResponseGenerico<any>) => {
        resolve(response);
      }, reject);
    })
  }
  getNotaAsignaturaObjetoConsulta(maasCodigo, matCodigo): Promise<ResponseGenerico<any>> {
    let objetoConsulta = {
      acaMallaAsignatura: maasCodigo,
      matCodigo: matCodigo
    }
    return new Promise((resolve, reject) => {
      this._http.post(`${environment.url_academico}/private/buscarNotaAsignaturaPorMallaAsignaturaYMatricula`, objetoConsulta).subscribe((response: ResponseGenerico<any>) => {
        resolve(response);
      }, reject);
    })
  }
  getGuardarNotaAsignaturaObjetoConsulta(notaAsignaturaDTO): Promise<ResponseGenerico<any>> {
    return new Promise((resolve, reject) => {
      this._http.post(`${environment.url_academico}/private/guardarNotaAsignatura`, notaAsignaturaDTO).subscribe((response: ResponseGenerico<any>) => {
        resolve(response);
      }, reject);
    })
  }
  //obtener  rango de calificacion por reanleCodigo
  getRangoNotaPorReanleCodigo(reanleCodigo): Promise<ResponseGenerico<IcatRangoNota>> {
    return new Promise((resolve, reject) => {
      this._http.get(`${environment.url_catalogo}/private/obtenerRangoNotaPorReanleCodigoYEstado/` + reanleCodigo).subscribe((response: ResponseGenerico<IcatRangoNota>) => {
        resolve(response);
      }, reject);
    })
  }
  //obtener  califNivel por graCodigo y reanleCodigo
  getCalifNivelPorGraCodigoAndReanleCodigo(graCodigo: number, reanleCodigo: number): Promise<ResponseGenerico<IacaCalifNivel>> {
    let objetoConsulta = {
      reanleCodigo: reanleCodigo,
      graCodigo: graCodigo
    }
    return new Promise((resolve, reject) => {
      this._http.post(`${environment.url_academico}/private/buscarCalificacionNivelPorGraCodigoAndReanleCodigo`, objetoConsulta).subscribe((response: ResponseGenerico<IacaCalifNivel>) => {
        resolve(response);
      }, reject);
    })
  }
  getConsultarRangoEscalasObjetoConsulta(resAgrupacion: string, graCodigo: number): Promise<ResponseGenerico<any>> {
    let objetoConsulta = {
      resAgrupacion: resAgrupacion,
      graCodigo: graCodigo
    }
    return new Promise((resolve, reject) => {
      this._http.post(`${environment.url_academico}/private/listaRangoEscalasPorGradoYAgrupacion`, objetoConsulta).subscribe((response: ResponseGenerico<any>) => {
        resolve(response);
      }, reject);
    })
  }
  getObtenerRangoNotaPorReanleCodigoYEstado(reanleCodigo: number): Promise<ResponseGenerico<any>> {
    return new Promise((resolve, reject) => {
      this._http.get(`${environment.url_academico}/private/obtenerRangoNotaPorReanleCodigoYEstado/${reanleCodigo}`).subscribe((response: ResponseGenerico<any>) => {
        resolve(response);
      }, reject);
    })
  }

  getObtenerCoeficientePorAreaConocimientoPorReanleCodigo(arcoCodigo: number, reanleCodigo: number): Observable<ResponseGenerico<any>> {
    return this._http.get(`${environment.url_academico}/private/buscarCoeficientePorAreaConocimientoPorReanleCodigo/${arcoCodigo}/${reanleCodigo}`).pipe(
      map((response: ResponseGenerico<any>) => response)
    );
  }

  getObtenerValorCualitativoOVP(reanleCodigo: number, valor: number): Observable<ResponseGenerico<RangoOvp>> {
    return this._http.get(`${environment.url_academico}/private/getObtenerValorCualitativoOVP/${reanleCodigo}/${valor}`).pipe(
      map((response: ResponseGenerico<RangoOvp>) => response)
    );
  }

}
