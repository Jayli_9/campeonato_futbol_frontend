import { Routes } from '@angular/router';
import { CalificacionPrincipalComponent } from '../componentes/calificacion-principal/calificacion-principal.component';
import { PrimeraNotaComponent } from '../componentes/calificacion-principal/primera-nota/primera-nota.component';
import { SegundaNotaComponent } from '../componentes/calificacion-principal/segunda-nota/segunda-nota.component';
import { ListaEstudiantesHabilidadesComponent } from "../componentes/calificacion-principal/lista-estudiantes-habilidades/lista-estudiantes-habilidades.component";
import { CaliHabilidadesComponent } from "../componentes/calificacion-principal/cali-habilidades/cali-habilidades.component";
import { AuthGuard } from 'app/auth/helpers';

export const RUTA_CALIFICACION_ORDINARIO: Routes = [
  {
    path: 'calificacion_ordinaria',
    component: CalificacionPrincipalComponent,
    canActivate:[AuthGuard]
  },
  {
    path: 'primera_nota',
    component: PrimeraNotaComponent,
    canActivate:[AuthGuard]
  },
  {
    path: 'segunda_nota',
    component: SegundaNotaComponent,
    canActivate:[AuthGuard]
  },
  {
    path: 'listaEstudianteHabilidad',
    component: ListaEstudiantesHabilidadesComponent,
    canActivate:[AuthGuard]
  },
  {
    path: 'calificacionHabilidad',
    component: CaliHabilidadesComponent,
    canActivate:[AuthGuard]
  }
];