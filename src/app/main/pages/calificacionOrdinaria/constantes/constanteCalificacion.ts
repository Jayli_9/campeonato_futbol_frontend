export class constanteCalificacion {
    // public static modgenCodigoSupletorio = 13;
    // public static modgenCodigoNotaFinal = 14;
    // public static modgenCodigoEvalucacionSubnivel = 12;
    // public static modgenCodigoNotaTrimestral = 11;
    // public static modgenCodigoNotaTrimestral1 = 8;
    // public static modgenCodigoNotaTrimestral2 = 9;
    // public static modgenCodigoNotaTrimestral3 = 10;

    //niveles para los calculos agrupados
    public static modgenNivelParciales = 3;
    public static modgenNivelPromedios = 2;

    //suppletorio valor minimo
    public static valorminimoSupletorio = 7;
    // nota ponderada costa
    // public static modgenCodigoNotaPonderada=15;

    //Nemonicos para calculos promedios
    public static nemonicoNotaTrimestral="NTRI"
    public static nemonicoEvaluacionFinal="EVSNIV"
    public static nemonicoSupletorio="SUPLE"
    public static nemonicoNotaFinal="NOTFI"
    public static nemonicoNotaPonderada="PP"
}
