export interface habilidadesInterface{
       habCodigo: number;
       cthCodigo: number;
       habDescipcion: string;
       habPredeterminado: number;
       habEstado: number;
}