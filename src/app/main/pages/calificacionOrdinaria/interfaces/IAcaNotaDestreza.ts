export interface IAcaNotaDestreza {
  nodeCodigo?: number
  nodeCalNotaDes: string
  matCodigo: number
  nodeEstado: number
  maasCodigo: number
}
