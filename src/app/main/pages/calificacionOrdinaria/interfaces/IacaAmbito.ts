export interface IAcaAmbito{
    ambCodigo: number;
    apreCodigo: number;
    ambDescripcion: string;
    ambEstado: number;
}