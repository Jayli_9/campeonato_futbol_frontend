export interface IacaCalifNivel{
    calCodigo: number,
    calDescripcion: string,
    calnivCodigo: number,
    calnivEstado: number,
    graCodigo: number,
    modgenCodigo: number,
    nivCodigo: number,
    nivDescripcion: string,
    reanleCodigo: number,
    calnivActivo: number
}