export interface servicioEducativoInterface{
     sereduCodigo: number;
     sereduEstado: number;
     motiedCodigo: number;
     sereduVigente: string;
     reanleCodigo: number;
     codigoRegimen: number;
     codigoAnioLectivo: number;
     codigoModalidadTipoEducacion: number;
     codigoModalidad: number;
     codigoTipoEducacion: number;
     codigoPrograma: number;
     nombrePrograma: string;
     tipeduNombre: number;
     modalidad: number;
     modCodigo: number;
     impClasificacion: null;
     impDescripcion: null;
}