export interface datosParalelosInterface{
           parCodigo: number;
           parDescripcion: string;
           parEstado: number;
           parFechaCreacion: Date;
}