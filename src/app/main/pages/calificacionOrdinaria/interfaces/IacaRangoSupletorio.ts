export interface IacaRangoSupletorio {
    rasuCodigo: number;
    rasuEstado: number;
    rasuMax: string;
    rasuMin: string;
    rasuPromedioFinal: number;
    reanleCodigo: number;
}