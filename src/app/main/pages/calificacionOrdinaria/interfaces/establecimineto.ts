export interface establecimientoInterface{
     insestCodigo: number;
      insInstitucion: {
         insCodigo: number;
         insDescripcion: string;
         insEstado: number;
         insAmie: string;
         insLogo: null,
         insEslogan: null,
         sosCodigo: number;
         denCodigo: number;
         jurCodigo: number;
         insFechaCreacion: number;
         insDireccion: null,
         tipinsCodigo: null,
         insFechaResolucion: null,
         regimenes: null,
         inDescripcion: string;
      },
       insestEstado: number;
       insestDireccion: string;
       insestCoordenadaX: string;
       insestCoordenadaY: string;
       parCodigo: number;
       disCodigo: number;
       insestGeocodigo: string;
       insTipoEstablecimiento: {
         tipEstCodigo: number;
         tipEstDescripcion: string;
         tipEstEstado: number;
         tipEstFechaCreacion: Date
      },
       insestFechaCreacion: Date;
       nombreTipo: string;
}