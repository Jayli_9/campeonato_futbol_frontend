export interface intervalosInterface{
       intCodigo: number;
       intNemonico: string;
       intInicio: number;
       intFin: number;
       intDescripcion: string;
       intEstdado: number;
}