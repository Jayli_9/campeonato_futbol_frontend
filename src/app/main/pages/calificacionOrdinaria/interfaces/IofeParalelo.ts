
export interface IofeParalelo {
    parCodigo: number,
    parDescripcion: string,
    parEstado: number,
    parFechaCreacion: string
}