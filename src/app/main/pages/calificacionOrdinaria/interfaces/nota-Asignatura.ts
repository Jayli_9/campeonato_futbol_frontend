export interface notaAsignaturaInterface{
     noasCodigo: number;
     matCodigo: number;
      noasCalNot: string;
      noasEstado: number;
      acaMallaAsignatura: {
        maasCodigo: number;
        curCodigo: number;
        maasEstado: number;
        maasHoras: number;
        maasNemonico: null,
        reanleCodigo: number;
        insestCodigo: number;
        acaAsignatura: {
          asiCodigo: number;
          asiDescripcion: string;
          asiHoras: number;
          asiEstado: number;
          asiNemonico: null,
          espCodigo: number;
          graCodigo: number;
          acaAgrupacion: {
            agrCodigo: number;
            agrDescripcion: string;
            agrEstado: number;
            agrNemonico: string;
          },
          acaAreaConocimiento: {
            arcoCodigo: number;
            arcoDescripcion: string;
            arcoEstado: number;
            arcoNemonico: string;
          },
          acaTipoAsignatura: {
            tiasCodigo: number;
            tiasDescripcion: string;
            tiasEstado: number;
            tiasObligatorio: string;
          },
          acaTipoValoracion: {
            tivaCodigo: number;
            tivaDescripcion: string;
            tivaEstado: number;
          }
        },
        acaAsignaturaPrepa: {}
      }
}