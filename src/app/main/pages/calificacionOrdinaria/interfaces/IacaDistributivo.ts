import { IacaMallaAsignatura } from "../../asignacionDocentesOrdinaria/interfaces/IacaMallaAsignatura"

export interface IacaDistributivo {
      acaMallaAsignatura: IacaMallaAsignatura,
      curCodigo: number,
      curparCodigo: number,
      disCodigo: number,
      disEstado: number,
      docCodigo: number,
      reanleCodigo: number,
      tutor: number,
      ofeCursoParalelo: any,
      ofeCurso: any,
}