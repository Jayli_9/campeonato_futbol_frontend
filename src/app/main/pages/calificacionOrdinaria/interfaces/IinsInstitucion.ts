
export interface IinsInstitucion {

  denCodigo: number,
  inDescripcion: string,
  insAmie: string,
  insCodigo: number,
  insDescripcion: string,
  insDireccion: string,
  insEslogan: string,
  insEstado: number,
  insFechaCreacion: string,
  insFechaResolucion: string,
  insLogo: string,
  insNumResolucion: string,
  insNumeroPermiso: string,
  insRutaArchivo: string,
  jurCodigo: number,
  regimenes: any[],
  sosCodigo: number,
  tipinsCodigo: number
}