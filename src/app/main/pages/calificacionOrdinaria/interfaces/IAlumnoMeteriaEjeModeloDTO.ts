export interface IAlumnoMeteriaEjeModeloDTO {
    curCodigo: number;
    curParCodigo: number;
    insestCodigo:number;
    reanleCodigo:number;
    maasCodigo: number;
    ejgrCodigo: number;
    estudiantes: number[];
}
