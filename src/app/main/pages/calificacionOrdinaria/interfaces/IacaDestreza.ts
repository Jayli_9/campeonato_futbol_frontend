export interface IAcaDestreza {
    desCodigo: number;
    ambCodigo: number;
    desDescripcion: string;
    desEstado: number;
}