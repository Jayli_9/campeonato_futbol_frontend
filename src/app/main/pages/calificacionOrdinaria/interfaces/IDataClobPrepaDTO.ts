import { IDataDestrezaDTO } from "./IDataDestrezaDTO"

export interface IDataClobPrepaDTO {
  modelo: number
  ambito: number
  destrezas: IDataDestrezaDTO[]
}
