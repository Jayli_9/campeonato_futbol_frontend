
export interface IacaCalificacionDTO {
    anilecAnioFin: number,
    anilecAnioInicio: number,
    calCodigo: number,
    calDescripcion: string,
    calEstado: number,
    calMaximo: number,
    calMinimo: number,
    calNemonico: string,
    calPorcentaje: number,
    nemonicoCodigo: number,
    notaValorCual: string,
    notaValorCunt: number,
    nroParcial: number,
    reanleCodigo: number,
    regDescripcion: string,
    sereduCodigo: number,
    maasCodigo?: number,
    noasCodigo?: number,
    matCodigo: number,
    sereduDescripcion: string,
    tipoNemonioCalificacionEnum: string,
    listaCalificacion: any[]
    listaModeloGeneral: any[]
    promedioNota?:number,
    estudiante?: any
}