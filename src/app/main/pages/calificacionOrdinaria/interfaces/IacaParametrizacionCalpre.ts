import { IacaModevaGen } from "./IacaModevaGen";

export interface IacaParametrizacionCalpre {
    acaModevaGen: IacaModevaGen,
      modCodigo: number,
      papreCodigo: number,
      papreEstado: number,
      papreFin: string,
      papreInicio: string,
      pbojCodigo: number,
      reanleCodigo: number,
      sereduCodigo: number
}