export interface criteriosInterface{
       criCodigo: number;
       habCodigo: number;
       criDescripcion: string;
       nivCodigo: number;
       reanleCodigo: number;
       sereduCodigo: number;
       criEstado: number;
       codigoHabilidad: number;
       nombreHabilidad: string;
       seleccion: boolean;
       codigoTrimestre: number;
       ponderacionSeleccionada:number;
       codigoCalificacion: number;
       hinCodigo: number;
       codigoIdentificador: number;
}