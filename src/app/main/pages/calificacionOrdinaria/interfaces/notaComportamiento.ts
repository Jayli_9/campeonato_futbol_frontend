export interface notaComportamientoInterface{
     nocoCodigo: number;
     comCodigo: number;
     nocoEstado: number;
     matCodigo: number;
     nocoCalNota: string;
}