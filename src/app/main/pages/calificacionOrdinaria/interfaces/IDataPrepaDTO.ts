import { IDataDestrezaDTO } from "./IDataDestrezaDTO"

export interface IDataPrepaDTO {
  nodeCodigo?: number
  estCodigo: number
  matCodigo: number
  nombre?: string
  modgenCodigo?: number
  destrezas: IDataDestrezaDTO[]
}
