import {IDataEjeGradoDTO} from './IDataEjeGradoDTO';


export interface IDataOvpDTO {
    ovpCodigo?: number
    estCodigo: number
    matCodigo: number
    nombre?: string
    calificacion: IDataEjeGradoDTO[]
    jsonActual?: string

}
