
export interface IacaRangoEscala {
    acaTipoValoracion: number,
    graCodigo: number,
    resAgrupacion: string,
    resCodigo: number,
    resEscala: string,
    resEstado: number,
    vmax: number,
    vmin: number
}