export interface RangoOvp {
    ranCodigo: number;
    ranVmin: string;
    ranVmax: string;
    ranValor: string;
    ranObservacion: string;
    ranEstado: number;
    reanleCodigo: number;
}
