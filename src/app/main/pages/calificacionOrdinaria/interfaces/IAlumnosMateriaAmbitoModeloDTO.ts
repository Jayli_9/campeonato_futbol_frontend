export interface IAlumnosMateriaAmbitoModeloDTO {
  ambCodigo: number;
  modgenCodigo: number;
  maatCodigo: number;
  estudiantes: number[];
}
