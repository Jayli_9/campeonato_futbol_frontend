export interface IAcaParamCalPreDTO {
  papreCodigo: number;
  modgenCodigo: number;
  papreInicio: Date;
  papreFin: Date;
  reanleCodigo: number;
  sereduCodigo: number;
  modCodigo: number;
  pbojCodigo: number;
  papreEstado: number;
}
