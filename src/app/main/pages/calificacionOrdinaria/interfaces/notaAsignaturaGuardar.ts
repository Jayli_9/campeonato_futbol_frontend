export interface editNotaAsignaturaInterface{
    acaMallaAsignatura: number;
    acaModevaGen: number;
    matCodigo: number;
    noasCalNot: string;
    noasCodigo: number;
    noasEstado: number;
}