export interface CalifHabilidadesInterface{
     ccriCodigo: number;
     ccriEstado: number;
     criCodigo: number;
     curCodigo: number;
     curparCodigo: number;
     hinCodigo: number;
     matCodigo: number;
     modgenCodigo: number;
     ponCodigo: number;
     reanleCodigo: number;
}