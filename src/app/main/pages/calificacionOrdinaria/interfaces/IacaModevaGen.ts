import { IacaCalificacion } from "./IacaCalificacion"

export interface IacaModevaGen {
      anilecAnioFin: number,
      anilecAnioInicio: number,
      modgenCodigo: number,
      modgenDescripcion: string,
      modgenEstado: number,
      modgenNemonico: string,
      moevCodigo: number,
      moevDescripcion: string,
      moevaNemonico: string,
      reanleCodigo: number,
      regDescripcion: string
      listaCalificacion?:IacaCalificacion[]
}