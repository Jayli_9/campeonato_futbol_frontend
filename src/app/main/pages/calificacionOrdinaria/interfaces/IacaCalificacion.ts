
export interface IacaCalificacion {
    anilecAnioFin: number,
    anilecAnioInicio: number,
    calCodigo: number,
    calDescripcion: string,
    calEstado: number,
    calMaximo: number,
    calMinimo: number,
    calNemonico: string,
    calPorcentaje: number,
    modgenCodigo: number,
    nemonicoCodigo: number,
    notaValorCual: string,
    notaValorCunt: number,
    nroParcial: number,
    reanleCodigo: number,
    regDescripcion: string,
    sereduCodigo: number,
    sereduDescripcion: string,
    tipoNemonioCalificacionEnum: string
}