export interface gradoCatalogoInterface{
       graCodigo: number;
       nivCodigo: number;
       graDescripcion: string;
       graEstado: number;
       graNemonico: string;
       graFechaCreacion: Date;
}