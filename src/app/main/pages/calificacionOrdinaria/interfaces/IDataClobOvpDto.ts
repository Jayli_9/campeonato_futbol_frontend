import {EjeGradoDto} from '../../eje-grado/interfaces/eje-grado-dto';
import {IDataEjeGradoDTO} from './IDataEjeGradoDTO';

export interface IDataClobOvpDto {
    modelo: number;
    ejgrCodigo:number;
    calificacion:IDataEjeGradoDTO[];
}
