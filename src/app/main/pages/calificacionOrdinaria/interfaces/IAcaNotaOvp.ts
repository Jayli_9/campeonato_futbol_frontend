export interface IAcaNotaOvp {
    ovpCodigo?: number;
    matCodigo: number;
    curCodigo: number;
    curParCodigo: number;
    reanleCodigo: number;
    ovpCalNot: string;
    insEstCodigo: number;
    ovpEstado: number;
    maasCodigo: number;
}
