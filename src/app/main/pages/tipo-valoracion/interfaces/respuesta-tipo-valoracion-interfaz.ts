import { RespuestaGeneralInterfaz } from "app/main/shared/interfaces/respuesta-general-interfaz";
import { TipoValoracion } from "./tipo-valoracion";

export interface RespuestaTipoValoracionInterfaz extends RespuestaGeneralInterfaz{
    listado: TipoValoracion[];
    objeto: TipoValoracion;
}
