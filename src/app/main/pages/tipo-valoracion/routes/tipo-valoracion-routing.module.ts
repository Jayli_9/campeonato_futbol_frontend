import { Routes } from "@angular/router";
import { AuthGuard } from "app/auth/helpers/auth.guards";
import { TipoValoracionComponent } from "../components/tipo-valoracion/tipo-valoracion.component";

export const RUTA_TIPO_VALORACION:Routes=[
    {
        path:'tipo-valoracion',
        component:TipoValoracionComponent,
        canActivate:[AuthGuard]
    }
]