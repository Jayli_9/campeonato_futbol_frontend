import { Component, OnInit, ViewChild } from '@angular/core';
import { UntypedFormBuilder, UntypedFormGroup, Validators } from '@angular/forms';
import {MatPaginator} from '@angular/material/paginator';
import {MatSort} from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import Swal from 'sweetalert2';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { NgxSpinnerService } from 'ngx-spinner';
import { TipoValoracion } from '../../interfaces/tipo-valoracion';
import { TipoValoracionService } from '../../services/tipo-valoracion.service';
import { RespuestaTipoValoracionInterfaz } from '../../interfaces/respuesta-tipo-valoracion-interfaz';

@Component({
  selector: 'app-tipo-valoracion',
  templateUrl: './tipo-valoracion.component.html',
  styleUrls: ['./tipo-valoracion.component.scss']
})
export class TipoValoracionComponent implements OnInit {
  public contentHeader: object;
  public frmTipoValoracionCreateEdit: UntypedFormGroup;
  public submitted = false;
  listaTiposValoracion:TipoValoracion[]=null;
  columnasTipoValoracion=['num','descripcion','estado','acciones'];
  dataSource: MatTableDataSource<TipoValoracion>;

  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;
  constructor(
    private readonly fb: UntypedFormBuilder,
    private modalService: NgbModal,
    public spinner: NgxSpinnerService,
    private _tipoValoracionService:TipoValoracionService
  ) { }

  ngOnInit(): void {
    this.contentHeader = {
      headerTitle: 'Tipo Valoración',
      actionButton: false,
      breadcrumb: {
        type: '',
        links: [
          {
            name: 'Inicio',
            isLink: true,
            link: '/pages/inicio'
          },
          {
            name: 'Tipo Valoración',
            isLink: false
          },
        ]
      }
    };
    this.listarTodosLosTiposValoracion();
  }
  listarTodosLosTiposValoracion(){
    this.spinner.show();
    this._tipoValoracionService.listarTodosLosTiposDeValoracion().subscribe(
      (respuesta:RespuestaTipoValoracionInterfaz)=>{
        this.listaTiposValoracion=respuesta.listado;
        this.dataSource = new MatTableDataSource(this.listaTiposValoracion);
        this.dataSource.paginator = this.paginator;
        this.dataSource.paginator._intl.itemsPerPageLabel="Tipos de Valoración por página";
        this.dataSource.paginator._intl.nextPageLabel="Siguiente";
        this.dataSource.paginator._intl.previousPageLabel="Anterior";
        this.dataSource.sort = this.sort;
        this.spinner.hide();
      },
      (error:any)=>{
        this.spinner.hide();
       Swal.fire('Ups! ocurrió un error al cargar Tipos de Valoración ','','error'); 
      }
    );
  }

  nuevoTipoValoracion(modalForm){
    this.submitted=false;
    this.frmTipoValoracionCreateEdit=this.fb.group({
      descripcion: ['', [Validators.required]],
    });
    this.modalService.open(modalForm);
  }
  editTipoValoracion(modalForm,tipoValoracionEdit:TipoValoracion){
    this.submitted=false;
    this.frmTipoValoracionCreateEdit=this.fb.group({
      codigo:[tipoValoracionEdit.tivaCodigo],
      descripcion: [tipoValoracionEdit.tivaDescripcion, [Validators.required]],
      estado:[tipoValoracionEdit.tivaEstado]
    });
    this.modalService.open(modalForm);
  }

  guardarTipoValoracion(modalForm){
    this.spinner.show();
    this.submitted = true;
    if (this.frmTipoValoracionCreateEdit.invalid) {
      this.spinner.hide();
      return;
    }
    let tipoValoracionNueva:TipoValoracion={
      tivaDescripcion:this.frmTipoValoracionCreateEdit.get("descripcion").value,
      tivaEstado:1
    };
    this._tipoValoracionService.guardarActualizarTipoValoracion(tipoValoracionNueva).subscribe(
      (respuesta:any)=>{
          Swal.fire(`Tipo de Valoración Creado!`,'','success');
          this.spinner.hide();
          this.listarTodosLosTiposValoracion();
          
      },
      (error:any)=>{
        Swal.fire(`No se pudo crear el Tipo de Valoración `,'','error'); 
        this.spinner.hide();
        this.listarTodosLosTiposValoracion();
      }
    );
    modalForm.close('Accept click');  
  }

  activarInactivar(tipoValoracionActualizar:TipoValoracion){
    this.spinner.show();
    if (tipoValoracionActualizar.tivaEstado==1) {
      this._tipoValoracionService.inactivarTipoValoracion(tipoValoracionActualizar.tivaCodigo).subscribe(
        (respuesta:any)=>{
          this.spinner.hide();
          Swal.fire(' Tipo Valoración Inactivado','','success'); 
          this.listarTodosLosTiposValoracion();
        },
        (error:any)=>{
          this.spinner.hide();
          Swal.fire('Ups! ocurrió un error','','error'); 
          this.listarTodosLosTiposValoracion();
        }
      );
    } else {
      tipoValoracionActualizar.tivaEstado=1;
      this._tipoValoracionService.guardarActualizarTipoValoracion(tipoValoracionActualizar).subscribe(
        (respuesta:any)=>{
          this.spinner.hide();
          Swal.fire(' Tipo Valoración Activada','','success'); 
          this.listarTodosLosTiposValoracion();
        },
        (error:any)=>{
          this.spinner.hide();
          Swal.fire('Ups! ocurrió un error','','error'); 
          this.listarTodosLosTiposValoracion();
        }
      );
    }
    
}
actualizarTipoValoracion(modalForm){
  this.spinner.show();
    this.submitted = true;
    // stop here if form is invalid
    if (this.frmTipoValoracionCreateEdit.invalid) {
      this.spinner.hide();
      return;
    }
    let tipoValoracionGuardar:TipoValoracion={
      tivaCodigo:this.frmTipoValoracionCreateEdit.get("codigo").value,
      tivaDescripcion:this.frmTipoValoracionCreateEdit.get("descripcion").value,
      tivaEstado:this.frmTipoValoracionCreateEdit.get("estado").value,
    };
    this._tipoValoracionService.guardarActualizarTipoValoracion(tipoValoracionGuardar).subscribe(
      (respuesta:any)=>{
          Swal.fire(`Tipo Valoración Actualizada!`,'','success'); 
          this.spinner.hide();
          this.listarTodosLosTiposValoracion();
      },
      (error:any)=>{
        Swal.fire(`No se pudo guardar el Tipo de Valoración`,'','error'); 
        this.spinner.hide();
        this.listarTodosLosTiposValoracion();
      }
    );
    modalForm.close('Accept click');
}
applyFilter(event: Event) {
  const filterValue = (event.target as HTMLInputElement).value;
  this.dataSource.filter = filterValue.trim().toLowerCase();

  if (this.dataSource.paginator) {
    this.dataSource.paginator.firstPage();
  }
}
 // getter for easy access to form fields
 get ReactiveFrmTipoValoracionCreateEdit() {
  return this.frmTipoValoracionCreateEdit.controls;
 }
}
