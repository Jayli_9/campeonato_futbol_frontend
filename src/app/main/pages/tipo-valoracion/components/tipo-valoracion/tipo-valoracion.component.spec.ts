import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TipoValoracionComponent } from './tipo-valoracion.component';

describe('TipoValoracionComponent', () => {
  let component: TipoValoracionComponent;
  let fixture: ComponentFixture<TipoValoracionComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ TipoValoracionComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(TipoValoracionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
