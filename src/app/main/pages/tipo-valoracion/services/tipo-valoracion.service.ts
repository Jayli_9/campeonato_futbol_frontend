import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'environments/environment';
import { RespuestaTipoValoracionInterfaz } from '../interfaces/respuesta-tipo-valoracion-interfaz';
import { TipoValoracion } from '../interfaces/tipo-valoracion';

@Injectable({
  providedIn: 'root'
})
export class TipoValoracionService {

  url_academico=environment.url_academico;
  constructor(
    public _http:HttpClient
  ) { }
  guardarActualizarTipoValoracion(valoracion:TipoValoracion){
    let url_ws=`${this.url_academico}/private/guardarTipoValoracion`;
    return this._http.post(url_ws,valoracion);
  }

  listarTodosLosTiposDeValoracion(){
    let url_ws=`${this.url_academico}/private/listarTodosLosTiposDeValoracion`;
    return this._http.get<RespuestaTipoValoracionInterfaz>(url_ws);
  }

  listarTiposValoracionActivos(){
    let url_ws=`${this.url_academico}/private/listarTiposValoracionActivos`;
    return this._http.get<RespuestaTipoValoracionInterfaz>(url_ws);
  }

  buscarTipoValoracionPorCodigo(codigo:number){
    let url_ws=`${this.url_academico}/private/buscarTipoValoracionPorCodigo/${codigo}`;
    return this._http.get<RespuestaTipoValoracionInterfaz>(url_ws);
  }
  
  inactivarTipoValoracion(codigo:number){
    let url_ws=`${this.url_academico}/private/eliminarTipoValoracionPorId/${codigo}`;
    return this._http.delete(url_ws);
  }
}
