import { TestBed } from '@angular/core/testing';

import { TipoValoracionService } from './tipo-valoracion.service';

describe('TipoValoracionService', () => {
  let service: TipoValoracionService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(TipoValoracionService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
