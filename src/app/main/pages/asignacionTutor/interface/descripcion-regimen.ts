export interface descipcionRegimenInterface{
     regCodigo: number;
     regDescripcion: string;
     regEstado: number;
     regFechaCreacion: Date;
     regAnioLecActual: null,
    regAnioLecSiguiente: null
    
}