export interface ResponseGenerico<T>{
    codigoRespuesta: string;
    mensaje: string;
    objeto: T;
    listado: Array<T>;
    totalRegistros: number;
}