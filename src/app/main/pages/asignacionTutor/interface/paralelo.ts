export interface paraleloInterface{
     parCodigo: number;
     parDescripcion: string;
     parEstado: number;
     parFechaCreacion: Date;
}