export interface figuraProfesionalInterface{
     figproCodigo: number;
     figproDescripcion: string;
     figproEstado: number;
     figproFechaCreacion: Date;
     arecieCodigo: number;
}