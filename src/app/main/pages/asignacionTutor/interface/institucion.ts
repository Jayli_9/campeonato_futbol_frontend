export interface institucionInterface{
     insCodigo: number;
     amie: string;
     nombreInstitucion: string;
     insEstado: number;
     sostenimiento: string;
     regimen: string;
     jurisdiccion: string;
     tipoEstablecimiento: string;
     disDescripcion: string;
     disCodAd: string;
     zonaDescripcion: string;
     parDescripcion: string;
     zonCodAd: string;
     canDescripcion: string;
     proDescripcion: string;
     parCodDpa: string;
     zonaParroquia: string;
     coordenadaX: string;
     coordenadaY: string;
}