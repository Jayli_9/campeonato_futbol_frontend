import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RUTA_ASIGNACION_TUTORES } from "../rutas/asignacion-tutores.routing";
import { RUTA_SELECCION_GRADOS } from "../rutas/seleccion-grados.routing";
import { SeleccionGradosComponent } from "../componentes/seleccion-grados/seleccion-grados.component";
import { AsignacionTutoresComponent } from "../componentes/asignacion-tutores/asignacion-tutores.component";
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ProgramStoreService } from "../servicios/program-store.service";
import { RouterModule } from '@angular/router';
import { CoreCommonModule } from '@core/common.module';
import { ContentHeaderModule } from 'app/layout/components/content-header/content-header.module';
import { MaterialModule } from 'app/main/shared/material/material.module';

@NgModule({
    declarations: [SeleccionGradosComponent, AsignacionTutoresComponent],
    imports: [
      CommonModule,
      RouterModule.forChild(RUTA_ASIGNACION_TUTORES),
      RouterModule.forChild(RUTA_SELECCION_GRADOS),
      FormsModule,
      ReactiveFormsModule,
      MaterialModule,
      ContentHeaderModule,
      CoreCommonModule
    ],
    providers: [ProgramStoreService]
  })

  export class asignacionTutorModule { }