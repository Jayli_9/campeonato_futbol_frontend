import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { environment } from 'environments/environment';
import { ResponseGenerico } from '../interface/response-generico';
import { regimenInterface } from "../interface/regimen";
import { descipcionRegimenInterface } from "../interface/descripcion-regimen";
import { anioElectivoInterface } from "../interface/anioElectivo";
import { nivelInterface } from "../interface/nivel";
import { especialidadInterface } from "../interface/especialidad";
import { figuraProfesionalInterface } from "../interface/figuraProfesional";
import { gradosInterface } from "../interface/grados";
import { jornadaInterface } from "../interface/jornada";
import { IcatPoblacionObjetivo } from '../interface/poblacion';

@Injectable({
  providedIn: 'root'
})
export class CatalogoService {

constructor(private _http: HttpClient) { }

buscarRegimenPorCodigo(cod): Promise<ResponseGenerico<regimenInterface>>{
  return new Promise((resolve, reject) => {
    this._http.get(`${environment.url_catalogo}/private/listarRegimenAnioLectivoPorCodigoRegimen/`+cod).subscribe((response: ResponseGenerico<regimenInterface>) => {
      resolve(response);
    }, reject);
  })
}

buscarRegimenPorCodigoDescipcion(cod): Promise<ResponseGenerico<descipcionRegimenInterface>>{
  return new Promise((resolve, reject) => {
    this._http.get(`${environment.url_catalogo}/private/buscarRegimenPorCodigo/`+cod).subscribe((response: ResponseGenerico<descipcionRegimenInterface>) => {
      resolve(response);
    }, reject);
  })
}


buscarAnioElectivoPorCodigo(cod): Promise<ResponseGenerico<anioElectivoInterface>>{
  return new Promise((resolve, reject) => {
    this._http.get(`${environment.url_catalogo}/private/buscarAnioLectivoPorCodigo/`+cod).subscribe((response: ResponseGenerico<anioElectivoInterface>) => {
      resolve(response);
    }, reject);
  })
}

buscarNivelPorCodigo(cod) : Promise<ResponseGenerico<nivelInterface>>{
  return new Promise((resolve, reject) => {
    this._http.get(`${environment.url_catalogo}/private/buscarNivelPorCodigo/`+cod).subscribe((response: ResponseGenerico<nivelInterface>) => {
      resolve(response);
    }, reject);
  })
}


buscarEspecialidadPorCodigo(cod): Promise<ResponseGenerico<especialidadInterface>>{
  return new Promise((resolve, reject) => {
    this._http.get(`${environment.url_catalogo}/private/buscarCatalogoEspecialidadPorCodigo/`+cod).subscribe((response: ResponseGenerico<especialidadInterface>) => {
      resolve(response);
    }, reject);
  })
}


buscarFiguraProfesional(cod):  Promise<ResponseGenerico<figuraProfesionalInterface>>{
  return new Promise((resolve, reject) => {
    this._http.get(`${environment.url_catalogo}/private/buscarCatalogoFiguraProfesionalPorCodigo/`+cod).subscribe((response: ResponseGenerico<figuraProfesionalInterface>) => {
      resolve(response);
    }, reject);
  })
}

buscarGradosPorNivel(cod):  Promise<ResponseGenerico<gradosInterface>>{
  return new Promise((resolve, reject) => {
    this._http.get(`${environment.url_catalogo}/private/buscarCatalogoGradosPorNivelCodigo/`+cod).subscribe((response: ResponseGenerico<gradosInterface>) => {
      resolve(response);
    }, reject);
  })
}

buscarJornadaPorCodigo(cod): Promise<ResponseGenerico<jornadaInterface>>{
  return new Promise((resolve, reject) => {
    this._http.get(`${environment.url_catalogo}/private/buscarJornadaPorCodigo/`+cod).subscribe((response: ResponseGenerico<jornadaInterface>) => {
      resolve(response);
    }, reject);
  })
}


listarJornadas(): Promise<ResponseGenerico<jornadaInterface>>{
  return new Promise((resolve, reject) => {
    this._http.get(`${environment.url_catalogo}/private/listarJornadas`).subscribe((response: ResponseGenerico<jornadaInterface>) => {
      resolve(response);
    }, reject);
  })
}


buscarGradoPorCodigo(cod): Promise<ResponseGenerico<gradosInterface>>{
  return new Promise((resolve, reject) => {
    this._http.get(`${environment.url_catalogo}/private/buscarCatalogoGradosPorCodigo/`+cod).subscribe((response: ResponseGenerico<gradosInterface>) => {
      resolve(response);
    }, reject);
  })
}


//Anio electivo segun codigo
getPoblacionObjetivo(): Promise<ResponseGenerico<IcatPoblacionObjetivo>> {
  return new Promise((resolve, reject) => {
    this._http.get(`${environment.url_catalogo}/private/listarPoblacionObjetivo`).subscribe((response: ResponseGenerico<IcatPoblacionObjetivo>) => {
      resolve(response);
    }, reject);
  })
}



}
