import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { environment } from 'environments/environment';
import { ResponseGenerico } from '../interface/response-generico';
import { cursoParaleloInterface } from "../interface/cursoParalelo";
import { paraleloInterface } from '../interface/paralelo';
import { cursoInterface } from '../interface/curso';

@Injectable({
  providedIn: 'root'
})
export class OfertaService {

constructor(private _http: HttpClient) { }

buscarcursoParaleloPorCodigo(cod): Promise<ResponseGenerico<cursoParaleloInterface>>{
  return new Promise((resolve, reject) => {
    this._http.get(`${environment.url_oferta}/private/buscarCursoParaleloPorCodigo/`+cod).subscribe((response: ResponseGenerico<cursoParaleloInterface>) => {
      resolve(response);
    }, reject);
  })
}


buscarParaleloPorCodigo(cod): Promise<ResponseGenerico<paraleloInterface>>{
  return new Promise((resolve, reject) => {
    this._http.get(`${environment.url_oferta}/private/buscarParaleloPorCodigo/`+cod).subscribe((response: ResponseGenerico<paraleloInterface>) => {
      resolve(response);
    }, reject);
  })
}

buscarCursoPorCodigo(cod): Promise<ResponseGenerico<cursoInterface>>{
  return new Promise((resolve, reject) => {
    this._http.get(`${environment.url_oferta}/private/buscarCursoPorCodigo/`+cod).subscribe((response: ResponseGenerico<cursoInterface>) => {
      resolve(response);
    }, reject);
  })
}


buscarCurosPorCodigoServicioEducativo(cod): Promise<ResponseGenerico<cursoInterface>>{
  return new Promise((resolve, reject) => {
    this._http.get(`${environment.url_oferta}/private/buscarCursoPorCodigoServicio/`+cod).subscribe((response: ResponseGenerico<cursoInterface>) => {
      resolve(response);
    }, reject);
  })
}

listarCurso(): Promise<ResponseGenerico<cursoInterface>>{
  return new Promise((resolve, reject) => {
    this._http.get(`${environment.url_oferta}/private/listaCurso`).subscribe((response: ResponseGenerico<cursoInterface>) => {
      resolve(response);
    }, reject);
  })
}


}
