import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { environment } from 'environments/environment';
import { ResponseGenerico } from '../interface/response-generico';
import { regimenInstitucionInterface } from "../interface/regimen-institucion";
import { permisoNivelIntitucionInterface } from "../interface/permisoNivelInstitucion";
import { institucionInterface } from "../interface/institucion";
import { CatEstablecimiento } from "../interface/establecimiento";

@Injectable({
  providedIn: 'root'
})
export class GieeService {

constructor(private _http: HttpClient) { }

buscarRegimenPorInstitucion(cod): Promise<ResponseGenerico<regimenInstitucionInterface>>{
  return new Promise((resolve, reject) => {
    this._http.get(`${environment.url_institucion}/private/listarInstitucionRegimenPorCodigoInstitucion/`+cod).subscribe((response: ResponseGenerico<regimenInstitucionInterface>) => {
      resolve(response);
    }, reject);
  })
}


buscarPermisoNivelPorInstitucion(cod): Promise<ResponseGenerico<permisoNivelIntitucionInterface>>{
  return new Promise((resolve, reject) => {
    this._http.get(`${environment.url_institucion}/private/listarNivelPermisoPorCodigoInstitucionEstadoYVigente/`+cod).subscribe((response: ResponseGenerico<permisoNivelIntitucionInterface>) => {
      resolve(response);
    }, reject);
  })
}


buscarInstitucionPorAmie(cod): Promise<ResponseGenerico<institucionInterface>>{
  return new Promise((resolve, reject) => {
    this._http.get(`${environment.url_institucion}/private/buscarInstitucionPorAmie/`+cod).subscribe((response: ResponseGenerico<institucionInterface>) => {
      resolve(response);
    }, reject);
  })
}

getEstablecimientoPorAmie(cod): Promise<ResponseGenerico<CatEstablecimiento>>{
  return new Promise((resolve, reject) => {
this._http.get(`${environment.url_institucion}/private/listarEstablecimientosPorAmie/`+ cod).subscribe((response: ResponseGenerico<CatEstablecimiento>) => {
   resolve(response);
 }, reject);
})
}


}
