import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { environment } from 'environments/environment';
import { ResponseGenerico } from '../interface/response-generico';
import { docenteInterface } from "../interface/docente";

@Injectable({
  providedIn: 'root'
})
export class DocenteService {

constructor(private _http: HttpClient) { }


buscarDocentePorCodigo(cod): Promise<ResponseGenerico<docenteInterface>>{
  return new Promise((resolve, reject) => {
    this._http.get(`${environment.url_docente}/private/obtenerDocenteHispanaPorCodigo/`+cod).subscribe((response: ResponseGenerico<docenteInterface>) => {
      resolve(response);
    }, reject);
  })
}

buscarDocentePorIdentificacion(cod): Promise<ResponseGenerico<docenteInterface>>{
  return new Promise((resolve, reject) => {
    this._http.get(`${environment.url_docente}/private/docenteHispanaPorIdentificacionYEstado/`+cod).subscribe((response: ResponseGenerico<docenteInterface>) => {
      resolve(response);
    }, reject);
  })
}

}
