import { Injectable } from '@angular/core';

@Injectable()
export class ProgramStoreService {

    private grado = [];
    private tutorGrado = [];
    private distributivos = [];
    private docente;
    
    private datosEstablecimiento;
    private codigoRegimen;


    get datosEstablecimientoGet(){
        return this.datosEstablecimiento;
    }

    set datosEstablecimientoSet(datosEstablecimiento){
        this.datosEstablecimiento = datosEstablecimiento;
    }

    get codigoRegimenGet(){
        return this.codigoRegimen;
    }

    set codigoRegimenSet(codigoRegimen){
        this.codigoRegimen = codigoRegimen;
    }


    get nombreDocente(){
        return this.docente;
    }

    set nombreDocente(docente){
        this.docente = docente;
    }

    get totalDistributivos() {
        return this.distributivos;
    }

    set totalDistributivos(distributivos){
        this.distributivos = distributivos;
    }


    get seleccionTutorGrado() {
        return this.tutorGrado;
    }

    set seleccionTutorGrado(tutorGrado){
        this.tutorGrado = tutorGrado;
    }

    get seleccionGrado() {
        return this.grado;
    }

    set seleccionGrado(grado){
        this.grado = grado;
    }

}
