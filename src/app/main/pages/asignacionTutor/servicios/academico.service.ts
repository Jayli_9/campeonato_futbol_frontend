import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { environment } from 'environments/environment';
import { ResponseGenerico } from '../interface/response-generico';
import { distributivoInterface } from "../interface/distributivo";
import { tutorInterface } from '../interface/tutor';
import { distributivoDTOInterface } from "../interface/busquedaDistributivo";

@Injectable({
  providedIn: 'root'
})
export class AcademicoService {

constructor(private _http: HttpClient) { }

listarDistributivosActivos(): Promise<ResponseGenerico<distributivoInterface>>{
  return new Promise((resolve, reject) => {
    this._http.get(`${environment.url_academico}/private/listarDistributivosActivos`).subscribe((response: ResponseGenerico<distributivoInterface>) => {
      resolve(response);
    }, reject);
  })
}


buscarTutorPorCurCodigo(cod): Promise<ResponseGenerico<tutorInterface>>{
  return new Promise((resolve, reject) => {
    this._http.get(`${environment.url_academico}/private/buscarTutorOrdinariaPorCurCodigo/`+ cod).subscribe((response: ResponseGenerico<tutorInterface>) => {
      resolve(response);
    }, reject);
  })
}


listaTutor(): Promise<ResponseGenerico<tutorInterface>>{
  return new Promise((resolve, reject) => {
    this._http.get(`${environment.url_academico}/private/listarTutorOrdinaria`).subscribe((response: ResponseGenerico<tutorInterface>) => {
      resolve(response);
    }, reject);
  })
}


buscarTutorCurparCodigo(cod): Promise<ResponseGenerico<tutorInterface>>{
  return new Promise((resolve, reject) => {
    this._http.get(`${environment.url_academico}/private/buscarTutorOrdinariaPorCuparCodigo/`+ cod).subscribe((response: ResponseGenerico<tutorInterface>) => {
      resolve(response);
    }, reject);
  })
}

buscarDistributivoPorRealmCodigoYInsestCodigo(parametro: any){
  return this._http.post<distributivoDTOInterface>(`${environment.url_academico}/private/listarDistributivosRealmCodigoYInsestCodigo`, parametro);
}


guardarTutor(parametro: any){
  return this._http.post<tutorInterface>(`${environment.url_academico}/private/guardarTutorOrdinaria`, parametro);
}


editarTutor(parametro: any){
  return this._http.post<tutorInterface>(`${environment.url_academico}/private/actualizarTutorOrdinaria`, parametro);
}

EliminarTutor(cod): Promise<ResponseGenerico<tutorInterface>>{
  return new Promise((resolve, reject) => {
    this._http.delete(`${environment.url_academico}/private/eliminarTutorOrdinaria/`+ cod).subscribe((response: ResponseGenerico<tutorInterface>) => {
      resolve(response);
    }, reject);
  })
}


}
