import { Routes } from "@angular/router";
import { AuthGuard } from "app/auth/helpers/auth.guards";
import { AsignacionTutoresComponent } from "../componentes/asignacion-tutores/asignacion-tutores.component";


export const RUTA_ASIGNACION_TUTORES: Routes = [
    {
      path: 'asignacionTutores',
      component: AsignacionTutoresComponent,
    }
  ];