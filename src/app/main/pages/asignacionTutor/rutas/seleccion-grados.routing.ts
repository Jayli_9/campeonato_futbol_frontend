import { Routes } from "@angular/router";
import { AuthGuard } from "app/auth/helpers/auth.guards";
import { SeleccionGradosComponent } from "../componentes/seleccion-grados/seleccion-grados.component";

export const RUTA_SELECCION_GRADOS: Routes = [
    {
      path: 'seleccionGrados',
      component: SeleccionGradosComponent,
    }
  ];