import { Component, OnInit, Inject, Input, Output, EventEmitter } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { ProgramStoreService } from '../../../servicios/program-store.service';
import { DocenteService } from "../../../servicios/docente.service";
import { AcademicoService } from "../../../servicios/academico.service";
import Swal from 'sweetalert2';
@Component({
  selector: 'app-modaltutor',
  templateUrl: './modaltutor.component.html',
  styleUrls: ['./modaltutor.component.scss']
})
export class ModaltutorComponent implements OnInit {
  @Input() fromParent;


  //lista docente
  listaDocente;
  codigoDocente;


  //data docente
  dataCursoDocente;
  listaDistributivo;

  //codigoCurso
  codigoParalelo;

  //docenteDatos
  codigosDocentes = [];
  listaDocentes = [];
  listaDocentesFiltro = [];


  //lista de tutores
  listaTutores;
  listaEdicion;
  opcionAcccionEditar = null;
  opcionAccionGuardar = null;
  codigoTutor;


  constructor(
    public activeModal: NgbActiveModal,
    private readonly progamstore: ProgramStoreService,
    public docenteService: DocenteService,
    public academicoService: AcademicoService
  ) {
    this.dataCursoDocente = this.progamstore.seleccionGrado;
    this.listaDistributivo = this.progamstore.totalDistributivos;
    this.codigoDocente = this.dataCursoDocente.codigoDocente;
   }


   buscarDatos(){
    let curCodigo = this.dataCursoDocente.curCodigo;
      let curparCodigo = this.dataCursoDocente.cuparCodigo;
      let realCodigo = this.dataCursoDocente.realCodigo;

    this.academicoService.listaTutor().then(data => {
      this.listaTutores = data.listado;
      this.listaTutores.forEach(element => {
        if(element.curCodigo === curCodigo && element.curparCodigo === curparCodigo && element.reanleCodigo === realCodigo && element.tuorEstado === 1){
          this.codigoTutor = element.tuorCodigo;
          this.opcionAcccionEditar = 1;
          return;
        }else{
          this.opcionAccionGuardar = 2;
        }
      });
    })
   }

  ngOnInit() {
    this.codigoParalelo = this.dataCursoDocente.cuparCodigo;
    this.codigoDocente = this.dataCursoDocente.codigoDocente;
    this.sacarDocentes(this.listaDistributivo, this.codigoParalelo);
    this.buscarDatos();
  }


  async sacarDocentes(obj, cod){

    obj.forEach(element => {

      if(cod === element.curparCodigo){

        this.codigosDocentes.push(element.docCodigo);

      }

    });

    this.obtenerDocentes(this.codigosDocentes);

  }


  cambioDocente(event){
    this.codigoDocente = event;
  }


  async obtenerDocentes (obj){

    obj.forEach(element => {

      this.docenteService.buscarDocentePorCodigo(element).then(data => {
        this.listaDocentes.push(data.objeto);

        this.listaDocentesFiltro = this.listaDocentes.filter((item, index, self) => {
          const foundIndex = self.findIndex((el) => (
            el.nomPersona === item.nomPersona
          ));
          return index === foundIndex;
        });

      })

    });

  }


  async guardado(){

    let curCodigo = this.dataCursoDocente.curCodigo;
      let curparCodigo = this.dataCursoDocente.cuparCodigo;
      let realCodigo = this.dataCursoDocente.realCodigo;


      // Obtener la fecha actual
const fechaActual = new Date();

// Obtener los componentes de la fecha
const año = fechaActual.getFullYear();
const mes = (fechaActual.getMonth() + 1).toString().padStart(2, '0'); // Los meses comienzan desde 0
const dia = fechaActual.getDate().toString().padStart(2, '0');
const horas = fechaActual.getHours().toString().padStart(2, '0');
const minutos = fechaActual.getMinutes().toString().padStart(2, '0');
const segundos = fechaActual.getSeconds().toString().padStart(2, '0');
const milisegundos = fechaActual.getMilliseconds().toString().padStart(3, '0');

// Formatear la fecha
const fechaFormateada = `${año}-${mes}-${dia}T${horas}:${minutos}:${segundos}.${milisegundos}Z`;
      let datosGurardado = {
        "curCodigo": curCodigo,
        "curparCodigo": curparCodigo,
        "docCodigo": parseInt(this.codigoDocente),
        "reanleCodigo": realCodigo,
        "tuorEstado": 1,
        "tuorFecha": fechaFormateada,
        "tuotTutor": 1
      }

      Swal.fire({
        title: '¿Esta seguro que quiere asignar este docente como tutor?',
        icon: 'info',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Si',
        cancelButtonText: 'No'
      }).then((result) => {
        if(result.isConfirmed){
          this.academicoService.guardarTutor(datosGurardado).subscribe({
            next: (respuesta) => {
              Swal.fire({
                title: 'Datos Guardados',
                text: "Datos registrados y guardados",
                icon: 'success',
                confirmButtonColor: '#3085d6',
                confirmButtonText: 'ok'
              }).then((result) => {
                if(result.isConfirmed){
                  this.activeModal.close(this.fromParent);
                }
              })
            },
            error: (error) =>{
              Swal.fire({
                icon: 'error',
                title: 'Atención!',
                text: 'Error en guardar los datos'
              })
            }
          })
        }
      })

  }


  async editar (){

    let curCodigo = this.dataCursoDocente.curCodigo;
      let curparCodigo = this.dataCursoDocente.cuparCodigo;
      let realCodigo = this.dataCursoDocente.realCodigo;


      // Obtener la fecha actual
const fechaActual = new Date();

// Obtener los componentes de la fecha
const año = fechaActual.getFullYear();
const mes = (fechaActual.getMonth() + 1).toString().padStart(2, '0'); // Los meses comienzan desde 0
const dia = fechaActual.getDate().toString().padStart(2, '0');
const horas = fechaActual.getHours().toString().padStart(2, '0');
const minutos = fechaActual.getMinutes().toString().padStart(2, '0');
const segundos = fechaActual.getSeconds().toString().padStart(2, '0');
const milisegundos = fechaActual.getMilliseconds().toString().padStart(3, '0');

// Formatear la fecha
const fechaFormateada = `${año}-${mes}-${dia}T${horas}:${minutos}:${segundos}.${milisegundos}Z`;


      let datosGurardado = {
        "tuorCodigo": parseInt(this.codigoTutor),
        "curCodigo": curCodigo,
        "curparCodigo": curparCodigo,
        "docCodigo": parseInt(this.codigoDocente),
        "reanleCodigo": realCodigo,
        "tuorEstado": 1,
        "tuorFecha": fechaFormateada,
        "tuotTutor": 1
      }

      Swal.fire({
        title: '¿Esta seguro que quiere asignar este docente como tutor?',
        icon: 'info',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Si',
        cancelButtonText: 'No'
      }).then((result) => {
        if(result.isConfirmed){
          this.academicoService.editarTutor(datosGurardado).subscribe({
            next: (respuesta) => {
              Swal.fire({
                title: 'Datos Editados',
                text: "Datos registrados y editados",
                icon: 'success',
                confirmButtonColor: '#3085d6',
                confirmButtonText: 'ok'
              }).then((result) => {
                if(result.isConfirmed){
                  this.activeModal.close(this.fromParent);
                }
              })
            },
            error: (error) =>{
              Swal.fire({
                icon: 'error',
                title: 'Atención!',
                text: 'Error en guardar los datos'
              })
            }
          })
        }
      })

  }


  closeModal(sendData) {
    let existeCurso=false;
    if(sendData==='ok'){

      if(this.opcionAcccionEditar == 1){
        this.editar();
      }else if(this.opcionAccionGuardar == 2){
        this.guardado();
      }

    }else if(sendData==='cancel'){
      this.activeModal.close(this.fromParent);
    }else if(sendData==='dismiss'){
      this.activeModal.close(this.fromParent);
    }
  }

}
