import { Component, OnInit, ViewChild } from '@angular/core';
import { CatalogoService } from "../../servicios/catalogo.service";
import { GieeService } from "../../servicios/giee.service";
import { User } from 'app/auth/models';
import { ProgramStoreService } from "../../servicios/program-store.service";
import { AcademicoService } from "../../servicios/academico.service";
import { OfertaService } from "../../servicios/oferta.service";
import { NgxSpinnerService } from 'ngx-spinner';
import { DocenteService } from "../../servicios/docente.service";
import { ModaltutorComponent } from "../../componentes/asignacion-tutores/modaltutor/modaltutor.component";
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import Swal from 'sweetalert2';

import pdfMake from 'pdfmake/build/pdfmake';
import pdfFonts from 'pdfmake/build/vfs_fonts';
pdfMake.vfs = pdfFonts.pdfMake.vfs;

@Component({
  selector: 'app-asignacion-tutores',
  templateUrl: './asignacion-tutores.component.html',
  styleUrls: ['./asignacion-tutores.component.scss']
})
export class AsignacionTutoresComponent implements OnInit {

  @ViewChild(ModaltutorComponent) colocarTutor: ModaltutorComponent;

  contentHeader: object;

  //datos de seleccion de grados
  listaSeleccionGrado;
  seleccionGrados;

  //variables para los filtros
  listaJornada;
  codigoJornadaSeleccionada;
  codigoJornada;
  listaGrados;
  codigoGrado;
  gradoNemonico;

  //datos a mostrar
  regimen;
  anioInicio;
  anioFin;
  nombreLogin;

  //datos institucion
  currentUser: User;
  datosInstitucion = [];
  nombreInstitucion;
  codigoIntitucion;
  codigoRegimen;
  codigoAnioLectivo;
  nemonico;
  amie;


  //datos de distributivo
  listaDistributivos;
  listaDistributivofiltrado = [];
  listaDistributivoLleno = [];
  ListaMenu = [];
  ListaMenuFiltro = [];


  //lista de tutores
  listaTutores = [];
  listaTutoresFiltro = [];
  sacarFiltrosDocente = [];
  nombreDocente;

  //datos Extraidos
  codigoRegimenActual;
  codigoEstablecimiento;

  constructor(
    private readonly gieeService: GieeService,
    private readonly catalogoService: CatalogoService,
    private readonly programaStore: ProgramStoreService,
    private readonly academicoService: AcademicoService,
    private readonly ofertaService: OfertaService,
    private readonly docenteService :DocenteService,
    private modalService: NgbModal,
    private spinnerService: NgxSpinnerService
  ) { 
    this.currentUser = JSON.parse(localStorage.getItem("currentUser"))
    this.listaSeleccionGrado = this.programaStore.seleccionGrado;
    this.nombreDocente = this.programaStore.nombreDocente;
    this.codigoRegimenActual = this.programaStore.codigoRegimenGet;
    this.codigoEstablecimiento = this.programaStore.datosEstablecimientoGet;
  }

  cambioJornada(event){
    this.codigoJornada = event;
    this.listaDistributivofiltrado = [];
    this.listaDistributivoLleno = [];
    this.ListaMenu = [];
    this.ListaMenuFiltro = [];
    this.codigoGrado;
    this.seleccionGrados;
    this.gradosPorNivel(this.listaSeleccionGrado);
  }

  cambioGrado(event){
    this.listaDistributivofiltrado = [];
    this.listaDistributivoLleno = [];
    this.ListaMenu = [];
    this.ListaMenuFiltro =[];
    this.gradoNemonico = event.graNemonico;
    this.codigoGrado = event.graCodigo;
    this.sacarDatosDistributivo(event.graCodigo);
  }


   sacarGrado(cod, obj){
    this.catalogoService.buscarGradoPorCodigo(cod).then(data => {
      let listaDatos = data.objeto;
      obj.nombreGrado = listaDatos.graDescripcion;
    })
  }

   sacarParalelo(cod, obj){
    this.ofertaService.buscarcursoParaleloPorCodigo(cod).then(data => {
      let listado = data.objeto;
      if(listado.parCodigo !== null){
        this.ofertaService.buscarParaleloPorCodigo(listado.parCodigo).then(paralelo => {
          let paraleloLista = paralelo.objeto;
          obj.nombreParalelo = paraleloLista.parDescripcion;
        })
      }
    })
  }


   sacarDocente(cod, obj){
    this.docenteService.buscarDocentePorCodigo(cod).then(data => {
      let listaDocente = data.objeto; 
      obj.nombreDocente = listaDocente.nomPersona;
      obj.codigoDocente = listaDocente.codPersona;
    })
  }


  sacarJornada(cod, obj){
    this.ofertaService.buscarCursoPorCodigo(cod).then(data => {
      let listado = data.objeto;
        this.catalogoService.buscarJornadaPorCodigo(listado.jorCodigo).then(dataJor => {
          let listaJornada = dataJor.objeto;
          obj.nombreJornada = listaJornada.jorNombre;
          obj.codigoJornada = listaJornada.jorCodigo;
        })

    })
  }
  
 async rellenarDatosDistributivo(element){
  this.spinnerService.show();
  try{
    if(element.acaMallaAsignatura.acaAsignatura != null){
      this.sacarGrado(element.acaMallaAsignatura.acaAsignatura.graCodigo, element);
     this.sacarParalelo(element.curparCodigo, element);
     this.sacarDocente(element.docCodigo, element);
     this.sacarJornada(element.curCodigo, element);
   }else if(element.acaMallaAsignatura.acaAsignaturaPrepa != null){
     this.sacarGrado(element.acaMallaAsignatura.acaAsignaturaPrepa.graCodigo, element);
     this.sacarParalelo(element.curparCodigo, element);
     this.sacarDocente(element.docCodigo, element);
     this.sacarJornada(element.curCodigo, element);
   }

  } finally{
    setTimeout(() => {
      this.utilizarObjeto(element);
    }, 5000);
      
  }

      

      
 }


 async utilizarObjeto(obj){
  if(obj.codigoJornada === this.codigoJornada){

    this.listaDistributivofiltrado.push(obj);

    this.listaDistributivoLleno = [...new Set(this.listaDistributivofiltrado)];

    this.sacarImplementacion(obj)

  }else{
    this.spinnerService.hide();
  }
 }

 async sacarImplementacion(element){
    let datos = {
      curCodigo: element.curCodigo,
      cuparCodigo: element.curparCodigo,
      nombreGrado: element.nombreGrado,
      nombreParalelo: element.nombreParalelo,
      nombreJornada: element.nombreJornada,
      disCodigo: element.disCodigo,
      realCodigo: element.reanleCodigo
    }

    this.ListaMenu.push(datos);

    this.ListaMenuFiltro = this.ListaMenu.filter((item, index, self) => {
      const foundIndex = self.findIndex((el) => (
        el.curCodigo === item.curCodigo &&
        el.cuparCodigo === item.cuparCodigo &&
        el.nombreGrado === item.nombreGrado &&
        el.nombreParalelo === item.nombreParalelo &&
        el.nombreJornada === item.nombreJornada
      ));
      return index === foundIndex;
    });

    this.ListaMenuFiltro.sort((a, b) => {
      const nombreA = a.nombreParalelo.toUpperCase();
      const nombreB = b.nombreParalelo.toUpperCase();
    
      if (nombreA < nombreB) {
        return -1;
      }
      if (nombreA > nombreB) {
        return 1;
      }
      return 0;
    });

    this.tutoria(this.ListaMenuFiltro);

 }


 async tutoria(obj){
  this.spinnerService.hide();
  obj.forEach(element => {
    this.academicoService.buscarTutorCurparCodigo(element.cuparCodigo).then(data => {
      let listado = data.listado;
      listado.forEach(tutor => {
        if(tutor.tuorEstado === 1){
          element.codigoTutor = tutor.tuorCodigo;
          this.sacarDocente(tutor.docCodigo, element);
        }
      });
  
    })
  });
 }




  async sacarDatosDistributivo (cod){
   await this.listaDistributivos.forEach(element => {
      if(element.acaMallaAsignatura.acaAsignatura != null){
        if(element.acaMallaAsignatura.acaAsignatura.graCodigo === cod){
          this.rellenarDatosDistributivo(element);
        }
      }else if(element.acaMallaAsignatura.acaAsignaturaPrepa.graCodigo === cod){
          this.rellenarDatosDistributivo(element);
      }
    });
  }

  async gradosPorNivel (objeto){
      this.catalogoService.buscarGradosPorNivel(objeto.nivCodigo).then(data => {
        this.listaGrados = data.listado;
      })
  }


 async jornadaPorCodigo (obj){
    this.catalogoService.listarJornadas().then(data => {
      this.listaJornada = data.listado;
      this.codigoJornadaSeleccionada = obj.jorCodigo;
    
      if (this.codigoJornadaSeleccionada === 4) {
        this.listaJornada = this.listaJornada.filter(codigo => codigo.jorCodigo === 1 || codigo.jorCodigo === 3);
      } else if (this.codigoJornadaSeleccionada === 5) {
        this.listaJornada = this.listaJornada.filter(codigo => codigo.jorCodigo === 1 || codigo.jorCodigo === 2);
      } else if (this.codigoJornadaSeleccionada === 6) {
        this.listaJornada = this.listaJornada.filter(codigo => codigo.jorCodigo === 2 || codigo.jorCodigo === 3);
      } else if (this.codigoJornadaSeleccionada === 7) {
        this.listaJornada = this.listaJornada.filter(codigo => codigo.jorCodigo === 1 || codigo.jorCodigo === 2 || codigo.jorCodigo === 3);
      } else {
        this.listaJornada = this.listaJornada.filter(codigo => codigo.jorCodigo === this.codigoJornadaSeleccionada);
      }
    });
  }

  
  sacarDistributivos(realmCodigo, insestCodigo){
    let datos ={
       reanleCodigo: realmCodigo,
       insestCodigo: insestCodigo,
       disEstado: 1
    }

    this.academicoService.buscarDistributivoPorRealmCodigoYInsestCodigo(datos).subscribe({
      next: (respuesta) => {
        this.listaDistributivos = respuesta.listado;
      },
      error: (error) =>{
      }
    })

  }

  ngOnInit(){

    this.academicoService.listaTutor().then(data => {
      let listado = data.listado;

      listado.forEach(element => {
        if(element.tuorEstado === 1){
          this.listaTutores.push(element)
        }
      });

    })

    this.sacarDistributivos(this.codigoRegimenActual, this.codigoEstablecimiento);
    
  this.jornadaPorCodigo(this.listaSeleccionGrado);

    this.nombreLogin = this.currentUser.nombre;
    this.datosInstitucion.push(this.currentUser.sede);
    this.amie = this.datosInstitucion[0].nemonico;
    this.nombreInstitucion = this.datosInstitucion[0].descripcion;
    this.nemonico = this.datosInstitucion[0].nemonico;

    this.contentHeader = {
      headerTitle: 'Seleccion Grado',
      actionButton: false,
      breadcrumb: {
          type: '',
          links: [
              {
                name: 'Inicio',
                isLink: true,
                link: '/pages/inicio'
              },
              {
                name: 'seleccionGrados',
                isLink: true,
                link: '/pages/seleccionGrados'
              },
              {
                name: 'asignacionTutores',
                isLink: false
              }
          ]
      }
    };
    
  }


  asignarTutor(est){

   this.programaStore.totalDistributivos=this.listaDistributivos;
   this.programaStore.seleccionGrado=est;

   const Refet = this.modalService.open(ModaltutorComponent);

   Refet.result.then((result) => {
    this.listaDistributivofiltrado = [];
    this.listaDistributivoLleno = [];
    this.ListaMenu = [];
    this.ListaMenuFiltro =[];
    this.sacarDatosDistributivo(this.codigoGrado);

  });

  }

  openEliminarModal(est){
    Swal.fire({
      title: '¿Esta seguro que quiere eliminar al tutor?',
      icon: 'info',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Si',
      cancelButtonText: 'No'
    }).then((result) =>{
      if(result.isConfirmed){

        this.academicoService.EliminarTutor(est.codigoTutor).then(data => {
          Swal.fire({
            title: 'Dato Eliminado',
            text: "Tutor Eliminado",
            icon: 'success',
            confirmButtonColor: '#3085d6',
            confirmButtonText: 'ok'
          }).then((result) => {
            if(result.isConfirmed){
              this.listaDistributivofiltrado = [];
              this.listaDistributivoLleno = [];
              this.ListaMenu = [];
              this.ListaMenuFiltro =[];
              this.sacarDatosDistributivo(this.codigoGrado);
            }
          })
        })

      }
    })
  }


  generarPdf(obj){



    const bodyData = obj.map((datos) => [datos.nombreDocente, datos.nombreGrado, datos.nombreParalelo, datos.nombreJornada]);

    // Ordenar por nombreGrado y luego por nombreParalelo
bodyData.sort((a, b) => {
  // Comparar por nombreGrado
  const comparacionGrado = a[1].localeCompare(b[1]);

  // Si nombreGrado es igual, comparar por nombreParalelo
  if (comparacionGrado === 0) {
    return a[2].localeCompare(b[2]);
  }

  return comparacionGrado;
});

const pdfDefinition: any = {
  content: [
    {
      text: this.nombreInstitucion + " - " + this.nemonico, // Título
      style: 'titulo',
      alignment: 'center', // Centrar el texto
      margin: [0, 0, 0, 10], // Margen inferior para separar el título de la tabla
    },
    {
      columns: [
        {
          text: 'Establecimiento: Matriz', // Subtítulo 1
          style: 'subtitulo',
          width: 'auto',
        },
        {
          text: this.nombreLogin, // Subtítulo 2
          style: 'subtitulo',
          width: 'auto',
          margin: [150, 0, 0, 0], // Ajustar el margen derecho para separar los subtítulos
        },
      ],
      margin: [0, 0, 0, 10], // Margen inferior para separar los subtítulos de la tabla
    },
    {
      table: {
        body: [
          ['Tutor', 'Grado', 'Paralelo', 'Jornada'],
          ...bodyData,
        ],
        widths: ['auto', 'auto', 'auto', 'auto'], // Ancho automático para las columnas
      },
      style: 'datosTabla',
      alignment: 'center', // Centrar la tabla
    },
  ],
  styles: {
    titulo: {
      fontSize: 16, // Tamaño de la letra mediana para el título
      bold: true, // Negrita
    },
    subtitulo: {
      fontSize: 12, // Tamaño de la letra para los subtítulos
      bold: true, // Negrita
    },
    datosTabla: {
      fontSize: 8, // Tamaño original de la letra de la tabla
      margin: [60, 10, 0, 10], // Margen inferior para separar la tabla de otros elementos
      fillColor: '#F2F2F2', // Color de fondo de la tabla
    },
  },
};

    
          this.spinnerService.hide();
        const pdf = pdfMake.createPdf(pdfDefinition);
        pdf.open();


  }


  async desarrolloDatos(obj) {
    try {
      for (const element of obj) {
        const grado = this.sacarGrado(element.codGrado, element);
        const paralelo = this.sacarParalelo(element.curparCodigo, element);
        const docente = this.sacarDocente(element.docCodigo, element);
        const jornada = this.sacarJornada(element.curCodigo, element);
  
        // Espera a que todas las operaciones síncronas se completen antes de continuar
        await grado;
        await paralelo;
        await docente;
        await jornada;
  
        // Agrega el elemento a sacarFiltrosDocente después de que todas las funciones han terminado
        this.sacarFiltrosDocente.push(element);
      }
  
      // Puedes realizar acciones adicionales después de que todas las funciones han terminado
      setTimeout(() => {
        this.generarPdf(this.sacarFiltrosDocente);
      }, 9000);
    } catch (error) {
      console.error('Error en desarrolloDatos:', error);
    }
  }


  async sacarDatos(){
    this.listaTutores.forEach(element => {
      this.ofertaService.buscarCursoPorCodigo(element.curCodigo).then(data => {
        let lista = data.objeto;
        if(lista.jorCodigo === this.codigoJornada){
          element.codGrado = lista.graCodigo;
          this.listaTutoresFiltro.push(element);
        }
      })
    });

    setTimeout(() => {
      this.desarrolloDatos(this.listaTutoresFiltro);
    }, 3000);

  }

  generarReporte(){
    this.spinnerService.show();
    this.sacarFiltrosDocente = [];
    this.listaTutoresFiltro = [];
    this.sacarDatos();
  }

}
