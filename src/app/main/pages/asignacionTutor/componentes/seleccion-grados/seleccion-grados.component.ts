import { Component, OnInit } from '@angular/core';
import { User } from 'app/auth/models';
import Swal from 'sweetalert2/dist/sweetalert2.js';
import { GieeService } from "../../servicios/giee.service";
import { CatalogoService } from "../../servicios/catalogo.service";
import { ProgramStoreService } from "../../servicios/program-store.service";
import { Router } from "@angular/router";

@Component({
  selector: 'app-seleccion-grados',
  templateUrl: './seleccion-grados.component.html',
  styleUrls: ['./seleccion-grados.component.scss']
})
export class SeleccionGradosComponent implements OnInit {

  page = 1;
  pageSize = 4;
  datoEvent;

  contentHeader: object;

  //datos de lista permisos
  objetoPermisos = [];
  objetoPermisosFiltro = [];

   //datos institucion
   currentUser: User;
   datosInstitucion = [];
   nombreInstitucion;
   codigoIntitucion;
   codigoRegimen;
   codigoAnioLectivo;
   nemonico;
   amie;

   //datos a mostrar
   regimen;
   anioInicio;
   anioFin;
   nombreLogin;


   //lista de permisos
   listaPermisos;

   //variables
   codigoPoblacion
   listaPoblacionObjetiva;


   //codigoParaPasar
   codigoEstablecimiento;
   codigoRegimenActual;


  constructor(
    private readonly gieeService: GieeService,
    private readonly catalogoService: CatalogoService,
    private readonly programaStore: ProgramStoreService,
    private readonly router: Router,
    ) {
    this.currentUser = JSON.parse(localStorage.getItem("currentUser"))
   }

   async sacarAnioElectivo(codAnio, codRegimen){
    this.catalogoService.buscarRegimenPorCodigoDescipcion(codRegimen).then(data => {
      let objetoRegimen = data.objeto;
      
      this.regimen = objetoRegimen.regDescripcion;
    })

    this.catalogoService.buscarAnioElectivoPorCodigo(codAnio).then(data => {
      let objetoAnioElectivo = data.objeto;

      this.anioInicio = objetoAnioElectivo.anilecAnioInicio;
      this.anioFin = objetoAnioElectivo.anilecAnioFin;
    })

   }

   async añadirEspecialidad(objeto){
    objeto.forEach(element => {
      if(element.espbtCodigo !== null){
        this.catalogoService.buscarEspecialidadPorCodigo(element.espbtCodigo).then(data => {
          let listaSistema = data.objeto;

          element.descripcionEspecialidad = listaSistema.espDescripcion;

          this.catalogoService.buscarFiguraProfesional(element.figprobtCodigo).then(data => {
            let listaFigurasProfesionales = data.objeto;

            element.descripcionFiguraPro = listaFigurasProfesionales.figproDescripcion;

          })

        })

      }

    });

   }

   async añadirDescripcion(objeto){

    objeto.forEach(element => {
      this.catalogoService.buscarNivelPorCodigo(element.nivCodigo).then(data => {
        let datosNivel = data.objeto;
        element.nivelDescripcion = datosNivel.nivDescripcion;
      })
    });
   }

   async sacarListaPermisoNivel(codRegimen){

    this.gieeService.buscarPermisoNivelPorInstitucion(codRegimen).then(data => {
      let listaNiveles = data.listado;

      listaNiveles.forEach(element => {
        this.objetoPermisos.push(element);
      })

      this.objetoPermisosFiltro = this.objetoPermisos.filter((item, index, self) => {
        const foundIndex = self.findIndex((el) => (
          el.nivCodigo === item.nivCodigo
        ));
        return index === foundIndex;
      });

      this.añadirDescripcion(this.objetoPermisosFiltro);

      this.añadirEspecialidad(this.objetoPermisosFiltro);

    })
   }



   
   async sacarInstitucionPorAmie(cod){
    this.gieeService.buscarInstitucionPorAmie(cod).then(data => {
      let listaIntitucion =data.objeto;
      this.sacarListaPermisoNivel(listaIntitucion.insCodigo);

      this.gieeService.buscarRegimenPorInstitucion(listaIntitucion.insCodigo).then(data => {
        let lista = data.listado;
  
        lista.forEach(element => {
  
        this.codigoRegimen = element.regCodigo;
  
        this.catalogoService.buscarRegimenPorCodigo(element.regCodigo).then(data => {
          let listaRegimen = data.listado;
          listaRegimen.forEach(element => {
            if(element.reanleTipo === "A"){
              this.codigoAnioLectivo = element.anilecCodigo;
              this.codigoRegimenActual = element.reanleCodigo;
            }
          });
  
          this.sacarAnioElectivo(this.codigoAnioLectivo, this.codigoRegimen);
  
        })
  
        });
  
        
  
      })

    })
   }

   cambioPoblacion(event){
    this.codigoPoblacion = event;
    this.sacarInstitucionPorAmie(this.amie);
    this.sacarEstablecimiento(this.amie);
   }

   sacarEstablecimiento(cod){
    this.gieeService.getEstablecimientoPorAmie(cod).then(data => {
      let lista = data.listado;
      lista.forEach(element => {
        this.codigoEstablecimiento = element.insestCodigo;
      });

    })
   }

  ngOnInit(){
    this.nombreLogin = this.currentUser.nombre;
    this.datosInstitucion.push(this.currentUser.sede);
    this.amie = this.datosInstitucion[0].nemonico;
    this.nombreInstitucion = this.datosInstitucion[0].descripcion;
    this.nemonico = this.datosInstitucion[0].nemonico;

    this.catalogoService.getPoblacionObjetivo().then(data => {
      this.listaPoblacionObjetiva = data.listado
    })


    this.contentHeader = {
      headerTitle: 'Seleccion Grado',
      actionButton: false,
      breadcrumb: {
          type: '',
          links: [
              {
                name: 'Inicio',
                isLink: true,
                link: '/pages/inicio'
              },
              {
                name: 'seleccionGrados',
                isLink: false
              }
          ]
      }
    };

  }


  async seleccionarGrado(est){
    this.programaStore.seleccionGrado = est
    this.programaStore.datosEstablecimientoSet=this.codigoEstablecimiento;
    this.programaStore.codigoRegimenSet=this.codigoRegimenActual;
    this.router.navigate(["pages/asignacionTutores"])
  }

}
