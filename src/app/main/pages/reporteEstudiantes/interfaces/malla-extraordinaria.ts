export interface mallaExtraordinariaInterface{
             maasiexCodigo: number;
             maasiexNemonico: string;
             maasiexEstado: number;
             maasiexHorasPre: number;
             maasiexHorasAuto: number;
             maasiexHraTotal: number;
             curextCodigo: number;
             reanleCodigo: number;
}