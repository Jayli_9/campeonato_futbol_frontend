export interface asignaturaExtraordinariasInterface{
    curextCodigo: number;
    maasiexCodigo: number;
    maasiexEstado: number;
    maasiexHorasAuto: number;
    maasiexHorasPre: number;
    maasiexHraTotal: number;
    maasiexNemonico: string;
    reanleCodigo: number;
}