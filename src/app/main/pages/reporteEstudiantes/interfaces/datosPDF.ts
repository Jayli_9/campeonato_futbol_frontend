export interface datosPDFInterface{
    id: number;
    area: string;
    materia: string;
    nivCodigo: string;
    horas: number;
}