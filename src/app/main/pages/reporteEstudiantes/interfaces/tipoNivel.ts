export interface tipoNivelInterface{
     tipnivCodigo: number;
     tipnivDescripcion: string;
     tipnivEstado: number;
     tipnivFechaCreacion: number;
}