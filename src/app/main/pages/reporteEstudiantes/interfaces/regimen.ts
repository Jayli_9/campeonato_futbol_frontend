export interface regimenInterface{
       regCodigo: number;
       regDescripcion: string;
       regEstado: number;
       regFechaCreacion: Date;
       regAnioLecActual: number;
       regAnioLecSiguiente: number;
}