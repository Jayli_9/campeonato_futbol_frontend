export interface mallaInterface{
       maasCodigo: number;
       curCodigo: number;
       maasEstado: number;
       maasHoras: number;
       maasNemonico: string;
       reanleCodigo: number;
       insestCodigo: number;
       acaAsignatura: {
         asiCodigo: number;
         asiDescripcion: string;
         asiHoras: number;
         asiEstado: number;
         asiNemonico: string;
         espCodigo: number;
         graCodigo: number;
         acaAgrupacion: {
           agrCodigo: number;
           agrDescripcion: string;
           agrEstado: number;
           agrNemonico: string;
        },
         acaAreaConocimiento: {
           arcoCodigo: 1,
           arcoDescripcion: string;
           arcoEstado: string;
           arcoNemonico: string;
        },
         acaTipoAsignatura: {
           tiasCodigo: number;
           tiasDescripcion: string;
           tiasEstado: number;
           tiasObligatorio: string;
        },
         acaTipoValoracion: {
           tivaCodigo: number;
           tivaDescripcion: string;
           tivaEstado: number;
        }
      },
      acaAsignaturaPrepa: null
}