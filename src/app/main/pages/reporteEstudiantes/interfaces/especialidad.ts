export interface especialidadInterface{
       espCodigo: number;
       espDescripcion: string;
       espEstado: number;
       espFechaCreacion: Date;
       codigoFiguraProfesional: number;

}