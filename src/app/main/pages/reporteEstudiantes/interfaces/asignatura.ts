export interface AsignaturasInterface{
         asiCodigo: number;
         asiDescripcion: string;
         asiHoras: number;
         asiEstado: number;
         asiNemonico: string;
         espCodigo: number;
         graCodigo: number;
         acaAgrupacion: {
             agrCodigo: number;
             agrDescripcion: string;
             agrEstado: number;
             agrNemonico: string;
        },
         acaAreaConocimiento: {
             arcoCodigo: number;
             arcoDescripcion: string;
             arcoEstado: number;
             arcoNemonico: string;
        },
         acaTipoAsignatura: {
             tiasCodigo: number;
             tiasDescripcion: string;
             tiasEstado: number;
             tiasObligatorio: string;
        },
         acaTipoValoracion: {
             tivaCodigo: number;
             tivaDescripcion: string;
             tivaEstado: number;
        }
}