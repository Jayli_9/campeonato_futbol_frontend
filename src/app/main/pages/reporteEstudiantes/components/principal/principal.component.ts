import { Component, OnInit } from '@angular/core';

//pdf
import pdfMake from 'pdfmake/build/pdfmake';
import pdfFonts from 'pdfmake/build/vfs_fonts';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import Swal from 'sweetalert2';
import { CatalogoService } from '../../services/catalogo.service';
import { GieeService } from '../../services/giee.service';
import { User } from 'app/auth/models';
import { AcademicoService } from '../../services/academico.service';
import { NgxSpinnerService } from 'ngx-spinner';
import { asignaturaExtraordinariasInterface } from '../../interfaces/asignaturasExtraor';
import { datosPDFInterface } from '../../interfaces/datosPDF';
import { animate } from '@angular/animations';
pdfMake.vfs = pdfFonts.pdfMake.vfs;

@Component({
  selector: 'app-principal',
  templateUrl: './principal.component.html',
  styleUrls: ['./principal.component.scss']
})
export class PrincipalComponent implements OnInit {

  datosDeMalla;

  contentHeader: object;


  //variables de isntitucion
  codigoAmie = "";
  nombreInstitucion;
  codigoIntitucion;
  verFormulario = 2;

  //variable para ver que pdf se crea
  pdfCreado;

  //lista de region anio lectivo
  listaRegimenAnioElectivo = [];
  listaAnioElectivo = [];
  codigoRegimenAnioElectivo=0;
  codigoAnioElectivo=0;

  //vairbales de especialidad
  listaEspecialidad;
  codigoEspecialidades;
  nombreEspecialidad;

  //seleccion de datos
  estudiantesSeleccionados = [];

  currentUser: User


  //datos de tipoNivel
  tipoNivel;

  //malla
  listaMalla = [];
  datoTotal;

  //datos para mostrar
  mostrarListaMaterias: datosPDFInterface[] = [];
  constante: number = 0;


  //datos para nivel
  codigoNivel;
  listaNiveles;

   //variables de figura profesional
   codigoFiguraProfesional;
   listaFiguraProfesional;

     //vairables de grados
  listaGrados = [];


  //vairables de listaMalla
  listaMallas;



  constructor(
    private readonly catalogoService: CatalogoService,
    private readonly gieeService: GieeService,
    private readonly adecademicoService: AcademicoService,
    private spinnerService: NgxSpinnerService
  ) {
    this.currentUser = JSON.parse(localStorage.getItem("currentUser"))
    this.buscarInstitucionAmie(this.currentUser.sede.nemonico);
   }

   getFiguraProfesionalFiltrados(): any[] {
    return this.listaFiguraProfesional.filter(codigo => codigo.figproCodigo !== 22 && codigo.figproCodigo !== 23);
  }


  async sacarEstablecimiento(codigo){

    this.gieeService.buscarEstablecimientoPorInstitucion(codigo).then(data => {
      let listasEstablecimiento = data.listado;

      listasEstablecimiento.forEach(element => {

        if(element.nombreTipo === "MATRIZ"){
          this.codigoIntitucion = element.insestCodigo;
        }
        
      });
    })


  }


  buscarInstitucion(amie){
    this.listaRegimenAnioElectivo = [];
    this.codigoAmie;
    this.codigoRegimenAnioElectivo;
    this.codigoAnioElectivo;
    this.codigoNivel;
    this.codigoFiguraProfesional;
    this.nombreEspecialidad = null;
    this.estudiantesSeleccionados = [];
    this.codigoEspecialidades=0;
    this.listaFiguraProfesional;
    this.listaEspecialidad;
    this.listaGrados = [];

    this.gieeService.buscarInstitucionPorAmie(amie).then(data => {
      let listado = data.objeto;



      // Verificar si listado es un array y su longitud es diferente de 0
    if (listado.insCodigo === null) {
      // Tu lógica cuando el array tiene elementos
           this.verFormulario = 2;

           Swal.fire({
            icon: "error",
            title: "Atención",
            text: "El amie colocado no corresponde a ninguna Institución"
          });

      }else{

        this.nombreInstitucion = listado.nombreInstitucion;
        let insCodigo = listado.insCodigo;

        this.gieeService.buscarRegimenPorCodigoInstitucion(listado.insCodigo).then(data => {

          let lista = data.listado;

          lista.forEach(element => {
            this.catalogoService.buscarRegimenPorCodigo(element.regCodigo).then(data => {
              let listadoData = data.objeto;

              this.listaRegimenAnioElectivo.push(listadoData);

            })
          });

        })

        this.verFormulario = 1;

        Swal.fire({
          title: "Datos Cargados",
          text: "Datos Institución cargados",
          icon: "success"
        });

        this.sacarEstablecimiento(insCodigo);

      }

    })

  }

  ngOnInit(){

    this.adecademicoService.buscarListarMalla().then(data => {
      this.listaMallas = data.listado;
    })

    this.catalogoService.listaFiguraProfesional().then(data => {
      this.listaFiguraProfesional = data.listado;
    })

    this.catalogoService.listarNiveles().then(data => {
      this.listaNiveles = data.listado;
    })

    let pagianaSiguiente = 3;
    let paginaInicial = 0;


    this.contentHeader = {
      headerTitle: 'Reporte Planes Estudio',
      actionButton: false,
      breadcrumb: {
          type: '',
          links: [
              {
                name: 'Inicio',
                isLink: true,
                link: '/pages/inicio'
              },
              {
                name: 'ReportePlanesEstudio',
                isLink: false
              }
          ]
      }
    };
  }


  seleccionNivel(event){
    this.nombreEspecialidad = null;
    this.estudiantesSeleccionados = [];
    this.codigoEspecialidades=0;
    this.listaFiguraProfesional;
    this.listaEspecialidad;
    this.listaGrados = [];

    if(event != 8){
      this.catalogoService.buscarGradosPorNiveles(event).then(data=> {
        let listaGrados = data.listado;
        listaGrados.forEach(element => {
          this.listaGrados.push(element);
        });
      })
    }

    this.sacarTipoNivel(event);

  }

  async sacarTipoNivel(cod){

    this.catalogoService.buscarNivelesPorCodigo(cod).then(data => {
      let nivel = data.objeto;

      this.catalogoService.buscarTipoNivelPorCodigo(nivel.tipnivCodigo).then(tipoNivel => {
        const descipcion = tipoNivel.tipnivDescripcion;
        this.tipoNivel = descipcion;
      })

    })

  }

  buscarInstitucionAmie(amie: string) {
    this.gieeService.buscarAmiePorCodigo(amie).then(data => {
      let institucion = data.objeto
      for(let i = 0; i < institucion.regimenes.length; i++){
        this.buscarRegimenPorCodigo(institucion.regimenes[i]["insregCodigo"]);
      }
    })
  }


  buscarRegimenPorCodigo(cod){

      this.catalogoService.regimenListado().then(data => {
        let lista = data.listado; 

        lista.forEach(element => {
        });

      })
  }

  cambioAnioElectivo(event){
    this.codigoAnioElectivo =event;
    console.log(this.codigoAnioElectivo);
  }

  seleccionEspecialidad(event){

    this.codigoEspecialidades = event;
    this.nombreEspecialidad = null;

    this.listaEspecialidad.forEach(element => {

      if(element.espCodigo == event){

        this.nombreEspecialidad = element.espDescripcion;

      }

    });

    this.estudiantesSeleccionados = [];
    this.listaGrados = [];


    if(this.codigoEspecialidades != 0){

      this.catalogoService.buscarGradosPorNiveles(this.codigoNivel).then(data=> {
        let listaGrados = data.listado;
  
  
        listaGrados.forEach(element => {
  
          this.listaGrados.push(element);
  
        });
  
  
      })

    }

  }


  seleccionFiguraPro(event){
    this.catalogoService.buscarEspecialidadPorFiguraProf(event).then(data=>{
      this.listaEspecialidad = data.listado;
    })
  }

  cambioRegimen(event){

    this.codigoRegimenAnioElectivo = event;

    this.listaAnioElectivo=[];

    this.catalogoService.buscarRegimenAnioelectivo(event).then(data => {
      let listado = data.listado;
      listado.forEach(element => {
        this.catalogoService.buscarCodigoAnioLectivo(element.anilecCodigo).then(data => {
          let listaDatos = data.objeto;

          let datos = {
            realcodgio: element.reanleCodigo,
            datosObjeto: listaDatos
          }

          this.listaAnioElectivo.push(datos);
        })
      });
    })

  }

   // Método para manejar cambios en la selección de checkbox
   toggleCheckbox(grado): void {
    const index = this.estudiantesSeleccionados.findIndex(e => e.graCodigo === grado.graCodigo);

    if (index === -1) {
      this.estudiantesSeleccionados = [...this.estudiantesSeleccionados, grado];
    } else {
      this.estudiantesSeleccionados = this.estudiantesSeleccionados.filter(e => e.graCodigo !== grado.graCodigo);
    }
  }


  async diseñoPDF(mostrarListaMaterias){

    if(this.pdfCreado == 1){

      let result = mostrarListaMaterias.filter((item,index)=>{
        return mostrarListaMaterias.indexOf(item) === index;
      })
  
      const pdfDefinition: any = {
        content: [
          {
            table: {
              body: [
                [
                  { text: 'ASIGNATURAS' , colSpan: 2},
                  {},
                  {},
                  {}
                ],
                [
                  {text: 'Áreas'},
                  {text: 'Asignatura'},
                  {text: 'Categoría Nivel',},
                  {text:  'Horas Asignadas',}
                ],
                ...result.map(item => [item.area, item.materia, item.nivCodigo, item.horas]),
              ],
            },
            style: 'datosTabla'
          },
        ],
        styles: {
          datosTabla: {
            fontSize: 6,
            margin: [0, 5, 0, 15], // Margen inferior para separar la tabla de otros elementos
            fillColor: '#F2F2F2', // Color de fondo de la tabla
          },
        },
      }
          const pdf = pdfMake.createPdf(pdfDefinition);
          pdf.open();
          this.spinnerService.hide();
          location.reload();

    }else if(this.pdfCreado == 2){

      let result = mostrarListaMaterias.filter((item,index)=>{
        return mostrarListaMaterias.indexOf(item) === index;
      })
  
      const pdfDefinition: any = {
        content: [
          {
            table: {
              body: [
                [
                  { text: 'ASIGNATURAS' , colSpan: 1},
                  {},
                  {}
                ],
                [
                  {text: 'Asignatura'},
                  {text: 'Categoría Nivel',},
                  {text:  'Horas Asignadas',}
                ],
                ...result.map(item => [item.materia, item.nivCodigo, item.horas]),
              ],
            },
            style: 'datosTabla'
          },
        ],
        styles: {
          datosTabla: {
            fontSize: 6,
            margin: [0, 5, 0, 15], // Margen inferior para separar la tabla de otros elementos
            fillColor: '#F2F2F2', // Color de fondo de la tabla
          },
        },
      }
          const pdf = pdfMake.createPdf(pdfDefinition);
          pdf.open();
          this.spinnerService.hide();
          location.reload();

    }
  }


  async creacionPDF(listaMalla){
    const a = 0;

    try {

      listaMalla.forEach(element => {
        if(element.acaAsignatura != null){

          this.adecademicoService.buscarAsignaturaPorCodigo(element.acaAsignatura.asiCodigo).then(data =>{
            let datosMaterias = data.objeto;
    
            this.estudiantesSeleccionados.forEach(element => {
    
              if(datosMaterias.graCodigo === element.graCodigo){
    
                this.catalogoService.buscarGradosPorCodigo(datosMaterias.graCodigo).then(datos => {
                  let listaMateria = datos.objeto;

                  if(listaMateria.nivCodigo === this.codigoNivel){

                    this.pdfCreado = 1;


                    if(datosMaterias.asiHoras > 20){
                      this.constante = 20
                    }else{
                      this.constante = datosMaterias.asiHoras;
                    }
      
                    let i = a + 1;
      
                    if(listaMateria.nivCodigo != null){
                      if(listaMateria.nivCodigo !=null){
                        let completo={
                          id: i,
                          area: datosMaterias["acaAreaConocimiento"].arcoDescripcion,
                          materia: datosMaterias.asiDescripcion,
                          nivCodigo: this.tipoNivel,
                          horas: this.constante
                        }
                        this.mostrarListaMaterias.push(completo);
                      }else{
                        let completo={
                          id: i,
                          area: datosMaterias["acaAreaConocimiento"].arcoDescripcion,
                          materia: datosMaterias.asiDescripcion,
                          nivCodigo: this.tipoNivel,
                          horas: this.constante
                        }
                        this.mostrarListaMaterias.push(completo);
                      }
                    }else{
                      let completo={
                        id: i,
                        area: datosMaterias["acaAreaConocimiento"].arcoDescripcion,
                        materia: datosMaterias.asiDescripcion,
                        nivCodigo: this.tipoNivel,
                        horas: this.constante
                      }
                      this.mostrarListaMaterias.push(completo);
                    }

                  }
                })
              }
            });
            
    
          })

        }else{

          this.estudiantesSeleccionados.forEach(data => {

            if(element.acaAsignaturaPrepa.graCodigo === data.graCodigo){

              this.catalogoService.buscarGradosPorCodigo(element.acaAsignaturaPrepa.graCodigo).then(datos => {
                let listaMateria = datos.objeto;

                if(listaMateria.nivCodigo === this.codigoNivel){

                  this.pdfCreado = 2;

                  if(element.acaAsignaturaPrepa.apreHras > 20){
                    this.constante = 20
                  }else{
                    this.constante = element.acaAsignaturaPrepa.apreHras
                  }
    
                  let i = a + 1;
    
                  if(listaMateria.nivCodigo != null){
                    if(listaMateria.nivCodigo !=null){
                      let completo={
                        id: i,
                        area: element.acaAsignaturaPrepa.resAgrupacion,
                        materia: element.acaAsignaturaPrepa.apreDescripcion,
                        nivCodigo: this.tipoNivel,
                        horas: this.constante
                      }
                      this.mostrarListaMaterias.push(completo);
                    }else{
                      let completo={
                        id: i,
                        area: element.acaAsignaturaPrepa.resAgrupacion,
                        materia: element.acaAsignaturaPrepa.apreDescripcion,
                        nivCodigo: this.tipoNivel,
                        horas: this.constante
                      }
                      this.mostrarListaMaterias.push(completo);
                    }
                  }else{
                    let completo={
                      id: i,
                      area: element.acaAsignaturaPrepa.resAgrupacion,
                      materia: element.acaAsignaturaPrepa.apreDescripcion,
                      nivCodigo: this.tipoNivel,
                      horas: this.constante
                    }
                    this.mostrarListaMaterias.push(completo);
                  }

                }
              })
            }
          });


        }


    });
      
    } finally {

      this.diseñoPDF(this.mostrarListaMaterias);
      
    }

  }

  async crearPDF(){
    this.spinnerService.show();
    this.listaMalla=[];
    this.mostrarListaMaterias=[];
    this.constante = 0;

     try {
        this.listaMallas.forEach(element => {
          if(element.reanleCodigo === this.codigoAnioElectivo && element.insestCodigo == this.codigoIntitucion){
              this.listaMalla.push(element);
          }
         });
      
     } finally {
        this.creacionPDF(this.listaMalla);
      
     }

    

     

    

   
  }

  convertirAMayusculas() {
    this.codigoAmie = this.codigoAmie.toUpperCase();
  }

  convertirAMayusculasInput() {
    this.codigoAmie = this.codigoAmie.toUpperCase();
  }

}


function convertirAMayusculas(input) {
  // Convierte el valor del input a mayúsculas
  input.value = input.value.toUpperCase();
}


