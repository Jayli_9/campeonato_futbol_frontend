import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { environment } from 'environments/environment';
import { ResponseGenerico } from '../interfaces/response-generico';
import { institucionInterface } from '../interfaces/institucion';
import { amieInstitucionesInterface } from "../interfaces/amieIntituciones";
import { establecimientoInterface } from "../interfaces/establecimineto";
import { regimenInstitucionInterface } from "../interfaces/regimenInstitucion";

@Injectable({
  providedIn: 'root'
})
export class GieeService {

  private readonly URL_REST = environment.url_institucion;

constructor(private _http: HttpClient) { }

buscarAmiePorCodigo(cod):  Promise<ResponseGenerico<institucionInterface>>{
  return new Promise((resolve, reject) => {
this._http.get(`${environment.url_institucion}/private/buscarInstitucionPorAmie/` + cod).subscribe((response: ResponseGenerico<institucionInterface>) => {
   resolve(response);
 }, reject);
})
}


buscarInstitucionPorAmie(cod): Promise<ResponseGenerico<amieInstitucionesInterface>>{
  return new Promise((resolve, reject) => {
this._http.get(`${environment.url_institucion}/private/buscarInstitucionPorAmie/` + cod).subscribe((response: ResponseGenerico<amieInstitucionesInterface>) => {
   resolve(response);
 }, reject);
})
}


buscarEstablecimientoPorInstitucion(cod): Promise<ResponseGenerico<establecimientoInterface>>{
  return new Promise((resolve, reject) => {
this._http.get(`${environment.url_institucion}/private/listarEstablecimientosPorInstitucion/` + cod).subscribe((response: ResponseGenerico<establecimientoInterface>) => {
   resolve(response);
 }, reject);
})
}


buscarRegimenPorCodigoInstitucion(cod): Promise<ResponseGenerico<regimenInstitucionInterface>>{
  return new Promise((resolve, reject) => {
this._http.get(`${environment.url_institucion}/private/listarInstitucionRegimenPorCodigoInstitucion/` + cod).subscribe((response: ResponseGenerico<regimenInstitucionInterface>) => {
   resolve(response);
 }, reject);
})
}

}
