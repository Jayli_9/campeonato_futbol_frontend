import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { environment } from 'environments/environment';
import { ResponseGenerico } from '../interfaces/response-generico';
import { mallaExtraordinariaInterface } from "../interfaces/malla-extraordinaria";
import { AsignaturasInterface } from '../interfaces/asignatura';
import { mallaInterface } from '../interfaces/malla';

@Injectable({
  providedIn: 'root'
})
export class AcademicoService {

  private readonly URL_REST = environment.url_academico;

constructor(private _http: HttpClient) { }

listarMallaExtraordinaria(): Promise<ResponseGenerico<mallaExtraordinariaInterface>>{
  return new Promise((resolve, reject) => {
    this._http.get(`${environment.url_academico}/private/listarMallaAsigExtra`).subscribe((response: ResponseGenerico<mallaExtraordinariaInterface>) => {
      resolve(response);
    }, reject);
  })
}


buscarMallaExtraordinariaPorCodigoRealm(cod): Promise<ResponseGenerico<mallaExtraordinariaInterface>>{
  return new Promise((resolve, reject) => {
    this._http.get(`${environment.url_academico}/private/buscarListaMallaExtraPorReanleCodigo/`+cod).subscribe((response: ResponseGenerico<mallaExtraordinariaInterface>) => {
      resolve(response);
    }, reject);
  })
}


buscarAsignaturaPorCodigo(cod): Promise<ResponseGenerico<AsignaturasInterface>>{
  return new Promise((resolve, reject) => {
    this._http.get(`${environment.url_academico}/private/buscarAsignaturaPorCodigo/`+cod).subscribe((response: ResponseGenerico<AsignaturasInterface>) => {
      resolve(response);
    }, reject);
  })
}


buscarListarMalla(): Promise<ResponseGenerico<mallaInterface>>{
  return new Promise((resolve, reject) => {
    this._http.get(`${environment.url_academico}/private/listarMallasAsignaturaActivas`).subscribe((response: ResponseGenerico<mallaInterface>) => {
      resolve(response);
    }, reject);
  })
}



}
