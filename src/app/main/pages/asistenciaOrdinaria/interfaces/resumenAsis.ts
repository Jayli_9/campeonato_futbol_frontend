export interface resumenAsisInterface{
       resasiCodigo: number;
       resasiJustf: number;
       resasiInjust: number;
       resasiAsistencia: number;
       matCodigo: number;
       resasiEstado: number;
}