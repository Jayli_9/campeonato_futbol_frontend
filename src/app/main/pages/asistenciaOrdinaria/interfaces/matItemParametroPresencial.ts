export interface MatItemParametroPresencial {
    graCodigo: number;
    itpaprCadena: string;
    itpaprCodigo: number;
    itpaprDecimal: number;
    itpaprDescripcion: string;
    itpaprEntero: number;
    itpaprEstado: number;
    itpaprFecha: Date;
    itpaprNemonico: string;
    reanleCodigo: number;
}
