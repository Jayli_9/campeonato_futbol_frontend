export interface faltasOrdinariasInterface{
       falordCodigo: number;
       meCodigo: number;
       docCodigo: number;
       matCodigo: number;
       estCodigo: number;
       curparCodigo: number;
       curCodigo: number;
       faloredFecha: Date;
       reanleCodigo: number;
       falordTipoFalta: string;
       falordObservacion: string;
       falordEstado: number;
       tuorCodigo: number;
}