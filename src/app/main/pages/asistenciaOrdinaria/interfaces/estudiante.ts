export interface estudianteInterface{
       matCodigo: number;
       matEstudiante: number;
       matEstado: number;
       estEstado: number;
       curCodigo: number;
       curparCodigo: number;
       reanleCodigo: number;
       sereduCodigo: number;
       estIdentificacion: string;
       estNombres: string;
       etnCodigo: number;
       estCodigo: number;
}