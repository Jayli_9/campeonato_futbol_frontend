import {IinfoDia} from './IinfoDia';

export interface CalendarioAsistencia{
    fechaInicial: Date;
    fechaFinal: Date;
    anios: {
        [anio: number]: {

            [mes: string]: {
                numeroMes: number;
                semanas: {
                    [semana: number]: IinfoDia[];
                };
            };
        };
    };
}
