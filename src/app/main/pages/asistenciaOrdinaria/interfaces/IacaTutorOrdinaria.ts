export interface IacaTutorOrdinaria{
    anilecAnioFin: number,
    anilecAnioInicio: number,
    anilecCodigo: number,
    anilecEstado: number,
    anilecFechaCreacion: Date
}