export interface IinfoDia {
    fecha: Date;
    diaSemana: number;
    nombreDia: string;
  }