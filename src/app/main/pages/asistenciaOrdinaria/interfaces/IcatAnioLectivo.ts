export interface IcatAnioLectivo{
    curCodigo: number,
    curparCodigo: number,
    docCodigo: number,
    reanleCodigo: number,
    tuorCodigo: number,
    tuorEstado: number,
    tuorFecha: string,
    tuotTutor: number
}