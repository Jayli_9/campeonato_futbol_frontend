import { Routes } from '@angular/router';
import { AsistenciaPrincipalComponent } from '../componentes/asistencia-principal/asistencia-principal.component';
import { JustificacionComponent } from '../componentes/asistencia-principal/justificacion/justificacion.component';

export const RUTA_ASISTENCIA_ORDINARIO: Routes = [
  {
    path: 'asistencia',
    component: AsistenciaPrincipalComponent,
  },
  {
    path: 'justificacion',
    component: JustificacionComponent,
  }
];