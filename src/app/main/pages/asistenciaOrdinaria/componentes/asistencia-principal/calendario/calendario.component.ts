import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {CalendarioAsistencia} from '../../../interfaces/calendarioAsistencia';
import {IinfoDia} from '../../../interfaces/IinfoDia';
import {AlumnosFaltaOrdinariaDTO} from '../../../interfaces/alumnosFaltaOrdinariaDTO';
import {construirCalendario, obtenerNumeroMes} from '../../../../../shared/funtions/calendario-academico.function';
import {IacaCalificacionDTO} from '../../../../calificacionOrdinaria/interfaces/IacaCalificacionDTO';
import {TutorOrdinariaService} from '../../../servicios/tutorOrdinaria.service';
import {NgxSpinnerService} from 'ngx-spinner';
import {faltasOrdinariasInterface} from '../../../interfaces/faltasOrdinaria';
import {forkJoin, Observable} from 'rxjs';
import {constanteAsistencia} from '../../../constantes/ConstanteAsistencia';
import {map} from 'rxjs/operators';


@Component({
    selector: 'app-calendario',
    templateUrl: './calendario.component.html',
    styleUrls: ['./calendario.component.scss']
})
export class CalendarioComponent implements OnInit {
    calendarioAsistencia: CalendarioAsistencia;
    anioSeleccionado: number;
    mesSeleccionado: string;
    semanaSeleccionada: number;
    diasEnSemanaSeleccionados: IinfoDia[] = [];
    alumnosFaltaOrdinaria!: AlumnosFaltaOrdinariaDTO;
    listaEstudiantesNotas: IacaCalificacionDTO[] = [];
    listaFaltasAlumnos: faltasOrdinariasInterface[];
    public columnasEncabezado: string[] = ['nombreEstudiante'];
    @Input() cursoSeleccionadoChild;
    @Output() diasSeleccionadosChanged = new EventEmitter<IinfoDia[]>();
    @Output() mesSeleccionadosChanged = new EventEmitter<string>();

    constructor(
        private tutorOrdinariaService: TutorOrdinariaService,
        private spinnerService: NgxSpinnerService,
    ) {
    }

    async ngOnInit(): Promise<void> {
        try {

            this.crearCalendario(this.cursoSeleccionadoChild.reanleCodigo).subscribe({
                next: (calendario) => {
                    this.calendarioAsistencia = calendario;

                    console.log('calendario de asistencia', this.calendarioAsistencia);
                },
                error: (error) => {
                    console.error('Se produjo un error al crear el calendario:', error);
                }
            });

            this.spinnerService.show();
        } finally {
            this.spinnerService.hide();
        }
    }

    getAnios(anios: { [anio: number]: any }): number[] {
        return Object.keys(anios).map(Number);
    }

    getMesesPorAnio(anioSeleccionado: number): string[] {
        if (this.calendarioAsistencia) {
            const anio = this.calendarioAsistencia.anios[anioSeleccionado];
            if (anio) {
                return Object.keys(anio);
            }
        }
        return [];
    }

    getSemanasPorMes(anioSeleccionado: number, mesSeleccionado: string): string[] {
        if (this.calendarioAsistencia && this.calendarioAsistencia.anios && this.calendarioAsistencia.anios[anioSeleccionado] &&
            this.calendarioAsistencia.anios[anioSeleccionado][mesSeleccionado] &&
            this.calendarioAsistencia.anios[anioSeleccionado][mesSeleccionado].semanas) {
            const semanas = this.calendarioAsistencia.anios[anioSeleccionado][mesSeleccionado].semanas;
            return Object.keys(semanas);
        }
        return [];
    }

    reestablecerListaFaltas() {
        this.diasEnSemanaSeleccionados = [];
        this.diasSeleccionadosChanged.emit(this.diasEnSemanaSeleccionados);

    }

    reiniciarSemana() {
        this.reestablecerListaFaltas()
        this.semanaSeleccionada = null;
        this.mesSeleccionadosChanged.emit(this.mesSeleccionado);

    }
    cargarInfoSemanaSeleccionada(semanaSeleccionada: number){
        this.diasEnSemanaSeleccionados = [];
        const anio = this.calendarioAsistencia.anios[this.anioSeleccionado]?.[this.mesSeleccionado]?.semanas;
        this.diasEnSemanaSeleccionados = anio ? anio[semanaSeleccionada] || [] : [];
        this.diasSeleccionadosChanged.emit(this.diasEnSemanaSeleccionados);

    }

    crearCalendario(reanleCodigo: number): Observable<CalendarioAsistencia> {
        let fechaInicial$: Observable<Date> = this.tutorOrdinariaService
            .buscarPorNemonicoAndReanleCodigo(constanteAsistencia.itpaprNemonicoFechaInicio, reanleCodigo)
            .pipe(
                map(respuesta => respuesta.totalRegistros === 1 ? respuesta.objeto.itpaprFecha : null)
            );

        let fechaFinal$: Observable<Date> = this.tutorOrdinariaService
            .buscarPorNemonicoAndReanleCodigo(constanteAsistencia.itpaprNemonicoFechaFin, reanleCodigo)
            .pipe(
                map(respuesta => respuesta.totalRegistros === 1 ? respuesta.objeto.itpaprFecha : null)
            );

        return forkJoin([fechaInicial$, fechaFinal$]).pipe(
            map(([fechaInicial, fechaFinal]) => {
                if (fechaInicial && fechaFinal) {
                    return construirCalendario(fechaInicial.toString(), fechaFinal.toString());
                } else {
                    throw new Error('Las fechas inicial o final no están disponibles.');
                }
            })
        );
    }

}
