import { ChangeDetectorRef, Component, Input, OnInit, ViewChild } from '@angular/core';
import { MatPaginator, MatPaginatorIntl } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { IacaCalificacionDTO } from 'app/main/pages/calificacionOrdinaria/interfaces/IacaCalificacionDTO';
import { CalificacionOrdinariaService } from 'app/main/pages/calificacionOrdinaria/servicios/calificacionOrdinaria.service';
import { TutorOrdinariaService } from "../../../servicios/tutorOrdinaria.service";
import { TipoJustificacionComponent } from "../justificacion/tipoJustificacion/tipoJustificacion.component";
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { NgxSpinnerService } from 'ngx-spinner';
import { formatDate } from '@angular/common';
import { orderBy } from 'lodash-es';
import * as moment from 'moment';
import { IinfoDia } from '../../../interfaces/IinfoDia';
import { ProgramStoreService } from '../../../servicios/programStore.service';
import {AlumnosFaltaOrdinariaDTO} from '../../../interfaces/alumnosFaltaOrdinariaDTO';
import {faltasOrdinariasInterface} from '../../../interfaces/faltasOrdinaria';
import {obtenerNumeroMes} from '../../../../../shared/funtions/calendario-academico.function';
import {getMonth, getYear} from 'date-fns';

@Component({
  selector: 'app-justificacion',
  templateUrl: './justificacion.component.html',
  styleUrls: ['./justificacion.component.scss']
})
export class JustificacionComponent implements OnInit {

  formatDate(date: string): string {
    return formatDate(date, 'dd', 'en-US'); // Puedes ajustar el formato según tus necesidades
}

  //variables tabla
  public columnasEncabezado: string[] = ['nombreEstudiante'];

  //checked
  checkedItems: { [key: string]: boolean } = {};
  diasEnSemanaSeleccionados: IinfoDia[] = [];
  mesSeleccionado: string;
  alumnosFaltaOrdinaria!: AlumnosFaltaOrdinariaDTO;

  listaFaltasAlumnos: faltasOrdinariasInterface[];


  /*****************************************/

  //varible de la lista de faltas
  listaFaltas = [];
  listaFaltasFiltro = [];
   listaObjetos = [];
  codigoCuros;
  
  
  reflexion;



  dataSource: MatTableDataSource<any>;
  //////////////////////////////////////////////
  anioSeleccionado: number;
/*  anioMinimo: number;
  anioMaximo: number ;
  anios: number[];
  mesSeleccionada: number; // Puedes usar números del 1 al 12 para representar los meses
  meses: { valor: number, nombre: string }[] = this.generarMeses();
  numeroSemanas: number;
  listaSemana: any[];
  semanaSeleccionada: number;
  diasEnSemanaSeleccionada: IinfoDia[] = [];*/
  /*nombresDias: string[] = ['Lunes', 'Martes', 'Miercoles', 'Jueves', 'Viernes', 'Sabado', 'Domingo'];*/
  //////////////////////////////////////////////
  //variables paginación y orden
  @ViewChild(MatPaginator) paginator!: MatPaginator;
  @ViewChild(MatSort) sort!: MatSort;
  //Inputs
  @Input() cursoSeleccionadoChild;
  @Input() anioLectivoChild;

  //variables
  public listaEstudiantesNotas: IacaCalificacionDTO[] = [];

  constructor(private calificacionOrdinariaService: CalificacionOrdinariaService,
    private spinnerService: NgxSpinnerService,
    private matPaginatorIntl: MatPaginatorIntl,
    private cdr: ChangeDetectorRef, 
    private tutorOrdinariaService: TutorOrdinariaService,
    private modalService: NgbModal,
    private readonly programaStore: ProgramStoreService) { }

   /* async sacarListadoFaltas(cur){
      this.spinnerService.show();
      this.tutorOrdinariaService.listarFaltas(cur).then(data => {
        let lista = data.listado;
        lista.forEach(element => {
            this.fechaTotal(element);
        });
      })
    }*/

   /* fechaTotal(obj){
      const timestamp = obj.faloredFecha;
      const fecha = new Date(timestamp);
      const numeroDeDia = fecha.getDate();
      const anio = fecha.getFullYear();
      obj.dia = numeroDeDia;
      obj.anio = anio;
     this.obtenerEstudiante(obj);
    }*/

    /*obtenerEstudiante(obj){
      this.tutorOrdinariaService.buscarEstudiantePorCodigoMatricula(obj.estCodigo).then(data => {
        let listaEstudiante = data.objeto;
        obj.nombreEstudiante = listaEstudiante.estNombres;
        obj.codigoEst = listaEstudiante.estCodigo;
        obj.codigoMa = listaEstudiante.matCodigo;
        this.listaFaltas.push(obj);
        this.listaFaltasFiltro = this.listaFaltas.filter(item => item.meCodigo === this.mesSeleccionada && item.anio === this.anioSeleccionado && item.curCodigo === this.codigoCuros && item.falordTipoFalta === "I");
        this.spinnerService.hide();
      })
    }*/

    async ngOnInit(): Promise<void> {
    /*  await this.sacarListadoFaltas(this.cursoSeleccionadoChild.tuorCodigo);*/

   /* try {
      this.codigoCuros = this.cursoSeleccionadoChild.curCodigo;
      this.anioMinimo = this.anioLectivoChild.anilecAnioInicio;
      this.anioMaximo = this.anioLectivoChild.anilecAnioFin;
      this.anios = this.generarAnio(this.anioMinimo, this.anioMaximo);
      await this.listarEstudiantesMatriculados(this.cursoSeleccionadoChild.reanleCodigo, this.cursoSeleccionadoChild.curCodigo, this.cursoSeleccionadoChild.curparCodigo, this.cursoSeleccionadoChild.ofeCurso.sereduCodigo);
      this.spinnerService.show();
       } finally {
      this.spinnerService.hide();
    }*/
      this.listaFaltasAlumnos=[];
      try {

        await this.listarEstudiantesMatriculados(this.cursoSeleccionadoChild.reanleCodigo, this.cursoSeleccionadoChild.curCodigo, this.cursoSeleccionadoChild.curparCodigo, this.cursoSeleccionadoChild.ofeCurso.sereduCodigo);

        this.spinnerService.show();
      } finally {
        this.spinnerService.hide();
      }
  }

  async listarEstudiantesMatriculados(reanleCodigo, curCodigo, curparCodigo, sereduCodigo) {
    try {
      this.spinnerService.show();
      const data = await this.calificacionOrdinariaService.getMatriculaOrdinariaObjetoConsulta(reanleCodigo, curCodigo, curparCodigo, sereduCodigo);
      
      this.listaEstudiantesNotas = orderBy(data.listado, ['estudiante.estNombres'], ['asc']);
      this.dataSource = new MatTableDataSource(this.listaEstudiantesNotas);
      this.dataSource.paginator = this.paginator;
      this.dataSource.sort = this.sort;
      this.matPaginatorIntl.itemsPerPageLabel = 'Elementos por página:';
      this.matPaginatorIntl.nextPageLabel = 'Siguiente:';
      this.matPaginatorIntl.previousPageLabel = 'Anterior:';
      this.cdr.detectChanges(); // Forzar la actualización
      
    } finally {
      this.spinnerService.hide();
    }
  }
  /*generarAnio(inicio: number, fin: number): number[] {
    const anios = [];
    for (let anio = inicio; anio <= fin; anio++) {
      anios.push(anio);
    }
    return anios;
  }*/
  buscarFaltasFecha(matCodigo: number, fecha: Date): faltasOrdinariasInterface | null {
    const encontrado = this.listaFaltasAlumnos.find(item => {
      const fechaItem = new Date(item.faloredFecha);
      return item.matCodigo === matCodigo && fechaItem.getTime() === fecha.getTime();
    });

    return encontrado ? encontrado : null;
  }
  async handleCheckboxChange(event: any, matCodigo: number, fecha: Date, estCodigo: number) {

    if (event.target.checked) {
      await this.buscarFaltasPorMes();
      let itemEncontrado: faltasOrdinariasInterface;
      itemEncontrado = this.buscarFaltasFecha(matCodigo, fecha);
      if (itemEncontrado) {
        this.programaStore.setFaltaEncontrada =itemEncontrado;
        const Refet = this.modalService.open(TipoJustificacionComponent);
        Refet.result.then((result) => {
          this.cargarEncabezadoFechaSeleccionada()
        });
      }

      /*this.programaStore.setEstCodigo = estCodigo;
      this.programaStore.setMatCodigo = matCodigo;
      this.programaStore.setFecha = fecha;
      this.programaStore.setDatosExternos = this.cursoSeleccionadoChild;
      this.programaStore.setCodigoCurso = this.codigoCuros;
      this.programaStore.setCodigoMes = getMonth(fecha) + 1;
      this.programaStore.setAnioSeleccion = getYear(fecha);*/


  }
}
  cargarEncabezadoFechaSeleccionada() {
    this.columnasEncabezado = ['nombreEstudiante'];
    for (const semana of this.diasEnSemanaSeleccionados) {
      this.columnasEncabezado.push(semana.nombreDia);
    }

  }
  tieneFaltasFecha(matCodigo: number, fecha: Date ): boolean {

    const encontrado = this.listaFaltasAlumnos.find(item => {
      const fechaItem = new Date(item.faloredFecha);
      return item.matCodigo === matCodigo && fechaItem.getTime() === fecha.getTime() && item.falordEstado===1;
    });
    return !!encontrado;
  }

  trackByFn(index, item) {
    return item.id;
  }

  async onDiasSeleccionadosChanged(diasSeleccionados: IinfoDia[]) {

    this.diasEnSemanaSeleccionados = diasSeleccionados;
    if(this.diasEnSemanaSeleccionados.length > 0) {
      await this.cargarEncabezadoFechaSeleccionada()
      if (this.dataSource) {
        this.dataSource.data = this.listaEstudiantesNotas.slice();
      }
      this.cdr.detectChanges();
    }
  }
  async onMesSeleccionadosChanged(mesSeleccionado: string) {
    this.mesSeleccionado=mesSeleccionado
    await this.setearAlumnosFaltaOrdinaria();
    await this.buscarFaltasPorMes();
  }
  setearAlumnosFaltaOrdinaria() {
    this.alumnosFaltaOrdinaria = {
      curparCodigo: 0, estudiantes: [], meCodigo: 0, reanleCodigo: 0, tuorCodigo: 0
    };
    this.alumnosFaltaOrdinaria.curparCodigo = this.cursoSeleccionadoChild.curparCodigo;
    this.alumnosFaltaOrdinaria.reanleCodigo = this.cursoSeleccionadoChild.reanleCodigo;
    this.alumnosFaltaOrdinaria.tuorCodigo = this.cursoSeleccionadoChild.tuorCodigo;
    this.alumnosFaltaOrdinaria.meCodigo = obtenerNumeroMes(this.mesSeleccionado);
    this.alumnosFaltaOrdinaria.estudiantes = this.listaEstudiantesNotas.map(item => item.matCodigo);
  }
  async buscarFaltasPorMes() {
    try {
      const respuesta = await this.tutorOrdinariaService.listarFaltasOrdinariaPorTutorPorCursoYMes(this.alumnosFaltaOrdinaria).toPromise();
      if (respuesta) {
        this.listaFaltasAlumnos = respuesta.listado;
      }
    } catch (error) {
      console.error('Error al obtener faltas de alumnos:', error);
    }
  }
}

