import {Component, Input, OnInit} from '@angular/core';
import {NgbActiveModal} from '@ng-bootstrap/ng-bootstrap';
import {ProgramStoreService} from 'app/main/pages/asistenciaOrdinaria/servicios/programStore.service';
import {TutorOrdinariaService} from 'app/main/pages/asistenciaOrdinaria/servicios/tutorOrdinaria.service';
import {NgxSpinnerService} from 'ngx-spinner';
import Swal from 'sweetalert2';
import {faltasOrdinariasInterface} from '../../../../interfaces/faltasOrdinaria';

@Component({
    selector: 'app-tipoJustificacion',
    templateUrl: './tipoJustificacion.component.html',
    styleUrls: ['./tipoJustificacion.component.scss']
})
export class TipoJustificacionComponent implements OnInit {
    @Input() fromParent;
    faltaOrdinariaEdicion: faltasOrdinariasInterface;

    codigoJustificacion;
    observacionFalta;
    tipoFalta;
    variableJustificacionJ = 0;
    variableJustificacionI = 0;

    //datos que traemos
    fecha;
    estCodigo;
    matCodigo;
    cursoSeleccionadoChild;
    codigoCuros;
    mesSeleccionada;
    anioSeleccionado;
    reflexion = 0;
    diasLaborables = 250;


    constructor(
        public activeModal: NgbActiveModal,
        private tutorOrdinariaService: TutorOrdinariaService,
        private readonly programaStore: ProgramStoreService,
        private spinnerService: NgxSpinnerService
    ) {
        this.faltaOrdinariaEdicion = this.programaStore.getFaltaEncontrada;

    }

    ngOnInit() {

    }

    cambioJustificacion(event) {
        this.codigoJustificacion = event;
        if (event === '1') {
            this.tipoFalta = 'J';
        } else if (event === '2') {
            this.tipoFalta = 'I';
        }
    }


    closeModal(sendData) {
        let existeCurso = false;
        if (sendData === 'ok') {
            Swal.fire({
                title: '¿Esta seguro que quiere guardar la justificacion?',
                icon: 'info',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Si',
                cancelButtonText: 'No'
            }).then((result) => {
                if (result.isConfirmed) {
                    this.spinnerService.show();
                    this.editarData();
                }
            });
        } else if (sendData === 'cancel') {
            this.activeModal.close(this.fromParent);
        } else if (sendData === 'dismiss') {
            this.activeModal.close(this.fromParent);
        }
    }


    async editarData() {
        if (this.faltaOrdinariaEdicion) {
            this.faltaOrdinariaEdicion.falordObservacion = this.observacionFalta;
            this.faltaOrdinariaEdicion.falordTipoFalta = this.tipoFalta;
            this.faltaOrdinariaEdicion.falordEstado = 0;
            await this.tutorOrdinariaService.EditarFalta(this.faltaOrdinariaEdicion).subscribe({
                next: (respuesta) => {
                    this.buscarResumenAsis();
                },
                error: (error) => {
                }
            });
        }

    }

    async editarResumenAsis(obj) {
        try {
            if (this.tipoFalta === 'J') {
                this.variableJustificacionJ = obj.resasiJustf + 1;
                this.diasLaborables = obj.resasiAsistencia - 1;
            } else if (this.tipoFalta === 'I') {
                this.variableJustificacionI = obj.resasiInjust + 1;
                this.diasLaborables = obj.resasiAsistencia - 1;
            }
        } finally {
            let datosGuardar = {
                resasiCodigo: obj.resasiCodigo,
                matCodigo: this.matCodigo,
                resasiAsistencia: this.diasLaborables,
                resasiEstado: 1,
                resasiInjust: this.variableJustificacionI,
                resasiJustf: this.variableJustificacionJ
            };

            this.tutorOrdinariaService.EditarResumenAsis(datosGuardar).subscribe({
                next: (respuesta) => {
                    this.spinnerService.hide();
                    Swal.fire({
                        title: 'Datos Guardados',
                        text: 'Datos registrados y guardados',
                        icon: 'success',
                        confirmButtonColor: '#3085d6',
                        confirmButtonText: 'ok'
                    }).then((result) => {
                        if (result.isConfirmed) {
                            this.activeModal.close(this.fromParent);
                        }
                    });
                },
                error: (error) => {
                }
            });

        }
    }

    async guardarResumenAsis() {
        try {
            if (this.tipoFalta === 'J') {
                this.variableJustificacionJ = 1;
                this.diasLaborables = this.diasLaborables - 1;
            } else if (this.tipoFalta === 'I') {
                this.variableJustificacionI = 1;
                this.diasLaborables = this.diasLaborables - 1;
            }
        } finally {
            let datosGuardar = {
                matCodigo: this.matCodigo,
                resasiAsistencia: this.diasLaborables,
                resasiEstado: 1,
                resasiInjust: this.variableJustificacionI,
                resasiJustf: this.variableJustificacionJ
            };

            this.tutorOrdinariaService.guardarResumenAsis(datosGuardar).subscribe({
                next: (respuesta) => {
                    this.spinnerService.hide();
                    Swal.fire({
                        title: 'Datos Guardados',
                        text: 'Datos registrados y guardados',
                        icon: 'success',
                        confirmButtonColor: '#3085d6',
                        confirmButtonText: 'ok'
                    }).then((result) => {
                        if (result.isConfirmed) {
                            this.activeModal.close(this.fromParent);
                        }
                    });
                },
                error: (error) => {
                }
            });

        }
    }


    async buscarResumenAsis() {
        this.tutorOrdinariaService.buscarResumenAsistenciaPorCodigoMat(this.matCodigo).then(data => {
            let datosResumenAsis = data.objeto;
            if (datosResumenAsis && Object.keys(datosResumenAsis).length !== 0) {
                this.editarResumenAsis(datosResumenAsis);
            } else {
                this.guardarResumenAsis();
            }

        });
    }


    async guardarData(fecha, estCodigo, matCodigo) {
        this.reflexion = 0;
        const fechaDiaString = fecha < 10 ? `0${fecha}` : fecha.toString();
        const fechaDia = parseInt(fechaDiaString, 10); // Convierte a número
        const cadenaGenerada = crearCadenaFechaHora(this.anioSeleccionado, this.mesSeleccionada, fechaDia);

        this.tutorOrdinariaService.buscarFaltasOrdinariaPorEstCodigo(estCodigo).then(data => {
            let listadoFaltas = data.listado;
            try {
                listadoFaltas.forEach(element => {
                    var timestamp = element.faloredFecha;
                    var fechaTime = new Date(timestamp);
                    var fechaFormateada = fechaTime.toISOString();
                    if (fechaFormateada === cadenaGenerada) {
                        this.reflexion = 1;
                        let dataGuardado = {
                            falordCodigo: element.falordCodigo,
                            curparCodigo: this.cursoSeleccionadoChild.curparCodigo,
                            curCodigo: this.codigoCuros,
                            docCodigo: this.cursoSeleccionadoChild.docCodigo,
                            estCodigo: estCodigo,
                            falordEstado: 1,
                            falordObservacion: null,
                            falordTipoFalta: 'I',
                            faloredFecha: cadenaGenerada,
                            matCodigo: matCodigo,
                            meCodigo: this.mesSeleccionada,
                            reanleCodigo: this.cursoSeleccionadoChild.reanleCodigo,
                            tuorCodigo: this.cursoSeleccionadoChild.tuorCodigo
                        };

                        this.tutorOrdinariaService.EditarFalta(dataGuardado).subscribe({
                            next: (respuesta) => {
                            },
                            error: (error) => {
                            }
                        });
                    }
                });

            } finally {

                if (this.reflexion !== 1) {
                    let dataGuardado = {
                        curparCodigo: this.cursoSeleccionadoChild.curparCodigo,
                        curCodigo: this.codigoCuros,
                        docCodigo: this.cursoSeleccionadoChild.docCodigo,
                        estCodigo: estCodigo,
                        falordEstado: 1,
                        falordObservacion: null,
                        falordTipoFalta: 'I',
                        faloredFecha: cadenaGenerada,
                        matCodigo: matCodigo,
                        meCodigo: this.mesSeleccionada,
                        reanleCodigo: this.cursoSeleccionadoChild.reanleCodigo,
                        tuorCodigo: this.cursoSeleccionadoChild.tuorCodigo
                    };

                    this.tutorOrdinariaService.guardarFalta(dataGuardado).subscribe({
                        next: (respuesta) => {
                        },
                        error: (error) => {
                        }
                    });
                }
            }


        });


    }

}

function crearCadenaFechaHora(year: number, month: number, day: number): string {
    // Crear un nuevo objeto Date con los componentes dados
    const fechaHora = new Date(year, month - 1, day);

    // Obtener los componentes de la fecha y hora en formato ISO
    const isoString = fechaHora.toISOString();

    // Formatear la cadena según tus necesidades
    const cadenaFechaHora = isoString.slice(0, -1) + 'Z';

    return cadenaFechaHora;
}
