import {ChangeDetectorRef, Component, Input, OnInit, ViewChild} from '@angular/core';
import {MatPaginator, MatPaginatorIntl} from '@angular/material/paginator';
import {MatSort} from '@angular/material/sort';
import {MatTableDataSource} from '@angular/material/table';
import {IacaCalificacionDTO} from 'app/main/pages/calificacionOrdinaria/interfaces/IacaCalificacionDTO';
import {CalificacionOrdinariaService} from 'app/main/pages/calificacionOrdinaria/servicios/calificacionOrdinaria.service';
import {TutorOrdinariaService} from '../../../servicios/tutorOrdinaria.service';
import {NgxSpinnerService} from 'ngx-spinner';
import {orderBy} from 'lodash-es';
import {IinfoDia} from '../../../interfaces/IinfoDia';
import {CalendarioAsistencia} from '../../../interfaces/calendarioAsistencia';
import {constanteAsistencia} from '../../../constantes/ConstanteAsistencia';
import {getMonth} from 'date-fns';
import {forkJoin, Observable} from 'rxjs';
import {map} from 'rxjs/operators';
import {Object} from 'core-js';
import {AlumnosFaltaOrdinariaDTO} from '../../../interfaces/alumnosFaltaOrdinariaDTO';
import {faltasOrdinariasInterface} from '../../../interfaces/faltasOrdinaria';
import {construirCalendario, obtenerNumeroMes} from '../../../../../shared/funtions/calendario-academico.function';

@Component({
    selector: 'app-asistencia',
    templateUrl: './asistencia.component.html',
    styleUrls: ['./asistencia.component.scss']
})
export class AsistenciaComponent implements OnInit {
    public columnasEncabezado: string[] = ['nombreEstudiante'];
    //checked
    checkedItems: { [key: string]: boolean } = {};
    /*calendarioAsistencia: CalendarioAsistencia;*/
    mesSeleccionado: string;
    /*semanaSeleccionada: number;*/
    diasEnSemanaSeleccionados: IinfoDia[] = [];
    alumnosFaltaOrdinaria!: AlumnosFaltaOrdinariaDTO;
    listaFaltasAlumnos: faltasOrdinariasInterface[];
    faltaOrdinariaGuardar: faltasOrdinariasInterface;
    listaEstudiantesNotas: IacaCalificacionDTO[] = [];
    dataSource: MatTableDataSource<any>;
   /* anioSeleccionado: number;*/

    //variables paginación y orden
    @ViewChild(MatPaginator) paginator!: MatPaginator;
    @ViewChild(MatSort) sort!: MatSort;
    //Inputs
    @Input() cursoSeleccionadoChild;
    /*@Input() anioLectivoChild;*/

    constructor(private calificacionOrdinariaService: CalificacionOrdinariaService,
                private spinnerService: NgxSpinnerService,
                private matPaginatorIntl: MatPaginatorIntl,
                private cdr: ChangeDetectorRef,
                private tutorOrdinariaService: TutorOrdinariaService) {
    }

    async ngOnInit(): Promise<void> {
            this.listaFaltasAlumnos=[];
        try {

            await this.listarEstudiantesMatriculados(this.cursoSeleccionadoChild.reanleCodigo, this.cursoSeleccionadoChild.curCodigo, this.cursoSeleccionadoChild.curparCodigo, this.cursoSeleccionadoChild.ofeCurso.sereduCodigo);

            this.spinnerService.show();
        } finally {
            this.spinnerService.hide();
        }
    }
    async listarEstudiantesMatriculados(reanleCodigo, curCodigo, curparCodigo, sereduCodigo) {
        try {
            this.spinnerService.show();
            const data = await this.calificacionOrdinariaService.getMatriculaOrdinariaObjetoConsulta(reanleCodigo, curCodigo, curparCodigo, sereduCodigo);

            this.listaEstudiantesNotas = orderBy(data.listado, ['estudiante.estNombres'], ['asc']);
            this.dataSource = new MatTableDataSource(this.listaEstudiantesNotas);
            this.dataSource.paginator = this.paginator;
            this.dataSource.sort = this.sort;
            this.matPaginatorIntl.itemsPerPageLabel = 'Elementos por página:';
            this.matPaginatorIntl.nextPageLabel = 'Siguiente:';
            this.matPaginatorIntl.previousPageLabel = 'Anterior:';
            this.cdr.detectChanges(); // Forzar la actualización

        } finally {
            this.spinnerService.hide();
        }
    }

    trackByFn(index, item) {
        return item.id;
    }

    tieneFaltasFecha(matCodigo: number, fecha: Date ): boolean {

        const encontrado = this.listaFaltasAlumnos.find(item => {
            const fechaItem = new Date(item.faloredFecha);
            return item.matCodigo === matCodigo && fechaItem.getTime() === fecha.getTime() && item.falordEstado===1;
        });
        return !!encontrado; // Devuelve true si se encuentra el elemento y false si no
    }

    buscarFaltasFecha(matCodigo: number, fecha: Date): faltasOrdinariasInterface | null {
        const encontrado = this.listaFaltasAlumnos.find(item => {
            const fechaItem = new Date(item.faloredFecha);
            return item.matCodigo === matCodigo && fechaItem.getTime() === fecha.getTime();
        });

        return encontrado ? encontrado : null;
    }

    async handleCheckboxChange(event: any, matCodigo: number, fecha: Date, estCodigo: number) {

        await this.buscarFaltasPorMes();

        if (event.target.checked) {

            this.guardarFaltaChecked(fecha, estCodigo, matCodigo);

        } else {
             this.guardarFaltaUnchecked(matCodigo, fecha, estCodigo);
        }
        await this.buscarFaltasPorMes();

    }

    async guardarFaltaChecked(fecha, estCodigo, matCodigo) {
        let itemEncontrado: faltasOrdinariasInterface;
        itemEncontrado = this.buscarFaltasFecha(matCodigo, fecha);
        if (itemEncontrado) {
            //activa de nuevo el registro de la falta
            this.faltaOrdinariaGuardar = itemEncontrado;
            this.faltaOrdinariaGuardar.falordEstado = 0;
        }
        this.tutorOrdinariaService.guardarFalta(this.faltaOrdinariaGuardar).subscribe(
            result => {
                this.cdr.detectChanges();
            }, err => {
                console.error(err);
            }
        );
    }

    async guardarFaltaUnchecked(matCodigo: number, fecha: Date, estCodigo: number) {
        let itemEncontrado: faltasOrdinariasInterface;
        itemEncontrado = this.buscarFaltasFecha(matCodigo, fecha);

        if (itemEncontrado) {
            //activa de nuevo el registro de la falta
            this.faltaOrdinariaGuardar = itemEncontrado;
            this.faltaOrdinariaGuardar.falordEstado = 1;
        } else {//crea de nuevo el registro de la falta
            this.faltaOrdinariaGuardar = {
                curCodigo: this.cursoSeleccionadoChild.curCodigo,
                curparCodigo: this.cursoSeleccionadoChild.curparCodigo,
                docCodigo: this.cursoSeleccionadoChild.docCodigo,
                estCodigo: estCodigo,
                falordCodigo: null,
                falordObservacion: null,
                falordTipoFalta: 'I',
                faloredFecha: fecha,
                matCodigo: matCodigo,
                meCodigo: getMonth(fecha) + 1,
                reanleCodigo: this.cursoSeleccionadoChild.reanleCodigo,
                tuorCodigo: this.cursoSeleccionadoChild.tuorCodigo,
                falordEstado: 1
            };
        }

        this.tutorOrdinariaService.guardarFalta(this.faltaOrdinariaGuardar).subscribe(
            result => {
                this.cdr.detectChanges();
            }, err => {
                console.error(err);
            }
        );
    }

   /* crearCalendario(reanleCodigo: number): Observable<CalendarioAsistencia> {
        let fechaInicial$: Observable<Date> = this.tutorOrdinariaService
            .buscarPorNemonicoAndReanleCodigo(constanteAsistencia.itpaprNemonicoFechaInicio, reanleCodigo)
            .pipe(
                map(respuesta => respuesta.totalRegistros === 1 ? respuesta.objeto.itpaprFecha : null)
            );

        let fechaFinal$: Observable<Date> = this.tutorOrdinariaService
            .buscarPorNemonicoAndReanleCodigo(constanteAsistencia.itpaprNemonicoFechaFin, reanleCodigo)
            .pipe(
                map(respuesta => respuesta.totalRegistros === 1 ? respuesta.objeto.itpaprFecha : null)
            );

        return forkJoin([fechaInicial$, fechaFinal$]).pipe(
            map(([fechaInicial, fechaFinal]) => {
                if (fechaInicial && fechaFinal) {
                    return construirCalendario(fechaInicial.toString(), fechaFinal.toString());
                } else {
                    throw new Error('Las fechas inicial o final no están disponibles.');
                }
            })
        );
    }*/

   /* getAnios(anios: { [anio: number]: any }): number[] {
        return Object.keys(anios).map(Number);
    }*/

   /* getMesesPorAnio(anioSeleccionado: number): string[] {
        if (this.calendarioAsistencia) {
            const anio = this.calendarioAsistencia.anios[anioSeleccionado];
            if (anio) {
                return Object.keys(anio);
            }
        }
        return [];
    }*/

    /*getSemanasPorMes(anioSeleccionado: number, mesSeleccionado: string): string[] {
        if (this.calendarioAsistencia && this.calendarioAsistencia.anios && this.calendarioAsistencia.anios[anioSeleccionado] &&
            this.calendarioAsistencia.anios[anioSeleccionado][mesSeleccionado] &&
            this.calendarioAsistencia.anios[anioSeleccionado][mesSeleccionado].semanas) {
            const semanas = this.calendarioAsistencia.anios[anioSeleccionado][mesSeleccionado].semanas;
            return Object.keys(semanas);
        }
        return [];
    }*/

    /*async cargarInfoSemanaSeleccionada(semanaSeleccionada: number): Promise<IinfoDia[]> {
        this.diasEnSemanaSeleccionados = [];

        const anio = this.calendarioAsistencia.anios[this.anioSeleccionado]?.[this.mesSeleccionado]?.semanas;
        this.diasEnSemanaSeleccionados = anio ? anio[semanaSeleccionada] || [] : [];
        this.columnasEncabezado = ['nombreEstudiante'];
        for (const semana of this.diasEnSemanaSeleccionados) {
            this.columnasEncabezado.push(semana.nombreDia);
        }
        return anio ? anio[semanaSeleccionada] || [] : [];
    }
*/
   /* reiniciarSemana() {
        this.diasEnSemanaSeleccionados = [];
        this.semanaSeleccionada = null;
        this.setearAlumnosFaltaOrdinaria();
        try {
            this.buscarFaltasPorMes();

        } catch (error) {
            console.error('Error al obtener el listado de faltas:', error);
        }
    }
*/
    setearAlumnosFaltaOrdinaria() {
        this.alumnosFaltaOrdinaria = {
            curparCodigo: 0, estudiantes: [], meCodigo: 0, reanleCodigo: 0, tuorCodigo: 0
        };
        this.alumnosFaltaOrdinaria.curparCodigo = this.cursoSeleccionadoChild.curparCodigo;
        this.alumnosFaltaOrdinaria.reanleCodigo = this.cursoSeleccionadoChild.reanleCodigo;
        this.alumnosFaltaOrdinaria.tuorCodigo = this.cursoSeleccionadoChild.tuorCodigo;
        this.alumnosFaltaOrdinaria.meCodigo = obtenerNumeroMes(this.mesSeleccionado);
        this.alumnosFaltaOrdinaria.estudiantes = this.listaEstudiantesNotas.map(item => item.matCodigo);

    }

    async buscarFaltasPorMes() {
        try {
            const respuesta = await this.tutorOrdinariaService.listarFaltasOrdinariaPorTutorPorCursoYMes(this.alumnosFaltaOrdinaria).toPromise();
            if (respuesta) {
                this.listaFaltasAlumnos = respuesta.listado;
            }
        } catch (error) {
            console.error('Error al obtener faltas de alumnos:', error);
        }
    }

    async onDiasSeleccionadosChanged(diasSeleccionados: IinfoDia[]) {
        this.diasEnSemanaSeleccionados = diasSeleccionados;

        if(this.diasEnSemanaSeleccionados.length > 0) {
            await this.cargarEncabezadoFechaSeleccionada()
            if (this.dataSource) {
                this.dataSource.data = this.listaEstudiantesNotas.slice();
            }
            this.cdr.detectChanges();
        }
    }
    cargarEncabezadoFechaSeleccionada() {
       this.columnasEncabezado = ['nombreEstudiante'];
        for (const semana of this.diasEnSemanaSeleccionados) {
            this.columnasEncabezado.push(semana.nombreDia);
        }

    }
    /*reestablecerListaFaltas() {
        this.diasEnSemanaSeleccionados = [];
    }*/


    async onMesSeleccionadosChanged(mesSeleccionado: string) {
        this.mesSeleccionado=mesSeleccionado
        await this.setearAlumnosFaltaOrdinaria();
        await this.buscarFaltasPorMes();
    }
}

/*function capitalize(word: string): string {
    return word.charAt(0).toUpperCase() + word.slice(1);
}*/

/*
function construirCalendario(fechaInicial: string, fechaFinal: string): CalendarioAsistencia {
    const anios: CalendarioAsistencia['anios'] = {};

    const fInicial = parseISO(fechaInicial);
    const fFinal = parseISO(fechaFinal);

    const diasEntreFechas = eachDayOfInterval({start: fInicial, end: fFinal});

    let semanaActual = 1;
    let primerDiaSemana = startOfMonth(fInicial);

    diasEntreFechas.forEach(dia => {
        const diaSemana = getDay(dia);
        const mesActual = getMonth(dia);

        // Omitimos fines de semana
        if (diaSemana === 0 || diaSemana === 6) {
            return;
        }

        if (diaSemana === 1 || mesActual !== getMonth(primerDiaSemana)) {
            semanaActual = getWeekOfMonth(dia);
            primerDiaSemana = dia;
        }

        const anio = getYear(dia);
        const mesNombre = capitalize(format(dia, 'MMMM', {locale: es}));
        const numeroMes = getMonth(dia) + 1;
        const diaNombre = format(dia, 'EEEE', {locale: es});

        anios[anio] ??= {};
        anios[anio][mesNombre] ??= {numeroMes, semanas: {}};
        anios[anio][mesNombre].semanas[semanaActual] ??= [];

        anios[anio][mesNombre].semanas[semanaActual].push({
            diaSemana: parseInt(format(dia, 'd')),
            nombreDia: diaNombre,
            fecha: dia
        });
    });

    return {
        fechaInicial: fInicial,
        fechaFinal: fFinal,
        anios: anios
    };
}
*/

/*function obtenerNumeroMes(nombreMes: string): number {
    const fecha = parse(`01 ${nombreMes} 2000`, 'dd MMMM yyyy', new Date(), {locale: es});
    const numeroMes = getMonth(fecha);
    return numeroMes + 1;
}*/
