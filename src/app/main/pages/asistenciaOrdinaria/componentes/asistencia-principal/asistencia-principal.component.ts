import { Component, OnInit, ViewChild } from '@angular/core';
import { MatPaginator, MatPaginatorIntl } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { Router } from '@angular/router';
import { CatalogoService } from 'app/main/pages/asignacionDocentes/servicios/catalogo.service';
import { GieeService } from 'app/main/pages/asignacionDocentes/servicios/giee.service';
import { IcatDocenteHispana } from 'app/main/pages/asignacionDocentesOrdinaria/interfaces/IcatDocenteHispana';
import { IinsInstitucion } from 'app/main/pages/asignacionDocentesOrdinaria/interfaces/IinsInstitucion';
import { DocenteOrdinariaService } from 'app/main/pages/asignacionDocentesOrdinaria/servicios/docenteOrdinaria.service';
import { MensajesService } from 'app/main/pages/asignacionDocentesOrdinaria/servicios/mensajes.service';
import { CalificacionOrdinariaService } from 'app/main/pages/calificacionOrdinaria/servicios/calificacionOrdinaria.service';
import { NgxSpinnerService } from 'ngx-spinner';
import { TutorOrdinariaService } from '../../servicios/tutorOrdinaria.service';
import { IcatAnioLectivo } from '../../interfaces/IcatAnioLectivo';
import { CatAnioLectivo } from 'app/main/pages/asignacionDocentes/interface/cat_anio_lectivo';
import { Catregimen } from 'app/main/pages/asignacionDocentes/interface/cat_regimen';


@Component({
  selector: 'app-asistencia-principal',
  templateUrl: './asistencia-principal.component.html',
  styleUrls: ['./asistencia-principal.component.scss']
})
export class AsistenciaPrincipalComponent implements OnInit {
  //variables tabla
  columnasEncabezado: string[] = ['nombre_grado', 'opciones'];
  dataSource: MatTableDataSource<any>;
  //variables paginación y orden
  @ViewChild(MatPaginator) paginator!: MatPaginator;
  @ViewChild(MatSort) sort!: MatSort;
  //Variables
  public institucionObjeto: IinsInstitucion;
  public docenteObjeto: IcatDocenteHispana = { numIdentificacion: '' };
  public listaTutorOrdinario: any[] = [];
  public habilitarJustificacion = false;
  public habilitarAsistencia = false;
  public cursoSeleccionado;
  public codAmie;
  public informacionSede: any;
  public informacionInstitucion;
  public codInstitucion;
  public codgiRegimen;
  public regimen:Catregimen;
  public listaAnio;
  public reanleCodigo:number;
  public anioElectivoCodigo:number;
  public anioLectivo: CatAnioLectivo;
  public informacionEstablecimiento: any;
  public establecimiento;
  public listaInstitucion: any;
  public nombreInstitucion;
  constructor(private calificacionOrdinariaService: CalificacionOrdinariaService,
    private tutorOrdinariaService: TutorOrdinariaService,
    private mensajeService: MensajesService,
    private spinnerService: NgxSpinnerService,
    private asignacionOrdinariaService: DocenteOrdinariaService,
    private GieeService: GieeService,
    private CatalogoService: CatalogoService,
    private matPaginatorIntl: MatPaginatorIntl,
    private readonly router: Router,) { }

  async ngOnInit(): Promise<void> {
    try {
      this.spinnerService.show();
      //asignacion codigo amie
      this.listaInstitucion = JSON.parse(localStorage.getItem('currentUser'));
      this.informacionSede = this.listaInstitucion.sede;
      this.nombreInstitucion = this.informacionSede.descripcion;
      this.codAmie = this.informacionSede.nemonico;
      await this.consultarInstitucionPorAmie(this.codAmie);
      await this.buscarRegiment(this.codAmie);
      //buscar Establecimiento de la institucion
      await this.obtenerEstablecimiento(this.codAmie);
      await this.consultarDocentePorcedula(this.listaInstitucion.identificacion);

      // await this.obtenerListadoDistributivo(this.docenteObjeto.codPersona, this.reanleCodigo, this.establecimiento);
      await this.obtenerListadoDistributivo(this.docenteObjeto.codPersona, this.reanleCodigo, this.establecimiento);



    } finally {
      this.spinnerService.hide();
    }

  }

  consultarInstitucionPorAmie(codAmie) {
    this.calificacionOrdinariaService.getIntitucionPorAmie(codAmie).then(data => {
      this.institucionObjeto = data.objeto;
    }, error => {
      this.mensajeService.mensajeError("", "Error al consultar servicios instituciones app" + error);
      this.spinnerService.hide();
    })
  }
  //buscar los datos de la institucion y regimen
  async buscarRegiment(cod) {
    //buscar la institucion
    await this.GieeService.getDatosInstitucion(cod).then(async data => {
      this.informacionInstitucion = data.objeto;
      for (let dato in this.informacionInstitucion) {
        this.codInstitucion = this.informacionInstitucion.insCodigo;
        this.codgiRegimen = this.informacionInstitucion.regimenes[0].regCodigo;
      }

      await this.CatalogoService.getRegimen(this.codgiRegimen).then(data => {
        this.regimen = data.objeto;
      }, error => {
        this.mensajeService.mensajeError("", "Error al consultar catálogo");
        this.spinnerService.hide();
      })
      //listar regimenAñolectivo 
      await this.CatalogoService.getAnioElectivo(this.codgiRegimen).then(data => {
        this.listaAnio = data.listado;
        for (let dato of this.listaAnio) {
          if (dato.reanleTipo === "A") {
            this.reanleCodigo = dato.reanleCodigo;
            this.anioElectivoCodigo = dato.anilecCodigo;
            this.CatalogoService.getAnioLectivoPorAnilecCodigo(this.anioElectivoCodigo).then(data => {
              this.anioLectivo = data.objeto
            })
          }
        }
      })
    }, error => {
      this.mensajeService.mensajeError("", "Error al consultar servicios Giee");
      this.spinnerService.hide();
    })
  }

  //sacar los establecimientos de la institucion
  async obtenerEstablecimiento(cod) {
    await this.GieeService.getEstablecimientoPorAmie(cod).then(data => {
      this.informacionEstablecimiento = data.listado;
      //Buscar el establecimientto matriz
      let matriz = this.informacionEstablecimiento.find(obj => obj.insTipoEstablecimiento.tipEstCodigo === 1)
      this.establecimiento = matriz.insestCodigo

    }, error => {
      this.mensajeService.mensajeError("", "Error al consultar servicios Giee");
      this.spinnerService.hide();
    })
  }

  async consultarDocentePorcedula(identificacion) {
    await this.asignacionOrdinariaService.getDocentePorIdentificacion(identificacion).then(data => {
      this.docenteObjeto = data.objeto;
    }, error => {
      console.log("error");
      this.spinnerService.hide();
    })
  }
  async obtenerListadoDistributivo(docCodigo, reanleCodigo, insestCodigo) {
    await this.tutorOrdinariaService.getTutorOrdinariaPorDocCOdigo(docCodigo).then(async data => {
      this.listaTutorOrdinario = data.listado;
      for (const objeto of this.listaTutorOrdinario) {
        await this.calificacionOrdinariaService.getCursoParaleloPorCurparCodigo(objeto.curparCodigo).then(async data => {
          objeto.ofeCursoParalelo = data.objeto;
          await this.calificacionOrdinariaService.getParaleloPorParCodigo(objeto.ofeCursoParalelo.parCodigo).then(data => {
            objeto.ofeCursoParalelo.nombreParalelo = data.objeto.parDescripcion;
          }, error => {
            this.mensajeService.mensajeError("", "Error al consultar servicios oferta");
            this.spinnerService.hide();
          })

        }, error => {
          this.mensajeService.mensajeError("", "Error al consultar servicios oferta");
          this.spinnerService.hide();
        })
        await this.calificacionOrdinariaService.getCursoPorCurCodigo(objeto.curCodigo).then(async data => {
          objeto.ofeCurso = data.objeto;
          //consulta el grado 
          await this.calificacionOrdinariaService.getGradoPorGraCodigo(objeto.ofeCurso.graCodigo).then(async data => {
            //consulta el nivel 
            objeto.ofeCurso.nombreGrado = data.objeto.graDescripcion;
            objeto.ofeCurso.nivCodigo = data.objeto.nivCodigo;
            // await this.asignacionOrdinariaService.getNivelPorNivCodigo(data.objeto.nivCodigo).then(data => {
            // })
          }, error => {
            this.mensajeService.mensajeError("", "Error al consultar servicios catálogo " + error);
            this.spinnerService.hide();
          })
          await this.asignacionOrdinariaService.getJornadaPorJorCodigo(objeto.ofeCurso.jorCodigo).then(async data => {
            //consulta jornada
            objeto.ofeCurso.nombreJornada = data.objeto.jorNombre;
            // await this.asignacionOrdinariaService.getNivelPorNivCodigo(data.objeto.nivCodigo).then(data => {
            // })
          }, error => {
            this.mensajeService.mensajeError("", "Error al consultar servicios catálogo " + error);
            this.spinnerService.hide();
          })
        }, error => {
          this.mensajeService.mensajeError("", "Error al consultar oferta " + error);
          this.spinnerService.hide();
        })
      }
    }, error => {
      this.mensajeService.mensajeError("", "Error al consultar servicios academico" + error);
      this.spinnerService.hide();
    })
    this.listaTutorOrdinario.sort((a, b) => a.ofeCurso.graCodigo - b.ofeCurso.graCodigo);
    this.dataSource = new MatTableDataSource(this.listaTutorOrdinario);
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
    this.dataSource.paginator = this.paginator;
    this.matPaginatorIntl.itemsPerPageLabel = 'Elementos por página:';
    this.matPaginatorIntl.nextPageLabel = 'Siguiente:';
    this.matPaginatorIntl.previousPageLabel = 'Anterior:';

  }

  abrirJustificacionPrincipal(elemento) {
    this.cursoSeleccionado = elemento;
    this.habilitarJustificacion = true;
    this.habilitarAsistencia = false;
  }
  abrirAsistencia(elemento) {
    this.cursoSeleccionado = elemento;
    this.habilitarAsistencia = true;
    this.habilitarJustificacion = false;
  }

}



