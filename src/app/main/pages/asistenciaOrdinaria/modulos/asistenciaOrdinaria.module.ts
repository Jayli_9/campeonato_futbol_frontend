import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';

import { ProgramStoreService } from "../servicios/programStore.service";

//importacion material
import { MaterialModule } from "app/main/pages/material/material.module";
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatAutocompleteModule } from '@angular/material/autocomplete';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { MatIconModule } from '@angular/material/icon';
import { MatDividerModule } from '@angular/material/divider';
import { MatSelectModule } from '@angular/material/select';
import { MatFormFieldModule } from '@angular/material/form-field';
import { RUTA_ASISTENCIA_ORDINARIO } from '../rutas/asistenciaOrdinaria.routing';
import { MatTabsModule } from '@angular/material/tabs';
import { MatPaginatorModule } from '@angular/material/paginator';
import { AsistenciaPrincipalComponent } from '../componentes/asistencia-principal/asistencia-principal.component';
import { AsistenciaComponent } from '../componentes/asistencia-principal/asistencia/asistencia.component';
import { JustificacionComponent } from '../componentes/asistencia-principal/justificacion/justificacion.component';
import { FullCalendarModule } from '@fullcalendar/angular';
import { MatDatepickerModule } from '@angular/material/datepicker';
import {CalendarioComponent} from '../componentes/asistencia-principal/calendario/calendario.component';
import {NgxSpinnerModule} from 'ngx-spinner';


@NgModule({
    declarations: [
        AsistenciaPrincipalComponent,
        JustificacionComponent,
        AsistenciaComponent,
        CalendarioComponent,
    ],
    imports: [
        CommonModule,
        RouterModule.forChild(RUTA_ASISTENCIA_ORDINARIO),
        MaterialModule,
        FormsModule,
        ReactiveFormsModule,
        MatAutocompleteModule,
        MatCheckboxModule,
        MatIconModule,
        MatDividerModule,
        MatSelectModule,
        MatFormFieldModule,
        MatTabsModule,
        MatPaginatorModule,
        FullCalendarModule,
        MatDatepickerModule,
        MatFormFieldModule,
        NgxSpinnerModule,
        // ContentHeaderModule
    ],
  schemas: [
    CUSTOM_ELEMENTS_SCHEMA
  ],
  providers: [ProgramStoreService]

})
export class asistenciaOrdinariaModule { }
