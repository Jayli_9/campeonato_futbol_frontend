import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'environments/environment';
import { AuthenticationService } from '../../../../auth/service/authentication.service';

//exportacion interfaces
import { ResponseGenerico } from '../../asignacionDocentes/interface/response-generico';
import { IacaTutorOrdinaria } from '../interfaces/IacaTutorOrdinaria';
import { faltasOrdinariasInterface } from "../interfaces/faltasOrdinaria";
import { matriculaEstudianteInterface } from "../interfaces/matriculaEstudiante";
import { estudianteInterface } from "../interfaces/estudiante";
import { resumenAsisInterface } from "../interfaces/resumenAsis";
import {RespuestaModuloInterfaz} from '../../asistencia-estudiantes/interfaces/respuesta-modulo-interfaz';
import {MatItemParametroPresencial} from '../interfaces/matItemParametroPresencial';
import {AlumnosFaltaOrdinariaDTO} from '../interfaces/alumnosFaltaOrdinariaDTO';

@Injectable({
  providedIn: 'root'
})
export class TutorOrdinariaService {

  constructor(private _http: HttpClient, private _authService: AuthenticationService) { }


  //obtener lista de distributivo;
  getTutorOrdinariaPorDocCOdigo(docCodigo:number): Promise<ResponseGenerico<IacaTutorOrdinaria>> {
    return new Promise((resolve, reject) => {
      this._http.get(`${environment.url_academico}/private/buscarTutorOrdinariaPorDocente/${docCodigo}`).subscribe((response: ResponseGenerico<IacaTutorOrdinaria>) => {
        resolve(response);
      }, reject);
    })
  }

  //listarfaltas
  listarFaltas(cod): Promise<ResponseGenerico<faltasOrdinariasInterface>> {
    return new Promise((resolve, reject) => {
      this._http.get(`${environment.url_academico}/private/buscarFaltasOrdinariaPorTutor/`+cod).subscribe((response: ResponseGenerico<faltasOrdinariasInterface>) => {
        resolve(response);
      }, reject);
    })
  }

  buscarFaltasOrdinariaPorMes(cod): Promise<ResponseGenerico<faltasOrdinariasInterface>> {
    return new Promise((resolve, reject) => {
      this._http.get(`${environment.url_academico}/private/buscarFaltasOrdinariaPorMes/`+cod).subscribe((response: ResponseGenerico<faltasOrdinariasInterface>) => {
        resolve(response);
      }, reject);
    })
  }


  buscarFaltasOrdinariaPorEstCodigo(cod): Promise<ResponseGenerico<faltasOrdinariasInterface>> {
    return new Promise((resolve, reject) => {
      this._http.get(`${environment.url_academico}/private/buscarFaltasPorCodigoEstudiante/`+cod).subscribe((response: ResponseGenerico<faltasOrdinariasInterface>) => {
        resolve(response);
      }, reject);
    })
  }


  buscarEstudiantePorCodigoMatricula(cod): Promise<ResponseGenerico<matriculaEstudianteInterface>> {
    return new Promise((resolve, reject) => {
      this._http.get(`${environment.url_matricula}/private/buscarPorEstCodigo/`+cod).subscribe((response: ResponseGenerico<matriculaEstudianteInterface>) => {
        resolve(response);
      }, reject);
    })
  }


  buscarEstudiantePorCodigoEstCodigo(cod): Promise<ResponseGenerico<estudianteInterface>> {
    return new Promise((resolve, reject) => {
      this._http.get(`${environment.url_matricula}/private/listarMatriculaOrdinariaPorEstCodigo/`+cod).subscribe((response: ResponseGenerico<estudianteInterface>) => {
        resolve(response);
      }, reject);
    })
  }


  buscarResumenAsistenciaPorCodigoMat(cod): Promise<ResponseGenerico<resumenAsisInterface>> {
    return new Promise((resolve, reject) => {
      this._http.get(`${environment.url_academico}/private/buscarAsistenciaResumenPorMatCodigo/`+cod).subscribe((response: ResponseGenerico<resumenAsisInterface>) => {
        resolve(response);
      }, reject);
    })
  }

  guardarResumenAsis(parametro: any){
    return this._http.post<estudianteInterface>(`${environment.url_academico}/private/guardarAsistenciaResumen`, parametro);
  }

  EditarResumenAsis(parametro: any){
    return this._http.post<estudianteInterface>(`${environment.url_academico}/private/editarAsistenciaResumen`, parametro);
  }


  guardarFalta(parametro: any){
    return this._http.post<faltasOrdinariasInterface>(`${environment.url_academico}/private/guardarFaltasOrdinaria`, parametro);
  }

  EditarFalta(parametro: any){
    return this._http.post<faltasOrdinariasInterface>(`${environment.url_academico}/private/actualizarFaltasOrdinarias`, parametro);
  }

  buscarPorNemonicoAndReanleCodigo(nemonico:string,reanleCodigo:number) {
    let url_ws=`${environment.url_matricula}/private/buscarPorNemonicoAndReanleCodigo/${nemonico}/${reanleCodigo}`;
    return this._http.get<ResponseGenerico<MatItemParametroPresencial>>(url_ws);
  }

  listarFaltasOrdinariaPorTutorPorCursoYMes(parametro: AlumnosFaltaOrdinariaDTO){
    return this._http.post<ResponseGenerico< faltasOrdinariasInterface>>(`${environment.url_academico}/private/listarFaltasOrdinariaPorTutorPorCursoYMes`, parametro);
  }
}
