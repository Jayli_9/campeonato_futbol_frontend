import { Injectable } from '@angular/core';
import {faltasOrdinariasInterface} from '../interfaces/faltasOrdinaria';

@Injectable()
export class ProgramStoreService {

  private estCodigo: number;
  private matCodigo: number;
  private fecha;
  private datosExternos;
  private codigoCurso;
  private codigoMes;
  private anioSeleccion;
  private _faltaEncontrada: faltasOrdinariasInterface;

  get getAnioSeleccion(){
    return this.anioSeleccion;
  }

  set setAnioSeleccion(anioSeleccion){
    this.anioSeleccion = anioSeleccion;
  }



  get getCodigoCurso(){
    return this.codigoCurso;
  }

  set setCodigoCurso(codigoCurso){
    this.codigoCurso = codigoCurso;
  }

  get getCodigoMes(){
    return this.codigoMes;
  }

  set setCodigoMes(codigoMes){
    this.codigoMes = codigoMes;
  }

  get getDatosExternos(){
    return this.datosExternos;
  }

  set setDatosExternos(datosExternos){
    this.datosExternos = datosExternos;
  }

  get getEstCodigo(){
    return this.estCodigo;
  }

  set setEstCodigo(estCodigo: number){
    this.estCodigo = estCodigo;
  }

  get getMatCodigo(){
    return this.matCodigo;
  }

  set setMatCodigo(matCodigo: number){
    this.matCodigo = matCodigo;
  }

  get getFecha(){
    return this.fecha;
  }

  set setFecha(fecha){
    this.fecha = fecha;
  }

  get getFaltaEncontrada(): faltasOrdinariasInterface {
    return this._faltaEncontrada;
  }

  set setFaltaEncontrada(value: faltasOrdinariasInterface) {
    this._faltaEncontrada = value;
  }


}
