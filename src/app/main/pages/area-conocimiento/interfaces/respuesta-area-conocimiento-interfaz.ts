import { RespuestaGeneralInterfaz } from "app/main/shared/interfaces/respuesta-general-interfaz";
import { AreaConocimiento } from "./area-conocimiento";

export interface RespuestaAreaConocimientoInterfaz extends RespuestaGeneralInterfaz{
    listado: AreaConocimiento[];
    objeto: AreaConocimiento;
}
