import { Routes } from "@angular/router";
import { AuthGuard } from "app/auth/helpers/auth.guards";
import { AreaConocimientoComponent } from "../components/area-conocimiento/area-conocimiento.component";

export const RUTA_AREA_CONOCIMIENTO:Routes=[
    {
        path:'area-conocimiento',
        component:AreaConocimientoComponent,
        canActivate:[AuthGuard]
    }
]