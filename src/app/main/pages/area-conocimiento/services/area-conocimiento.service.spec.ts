import { TestBed } from '@angular/core/testing';

import { AreaConocimientoService } from './area-conocimiento.service';

describe('AreaConocimientoService', () => {
  let service: AreaConocimientoService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(AreaConocimientoService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
