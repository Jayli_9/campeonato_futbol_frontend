import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'environments/environment';
import { AreaConocimiento } from '../interfaces/area-conocimiento';
import { RespuestaAreaConocimientoInterfaz } from '../interfaces/respuesta-area-conocimiento-interfaz';

@Injectable({
  providedIn: 'root'
})
export class AreaConocimientoService {

  url_academico=environment.url_academico;
  constructor(
    public _http:HttpClient
  ) { }
  guardarActualizarAreaDeConocimiento(areaConocimiento:AreaConocimiento){
    let url_ws=`${this.url_academico}/private/guardarAreaConocimiento`;
    return this._http.post(url_ws,areaConocimiento);
  }

  listarTodasLasAreasDeConocimiento(){
    let url_ws=`${this.url_academico}/private/listarTodasLasAreasDeConocimiento`;
    return this._http.get<RespuestaAreaConocimientoInterfaz>(url_ws);
  }

  listarAreasDeConocimientoActivas(){
    let url_ws=`${this.url_academico}/private/listarAreasDeConocimientoActivas`;
    return this._http.get<RespuestaAreaConocimientoInterfaz>(url_ws);
  }

  buscarAreaDeConocimientoPorCodigo(codigo:number){
    let url_ws=`${this.url_academico}/private/buscarAreaConocimientoPorCodigo/${codigo}`;
    return this._http.get<RespuestaAreaConocimientoInterfaz>(url_ws);
  }
  
  inactivarAreas(codigo:number){
    let url_ws=`${this.url_academico}/private/eliminarAreaConocimientoPorId/${codigo}`;
    return this._http.delete(url_ws);
  }

}
