import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RUTA_AREA_CONOCIMIENTO } from '../routes/area-conocimiento-routing.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { CoreCommonModule } from '@core/common.module';
import { ContentHeaderModule } from 'app/layout/components/content-header/content-header.module';
import { AreaConocimientoComponent } from '../components/area-conocimiento/area-conocimiento.component';
import { MaterialModule } from 'app/main/shared/material/material.module';



@NgModule({
  declarations: [AreaConocimientoComponent],
  imports: [
    CommonModule,
    RouterModule.forChild(RUTA_AREA_CONOCIMIENTO),
    FormsModule,
    ReactiveFormsModule,
    MaterialModule,
    ContentHeaderModule,
    CoreCommonModule
  ]
})
export class AreaConocimientoModule { }
