import { Component, OnInit, ViewChild } from '@angular/core';
import { UntypedFormBuilder, UntypedFormGroup, Validators } from '@angular/forms';
import {MatPaginator} from '@angular/material/paginator';
import {MatSort} from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { AreaConocimiento } from '../../interfaces/area-conocimiento';
import { RespuestaAreaConocimientoInterfaz } from '../../interfaces/respuesta-area-conocimiento-interfaz';
import { AreaConocimientoService } from '../../services/area-conocimiento.service';
import Swal from 'sweetalert2';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { NgxSpinnerService } from 'ngx-spinner';

@Component({
  selector: 'app-area-conocimiento',
  templateUrl: './area-conocimiento.component.html',
  styleUrls: ['./area-conocimiento.component.scss']
})
export class AreaConocimientoComponent implements OnInit {
  public contentHeader: object;
  public frmAreaCreateEdit: UntypedFormGroup;
  public submitted = false;
  listaAreas:AreaConocimiento[]=null;
  columnasAreas=['num','descripcion','nemonico','estado','acciones'];
  dataSource: MatTableDataSource<AreaConocimiento>;

  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;
  constructor(
    private readonly fb: UntypedFormBuilder,
    private modalService: NgbModal,
    public spinner: NgxSpinnerService,
    private _areaService:AreaConocimientoService
  ) { }
  
  ngOnInit(): void {
    this.contentHeader = {
      headerTitle: 'Area de Conocimiento',
      actionButton: false,
      breadcrumb: {
        type: '',
        links: [
          {
            name: 'Inicio',
            isLink: true,
            link: '/pages/inicio'
          },
          {
            name: 'Area de Conocimiento',
            isLink: false
          },
        ]
      }
    };
    this.listarTodasLasAreas();
  }
  listarTodasLasAreas(){
    this.spinner.show();
    this._areaService.listarTodasLasAreasDeConocimiento().subscribe(
      (respuesta:RespuestaAreaConocimientoInterfaz)=>{
        this.listaAreas=respuesta.listado;
        this.dataSource = new MatTableDataSource(this.listaAreas);
        this.dataSource.paginator = this.paginator;
        this.dataSource.paginator._intl.itemsPerPageLabel="Areas por página";
        this.dataSource.paginator._intl.nextPageLabel="Siguiente";
        this.dataSource.paginator._intl.previousPageLabel="Anterior";
        this.dataSource.sort = this.sort;
        this.spinner.hide();
      },
      (error:any)=>{
        this.spinner.hide();
       Swal.fire('Ups! ocurrió un error al cargar Areas','','error'); 
      }
    );
  }
  nuevaArea(modalForm){
    this.submitted=false;
    this.frmAreaCreateEdit=this.fb.group({
      descripcion: ['', [Validators.required]],
      nemonico: ['', [Validators.required,Validators.maxLength(10)]]
    });
    this.modalService.open(modalForm);
  }
  editArea(modalForm,areaEdit:AreaConocimiento){
    this.submitted=false;
    this.frmAreaCreateEdit=this.fb.group({
      codigo:[areaEdit.arcoCodigo],
      descripcion: [areaEdit.arcoDescripcion, [Validators.required]],
      nemonico: [areaEdit.arcoNemonico, [Validators.required,Validators.maxLength(10)]],
      estado:[areaEdit.arcoEstado]
    });
    this.modalService.open(modalForm);
  }
  guardarArea(modalForm){
    this.spinner.show();
    this.submitted = true;
    if (this.frmAreaCreateEdit.invalid) {
      this.spinner.hide();
      return;
    }
    let areaNueva:AreaConocimiento={
      arcoDescripcion:this.frmAreaCreateEdit.get("descripcion").value,
      arcoNemonico:this.frmAreaCreateEdit.get("nemonico").value,
      arcoEstado:1
    };
    this._areaService.guardarActualizarAreaDeConocimiento(areaNueva).subscribe(
      (respuesta:any)=>{
          Swal.fire(`Area Conocimiento Creada!`,'','success');
          this.spinner.hide();
          this.listarTodasLasAreas(); 
          
      },
      (error:any)=>{
        Swal.fire(`No se pudo crear el Area de Conocimiento`,'','error'); 
        this.spinner.hide();
        this.listarTodasLasAreas();
      }
    );
    modalForm.close('Accept click');  
  }
  activarInactivar(areaActualizar:AreaConocimiento){
    this.spinner.show();
    if (areaActualizar.arcoEstado==1) {
      this._areaService.inactivarAreas(areaActualizar.arcoCodigo).subscribe(
        (respuesta:any)=>{
          this.spinner.hide();
          Swal.fire(' Area Inactivada','','success'); 
          this.listarTodasLasAreas();
        },
        (error:any)=>{
          this.spinner.hide();
          Swal.fire('Ups! ocurrió un error','','error'); 
          this.listarTodasLasAreas();
        }
      );
    } else {
      areaActualizar.arcoEstado=1;
      this._areaService.guardarActualizarAreaDeConocimiento(areaActualizar).subscribe(
        (respuesta:any)=>{
          this.spinner.hide();
          Swal.fire(' Area Activada','','success'); 
          this.listarTodasLasAreas();
        },
        (error:any)=>{
          this.spinner.hide();
          Swal.fire('Ups! ocurrió un error','','error'); 
          this.listarTodasLasAreas();
        }
      );
    }
    
}
actualizarArea(modalForm){
  this.spinner.show();
    this.submitted = true;
    // stop here if form is invalid
    if (this.frmAreaCreateEdit.invalid) {
      this.spinner.hide();
      return;
    }
    let areaGuardar:AreaConocimiento={
      arcoCodigo:this.frmAreaCreateEdit.get("codigo").value,
      arcoDescripcion:this.frmAreaCreateEdit.get("descripcion").value,
      arcoNemonico:this.frmAreaCreateEdit.get("nemonico").value,
      arcoEstado:this.frmAreaCreateEdit.get("estado").value,
    };
    this._areaService.guardarActualizarAreaDeConocimiento(areaGuardar).subscribe(
      (respuesta:any)=>{
          Swal.fire(`Area Conocimiento Actualizada!`,'','success'); 
          this.spinner.hide();
          this.listarTodasLasAreas();
      },
      (error:any)=>{
        Swal.fire(`No se pudo guardar el Area de Conocimiento`,'','error'); 
        this.spinner.hide();
        this.listarTodasLasAreas();
      }
    );
    modalForm.close('Accept click');
}
applyFilter(event: Event) {
  const filterValue = (event.target as HTMLInputElement).value;
  this.dataSource.filter = filterValue.trim().toLowerCase();

  if (this.dataSource.paginator) {
    this.dataSource.paginator.firstPage();
  }
}
 // getter for easy access to form fields
 get ReactiveFrmAreaCreateEdit() {
  return this.frmAreaCreateEdit.controls;
 }

}
