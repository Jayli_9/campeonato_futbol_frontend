import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AreaConocimientoComponent } from './area-conocimiento.component';

describe('AreaConocimientoComponent', () => {
  let component: AreaConocimientoComponent;
  let fixture: ComponentFixture<AreaConocimientoComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AreaConocimientoComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AreaConocimientoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
