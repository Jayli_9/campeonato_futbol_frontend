import { Routes } from '@angular/router';
import { PrincipalComponent } from '../componentes/principal/principal.component';

export const RUTA_ASIGNACION_PREPA: Routes = [
    {
      path: 'asignacionPrepa',
      component: PrincipalComponent,
    }
  ];