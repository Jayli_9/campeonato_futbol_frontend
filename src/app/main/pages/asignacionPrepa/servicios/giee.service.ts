import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { environment } from 'environments/environment';
import { ResponseGenerico } from '../interface/response-generico';
import { institucionInterface } from "../interface/institucion"

@Injectable({
  providedIn: 'root'
})
export class GieeService {

constructor(private _http: HttpClient) { }

buscarinstitucionPorAmie(cod): Promise<ResponseGenerico<institucionInterface>>{
  return new Promise((resolve, reject) => {
    this._http.get(`${environment.url_institucion}/private/buscarInstitucionPorAmie/`+cod).subscribe((response: ResponseGenerico<institucionInterface>) => {
      resolve(response);
    }, reject);
  })
}

}
