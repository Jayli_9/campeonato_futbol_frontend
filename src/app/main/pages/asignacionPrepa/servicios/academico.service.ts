import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { environment } from 'environments/environment';
import { ResponseGenerico } from '../interface/response-generico';
import { ambitoInterface } from "../interface/ambito";
import { AsignaturasInterface } from "../interface/asignacion";


@Injectable({
  providedIn: 'root'
})
export class AcademicoService {

constructor(private _http: HttpClient) { }


listarAmbito(): Promise<ResponseGenerico<ambitoInterface>>{
  return new Promise((resolve, reject) => {
    this._http.get(`${environment.url_academico}/private/listarAsignaturaPrepa`).subscribe((response: ResponseGenerico<ambitoInterface>) => {
      resolve(response);
    }, reject);
  })
}


ListarAsignadosActivos(): Promise<ResponseGenerico<AsignaturasInterface>>{
  return new Promise((resolve, reject) => {
    this._http.get(`${environment.url_academico}/private/listarDistributivosActivos`).subscribe((response: ResponseGenerico<AsignaturasInterface>) => {
      resolve(response);
    }, reject);
  })
}


}
