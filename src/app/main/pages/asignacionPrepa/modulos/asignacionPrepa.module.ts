import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PrincipalComponent } from "../componentes/principal/principal.component";
import { RUTA_ASIGNACION_PREPA } from '../rutas/asignacionPrepa.routing';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { CoreCommonModule } from '@core/common.module';
import { ContentHeaderModule } from 'app/layout/components/content-header/content-header.module';
import { MaterialModule } from 'app/main/shared/material/material.module';

@NgModule({
    declarations: [PrincipalComponent],
    imports: [
      CommonModule,
      RouterModule.forChild(RUTA_ASIGNACION_PREPA),
      FormsModule,
      ReactiveFormsModule,
      MaterialModule,
      ContentHeaderModule,
      CoreCommonModule
    ]
  })

  export class asignacionPrepaModule { }