export interface AsignaturasInterface{
             disCodigo: number;
             docCodigo: number;
             curparCodigo: number;
             curCodigo: number;
             reanleCodigo: number;
             disEstado: number;
             tutor: number;
            insestCodigo: number;
             acaMallaAsignatura: {
                 maasCodigo: number;
                 curCodigo: number;
                 maasEstado:  number;
                 maasHoras: number;
                 maasNemonico: string;
                 reanleCodigo: number;
                 acaAsignatura: {
                     asiCodigo: number;
                     asiDescripcion: string;
                     asiHoras: number;
                     asiEstado: number;
                     asiNemonico: null,
                     espCodigo: null,
                     graCodigo: number;
                     acaAgrupacion: {
                         agrCodigo: number;
                         agrDescripcion: string;
                         agrEstado: number;
                         agrNemonico: string;
                    },
                     acaAreaConocimiento: {
                         arcoCodigo: number;
                         arcoDescripcion: string;
                         arcoEstado: number;
                         arcoNemonico: string;
                    },
                     acaTipoAsignatura: {
                         tiasCodigo: number;
                         tiasDescripcion: string;
                         tiasEstado: number;
                         tiasObligatorio: string;
                    },
                     acaTipoValoracion: {
                         tivaCodigo: number;
                         tivaDescripcion: string;
                        tivaEstado: number;
                    }
                },
                 apreCodigo: number;
            }
}