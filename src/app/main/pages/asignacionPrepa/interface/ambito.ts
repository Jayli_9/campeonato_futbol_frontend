export interface ambitoInterface{
    apreCodigo: number;
    apreDescripcion: string;
    apreEstado: number;
    graCodigo: number;
    apreHras: number;
}