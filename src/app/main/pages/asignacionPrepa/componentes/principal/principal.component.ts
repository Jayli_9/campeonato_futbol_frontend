import { Component, OnInit } from '@angular/core';
import { AcademicoService } from "../../servicios/academico.service";
import { GieeService } from "../../servicios/giee.service";
import { User } from 'app/auth/models';

@Component({
  selector: 'app-principal',
  templateUrl: './principal.component.html',
  styleUrls: ['./principal.component.scss']
})
export class PrincipalComponent implements OnInit {

  contentHeader: object;

  //datos de institucion
  currentUser: User;
  datosInstitucion = [];
  listaAsignatura =  [];
  amie;

  //lista de 

  //datos de ambito seleccion
  codigoAmbito;
  listaAmbito;

  constructor(
    private readonly academicoService: AcademicoService,
    private readonly gieeService: GieeService
  ) { 
    this.currentUser = JSON.parse(localStorage.getItem("currentUser"))
  }

  ngOnInit() {

    this.datosInstitucion.push(this.currentUser.sede);
    this.amie = this.datosInstitucion[0].nemonico;


    this.gieeService.buscarinstitucionPorAmie(this.amie).then(data => {
      let objeto = data.objeto;

      let datoist = objeto.insCodigo;


      this.academicoService.ListarAsignadosActivos().then(data => {
        let listaAsignacion = data.listado;

        listaAsignacion.forEach(element => {

          if(element.insestCodigo == datoist){

            this.listaAsignatura.push(element);

            //console.log(this.listaAsignatura);
          }

        });

      })


    })


    this.academicoService.listarAmbito().then(data => {
      this.listaAmbito = data.listado;
    })

    this.contentHeader = {
      headerTitle: 'Asignacion Prepa',
      actionButton: false,
      breadcrumb: {
          type: '',
          links: [
              {
                name: 'Inicio',
                isLink: true,
                link: '/pages/inicio'
              },
              {
                name: 'asignacionPrepa',
                isLink: false
              }
          ]
      }
    };
  }


  seleccionAmbito(event){

    this.listaAsignatura.forEach(element => {
      
    })


  }

}
