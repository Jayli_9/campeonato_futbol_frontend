import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ModeloEvaluacionGeneralComponent } from './modelo-evaluacion-general.component';

describe('ModeloEvaluacionGeneralComponent', () => {
  let component: ModeloEvaluacionGeneralComponent;
  let fixture: ComponentFixture<ModeloEvaluacionGeneralComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ModeloEvaluacionGeneralComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ModeloEvaluacionGeneralComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});