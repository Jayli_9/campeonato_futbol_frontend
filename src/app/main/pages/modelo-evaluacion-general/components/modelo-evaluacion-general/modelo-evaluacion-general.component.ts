import { Component, OnInit, ViewChild } from "@angular/core";
import { UntypedFormBuilder, UntypedFormGroup, Validators } from "@angular/forms";
import { MatPaginator } from "@angular/material/paginator";
import { MatSort } from "@angular/material/sort";
import { MatTableDataSource } from "@angular/material/table";
import { NgbModal } from "@ng-bootstrap/ng-bootstrap";
import { AuthenticationService } from "app/auth/service";
import { NgxSpinnerService } from "ngx-spinner";
import Swal from "sweetalert2";
import { CursoExtraordinaria } from "../../interfaces/curso-extraordinaria";
import { InstEstablecimiento } from "../../interfaces/instEstablecimiento";
import { Institucion } from "../../interfaces/institucion";
import { ModeloEvaluacion } from "../../interfaces/modelo-evaluacion";
import { ModeloEvaluacionGeneral } from "../../interfaces/modelo-evaluacion-general";
import { RegimenAnioLectivo } from "../../interfaces/regimen-anio-lectivo";
import { RespuestaCursoExtraordinariaInterfaz } from "../../interfaces/respuesta-curso-extraordinaria-interfaz";
import { RespuestaInstEstablecimientoInterfaz } from "../../interfaces/respuesta-inst-establecimiento-interfaz";
import { RespuestaInstitucionInterfaz } from "../../interfaces/respuesta-institucion-interfaz";
import { RespuestaModeloEvaluacionGeneralInterfaz } from "../../interfaces/respuesta-modelo-evaluacion-general-interfaz";
import { RespuestaModeloEvaluacionInterfaz } from "../../interfaces/respuesta-modelo-evaluacion-interfaz";
import { RespuestaRegimenAnioLectivoInterfaz } from "../../interfaces/respuesta-regimen-anio-lectivo-interfaz";
import { ModeloEvaluacionGeneralService } from "../../services/modelo-evaluacion-general.service";
import { CursoExtraParametros } from "../../interfaces/curso-extra-parametros";
import { RegimenAnioLectParametros } from "../../interfaces/regimen-anio-lect-parametros";

@Component({
    selector: 'app-modelo-evaluacion',
    templateUrl: './modelo-evaluacion-general.component.html',
    styleUrls: ['./modelo-evaluacion-general.component.scss']
})
export class ModeloEvaluacionGeneralComponent implements OnInit {

    public contentHeader: object;
    public frmModeloEvaluacionGeneralCreateEdit: UntypedFormGroup;
    public submitted = false;

    listaModeloEvaluacionGeneral:ModeloEvaluacionGeneral[];
    listaRegimenAnioLectivo:RegimenAnioLectivo[];
    listaModeloEvaluacion:ModeloEvaluacion[];
    listaInstEstablecimento: InstEstablecimiento[];
    listaCursoExtraordinaria: CursoExtraordinaria[];

    institucion:Institucion;

    columnasModeloEvaluacionGeneral=['num', 'descripcion', 'nemonico', 'modelo-evaluacion', 'anio-lectivo', 'estado', 'acciones'];
    dataSource: MatTableDataSource<ModeloEvaluacionGeneral>;

    @ViewChild(MatPaginator) paginator: MatPaginator;
    @ViewChild(MatSort) sort: MatSort;
    constructor(private readonly fb: UntypedFormBuilder,
        private modalService: NgbModal,
        public spinner: NgxSpinnerService,
        private _modeloEvaluacionGeneralService: ModeloEvaluacionGeneralService,
        private _authenticationService: AuthenticationService
        ){
    }

    ngOnInit(): void {
        this.contentHeader = {
            headerTitle: 'Modelo Evaluación General',
            actionButton: false,
            breadcrumb: {
                type: '',
                links: [
                    {
                      name: 'Inicio',
                      isLink: true,
                      link: '/pages/inicio'
                    },
                    {
                      name: 'Modelo Evaluación General',
                      isLink: false
                    },
                ]
            }
        };
        this.obtenerInstitucionPorAmie();
    }

    obtenerInstitucionPorAmie() {
        //let codInstitucion = this.docente.codInstitucion;
        this.spinner.show();
        const currentUser = this._authenticationService.currentUserValue;
        let codAmie = currentUser.sede.nemonico;
        this._modeloEvaluacionGeneralService.obtenerInstitucionPorAmie(codAmie).subscribe(
            (respuesta: RespuestaInstitucionInterfaz)=>{
                this.institucion = respuesta.objeto;
                this.listarEstablecimientoInstitucion();
                this.listarModeloEvaluacionPorEstado();
                this.spinner.hide();
            },
            (error:any)=>{
                this.spinner.hide();
                Swal.fire('Ups! ocurrió un error al cargar la Institución','','error'); 
            }
        );
    }

    listarEstablecimientoInstitucion() {
        //let codInstitucion = this.docente.codInstitucion;
        let codInstitucion = this.institucion.insCodigo;
        this._modeloEvaluacionGeneralService.obtenerTodosEstablecimientoPorInstitucion(codInstitucion).subscribe(
            (respuesta: RespuestaInstEstablecimientoInterfaz)=>{
                this.listaInstEstablecimento=respuesta.listado;
                this.listarCursoExtaordinarioPorListaEstablecimiento();
            },
            (error:any)=>{
                this.spinner.hide();
                Swal.fire('Ups! ocurrió un error al cargar los establecimientos de la institución','','error'); 
            }
        );
    }

    listarCursoExtaordinarioPorListaEstablecimiento() {
        let insestCodigo = this.listaInstEstablecimento.map((estable)=>estable.insestCodigo);
        let cursoExtraParametros:CursoExtraParametros = {listaCodigoEstablecimiento:insestCodigo};
        this._modeloEvaluacionGeneralService.obtenerTodosCursosExtraordinarioPorListaEstablecimiento(cursoExtraParametros).subscribe(
            (respuesta:RespuestaCursoExtraordinariaInterfaz)=>{
                this.listaCursoExtraordinaria = respuesta.listado;
                this.listarRegimenAnioLectivo();
            },
            (error:any)=>{
                this.spinner.hide();
                Swal.fire('Ups! ocurrió un error al cargar los cursos extraordinarios','','error'); 
            }
        );
    }

    listarRegimenAnioLectivo() {
        let reanleCodigo = this.listaCursoExtraordinaria.map((cursoExtrao) => cursoExtrao.reanleCodigo);
        let reanleCodigoLimpio:number[] =  Array.from(new Set(reanleCodigo));
        let regimenAnioLectParametros:RegimenAnioLectParametros = {listaReanleCodigo:reanleCodigoLimpio};
        this._modeloEvaluacionGeneralService.obtenerTodasRegimenAnioLectivoPorListaEstablecimiento(regimenAnioLectParametros).subscribe(
            (respuesta:RespuestaRegimenAnioLectivoInterfaz) => {
                this.listaRegimenAnioLectivo = respuesta.listado;
                this.listarModeloEvaluacionGeneral();
            },
            (error:any)=>{
                this.spinner.hide();
                Swal.fire('Ups! ocurrió un error al cargar los regimen del año lectivo','','error'); 
            }
        );
    }

    listarModeloEvaluacionPorEstado() {
        this._modeloEvaluacionGeneralService.obtenerTodosModelosEvaluacionPorEstado().subscribe(
            (respuesta:RespuestaModeloEvaluacionInterfaz) => {
                this.listaModeloEvaluacion = respuesta.listado;
            },
            (error:any)=>{
                this.spinner.hide();
                Swal.fire('Ups! ocurrió un error al cargar los modelos de evaluación','','error');
            }
        );
    }

    listarModeloEvaluacionGeneral() {
        this._modeloEvaluacionGeneralService.obenerTodosModelosEvaluacionGeneral().subscribe(
            (respuesta:RespuestaModeloEvaluacionGeneralInterfaz) => {
                this.listaModeloEvaluacionGeneral = respuesta.listado;
                for (let i=0; i<this.listaModeloEvaluacionGeneral.length;i++) {
                    let regimanAnioLect:RegimenAnioLectivo=this.listaRegimenAnioLectivo.find((regimanAnioLect)=>regimanAnioLect.reanleCodigo == this.listaModeloEvaluacionGeneral[i].reanleCodigo);
                    if (regimanAnioLect != null && regimanAnioLect != undefined) {
                        this.listaModeloEvaluacionGeneral[i].regDescripcion = regimanAnioLect.regDescripcion;
                        this.listaModeloEvaluacionGeneral[i].anilecAnioInicio = regimanAnioLect.anilecAnioInicio;
                        this.listaModeloEvaluacionGeneral[i].anilecAnioFin = regimanAnioLect.anilecAnioFin;
                    }
                    this.dataSource = new MatTableDataSource(this.listaModeloEvaluacionGeneral);
                    this.dataSource.paginator = this.paginator;
                    this.dataSource.paginator._intl.itemsPerPageLabel="Asignaturas por página";
                    this.dataSource.paginator._intl.nextPageLabel="Siguiente";
                    this.dataSource.paginator._intl.previousPageLabel="Anterior";
                    this.dataSource.sort = this.sort;
                }
            },
            (error:any)=>{
                this.spinner.hide();
                Swal.fire('Ups! ocurrió un error al cargar los modelos de evaluación','','error');
            }
        );
    }

    refrescarComponentes() {
        this.frmModeloEvaluacionGeneralCreateEdit.get('modgenDescripcion').reset;
        this.frmModeloEvaluacionGeneralCreateEdit.get('modgenNemonico').reset;
        this.frmModeloEvaluacionGeneralCreateEdit.get('reanleCodigo').reset;
    }

    nuevoModeloEvaluacionGeneral(modalForm) {
        this.submitted=false;
        this.frmModeloEvaluacionGeneralCreateEdit = this.fb.group({
            moevCodigo:[null, [Validators.required]],
            modgenDescripcion:['', [Validators.required]],
            modgenNemonico:['', [Validators.required]],
            reanleCodigo:[null, [Validators.required]]
        });
        this.modalService.open(modalForm);
    }

    editarModeloEvaluacionGeneral(modalForm, modeloEvaluacionGeneral:ModeloEvaluacionGeneral) {

        if (modeloEvaluacionGeneral.modgenEstado == 0) {
            Swal.fire(`Debe activar el modelo de evaluación general.!`,'','success');
            return;
        }

        this.submitted=false;
        this.frmModeloEvaluacionGeneralCreateEdit = this.fb.group({
            modgenCodigo:[modeloEvaluacionGeneral.modgenCodigo],
            moevCodigo:[modeloEvaluacionGeneral.moevCodigo],
            modgenDescripcion:[modeloEvaluacionGeneral.modgenDescripcion],
            modgenNemonico:[modeloEvaluacionGeneral.modgenNemonico],
            modgenEstado:[modeloEvaluacionGeneral.modgenEstado],
            reanleCodigo:[modeloEvaluacionGeneral.reanleCodigo]
        });
        this.modalService.open(modalForm);
    }

    guardarModeloEvaluacionGeneral(modalForm) {

        this.spinner.show();
        this.submitted = true;

        if (this.frmModeloEvaluacionGeneralCreateEdit.invalid) {
            this.spinner.hide();
            return;
        }

        let nuevoModeloEvaluacionGeneral:ModeloEvaluacionGeneral = {
            moevCodigo: this.frmModeloEvaluacionGeneralCreateEdit.get('moevCodigo').value,
            modgenDescripcion: this.frmModeloEvaluacionGeneralCreateEdit.get('modgenDescripcion').value,
            modgenNemonico: this.frmModeloEvaluacionGeneralCreateEdit.get('modgenNemonico').value,
            modgenEstado: 1,
            reanleCodigo: this.frmModeloEvaluacionGeneralCreateEdit.get('reanleCodigo').value
        }
        this._modeloEvaluacionGeneralService.guardarModeloEvaluacionGeneral(nuevoModeloEvaluacionGeneral).subscribe(
            (respuesta:any)=>{
                Swal.fire(`Modelo Evaluación General Creada!`,'','success');
                this.spinner.hide();
                this.listarModeloEvaluacionGeneral();
            },
            (error:any)=>{
                Swal.fire(`No se pudo crear el Modelo de Evaluación General`,'','error');
                this.spinner.hide();
                this.listarModeloEvaluacionGeneral();
            }
        );

        modalForm.close('Accept click');

    }

    actualizarModeloEvaluacionGeneral(modalForm) {

        this.spinner.show();
        this.submitted = true;

        if (this.frmModeloEvaluacionGeneralCreateEdit.invalid) {
            this.spinner.hide();
            return;
        }

        let actualizarModeloEvaluacionGeneral:ModeloEvaluacionGeneral = {
            modgenCodigo: this.frmModeloEvaluacionGeneralCreateEdit.get('modgenCodigo').value,
            moevCodigo: this.frmModeloEvaluacionGeneralCreateEdit.get('moevCodigo').value,
            modgenDescripcion: this.frmModeloEvaluacionGeneralCreateEdit.get('modgenDescripcion').value,
            modgenNemonico: this.frmModeloEvaluacionGeneralCreateEdit.get('modgenNemonico').value,
            modgenEstado: this.frmModeloEvaluacionGeneralCreateEdit.get('modgenEstado').value,
            reanleCodigo: this.frmModeloEvaluacionGeneralCreateEdit.get('reanleCodigo').value
        }

        
        this._modeloEvaluacionGeneralService.guardarModeloEvaluacionGeneral(actualizarModeloEvaluacionGeneral).subscribe(
            (respuesta:any)=>{
                Swal.fire(`Modelo Evaluación General actualizada!`,'','success');
                this.spinner.hide();
                this.listarModeloEvaluacionGeneral();
            },
            (error:any)=>{
                Swal.fire(`No se pudo actualizar el Modelo de Evaluación General`,'','error');
                this.spinner.hide();
                this.listarModeloEvaluacionGeneral();
            }
        );

        modalForm.close('Accept click');
    }


    activarInactivar(modeloEvaluacionGeneral:ModeloEvaluacionGeneral) {
        this.spinner.show();
        if (modeloEvaluacionGeneral.modgenEstado == 1) {
            this._modeloEvaluacionGeneralService.activarInactivarModeloEvaluacionGeneral(modeloEvaluacionGeneral.modgenCodigo).subscribe(
                (respuesta:any)=>{
                    Swal.fire(`Modelo Evaluación General inactivada!`,'','success');
                    this.spinner.hide();
                    this.listarModeloEvaluacionGeneral();
                },
                (error:any)=>{
                    Swal.fire(`No se pudo inactivar el Modelo de Evaluación General`,'','error');
                    this.spinner.hide();
                    this.listarModeloEvaluacionGeneral();
                }
            );
        } else {
            modeloEvaluacionGeneral.modgenEstado = 1;
            this._modeloEvaluacionGeneralService.guardarModeloEvaluacionGeneral(modeloEvaluacionGeneral).subscribe(
                (respuesta:any)=>{
                    Swal.fire(`Modelo Evaluación General activada!`,'','success');
                    this.spinner.hide();
                    this.listarModeloEvaluacionGeneral();
                },
                (error:any)=>{
                    Swal.fire(`No se pudo activar el Modelo de Evaluación General`,'','error');
                    this.spinner.hide();
                    this.listarModeloEvaluacionGeneral();
                }
            );
        }
    }

    applyFilter(event: Event) {
        const filterValue = (event.target as HTMLInputElement).value;
        this.dataSource.filter = filterValue.trim().toLowerCase();
      
        if (this.dataSource.paginator) {
          this.dataSource.paginator.firstPage();
        }
    }
    
    get ReactiveFrmModeloEvaluacionGeneralCreateEdit() {
        return this.frmModeloEvaluacionGeneralCreateEdit.controls;
    }
    
}