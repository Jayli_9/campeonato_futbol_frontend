import { CommonModule } from "@angular/common";
import { NgModule } from "@angular/core";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { RouterModule } from "@angular/router";
import { CoreCommonModule } from "@core/common.module";
import { ContentHeaderModule } from "app/layout/components/content-header/content-header.module";
import { MaterialModule } from "app/main/shared/material/material.module";
import { ModeloEvaluacionGeneralComponent } from "../components/modelo-evaluacion-general/modelo-evaluacion-general.component";
import { RUTA_MODELO_EVALUACION_GENERAL } from "../routes/modelo-evaluacion-general-routing.module";

@NgModule({
    declarations: [ModeloEvaluacionGeneralComponent],
    imports: [
      CommonModule,
      RouterModule.forChild(RUTA_MODELO_EVALUACION_GENERAL),
      FormsModule,
      ReactiveFormsModule,
      MaterialModule,
      ContentHeaderModule,
      CoreCommonModule
    ]
  })
export class ModeloEvaluacionGeneralModule {

}