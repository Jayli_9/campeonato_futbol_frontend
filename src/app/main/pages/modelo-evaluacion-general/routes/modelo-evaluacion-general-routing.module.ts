import { Routes } from "@angular/router";
import { AuthGuard } from "app/auth/helpers";
import { ModeloEvaluacionGeneralComponent } from "../components/modelo-evaluacion-general/modelo-evaluacion-general.component";

export const RUTA_MODELO_EVALUACION_GENERAL:Routes=[
    {
        path:'modelo-evaluacion-general',
        component:ModeloEvaluacionGeneralComponent
    }
]