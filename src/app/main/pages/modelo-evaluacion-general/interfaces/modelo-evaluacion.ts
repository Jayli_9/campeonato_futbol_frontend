export interface ModeloEvaluacion {
    moevCodigo?: number;
    moevDescripcion: string;
    moevEstado: number;
    moevNemonico: string;
}