export interface ModeloEvaluacionGeneral{
    modgenCodigo?: number;
    moevCodigo: number;
    modgenDescripcion: string;
    modgenNemonico: string;
    modgenEstado: number;
    reanleCodigo: number;
    moevDescripcion?: string;
    regDescripcion?: string;
    anilecAnioInicio?:number;
    anilecAnioFin?:number;
    moevaNemonico?:string;
}