import { RespuestaGeneralInterfaz } from "app/main/shared/interfaces/respuesta-general-interfaz";
import { ModeloEvaluacionGeneral } from "./modelo-evaluacion-general";

export interface RespuestaModeloEvaluacionGeneralInterfaz extends RespuestaGeneralInterfaz {
    listado: ModeloEvaluacionGeneral[];
    objeto: ModeloEvaluacionGeneral;
}