import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { environment } from "environments/environment";
import { ModeloEvaluacionGeneral } from "../interfaces/modelo-evaluacion-general";
import { RespuestaCursoExtraordinariaInterfaz } from "../interfaces/respuesta-curso-extraordinaria-interfaz";
import { RespuestaInstEstablecimientoInterfaz } from "../interfaces/respuesta-inst-establecimiento-interfaz";
import { RespuestaInstitucionInterfaz } from "../interfaces/respuesta-institucion-interfaz";
import { RespuestaModeloEvaluacionGeneralInterfaz } from "../interfaces/respuesta-modelo-evaluacion-general-interfaz";
import { RespuestaModeloEvaluacionInterfaz } from "../interfaces/respuesta-modelo-evaluacion-interfaz";
import { RespuestaRegimenAnioLectivoInterfaz } from "../interfaces/respuesta-regimen-anio-lectivo-interfaz";
import { CursoExtraParametros } from "../interfaces/curso-extra-parametros";
import { RegimenAnioLectParametros } from "../interfaces/regimen-anio-lect-parametros";

@Injectable({
    providedIn: 'root'
})
export class ModeloEvaluacionGeneralService {
    
    url_academico=environment.url_academico;
    url_catalogo=environment.url_catalogo;
    url_institucion=environment.url_institucion;
    url_oferta=environment.url_oferta;

    constructor(
        public _http:HttpClient
    ) {}

    //Servicios de Academico
    guardarModeloEvaluacionGeneral(modeloEvaluacionGeneral:ModeloEvaluacionGeneral) {
        let url_ws=`${this.url_academico}/private/guardarModevaGenDTO`;
        return this._http.post(url_ws,modeloEvaluacionGeneral);
    }

    activarInactivarModeloEvaluacionGeneral(modgenCodigo:any) {
        let url_ws=`${this.url_academico}/private/eliminarModevaGen/${modgenCodigo}`;
        return this._http.delete(url_ws);
    }

    obenerTodosModelosEvaluacionGeneral() {
        let url_ws=`${this.url_academico}/private/listarTodosModevaGen`;
        return this._http.get<RespuestaModeloEvaluacionGeneralInterfaz>(url_ws);
    } 

    obtenerTodosModelosEvaluacionPorEstado() {
        let url_ws=`${this.url_academico}/private/listarModelosEvaluacionPorEstado`;
        return this._http.get<RespuestaModeloEvaluacionInterfaz>(url_ws);
    }

    //Servicio de catalogos
    obtenerTodasRegimenAnioLectivoPorListaEstablecimiento(regimenAnioLectParametros:RegimenAnioLectParametros) {
        let url_ws=`${this.url_catalogo}/private/listaTodasRegionAnioLectivoPorListaEstablecimiento`;
        return this._http.post<RespuestaRegimenAnioLectivoInterfaz>(url_ws, regimenAnioLectParametros);
    }

    //servicios web de ofertas
    obtenerTodosCursosExtraordinarioPorListaEstablecimiento(cursoExtraParametros:CursoExtraParametros) {
        let url_ws=`${this.url_oferta}/private/listarCursosExtraordinariosPorListaEstablecimiento`;
        return this._http.post<RespuestaCursoExtraordinariaInterfaz>(url_ws, cursoExtraParametros);
    }

    //servicios web de instituciones
    obtenerInstitucionPorAmie(insAmie:any) {
        let url_ws=`${this.url_institucion}/private/buscarInstitucionPorAmie/${insAmie}`;
        return this._http.get<RespuestaInstitucionInterfaz>(url_ws);
    }

    obtenerTodosEstablecimientoPorInstitucion(codigoInstitucion:any) {
        let url_ws=`${this.url_institucion}/private/listarEstablecimientosPorInstitucion/${codigoInstitucion}`;
        return this._http.get<RespuestaInstEstablecimientoInterfaz>(url_ws);
    }

}