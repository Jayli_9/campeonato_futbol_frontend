import { TestBed } from '@angular/core/testing';
import { ModeloEvaluacionGeneralService } from './modelo-evaluacion-general.service';

describe('ModeloEvaluacionGeneralService', () => {
  let service: ModeloEvaluacionGeneralService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(ModeloEvaluacionGeneralService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});