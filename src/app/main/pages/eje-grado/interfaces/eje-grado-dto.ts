export interface EjeGradoDto {
    ejgrCodigo?:number;
    ejeCodigo:number;
    graCodigo:number;
    reanleCodigo:number;
    ejeDescripcion:string;
    graDescripcion: string;
    ejgrEstado:number;
    periodoLectivo: string;
}
