import {EjeConstitutivo} from '../../eje/interfaces/eje-constitutivo';

export interface AcaEjeGrado {
    ejgrCodigo: number;
    acaEje: EjeConstitutivo;
    graCodigo: number;
    ejgrEstado: number;
    reanleCodigo: number;
}
