import {RespuestaGeneralInterfaz} from '../../../shared/interfaces/respuesta-general-interfaz';
import {EjeGradoDto} from './eje-grado-dto';

export interface RespuestaEjeGradoDto extends RespuestaGeneralInterfaz {
    listado: EjeGradoDto[];
    objeto: EjeGradoDto;
}
