import {RespuestaGeneralInterfaz} from '../../../shared/interfaces/respuesta-general-interfaz';
import {EjeGrado} from './eje-grado';

export interface RespuestaEjeGrado extends RespuestaGeneralInterfaz {
    listado: EjeGrado[];
    objeto: EjeGrado;
}
