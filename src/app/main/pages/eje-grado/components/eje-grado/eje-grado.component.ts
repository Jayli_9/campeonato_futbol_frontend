import {Component, OnInit, ViewChild} from '@angular/core';
import {UntypedFormBuilder, UntypedFormGroup, Validators} from '@angular/forms';
import {MatTableDataSource} from '@angular/material/table';
import {MatPaginator} from '@angular/material/paginator';
import {MatSort} from '@angular/material/sort';
import {NgbModal} from '@ng-bootstrap/ng-bootstrap';
import {NgxSpinnerService} from 'ngx-spinner';
import Swal from 'sweetalert2';
import {RespuestaEjeGrado} from '../../interfaces/respuesta-eje-grado';
import {PeriodoLectivoDto} from '../../interfaces/periodo-lectivo-dto';
import {CatalogoService} from '../../../../shared/services/catalogo.service';
import {EjeGradoService} from '../../services/eje-grado.service';
import {EjeGrado} from '../../interfaces/eje-grado';
import {EjeGradoWorkflowService} from '../../services/eje-grado-workflow.service';
import {EjeConstitutivo} from '../../../eje/interfaces/eje-constitutivo';
import {RespuestaEjeConstitutivo} from '../../../eje/interfaces/respuesta-eje-constitutivo';
import {EjeConstitutivoService} from '../../../eje/services/eje-constitutivo.service';
import {RespuestaGradoInterfaz} from '../../../../shared/interfaces/respuesta-grado-interfaz';
import {CatGrado} from '../../../../shared/interfaces/cat-grado';
import {forkJoin} from 'rxjs';
import {EjeGradoDto} from '../../interfaces/eje-grado-dto';

@Component({
    selector: 'app-eje-grado',
    templateUrl: './eje-grado.component.html',
    styleUrls: ['./eje-grado.component.scss']
})
export class EjeGradoComponent implements OnInit {
    public contentHeader: object;
    public frmEjeGradoCreateEdit: UntypedFormGroup;
    public submitted = false;
    columnasEjeGrados = ['codigo', 'descripcion', 'grado', 'reanleCodigo', 'estado', 'acciones'];
    dataSource: MatTableDataSource<EjeGradoDto>;

    @ViewChild(MatPaginator) paginator: MatPaginator;
    @ViewChild(MatSort) sort: MatSort;
    listaPeriodoLectivo: PeriodoLectivoDto[] = [];
    listaEjeGradosDto: EjeGradoDto[] = null;
    listaEjesConstitutivos: EjeConstitutivo[] = [];
    listaGrados: CatGrado[] = [];

    constructor(
        private readonly fb: UntypedFormBuilder,
        private modalService: NgbModal,
        public spinner: NgxSpinnerService,
        private _ejeGradoService: EjeGradoService,
        private _ejeGradoWorkflowService: EjeGradoWorkflowService,
        private _ejeService: EjeConstitutivoService,
        private _catalogoService: CatalogoService
    ) {
    }

    ngOnInit(): void {
        this.contentHeader = {
            headerTitle: 'Eje Grado',
            actionButton: false,
            breadcrumb: {
                type: '',
                links: [
                    {
                        name: 'Inicio',
                        isLink: true,
                        link: '/pages/inicio'
                    },
                    {
                        name: 'Eje Grado',
                        isLink: false
                    },
                ]
            }
        };

        this.listarRegimenAnioLectivos();
        this.listarGrados();
        this.listarEjesConstitutivos();
        this.listarTodosLosEjesGrado();
    }

    listarTodosLosEjesGrado() {
        this.spinner.show();
        this._ejeGradoService.listarTodosLosEjeGradosDTO().subscribe(
            (respuesta: RespuestaEjeGrado) => {

                this.convertirListaEjeGradoAdto(respuesta.listado);

                this.dataSource = new MatTableDataSource(this.listaEjeGradosDto);
                this.dataSource.paginator = this.paginator;
                this.dataSource.paginator._intl.itemsPerPageLabel = 'Eje Grado por página';
                this.dataSource.paginator._intl.nextPageLabel = 'Siguiente';
                this.dataSource.paginator._intl.previousPageLabel = 'Anterior';
                this.dataSource.sort = this.sort;
                this.spinner.hide();

            },
            (error: any) => {
                this.spinner.hide();
                Swal.fire('Ups! ocurrió un error al cargar Ejes Grado', '', 'error');
            }
        );
    }

    listarRegimenAnioLectivos() {
        this._ejeGradoWorkflowService.obtenerPeriodosLectivos().subscribe({
            next: (periodos: PeriodoLectivoDto[] | null) => {
                if (periodos) {
                    this.listaPeriodoLectivo = periodos;
                } else {
                    console.error('No se recibieron datos de periodos lectivos');
                }
            },
            error: (error) => {
                return null;
                console.error('Se produjo un error al obtener los periodos lectivos', error);
            }
        });
    }

    listarEjesConstitutivos() {
        this._ejeService.listarEjesConstitutivosActivos()
            .subscribe((respuesta: RespuestaEjeConstitutivo) => {
                    this.listaEjesConstitutivos = respuesta.listado;
                }, error => {
                    return null;
                }
            );
    }

    listarGrados() { //grados de nivel 6
        this._catalogoService.buscarGradosNivel(6).subscribe((respuesta: RespuestaGradoInterfaz) => {
            this.listaGrados = respuesta.listado;
        }, error => {
            return null;
        });
    }

    nuevoEjeGrado(modalForm) {
        this.submitted = false;
        this.frmEjeGradoCreateEdit = this.fb.group({
            reanleCodigo: [Validators.required],
            eje: ['', [Validators.required, Validators.maxLength(20)]],
            grado: [Validators.required]

        });
        this.modalService.open(modalForm);
    }




    editEje(modalForm, ejeGradoEdit: EjeGradoDto) {
        this.submitted = false;
        this.frmEjeGradoCreateEdit = this.fb.group({
            codigo: [ejeGradoEdit.ejgrCodigo],
            eje: [ejeGradoEdit.ejeCodigo, [Validators.required]],
            grado: [ejeGradoEdit.graCodigo, [Validators.required]],
            reanleCodigo: [ejeGradoEdit.reanleCodigo, [Validators.required]],
            estado: [ejeGradoEdit.ejgrEstado]

        });
        this.modalService.open(modalForm);
    }

    guardarEje(modalForm) {

        this.spinner.show();
        this.submitted = true;
        if (this.frmEjeGradoCreateEdit.invalid) {
            this.spinner.hide();
            return;
        }
        let ejeGradoNuevo: EjeGrado = {
            reanleCodigo: this.frmEjeGradoCreateEdit.get('reanleCodigo').value,
            ejeCodigo: this.frmEjeGradoCreateEdit.get('eje').value,
            graCodigo: this.frmEjeGradoCreateEdit.get('grado').value,
            ejgrEstado: 1
        };
        let ejeGradoEncontrado:EjeGrado
        this._ejeGradoService.buscarEjeGradoPorGradoPorEjePorPeriodo(ejeGradoNuevo).subscribe(
            (respuesta:RespuestaEjeGrado)=>{
                ejeGradoEncontrado=respuesta.objeto
                if(ejeGradoEncontrado.ejgrCodigo===null){
                    this._ejeGradoService.guardarActualizarEjeGrado(ejeGradoNuevo).subscribe(
                        (respuesta: any) => {
                            Swal.fire(`Eje Grado Creado!`, '', 'success');
                            this.spinner.hide();
                            this.listarTodosLosEjesGrado();

                        },
                        (error: any) => {
                            Swal.fire(`No se pudo crear el Eje Grado`, '', 'error');
                            this.spinner.hide();
                            this.listarTodosLosEjesGrado();
                        }
                    );


                }
                else{
                    Swal.fire(`No se puede guardar Duplicados`, '', 'error');
                    this.spinner.hide();
                    this.listarTodosLosEjesGrado();
                }

            }

        )

        modalForm.close('Accept click');
    }

    activarInactivar(ejeGradoActualizarDto: EjeGradoDto) {
        let ejeGradoActualizar:EjeGrado
        ejeGradoActualizar=this.convertirDtoAEjeGrado(ejeGradoActualizarDto)
        this.spinner.show();
        if (ejeGradoActualizar.ejgrEstado == 1) {
            this._ejeGradoService.inactivarEjeGrado(ejeGradoActualizar.ejgrCodigo).subscribe(
                (respuesta: any) => {
                    this.spinner.hide();
                    Swal.fire(' Eje Inactivado', '', 'success');
                    this.listarTodosLosEjesGrado();
                },
                (error: any) => {
                    this.spinner.hide();
                    Swal.fire('Ups! ocurrió un error', '', 'error');
                    this.listarTodosLosEjesGrado();
                }
            );
        } else {

            ejeGradoActualizar.ejgrEstado = 1;
            this._ejeGradoService.guardarActualizarEjeGrado(ejeGradoActualizar).subscribe(
                (respuesta: any) => {

                    this.spinner.hide();
                    Swal.fire(' Eje Grado Activado', '', 'success');
                    this.listarTodosLosEjesGrado();
                },
                (error: any) => {
                    this.spinner.hide();
                    Swal.fire('Ups! ocurrió un error', '', 'error');
                    this.listarTodosLosEjesGrado();
                }
            );
        }

    }

    actualizarEjeGrado(modalForm) {
        this.spinner.show();
        this.submitted = true;
        // stop here if form is invalid
        if (this.frmEjeGradoCreateEdit.invalid) {
            this.spinner.hide();
            return;
        }
        let ejeGradoGuardar: EjeGrado = {
            reanleCodigo: this.frmEjeGradoCreateEdit.get('reanleCodigo').value,
            ejgrCodigo: this.frmEjeGradoCreateEdit.get('codigo').value,
            ejeCodigo: this.frmEjeGradoCreateEdit.get('eje').value,
            graCodigo: this.frmEjeGradoCreateEdit.get('grado').value,
            ejgrEstado: this.frmEjeGradoCreateEdit.get('estado').value,
        };
        this._ejeGradoService.guardarActualizarEjeGrado(ejeGradoGuardar).subscribe(
            (respuesta: any) => {
                Swal.fire(`Eje Grado Actualizado!`, '', 'success');
                this.spinner.hide();
                this.listarTodosLosEjesGrado();
            },
            (error: any) => {
                Swal.fire(`No se pudo guardar el Eje Grado`, '', 'error');
                this.spinner.hide();
                this.listarTodosLosEjesGrado();
            }
        );
        modalForm.close('Accept click');
    }

    applyFilter(event: Event) {
        const filterValue = (event.target as HTMLInputElement).value;
        this.dataSource.filter = filterValue.trim().toLowerCase();

        if (this.dataSource.paginator) {
            this.dataSource.paginator.firstPage();
        }
    }

    get ReactiveFrmEjeGradoCreateEdit() {
        return this.frmEjeGradoCreateEdit.controls;
    }
    private convertirDtoAEjeGrado(ejeGradoDto: EjeGradoDto): EjeGrado {
        let ejeGrado: EjeGrado = {
            ejgrCodigo: ejeGradoDto.ejgrCodigo,
            ejgrEstado: ejeGradoDto.ejgrEstado,
            ejeCodigo:  ejeGradoDto.ejeCodigo || null,
            graCodigo: ejeGradoDto.graCodigo || null,
            reanleCodigo: ejeGradoDto.reanleCodigo || null

        };

        return ejeGrado;
    }

    private convertirListaEjeGradoAdto(listado: EjeGrado[]) {
        this.listaEjeGradosDto = [];

        listado.forEach((ejeGrado: EjeGrado) => {
            let ejeGradoDto: EjeGradoDto = {
                reanleCodigo: ejeGrado.reanleCodigo,
                ejeCodigo: ejeGrado.ejeCodigo,
                graCodigo: ejeGrado.graCodigo,
                ejgrCodigo: ejeGrado.ejgrCodigo,
                ejgrEstado: ejeGrado.ejgrEstado,
                ejeDescripcion: this.listaEjesConstitutivos.find(eje => eje.ejeCodigo === ejeGrado.ejeCodigo)?.ejeDescripcion || '',
                graDescripcion: this.listaGrados.find(grado => grado.graCodigo === ejeGrado.graCodigo)?.graDescripcion || '',
                periodoLectivo: this.listaPeriodoLectivo.find(periodoLectivo => periodoLectivo.reanleCodigo === ejeGrado.reanleCodigo)?.regDescripcion || ''
            };

            this.listaEjeGradosDto.push(ejeGradoDto);
        });
    }

}
