import { ComponentFixture, TestBed } from '@angular/core/testing';

import { EjeGradoComponent } from './eje-grado.component';

describe('EjeGradoComponent', () => {
  let component: EjeGradoComponent;
  let fixture: ComponentFixture<EjeGradoComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ EjeGradoComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(EjeGradoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
