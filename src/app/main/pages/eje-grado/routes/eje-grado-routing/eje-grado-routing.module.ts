import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {Routes} from '@angular/router';
import {EjeConstitutivoComponent} from '../../../eje/components/eje-constitutivo/eje-constitutivo.component';
import {AuthGuard} from '../../../../../auth/helpers';
import {EjeGradoComponent} from '../../components/eje-grado/eje-grado.component';



export const RUTA_EJE_GRADO: Routes = [
  {
    path: 'eje-grado',
    component: EjeGradoComponent,
    canActivate: [AuthGuard]
  }
];
