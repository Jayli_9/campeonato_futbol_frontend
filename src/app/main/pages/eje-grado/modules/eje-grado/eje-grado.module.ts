import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {RouterModule} from '@angular/router';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {MaterialModule} from '../../../../shared/material/material.module';
import {ContentHeaderModule} from '../../../../../layout/components/content-header/content-header.module';
import {CoreCommonModule} from '../../../../../../@core/common.module';
import {RUTA_EJE_GRADO} from '../../routes/eje-grado-routing/eje-grado-routing.module';
import {EjeGradoComponent} from '../../components/eje-grado/eje-grado.component';


@NgModule({
    declarations: [
        EjeGradoComponent
    ],
    imports: [
        CommonModule,
        RouterModule.forChild(RUTA_EJE_GRADO),
        FormsModule,
        ReactiveFormsModule,
        MaterialModule,
        ContentHeaderModule,
        CoreCommonModule
    ]
})
export class EjeGradoModule {
}
