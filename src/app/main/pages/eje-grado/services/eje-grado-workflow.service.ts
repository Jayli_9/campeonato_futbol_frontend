import {Injectable} from '@angular/core';
import {RespuestaRegimenInterfaz} from '../../../shared/interfaces/respuesta-regimen-interfaz';
import {CatalogoService} from '../../../shared/services/catalogo.service';
import {catchError, map, mergeMap, switchMap, toArray} from 'rxjs/operators';
import {forkJoin, from, Observable, throwError} from 'rxjs';
import {CatRegimen} from '../../../shared/interfaces/cat-regimen';
import {PeriodoLectivoDto} from '../interfaces/periodo-lectivo-dto';
import {RespuestaAnioLectivoInterfaz} from '../../../shared/interfaces/respuesta-anio-lectivo-interfaz';
import {EjeGrado} from '../interfaces/eje-grado';
import {EjeGradoDto} from '../interfaces/eje-grado-dto';
import {EjeConstitutivoService} from '../../eje/services/eje-constitutivo.service';
import {RespuestaEjeConstitutivo} from '../../eje/interfaces/respuesta-eje-constitutivo';

@Injectable({
    providedIn: 'root'
})
export class EjeGradoWorkflowService {

    constructor(
        private _ejeConstitutivoService: EjeConstitutivoService,
        private _catalogoService: CatalogoService,) {
    }

    obtenerPeriodosLectivos(): Observable<PeriodoLectivoDto[] | null> {

        return this._catalogoService.listarRegimenTipo().pipe(
            mergeMap((respuesta: RespuestaRegimenInterfaz) => from(respuesta.listado as CatRegimen[])),
            mergeMap((regimen: CatRegimen) =>
                this._catalogoService.buscarAnioLectivo(regimen.regAnioLecActual.anilecCodigo).pipe(
                    map((anioLectivo: RespuestaAnioLectivoInterfaz) => {

                        return {
                            regDescripcion: regimen.regDescripcion,
                            reanleCodigo: regimen.regAnioLecActual.reanleCodigo,
                            anilecAnioInicio: anioLectivo.objeto.anilecAnioInicio,
                            anilecAnioFin: anioLectivo.objeto.anilecAnioFin
                        };
                    })
                )
            ),

            toArray()
        ) as Observable<PeriodoLectivoDto[]>
            ;
    }


}
