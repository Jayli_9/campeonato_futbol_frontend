import { TestBed } from '@angular/core/testing';

import { EjeGradoWorkflowService } from './eje-grado-workflow.service';

describe('EjeGradoWorkflowService', () => {
  let service: EjeGradoWorkflowService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(EjeGradoWorkflowService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
