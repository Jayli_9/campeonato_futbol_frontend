import { TestBed } from '@angular/core/testing';

import { EjeGradoService } from './eje-grado.service';

describe('EjeGradoService', () => {
  let service: EjeGradoService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(EjeGradoService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
