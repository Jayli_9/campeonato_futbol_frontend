import {Injectable} from '@angular/core';
import {environment} from '../../../../../environments/environment';
import {HttpClient} from '@angular/common/http';
import {EjeGrado} from '../interfaces/eje-grado';
import {RespuestaEjeGrado} from '../interfaces/respuesta-eje-grado';

@Injectable({
    providedIn: 'root'
})
export class EjeGradoService {

    url_academico = environment.url_academico;
    url_catalogo = environment.url_catalogo;

    constructor(
        public _http: HttpClient
    ) {
    }

    guardarActualizarEjeGrado(ejeGrado: EjeGrado) {
        let url_ws = `${this.url_academico}/private/guardarEjeGrado`;
        return this._http.post(url_ws, ejeGrado);
    }

    listarTodosLosEjeGradosDTO() {
        let url_ws = `${this.url_academico}/private/listarTodosLosEjeGradosDTO`;
        return this._http.get<RespuestaEjeGrado>(url_ws);
    }

    listarEjeGradosActivos() {
        let url_ws = `${this.url_academico}/private/listarEjeGradosActivos`;
        return this._http.get<RespuestaEjeGrado>(url_ws);
    }

    buscarEjeGradoPorCodigo(codigo: number) {
        let url_ws = `${this.url_academico}/private/buscarEjeGradoPorCodigo/${codigo}`;
        return this._http.get<RespuestaEjeGrado>(url_ws);
    }

    inactivarEjeGrado(codigo: number) {
        let url_ws = `${this.url_academico}/private/eliminarEjeGradoPorId/${codigo}`;
        return this._http.delete(url_ws);
    }

    buscarEjeGradoPorGradoPorEjePorPeriodo(parametro: EjeGrado) {
        let url_ws = `${this.url_academico}/private/buscarEjeGradoPorGradoPorEjePorPeriodo/`;
        return this._http.post<RespuestaEjeGrado>(url_ws, parametro);
    }

}
