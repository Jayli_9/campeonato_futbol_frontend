import { Routes } from "@angular/router";
import { AuthGuard } from "app/auth/helpers";
import { ReporteAsignacionComponent } from "../componentes/reporte-asignacion/reporte-asignacion.component";

export const RUTA_REPORTE_ASIGNACION:Routes=[
    {
        path:'reporteAsignacion',
        component:ReporteAsignacionComponent,
        canActivate:[AuthGuard]
    }
]