export interface nivelInterface{
       nivCodigo: number;
       nivDescripcion: string;
       nivEstado: number;
       nivFechaCreacion: Date;
       nemonicoBilingue: string;
       descripcionBilingue: string;
       tipnivCodigo: number;
}