export interface FiguraProfesional {
    figproCodigo?: number;
	figproDescripcion: string;
	figproEstado: number;
	figproFechaCreacion: string;
}
