import { Component, OnInit } from '@angular/core';
import { CatalogoService } from "../../servicios/catalogo.service";
import { AcademicoService } from "../../servicios/academico.service";
import { NgxSpinnerService } from 'ngx-spinner';

import pdfMake from 'pdfmake/build/pdfmake';
import pdfFonts from 'pdfmake/build/vfs_fonts';
pdfMake.vfs = pdfFonts.pdfMake.vfs;

@Component({
  selector: 'app-reporte-asignacion',
  templateUrl: './reporte-asignacion.component.html',
  styleUrls: ['./reporte-asignacion.component.scss']
})
export class ReporteAsignacionComponent implements OnInit {

  contentHeader: object;

  //variables de asignatura
  listaAsignatura = [];


  //variales nivel
  listaNiveles;
  codigoNivel;

  //variables de figura profesional
  codigoFiguraProfesional;
  listaFiguraProfesional;

  //vairbales de especialidad
  listaEspecialidad;
  codigoEspecialidades;
  nombreEspecialidad;

  //vairables de grados
  listaGrados = [];

  //seleccion de datos
  estudiantesSeleccionados = [];

  constructor(
    private readonly catalogoService: CatalogoService,
    private readonly academicoService: AcademicoService,
    private spinnerService: NgxSpinnerService
  ) { }


  getFiguraProfesionalFiltrados(): any[] {
    return this.listaFiguraProfesional.filter(codigo => codigo.figproCodigo !== 22 && codigo.figproCodigo !== 23);
  }

  ngOnInit(){

    this.catalogoService.listaFiguraProfesional().then(data => {
      this.listaFiguraProfesional = data.listado;
    })

    this.catalogoService.listarNiveles().then(data => {
      this.listaNiveles = data.listado;
    })

    this.contentHeader = {
      headerTitle: 'Reporte Asignatura',
      actionButton: false,
      breadcrumb: {
          type: '',
          links: [
              {
                name: 'Inicio',
                isLink: true,
                link: '/pages/inicio'
              },
              {
                name: 'reporteAsignacion',
                isLink: false
              }
          ]
      }
    };

  }

  seleccionFiguraPro(event){
    this.nombreEspecialidad = null;
    this.estudiantesSeleccionados = [];
    this.listaEspecialidad;
    this.codigoEspecialidades = 0;
    this.codigoEspecialidades = 0;
    this.listaGrados = [];

    this.catalogoService.buscarEspecialidadPorCodigofiguraProfesional(event).then(data => {
      this.listaEspecialidad = data.listado;
    })
  }

  seleccionNivel(event){
    this.nombreEspecialidad = null;
    this.estudiantesSeleccionados = [];
    this.codigoEspecialidades=0;
    this.listaFiguraProfesional;
    this.listaEspecialidad;

    this.listaGrados = [];


    if(event != 8){

      this.catalogoService.buscarGradosPorNiveles(event).then(data=> {
        let listaGrados = data.listado;
  
  
        listaGrados.forEach(element => {
  
          this.listaGrados.push(element);
  
        });
  
  
      })

    }

  }

  seleccionEspecialidad(event){

    this.codigoEspecialidades = event;
    this.nombreEspecialidad = null;

    this.listaEspecialidad.forEach(element => {

      if(element.espCodigo == event){

        this.nombreEspecialidad = element.espDescripcion;

      }

    });

    this.estudiantesSeleccionados = [];
    this.listaGrados = [];


    if(this.codigoEspecialidades != 0){

      this.catalogoService.buscarGradosPorNiveles(this.codigoNivel).then(data=> {
        let listaGrados = data.listado;
  
  
        listaGrados.forEach(element => {
  
          this.listaGrados.push(element);
  
        });
  
  
      })

    }

  }


   // Método para manejar cambios en la selección de checkbox
   toggleCheckbox(grado): void {
    const index = this.estudiantesSeleccionados.findIndex(e => e.graCodigo === grado.graCodigo);

    if (index === -1) {
      this.estudiantesSeleccionados = [...this.estudiantesSeleccionados, grado];
    } else {
      this.estudiantesSeleccionados = this.estudiantesSeleccionados.filter(e => e.graCodigo !== grado.graCodigo);
    }
  }
  


  generarReporte(){
    this.spinnerService.show();
    this.listaAsignatura= [];

    if(this.codigoEspecialidades == 0){

      this.academicoService.listarAsignaturas().then(data => {
        let listado = data.listado;
  
        for (let i = 0; i < listado.length; i++) {
  
          this.estudiantesSeleccionados.forEach(elementSeleccion => {
  
            if(listado[i].graCodigo == elementSeleccion.graCodigo){
              if(listado[i].acaAgrupacion.agrCodigo == 3){
                let datos = {
                  nombreGrado: elementSeleccion.graNemonico,
                  datosAsignatura: listado[i].asiDescripcion,
                  codigoAreaConocimiento: listado[i].acaAreaConocimiento.arcoCodigo
                }
                this.listaAsignatura.push(datos);
              }
            }
      
        }); 
        }
      })

      

      setTimeout(() => {
        
        const result = this.listaAsignatura.reduce((acc,item)=>{
          if(!acc.includes(item)){
            acc.push(item);
          }
          return acc;
        },[])

        result.sort(function(a, b) {
          // Compara los valores de codigoAreaConocimiento de a y b
          // Devuelve un número negativo si a debe ir antes que b
          // Devuelve un número positivo si b debe ir antes que a
          // Devuelve 0 si son iguales y su orden no importa
      
          return a.codigoAreaConocimiento - b.codigoAreaConocimiento;
      });

      setTimeout(() => {
        const bodyData = result.map((datos) => [datos.nombreGrado, datos.datosAsignatura]);

        const pdfDefinition: any = {
          content: [
            {
              table: {
                body: [
                  ['Grado', 'Descripcion de la asignatura'],
                  ...bodyData
                ],
              },
              style: 'datosTabla'
            },
          ],
          styles: {
            datosTabla: {
              fontSize: 6,
               margin: [0, 10, 0, 10], // Margen inferior para separar la tabla de otros elementos
              fillColor: '#F2F2F2', // Color de fondo de la tabla
            }
          },
        }
            location.reload();
            this.spinnerService.hide();
            const pdf = pdfMake.createPdf(pdfDefinition);
            pdf.open();
      }, 5000);

       
          
      }, 10000);
    }else{

      this.academicoService.listarAsignaturas().then(data => {
        let listado = data.listado;
  
        for (let i = 0; i < listado.length; i++) {
  
          this.estudiantesSeleccionados.forEach(elementSeleccion => {
  
            if(listado[i].graCodigo == elementSeleccion.graCodigo && listado[i].espCodigo == this.codigoEspecialidades){
  
              let datos = {
                nombreGrado: elementSeleccion.graNemonico,
                datosAsignatura: listado[i].asiDescripcion,
                nombreEspecialidad: this.nombreEspecialidad
              }
              this.listaAsignatura.push(datos);
            }
      
        }); 
        }
      })

      

      setTimeout(() => {

        const result = this.listaAsignatura.reduce((acc,item)=>{
          if(!acc.includes(item)){
            acc.push(item);
          }
          return acc;
        },[])

        result.sort();


        setTimeout(() => {
          const bodyData = result.map((datos) => [datos.nombreGrado, datos.datosAsignatura, datos.nombreEspecialidad]);

      const pdfDefinition: any = {
        content: [
          {
            table: {
              body: [
                ['Grado', 'Descripcion de la asignatura', 'Especialidad'],
                ...bodyData
              ],
            },
            style: 'datosTabla'
          },
        ],
        styles: {
          datosTabla: {
            fontSize: 6,
            margin: [0, 10, 0, 10], // Margen inferior para separar la tabla de otros elementos
            fillColor: '#F2F2F2', // Color de fondo de la tabla
          }
        },
      }
            location.reload();
            this.spinnerService.hide();
          const pdf = pdfMake.createPdf(pdfDefinition);
          pdf.open();
        }, 5000);

        
      }, 10000);

    }
  }

}
