import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReporteAsignacionComponent } from "../componentes/reporte-asignacion/reporte-asignacion.component";
import { RUTA_REPORTE_ASIGNACION } from "../routes/reporteAsignacion.routing";
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { CoreCommonModule } from '@core/common.module';
import { ContentHeaderModule } from 'app/layout/components/content-header/content-header.module';
import { MaterialModule } from 'app/main/shared/material/material.module';


@NgModule({
    declarations: [ReporteAsignacionComponent],
    imports: [
      CommonModule,
      RouterModule.forChild(RUTA_REPORTE_ASIGNACION),
      FormsModule,
      ReactiveFormsModule,
      MaterialModule,
      ContentHeaderModule,
      CoreCommonModule
    ]
  })

  export class ReporteAsignacionModule { }