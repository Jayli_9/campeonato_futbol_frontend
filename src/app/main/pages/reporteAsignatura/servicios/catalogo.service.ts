import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { environment } from 'environments/environment';
import { ResponseGenerico } from '../interfaces/response-generico';
import { nivelInterface } from "../interfaces/nivel";
import { gradosInterface } from "../interfaces/grado";
import { especialidadInterface } from "../interfaces/especialidad";
import { FiguraProfesional } from "../interfaces/figuraProfesional";

@Injectable({
  providedIn: 'root'
})
export class CatalogoService {

constructor(private _http: HttpClient) { }


listarNiveles(): Promise<ResponseGenerico<nivelInterface>>{
  return new Promise((resolve, reject) => {
this._http.get(`${environment.url_catalogo}/private/listarNiveles`).subscribe((response: ResponseGenerico<nivelInterface>) => {
   resolve(response);
 }, reject);
})
}

buscarGradosPorNiveles(cod): Promise<ResponseGenerico<gradosInterface>>{
  return new Promise((resolve, reject) => {
this._http.get(`${environment.url_catalogo}/private/buscarCatalogoGradosPorNivelCodigo/`+ cod).subscribe((response: ResponseGenerico<gradosInterface>) => {
   resolve(response);
 }, reject);
})
}

listaEspecialidad(): Promise<ResponseGenerico<especialidadInterface>>{
  return new Promise((resolve, reject) => {
this._http.get(`${environment.url_catalogo}/private/listarCatalogoEspecialidades`).subscribe((response: ResponseGenerico<especialidadInterface>) => {
   resolve(response);
 }, reject);
})
}


listaFiguraProfesional(): Promise<ResponseGenerico<FiguraProfesional>>{
  return new Promise((resolve, reject) => {
    this._http.get(`${environment.url_catalogo}/private/listarCatalogoFiguraProfesionales`).subscribe((response: ResponseGenerico<FiguraProfesional>) => {
      resolve(response);
    }, reject);
  })
}

buscarEspecialidadPorCodigofiguraProfesional(cod): Promise<ResponseGenerico<especialidadInterface>>{
  return new Promise((resolve, reject) => {
    this._http.get(`${environment.url_catalogo}/private/buscarCatalogoEspecialidadPorFiguraProfesionalCodigo/`+cod).subscribe((response: ResponseGenerico<especialidadInterface>) => {
      resolve(response);
    }, reject);
  })
}


}
