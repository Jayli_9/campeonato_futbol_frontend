import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { environment } from 'environments/environment';
import { ResponseGenerico } from '../interfaces/response-generico';
import { asignaturaInterface } from "../interfaces/asignatura";

@Injectable({
  providedIn: 'root'
})
export class AcademicoService {

constructor(private _http: HttpClient) { }

listarAsignaturas(): Promise<ResponseGenerico<asignaturaInterface>>{
    return new Promise((resolve, reject) => {
      this._http.get(`${environment.url_academico}/private/listarAsignaturasActivas`).subscribe((response: ResponseGenerico<asignaturaInterface>) => {
        resolve(response);
      }, reject);
    })
  }

}
