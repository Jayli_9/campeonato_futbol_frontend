import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { environment } from "environments/environment";
import { RespuestaComportamientoInterfaz } from "../interfaces/respuesta-comportamiento-interfaz";
import { RespuestaCursoExtraordinariaInterfaz } from "../interfaces/respuesta-curso-extraordinaria-interfaz";
import { RespuestaDocenteInterfaz } from "../interfaces/respuesta-docente-interfaz";
import { RespuestaEspecialidadInterfaz } from "../interfaces/respuesta-especialidad-interfaz";
import { RespuestaGradoInterfaz } from "../interfaces/respuesta-grado-interfaz";
import { RespuestaInstEstablecimientoInterfaz } from "../interfaces/respuesta-inst-establecimiento-interfaz";
import { RespuestaInstitucionInterfaz } from "../interfaces/respuesta-institucion-interfaz";
import { RespuestaJornadaInterfaz } from "../interfaces/respuesta-jornada-interfaz";
import { RespuestaMallaAsignaturaExtraInterfaz } from "../interfaces/respuesta-malla-asignatura-extra-interfaz";
import { RespuestaMatriculaExtraordinariaInterfaz } from "../interfaces/respuesta-matricula-extraordinaria-interfaz";
import { RespuestaModalidadRespuesta } from "../interfaces/respuesta-modalidad-interfaz";
import { RespuestaModuloInterfaz } from "../interfaces/respuesta-modulo-interfaz";
import { RespuestaNotaAlfabetizacionInterfaz } from "../interfaces/respuesta-nota-alfabetizacion-interfaz";
import { RespuestaNotaAsignaturaExtraInterfaz } from "../interfaces/respuesta-nota-asignatura-extra-interfaz";
import { RespuestaNotaComportamientoInterfaz } from "../interfaces/respuesta-nota-comportamiento-interfaz";
import { RespuestaNotasMateriaModuloInterfaz } from "../interfaces/respuesta-nota-materia-modulo-interfaz";
import { RespuestaParmCalificacionCualitativoInterfaz } from "../interfaces/respuesta-param-calificacion-cualitativo-interfaz";
import { RespuestaParmCalificacionCuantitativoInterfaz } from "../interfaces/respuesta-param-calificacion-cuantitativo-interfaz";
import { RespuestaRangoNotaInterfaz } from "../interfaces/respuesta-rango-nota-interfaz";
import { RespuestaRegimenAnioLectivoInterfaz } from "../interfaces/respuesta-regimen-anio-lectivo-interfaz";
import { RespuestaServicioEducativoInterfaz } from "../interfaces/respuesta-servicio-educativo-interfaz";
import { RespuestaTipoValoracionInterfaz } from "../interfaces/respuesta-tipo-valoracion-interfaz";
import { MallaAsigExtraParametro } from "../interfaces/malla-asig-extra-parametro";
import { CursoExtraParametros } from "../interfaces/curso-extra-parametros";
import { RegimenAnioLectParametros } from "../interfaces/regimen-anio-lect-parametros";
import { ServicioEducativoParametros } from "../interfaces/servicio-educativo-parametros";
import { GradosParametros } from "../interfaces/grados-parametros";
import { ModuloParametros } from "../interfaces/modulo-parametros";
import { JornadaParametros } from "../interfaces/jornada-parametros";
import { EspecialidadParametros } from "../interfaces/especialidad-parametros";
import { NotaAsigExtraParametros } from "../interfaces/nota-asig-extra-parametros";
import { MatriculaExtraParametros } from "../interfaces/matricula-extra-parametros";
import { RespuestaGradoEquivalenciaInterfaz } from "../interfaces/respuesta-grado-equivalencia-interfaz";
import { DistributivoExtraParametros } from "../interfaces/distributivo-extra-parametros";
import { RespuestaDistributivoExtraInterfaz } from "../interfaces/respuesta-distributivo-extra-interfaz";
import { RespuestaDistributivoModuloInterfaz } from "../interfaces/respuesta-distributivo-modulo-interfaz";

@Injectable({
    providedIn: 'root'
})
export class EvaluacionService {

    url_academico=environment.url_academico;
    url_oferta=environment.url_oferta;
    url_institucion=environment.url_institucion;
    url_catalogo=environment.url_catalogo;
    url_matricula=environment.url_matricula;
    url_docente=environment.url_docente;

    constructor(
        public _http:HttpClient
    ) { }

    //servicios web de academico
    guardarIndividualNotaAsigExtra(notaAsiExtra:any) {
        let url_ws=`${this.url_academico}/private/guardarNotaAsigExtra`;
        return this._http.post(url_ws,notaAsiExtra);
    }

    guardarListaNotaAsigExtra(listaNotaAsiExtra:any) {
        let url_ws=`${this.url_academico}/private/guardarListaNotaAsigExtra`;
        return this._http.post(url_ws,listaNotaAsiExtra);
    }

    guardarIndividualNotaAlfbetizacion(notaAlfabetizacion:any) {
        let url_ws=`${this.url_academico}/private/guardarNotaAlfabetizacion`;
        return this._http.post(url_ws,notaAlfabetizacion);
    }

    guardarListaNotaAlfabetizacion(listaNotaAlfabetizacion:any) {
        let url_ws=`${this.url_academico}/private/guardarListaNotaAlfabetizacion`;
        return this._http.post(url_ws,listaNotaAlfabetizacion);
    }

    guardarNotaComportamiento(notaComportamiento:any) {
        let url_ws=`${this.url_academico}/private/guardarNotaComportamiento`;
        return this._http.post(url_ws,notaComportamiento);
    }

    guardarListaNotaComportamiento(listaNotaComportamiento:any) {
        let url_ws=`${this.url_academico}/private/guardarListaNotaComportamiento`;
        return this._http.post(url_ws,listaNotaComportamiento);
    }

    inactivarNotaAsigExtra(nasiextCodigo:any) {
        let url_ws=`${this.url_academico}/private/eliminarNotaAsigExtraPorId/${nasiextCodigo}`;
        return this._http.delete(url_ws);
    }

    inactivarNotaAlfabetizacion(nouniCodigo:any) {
        let url_ws=`${this.url_academico}/private/eliminarNotaAlfabetizacionPorId/${nouniCodigo}`;
        return this._http.delete(url_ws);
    }

    inactivarNotaComportamiento(nocoCodigo:any) {
        let url_ws=`${this.url_academico}/private/eliminarNotaComportientoPorId/${nocoCodigo}`;
        return this._http.delete(url_ws);
    }

    obtenerTodasMallaAsignaturaExtraPorDocenteCurextCodigoYParalelo(mallaAsigExtraParametro:MallaAsigExtraParametro) {
        let url_ws=`${this.url_academico}/private/buscarListaMallaAsigExtraPorCurextCodigoDocenteYParalelo`;
        return this._http.post<RespuestaMallaAsignaturaExtraInterfaz>(url_ws,mallaAsigExtraParametro);
    }

    obtenerNotasAsignaturasExtrPorListaMatriculaYMallaAsignatura(notaAsigExtraParametros:NotaAsigExtraParametros) {
        let url_ws=`${this.url_academico}/private/listaNotaAsigExtraMatriculaPorListaMatCodigo`;
        return this._http.post<RespuestaNotaAsignaturaExtraInterfaz>(url_ws, notaAsigExtraParametros);
    }

    obtenerListaNotasAlfabetizacionPorModulo(notaAsigExtraParametros:NotaAsigExtraParametros) {
        let url_ws=`${this.url_academico}/private/listaNotaAlfabetizacionPorModulo`;
        return this._http.post<RespuestaNotaAlfabetizacionInterfaz>(url_ws, notaAsigExtraParametros);
    }

    obtenerListaNotasMateriasPorMallaDocente(notaAsigExtraParametros:NotaAsigExtraParametros) {
        let url_ws=`${this.url_academico}/private/listaNotaAsigExtraMateriaPorDocenteYCursoExtraordinarioYParalelo`;
        return this._http.post<RespuestaNotasMateriaModuloInterfaz>(url_ws, notaAsigExtraParametros);
    }

    obtenerNotasComportamientoPorListaMatriculaYMallaAsignatura(notaAsigExtraParametros:NotaAsigExtraParametros) {
        let url_ws=`${this.url_academico}/private/listarNotaComportamientoPorListaMatReanleYSeredu`;
        return this._http.post<RespuestaNotaComportamientoInterfaz>(url_ws, notaAsigExtraParametros);
    }

    obtenerTipoValoracionPorAsignatura(tivaCodigo:any){
        let url_ws=`${this.url_academico}/private/buscarTipoValoracionPorCodigo/${tivaCodigo}`;
        return this._http.get<RespuestaTipoValoracionInterfaz>(url_ws);
    }

    obtenerListaComportamientoPorEstado() {
        let url_ws=`${this.url_academico}/private/listarComportamientoPorEstado`;
        return this._http.get<RespuestaComportamientoInterfaz>(url_ws);
    }

    obtenerListaDitributivoExtraPorListaCurexListaCuparYDocente(distributivoExtraParametros:DistributivoExtraParametros) {
        let url_ws=`${this.url_academico}/private/listarDistributivoExtraPorListaCurexListaParaleloDocenteYEstado`;
        return this._http.post<RespuestaDistributivoExtraInterfaz>(url_ws, distributivoExtraParametros);
    }

    obtenerListaDitributivoModuloPorListaCurexListaCuparYDocente(distributivoExtraParametros:DistributivoExtraParametros) {
        let url_ws=`${this.url_academico}/private/listarDistributivoModuloPorListaCurexListaParaleloDocenteYEstado`;
        return this._http.post<RespuestaDistributivoModuloInterfaz>(url_ws, distributivoExtraParametros);
    }

    //servicio web de catalogos
    obtenerTodasRegimenAnioLectivoPorListaEstablecimiento(regimenAnioLectParametros:RegimenAnioLectParametros) {
        let url_ws=`${this.url_catalogo}/private/listaTodasRegionAnioLectivoPorListaEstablecimiento`;
        return this._http.post<RespuestaRegimenAnioLectivoInterfaz>(url_ws, regimenAnioLectParametros);
    }

    obtenerTodasJornadasPorCodigo(jornadaParametros:JornadaParametros) {
        let url_ws=`${this.url_catalogo}/private/listarJornadasPorListaJorCodigoYEstado`;
        return this._http.post<RespuestaJornadaInterfaz>(url_ws,jornadaParametros);
    }

    obtenerTodasModalidadesPorListaCodigos(modCodigo:any){
        let url_ws=`${this.url_catalogo}/private/listarModalidadesPorListaModCodigoYEstado/${modCodigo}`;
        return this._http.get<RespuestaModalidadRespuesta>(url_ws);
    }

    obtenerTodosServiciosEducativosPorListaCodigos(servicioEducativoParametros:ServicioEducativoParametros){
        let url_ws=`${this.url_catalogo}/private/listarServicioEducativosPorListaSereduCodigo`;
        return this._http.post<RespuestaServicioEducativoInterfaz>(url_ws, servicioEducativoParametros);
    }

    obtenerServicioEducativoPorCodigo(sereduCodigo:any) {
        let url_ws=`${this.url_catalogo}/private/buscarServicioEducativoPorCodigo/${sereduCodigo}`;
        return this._http.get<RespuestaServicioEducativoInterfaz>(url_ws);
    }

    listarLasEspecialidadesPorListaEspCodigoYEstado(especialidadParametros:EspecialidadParametros) {
        let url_ws=`${this.url_catalogo}/private/buscarEspecialidadesPorListaEspCodigoYEstado`;
        return this._http.post<RespuestaEspecialidadInterfaz>(url_ws, especialidadParametros);
    }

    obtenerTodosGradorPorListaGraCodigoYEstado(gradosParametros:GradosParametros) {
        let url_ws=`${this.url_catalogo}/private/listarGradosPorListaGraCodigoYEstado`;
        return this._http.post<RespuestaGradoInterfaz>(url_ws, gradosParametros);
    }

    obtenerParmCalificacionCualitativoPorReanleCodigo(reanleCodigo:any) {
        let url_ws=`${this.url_catalogo}/private/obtenerParmCalifCualitativaPorReanleCodigoYEstado/${reanleCodigo}`;
        return this._http.get<RespuestaParmCalificacionCualitativoInterfaz>(url_ws);
    }

    obtenerParmCalificacionCuantitativoPorReanleCodigo(reanleCodigo:any) {
        let url_ws=`${this.url_catalogo}/private/obtenerParmCalifCuantitativaPorReanleCodigoYEstado/${reanleCodigo}`;
        return this._http.get<RespuestaParmCalificacionCuantitativoInterfaz>(url_ws);
    }

    obtenerRangoNotaPorReanleCodigo(reanleCodigo:any) {
        let url_ws=`${this.url_catalogo}/private/obtenerRangoNotaPorReanleCodigoYEstado/${reanleCodigo}`;
        return this._http.get<RespuestaRangoNotaInterfaz>(url_ws);
    }

    obtenerGradoEquivaleciaPorServicioEducativa(sereduCodigo:any) {
        let url_ws=`${this.url_catalogo}/private/buscarGradoPorServicioEducativo/${sereduCodigo}`;
        return this._http.get<RespuestaGradoEquivalenciaInterfaz>(url_ws);
    }

    //servicios web de docentes
    obtenerDocentePorIdentificacion(identificacion:any) {
        let url_ws=`${this.url_docente}/private/docenteHispanaPorIdentificacionYEstado/${identificacion}`;
        return this._http.get<RespuestaDocenteInterfaz>(url_ws);
    }

    //servicios web de ofetas
    obtenerTodosCursosExtraordinarioPorListaEstablecimiento(cursoExtraParametros:CursoExtraParametros) {
        let url_ws=`${this.url_oferta}/private/listarCursosExtraordinariosPorListaEstablecimiento`;
        return this._http.post<RespuestaCursoExtraordinariaInterfaz>(url_ws, cursoExtraParametros);
    }

    obtenerTodosCursosExtraordinarioPorRegimenYListaEstablecimiento(cursoExtraParametros:CursoExtraParametros) {
        let url_ws=`${this.url_oferta}/private/buscarCursoExtraordinarioPorRegimenAnioLectivoYlistaEstablecimiento`;
        return this._http.post<RespuestaCursoExtraordinariaInterfaz>(url_ws, cursoExtraParametros);
    }

    obtenerTodosCursosExtraordinarioListacurextCodigo(cursoExtraParametros:CursoExtraParametros) {
        let url_ws=`${this.url_oferta}/private/listarParaleloExtraordinarioPorListaCursoExtra`;
        return this._http.post<RespuestaCursoExtraordinariaInterfaz>(url_ws, cursoExtraParametros);
    }

    obtenerCursoExtraordinarioPorCurextCodigo(curextCodigo:any) {
        let url_ws=`${this.url_oferta}/private/buscarCursoExtraordinarioPorCodigo/${curextCodigo}`;
        return this._http.get<RespuestaCursoExtraordinariaInterfaz>(url_ws);
    }

    //servicios web de instituciones
    obtenerInstitucionPorAmie(insAmie:any) {
        let url_ws=`${this.url_institucion}/private/buscarInstitucionPorAmie/${insAmie}`;
        return this._http.get<RespuestaInstitucionInterfaz>(url_ws);
    }

    obtenerTodosEstablecimientoPorInstitucion(codigoInstitucion:any) {
        let url_ws=`${this.url_institucion}/private/listarEstablecimientosPorInstitucion/${codigoInstitucion}`;
        return this._http.get<RespuestaInstEstablecimientoInterfaz>(url_ws);
    }

    //servicios web de matriculas
    obtenerTodosModulosPorListaCodigo(moduloParametros:ModuloParametros){
        let url_ws=`${this.url_matricula}/private/listarModulosPorListaCodigoYEstado`;
        return this._http.post<RespuestaModuloInterfaz>(url_ws, moduloParametros);
    }

    obtenerModuloPorCodigo(modCodigo:any) {
        let url_ws=`${this.url_matricula}/private/obtenerModuloPorCodigo/${modCodigo}`;
        return this._http.get<RespuestaModuloInterfaz>(url_ws);
    }

    obtenerTodosEstudianteMatriculados(matriculaExtraParametros:MatriculaExtraParametros){
        let url_ws=`${this.url_matricula}/private/obtenerMatriculaExtraordinariaPorCursoExtraordParaleloYEstado`;
        return this._http.post<RespuestaMatriculaExtraordinariaInterfaz>(url_ws, matriculaExtraParametros);
    }

}