import { Component, OnInit, ViewChild } from "@angular/core";
import { MatPaginator } from "@angular/material/paginator";
import { MatSort } from "@angular/material/sort";
import { MatTableDataSource } from "@angular/material/table";
import { Router } from "@angular/router";
import { NgxSpinnerService } from "ngx-spinner";
import Swal from "sweetalert2";
import { Alfabetizacion } from "../../interfaces/alfabetizacion";
import { Calificacion } from "../../interfaces/calificacion";
import { Comportamiento } from "../../interfaces/comportamiento";
import { CursoExtraordinaria } from "../../interfaces/curso-extraordinaria";
import { Docente } from "../../interfaces/docente";
import { MallaAsignaturaExtra } from "../../interfaces/malla-asignatura-extra";
import { MatriculaExtaordinaria } from "../../interfaces/matricula-extraordinaria";
import { ModeloEvaluacion } from "../../interfaces/modelo-evaluacion";
import { ModeloEvaluacionGeneral } from "../../interfaces/modelo-evaluacion-general";
import { Modulo } from "../../interfaces/modulo";
import { NotaAlfabetizacion } from "../../interfaces/nota-alfabetizacion";
import { NotaAsignaturaExtra } from "../../interfaces/nota-asignatura-extra";
import { NotaComportamiento } from "../../interfaces/nota-comportamiento";
import { ParmCalificacionCualitativo } from "../../interfaces/param-calificacion-cualitativo";
import { ParmCalificacionCuantitativo } from "../../interfaces/param-calificacion-cuantitativo";
import { RangoNota } from "../../interfaces/rango-nota";
import { RespuestaComportamientoInterfaz } from "../../interfaces/respuesta-comportamiento-interfaz";
import { RespuestaMallaAsignaturaExtraInterfaz } from "../../interfaces/respuesta-malla-asignatura-extra-interfaz";
import { RespuestaMatriculaExtraordinariaInterfaz } from "../../interfaces/respuesta-matricula-extraordinaria-interfaz";
import { RespuestaModuloInterfaz } from "../../interfaces/respuesta-modulo-interfaz";
import { RespuestaNotaAlfabetizacionInterfaz } from "../../interfaces/respuesta-nota-alfabetizacion-interfaz";
import { RespuestaNotaAsignaturaExtraInterfaz } from "../../interfaces/respuesta-nota-asignatura-extra-interfaz";
import { RespuestaNotaComportamientoInterfaz } from "../../interfaces/respuesta-nota-comportamiento-interfaz";
import { RespuestaParmCalificacionCualitativoInterfaz } from "../../interfaces/respuesta-param-calificacion-cualitativo-interfaz";
import { RespuestaParmCalificacionCuantitativoInterfaz } from "../../interfaces/respuesta-param-calificacion-cuantitativo-interfaz";
import { RespuestaRangoNotaInterfaz } from "../../interfaces/respuesta-rango-nota-interfaz";
import { RespuestaServicioEducativoInterfaz } from "../../interfaces/respuesta-servicio-educativo-interfaz";
import { RespuestaTipoValoracionInterfaz } from "../../interfaces/respuesta-tipo-valoracion-interfaz";
import { ServicioEducativo } from "../../interfaces/servicio-educativo";
import { TipoValoracion } from "../../interfaces/tipo-valoracion";
import { EvaluacionService } from "../../services/evaluacion.service";
import { ProgramaStore } from "../../services/programa-store";
import { MallaAsigExtraParametro } from "../../interfaces/malla-asig-extra-parametro";
import { NotaAsigExtraParametros } from "../../interfaces/nota-asig-extra-parametros";
import { MatriculaExtraParametros } from "../../interfaces/matricula-extra-parametros";
import { GradoEquivalencia } from "../../interfaces/grado-equivalencia";
import { RespuestaGradoEquivalenciaInterfaz } from "../../interfaces/respuesta-grado-equivalencia-interfaz";
import { NgbModal } from "@ng-bootstrap/ng-bootstrap";

@Component({
    selector: 'app-evaluacion',
    templateUrl: './evaluacion.component.html',
    styleUrls: ['./evaluacion.component.scss']
})
export class EvaluacionComponent implements OnInit {

    public contentHeader: object;
    public submitted = false;
    
    listaMatriculaExtraordinario:MatriculaExtaordinaria[];
    listaMallaAsignatura:MallaAsignaturaExtra[];
    listaNotaAsigExtra:NotaAsignaturaExtra[];
    listaCalificaciones:Calificacion[];
    listaCalificacionesComportamiento:Calificacion[];
    listaModeloEvaluacionGeneral:ModeloEvaluacionGeneral[];
    listaParmCalificacionCualitativo:ParmCalificacionCualitativo[];
    listaParmCalificacionCuantitativo:ParmCalificacionCuantitativo[];
    listaNotaAlfabetizacion:NotaAlfabetizacion[];
    listaAlfabetizacion:Alfabetizacion[];
    listaNotaComportamiento:NotaComportamiento[];
    listaComportamiento:Comportamiento[];
    
    tipoNiveModulo:string;
    cursoExtraordinaria:CursoExtraordinaria;
    mallaAsignatura:MallaAsignaturaExtra;
    tipoValoracion:TipoValoracion;
    modeloEvaluacion:ModeloEvaluacion;
    rangoNota:RangoNota;
    servicioEducativo:ServicioEducativo;
    modulo:Modulo;
    docente:Docente;
    letrasExpReg:string;
    comportamiento:Comportamiento;
    gradoEquivalencia:GradoEquivalencia;

    columnasEvaluacion=[];
    columnasComportamiento=[];
    dataSource: MatTableDataSource<NotaAsignaturaExtra>;
    dataSourceAlfaPost: MatTableDataSource<NotaAlfabetizacion>;
    dataSourceComportamieto: MatTableDataSource<NotaComportamiento>;

    @ViewChild(MatPaginator) paginator: MatPaginator;
    @ViewChild(MatSort) sort: MatSort;
    constructor(private readonly router: Router,
        public spinner: NgxSpinnerService,
        private _evaluacionService: EvaluacionService,
        private readonly programaStore: ProgramaStore,
        private modalService: NgbModal,
        ){}

    ngOnInit(): void {
        this.contentHeader = {
            headerTitle: 'Calificación Estudiantes',
            actionButton: false,
            breadcrumb: {
                type: '',
                links: [
                    {
                      name: 'Inicio',
                      isLink: true,
                      link: '/pages/inicio'
                    },
                    {
                      name: 'Calificación Estudiantes',
                      isLink: false
                    },
                ]
            }
        };  
        this.obtenerCursoExtraordinario();
    }

    obtenerCursoExtraordinario() {
        this.cursoExtraordinaria = this.programaStore.cursoExtraordinaria;
        if (this.cursoExtraordinaria == null || this.cursoExtraordinaria == undefined) {
            this.router.navigate(['pages/evaluacion-periodo-lectivo']);
            return;
        }
        this.docente=this.programaStore.docente;
        this.spinner.show();
        this.obternerServicioEducativo();
        this.obtenerParmCalificacionCualitativoPorReanleCodigo();
        this.obtenerParmCalificacionCuantitativoPorReanleCodigo();
        this.obtenerRangoNotaPorReanleCodigo();
        this.spinner.hide();
    }

    obternerServicioEducativo() {
        this._evaluacionService.obtenerServicioEducativoPorCodigo(this.cursoExtraordinaria.sereduCodigo).subscribe(
            (respuesta:RespuestaServicioEducativoInterfaz)=>{
                this.servicioEducativo = respuesta.objeto;
                this.obtenerTodasMatriculaEstudiantePorCurextCodigo();
            },
            (error:any)=>{
                this.spinner.hide();    
                Swal.fire('Ups! ocurrió un error con el servicio educativo','','error'); 
            }
        );
    }

    calificacionPorModulo() {
        this._evaluacionService.obtenerModuloPorCodigo(this.cursoExtraordinaria.modCodigo).subscribe(
            (respuesta:RespuestaModuloInterfaz) => {
                this.modulo=respuesta.objeto;
                if (this.modulo.modCodigoGradoInicio == 4 && this.modulo.modCodigoGradoFin == 5) {
                    this.tipoNiveModulo = 'ALFABETIZACION';
                    this.listarNotasModuloAlfabetizacionYPost();
                } else if ((this.modulo.modCodigoGradoInicio == 6 && this.modulo.modCodigoGradoFin == 7) || (this.modulo.modCodigoGradoInicio == 8 && this.modulo.modCodigoGradoFin == 9)) {
                    this.tipoNiveModulo = 'POST-ALFABETIZACION';
                    this.listarNotasModuloAlfabetizacionYPost();
                } else {
                    this.obtenerTodasMallaAsignaturasExtraCursoExtraordinariaYGrado();
                    this.tipoNiveModulo = 'BASICA-SUPERIOR-BACHILLERATO';
                }
            },
            (error:any)=>{
                this.spinner.hide();    
                Swal.fire('Ups! ocurrió un error con el modulo','','error'); 
            }
        );
    }

    obtenerGradoEquivalencia() {
        this._evaluacionService.obtenerGradoEquivaleciaPorServicioEducativa(this.cursoExtraordinaria.sereduCodigo).subscribe(
            (respuesta:RespuestaGradoEquivalenciaInterfaz) => {
                this.gradoEquivalencia = respuesta.objeto;
                if (this.gradoEquivalencia.graequGradoInicio == 4 && this.gradoEquivalencia.graequGradoFin == 5) {
                    this.tipoNiveModulo = 'ALFABETIZACION';
                    this.listarNotasModuloAlfabetizacionYPost();
                } else if ((this.gradoEquivalencia.graequGradoInicio == 6 && this.gradoEquivalencia.graequGradoFin == 7) || (this.gradoEquivalencia.graequGradoInicio == 8 && this.gradoEquivalencia.graequGradoFin == 9)) {
                    this.tipoNiveModulo = 'POST-ALFABETIZACION';
                    this.listarNotasModuloAlfabetizacionYPost();
                } else {
                    this.obtenerTodasMallaAsignaturasExtraCursoExtraordinariaYGrado();
                    this.tipoNiveModulo = 'BASICA-SUPERIOR-BACHILLERATO';
                }
            },
            (error:any)=>{
                this.spinner.hide();    
                Swal.fire('Ups! ocurrió un error con le Grado Equivalencia','','error'); 
            }
        );
    }

    obtenerTodasMallaAsignaturasExtraCursoExtraordinariaYGrado() {
        let mallaAsigExtraParametro:MallaAsigExtraParametro={
            docCodigo:this.docente.codPersona,
            curextCodigo:this.cursoExtraordinaria.curextCodigo,
            cupaexCodigo:this.cursoExtraordinaria.cupaexCodigo,
            grado:0
        }
        this._evaluacionService.obtenerTodasMallaAsignaturaExtraPorDocenteCurextCodigoYParalelo(mallaAsigExtraParametro).subscribe(
            (respuesta:RespuestaMallaAsignaturaExtraInterfaz)=>{
                this.listaMallaAsignatura = respuesta.listado;
            },
            (error:any)=>{
                this.spinner.hide();    
                Swal.fire('Ups! ocurrió un error con la malla asignatura','','error'); 
            }
        );
    }

    obtenerTodasMatriculaEstudiantePorCurextCodigo() {
        let matriculaExtraParametros:MatriculaExtraParametros = {
            curextCodigo:this.cursoExtraordinaria.curextCodigo,
            cupaexCodigo:this.cursoExtraordinaria.cupaexCodigo
        };
        this._evaluacionService.obtenerTodosEstudianteMatriculados(matriculaExtraParametros).subscribe(
            (respuesta:RespuestaMatriculaExtraordinariaInterfaz)=>{
                this.listaMatriculaExtraordinario = respuesta.listado;
                if (this.listaMatriculaExtraordinario.length == 0) {
                    Swal.fire('No existe estudiantes matricuados en este curso paralelo','','error');
                    this.volverPeriodoLectivo();
                    return;
                }
                if (this.cursoExtraordinaria.modCodigo != 0) {
                    this.calificacionPorModulo();
                } else if (this.cursoExtraordinaria.graCodigo != 0) {
                    if (this.cursoExtraordinaria.graCodigo == 10 || this.cursoExtraordinaria.graCodigo == 11 || this.cursoExtraordinaria.graCodigo == 12 || 
                        this.cursoExtraordinaria.graCodigo == 13 || this.cursoExtraordinaria.graCodigo == 14 || this.cursoExtraordinaria.graCodigo == 15 || 
                        this.cursoExtraordinaria.graCodigo == 16 || this.cursoExtraordinaria.graCodigo == 17 || this.cursoExtraordinaria.graCodigo == 18) {
                        this.obtenerTodasMallaAsignaturasExtraCursoExtraordinariaYGrado();
                        this.tipoNiveModulo = 'BASICA-SUPERIOR-BACHILLERATO';
                    } else {
                        this.listarNotasModuloAlfabetizacionYPost();
                        this.tipoNiveModulo = 'POST-ALFABETIZACION';
                    }
                    
                } if (this.cursoExtraordinaria.moduloCodigo == 0 && this.cursoExtraordinaria.graCodigo != 0) {
                    this.obtenerGradoEquivalencia();
                }
                this.obtenerListaComportamientoPorEstado();
            },
            (error:any)=>{
                this.spinner.hide();    
                Swal.fire('Ups! ocurrió un error con la malla asignatura','','error'); 
            }
        );
    }

    obtenerParmCalificacionCualitativoPorReanleCodigo() {
        this._evaluacionService.obtenerParmCalificacionCualitativoPorReanleCodigo(this.cursoExtraordinaria.reanleCodigo).subscribe(
            (respuesta:RespuestaParmCalificacionCualitativoInterfaz)=>{
                this.listaParmCalificacionCualitativo = respuesta.listado;
                let parmCalifCualiValores:ParmCalificacionCualitativo[] = this.listaParmCalificacionCualitativo.filter((parmCalifCualiValores)=>parmCalifCualiValores.parcualValor!=null);
                this.letrasExpReg = parmCalifCualiValores.map((parmCalifCuali)=>parmCalifCuali.parcualLetra).join(',');
                this.letrasExpReg = '{'+this.letrasExpReg+'}';
            },
            (error:any)=>{
                this.spinner.hide();    
                Swal.fire('Ups! ocurrió un error con los parametros de calificación cualitativo','','error'); 
            }
        );
    }

    obtenerParmCalificacionCuantitativoPorReanleCodigo() {
        
        this._evaluacionService.obtenerParmCalificacionCuantitativoPorReanleCodigo(this.cursoExtraordinaria.reanleCodigo).subscribe(
            (respuesta:RespuestaParmCalificacionCuantitativoInterfaz)=>{
                this.listaParmCalificacionCuantitativo = respuesta.listado;
            },
            (error:any)=>{
                this.spinner.hide();    
                Swal.fire('Ups! ocurrió un error con los parametros de calificación cuantitativo','','error'); 
            }
        );
    }

    obtenerRangoNotaPorReanleCodigo() {
        this._evaluacionService.obtenerRangoNotaPorReanleCodigo(this.cursoExtraordinaria.reanleCodigo).subscribe(
            (respuesta:RespuestaRangoNotaInterfaz)=>{
                this.rangoNota = respuesta.listado[0];
            },
            (error:any)=>{
                this.spinner.hide();    
                Swal.fire('Ups! ocurrió un error con el rango de notas','','error'); 
            }
        );
    }

    listarNotasModuloAlfabetizacionYPost() {
        this.listaNotaAlfabetizacion =  null;
        this.listaAlfabetizacion = null;
        this.listaModeloEvaluacionGeneral = null;
        let matCodigo =  this.listaMatriculaExtraordinario.map((matricula) => matricula.matCodigo);
        this.columnasEvaluacion = ['num', 'identificacion', 'apellido-nombre'];
        let notaAsigExtraParametros:NotaAsigExtraParametros = {
            listaMatCodigo:matCodigo,
            docCodigo:this.docente.codPersona,
            curextCodigo:this.cursoExtraordinaria.curextCodigo,
            cupaexCodigo:this.cursoExtraordinaria.cupaexCodigo,
            modCodigo:this.cursoExtraordinaria.modCodigo,
            reanleCodigo:this.cursoExtraordinaria.reanleCodigo,
            sereduCodigo:this.cursoExtraordinaria.sereduCodigo,
        };
        this._evaluacionService.obtenerListaNotasAlfabetizacionPorModulo(notaAsigExtraParametros).subscribe(
            (respuesta:RespuestaNotaAlfabetizacionInterfaz) => {
                this.listaNotaAlfabetizacion = respuesta.listado;
                this.listaCalificaciones = this.listaNotaAlfabetizacion[0].calificaciones;
                this.listaAlfabetizacion = this.listaNotaAlfabetizacion[0].alfabetizaciones;
                this.listaModeloEvaluacionGeneral = this.listaNotaAlfabetizacion[0].listaModevaGen;
                for (let i=0; i<this.listaNotaAlfabetizacion.length;i++) {
                    let matriculaExta:MatriculaExtaordinaria=this.listaMatriculaExtraordinario.find((matriculaExta)=>matriculaExta.matCodigo==this.listaNotaAlfabetizacion[i].matCodigo);
                    this.listaNotaAlfabetizacion[i].estIdentificacion=matriculaExta.estIdentificacion;
                    this.listaNotaAlfabetizacion[i].apellidoEstudiante=matriculaExta.apellidoEstudiante;
                    this.listaNotaAlfabetizacion[i].nombreEstudiante=matriculaExta.nombreEstudiante;
                }
                for (let i=0;i<this.listaCalificaciones.length;i++) {
                    this.columnasEvaluacion.push(this.listaCalificaciones[i].calNemonico);
                }
                this.listaNotaAlfabetizacion.sort(function(fObjec, sObjec) {
                    if (fObjec.apellidoEstudiante != null && sObjec.apellidoEstudiante != null) {
                        if (fObjec.apellidoEstudiante < sObjec.apellidoEstudiante) {
                            return -1;
                        }
                        if (fObjec.apellidoEstudiante > sObjec.apellidoEstudiante) {
                            return 1;
                        }
                    } else {
                        if (fObjec.nombreEstudiante < sObjec.nombreEstudiante) {
                            return -1;
                        }
                        if (fObjec.nombreEstudiante > sObjec.nombreEstudiante) {
                            return 1;
                        }
                    }
                    return 0;
                });
                this.columnasEvaluacion.push('acciones');
                this.dataSourceAlfaPost = new MatTableDataSource(this.listaNotaAlfabetizacion);
                this.dataSourceAlfaPost.paginator = this.paginator;
                this.dataSourceAlfaPost.paginator._intl.itemsPerPageLabel="Asignaturas por página";
                this.dataSourceAlfaPost.paginator._intl.nextPageLabel="Siguiente";
                this.dataSourceAlfaPost.paginator._intl.previousPageLabel="Anterior";
                this.dataSourceAlfaPost.sort = this.sort;
                this.spinner.hide();
            },
            (error:any)=>{
                this.spinner.hide();    
                Swal.fire('Ups! ocurrió un error con las Notas Alfabetización','','error'); 
            }
        );
    }

    obtenerTodosEstudiantesMatriculados(maasiexCodigo:any) {
        this.spinner.show();
        let matCodigo =  this.listaMatriculaExtraordinario.map((matricula) => matricula.matCodigo);
        this.listaNotaAsigExtra = null;
        this.modeloEvaluacion = null;
        this.listaCalificaciones = null;
        this.tipoValoracion = null;
        this.listaModeloEvaluacionGeneral = null;
        this.columnasEvaluacion = ['num', 'identificacion', 'apellido-nombre'];
        let notaAsigExtraParametros:NotaAsigExtraParametros = {
            listaMatCodigo:matCodigo,
            reanleCodigo:this.cursoExtraordinaria.reanleCodigo,
            sereduCodigo:this.cursoExtraordinaria.sereduCodigo,
            maasiexCodigo:maasiexCodigo
        };
       
        this._evaluacionService.obtenerNotasAsignaturasExtrPorListaMatriculaYMallaAsignatura(notaAsigExtraParametros).subscribe(
            (respuesta:RespuestaNotaAsignaturaExtraInterfaz)=>{
                this.listaNotaAsigExtra = respuesta.listado;
                this.modeloEvaluacion = this.listaNotaAsigExtra[0].modeloEvaluacion;
                this.listaCalificaciones = this.listaNotaAsigExtra[0].calificaciones;
                this.listaModeloEvaluacionGeneral = this.listaNotaAsigExtra[0].listaModevaGen;
                this.obtenerTipoValoracioPorAsignatura();    
            },
            (error:any)=>{
                this.spinner.hide();    
                Swal.fire('Ups! ocurrió un error al obtener las notas de la asignatura','','error'); 
            }
        );
    }

    obtenerTipoValoracioPorAsignatura() {
        let tivaCodigo = this.mallaAsignatura.tivaCodigo;
        this._evaluacionService.obtenerTipoValoracionPorAsignatura(tivaCodigo).subscribe(
            (respuesta:RespuestaTipoValoracionInterfaz)=>{
                this.tipoValoracion = respuesta.objeto;
                for (let i = 0; i<this.listaNotaAsigExtra.length;i++) {
                    let matriculaExta:MatriculaExtaordinaria=this.listaMatriculaExtraordinario.find((matriculaExta)=>matriculaExta.matCodigo==this.listaNotaAsigExtra[i].matCodigo);
                    this.listaNotaAsigExtra[i].estIdentificacion=matriculaExta.estIdentificacion;
                    this.listaNotaAsigExtra[i].apellidoEstudiante=matriculaExta.apellidoEstudiante;
                    this.listaNotaAsigExtra[i].nombreEstudiante=matriculaExta.nombreEstudiante;
                    let notaCalificacion:Calificacion = this.listaNotaAsigExtra[i].calificaciones.find((notaCalificacion) => notaCalificacion.calNemonico == 'PANUALORD');
                    if (notaCalificacion != null && notaCalificacion != undefined) {
                        this.listaNotaAsigExtra[i].promedioAnual = notaCalificacion.notaValorCunt;
                    }
                }
                for (let i=0;i<this.listaCalificaciones.length;i++) {
                    this.columnasEvaluacion.push(this.listaCalificaciones[i].calNemonico);
                }
                this.listaNotaAsigExtra.sort(function(fObjec, sObjec) {
                    if (fObjec.apellidoEstudiante != null && sObjec.apellidoEstudiante != null) {
                        if (fObjec.apellidoEstudiante < sObjec.apellidoEstudiante) {
                            return -1;
                        }
                        if (fObjec.apellidoEstudiante > sObjec.apellidoEstudiante) {
                            return 1;
                        }
                    } else {
                        if (fObjec.nombreEstudiante < sObjec.nombreEstudiante) {
                            return -1;
                        }
                        if (fObjec.nombreEstudiante > sObjec.nombreEstudiante) {
                            return 1;
                        }
                    }
                    return 0;
                });
                this.columnasEvaluacion.push('acciones');
                this.dataSource = new MatTableDataSource(this.listaNotaAsigExtra);
                this.dataSource.paginator = this.paginator;
                this.dataSource.paginator._intl.itemsPerPageLabel="Asignaturas por página";
                this.dataSource.paginator._intl.nextPageLabel="Siguiente";
                this.dataSource.paginator._intl.previousPageLabel="Anterior";
                this.dataSource.sort = this.sort;
                this.spinner.hide();
            },
            (error:any)=>{
                this.spinner.hide();    
                Swal.fire('Ups! ocurrió un error al obtener las notas de la asignatura','','error'); 
            }
        );
    }

    obtenerListaComportamientoPorEstado() {
        this.listaNotaComportamiento = null;
        this._evaluacionService.obtenerListaComportamientoPorEstado().subscribe(
            (respuesta:RespuestaComportamientoInterfaz) => {
                this.listaComportamiento = respuesta.listado;
                this.obtenetListaNotasComportamiento();
            },
            (error:any)=>{
                this.spinner.hide();    
                Swal.fire('Ups! ocurrió un error al obtener los comportamientos','','error'); 
            }
        );
    }

    obtenetListaNotasComportamiento() {
        this.columnasComportamiento = ['num', 'identificacion', 'apellido-nombre'];
        this.listaNotaComportamiento = null;
        let matCodigo =  this.listaMatriculaExtraordinario.map((matricula) => matricula.matCodigo);
        let notaAsigExtraParametros:NotaAsigExtraParametros = {
            listaMatCodigo:matCodigo,
            reanleCodigo:this.cursoExtraordinaria.reanleCodigo,
            sereduCodigo: this.cursoExtraordinaria.sereduCodigo
        };
        this._evaluacionService.obtenerNotasComportamientoPorListaMatriculaYMallaAsignatura(notaAsigExtraParametros).subscribe(
            (respuesta:RespuestaNotaComportamientoInterfaz) => {
                this.listaNotaComportamiento = respuesta.listado;
                for (let i=0; i<this.listaNotaComportamiento.length;i++) {
                    let calificaciones:Calificacion[] = this.listaNotaComportamiento[i].calificaciones.filter((calificaciones) => calificaciones.calNemonico.substring(0,3) == 'PAR' || calificaciones.calNemonico.substring(0,3) == 'PRP');
                    this.listaNotaComportamiento[i].calificaciones = calificaciones;
                    let matriculaExta:MatriculaExtaordinaria=this.listaMatriculaExtraordinario.find((matriculaExta)=>matriculaExta.matCodigo==this.listaNotaComportamiento[i].matCodigo);
                    this.listaNotaComportamiento[i].estIdentificacion=matriculaExta.estIdentificacion;
                    this.listaNotaComportamiento[i].apellidoEstudiante=matriculaExta.apellidoEstudiante;
                    this.listaNotaComportamiento[i].nombreEstudiante=matriculaExta.nombreEstudiante;
                }
                this.listaCalificacionesComportamiento = this.listaNotaComportamiento[0].calificaciones;
                for (let i=0;i<this.listaCalificacionesComportamiento.length;i++) {
                    this.columnasComportamiento.push(this.listaCalificacionesComportamiento[i].calNemonico);
                }
                this.listaNotaComportamiento.sort(function(fObjec, sObjec) {
                    if (fObjec.apellidoEstudiante != null && sObjec.apellidoEstudiante != null) {
                        if (fObjec.apellidoEstudiante < sObjec.apellidoEstudiante) {
                            return -1;
                        }
                        if (fObjec.apellidoEstudiante > sObjec.apellidoEstudiante) {
                            return 1;
                        }
                    } else {
                        if (fObjec.nombreEstudiante < sObjec.nombreEstudiante) {
                            return -1;
                        }
                        if (fObjec.nombreEstudiante > sObjec.nombreEstudiante) {
                            return 1;
                        }
                    }
                    return 0;
                });
                
                if (this.listaNotaComportamiento[0].comCodigo != null) {
                    this.comportamiento = this.listaComportamiento.find((compor)=>compor.comCodigo==this.listaNotaComportamiento[0].comCodigo);
                }
                this.columnasComportamiento.push('acciones');
                this.dataSourceComportamieto = new MatTableDataSource(this.listaNotaComportamiento);
                this.dataSourceComportamieto.paginator = this.paginator;
                this.dataSourceComportamieto.paginator._intl.itemsPerPageLabel="Asignaturas por página";
                this.dataSourceComportamieto.paginator._intl.nextPageLabel="Siguiente";
                this.dataSourceComportamieto.paginator._intl.previousPageLabel="Anterior";
                this.dataSourceComportamieto.sort = this.sort;
            },
            (error:any)=>{
                this.spinner.hide();    
                Swal.fire('Ups! ocurrió un error al obtener las notas del comportamiento','','error'); 
            }
        );
    }

    calcularPromediosPorCuantitativoAlfaYPos(notaAlfabetizacion:NotaAlfabetizacion) {
        let totalPromedios = 0;
        let listaModeloEvaluacionGeneralLimpio:ModeloEvaluacionGeneral[] = Array.from(new Set(this.listaModeloEvaluacionGeneral));
        let promedios = listaModeloEvaluacionGeneralLimpio.length;
        for (let i =0; i<listaModeloEvaluacionGeneralLimpio.length; i++) {
            let modeloGeneral = listaModeloEvaluacionGeneralLimpio[i];
            let calificacionesParcial:Calificacion[] = notaAlfabetizacion.calificaciones.filter((calificacion)=> calificacion.calNemonico.substring(0,3) == 'PAR' && calificacion.modgenCodigo == modeloGeneral.modgenCodigo);
            let sumaParciales:number = 0;
            let numerElementoPar = calificacionesParcial.length;
            for (let j=0;j<calificacionesParcial.length; j++) {
                if (calificacionesParcial[j].notaValorCunt < this.rangoNota.ranotMinimo || calificacionesParcial[j].notaValorCunt > this.rangoNota.ranotMaximo) {
                    Swal.fire(`El rango de las notas es de ${this.rangoNota.ranotMinimo} a ${this.rangoNota.ranotMaximo}`,'','warning');
                    calificacionesParcial[j].notaValorCunt = null;
                    return;
                }
                sumaParciales=sumaParciales + calificacionesParcial[j].notaValorCunt;
            }
            let proParcia = sumaParciales / numerElementoPar;
            let calificacionesProParciales:Calificacion = notaAlfabetizacion.calificaciones.find((calificacion)=> calificacion.calNemonico.substring(0,3) == 'PRP' && calificacion.modgenCodigo == modeloGeneral.modgenCodigo);
            if (calificacionesProParciales != null && calificacionesProParciales != undefined) {
                
                calificacionesProParciales.notaValorCunt = proParcia;
            }
            let calificacionesProAnual:Calificacion = notaAlfabetizacion.calificaciones.find((calificacion)=> calificacion.calNemonico == 'PANUALORD');
            totalPromedios = totalPromedios + proParcia;
            let promedioAnual = totalPromedios / promedios;
            notaAlfabetizacion.promedioAnual = promedioAnual;
            calificacionesProAnual.notaValorCunt = this.redondear(promedioAnual, 2);
        }
    }

    calcularPromediosPorCuantitativoBasicaSuperiorBachillerato(notaAsiExtra:NotaAsignaturaExtra) {
        let totalPromedios = 0;
        let listaModeloEvaluacionGeneralLimpio:ModeloEvaluacionGeneral[] = Array.from(new Set(this.listaModeloEvaluacionGeneral));
        let promedios = listaModeloEvaluacionGeneralLimpio.length;
        for (let i =0; i<listaModeloEvaluacionGeneralLimpio.length; i++) {
            let modeloGeneral = listaModeloEvaluacionGeneralLimpio[i];
            let calificacionesParcial:Calificacion[] = notaAsiExtra.calificaciones.filter((calificacion)=> calificacion.calNemonico.substring(0,3) == 'PAR' && calificacion.modgenCodigo == modeloGeneral.modgenCodigo);
            let sumaParciales:number = 0;
            let numerElementoPar = calificacionesParcial.length;
            for (let j=0;j<calificacionesParcial.length; j++) {
                
                if (calificacionesParcial[j].notaValorCunt < calificacionesParcial[j].calMinimo || calificacionesParcial[j].notaValorCunt > calificacionesParcial[j].calMaximo ) {
                    Swal.fire(`El rango de las notas es de ${calificacionesParcial[j].calMinimo} a ${calificacionesParcial[j].calMaximo}`,'','warning');
                    calificacionesParcial[j].notaValorCunt = null;
                    return;
                }
                sumaParciales=sumaParciales + calificacionesParcial[j].notaValorCunt;
            }
            let proParcia = sumaParciales / numerElementoPar;
            let calificacionesProParciales:Calificacion = notaAsiExtra.calificaciones.find((calificacion)=> calificacion.calNemonico.substring(0,3) == 'PRP' && calificacion.modgenCodigo == modeloGeneral.modgenCodigo);
            if (calificacionesProParciales != null && calificacionesProParciales != undefined) {
                calificacionesProParciales.notaValorCunt = this.redondear(proParcia,2);
            }
            let calificacionesPorProPar:Calificacion = notaAsiExtra.calificaciones.find((calificacion)=> calificacion.calNemonico.substring(0,3) == 'POP' && calificacion.modgenCodigo == modeloGeneral.modgenCodigo);
            if (calificacionesPorProPar != null && calificacionesPorProPar != undefined) {
                let porcentajeParcial = (proParcia*(calificacionesPorProPar.calPorcentaje/100)*this.rangoNota.ranotMaximo);
                calificacionesPorProPar.notaValorCunt = this.redondear(porcentajeParcial, 2);
            }
            let calificacionesExa:Calificacion = notaAsiExtra.calificaciones.find((calificacion)=> calificacion.calNemonico.substring(0,3) == 'EXA' && calificacion.modgenCodigo == modeloGeneral.modgenCodigo);
            let nemCal = 'POREXA';
            let calificacionesPorExa:Calificacion = notaAsiExtra.calificaciones.find((calificacion)=> calificacion.calNemonico.substring(0,6) == nemCal && calificacion.modgenCodigo == modeloGeneral.modgenCodigo);
            if (calificacionesExa != null && calificacionesExa != undefined) {
                if (calificacionesExa.notaValorCunt < this.rangoNota.ranotMinimo || calificacionesExa.notaValorCunt > this.rangoNota.ranotMaximo) {
                    Swal.fire(`El rango de las notas es de ${this.rangoNota.ranotMinimo} a ${this.rangoNota.ranotMaximo}`,'','warning');
                    calificacionesExa.notaValorCunt = null;
                    return;
                }
                let porcentajeExamen = (calificacionesExa.notaValorCunt*(calificacionesPorExa.calPorcentaje/100)*this.rangoNota.ranotMaximo);
                calificacionesPorExa.notaValorCunt = Number(porcentajeExamen.toFixed(2));
            }
            let promedioSut = 0;
            if (calificacionesProParciales != null && calificacionesProParciales != undefined) {
                promedioSut = (calificacionesProParciales.notaValorCunt + calificacionesExa.notaValorCunt)/2;
                let calificacionesProSub:Calificacion = notaAsiExtra.calificaciones.find((calificacion)=> calificacion.calNemonico.substring(0,3) == 'PRO' && calificacion.modgenCodigo == modeloGeneral.modgenCodigo);
                if (calificacionesProSub == undefined && calificacionesProSub == null) {
                    Swal.fire(`Crear promedio del parcial es obligatorio`,'','warning');
                }
                calificacionesProSub.notaValorCunt = this.redondear(promedioSut,2);
            }
            let calificacionesEscalaCual:Calificacion = notaAsiExtra.calificaciones.find((calificacion)=> calificacion.calNemonico.substring(0,2) == 'EC' && calificacion.modgenCodigo == modeloGeneral.modgenCodigo);
            if (calificacionesEscalaCual != null && calificacionesEscalaCual != undefined) {
                let parmCalifCuantitativo = this.listaParmCalificacionCuantitativo.find((parmCalifCuantitativo) => promedioSut >= parmCalifCuantitativo.parcaMinimo && promedioSut <= parmCalifCuantitativo.parcaMaximo);
                calificacionesEscalaCual.notaValorCual = parmCalifCuantitativo.parcaNemonico;
            }
            
            let calificacionesProAnual:Calificacion = notaAsiExtra.calificaciones.find((calificacion)=> calificacion.calNemonico == 'PANUALORD');
            if (calificacionesProAnual == undefined && calificacionesProAnual == null) {
                Swal.fire(`Crear promedio anual es obligatorio`,'','warning');
            }
            totalPromedios = totalPromedios + promedioSut;
            let promedioAnual = totalPromedios / promedios;
            notaAsiExtra.promedioAnual = promedioAnual;
            calificacionesProAnual.notaValorCunt = this.redondear(promedioAnual,2);
            let calificacionesSupletorio:Calificacion = notaAsiExtra.calificaciones.find((calificacion)=> calificacion.calNemonico == 'SUP');
            if (calificacionesSupletorio != null && calificacionesSupletorio != undefined) {
                if (calificacionesExa.notaValorCunt < this.rangoNota.ranotMinimo || calificacionesExa.notaValorCunt > this.rangoNota.ranotMaximo) {
                    Swal.fire(`El rango de las notas es de ${this.rangoNota.ranotMinimo} a ${this.rangoNota.ranotMaximo}`,'','warning');
                    calificacionesExa.notaValorCunt = null;
                    return;
                }
                if (calificacionesSupletorio.notaValorCunt >= 7) {
                    calificacionesProAnual.notaValorCunt = 7
                }
            }
            let calificacionesRemedial:Calificacion = notaAsiExtra.calificaciones.find((calificacion)=> calificacion.calNemonico == 'REM');
            if (calificacionesRemedial != null && calificacionesRemedial != undefined) {
                if (calificacionesExa.notaValorCunt < this.rangoNota.ranotMinimo || calificacionesExa.notaValorCunt > this.rangoNota.ranotMaximo) {
                    Swal.fire(`El rango de las notas es de ${this.rangoNota.ranotMinimo} a ${this.rangoNota.ranotMaximo}`,'','warning');
                    calificacionesExa.notaValorCunt = null;
                    return;
                }
                if (calificacionesRemedial.notaValorCunt >= 7) {
                    calificacionesProAnual.notaValorCunt = 7
                }
            }
            let calificacionesGracia:Calificacion = notaAsiExtra.calificaciones.find((calificacion)=> calificacion.calNemonico == 'GRA');
            if (calificacionesGracia != null && calificacionesGracia != undefined) {
                if (calificacionesExa.notaValorCunt < this.rangoNota.ranotMinimo || calificacionesExa.notaValorCunt > this.rangoNota.ranotMaximo) {
                    Swal.fire(`El rango de las notas es de ${this.rangoNota.ranotMinimo} a ${this.rangoNota.ranotMaximo}`,'','warning');
                    calificacionesExa.notaValorCunt = null;
                    return;
                }
                if (calificacionesGracia.notaValorCunt >= 7) {
                    calificacionesProAnual.notaValorCunt = 7
                }
            }
            let calificacionesEscalaCualAnual:Calificacion = notaAsiExtra.calificaciones.find((calificacion)=> calificacion.calNemonico.substring(0,3) == 'ECA');
            if (calificacionesEscalaCualAnual != null && calificacionesEscalaCualAnual != undefined) {
                let parmCalifCuantitativo = this.listaParmCalificacionCuantitativo.find((parmCalifCuantitativo) => calificacionesProAnual.notaValorCunt >= parmCalifCuantitativo.parcaMinimo && calificacionesProAnual.notaValorCunt <= parmCalifCuantitativo.parcaMaximo);
                calificacionesEscalaCualAnual.notaValorCual = parmCalifCuantitativo.parcaNemonico;
            }
        }
    }

    guardarIndividual(notaAsiExtra:NotaAsignaturaExtra){
        this.spinner.show();
        this._evaluacionService.guardarIndividualNotaAsigExtra(notaAsiExtra).subscribe(
            (respuesta:any)=>{
                this.spinner.hide();
                Swal.fire(`Notas de la asignatura grabada!`,'','success');
                this.obtenerTodosEstudiantesMatriculados(notaAsiExtra.maasiexCodigo);
            },
            (error:any)=>{
                this.spinner.hide();    
                Swal.fire('Ups! no se pudo grabar la notas de la asignatura','','error');
                this.obtenerTodosEstudiantesMatriculados(notaAsiExtra.maasiexCodigo);
            }
        );
    }

    guardarLista() {
        this.spinner.show();
        this._evaluacionService.guardarListaNotaAsigExtra(this.listaNotaAsigExtra).subscribe(
            (respuesta:any)=>{
                this.spinner.hide();
                Swal.fire(`Notas de las asignaturas grabada!`,'','success');
                this.obtenerTodosEstudiantesMatriculados(this.listaNotaAsigExtra[0].maasiexCodigo);
            },
            (error:any)=>{
                this.spinner.hide();    
                Swal.fire('Ups! no se pudo guardar todas las notas de la asignatura','','error');
                this.obtenerTodosEstudiantesMatriculados(this.listaNotaAsigExtra[0].maasiexCodigo);
            }
        );
    }

    guardarIndividualAlfabetizacion(notaAlfabetizacion:NotaAlfabetizacion) {
        this.spinner.show();
        this._evaluacionService.guardarIndividualNotaAlfbetizacion(notaAlfabetizacion).subscribe(
            (respuesta:any)=>{
                this.spinner.hide();
                Swal.fire(`Notas grabadas!`,'','success');
                this.listarNotasModuloAlfabetizacionYPost();
            },
            (error:any)=>{
                this.spinner.hide();    
                Swal.fire('Ups! no se pudo grabar la notas','','error');
                this.listarNotasModuloAlfabetizacionYPost();
            }
        );
    }

    guardarListaAlfabetizacion() {
        this.spinner.show();
        this._evaluacionService.guardarListaNotaAlfabetizacion(this.listaNotaAlfabetizacion).subscribe(
            (respuesta:any)=>{
                this.spinner.hide();
                Swal.fire(`Notas de los estudiantes fueron grabada!`,'','success');
                this.listarNotasModuloAlfabetizacionYPost();
            },
            (error:any)=>{
                this.spinner.hide();    
                Swal.fire('Ups! no se pudo guardar las notas de los estudiantes','','error');
                this.listarNotasModuloAlfabetizacionYPost();
            }
        );
    }

    guardarNotaComportamiento(notaComportamiento:NotaComportamiento) {
        if (this.comportamiento == null || this.comportamiento == undefined) {
            Swal.fire('Ups! Seleccione el tipo de comportamiento','','warning');
            return;
        }
        notaComportamiento.comCodigo = this.comportamiento.comCodigo;
        this.spinner.show();
        this._evaluacionService.guardarNotaComportamiento(notaComportamiento).subscribe(
            (respuesta:any)=>{
                this.spinner.hide();
                Swal.fire(`Notas del comportamiento de los estudiantes fueron grabada!`,'','success');
                this.obtenerListaComportamientoPorEstado();
            },
            (error:any)=>{
                this.spinner.hide();    
                Swal.fire('Ups! no se pudo guardar las notas del comportamiento de los estudiantes','','error');
                this.obtenerListaComportamientoPorEstado();
            }
        );
    }

    guardarListaNotaComportamiento() {
        if (this.comportamiento == null || this.comportamiento == undefined) {
            Swal.fire('Ups! Seleccione el tipo de comportamiento','','warning');
            return;
        }
        for(let i = 0; i<this.listaNotaComportamiento.length;i++) {
            this.listaNotaComportamiento[i].comCodigo = this.comportamiento.comCodigo;
        }
        this.spinner.show();
        this._evaluacionService.guardarListaNotaComportamiento(this.listaNotaComportamiento).subscribe(
            (respuesta:any)=>{
                this.spinner.hide();
                Swal.fire(`Notas del comportamiento de los estudiantes fueron grabada!`,'','success');
                this.obtenerListaComportamientoPorEstado();
            },
            (error:any)=>{
                this.spinner.hide();    
                Swal.fire('Ups! no se pudo guardar las notas del comportamiento de los estudiantes','','error');
                this.obtenerListaComportamientoPorEstado();
            }
        );
    }

    activarInactivar(notaAsiExtra:NotaAsignaturaExtra) {
        this.spinner.show();
        if (notaAsiExtra.nasiextEstado == 1) {
            this._evaluacionService.inactivarNotaAsigExtra(notaAsiExtra.nasiextCodigo).subscribe(
                (respuesta:any)=>{
                    this.spinner.hide();
                    Swal.fire(`Nota asignatura inactivada!`,'','success');
                    this.obtenerTodosEstudiantesMatriculados(notaAsiExtra.maasiexCodigo);
                },
                (error:any)=>{
                    this.spinner.hide();    
                    Swal.fire('Ups! no se pudo inactivar la nota de la asignatura','','error');
                    this.obtenerTodosEstudiantesMatriculados(notaAsiExtra.maasiexCodigo);
                }
            );
        } else {
            notaAsiExtra.nasiextEstado = 1;
            this._evaluacionService.guardarIndividualNotaAsigExtra(notaAsiExtra).subscribe(
                (respuesta:any)=>{
                    Swal.fire(`Notas asignaturas activada!`,'','success');
                    this.spinner.hide();
                    this.obtenerTodosEstudiantesMatriculados(notaAsiExtra.maasiexCodigo);
                },
                (error:any)=>{
                    this.spinner.hide();    
                    Swal.fire('Ups! no se pudo activar las notas de la asignatura','','error');
                    this.obtenerTodosEstudiantesMatriculados(notaAsiExtra.maasiexCodigo);
                }
            );
        }
    }

    activarInactivarAlfabetizacion(notaAlfabetizacion:NotaAlfabetizacion) {
        this.spinner.show();
        if (notaAlfabetizacion.nouniEstado == 1) {
            this._evaluacionService.inactivarNotaAlfabetizacion(notaAlfabetizacion.nouniCodigo).subscribe(
                (respuesta:any)=>{
                    Swal.fire(`Nota asignatura inactivada!`,'','success');
                    this.spinner.hide();
                    this.listarNotasModuloAlfabetizacionYPost();
                },
                (error:any)=>{
                    this.spinner.hide();    
                    Swal.fire('Ups! no se pudo inactivar la nota de la asignatura','','error'); 
                }
            );
        } else {
            notaAlfabetizacion.nouniEstado = 1;
            this._evaluacionService.guardarIndividualNotaAlfbetizacion(notaAlfabetizacion).subscribe(
                (respuesta:any)=>{
                    Swal.fire(`Notas Modulo inactivada!`,'','success');
                    this.spinner.hide();
                    this.listarNotasModuloAlfabetizacionYPost();
                },
                (error:any)=>{
                    this.spinner.hide();    
                    Swal.fire('Ups! no se pudo activar las notas de la asignatura','','error');
                    this.listarNotasModuloAlfabetizacionYPost();
                }
            );
        }
    }

    activarInactivarNotaComportamiento(notaComportamiento:NotaComportamiento) {
        this.spinner.show();
        if (notaComportamiento.nocoEstado == 1) {
            this._evaluacionService.inactivarNotaComportamiento(notaComportamiento.nocoCodigo).subscribe(
                (respuesta:any)=>{
                    Swal.fire(`Notas del comportamiento inactivada!`,'','success');
                    this.spinner.hide();
                    this.obtenerListaComportamientoPorEstado();
                },
                (error:any)=>{
                    this.spinner.hide();    
                    Swal.fire('Ups! no se pudo inactivar las notas del comportamiento','','error');
                    this.obtenerListaComportamientoPorEstado();
                }
            );
        } else {
            notaComportamiento.nocoEstado = 1;
            this._evaluacionService.guardarNotaComportamiento(notaComportamiento).subscribe(
                (respuesta:any)=>{
                    Swal.fire(`Notas comportamiento activada!`,'','success');
                    this.spinner.hide();
                    this.obtenerListaComportamientoPorEstado();
                },
                (error:any)=>{
                    this.spinner.hide();    
                    Swal.fire('Ups! no se pudo activar las notas del comportamiento','','error');
                    this.obtenerListaComportamientoPorEstado();
                }
            );
        }
    }

    notasComportamientoValida(calificacion:Calificacion) {
        let paramsCualitativo:ParmCalificacionCualitativo[] = this.listaParmCalificacionCualitativo.filter((paramsCualitativo)=>paramsCualitativo.parcualValor == 1);
        let califCualitativo:ParmCalificacionCualitativo =  paramsCualitativo.find((califCualitativo) => califCualitativo.parcualLetra == calificacion.notaValorCual);
        if (califCualitativo == null || califCualitativo == undefined) {
            let letasValidad:string[] = paramsCualitativo.map((letasValidad)  => letasValidad.parcualLetra);
            Swal.fire(`Ups! nota comportamiento no valida. Las notas validas son ${letasValidad}`,'','warning');
            calificacion.notaValorCual =  null;
            return;
        }
    }

    //validación solo letras
    validateFormat(event) {
        let key;
        if (event.type === 'paste') {
            key = event.clipboardData.getData('text/plain');
        } else {
            key = event.keyCode;
            key = String.fromCharCode(key);
        }
        const regex = /[A-Za-z]/;
        if (!regex.test(key)) {
            event.returnValue = false;
            if (event.preventDefault) {
                event.preventDefault();
            }
        }
    }

    redondear(num:number, dec:number){
        let exp = Math.pow(10, dec || 2);
        let numStr:string = (num*exp).toString();
        return parseInt(numStr, 10)/exp;
    } 

    volverPeriodoLectivo() {
        this.router.navigate(['pages/evaluacion-periodo-lectivo']);
    }

    applyFilter(event: Event) {
       const filterValue = (event.target as HTMLInputElement).value;
        this.dataSource.filter = filterValue.trim().toLowerCase();
      
        if (this.dataSource.paginator) {
          this.dataSource.paginator.firstPage();
        }
    }

    applyFilterAlfaPost(event: Event) {
        const filterValue = (event.target as HTMLInputElement).value;
        this.dataSourceAlfaPost.filter = filterValue.trim().toLowerCase();
        if (this.dataSourceAlfaPost.paginator) {
           this.dataSourceAlfaPost.paginator.firstPage();
        }
     }

     applyFilterComprtamiento(event: Event) {
        const filterValue = (event.target as HTMLInputElement).value;
        this.dataSourceComportamieto.filter = filterValue.trim().toLowerCase();
        if (this.dataSourceComportamieto.paginator) {
           this.dataSourceComportamieto.paginator.firstPage();
        }
     }
     modalSat(modalForm) {
        this.submitted=false;        
        this.modalService.open(modalForm, {size: 'lg'});
    }  
}