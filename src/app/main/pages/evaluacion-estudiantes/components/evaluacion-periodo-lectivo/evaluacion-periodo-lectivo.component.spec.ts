import { ComponentFixture, TestBed } from '@angular/core/testing';

import { EvaluacionPeriodoLectivoComponent } from './evaluacion-periodo-lectivo.component';

describe('EvaluacionPeriodoLectivoComponent', () => {
  let component: EvaluacionPeriodoLectivoComponent;
  let fixture: ComponentFixture<EvaluacionPeriodoLectivoComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ EvaluacionPeriodoLectivoComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(EvaluacionPeriodoLectivoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});