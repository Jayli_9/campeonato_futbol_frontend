import { Component, OnInit, ViewChild } from "@angular/core";
import { MatPaginator } from "@angular/material/paginator";
import { MatSort } from "@angular/material/sort";
import { MatTableDataSource } from "@angular/material/table";
import { Router } from "@angular/router";
import { NgbModal } from "@ng-bootstrap/ng-bootstrap";
import { NgxSpinnerService } from "ngx-spinner";
import Swal from "sweetalert2";
import { Calificacion } from "../../interfaces/calificacion";
import { CursoExtraordinaria } from "../../interfaces/curso-extraordinaria";
import { Docente } from "../../interfaces/docente";
import { ModeloEvaluacionGeneral } from "../../interfaces/modelo-evaluacion-general";
import { NotaMateriaModulo } from "../../interfaces/nota-materia-modulo";
import { ParmCalificacionCuantitativo } from "../../interfaces/param-calificacion-cuantitativo";
import { RespuestaNotasMateriaModuloInterfaz } from "../../interfaces/respuesta-nota-materia-modulo-interfaz";
import { RespuestaParmCalificacionCuantitativoInterfaz } from "../../interfaces/respuesta-param-calificacion-cuantitativo-interfaz";
import { RespuestaServicioEducativoInterfaz } from "../../interfaces/respuesta-servicio-educativo-interfaz";
import { ServicioEducativo } from "../../interfaces/servicio-educativo";
import { EvaluacionService } from "../../services/evaluacion.service";
import { ProgramaStore } from "../../services/programa-store";
import { NotaAsigExtraParametros } from "../../interfaces/nota-asig-extra-parametros";

@Component({
    selector: 'app-evaluacion',
    templateUrl: './evaluacion-materias.component.html',
    styleUrls: ['./evaluacion-materias.component.scss']
})
export class EvaluacionMateriasComponent implements OnInit {

    public contentHeader: object;
    public submitted = false;

    listaNotaMateriaModulo:NotaMateriaModulo[];
    listaParmCalificacionCuantitativo:ParmCalificacionCuantitativo[];
    listaCalificaciones:Calificacion[];
    listaModeloEvaluacionGeneral:ModeloEvaluacionGeneral[];

    cursoExtraordinaria:CursoExtraordinaria;
    docente:Docente;
    servicioEducativo:ServicioEducativo;

    columnasEvaluacion=['num', 'asignatura'];
    dataSource: MatTableDataSource<NotaMateriaModulo>;

    @ViewChild(MatPaginator) paginator: MatPaginator;
    @ViewChild(MatSort) sort: MatSort;
    constructor(private readonly router: Router,
        private modalService: NgbModal,
        public spinner: NgxSpinnerService,
        private _evaluacionService: EvaluacionService,
        private readonly programaStore: ProgramaStore
        ){
    }

    ngOnInit(): void {
        this.contentHeader = {
            headerTitle: 'Reporte de Calificaciones',
            actionButton: false,
            breadcrumb: {
                type: '',
                links: [
                    {
                      name: 'Inicio',
                      isLink: true,
                      link: '/pages/inicio'
                    },
                    {
                      name: 'Reporte de Calificaciones',
                      isLink: false
                    },
                ]
            }
        };
        this.obtenerCursoExtraordinario();
    }

    obtenerCursoExtraordinario() {
        this.cursoExtraordinaria = this.programaStore.cursoExtraordinaria;
        if (this.cursoExtraordinaria == null || this.cursoExtraordinaria == undefined) {
            this.router.navigate(['pages/evaluacion-periodo-lectivo']);
            return;
        }
        this.docente=this.programaStore.docente;
        this.obternerServicioEducativo();
    }

    obternerServicioEducativo() {
        this.spinner.show();
        this._evaluacionService.obtenerServicioEducativoPorCodigo(this.cursoExtraordinaria.sereduCodigo).subscribe(
            (respuesta:RespuestaServicioEducativoInterfaz)=>{
                this.servicioEducativo = respuesta.objeto;
                this.obtenerParmCalificacionCuantitativoPorReanleCodigo();
                this.spinner.hide(); 
            },
            (error:any)=>{
                this.spinner.hide();    
                Swal.fire('Ups! ocurrió un error con el servicio educativo','','error'); 
            }
        );
    }

    obtenerParmCalificacionCuantitativoPorReanleCodigo() {
        this._evaluacionService.obtenerParmCalificacionCuantitativoPorReanleCodigo(this.cursoExtraordinaria.reanleCodigo).subscribe(
            (respuesta:RespuestaParmCalificacionCuantitativoInterfaz)=>{
                this.listaParmCalificacionCuantitativo = respuesta.listado;
                this.obtenerListaNotasMateriasModulo();
            },
            (error:any)=>{
                this.spinner.hide();    
                Swal.fire('Ups! ocurrió un error con los parametros de calificación cuantitativo','','error'); 
            }
        );
    }
    
    obtenerListaNotasMateriasModulo() {
        let notaAsigExtraParametros:NotaAsigExtraParametros = {
            docCodigo:this.docente.codPersona,
            curextCodigo:this.cursoExtraordinaria.curextCodigo,
            cupaexCodigo:this.cursoExtraordinaria.cupaexCodigo,
            sereduCodigo:this.cursoExtraordinaria.sereduCodigo,
            reanleCodigo:this.cursoExtraordinaria.reanleCodigo
        };
        this._evaluacionService.obtenerListaNotasMateriasPorMallaDocente(notaAsigExtraParametros).subscribe(
            (respuesta:RespuestaNotasMateriaModuloInterfaz) => {
                this.listaNotaMateriaModulo = respuesta.listado;
                this.listaCalificaciones = this.listaNotaMateriaModulo[0].calificaciones;
                this.listaModeloEvaluacionGeneral = this.listaNotaMateriaModulo[0].listaModevaGen;

                if (this.listaCalificaciones == null || this.listaCalificaciones == undefined ) {
                    Swal.fire('No existe calificaciones registradas','','warning');
                    this.router.navigate(['pages/evaluacion-periodo-lectivo']);
                    return;
                }

                for (let i=0;i<this.listaNotaMateriaModulo.length; i++) {
                    if (this.listaNotaMateriaModulo[i].calificaciones != null) {
                        this.notasCualitativas(this.listaNotaMateriaModulo[i]);
                    }
                }
                for (let i=0;i<this.listaCalificaciones.length;i++) {
                    this.columnasEvaluacion.push(this.listaCalificaciones[i].calNemonico);
                }
                this.dataSource = new MatTableDataSource(this.listaNotaMateriaModulo);
                this.dataSource.paginator = this.paginator;
                this.dataSource.paginator._intl.itemsPerPageLabel="Asignaturas por página";
                this.dataSource.paginator._intl.nextPageLabel="Siguiente";
                this.dataSource.paginator._intl.previousPageLabel="Anterior";
                this.dataSource.sort = this.sort;
            },
            (error:any)=>{
                this.spinner.hide();    
                Swal.fire('Ups! ocurrió un error al obtener las notas de la materias','','error'); 
            }
        );
    }

    notasCualitativas(notaMateriaModulo:NotaMateriaModulo) {
        let listaModeloEvaluacionGeneralLimpio:ModeloEvaluacionGeneral[] = Array.from(new Set(this.listaModeloEvaluacionGeneral));
        for (let i =0; i<listaModeloEvaluacionGeneralLimpio.length; i++) {
            let modeloGeneral = listaModeloEvaluacionGeneralLimpio[i];
            let calificacionesProSub:Calificacion = notaMateriaModulo.calificaciones.find((calificacion)=> calificacion.calNemonico.substring(0,3) == 'PRO' && calificacion.modgenCodigo == modeloGeneral.modgenCodigo);
            let calificacionesEscalaCual:Calificacion = notaMateriaModulo.calificaciones.find((calificacion)=> calificacion.calNemonico.substring(0,2) == 'EC' && calificacion.modgenCodigo == modeloGeneral.modgenCodigo);
            if (calificacionesEscalaCual != null && calificacionesEscalaCual != undefined) {
                let parmCalifCuantitativo = this.listaParmCalificacionCuantitativo.find((parmCalifCuantitativo) => calificacionesProSub.notaValorCunt >= parmCalifCuantitativo.parcaMinimo && calificacionesProSub.notaValorCunt <= parmCalifCuantitativo.parcaMaximo);
                calificacionesEscalaCual.notaValorCual = parmCalifCuantitativo.parcaNemonico;
            }

            let calificacionesProAnual:Calificacion = notaMateriaModulo.calificaciones.find((calificacion)=> calificacion.calNemonico == 'PANUALORD');
            let calificacionesEscalaCualAnual:Calificacion = notaMateriaModulo.calificaciones.find((calificacion)=> calificacion.calNemonico.substring(0,3) == 'ECA');
            if (calificacionesEscalaCualAnual != null && calificacionesEscalaCualAnual != undefined) {
                let parmCalifCuantitativo = this.listaParmCalificacionCuantitativo.find((parmCalifCuantitativo) => calificacionesProAnual.notaValorCunt >= parmCalifCuantitativo.parcaMinimo && calificacionesProAnual.notaValorCunt <= parmCalifCuantitativo.parcaMaximo);
                calificacionesEscalaCualAnual.notaValorCual = parmCalifCuantitativo.parcaNemonico;
            }
        }
    }

    volverPeriodoLectivo() {
        this.router.navigate(['pages/evaluacion-periodo-lectivo']);
    }

    applyFilter(event: Event) {
        const filterValue = (event.target as HTMLInputElement).value;
        this.dataSource.filter = filterValue.trim().toLowerCase();
       
        if (this.dataSource.paginator) {
           this.dataSource.paginator.firstPage();
        }
    }
    
}