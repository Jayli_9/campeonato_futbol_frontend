import { ComponentFixture, TestBed } from '@angular/core/testing';

import { EvaluacionMateriasComponent } from './evaluacion-materias.component';

describe('EvaluacionMateriaComponent', () => {
  let component: EvaluacionMateriasComponent;
  let fixture: ComponentFixture<EvaluacionMateriasComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ EvaluacionMateriasComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(EvaluacionMateriasComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});