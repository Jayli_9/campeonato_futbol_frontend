import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { CoreCommonModule } from '@core/common.module';
import { ContentHeaderModule } from 'app/layout/components/content-header/content-header.module';
import { MaterialModule } from 'app/main/shared/material/material.module';
import { EvaluacionComponent } from '../components/evaluacion-estudiantes/evaluacion.component';
import { RUTA_EVALUACION } from '../routes/evaluacion-routing.module';
import { EvaluacionMateriasComponent } from '../components/evaluacion-materias/evaluacion-materias.component';
import { EvaluacionPeriodoLectivoComponent } from '../components/evaluacion-periodo-lectivo/evaluacion-periodo-lectivo.component';
import { ProgramaStore } from '../services/programa-store';

@NgModule({
  declarations: [
    EvaluacionMateriasComponent, 
    EvaluacionComponent,
    EvaluacionPeriodoLectivoComponent],
  imports: [
    CommonModule,
    RouterModule.forChild(RUTA_EVALUACION),
    FormsModule,
    ReactiveFormsModule,
    MaterialModule,
    ContentHeaderModule,
    CoreCommonModule
  ],
  providers: [ProgramaStore]
})
export class EvaluacionModule { }