export interface Calificacion {
    calCodigo: number;
    calDescripcion: string;
    calEstado: number;
    modgenCodigo: number;
    calNemonico: string;
    calPorcentaje: number;
    reanleCodigo: number;
    calMinimo: number;
    calMaximo: number;
    sereduCodigo: number;
    notaValorCunt?: number;
    notaValorCual?: string;
}