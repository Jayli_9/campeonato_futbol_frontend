export interface Alfabetizacion {
    unialfCodigo:number;
    mmodCodigo:number;
    unialfDescripcion:string;
    unialfEstado:number;
}