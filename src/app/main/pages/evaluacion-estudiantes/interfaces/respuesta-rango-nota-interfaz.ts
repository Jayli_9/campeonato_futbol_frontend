import { RespuestaGeneralInterfaz } from "app/main/shared/interfaces/respuesta-general-interfaz";
import { RangoNota } from "./rango-nota";

export interface RespuestaRangoNotaInterfaz extends RespuestaGeneralInterfaz {
    listado: RangoNota[];
    objeto: RangoNota;
}