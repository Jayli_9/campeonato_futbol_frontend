export interface ParmCalificacionCuantitativo {
    parcaCodigo: number;
    parcaDescripcion: string;
    parcaNemonico: string;
    parcaMinimo: number;
    parcaMaximo: number;
    reanleCodigo: number;
    parcaEstado: number;
}