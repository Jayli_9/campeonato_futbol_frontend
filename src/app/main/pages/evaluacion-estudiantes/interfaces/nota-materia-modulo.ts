import { Calificacion } from "./calificacion";
import { ModeloEvaluacionGeneral } from "./modelo-evaluacion-general";

export interface NotaMateriaModulo {
    nasiextCodigo:number;
    asiCodigo:number;
    maasiexCodigo: number;
    asiDescripcion: string;
    calificaciones: Calificacion[];
    listaModevaGen:ModeloEvaluacionGeneral[];
}