export interface RangoNota {
    ranotCodigo: number;
    ragnotDescripcion: string;
    ranotMinimo: number;
    ranotMaximo: number;
    ranotEstado: number;
    reanleCodigo: number;
}