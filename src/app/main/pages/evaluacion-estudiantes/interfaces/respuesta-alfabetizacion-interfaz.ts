import { RespuestaGeneralInterfaz } from "app/main/shared/interfaces/respuesta-general-interfaz";
import { Alfabetizacion } from "./alfabetizacion";

export interface RespuestaAlfabetizacionInterfaz extends RespuestaGeneralInterfaz {
    listado: Alfabetizacion[];
    objeto: Alfabetizacion;
}