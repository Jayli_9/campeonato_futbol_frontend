import { RespuestaGeneralInterfaz } from "app/main/shared/interfaces/respuesta-general-interfaz";
import { NotaMateriaModulo } from "./nota-materia-modulo";

export interface RespuestaNotasMateriaModuloInterfaz extends RespuestaGeneralInterfaz {
    listado: NotaMateriaModulo[];
    objeto: NotaMateriaModulo;
}