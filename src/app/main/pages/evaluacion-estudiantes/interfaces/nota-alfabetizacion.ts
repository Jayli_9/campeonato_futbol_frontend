import { Alfabetizacion } from "./alfabetizacion";
import { Calificacion } from "./calificacion";
import { ModeloEvaluacionGeneral } from "./modelo-evaluacion-general";

export interface NotaAlfabetizacion {
    nouniCodigo:number;
    matCodigo:number;
    estIdentificacion:string;
    nombreEstudiante:string;
    apellidoEstudiante:string;
    nouniEstado:number;
    calificaciones:Calificacion[];
    alfabetizaciones:Alfabetizacion[];
    listaModevaGen:ModeloEvaluacionGeneral[];
    promedioAnual?:number;
}