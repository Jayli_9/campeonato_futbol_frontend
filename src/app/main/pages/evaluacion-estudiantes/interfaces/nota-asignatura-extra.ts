import { Calificacion } from "./calificacion";
import { ModeloEvaluacion } from "./modelo-evaluacion";
import { ModeloEvaluacionGeneral } from "./modelo-evaluacion-general";

export interface NotaAsignaturaExtra {
    nasiextCodigo?:number;
    matCodigo:number;
    estIdentificacion:string;
    nombreEstudiante:string;
    apellidoEstudiante:string;
    nasiextEstado:number;
    maasiexCodigo:number;
    maasiexNemonico:string;
    asiCodigo:number;
    asiDescripcion:string;
    asiNemonico:string;
    calificaciones:Calificacion[];
    listaModevaGen:ModeloEvaluacionGeneral[];
    modeloEvaluacion:ModeloEvaluacion;
    promedioAnual?:number;
}