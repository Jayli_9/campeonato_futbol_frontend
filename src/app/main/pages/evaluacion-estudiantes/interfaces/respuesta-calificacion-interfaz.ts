import { RespuestaGeneralInterfaz } from "app/main/shared/interfaces/respuesta-general-interfaz";
import { Calificacion } from "./calificacion";

export interface RespuestaCalificacionInterfaz extends RespuestaGeneralInterfaz {
    listado: Calificacion[];
    objeto: Calificacion;
}