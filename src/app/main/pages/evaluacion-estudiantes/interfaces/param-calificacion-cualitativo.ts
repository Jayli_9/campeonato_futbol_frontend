export interface ParmCalificacionCualitativo {
    parcualCodigo: number;
    parcualDescripcion: string;
    parcualLetra: string;
    parcualValor: number;
    reanleCodigo: number;
    parcualEstado: number;
}