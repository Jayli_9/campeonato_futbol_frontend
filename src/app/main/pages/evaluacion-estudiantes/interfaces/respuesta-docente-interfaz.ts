import { RespuestaGeneralInterfaz } from "app/main/shared/interfaces/respuesta-general-interfaz";
import { Docente } from "./docente";

export interface RespuestaDocenteInterfaz extends RespuestaGeneralInterfaz {
    listado: Docente[];
    objeto: Docente;
}