import { RespuestaGeneralInterfaz } from "app/main/shared/interfaces/respuesta-general-interfaz";
import { NotaAsignaturaExtra } from "./nota-asignatura-extra";

export interface RespuestaNotaAsignaturaExtraInterfaz extends RespuestaGeneralInterfaz {
    listado: NotaAsignaturaExtra[];
    objeto: NotaAsignaturaExtra;
}