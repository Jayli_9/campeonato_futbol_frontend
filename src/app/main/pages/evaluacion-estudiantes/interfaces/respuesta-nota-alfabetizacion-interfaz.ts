import { RespuestaGeneralInterfaz } from "app/main/shared/interfaces/respuesta-general-interfaz";
import { NotaAlfabetizacion } from "./nota-alfabetizacion";

export interface RespuestaNotaAlfabetizacionInterfaz extends RespuestaGeneralInterfaz {
    listado: NotaAlfabetizacion[];
    objeto: NotaAlfabetizacion;
}