import { RespuestaGeneralInterfaz } from "app/main/shared/interfaces/respuesta-general-interfaz";
import { NotaComportamiento } from "./nota-comportamiento";

export interface RespuestaNotaComportamientoInterfaz extends RespuestaGeneralInterfaz {
    listado: NotaComportamiento[];
    objeto: NotaComportamiento;
}