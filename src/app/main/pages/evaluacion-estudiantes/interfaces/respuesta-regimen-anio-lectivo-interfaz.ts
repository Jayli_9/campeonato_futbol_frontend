import { RespuestaGeneralInterfaz } from "app/main/shared/interfaces/respuesta-general-interfaz";
import { RegimenAnioLectivo } from "./regimen-anio-lectivo";

export interface RespuestaRegimenAnioLectivoInterfaz extends RespuestaGeneralInterfaz {
    listado: RegimenAnioLectivo[];
    objeto: RegimenAnioLectivo;
}