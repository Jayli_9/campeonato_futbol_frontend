import { RespuestaGeneralInterfaz } from "app/main/shared/interfaces/respuesta-general-interfaz";
import { Comportamiento } from "./comportamiento";

export interface RespuestaComportamientoInterfaz extends RespuestaGeneralInterfaz {
    listado: Comportamiento[];
    objeto: Comportamiento;
}