export interface GradoEquivalencia {
    graequCodigo?:number;
    graequGradoInicio:number;
    graequGradoFin:number;
    graequEstado:number;
    sereduCodigo:number;
    graequNemonico:string;
    graequDescripcion:string;
}