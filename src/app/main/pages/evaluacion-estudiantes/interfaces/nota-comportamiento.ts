import { Calificacion } from "./calificacion";

export interface NotaComportamiento {
    nocoCodigo:number;
    matCodigo:number;
    comCodigo:number;
    estIdentificacion:string;
    nombreEstudiante:string;
    apellidoEstudiante:string;
    nocoEstado:number;
    calificaciones:Calificacion[];
}