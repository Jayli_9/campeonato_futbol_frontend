import { RespuestaGeneralInterfaz } from "app/main/shared/interfaces/respuesta-general-interfaz";
import { ParmCalificacionCuantitativo } from "./param-calificacion-cuantitativo";

export interface RespuestaParmCalificacionCuantitativoInterfaz extends RespuestaGeneralInterfaz {
    listado: ParmCalificacionCuantitativo[];
    objeto: ParmCalificacionCuantitativo;
}