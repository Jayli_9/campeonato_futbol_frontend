import { RespuestaGeneralInterfaz } from "app/main/shared/interfaces/respuesta-general-interfaz";
import { ParmCalificacionCualitativo } from "./param-calificacion-cualitativo";

export interface RespuestaParmCalificacionCualitativoInterfaz extends RespuestaGeneralInterfaz {
    listado: ParmCalificacionCualitativo[];
    objeto: ParmCalificacionCualitativo;
}