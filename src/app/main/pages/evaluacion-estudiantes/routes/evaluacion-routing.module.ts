import { Routes } from "@angular/router";
import { AuthGuard } from "app/auth/helpers/auth.guards";
import { EvaluacionComponent } from "../components/evaluacion-estudiantes/evaluacion.component";
import { EvaluacionMateriasComponent } from "../components/evaluacion-materias/evaluacion-materias.component";
import { EvaluacionPeriodoLectivoComponent } from "../components/evaluacion-periodo-lectivo/evaluacion-periodo-lectivo.component";

export const RUTA_EVALUACION:Routes=[
    {
        path:'evaluacion-estudiantes',
        component:EvaluacionComponent,
    },
    {
        path:'evaluacion-reporte-materias',
        component:EvaluacionMateriasComponent,
    },
    {
        path:'evaluacion-periodo-lectivo',
        component:EvaluacionPeriodoLectivoComponent,
        canActivate:[AuthGuard]
    }
]