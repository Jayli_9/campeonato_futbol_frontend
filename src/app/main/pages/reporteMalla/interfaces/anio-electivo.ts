export interface AnioElectivo{
    anilecCodigo: number;
    anilecEstado: number;
    anilecFechaCreacion: Date;
    anilecAnioInicio: number;
    anilecAnioFin: number;
}