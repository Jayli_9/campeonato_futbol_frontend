export interface RegimenAnioElectivoInterface{
    reanleCodigo: number;
    reanleEstado: number;
    reanleFechaCreacion: Date;
    reanleTipo: string;
    regCodigo: number;
    anilecCodigo: number;
    descripcionAnioLectivo: string;
}