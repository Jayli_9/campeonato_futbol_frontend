export interface institucionInterface{
    denCodigo: number;
    inDescripcion: string;
    insAmie: string;
    insCodigo: number;
    insDescripcion: string;
    insDireccion: string;
    insEslogan: string;
    insEstado: number;
    insFechaCreacion: number;
    insFechaResolucion: string;
    insLogo: string;
    insNumResolucion: string;
    insNumeroPermiso: string;
    insRutaArchivo: string;
    jurCodigo: number;
    regimenes: [];
    sosCodigo: number;
    tipinsCodigo: number;
     
}