import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { environment } from 'environments/environment';
import { ResponseGenerico } from '../interfaces/response-generico';
import { institucionInterface } from '../interfaces/institucion';

@Injectable({
  providedIn: 'root'
})
export class GieeService {

  private readonly URL_REST = environment.url_institucion;

constructor(private _http: HttpClient) { }

buscarAmiePorCodigo(cod):  Promise<ResponseGenerico<institucionInterface>>{
  return new Promise((resolve, reject) => {
this._http.get(`${environment.url_institucion}/private/buscarInstitucionPorAmie/` + cod).subscribe((response: ResponseGenerico<institucionInterface>) => {
   resolve(response);
 }, reject);
})
}

}
