import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { environment } from 'environments/environment';
import { ResponseGenerico } from '../interfaces/response-generico';
import { RegimenAnioElectivoInterface } from '../interfaces/cat-regimen-anioleg';
import { AnioElectivo } from '../interfaces/anio-electivo';
import { regimenInterface } from '../interfaces/regimen';
import { gradosInterface } from '../interfaces/grados';

@Injectable({
  providedIn: 'root'
})
export class CatalogoService {
  
  private readonly URL_REST = environment.url_catalogo;

constructor(private _http: HttpClient) { }


buscarRegimenAnioelectivo(cod): Promise<ResponseGenerico<RegimenAnioElectivoInterface>>{
  return new Promise((resolve, reject) => {
this._http.get(`${environment.url_catalogo}/private/listarRegimenAnioLectivoPorCodigoRegimen/`+cod).subscribe((response: ResponseGenerico<RegimenAnioElectivoInterface>) => {
   resolve(response);
 }, reject);
})
}


buscarRegimenPorCodigo(cod): Promise<ResponseGenerico<regimenInterface>>{
  return new Promise((resolve, reject) => {
this._http.get(`${environment.url_catalogo}/private/buscarRegimenPorCodigo/`+cod).subscribe((response: ResponseGenerico<regimenInterface>) => {
   resolve(response);
 }, reject);
})
}


buscarCodigoAnioLectivo(cod): Promise<ResponseGenerico<AnioElectivo>>{
  return new Promise((resolve, reject) => {
this._http.get(`${environment.url_catalogo}/private/buscarAnioLectivoPorCodigo/`+ cod).subscribe((response: ResponseGenerico<AnioElectivo>) => {
   resolve(response);
 }, reject);
})
}

buscarGradosPorCodigo(cod): Promise<ResponseGenerico<gradosInterface>>{
  return new Promise((resolve, reject) => {
this._http.get(`${environment.url_catalogo}/private/buscarCatalogoGradosPorCodigo/`+ cod).subscribe((response: ResponseGenerico<gradosInterface>) => {
   resolve(response);
 }, reject);
})
}

listarRegimen(): Promise<ResponseGenerico<regimenInterface>>{
  return new Promise((resolve, reject) => {
this._http.get(`${environment.url_catalogo}/private/listarRegimenes`).subscribe((response: ResponseGenerico<regimenInterface>) => {
   resolve(response);
 }, reject);
})
}

}