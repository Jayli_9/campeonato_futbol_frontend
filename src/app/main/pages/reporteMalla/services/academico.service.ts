import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { environment } from 'environments/environment';
import { ResponseGenerico } from '../interfaces/response-generico';

@Injectable({
  providedIn: 'root'
})
export class AcademicoService {

constructor(private _http: HttpClient) { }


generarMalla(parametro: any){
  return this._http.post<ResponseGenerico<String>>(`${environment.url_academico}/private/generarMalla`, parametro);
}

}
