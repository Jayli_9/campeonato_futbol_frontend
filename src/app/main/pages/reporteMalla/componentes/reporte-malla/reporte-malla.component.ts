import { Component, OnInit } from '@angular/core';
import { User } from 'app/auth/models';
import { CatalogoService } from '../../services/catalogo.service';
import { GieeService } from '../../services/giee.service';
import { AcademicoService } from '../../services/academico.service';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-reporte-malla',
  templateUrl: './reporte-malla.component.html',
  styleUrls: ['./reporte-malla.component.scss']
})
export class ReporteMallaComponent implements OnInit {

  datosDeMalla;

  contentHeader: object;

  //lista de region anio lectivo
  listaRegimenAnioElectivo = [];
  listaAnioElectivo = [];
  descripcionRegimen;
  fechaInicio;
  fechaFin;
  codigoRegimenAnioElectivo=0;
  codigoAnioElectivo=0;

  currentUser: User


  constructor(
    private readonly catalogoService: CatalogoService,
    private readonly gieeService: GieeService,
    private readonly adecademicoService: AcademicoService
  ) { 
    this.currentUser = JSON.parse(localStorage.getItem("currentUser"))
    this.buscarInstitucionAmie(this.currentUser.sede.nemonico);
  }

  buscarInstitucionAmie(amie: string) {
    this.gieeService.buscarAmiePorCodigo(amie).then(data => {
      let institucion = data.objeto
      for(let i = 0; i < institucion.regimenes.length; i++){
      }
    })
  }


  cambioAnioElectivo(event){
    this.fechaInicio=event.datosObjeto.anilecAnioInicio;
    this.fechaFin = event.datosObjeto.anilecAnioFin;
  }

cambioRegimen(event){

  this.fechaInicio = null;
  this.fechaFin = null;

  this.listaAnioElectivo=[];

  this.descripcionRegimen = event.regDescripcion;

  this.catalogoService.buscarRegimenAnioelectivo(event.regCodigo).then(data => {
    let listado = data.listado;
    listado.forEach(element => {
      this.catalogoService.buscarCodigoAnioLectivo(element.anilecCodigo).then(data => {
        let listaDatos = data.objeto;

        let datos = {
          realcodgio: element.reanleCodigo,
          datosObjeto: listaDatos
        }

        this.listaAnioElectivo.push(datos);
      })
    });
  })

}

  ngOnInit() {

    this.catalogoService.listarRegimen().then(data => {
      this.listaRegimenAnioElectivo = data.listado;
    })

    this.contentHeader = {
      headerTitle: 'Reporte Malla',
      actionButton: false,
      breadcrumb: {
          type: '',
          links: [
              {
                name: 'Inicio',
                isLink: true,
                link: '/pages/inicio'
              },
              {
                name: 'ReporteMalla',
                isLink: false
              }
          ]
      }
    };


  }


  crearMalla(){
    Swal.fire({
      title: "¿Desea Generar La Malla?",
      icon: "warning",
      showCancelButton: true,
      confirmButtonColor: "#3085d6",
      cancelButtonColor: "#d33",
      confirmButtonText: "Si",
      cancelButtonText: "No"
    }).then((result) => {
      if (result.isConfirmed) {
        let datos = {
          "anioFin": this.fechaFin,
          "anioInicio": this.fechaInicio,
          "paramRegimen": this.descripcionRegimen
        }

        this.adecademicoService.generarMalla(datos).subscribe({
          next: (Response)=>{
            Swal.fire({
              title: "Malla Generada ",
              text: "Los datos han sido guardados con éxito",
              icon: "success"
            });
          },
          error: (error) =>{
            Swal.fire({
              icon: "error",
              title: "Atención",
              text: "Hubo problemas en generar la malla",
            });
          }
        })


      }
    });
  }

}
