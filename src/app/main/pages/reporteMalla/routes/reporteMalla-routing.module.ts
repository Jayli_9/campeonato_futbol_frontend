import { Routes } from "@angular/router";
import { AuthGuard } from "app/auth/helpers/auth.guards";
import { ReporteMallaComponent } from "../componentes/reporte-malla/reporte-malla.component";

export const RUTA_REPORTE_MALLA:Routes=[
    {
        path:'ReporteMalla',
        component:ReporteMallaComponent,
        canActivate:[AuthGuard]
    }
]