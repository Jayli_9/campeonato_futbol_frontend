import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReporteMallaComponent } from "../componentes/reporte-malla/reporte-malla.component";
import { RUTA_REPORTE_MALLA } from "../routes/reporteMalla-routing.module";
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { CoreCommonModule } from '@core/common.module';
import { ContentHeaderModule } from 'app/layout/components/content-header/content-header.module';
import { MaterialModule } from 'app/main/shared/material/material.module';


@NgModule({
    declarations: [ReporteMallaComponent],
    imports: [
      CommonModule,
      RouterModule.forChild(RUTA_REPORTE_MALLA),
      FormsModule,
      ReactiveFormsModule,
      MaterialModule,
      ContentHeaderModule,
      CoreCommonModule
    ]
  })

  export class ReporteMallaModule { }