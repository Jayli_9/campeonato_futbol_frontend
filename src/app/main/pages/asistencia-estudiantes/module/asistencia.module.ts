import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { CoreCommonModule } from '@core/common.module';
import { ContentHeaderModule } from 'app/layout/components/content-header/content-header.module';
import { MaterialModule } from 'app/main/shared/material/material.module';
import { AsistenciaComponent } from '../components/asistencia-estudiantes/asistencia.component';
import { RUTA_ASISTENCIA } from '../routes/asistencia-routing.module';
import { AsistenciaPeriodoLectivoComponent } from '../components/asistencia-periodo-lectivo/asistencia-periodo-lectivo.component';
import { ProgramaStore } from '../services/programa-store';



@NgModule({
  declarations: [
    AsistenciaComponent,
    AsistenciaPeriodoLectivoComponent,
    
  ],
  imports: [
    CommonModule,
    RouterModule.forChild(RUTA_ASISTENCIA),
    FormsModule,
    ReactiveFormsModule,
    MaterialModule,
    ContentHeaderModule,
    CoreCommonModule
  ],
  providers: [ProgramaStore]
})
export class AsistenciaModule { }