import { RespuestaGeneralInterfaz } from "app/main/shared/interfaces/respuesta-general-interfaz";
import { Especialidad } from "./especialidad";

export interface RespuestaEspecialidadInterfaz extends RespuestaGeneralInterfaz {
    listado: Especialidad[];
    objeto: Especialidad;
}