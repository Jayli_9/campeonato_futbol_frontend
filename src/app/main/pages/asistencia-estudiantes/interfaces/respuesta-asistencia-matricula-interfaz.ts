import { RespuestaGeneralInterfaz } from "app/main/shared/interfaces/respuesta-general-interfaz";
import { AsistenciaMatricula } from "./asistencia-matricula";

export interface RespuestaAsistenciaMatriculaInterfaz extends RespuestaGeneralInterfaz {
    listado: AsistenciaMatricula[];
    objeto: AsistenciaMatricula;
}