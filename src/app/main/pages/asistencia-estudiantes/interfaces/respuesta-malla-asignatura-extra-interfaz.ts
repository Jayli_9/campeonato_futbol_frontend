import { RespuestaGeneralInterfaz } from "app/main/shared/interfaces/respuesta-general-interfaz";
import { MallaAsignaturaExtra } from "./malla-asignatura-extra";

export interface RespuestaMallaAsignaturaExtraInterfaz extends RespuestaGeneralInterfaz {
    listado: MallaAsignaturaExtra[];
    objeto: MallaAsignaturaExtra;
}