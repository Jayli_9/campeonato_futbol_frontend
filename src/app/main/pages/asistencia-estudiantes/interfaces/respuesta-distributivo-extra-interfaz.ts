import { RespuestaGeneralInterfaz } from "app/main/shared/interfaces/respuesta-general-interfaz";
import { DistributivoExtra } from "./distributivo-extra";

export interface RespuestaDistributivoExtraInterfaz extends RespuestaGeneralInterfaz {
    listado: DistributivoExtra[];
    objeto: DistributivoExtra;
}