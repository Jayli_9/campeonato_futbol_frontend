import { RespuestaGeneralInterfaz } from "app/main/shared/interfaces/respuesta-general-interfaz";
import { Jornada } from "./jornada";

export interface RespuestaJornadaInterfaz extends RespuestaGeneralInterfaz {
    listado: Jornada[];
    objeto: Jornada;
}