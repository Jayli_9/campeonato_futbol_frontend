export interface MatriculaExtaordinaria {
    matCodigo: number;
    estCodigo: number;
    matEstado: number;
    matObservacion: string;
    estIdentificacion: string;
    nombreEstudiante: string;
    apellidoEstudiante: string;
    curextCodigo: number;
    cupaexCodigo: number;
    reanleCodigo: number;
    sereduCodigo: number;
    matTipoProceso: number;
}