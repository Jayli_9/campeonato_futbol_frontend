export interface AsistenciaParametros {
    listaMatCodigo:number[];
    docCodigo:number;
    curextCodigo:number;
    cupaexCodigo:number;
    maasiexCodig:number;
    reanleCodigo:number;
    disextCodigo:number;
    fechaInicio:Date;
    fechaFin:Date;
}