export interface Asistencia {
    asisCodigo:number;
    horCodigo:number;
    tasiCodigo:number;
    hoCodigo:number;
    matCodigo:number;
    asisFecAsistencia:Date;
    asisAsistencia:boolean;
    asisJustificacion:boolean;
    dia:string;
    fechaNombre:string;
    horario:string;
    reanleCodigo:number;
    asisEstado:number;
}