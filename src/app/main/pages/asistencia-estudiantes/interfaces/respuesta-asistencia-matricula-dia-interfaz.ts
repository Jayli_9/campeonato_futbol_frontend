import { RespuestaGeneralInterfaz } from "app/main/shared/interfaces/respuesta-general-interfaz";
import { AsistenciaMatriculaDia } from "./asistencia-matricula-dia";


export interface RespuestaAsistenciaMatriculaDiaInterfaz extends RespuestaGeneralInterfaz {
    listado: AsistenciaMatriculaDia[];
    objeto: AsistenciaMatriculaDia;
}