import { RespuestaGeneralInterfaz } from "app/main/shared/interfaces/respuesta-general-interfaz";
import { AsistenciaDia } from "./asistencia-dia";

export interface RespuestaAsistenciaDiaInterfaz extends RespuestaGeneralInterfaz {
    listado: AsistenciaDia[];
    objeto: AsistenciaDia;
}