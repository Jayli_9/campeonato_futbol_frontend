export interface MallaAsignaturaExtra {
    maasiexCodigo: number;
    disextCodigo:number;
    asiCodigo: number;
    asiDescripcion: string;
    tivaCodigo:number;
    maasiexNemonico: string
    maasiexEstado: number;
    maasiexHorasPre: number;
    maasiexHorasAuto: number;
    maasiexHraTotal: number;
    curextCodigo: number;
    reanleCodigo: number;
}