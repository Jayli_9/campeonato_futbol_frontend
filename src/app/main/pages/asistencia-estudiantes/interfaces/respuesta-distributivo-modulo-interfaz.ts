import { RespuestaGeneralInterfaz } from "app/main/shared/interfaces/respuesta-general-interfaz";
import { DistributivoModulo } from "./distributivo-modulo";

export interface RespuestaDistributivoModuloInterfaz extends RespuestaGeneralInterfaz {
    listado: DistributivoModulo[];
    objeto: DistributivoModulo;
}