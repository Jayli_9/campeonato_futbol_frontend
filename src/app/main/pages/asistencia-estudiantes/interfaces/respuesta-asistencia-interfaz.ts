import { RespuestaGeneralInterfaz } from "app/main/shared/interfaces/respuesta-general-interfaz";
import { Asistencia } from "./asistencia";

export interface RespuestaAsistenciaInterfaz extends RespuestaGeneralInterfaz {
    listado: Asistencia[];
    objeto: Asistencia;
}