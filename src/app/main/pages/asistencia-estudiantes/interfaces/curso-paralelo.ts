export interface CursoParalelo {
    curparCodigo: number;
    curparAforo: number;
    curparBancas: number;
    curparEstado: number;
    idiEtnCodigo: number;
    curparNumEstud: number;
    curCodigo: number;
    parCodigo: number;
    nombreParalelo: string;
    codModalidad?:number;
}