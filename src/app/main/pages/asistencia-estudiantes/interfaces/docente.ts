export interface Docente {
    codPersona: number;
    numIdentificacion: string;
    nomPersona: string;
    numRegistroAccion: string;
    codInstitucion: number;
}