import { RespuestaGeneralInterfaz } from "app/main/shared/interfaces/respuesta-general-interfaz";
import { InstEstablecimiento } from "./instEstablecimiento";

export interface RespuestaInstEstablecimientoInterfaz extends RespuestaGeneralInterfaz {
    listado: InstEstablecimiento[];
    objeto: InstEstablecimiento;
}