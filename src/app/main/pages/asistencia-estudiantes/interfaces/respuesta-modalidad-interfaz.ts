import { RespuestaGeneralInterfaz } from "app/main/shared/interfaces/respuesta-general-interfaz";
import { Modalidad } from "./modalidad";

export interface RespuestaModalidadRespuesta extends RespuestaGeneralInterfaz {
    listado: Modalidad[];
    objeto: Modalidad;
}