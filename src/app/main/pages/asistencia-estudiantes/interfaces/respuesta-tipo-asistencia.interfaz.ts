import { RespuestaGeneralInterfaz } from "app/main/shared/interfaces/respuesta-general-interfaz";
import { TipoAsistencia } from "./tipo-asistencia";

export interface RespuestaTipoAsistenciaInterfaz extends RespuestaGeneralInterfaz {
    listado: TipoAsistencia[];
    objeto: TipoAsistencia;
}