import { Routes } from "@angular/router";
import { AuthGuard } from "app/auth/helpers/auth.guards";
import { AsistenciaComponent } from "../components/asistencia-estudiantes/asistencia.component";
import { AsistenciaPeriodoLectivoComponent } from "../components/asistencia-periodo-lectivo/asistencia-periodo-lectivo.component";

export const RUTA_ASISTENCIA:Routes=[
    {
        path:'ingresar-asistencia',
        component:AsistenciaComponent
    },
    {
        path:'asistencia-periodo-lectivo',
        component:AsistenciaPeriodoLectivoComponent,
        canActivate:[AuthGuard]
    }
]