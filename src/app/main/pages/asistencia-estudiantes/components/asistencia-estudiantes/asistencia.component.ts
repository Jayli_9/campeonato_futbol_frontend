import { Component, OnInit, ViewChild } from "@angular/core";
import { MatPaginator } from "@angular/material/paginator";
import { MatSort } from "@angular/material/sort";
import { MatTableDataSource } from "@angular/material/table";
import { Router } from "@angular/router";
import { NgxSpinnerService } from "ngx-spinner";
import Swal from "sweetalert2";
import { Asistencia } from "../../interfaces/asistencia";
import { AsistenciaDia } from "../../interfaces/asistencia-dia";
import { AsistenciaMatricula } from "../../interfaces/asistencia-matricula";
import { AsistenciaMatriculaDia } from "../../interfaces/asistencia-matricula-dia";
import { CursoExtraordinaria } from "../../interfaces/curso-extraordinaria";
import { Docente } from "../../interfaces/docente";
import { EtapaServicioEducativo } from "../../interfaces/etapa-servicio-educativo";
import { Grado } from "../../interfaces/grado";
import { MallaAsignaturaExtra } from "../../interfaces/malla-asignatura-extra";
import { MatriculaExtaordinaria } from "../../interfaces/matricula-extraordinaria";
import { Mes } from "../../interfaces/mes";
import { Modulo } from "../../interfaces/modulo";
import { RegimenAnioLectivo } from "../../interfaces/regimen-anio-lectivo";
import { RespuestaAsistenciaMatriculaDiaInterfaz } from "../../interfaces/respuesta-asistencia-matricula-dia-interfaz";
import { RespuestaAsistenciaMatriculaInterfaz } from "../../interfaces/respuesta-asistencia-matricula-interfaz";
import { RespuestaEtapaServicioEducativoInterfaz } from "../../interfaces/respuesta-etapa-servicio-educativo-interfaz";
import { RespuestaGradoInterfaz } from "../../interfaces/respuesta-grado-interfaz";
import { RespuestaMallaAsignaturaExtraInterfaz } from "../../interfaces/respuesta-malla-asignatura-extra-interfaz";
import { RespuestaMatriculaExtraordinariaInterfaz } from "../../interfaces/respuesta-matricula-extraordinaria-interfaz";
import { RespuestaMesInterfaz } from "../../interfaces/respuesta-mes-interfaz";
import { RespuestaModuloInterfaz } from "../../interfaces/respuesta-modulo-interfaz";
import { RespuestaRegimenAnioLectivoInterfaz } from "../../interfaces/respuesta-regimen-anio-lectivo-interfaz";
import { RespuestaServicioEducativoInterfaz } from "../../interfaces/respuesta-servicio-educativo-interfaz";
import { RespuestaTipoAsistenciaInterfaz } from "../../interfaces/respuesta-tipo-asistencia.interfaz";
import { ServicioEducativo } from "../../interfaces/servicio-educativo";
import { TipoAsistencia } from "../../interfaces/tipo-asistencia";
import { AsistenciaService } from "../../services/asistencia.service";
import { ProgramaStore } from "../../services/programa-store";
import { AsistenciaParametros } from "../../interfaces/asistencia-parametros";
import { AsistenciaDiaParametros } from "../../interfaces/asistencia-dia-parametros";
import { MallaAsigExtraParametro } from "../../interfaces/malla-asig-extra-parametro";
import { MatriculaExtraParametros } from "../../interfaces/matricula-extra-parametros";

@Component({
    selector: 'app-asistencia',
    templateUrl: './asistencia.component.html',
    styleUrls: ['./asistencia.component.scss']
})
export class AsistenciaComponent implements OnInit {

    public contentHeader: object;
    public submitted = false;

    listaMatriculaExtraordinario:MatriculaExtaordinaria[];
    listaMallaAsignatura:MallaAsignaturaExtra[];
    listaAsistencia:Asistencia[];
    listaAsistenciaDia:AsistenciaDia[];
    listaAsistenciaMatricula:AsistenciaMatricula[];
    listaAsistenciaMatriculaDia:AsistenciaMatriculaDia[];
    listaMes:Mes[];
    columnasAsistencia=[];

    cursoExtraordinaria:CursoExtraordinaria;
    docente:Docente;
    servicioEducativo:ServicioEducativo;
    mallaAsignatura:MallaAsignaturaExtra;
    etapaServicioEducativo:EtapaServicioEducativo;
    regimenAnioLectivo:RegimenAnioLectivo;
    modulo:Modulo;
    grado:Grado;
    mes:Mes;
    tipoNiveModulo:string;
    fechaInicio:Date;
    fechaFin:Date;
    tipoAsistencia:string = '';

    dataSource: MatTableDataSource<AsistenciaMatricula>;
    dataSourceDia: MatTableDataSource<AsistenciaMatriculaDia>;

    @ViewChild(MatPaginator) paginator: MatPaginator;
    @ViewChild(MatSort) sort: MatSort;
    constructor( private readonly router: Router,
        public spinner: NgxSpinnerService,
        private _asistenciaService: AsistenciaService,
        private readonly programaStore: ProgramaStore
        ){
    }

    ngOnInit(): void {
        this.contentHeader = {
            headerTitle: 'Ingresar Asistencia',
            actionButton: false,
            breadcrumb: {
                type: '',
                links: [
                    {
                      name: 'Inicio',
                      isLink: true,
                      link: '/pages/inicio'
                    },
                    {
                      name: 'Ingresar Asistencia',
                      isLink: false
                    },
                ]
            }
        };
        this.obtenerCursoExtraordinario();
    }

    obtenerCursoExtraordinario() {
        this.cursoExtraordinaria = this.programaStore.cursoExtraordinaria;
        if (this.cursoExtraordinaria == null || this.cursoExtraordinaria == undefined) {
            this.volverAsistenciaPeriodo();
            return;
        }
        this.docente=this.programaStore.docente;
        this.spinner.show();
        this.obternerServicioEducativo();
        this.spinner.hide();
    }

    obternerServicioEducativo() {
        this._asistenciaService.obtenerServicioEducativoPorCodigo(this.cursoExtraordinaria.sereduCodigo).subscribe(
            (respuesta:RespuestaServicioEducativoInterfaz)=>{
                this.servicioEducativo = respuesta.objeto;
                this.obtenerTodasMatriculaEstudiantePorCurextCodigo();
            },
            (error:any)=>{
                this.spinner.hide();    
                Swal.fire('Ups! ocurrió un error con el servicio educativo','','error'); 
            }
        );
    }

    obtenerTodasMatriculaEstudiantePorCurextCodigo() {
        let matriculaExtraParametros:MatriculaExtraParametros = {
            curextCodigo:this.cursoExtraordinaria.curextCodigo,
            cupaexCodigo:this.cursoExtraordinaria.cupaexCodigo
        };
        this._asistenciaService.obtenerTodosEstudianteMatriculados(matriculaExtraParametros).subscribe(
            (respuesta:RespuestaMatriculaExtraordinariaInterfaz)=>{
                this.listaMatriculaExtraordinario = respuesta.listado;
                if (this.listaMatriculaExtraordinario.length == 0) {
                    Swal.fire('No existe estudiantes matricuados en este curso paralelo','','error');
                    this.volverAsistenciaPeriodo();
                    return;
                }
                
                if (this.cursoExtraordinaria.modCodigo != 0) {
                    this.obtenerRegimeAnioLectivo();
                } else if (this.cursoExtraordinaria.graCodigo != 0) {
                    this.obtenerGradoPorCodigo(this.cursoExtraordinaria.graCodigo);
                }
            },
            (error:any)=>{
                this.spinner.hide();    
                Swal.fire('Ups! ocurrió un error con la malla asignatura','','error'); 
            }
        );
    }

    obtenerRegimeAnioLectivo() {
        this._asistenciaService.obtenerRegimenAnioLectivoPorCodigo(this.cursoExtraordinaria.reanleCodigo).subscribe(
            (respuesta:RespuestaRegimenAnioLectivoInterfaz) => {
                let anio = new Date().getFullYear();
                this.regimenAnioLectivo = respuesta.objeto;
                if (this.regimenAnioLectivo.anilecAnioInicio > anio || this.regimenAnioLectivo.anilecAnioFin < anio) {
                    Swal.fire('Ups! el mes acual no esta dentro del regimen del año lectivo seleccionado','','warning'); 
                    this.volverAsistenciaPeriodo();
                } else {
                    this.obtenerModuloPorCodigo();
                }
            },
            (error:any)=>{
                this.spinner.hide();    
                Swal.fire('Ups! ocurrió un error el regimen año lectivo','','error'); 
            }
        );
    }

    obtenerModuloPorCodigo() {
        let modCodigo =  this.cursoExtraordinaria.modCodigo;
        this._asistenciaService.obtenerModuloPorCodigo(modCodigo).subscribe(
            (respuesta:RespuestaModuloInterfaz) => {
                this.modulo = respuesta.objeto;
                this.obtenerGradoPorCodigo(this.modulo.modCodigoGradoInicio);
            },
            (error:any)=>{
                this.spinner.hide();    
                Swal.fire('Ups! ocurrió un error con el modulo','','error'); 
            }
        );
    }

    obtenerGradoPorCodigo(graCodigo:any) {
        this._asistenciaService.obtenerGradorPorGraCodigo(graCodigo).subscribe(
            (respuesta:RespuestaGradoInterfaz) => {
                this.grado = respuesta.objeto;
                if (this.grado.graCodigo == 10 || this.grado.graCodigo == 11 || this.grado.graCodigo == 12 || this.grado.graCodigo == 13 ||
                    this.grado.graCodigo == 14 || this.grado.graCodigo == 15 || this.grado.graCodigo == 16 || this.grado.graCodigo == 17 || 
                    this.grado.graCodigo == 18) {
                    this.obtenerTodasMallaAsignaturasExtraCursoExtraordinariaYGrado();
                    this.obtenerListaEtapaServicioEducativoPorSevicioEducativo();
                    this.tipoAsistencia = 'POR_HORARIO';
                } else {
                    this.obtenerListaMes();
                    this.tipoAsistencia = 'POR_DIA';
                }
            },
            (error:any)=>{
                this.spinner.hide();    
                Swal.fire('Ups! ocurrió un error con el grado','','error'); 
            }
        );
    }

    obtenerListaMes(){
        this._asistenciaService.obtenerListaMeses().subscribe(
            (respuesta:RespuestaMesInterfaz) => {
                this.listaMes = respuesta.listado;
            },
            (error:any)=>{
                this.spinner.hide();    
                Swal.fire('Ups! ocurrió un error con el listado de meses','','error'); 
            }
        );
    }

    obtenerListaEtapaServicioEducativoPorSevicioEducativo() {
        this._asistenciaService.obtenerEtapaServicioEducaticoPorServicioEducativo(this.cursoExtraordinaria.sereduCodigo).subscribe(
            (respuesta:RespuestaEtapaServicioEducativoInterfaz) => {
                this.etapaServicioEducativo = respuesta.listado[0];
            },
            (error:any)=>{
                this.spinner.hide();    
                Swal.fire('Ups! ocurrió un error con el servicio educativo','','error'); 
            }
        );
    }

    obtenerTodasMallaAsignaturasExtraCursoExtraordinariaYGrado() {
        let mallaAsigExtraParametro:MallaAsigExtraParametro={
            docCodigo:this.docente.codPersona,
            curextCodigo:this.cursoExtraordinaria.curextCodigo,
            cupaexCodigo:this.cursoExtraordinaria.cupaexCodigo,
            grado:0
        }
        this._asistenciaService.obtenerTodasMallaAsignaturaExtraPorDocenteCurextCodigoYParalelo(mallaAsigExtraParametro).subscribe(
            (respuesta:RespuestaMallaAsignaturaExtraInterfaz)=>{
                this.listaMallaAsignatura = respuesta.listado;
            },
            (error:any)=>{
                this.spinner.hide();    
                Swal.fire('Ups! ocurrió un error con la malla asignatura','','error'); 
            }
        );
    }
    
    obtenerAsistenciaDiaEstudiantesMatriculados() {
        let matCodigo =  this.listaMatriculaExtraordinario.map((matricula) => matricula.matCodigo);
        let anio = new Date().getFullYear();
        this.listaAsistenciaMatriculaDia = null;
        this.columnasAsistencia = ['num', 'identificacion', 'apellido-nombre'];
        let asistenciaDiaParametros:AsistenciaDiaParametros = {
            listaMatCodigo:matCodigo,
            reanleCodigo:this.cursoExtraordinaria.reanleCodigo,
            curextCodigo:this.cursoExtraordinaria.curextCodigo,
            cupaexCodigo:this.cursoExtraordinaria.cupaexCodigo,
            meCodigo:this.mes.meCodigo,
            anioRegimen:anio};
        this._asistenciaService.obtenerListaAsistenciaDiaPorMatCodigoYCursoExtraordinario(asistenciaDiaParametros).subscribe(
            (respuesta:RespuestaAsistenciaMatriculaDiaInterfaz) => {
                this.spinner.hide();
                this.listaAsistenciaMatriculaDia = respuesta.listado;
                this.listaAsistenciaDia = this.listaAsistenciaMatriculaDia[0].asistenciasDias;
                for(let i=0; i<this.listaAsistenciaMatriculaDia.length;i++) {
                    let matriculaExta:MatriculaExtaordinaria=this.listaMatriculaExtraordinario.find((matriculaExta)=>matriculaExta.matCodigo==this.listaAsistenciaMatriculaDia[i].matCodigo);
                    this.listaAsistenciaMatriculaDia[i].estIdentificacion=matriculaExta.estIdentificacion;
                    this.listaAsistenciaMatriculaDia[i].apellidoEstudiante=matriculaExta.apellidoEstudiante;
                    this.listaAsistenciaMatriculaDia[i].nombreEstudiante=matriculaExta.nombreEstudiante;
                }
                for (let i=0;i<this.listaAsistenciaMatriculaDia[0].asistenciasDias.length;i++) {
                    this.columnasAsistencia.push(this.listaAsistenciaDia[i].fechaNombre);
                }
                this.listaAsistenciaMatriculaDia.sort(function(fObjec, sObjec) {
                    if (fObjec.apellidoEstudiante != null && sObjec.apellidoEstudiante != null) {
                        if (fObjec.apellidoEstudiante < sObjec.apellidoEstudiante) {
                            return -1;
                        }
                        if (fObjec.apellidoEstudiante > sObjec.apellidoEstudiante) {
                            return 1;
                        }
                    } else {
                        if (fObjec.nombreEstudiante < sObjec.nombreEstudiante) {
                            return -1;
                        }
                        if (fObjec.nombreEstudiante > sObjec.nombreEstudiante) {
                            return 1;
                        }
                    }
                    return 0;
                });
                this.dataSourceDia = new MatTableDataSource(this.listaAsistenciaMatriculaDia);
                this.dataSourceDia.paginator = this.paginator;
                this.dataSourceDia.paginator._intl.itemsPerPageLabel="Asignaturas por página";
                this.dataSourceDia.paginator._intl.nextPageLabel="Siguiente";
                this.dataSourceDia.paginator._intl.previousPageLabel="Anterior";
                this.dataSourceDia.sort = this.sort;
            },
            (error:any)=>{
                this.spinner.hide();    
                Swal.fire('Ups! ocurrió un error con la asistencia por día','','error'); 
            }
        );
    }

    obtenerAsistenciaEstudiantesMatriculados(maasiexCodigo:any) {
        this.spinner.show();
        let matCodigo =  this.listaMatriculaExtraordinario.map((matricula) => matricula.matCodigo);
        let matCodigoLimpi = Array.from(new Set(matCodigo));
        this.columnasAsistencia = ['num', 'identificacion', 'apellido-nombre'];
        this.listaAsistenciaMatricula = null;
        this.listaAsistencia = null;
        if (this.fechaInicio == null || this.fechaInicio == undefined) {
            this.spinner.hide();    
            Swal.fire('Ups! Debe seleccionar la fecha de inicio','','warning');
            return;
        }
        if (this.fechaFin == null || this.fechaFin == undefined) {
            this.spinner.hide();
            Swal.fire('Ups! Debe seleccionar la fecha de fin','','warning');
            return;
        }
        if (this.fechaInicio > this.fechaFin) {
            this.spinner.hide();
            Swal.fire('Ups! La fecha inicio tiene que ser menor o igual a la fecha fin','','warning');
            return;
        }

        let asistenciaParametro:AsistenciaParametros = {
            listaMatCodigo:matCodigoLimpi,
            docCodigo:this.docente.codPersona,
            curextCodigo:this.cursoExtraordinaria.curextCodigo,
            cupaexCodigo:this.cursoExtraordinaria.cupaexCodigo,
            maasiexCodig:maasiexCodigo,
            reanleCodigo:this.cursoExtraordinaria.reanleCodigo,
            disextCodigo:this.mallaAsignatura.disextCodigo,
            fechaInicio:this.fechaInicio,
            fechaFin: this.fechaFin,
        };
        this._asistenciaService.obtenerListaAsistenciaPorMatCodigoYCursoExtraordinario(asistenciaParametro).subscribe(
            (respuesta:RespuestaAsistenciaMatriculaInterfaz) => {
                this.spinner.hide();
                this.listaAsistenciaMatricula = respuesta.listado;
                this.listaAsistencia = this.listaAsistenciaMatricula[0].asistencias;
             
                for(let i=0; i<this.listaAsistenciaMatricula.length;i++) {
                    let matriculaExta:MatriculaExtaordinaria=this.listaMatriculaExtraordinario.find((matriculaExta)=>matriculaExta.matCodigo==this.listaAsistenciaMatricula[i].matCodigo);
                    this.listaAsistenciaMatricula[i].estIdentificacion=matriculaExta.estIdentificacion;
                    this.listaAsistenciaMatricula[i].apellidoEstudiante=matriculaExta.apellidoEstudiante;
                    this.listaAsistenciaMatricula[i].nombreEstudiante=matriculaExta.nombreEstudiante;
                }
                for (let i=0;i<this.listaAsistencia.length;i++) {
                    this.columnasAsistencia.push(this.listaAsistencia[i].fechaNombre);
                }
                this.listaAsistenciaMatricula.sort(function(fObjec, sObjec) {
                    if (fObjec.apellidoEstudiante != null && sObjec.apellidoEstudiante != null) {
                        if (fObjec.apellidoEstudiante < sObjec.apellidoEstudiante) {
                            return -1;
                        }
                        if (fObjec.apellidoEstudiante > sObjec.apellidoEstudiante) {
                            return 1;
                        }
                    } else {
                        if (fObjec.nombreEstudiante < sObjec.nombreEstudiante) {
                            return -1;
                        }
                        if (fObjec.nombreEstudiante > sObjec.nombreEstudiante) {
                            return 1;
                        }
                    }
                    return 0;
                });
                this.dataSource = new MatTableDataSource(this.listaAsistenciaMatricula);
                this.dataSource.paginator = this.paginator;
                this.dataSource.paginator._intl.itemsPerPageLabel="Asignaturas por página";
                this.dataSource.paginator._intl.nextPageLabel="Siguiente";
                this.dataSource.paginator._intl.previousPageLabel="Anterior";
                this.dataSource.sort = this.sort;
                this.spinner.hide();
            },
            (error:any)=>{
                this.spinner.hide();    
                Swal.fire('Ups! ocurrió un error las asignaturas de los estudiantes','','error'); 
            }
        );
    }

    guardarAsistencia(asistencia:Asistencia) {
        this.spinner.show();
        this._asistenciaService.guardarAsistencia(asistencia).subscribe(
            (respuesta:any) => {
                this.spinner.hide();
            },
            (error:any)=>{
                this.spinner.hide();    
                Swal.fire('Ups! ocurrió un error al guardar la asistencia.','','error'); 
            }
        );
    }

    guardarAsistenciaDia(asistenciaDia:AsistenciaDia) {
        this.spinner.show();
        this._asistenciaService.guardarAsistenciaDia(asistenciaDia).subscribe(
            (respuesta:any) => {
                this.spinner.hide();
            },
            (error:any)=>{
                this.spinner.hide();    
                Swal.fire('Ups! ocurrió un error al guardar la asistencia día.','','error'); 
            }
        );
    }

    limpiarDatosConFechaInicio() {
        this.listaAsistencia = null;
        this.listaAsistenciaMatricula = null;
        this.mallaAsignatura = null;
        this.fechaFin = null;
    }

    limpiarDatosConFechaFin() {
        this.listaAsistenciaMatricula = null;
        this.mallaAsignatura = null;
    }

    volverAsistenciaPeriodo() {
        this.router.navigate(['pages/asistencia-periodo-lectivo']);
    }

    applyFilter(event: Event) {
        const filterValue = (event.target as HTMLInputElement).value;
        this.dataSource.filter = filterValue.trim().toLowerCase();
       
        if (this.dataSource.paginator) {
           this.dataSource.paginator.firstPage();
        }
    }

    applyFilterDia(event: Event) {
        const filterValue = (event.target as HTMLInputElement).value;
        this.dataSourceDia.filter = filterValue.trim().toLowerCase();
       
        if (this.dataSourceDia.paginator) {
           this.dataSourceDia.paginator.firstPage();
        }
     }

}