import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AsistenciaPeriodoLectivoComponent } from './asistencia-periodo-lectivo.component';

describe('AsistenciaComponent', () => {
  let component: AsistenciaPeriodoLectivoComponent;
  let fixture: ComponentFixture<AsistenciaPeriodoLectivoComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AsistenciaPeriodoLectivoComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AsistenciaPeriodoLectivoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});