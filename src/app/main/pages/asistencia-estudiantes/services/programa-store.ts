import { Injectable } from "@angular/core";
import { Docente } from "../../malla-asignatura-extra/interfaces/docente";
import { CursoExtraordinaria } from "../interfaces/curso-extraordinaria";

@Injectable()
export class ProgramaStore {
    cursoExtraordinaria:CursoExtraordinaria;
    docente:Docente;
}