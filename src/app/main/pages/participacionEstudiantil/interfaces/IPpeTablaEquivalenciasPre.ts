export interface IPpeTablaEquivalenciasPre {
  ptequiCodigo?: number
  ptequiNroInicio: number
  ptequiNroFin: number
  ptequiCalif: number
  ptequiDescrip: string
  ptequiEstado: number
}
