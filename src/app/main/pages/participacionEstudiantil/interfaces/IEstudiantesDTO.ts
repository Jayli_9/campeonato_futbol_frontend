export interface IEstudiantesDTO {
  id?: number
  curCodigo?: number
  graCodigo?: number
  graDescripcion?: string
  idEstudiante?: number
  idParalelo?: number
  idPeriodo?: number
  identificacionEstudiante?: string
  nombreApellidoEstudiante?: string
}
