import { IPpeCargos } from "./IPpeCargos";
import { IPpeSubDenominacion } from "./IPpeSubdenominacion";

export interface IPpeDistributivoBuenVivir {
    dbuCodigo?: number;
    docCodigo: number;
    parCodigo: number;
    dbuEstado: number;
    ppeCargo: IPpeCargos;
    ppeSubdenominacion: IPpeSubDenominacion;
    crgHoras: number;
}