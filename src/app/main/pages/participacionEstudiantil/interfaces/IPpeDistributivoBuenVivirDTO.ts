export interface IPpeDistributivoBuenVivirDTO {
    dbuCodigo?: number;
    docCodigo: number;
    parCodigo: number;
    dbuEstado: number;
    ppeCargo: number;
    ppeSubdenominacion: number;
    crgHoras: number;
}