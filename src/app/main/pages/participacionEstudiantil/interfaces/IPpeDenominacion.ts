import { IPpeParticipacion } from "./IPpeParticipacion";

export interface IPpeDenominacion {
    pdenoCodigo: number;
    ppeParticipacion: IPpeParticipacion;
    pdenoDescripcion: string;
    pdenoEstado: number;

}