export interface IPpeCargos {
    crgCodigo?: number;
    crgDescripcion: string;
    crgHoras: number;
    crgEstado: number;
}