import { IPpeInscripcionSubdeno } from "./IPpeInscripcionSubdeno";
import { IPpeTablaEquivalenciasPre } from "./IPpeTablaEquivalenciasPre";

export interface IPpeRegistroAsisCalif {
    pregasisCodigo?: number;
    pregasisNroHoras: number;
    reanleCodigo: number;
    matsemCodigo?: number;
    matpreCodigo: number;
    pregasisFecCreacion: Date;
    pregasisEstado: number;
    ppeInscripcionSubdeno: IPpeInscripcionSubdeno;
    ppeTablaEquivalenciasPre: IPpeTablaEquivalenciasPre;


}