export interface InscripcionSubdenoDTO {
  pinsCodigo?: number
  insCodigo: number
  estCodigo: number
  matsemCodigo?: number
  matpreCodigo?: number
  parCodigo: number
  sereduCodigo: number
  reanleCodigo: number
  pinsFecCreacion: Date
  pinsEstado: number
  ppeSubdenominacion: number
}
