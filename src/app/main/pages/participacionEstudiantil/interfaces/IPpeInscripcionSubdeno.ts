import { IPpeSubDenominacion } from "./IPpeSubdenominacion";

export interface IPpeInscripcionSubdeno{
    pinsCodigo?: number;
    ppeSubdenominacion?: IPpeSubDenominacion;
    insCodigo: number;
    estCodigo: number;
    matsemCodigo?: number;
    matpreCodigo: number;
    parCodigo: number;
    sereduCodigo: number;
    reanleCodigo: number;
    pinsFecCreacion: Date;
    pinsEstado: number;

}