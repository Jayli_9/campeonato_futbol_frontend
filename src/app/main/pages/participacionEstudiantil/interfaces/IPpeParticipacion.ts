export interface IPpeParticipacion {
    pparCodigo: number;
    pparDescripcion: string;
    pparEstado: number;
}