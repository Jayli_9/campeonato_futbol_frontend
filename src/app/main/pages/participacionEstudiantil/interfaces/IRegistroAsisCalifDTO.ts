export interface IRegistroAsisCalifDTO {
  pregasisCodigo?: number;
  pregasisNroHoras?: number;
  reanleCodigo: number;
  matsemCodigo?: number;
  matpreCodigo?: number;
  pregasisFecCreacion: Date;
  pregasisEstado: number;
  ppeInscripcionSubdeno: number;
  ppeTablaEquivalenciasPre?: number;
  ppeTablaEquivalenciasSemi?: number;
  ppeTablaEquivalenciasDistan?: number;
}
