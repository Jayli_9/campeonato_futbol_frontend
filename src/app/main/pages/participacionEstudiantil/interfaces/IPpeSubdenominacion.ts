import { IPpeDenominacion } from "./IPpeDenominacion";

export interface IPpeSubDenominacion {
    psdenCodigo: number;
    psdenDescripcion: string;
    psdenEstado: number;
    ppeDenominacion: IPpeDenominacion;
}