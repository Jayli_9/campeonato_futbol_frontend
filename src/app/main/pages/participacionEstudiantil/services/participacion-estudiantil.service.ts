import { HttpClient } from "@angular/common/http"
import { Injectable } from "@angular/core"
import { AuthenticationService } from "app/auth/service"
import { ResponseGenerico } from "../../malla-asignatura-extra/interfaces/response-generico"
import { environment } from "environments/environment"
import { IofeCurso } from "../../asignacionDocentesOrdinaria/interfaces/IofeCurso"
import { IEstudiantesDTO } from "../interfaces/IEstudiantesDTO"
import { IPpeDenominacion } from "../interfaces/IPpeDenominacion"
import { IofeParalelo } from "../../calificacionOrdinaria/interfaces/IofeParalelo"
import { IPpeSubDenominacion } from "../interfaces/IPpeSubdenominacion"
import { InscripcionSubdenoDTO } from "../interfaces/InscripcionSubdenoDTO"
import { IPpeInscripcionSubdeno } from "../interfaces/IPpeInscripcionSubdeno"
import { IPpeCargos } from "../interfaces/IPpeCargos"
import { IPpeDistributivoBuenVivirDTO } from "../interfaces/IPpeDistributivoBuenVivirDTO"
import { IPpeTablaEquivalenciasPre } from "../interfaces/IPpeTablaEquivalenciasPre"
import { IRegistroAsisCalifDTO } from "../interfaces/IRegistroAsisCalifDTO"
import { IPpeRegistroAsisCalif } from "../interfaces/IPpeRegistroAsisCalif"
import { IPpeDistributivoBuenVivir } from "../interfaces/IPpeDistributivoBuenVivir"

@Injectable({
  providedIn: "root",
})
export class ParticipacionEstudiantilService {
  constructor(
    private _http: HttpClient,
    private _authService: AuthenticationService
  ) {}

  getEstudiantesMatriculaOrdinaria(
    reanleCodigo: number,
    curCodigo: number
  ): Promise<ResponseGenerico<IEstudiantesDTO>> {
    return new Promise((resolve, reject) => {
      this._http
        .get(
          `${environment.url_matricula}/private/listaMatriculaOrdinariaEstudiante/${reanleCodigo}/${curCodigo}`
        )
        .subscribe((response: ResponseGenerico<IEstudiantesDTO>) => {
          resolve(response)
        }, reject)
    })
  }

  getEstudiante(estCodigo: number): Promise<ResponseGenerico<any>> {
    return new Promise((resolve, reject) => {
      this._http
        .get(
          `${environment.url_matricula}/private/buscarPorEstCodigo/${estCodigo}/`
        )
        .subscribe((response: ResponseGenerico<any>) => {
          resolve(response)
        }, reject)
    })
  }

  getCursosPorEstablecimientoAndAnioLectivo(
    codigoEstablecimiento: number,
    codigoRegistroAnioLectivo: number
  ): Promise<ResponseGenerico<IofeCurso>> {
    return new Promise((resolve, reject) => {
      this._http
        .post(
          `${environment.url_oferta}/private/listarCursosPorEstablecimientoAndCodigoRegistroAnioLectivo/`,
          { codigoEstablecimiento, codigoRegistroAnioLectivo }
        )
        .subscribe((response: ResponseGenerico<IofeCurso>) => {
          resolve(response)
        }, reject)
    })
  }

  getDenominaciones(): Promise<ResponseGenerico<IPpeDenominacion>> {
    return new Promise((resolve, reject) => {
      this._http
        .get(`${environment.url_academico}/private/listarDenominacion/`)
        .subscribe((response: ResponseGenerico<IPpeDenominacion>) => {
          resolve(response)
        }, reject)
    })
  }

  getSubDenominaciones(): Promise<ResponseGenerico<IPpeSubDenominacion>> {
    return new Promise((resolve, reject) => {
      this._http
        .get(`${environment.url_academico}/private/listarSubdenominacion/`)
        .subscribe((response: ResponseGenerico<IPpeSubDenominacion>) => {
          resolve(response)
        }, reject)
    })
  }

  getParalelos(): Promise<ResponseGenerico<IofeParalelo>> {
    return new Promise((resolve, reject) => {
      this._http
        .get(`${environment.url_oferta}/private/listarParalelo/`)
        .subscribe((response: ResponseGenerico<IofeParalelo>) => {
          resolve(response)
        }, reject)
    })
  }

  getMatriculaOrdinariaEstudiante(
    matCodigo: number
  ): Promise<ResponseGenerico<any>> {
    return new Promise((resolve, reject) => {
      this._http
        .get(
          `${environment.url_matricula}/private/buscarMatriculaOrdinariaPorCodigo/${matCodigo}/`
        )
        .subscribe((response: ResponseGenerico<any>) => {
          resolve(response)
        }, reject)
    })
  }

  getCargos(): Promise<ResponseGenerico<IPpeCargos>> {
    return new Promise((resolve, reject) => {
      this._http
        .get(`${environment.url_academico}/private/listarCargos`)
        .subscribe((response: ResponseGenerico<IPpeCargos>) => {
          resolve(response)
        }, reject)
    })
  }

  getTablaEquivalencias(): Promise<
    ResponseGenerico<IPpeTablaEquivalenciasPre>
  > {
    return new Promise((resolve, reject) => {
      this._http
        .get(`${environment.url_academico}/private/listarTablaEquivalenciasPre`)
        .subscribe((response: ResponseGenerico<IPpeTablaEquivalenciasPre>) => {
          resolve(response)
        }, reject)
    })
  }

  guardarInscripcionPPE(
    inscripcion: InscripcionSubdenoDTO
  ): Promise<ResponseGenerico<IPpeInscripcionSubdeno>> {
    return new Promise((resolve, reject) => {
      this._http
        .post(
          `${environment.url_academico}/private/guardarInscripcionSubdeno`,
          inscripcion
        )
        .subscribe((response: ResponseGenerico<IPpeInscripcionSubdeno>) => {
          resolve(response)
        }, reject)
    })
  }

  guardarDistributivoBuenVivirPPE(
    distriBuenVivir: IPpeDistributivoBuenVivirDTO[]
  ): Promise<ResponseGenerico<any>> {
    return new Promise((resolve, reject) => {
      this._http
        .post(
          `${environment.url_academico}/private/guardarDistriBuenVivirList`,
          distriBuenVivir
        )
        .subscribe((response: ResponseGenerico<any>) => {
          resolve(response)
        }, reject)
    })
  }

  getInscripcionesSubdenoPorSubdenoCodigoAndParCodigo(
    psdenCodigo: number,
    parCodigo: number
  ): Promise<ResponseGenerico<IPpeInscripcionSubdeno>> {
    return new Promise((resolve, reject) => {
      this._http
        .post(
          `${environment.url_academico}/private/listarInscripcionesSubdenoPorSubdenoCodigoAndParCodigo/`,
          { psdenCodigo, parCodigo }
        )
        .subscribe((response: ResponseGenerico<IPpeInscripcionSubdeno>) => {
          resolve(response)
        }, reject)
    })
  }

  guardarRegistroAsisCalifList(
    registroAsisCalifDTO: IRegistroAsisCalifDTO[]
  ): Promise<ResponseGenerico<String>> {
    return new Promise((resolve, reject) => {
      this._http
        .post(
          `${environment.url_academico}/private/guardarRegistroAsisCalifList`,
          registroAsisCalifDTO
        )
        .subscribe((response: ResponseGenerico<String>) => {
          resolve(response)
        }, reject)
    })
  }

  listarRegistroAsisCalifListPorMatPresencial(
    matPresencial: number[]
  ): Promise<ResponseGenerico<IPpeRegistroAsisCalif>> {
    return new Promise((resolve, reject) => {
      this._http
        .post(
          `${environment.url_academico}/private/buscarRegistrosAsisCalifPorMatriculaPresencial`,
          matPresencial
        )
        .subscribe((response: ResponseGenerico<IPpeRegistroAsisCalif>) => {
          resolve(response)
        }, reject)
    })
  }

  listarDistriBuenvivirPorDocCodigo(
    docCodigo: number
  ): Promise<ResponseGenerico<IPpeDistributivoBuenVivir>> {
    return new Promise((resolve, reject) => {
      this._http
        .get(
          `${environment.url_academico}/private/buscarDistriBuenvivirPorDocCodigo/${docCodigo}`
        )
        .subscribe((response: ResponseGenerico<IPpeDistributivoBuenVivir>) => {
          resolve(response)
        }, reject)
    })
  }

  getDocentePorCedula(identificacion: string): Promise<ResponseGenerico<any>> {
    return new Promise((resolve, reject) => {
      this._http
        .get(
          `${environment.url_docente}/private/docenteHispanaPorIdentificacionYEstado/${identificacion}`
        )
        .subscribe((response: ResponseGenerico<any>) => {
          resolve(response)
        }, reject)
    })
  }

  getInscripcionPorMatpreCodigo(
    matpreCodigo: number
  ): Promise<ResponseGenerico<IPpeInscripcionSubdeno>> {
    return new Promise((resolve, reject) => {
      this._http
        .get(
          `${environment.url_academico}/private/buscarInscripcionSubdenoPorMatpreCodigo/${matpreCodigo}`
        )
        .subscribe((response: ResponseGenerico<IPpeInscripcionSubdeno>) => {
          resolve(response)
        }, reject)
    })
  }

  activarInactivarInscripcion(codigo: number): Promise<any> {
    return new Promise((resolve, reject) => {
      this._http
        .get(
          `${environment.url_academico}/private/cambiarEstadoInscripcionPorId/${codigo}`
        )
        .subscribe((response: any) => {
          resolve(response)
        }, reject)
    })
  }
  
}
