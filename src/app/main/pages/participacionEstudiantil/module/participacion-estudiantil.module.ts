import { CommonModule } from "@angular/common"
import { NgModule } from "@angular/core"
import { FormsModule, ReactiveFormsModule } from "@angular/forms"
import { RouterModule } from "@angular/router"
import { MaterialModule } from "app/main/shared/material/material.module"
import { NgxSpinnerModule } from "ngx-spinner"
import { MatAutocompleteModule } from "@angular/material/autocomplete"
import { MatCheckboxModule } from "@angular/material/checkbox"
import { MatIconModule } from "@angular/material/icon"
import { MatDividerModule } from "@angular/material/divider"
import { MatSelectModule } from "@angular/material/select"
import { MatFormFieldModule } from "@angular/material/form-field"
import { MatDialogModule } from "@angular/material/dialog"
import { AsignacionParticipacionComponent } from "../components/coordinacion-participacion/asignacion-participacion/asignacion-participacion.component"
import { ModalInscripcionComponent } from "../components/coordinacion-participacion/modal-inscripcion/modal-inscripcion.component"
import { CoordinacionParticipacionComponent } from "../components/coordinacion-participacion/coordinacion-participacion.component"
import { RUTAS_PPE } from "../routes/rutas-ppe.routing"
import { AsignacionDocenteComponent } from "../components/docente-participacion/seleccion-docente/asignacion-docente/asignacion-docente.component"
import { SeleccionDocenteComponent } from "../components/docente-participacion/seleccion-docente/seleccion-docente.component"
import { AsistenciaParticipacionComponent } from "../components/asistencia-participacion/asistencia-participacion.component"
import { ModalInscripcionEditarComponent } from "../components/coordinacion-participacion/modal-inscripcion-editar/modal-inscripcion-editar.component"

@NgModule({
  declarations: [
    CoordinacionParticipacionComponent,
    AsignacionParticipacionComponent,
    ModalInscripcionComponent,
    ModalInscripcionEditarComponent,
    SeleccionDocenteComponent,
    AsignacionDocenteComponent,
    AsistenciaParticipacionComponent
  ],
  imports: [
    CommonModule,
    RouterModule.forChild(RUTAS_PPE),
    MaterialModule,
    FormsModule,
    ReactiveFormsModule,
    MatAutocompleteModule,
    MatCheckboxModule,
    MatIconModule,
    MatDividerModule,
    MatSelectModule,
    MatFormFieldModule,
    NgxSpinnerModule,
    MatDialogModule,
  ],
})
export class ParticipacionEstudiantilModule {}
