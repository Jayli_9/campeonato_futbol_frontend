import { Routes } from "@angular/router"
import { AuthGuard } from "app/auth/helpers"
import { CoordinacionParticipacionComponent } from "../components/coordinacion-participacion/coordinacion-participacion.component"
import { SeleccionDocenteComponent } from "../components/docente-participacion/seleccion-docente/seleccion-docente.component"
import { AsistenciaParticipacionComponent } from "../components/asistencia-participacion/asistencia-participacion.component"

export const RUTAS_PPE: Routes = [
  {
    path: "inscripcion-ppe",
    component: CoordinacionParticipacionComponent,
    canActivate: [AuthGuard],
  },
  {
    path: "asignacion-docentes-ppe",
    component: SeleccionDocenteComponent,
    canActivate: [AuthGuard],
  },
  {
    path: "calificacion-asistencia-ppe",
    component: AsistenciaParticipacionComponent,
    canActivate: [AuthGuard],
  },
]
