import { Component, Inject, OnInit } from "@angular/core"
import { UntypedFormControl, UntypedFormGroup, Validators } from "@angular/forms"
import { ParticipacionEstudiantilService } from "../../../services/participacion-estudiantil.service"
import { IofeParalelo } from "app/main/pages/calificacionOrdinaria/interfaces/IofeParalelo"
import { IPpeDenominacion } from "../../../interfaces/IPpeDenominacion"
import { MatRadioChange } from "@angular/material/radio"
import { MAT_DIALOG_DATA, MatDialogRef } from "@angular/material/dialog"
import { IPpeSubDenominacion } from "../../../interfaces/IPpeSubdenominacion"
import { MatSelectChange } from "@angular/material/select"

@Component({
  selector: "app-modal-inscripcion",
  templateUrl: "./modal-inscripcion.component.html",
  styleUrls: ["./modal-inscripcion.component.scss"],
})
export class ModalInscripcionComponent {
  public inscripcionForm: UntypedFormGroup
  public paralelos: IofeParalelo[]
  private denominaciones: IPpeDenominacion[]
  private subDenominaciones: IPpeSubDenominacion[]
  public currentDenominaciones: IPpeDenominacion[]
  public currentSubDenominaciones: IPpeSubDenominacion[]

  constructor(
    private participacionEstudiantilService: ParticipacionEstudiantilService,
    public dialogRef: MatDialogRef<ModalInscripcionComponent>,
    @Inject(MAT_DIALOG_DATA) public inscripcion: any
  ) {
    this.initComponents()
    this.listarInformacion()
  }

  async listarInformacion() {
    await this.listarParalelos()
    await this.listarDenominaciones()
    await this.listarSubDenominaciones()
    await this.setCurrentDenominaciones(
      parseInt(this.inscripcionForm.get("areaAccion").value)
    )
  }

  async listarParalelos() {
    try {
      this.paralelos = (
        await this.participacionEstudiantilService.getParalelos()
      ).listado
    } catch (error) {
      console.log(error)
    }
  }

  async listarDenominaciones() {
    try {
      this.denominaciones = (
        await this.participacionEstudiantilService.getDenominaciones()
      ).listado
    } catch (error) {
      console.log(error)
    }
  }
  async listarSubDenominaciones() {
    try {
      this.subDenominaciones = (
        await this.participacionEstudiantilService.getSubDenominaciones()
      ).listado
    } catch (error) {
      console.log(error)
    }
  }

  handlerParticipacion(event: MatRadioChange) {
    this.resetForm()
    this.setCurrentDenominaciones(parseInt(event.value))
    this.currentSubDenominaciones = []
  }

  handlerDenominacion(event: MatSelectChange) {
    this.setCurrentSubDenominaciones(parseInt(event.value))
  }

  setCurrentDenominaciones(pparCodigo: number) {
    this.currentDenominaciones = this.denominaciones.filter(
      (denominacion) => denominacion.ppeParticipacion.pparCodigo === pparCodigo
    )
  }

  setCurrentSubDenominaciones(pdenoCodigo: number) {
    this.currentSubDenominaciones = this.subDenominaciones.filter(
      (subDenominacion) =>
        subDenominacion.ppeDenominacion.pdenoCodigo === pdenoCodigo
    )
  }

  guardar() {
    if (this.inscripcionForm.status === "VALID") {
      this.dialogRef.close(this.inscripcionForm.value)
    } else {
      //
    }
  }

  initComponents() {
    this.paralelos = []
    this.denominaciones = []
    this.subDenominaciones = []
    this.currentDenominaciones = []
    this.currentSubDenominaciones = []
    this.inscripcionForm = new UntypedFormGroup({
      areaAccion: new UntypedFormControl("1", [Validators.required]),
      denominacion: new UntypedFormControl("", [Validators.required]),
      subDenominacion: new UntypedFormControl("", [Validators.required]),
      paralelo: new UntypedFormControl("", [Validators.required]),
    })
  }

  resetForm(){
    this.inscripcionForm.get("denominacion").setValue(null)
    this.inscripcionForm.get("subDenominacion").setValue(null)
    this.inscripcionForm.get("paralelo").setValue(null)
    this.inscripcionForm.get("denominacion").markAsUntouched()
    this.inscripcionForm.get("subDenominacion").markAsUntouched()
    this.inscripcionForm.get("paralelo").markAsUntouched()
  }

  getErrorMessage(field: string) {
    return this.inscripcionForm.get(field).hasError("required")
      ? "Campo requerido"
      : this.inscripcionForm.get(field).hasError("email")
      ? "Correo inválido"
      : this.inscripcionForm.get(field).hasError("maxlength")
      ? "longitud incorrecta"
      : this.inscripcionForm.get(field).hasError("minlength")
      ? "longitud incorrecta"
      : ""
  }
}
