import { ChangeDetectorRef, Component, OnInit, ViewChild } from "@angular/core"
import { GieeService } from "app/main/pages/asignacionDocentes/servicios/giee.service"
import { NgxSpinnerService } from "ngx-spinner"
import { UntypedFormControl } from "@angular/forms"
import { Observable } from "rxjs"
import { MatTableDataSource } from "@angular/material/table"
import { MatPaginator, MatPaginatorIntl } from "@angular/material/paginator"
import { MatSort } from "@angular/material/sort"
import { IinsInstitucion } from "app/main/pages/asignacionDocentesOrdinaria/interfaces/IinsInstitucion"
import { IinsNivelPermisoInstitucionCruce } from "app/main/pages/asignacionDocentesOrdinaria/interfaces/IinsNivelPermisoInstitucionCruce"
import { IcatDocenteHispana } from "app/main/pages/asignacionDocentesOrdinaria/interfaces/IcatDocenteHispana"
import { User } from "app/auth/models"
import { CatEstablecimiento } from "app/main/pages/asignacionDocentes/interface/cat_establecimiento"
import { ParticipacionEstudiantilService } from "../../services/participacion-estudiantil.service"
import { IofeCurso } from "app/main/pages/asignacionDocentesOrdinaria/interfaces/IofeCurso"
import { IEstudiantesDTO } from "../../interfaces/IEstudiantesDTO"
import { MatSelectChange } from "@angular/material/select"
import { MensajesService } from "app/main/pages/asignacionDocentesOrdinaria/servicios/mensajes.service"
import { CatalogoService as CatComunService } from "app/main/shared/services/catalogo.service"
import { CatalogoService } from "app/main/pages/asignacionDocentes/servicios/catalogo.service"
import { CatGrado } from "app/main/shared/interfaces/cat-grado"
import { OfertaService } from "app/main/pages/asignacionTutor/servicios/oferta.service"
import { cursoInterface } from "app/main/pages/reporteDocente/interfaces/curso"
import { MatDialog } from "@angular/material/dialog"
import { ModalInscripcionComponent } from "./modal-inscripcion/modal-inscripcion.component"
import { InscripcionSubdenoDTO } from "../../interfaces/InscripcionSubdenoDTO"
import { CatPoblacionObjetivo } from "app/main/shared/interfaces/cat-poblacion-objetivo"
import { IPpeInscripcionSubdeno } from "../../interfaces/IPpeInscripcionSubdeno"
import { ModalInscripcionEditarComponent } from "./modal-inscripcion-editar/modal-inscripcion-editar.component"

@Component({
  selector: "app-coordinacion-participacion",
  templateUrl: "./coordinacion-participacion.component.html",
  styleUrls: ["./coordinacion-participacion.component.scss"],
})
export class CoordinacionParticipacionComponent implements OnInit {
  displayedColumns: string[] = [
    "identificacionEstudiante",
    "nombreApellidoEstudiante",
    "graDescripcion",
    "accion",
  ]
  public dataSource: MatTableDataSource<any> = new MatTableDataSource()

  @ViewChild(MatPaginator) paginator!: MatPaginator
  @ViewChild(MatSort) sort!: MatSort

  public estudiante: IEstudiantesDTO = {}
  public educacionFormalSeleccionado: number = null
  public isFormSubmitted: boolean = true
  public opcionBusqueda: boolean = true
  public reanleCodigo: number = null
  public institucion: IinsInstitucion = null
  public seleccionNivel: boolean = false
  public inscripcion: IPpeInscripcionSubdeno = null
  public userData: User
  public AMIE: string
  public informacionInstitucion: any
  public codInstitucion: number
  public codRegimen: number
  private listOferta: IofeCurso[]
  private estudianteMatriculaOrdinaria: any

  //listas
  public listaCruzadaNivelPermisoInstitucion: IinsNivelPermisoInstitucionCruce[] =
    []
  public establecimientos: CatEstablecimiento[]
  private listaEstudiantes: IEstudiantesDTO[]
  private listaGrados: CatGrado[]
  public listaPoblacionObjetivo: CatPoblacionObjetivo[]

  constructor(
    private catalogoService: CatalogoService,
    private catalogService: CatComunService,
    private GieeService: GieeService,
    private spinnerService: NgxSpinnerService,
    private participacionEstudiantilService: ParticipacionEstudiantilService,
    private matPaginatorIntl: MatPaginatorIntl,
    private mensajeService: MensajesService,
    private changeDetectorRefs: ChangeDetectorRef,
    private ofertaService: OfertaService,
    public dialog: MatDialog
  ) {
    this.isFormSubmitted = true
    this.listaEstudiantes = []
    this.listaGrados = []
    this.estudianteMatriculaOrdinaria = null
    this.listaPoblacionObjetivo = []
  }

  myControl = new UntypedFormControl()
  opcionesDocentes: Observable<IcatDocenteHispana[]>

  displayFn(estudiante: IEstudiantesDTO): string {
    return estudiante && estudiante.nombreApellidoEstudiante
      ? estudiante.nombreApellidoEstudiante
      : ""
  }

  async ngOnInit(): Promise<void> {
    this.spinnerService.show()
    this.userData = JSON.parse(localStorage.getItem("currentUser"))
    this.AMIE = this.userData.sede.nemonico
    //listar grados
    await this.listarGrados()
    //listar población objetivo
    await this.listarPoblacionObjetivo()
    //buscar la institucion y regimen
    await this.buscarRegiment(this.AMIE)
    //buscar Establecimiento de la institucion
    await this.obtenerEstablecimientos(this.AMIE)
    // getCursosPorEstablecimientoAndAnioLectivo
    await this.listarEstudiantes()
    this.spinnerService.hide()
  }

  handlerEduFormal(e: MatSelectChange) {}

  //buscar los datos de la institucion y regimen
  async buscarRegiment(amie: string) {
    //buscar la institucion
    await this.GieeService.getDatosInstitucion(amie).then(
      async (data) => {
        this.informacionInstitucion = data.objeto
        this.codInstitucion = this.informacionInstitucion.insCodigo
        this.codRegimen = this.informacionInstitucion.regimenes[0].regCodigo

        //listar regimen Año lectivo
        await this.catalogoService
          .getAnioElectivo(this.codRegimen)
          .then((data) => {
            data.listado.forEach((dato) => {
              if (dato.reanleTipo === "A") {
                this.reanleCodigo = dato.reanleCodigo
              }
            })
          })
      },
      (error) => {
        console.log(error)
        this.spinnerService.hide()
      }
    )
  }

  async obtenerEstudiantePorIdentificacion() {
    this.spinnerService.show()

    let estFind: IEstudiantesDTO = this.listaEstudiantes.find(
      (estudiante: IEstudiantesDTO) =>
        estudiante.identificacionEstudiante ===
        this.estudiante.identificacionEstudiante
    )
    if (!!estFind) {
      await this.buscarMatriculaOrdinariaEstudiante(estFind.id)
      await this.buscarInscripcionPorMatpreCodigo(
        this.estudianteMatriculaOrdinaria.matCodigo
      )
      this.renderEstudiante(await this.asignarGradoEstudiante(estFind))
    } else {
      this.mensajeService.mensajeAdvertencia(
        "Advertencia",
        "No se encuentra al estudiante."
      )
    }

    this.spinnerService.hide()
  }

  renderEstudiante(estudiante: IEstudiantesDTO) {
    const estudianteTemp = {
      ...estudiante,
      pinsCodigo: !!this.inscripcion ? this.inscripcion.pinsCodigo : null,
    }
    this.dataSource = new MatTableDataSource()
    this.dataSource.data = [estudianteTemp]
    this.matPaginatorIntl.itemsPerPageLabel = "Elementos por página:"
    this.matPaginatorIntl.nextPageLabel = "Siguiente:"
    this.matPaginatorIntl.previousPageLabel = "Anterior:"
    this.changeDetectorRefs.detectChanges()
  }

  reiniciarValores() {
    this.estudiante = {}
    this.reiniciarDatosTabla(this.estudiante)
  }

  //sacar los establecimientos de la institucion
  async obtenerEstablecimientos(amie: string) {
    await this.GieeService.getEstablecimientoPorAmie(amie)
      .then((data) => {
        this.establecimientos = data.listado
      })
      .catch((error) => {
        console.log(error)
        this.spinnerService.hide()
      })
  }

  //validar solo ingreso de numeros
  validateFormat(event) {
    let key
    if (event.type === "paste") {
      key = event.clipboardData.getData("text/plain")
    } else {
      key = event.keyCode
      key = String.fromCharCode(key)
    }
    const regex = /[0-9]|\./
    if (!regex.test(key)) {
      event.returnValue = false
      if (event.preventDefault) {
        event.preventDefault()
      }
    }
  }

  actualizarAsignacionOutput(event) {
    // this.seleccionNivel = event
    //this.reiniciarDatosTabla();
  }

  asignarInscripcion(estudiante: IEstudiantesDTO) {
    this.buscarMatriculaOrdinariaEstudiante(estudiante.id)
    const dialogRef = this.dialog.open(ModalInscripcionComponent, {
      width: "600px",
    })
    dialogRef.afterClosed().subscribe((result: any) => {
      if (!!result) {
        this.guardar(result)
      }
    })
  }

  editarInscripcion(estudiante: IEstudiantesDTO) {
    this.buscarMatriculaOrdinariaEstudiante(estudiante.id)
    const dialogRef = this.dialog.open(ModalInscripcionEditarComponent, {
      width: "600px",
      data: {
        estudiante: estudiante,
        inscripcion: this.inscripcion,
      },
    })
    dialogRef.afterClosed().subscribe((result: any) => {
      if (!!result) {
        this.guardar(result)
      }
    })
  }

  async inactivarInscripcion() {
    try {
      await this.participacionEstudiantilService.activarInactivarInscripcion(
        this.inscripcion.pinsCodigo
      )
      this.reiniciarValores()
      this.mensajeService.mensajeCorrecto("Éxito", "Estado cambiado")
    } catch (error) {
      this.mensajeService.mensajeError("Error", "Estado no cambiado")
    }
  }

  async listarEstudiantes() {
    this.listaEstudiantes = []

    await this.participacionEstudiantilService
      .getCursosPorEstablecimientoAndAnioLectivo(
        this.establecimientos[0].insestCodigo,
        // this.reanleCodigo
        47
      )
      .then(async (res) => {
        this.listOferta = res.listado

        this.listOferta.forEach((oferta) => {
          const match = this.listaGrados.find(
            (grado) => grado.graCodigo === oferta.graCodigo
          )
          if (match) {
            oferta.nivCodigo = match.nivCodigo
          }
        })
        let filtrados: IofeCurso[] = this.listOferta.filter(
          (oferta) => oferta.nivCodigo === 7 || oferta.nivCodigo === 8
        )

        let cursosPromesas = filtrados.map(async (curso) => {
          return await this.participacionEstudiantilService.getEstudiantesMatriculaOrdinaria(
            this.reanleCodigo,
            curso.curCodigo
          )
        })

        let cursosResultados = await Promise.all(cursosPromesas)
        cursosResultados.forEach((curso) => {
          this.listaEstudiantes = [...this.listaEstudiantes, ...curso.listado]
        })

        let estudiantesFiltrados = this.eliminarRepetidos(
          this.listaEstudiantes,
          "idEstudiante"
        )
        this.listaEstudiantes = [...estudiantesFiltrados]
      })
      .catch((error) => {
        console.log(error)
        this.spinnerService.hide()
      })
  }

  eliminarRepetidos(array: any[], clave: string) {
    return array.filter(
      (elemento, indice, self) =>
        indice === self.findIndex((objeto) => objeto[clave] === elemento[clave])
    )
  }

  onInputEvent(event: any) {
    // Aquí puedes manejar el evento input como desees
    this.listaCruzadaNivelPermisoInstitucion = []
    // Puedes realizar acciones adicionales, como buscar y filtrar datos en función de lo que se escribió en el campo de entrada.
  }

  reiniciarDatosTabla(estudiante: IEstudiantesDTO) {
    this.dataSource.data = [
      !!estudiante?.identificacionEstudiante
        ? this.asignarGradoEstudiante(estudiante)
        : {},
    ]
    this.changeDetectorRefs.detectChanges()
  }

  async listarGrados() {
    await this.catalogService
      .listarGrados()
      .then((res) => {
        this.listaGrados = res.listado
      })
      .catch((error) => {
        console.log("error al listar los grados: ", error)
      })
  }

  async listarPoblacionObjetivo() {
    await this.catalogService
      .listarPoblacionObjetivo()
      .then((res) => {
        this.listaPoblacionObjetivo = res.listado
      })
      .catch((error) => {
        console.log("error al listar la población objetivo: ", error)
      })
  }

  async buscarCursoPorCodigo(
    curCodigo: number
  ): Promise<cursoInterface | null> {
    try {
      const res = await this.ofertaService.buscarCursoPorCodigo(curCodigo)
      return !!res?.objeto ? res.objeto : null
    } catch (error) {
      console.error("Error al buscar curso por código:", error)
      return null
    }
  }

  async buscarMatriculaOrdinariaEstudiante(
    matCodigo: number
  ): Promise<any | null> {
    try {
      this.estudianteMatriculaOrdinaria = (
        await this.participacionEstudiantilService.getMatriculaOrdinariaEstudiante(
          matCodigo
        )
      ).objeto
    } catch (error) {
      console.error("Error al buscar matricula del estudiante: ", error)
    }
  }

  async asignarGradoEstudiante(estudiante: IEstudiantesDTO) {
    let curso: cursoInterface = await this.buscarCursoPorCodigo(
      estudiante.curCodigo
    )
    let grado: CatGrado = this.listaGrados.find(
      (grado) => grado.graCodigo === curso.graCodigo
    )
    estudiante.graCodigo = grado.graCodigo
    estudiante.graDescripcion = grado.graDescripcion
    return estudiante
  }

  async buscarInscripcionPorMatpreCodigo(
    matpreCodigo: number
  ): Promise<any | null> {
    try {
      this.inscripcion = (
        await this.participacionEstudiantilService.getInscripcionPorMatpreCodigo(
          matpreCodigo
        )
      ).objeto
    } catch (error) {}
  }

  async guardar(data: any) {
    this.spinnerService.show()
    let inscripcion: InscripcionSubdenoDTO = {
      estCodigo: this.estudiante.idEstudiante,
      insCodigo: this.codInstitucion,
      parCodigo: data.paralelo,
      pinsEstado: 1,
      pinsFecCreacion: new Date(),
      ppeSubdenominacion: data.subDenominacion,
      reanleCodigo: this.reanleCodigo,
      matpreCodigo: this.estudianteMatriculaOrdinaria.matCodigo,
      sereduCodigo: this.estudianteMatriculaOrdinaria.sereduCodigo,
      pinsCodigo: this.inscripcion?.pinsCodigo,
    }
    try {
      await this.participacionEstudiantilService.guardarInscripcionPPE(
        inscripcion
      )
      this.reiniciarValores()
      this.mensajeService.mensajeCorrecto(
        "Éxito",
        "Estudiante inscrito correctamente."
      )
    } catch (error) {
      this.mensajeService.mensajeError(
        "Error",
        "Ocurrió un error, intente más tarde."
      )
      console.log("error al guardar la participacion: ", error)
    }
    this.spinnerService.hide()
  }
}
