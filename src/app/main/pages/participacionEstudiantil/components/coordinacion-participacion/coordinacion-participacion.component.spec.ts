import { ComponentFixture, TestBed } from '@angular/core/testing';
import { SeleccionParticipacionComponent } from './seleccion-participacion.component';


describe('SeleccionParticipacionComponent', () => {
  let component: SeleccionParticipacionComponent;
  let fixture: ComponentFixture<SeleccionParticipacionComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SeleccionParticipacionComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SeleccionParticipacionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
