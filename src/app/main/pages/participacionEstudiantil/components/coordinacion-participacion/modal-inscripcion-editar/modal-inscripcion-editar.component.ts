import { Component, Inject, OnInit } from "@angular/core"
import { UntypedFormControl, UntypedFormGroup, Validators } from "@angular/forms"
import { ParticipacionEstudiantilService } from "../../../services/participacion-estudiantil.service"
import { IofeParalelo } from "app/main/pages/calificacionOrdinaria/interfaces/IofeParalelo"
import { IPpeDenominacion } from "../../../interfaces/IPpeDenominacion"
import { MatRadioChange } from "@angular/material/radio"
import { MAT_DIALOG_DATA, MatDialogRef } from "@angular/material/dialog"
import { IPpeSubDenominacion } from "../../../interfaces/IPpeSubdenominacion"
import { MatSelectChange } from "@angular/material/select"
import { NgxSpinnerService } from "ngx-spinner"

@Component({
  selector: "app-modal-inscripcion-editar",
  templateUrl: "./modal-inscripcion-editar.component.html",
  styleUrls: ["./modal-inscripcion-editar.component.scss"],
})
export class ModalInscripcionEditarComponent {
  public inscripcionForm: UntypedFormGroup
  public paralelos: IofeParalelo[]
  private denominaciones: IPpeDenominacion[]
  private subDenominaciones: IPpeSubDenominacion[]
  public currentDenominaciones: IPpeDenominacion[]
  public currentSubDenominaciones: IPpeSubDenominacion[]
  public currentParalelos: IofeParalelo[]
  public habilitarBotones: boolean

  constructor(
    private participacionEstudiantilService: ParticipacionEstudiantilService,
    private spinnerService: NgxSpinnerService,
    public dialogRef: MatDialogRef<ModalInscripcionEditarComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any
  ) {
    this.initComponents()
    this.listarInformacion()
    this.initValoresForm()
  }

  async listarInformacion() {
    await this.listarParalelos()
    await this.listarDenominaciones()
    await this.listarSubDenominaciones()
    this.habilitarBotones = false
  }

  async listarParalelos() {
    try {
      this.paralelos = (
        await this.participacionEstudiantilService.getParalelos()
      ).listado
    } catch (error) {
      console.log(error)
    }
  }

  async listarDenominaciones() {
    try {
      this.denominaciones = (
        await this.participacionEstudiantilService.getDenominaciones()
      ).listado
      await this.setCurrentDenominaciones(
        parseInt(
          this.data.inscripcion.ppeSubdenominacion.ppeDenominacion
            .ppeParticipacion.pparCodigo
        )
      )
    } catch (error) {
      console.log(error)
    }
  }
  async listarSubDenominaciones() {
    try {
      this.subDenominaciones = (
        await this.participacionEstudiantilService.getSubDenominaciones()
      ).listado
      await this.setCurrentSubDenominaciones(
        this.data.inscripcion.ppeSubdenominacion.ppeDenominacion.pdenoCodigo
      )
    } catch (error) {
      console.log(error)
    }
  }

  handlerParticipacion(event: MatRadioChange) {
    this.resetForm()
    this.setCurrentDenominaciones(parseInt(event.value))
    this.currentSubDenominaciones = []
  }

  handlerDenominacion(event: MatSelectChange) {
    this.resetSubdenominacion()
    this.setCurrentSubDenominaciones(parseInt(event.value))
  }

  async setCurrentDenominaciones(pparCodigo: number) {
    this.currentDenominaciones = this.denominaciones.filter(
      (denominacion) => denominacion.ppeParticipacion.pparCodigo === pparCodigo
    )
  }

  async setCurrentSubDenominaciones(pdenoCodigo: number) {
    this.currentSubDenominaciones = this.subDenominaciones.filter(
      (subDenominacion) =>
        subDenominacion.ppeDenominacion.pdenoCodigo === pdenoCodigo
    )
  }

  guardar() {
    if (this.inscripcionForm.status === "VALID") {
      this.dialogRef.close(this.inscripcionForm.value)
    } else {
      //
    }
  }

  initComponents() {
    this.spinnerService.show()
    this.habilitarBotones = true
    this.paralelos = []
    this.denominaciones = []
    this.currentParalelos = []
    this.subDenominaciones = []
    this.currentDenominaciones = []
    this.currentSubDenominaciones = []
  }

  initValoresForm() {
    this.inscripcionForm = new UntypedFormGroup({
      areaAccion: new UntypedFormControl(
        `${this.data.inscripcion.ppeSubdenominacion.ppeDenominacion.ppeParticipacion.pparCodigo}`,
        [Validators.required]
      ),
      denominacion: new UntypedFormControl(
        this.data.inscripcion.ppeSubdenominacion.ppeDenominacion.pdenoCodigo,
        [Validators.required]
      ),
      subDenominacion: new UntypedFormControl(
        this.data.inscripcion.ppeSubdenominacion.psdenCodigo,
        [Validators.required]
      ),
      paralelo: new UntypedFormControl(this.data.inscripcion.parCodigo, [
        Validators.required,
      ]),
    })
    this.spinnerService.hide()
  }

  resetForm() {
    this.resetDenominacion()
    this.resetSubdenominacion()
    this.resetParalelo()
  }

  resetDenominacion() {
    this.inscripcionForm.get("denominacion").setValue(null)
    this.inscripcionForm.get("denominacion").markAsUntouched()
  }

  resetSubdenominacion() {
    this.inscripcionForm.get("subDenominacion").setValue(null)
    this.inscripcionForm.get("subDenominacion").markAsUntouched()
  }

  resetParalelo() {
    this.inscripcionForm.get("paralelo").setValue(null)
    this.inscripcionForm.get("paralelo").markAsUntouched()
  }

  getErrorMessage(field: string) {
    return this.inscripcionForm.get(field).hasError("required")
      ? "Campo requerido"
      : this.inscripcionForm.get(field).hasError("email")
      ? "Correo inválido"
      : this.inscripcionForm.get(field).hasError("maxlength")
      ? "longitud incorrecta"
      : this.inscripcionForm.get(field).hasError("minlength")
      ? "longitud incorrecta"
      : ""
  }
}
