import { Component, ElementRef, OnInit, ViewChild } from "@angular/core"
import { CatAnioElectivo } from "app/main/pages/asignacionDocentes/interface/cat_anio"
import { CatalogoService } from "app/main/pages/asignacionDocentes/servicios/catalogo.service"
import { GieeService } from "app/main/pages/asignacionDocentes/servicios/giee.service"
import { NgxSpinnerService } from "ngx-spinner"
import { UntypedFormControl, UntypedFormGroup, Validators } from "@angular/forms"
import { Observable } from "rxjs"
import { map, startWith } from "rxjs/operators"
import { MatTableDataSource } from "@angular/material/table"
import { MatPaginator, MatPaginatorIntl } from "@angular/material/paginator"
import { MatSort } from "@angular/material/sort"
import { IcatPoblacionObjetivo } from "app/main/pages/asignacionDocentesOrdinaria/interfaces/IcatPoblacionObjetivo"
import { IcatTemporalidad } from "app/main/pages/asignacionDocentesOrdinaria/interfaces/IcatTemporalidad"
import { IcatEscolaridad } from "app/main/pages/asignacionDocentesOrdinaria/interfaces/IcatEscolaridad"
import { IcatImplementacion } from "app/main/pages/asignacionDocentesOrdinaria/interfaces/IcatImplementacion"
import { IcatTipoEducacion } from "app/main/pages/asignacionDocentesOrdinaria/interfaces/IcatTipoEducacion"
import { IinsInstitucion } from "app/main/pages/asignacionDocentesOrdinaria/interfaces/IinsInstitucion"
import { IinsNivelPermisoInstitucionCruce } from "app/main/pages/asignacionDocentesOrdinaria/interfaces/IinsNivelPermisoInstitucionCruce"
import { IcatModalidadTipoEducacion } from "app/main/pages/asignacionDocentesOrdinaria/interfaces/IcatModalidadTipoEducacion"
import { IcatDocenteHispana } from "app/main/pages/asignacionDocentesOrdinaria/interfaces/IcatDocenteHispana"
import { IcatServicioEducativo } from "app/main/pages/asignacionDocentesOrdinaria/interfaces/IcatServicioEducativo"
import { MensajesService } from "app/main/pages/asignacionDocentesOrdinaria/servicios/mensajes.service"
import { DocenteOrdinariaService } from "app/main/pages/asignacionDocentesOrdinaria/servicios/docenteOrdinaria.service"
import { IofeParalelo } from "app/main/pages/calificacionOrdinaria/interfaces/IofeParalelo"
import { IPpeDenominacion } from "../../../interfaces/IPpeDenominacion"
import { IPpeSubDenominacion } from "../../../interfaces/IPpeSubdenominacion"
import { MatRadioChange } from "@angular/material/radio"
import { MatSelectChange } from "@angular/material/select"
import { ParticipacionEstudiantilService } from "../../../services/participacion-estudiantil.service"
import { IPpeCargos } from "../../../interfaces/IPpeCargos"
import { IPpeDistributivoBuenVivirDTO } from "../../../interfaces/IPpeDistributivoBuenVivirDTO"

@Component({
  selector: "app-seleccion-docente",
  templateUrl: "./seleccion-docente.component.html",
  styleUrls: ["./seleccion-docente.component.scss"],
})
export class SeleccionDocenteComponent implements OnInit {
  @ViewChild("selectElement") selectElement: ElementRef
  public docenteForm: UntypedFormGroup

  displayedColumns: string[] = ["numIdentificacion", "nomPersona"]
  displayedColumnsCargos: string[] = [
    "crgDescripcion",
    "cargaHoraria",
    "parDescripcion",
    "acciones",
  ]
  dataSource: MatTableDataSource<any>
  dataSourceCargos: MatTableDataSource<any>

  public paralelos: IofeParalelo[]
  private denominaciones: IPpeDenominacion[]
  private subDenominaciones: IPpeSubDenominacion[]
  public currentDenominaciones: IPpeDenominacion[]
  public currentSubDenominaciones: IPpeSubDenominacion[]
  public cargos: IPpeCargos[]
  public cargaHoraria: any[]
  public cargaHorariaEliminar: any[]
  //objetos
  public contentHeader: object
  //variables
  //variables paginación y orden
  @ViewChild(MatPaginator) paginator!: MatPaginator
  @ViewChild(MatSort) sort!: MatSort
  public isFormSubmitted: boolean = true
  public opcionBusqueda: boolean = true
  public reanleCodigo: number = null
  public institucion: IinsInstitucion = null

  public docenteHispana: IcatDocenteHispana = { numIdentificacion: "" }
  public listaInstitucion: any
  public informacionSede: any
  public codAmie: any
  public informacionInstitucion
  public codInstitucion
  public codgiRegimen
  public listaRegimen
  public regimen
  public anioElectivoCodigo
  public anioElectivoDescripcion
  public nombreInstitucion
  //listas
  public listaOrdenada: IinsNivelPermisoInstitucionCruce[] = []
  public listaServicioEducativo: IcatServicioEducativo[] = []
  public listaDocenteHispana: IcatDocenteHispana[] = []
  public listaAnio: CatAnioElectivo[] = []
  public listaPoblacionObjetivo: IcatPoblacionObjetivo[] = []
  public listaTemporabilidad: IcatTemporalidad[] = []
  public listaImplementacion: IcatImplementacion[] = []
  public listaEscolaridad: IcatEscolaridad[] = []
  public listaTipoEducacion: IcatTipoEducacion[] = []
  public listaModalidadTipoEducacion: IcatModalidadTipoEducacion[] = []
  public informacionEstablecimiento: any

  constructor(
    private CatalogoService: CatalogoService,
    private asignacionOrdinariaService: DocenteOrdinariaService,
    private GieeService: GieeService,
    private matPaginatorIntl: MatPaginatorIntl,
    private mensajeService: MensajesService,
    private spinnerService: NgxSpinnerService,
    private participacionEstudiantilService: ParticipacionEstudiantilService
  ) {
    this.isFormSubmitted = true
    this.initComponents()
    this.listarInformacion()
  }

  myControl = new UntypedFormControl()
  opcionesDocentes: Observable<IcatDocenteHispana[]>
  displayFn(docente: IcatDocenteHispana): string {
    return docente && docente.nomPersona ? docente.nomPersona : ""
  }

  private _filter(name: string): IcatDocenteHispana[] {
    const filterValue = name.toLowerCase()

    return this.listaDocenteHispana.filter((option) =>
      option.nomPersona.toLowerCase().includes(filterValue)
    )
  }
  async ngOnInit(): Promise<void> {
    //asignacion codigo amie
    this.listaInstitucion = JSON.parse(localStorage.getItem("currentUser"))
    this.informacionSede = this.listaInstitucion.sede
    this.nombreInstitucion = this.informacionSede.descripcion
    this.codAmie = this.informacionSede.nemonico
    //buscar la institucion y regimen
    this.buscarRegiment(this.codAmie)
    await this.obtenerListadoDocentesHispano(this.codAmie)
    this.opcionesDocentes = this.myControl.valueChanges.pipe(
      startWith(""),
      map((value) => {
        const name = typeof value === "string" ? value : value?.name
        return name
          ? this._filter(name as string)
          : this.listaDocenteHispana.slice()
      })
    )
    //buscar Establecimiento de la institucion
    this.obtenerEstablecimiento(this.codAmie)
  }
  reiniciarValores() {
    this.docenteHispana = { numIdentificacion: "" }
    this.asignarDocenteTabla(this.docenteHispana)
    // this.opcionBusqueda = false;
  }

  calcularAnchoSelect() {
    const opcionesLongitudes = this.listaPoblacionObjetivo.map(
      (item) => item.pbObjtDescripcion.length
    )
    const longitudMaxima = Math.max(...opcionesLongitudes)

    // Establecer el ancho del select basado en la longitud máxima
    const anchoMinimo = 100 // Puedes ajustar esto según tus necesidades
    const anchoCalculado = Math.max(anchoMinimo, longitudMaxima * 10) // Puedes ajustar el factor de multiplicación

    this.selectElement.nativeElement.style.width = `${anchoCalculado}px`
  }

  //buscar los datos de la institucion y regimen
  buscarRegiment(cod) {
    this.spinnerService.show()
    //buscar la institucion
    this.GieeService.getDatosInstitucion(cod).then(
      (data) => {
        this.informacionInstitucion = data.objeto
        for (let dato in this.informacionInstitucion) {
          this.codInstitucion = this.informacionInstitucion.insCodigo
          this.codgiRegimen = this.informacionInstitucion.regimenes[0].regCodigo
        }

        this.CatalogoService.getRegimen(this.codgiRegimen).then((data) => {
          this.listaRegimen = data.objeto
          //exportar el objeto
          this.regimen = this.listaRegimen?.regDescripcion
        })
        //listar regimenAñolectivo
        this.CatalogoService.getAnioElectivo(this.codgiRegimen).then((data) => {
          this.listaAnio = data.listado
          for (let dato of this.listaAnio) {
            if (dato.reanleTipo === "A") {
              this.reanleCodigo = dato.reanleCodigo
              this.anioElectivoCodigo = dato.anilecCodigo
              this.CatalogoService.getAnioLectivoPorAnilecCodigo(
                this.anioElectivoCodigo
              ).then((data) => {
                let anioLectivo = data.objeto
                this.anioElectivoDescripcion =
                  anioLectivo.anilecAnioInicio +
                  " - " +
                  anioLectivo.anilecAnioFin
              })
            }
          }
        })
      },
      (error) => {
        // alert(error);
        // alert("Usuario sin sede");
      }
    )
    this.spinnerService.hide()
  }

  //sacar los establecimientos de la institucion
  async obtenerEstablecimiento(cod) {
    this.informacionEstablecimiento =
      await this.GieeService.getEstablecimientoPorAmie(cod).then(
        (data) => data.listado
      )
  }

  obtenerDocentePorIdentificacion(docenteHispana: IcatDocenteHispana) {
    this.spinnerService.show()
    if (typeof docenteHispana === "string") {
      this.mensajeService.mensajeAdvertencia(
        "Advertencia",
        "Seleccione un docente"
      )
      return
    }

    if (docenteHispana?.numIdentificacion != undefined) {
      this.asignacionOrdinariaService
        .getDocentePorIdentificacion(docenteHispana.numIdentificacion)
        .then(async (data) => {
          this.docenteHispana = data.objeto
          if (this.docenteHispana?.codAmie !== this.codAmie) {
            this.docenteHispana = { numIdentificacion: "" }
            this.mensajeService.mensajeError(
              "",
              "Ups! No se encuentra al docente con la identificación ingresada"
            )
          }
          this.asignarDocenteTabla(this.docenteHispana)
        })
    }
    this.spinnerService.hide()
  }
  //sacar población objetivo
  async obtenerListadoDocentesHispano(codAmie) {
    this.listaDocenteHispana = await this.asignacionOrdinariaService
      .getListaDocentesHispanoPorCodAmie(codAmie)
      .then((data) => data.listado)
  }
  //validar solo ingreso de numeros
  validateFormat(event) {
    let key
    if (event.type === "paste") {
      key = event.clipboardData.getData("text/plain")
    } else {
      key = event.keyCode
      key = String.fromCharCode(key)
    }
    const regex = /[0-9]|\./
    if (!regex.test(key)) {
      event.returnValue = false
      if (event.preventDefault) {
        event.preventDefault()
      }
    }
  }

  async asignarDocenteTabla(docente: IcatDocenteHispana) {
    this.dataSource = new MatTableDataSource([docente])
    this.dataSource.paginator = this.paginator
    this.dataSource.sort = this.sort
    this.dataSource.paginator = this.paginator
    this.matPaginatorIntl.itemsPerPageLabel = "Elementos por página:"
    this.matPaginatorIntl.nextPageLabel = "Siguiente:"
    this.matPaginatorIntl.previousPageLabel = "Anterior:"
  }

  async asignarCargaHorariaTabla(cargaHoraria: any[]) {
    this.dataSourceCargos = new MatTableDataSource(cargaHoraria)
    this.dataSourceCargos.paginator = this.paginator
    this.dataSourceCargos.sort = this.sort
    this.dataSourceCargos.paginator = this.paginator
    this.matPaginatorIntl.itemsPerPageLabel = "Elementos por página:"
    this.matPaginatorIntl.nextPageLabel = "Siguiente:"
    this.matPaginatorIntl.previousPageLabel = "Anterior:"
  }

  onInputEvent(event: any) {
    // Aquí puedes manejar el evento input como desees
    // this.listaCruzadaNivelPermisoInstitucion = []
    // Puedes realizar acciones adicionales, como buscar y filtrar datos en función de lo que se escribió en el campo de entrada.
  }
  reiniciarDatosTabla() {
    this.asignarDocenteTabla(this.docenteHispana)
    this.consultarDistributivoBuenVivirPorDocente(this.docenteHispana)
  }
  consultarDistributivoBuenVivirPorDocente(docenteHispana: IcatDocenteHispana) {
    this.participacionEstudiantilService
      .listarDistriBuenvivirPorDocCodigo(docenteHispana.codPersona)
      .then(async (res) => {
        for (const distributivo of res.listado) {
          let findParalelo = await this.paralelos.find(obj => obj.parCodigo === distributivo.parCodigo)
          this.cargaHoraria.push({
            dbuCodigo: distributivo.dbuCodigo,
            crgCodigo: distributivo.ppeCargo.crgCodigo,
            crgDescripcion: distributivo.ppeCargo.crgDescripcion,
            cargaHoraria: distributivo.crgHoras,
            parCodigo: distributivo.parCodigo,
            parDescripcion: findParalelo?.parDescripcion,
            psubdenCodigo: distributivo.ppeSubdenominacion.psdenCodigo,
          })
          this.asignarCargaHorariaTabla(this.cargaHoraria)
        }

        // this.mensajeService.mensajeCorrecto(
        //   "Éxito",
        //   "Información almacenada correctamente"
        // )
      })
      .catch((error) => {
        console.log("error:", error)
      })
  }
  getErrorMessage(field: string) {
    return this.docenteForm.get(field).hasError("required")
      ? "Campo requerido"
      : this.docenteForm.get(field).hasError("email")
        ? "Correo inválido"
        : this.docenteForm.get(field).hasError("maxlength")
          ? "longitud incorrecta"
          : this.docenteForm.get(field).hasError("minlength")
            ? "longitud incorrecta"
            : ""
  }

  async listarInformacion() {
    await this.listarCargos()
    await this.listarParalelos()
    await this.listarDenominaciones()
    await this.listarSubDenominaciones()
    await this.setCurrentDenominaciones(
      parseInt(this.docenteForm.get("areaAccion").value)
    )
  }
  initComponents() {
    this.cargos = []
    this.cargaHoraria = []
    this.cargaHorariaEliminar = []
    this.paralelos = []
    this.denominaciones = []
    this.subDenominaciones = []
    this.currentDenominaciones = []
    this.currentSubDenominaciones = []
    this.docenteForm = new UntypedFormGroup({
      areaAccion: new UntypedFormControl("1", [Validators.required]),
      denominacion: new UntypedFormControl("", [Validators.required]),
      subDenominacion: new UntypedFormControl("", [Validators.required]),
      paralelo: new UntypedFormControl("", [Validators.required]),
      cargo: new UntypedFormControl("", [Validators.required]),
      cargaHoraria: new UntypedFormControl("", [Validators.required]),
    })
  }

  async listarParalelos() {
    try {
      this.paralelos = (
        await this.participacionEstudiantilService.getParalelos()
      ).listado
    } catch (error) {
      console.log(error)
    }
  }

  async listarDenominaciones() {
    try {
      this.denominaciones = (
        await this.participacionEstudiantilService.getDenominaciones()
      ).listado
    } catch (error) {
      console.log(error)
    }
  }

  async listarSubDenominaciones() {
    try {
      this.subDenominaciones = (
        await this.participacionEstudiantilService.getSubDenominaciones()
      ).listado
    } catch (error) {
      console.log(error)
    }
  }

  async listarCargos() {
    try {
      this.cargos = (
        await this.participacionEstudiantilService.getCargos()
      ).listado
    } catch (error) {
      console.log(error)
    }
  }

  handlerParticipacion(event: MatRadioChange) {
    this.resetForm()
    this.setCurrentDenominaciones(parseInt(event.value))
    this.currentSubDenominaciones = []
  }

  handlerDenominacion(event: MatSelectChange) {
    this.setCurrentSubDenominaciones(parseInt(event.value))
  }

  setCurrentDenominaciones(pparCodigo: number) {
    this.currentDenominaciones = this.denominaciones.filter(
      (denominacion) => denominacion.ppeParticipacion.pparCodigo === pparCodigo
    )
  }

  setCurrentSubDenominaciones(pdenoCodigo: number) {
    this.currentSubDenominaciones = this.subDenominaciones.filter(
      (subDenominacion) =>
        subDenominacion.ppeDenominacion.pdenoCodigo === pdenoCodigo
    )
    this.docenteForm.get("subDenominacion").setErrors({ 'required': true })

  }

  resetForm() {
    this.docenteForm.get("denominacion").setValue(null)
    this.docenteForm.get("subDenominacion").setValue(null)
    this.docenteForm.get("paralelo").setValue(null)
    this.docenteForm.get("cargo").setValue(null)
    this.docenteForm.get("cargaHoraria").setValue(null)
    this.docenteForm.get("denominacion").markAsUntouched()
    this.docenteForm.get("subDenominacion").markAsUntouched()
    this.docenteForm.get("paralelo").markAsUntouched()
    this.docenteForm.get("cargo").markAsUntouched()
    this.docenteForm.get("cargaHoraria").markAsUntouched()
  }

  agregar() {
    if (this.docenteForm.status === "VALID") {
      if (this.docenteHispana?.nomPersona !== null) {
        let cargoFind: IPpeCargos = this.cargos.find(
          (cargo) =>
            cargo.crgCodigo === parseInt(this.docenteForm.get("cargo").value)
        )
        let paraleloFind: IofeParalelo = this.paralelos.find(
          (paralelo) =>
            paralelo.parCodigo ===
            parseInt(this.docenteForm.get("paralelo").value)
        )
        if (
          this.validarCargaHoraria(
            this.cargaHoraria,
            parseInt(this.docenteForm.get("cargo").value),
            parseInt(this.docenteForm.get("cargaHoraria").value)
          )
        ) {
          this.cargaHoraria.push({
            crgCodigo: cargoFind.crgCodigo,
            crgDescripcion: cargoFind.crgDescripcion,
            cargaHoraria: parseInt(this.docenteForm.get("cargaHoraria").value),
            parCodigo: paraleloFind.parCodigo,
            parDescripcion: paraleloFind.parDescripcion,
            psubdenCodigo: parseInt(
              this.docenteForm.get("subDenominacion").value
            ),
          })
          this.asignarCargaHorariaTabla(this.cargaHoraria)
        } else {
          this.mensajeService.mensajeError(
            "Error",
            `Carga horaria para ${cargoFind.crgDescripcion} debe ser máximo de ${cargoFind.crgHoras} hora(s) en total`
          )
        }
      }
    }
  }

  validarCargaHoraria(
    cargaHoraria: any[],
    crgCodigo: number,
    nuevaCarga: number
  ): boolean {
    let maximo = this.cargos.find(
      (cargo) => cargo.crgCodigo === crgCodigo
    ).crgHoras
    const resultado = cargaHoraria
      .filter((carga) => carga.crgCodigo === crgCodigo)
      .reduce((total, carga) => total + carga.cargaHoraria, 0)

    return resultado < maximo && resultado + nuevaCarga <= maximo
  }

  eliminarCargaHoraria(indice: number) {
    let cargaHorariaTemp = [...this.cargaHoraria]
    let elementoEliminado = cargaHorariaTemp.splice(indice, 1)[0]
    if (elementoEliminado.dbuCodigo != null) {
      this.cargaHorariaEliminar.push(elementoEliminado)
    }
    this.cargaHoraria = [...cargaHorariaTemp.slice()]
    this.asignarCargaHorariaTabla(this.cargaHoraria)
  }

  totalCargaHoraria(): number {
    return this.cargaHoraria.reduce(
      (total, carga) => total + carga.cargaHoraria,
      0
    )
  }

  guardarCargaHoraria() {
    if (this.cargaHoraria.length > 0) {
      if (this.totalCargaHoraria() <= 4) {
        this.spinnerService.show()

        let data: IPpeDistributivoBuenVivirDTO[] = this.formatDistriBuenVivir(
          this.cargaHoraria
        )
        this.eliminarDistributivo(this.cargaHorariaEliminar);
        this.participacionEstudiantilService
          .guardarDistributivoBuenVivirPPE(data)
          .then((res) => {
            this.encerarFormulario();
            this.mensajeService.mensajeCorrecto(
              "Éxito",
              "Información almacenada correctamente"
            )
          })
          .catch((error) => {
            console.log("error:", error)
          })
        this.spinnerService.hide()
      } else {
        this.mensajeService.mensajeError(
          "Error",
          "La carga horaria debe ser máximo de 4 horas."
        )
      }
    } else {
      this.mensajeService.mensajeError(
        "Error",
        "Ingrese al menos 1 carga horaria para el docente seleccionado."
      )
    }
  }
  eliminarDistributivo(listaDistributivoEliminar) {
    let data: IPpeDistributivoBuenVivirDTO[] = this.formatDistriBuenVivirEliminar(
      listaDistributivoEliminar
    )
    this.participacionEstudiantilService
      .guardarDistributivoBuenVivirPPE(data)
      .then((res) => {

      })
      .catch((error) => {
        console.log("error:", error)
      })
  }
  formatDistriBuenVivir(cargaHoraria: any[]): IPpeDistributivoBuenVivirDTO[] {
    return cargaHoraria.map((carga) => {
      return {
        dbuCodigo: carga.dbuCodigo,
        docCodigo: this.docenteHispana.codPersona,
        parCodigo: carga.parCodigo,
        dbuEstado: 1,
        ppeCargo: carga.crgCodigo,
        ppeSubdenominacion: carga.psubdenCodigo,
        crgHoras: carga.cargaHoraria,
      }
    })
  }
  formatDistriBuenVivirEliminar(cargaHoraria: any[]): IPpeDistributivoBuenVivirDTO[] {
    return cargaHoraria.map((carga) => {
      return {
        dbuCodigo: carga.dbuCodigo,
        docCodigo: this.docenteHispana.codPersona,
        parCodigo: carga.parCodigo,
        dbuEstado: 0,
        ppeCargo: carga.crgCodigo,
        ppeSubdenominacion: carga.psubdenCodigo,
        crgHoras: carga.cargaHoraria,
      }
    })
  }
  encerarFormulario() {
    this.asignarCargaHorariaTabla([]);
    this.reiniciarValores();
    this.cargaHoraria = []
    this.cargaHorariaEliminar = []
  }
}
