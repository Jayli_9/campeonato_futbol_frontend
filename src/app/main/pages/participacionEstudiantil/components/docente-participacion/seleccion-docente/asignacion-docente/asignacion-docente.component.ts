import { ChangeDetectorRef, Component, EventEmitter, Input, OnInit, Output, ViewChild } from '@angular/core';
import { Router } from "@angular/router"
import { MatTableDataSource } from '@angular/material/table';
import { MatPaginator, MatPaginatorIntl } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import * as _ from 'lodash';
import { NgxSpinnerService } from 'ngx-spinner';
import { IinsNivelPermisoInstitucionCruce } from 'app/main/pages/asignacionDocentesOrdinaria/interfaces/IinsNivelPermisoInstitucionCruce';
import { IcatDocenteHispana } from 'app/main/pages/asignacionDocentesOrdinaria/interfaces/IcatDocenteHispana';
import { IcatGrado } from 'app/main/pages/calificacionOrdinaria/interfaces/IcatGrado';
import { IcatEspecialidad } from 'app/main/pages/asignacionDocentesOrdinaria/interfaces/IcatEspecialidad';
import { IcatJornada } from 'app/main/pages/asignacionDocentesOrdinaria/interfaces/IcatJornada';
import { IofeCurso } from 'app/main/pages/asignacionDocentesOrdinaria/interfaces/IofeCurso';
import { DocenteOrdinariaService } from 'app/main/pages/asignacionDocentesOrdinaria/servicios/docenteOrdinaria.service';
import { MensajesService } from 'app/main/pages/asignacionDocentesOrdinaria/servicios/mensajes.service';
import { constante } from 'app/main/pages/asignacionDocentesOrdinaria/constantes/constante';

@Component({
  selector: 'app-asignacion-docente',
  templateUrl: './asignacion-docente.component.html',
  styleUrls: ['./asignacion-docente.component.scss'],
})
export class AsignacionDocenteComponent implements OnInit {
  displayedColumns: string[] = ['gradoDescripcion', 'paralelo', 'jornada', 'asignaturas', 'acciones'];
  dataSource: MatTableDataSource<any>;
  //inputs
  @Input() nivelCruceSeleccionadoChild: IinsNivelPermisoInstitucionCruce;
  @Input() tipoEducacionChild;
  @Input() informacionEstablecimientoChild;
  @Input() docenteHispanaChild: IcatDocenteHispana;
  //Outputs
  @Output() deshabilitarAsignacionOutput: EventEmitter<boolean>
  //listas
  listaPreparatorio = [];
  listaPrepa: any[] = [];
  preparatoriaData;

  public listaOriginal;

  datoVisual: number = 0;
  validadorGuardado: number = 0;
  public listaGrados: IcatGrado[] = [];
  public listaEspecialidad: IcatEspecialidad[] = [];
  public listaJornada: IcatJornada[] = [];
  public listaOferta: IofeCurso[] = [];
  public listaGradoOferta: any[] = [];
  public listaGradoOfertaCombinada: any[] = [];
  public listaNombreGrados: any[] = [];
  public listaJornadas: any[] = [];
  // public listaMalla:IlistaMalla[];
  //variables
  public listaInstitucion;
  public informacionSede;
  public nombreInstitucion;
  public codAmie;
  public especialidad: IcatEspecialidad;
  public jornada: IcatJornada;
  public gradoFiltro: any;
  //variables paginación y orden
  @ViewChild(MatPaginator) paginator!: MatPaginator;
  @ViewChild(MatSort) sort!: MatSort;

  //Distributivos
  distributivos = [];

  //objetos
  public establecimiento;
  constructor(private asignacionOrdinariaService: DocenteOrdinariaService,
    private matPaginatorIntl: MatPaginatorIntl,
    private mensajeService: MensajesService,
    private spinnerService: NgxSpinnerService,
    private readonly router: Router,
    private cdr: ChangeDetectorRef) {
    this.deshabilitarAsignacionOutput = new EventEmitter<boolean>()
    this.distributivos = [];
  }

  async ngOnInit(): Promise<void> {
    this.consultarJornadas();
    this.especialidad = this.nivelCruceSeleccionadoChild.especialidad;

    await this.gradosPorNiveles(this.nivelCruceSeleccionadoChild.nivel);
    //Buscar el establecimientto matriz
    let matriz = this.informacionEstablecimientoChild.find(obj => obj.insTipoEstablecimiento.tipEstCodigo === 1)
    this.establecimiento = matriz.insestCodigo
    await this.validarEstablecimiento();
    //Fin buscar matriz
    if (this.nivelCruceSeleccionadoChild.figprobtCodigo !== null) {
      await this.consultarEspecialidades(this.nivelCruceSeleccionadoChild.figprobtCodigo);
    }
    //asignacion codigo amie
    this.listaInstitucion = JSON.parse(localStorage.getItem('currentUser'));
    this.informacionSede = this.listaInstitucion.sede;
    this.nombreInstitucion = this.informacionSede.descripcion;
    this.codAmie = this.informacionSede.nemonico;

  }
  async gradosPorNiveles(nivel) {
    //listar los grados de acuerdo al nivel seleccionado 
    await this.asignacionOrdinariaService.getGradosporNivCodigo(nivel.nivCodigo).then(data => {
      this.listaGrados = data.listado;
    })
  }
  async validarEstablecimiento() {
    if (this.listaEspecialidad.length < 1) {
      await this.ofertaPorEtablecimientoYServicioEducativo(this.establecimiento);
    } else {
      this.especialidad = {};
    }
  }
  validarEspecialidad() {
    this.ofertaPorEtablecimientoYServicioEducativo(this.establecimiento);
  }


  async ofertaPorEtablecimientoYServicioEducativo(establecimiento) {
    this.spinnerService.show();
    this.listaGradoOfertaCombinada = [];
    //listar la oferta de acuerdo al establecimiento y servicio educativo
    await this.asignacionOrdinariaService.getOfertaPorInsestCodigoSereduCodigo(establecimiento, this.nivelCruceSeleccionadoChild.sereduCodigo).then(data => {
      this.listaOferta = data.listado;
    })
    // this.listaOferta = this.listaOferta.filter(obj => obj.sereduCodigo === this.nivelCruceSeleccionadoChild.sereduCodigo);
    this.listaGradoOferta = this.listaGrados.flatMap(obj1 => {
      const objetosCoincidentes = this.listaOferta.filter(obj2 => obj2.graCodigo === obj1.graCodigo);

      // Combinar obj1 con todos los objetos coincidentes de listaOferta
      return objetosCoincidentes.map(obj2 => ({ ...obj1, ...obj2 }));
    }); // Filtrar elementos nulos para eliminarlos

    // Obtener la lista de cursos paralelos para cada grado
    await Promise.all(this.listaGradoOferta.map(async (grado) => {
      const data = await this.asignacionOrdinariaService.getCursoParaleloPorCurCodigo(grado.curCodigo);
      grado.listaCursoParalelo = data.listado;
    }));

    // Crear la lista combinada
    this.listaGradoOfertaCombinada = this.listaGradoOferta.flatMap(objeto => {
      return objeto.listaCursoParalelo.map(objetoDentro => {
        const objetoCombinado = this.combinarObjetos(objeto, objetoDentro);
        objeto.asignaturaSeleccionada = null;
        return objetoCombinado;
      });
    });
    if (this.nivelCruceSeleccionadoChild.espbtCodigo !== null) {
      this.listaGradoOfertaCombinada = this.listaGradoOfertaCombinada.filter(obj => obj.espCodigo === this.nivelCruceSeleccionadoChild.espbtCodigo)
    }
    if (this.nivelCruceSeleccionadoChild.arecieCodigo !== null) {
      this.listaGradoOfertaCombinada = this.listaGradoOfertaCombinada.filter(obj => obj.arecieCodigo === this.nivelCruceSeleccionadoChild.arecieCodigo)
    }

    /////////////////////////////////////////////////////////////////

    // Obtener datos de manera concurrente para todos los objetos en listaGradoOfertaCombinada
    await Promise.all(this.listaGradoOfertaCombinada.map(async (objeto) => {
      const [jornadaData, mallaData, distributivoData] = await Promise.all([
        this.asignacionOrdinariaService.getJornadaPorJorCodigo(objeto.jorCodigo),
        this.asignacionOrdinariaService.getMallaAsignaturaPorCurCodigo(objeto.curCodigo),
        this.asignacionOrdinariaService.postDistributivoPorCodigoDocenteCurparCodigo(this.docenteHispanaChild.codPersona, objeto.curparCodigo)
      ]);

      objeto.objetoJornada = jornadaData.objeto;

      // Modificar la listaMalla directamente en lugar de crear una nueva lista
      objeto.listaMalla = mallaData.listado.map(item => {
        const objetoAnidadoSinOtroObjetoAnidado = { ...item.acaAsignatura };
        delete objetoAnidadoSinOtroObjetoAnidado.acaAgrupacion;
        delete objetoAnidadoSinOtroObjetoAnidado.acaAreaConocimiento;
        delete objetoAnidadoSinOtroObjetoAnidado.acaTipoAsignatura;
        delete objetoAnidadoSinOtroObjetoAnidado.acaTipoValoracion;
        return { ...item, acaAsignatura: objetoAnidadoSinOtroObjetoAnidado };
      });

      objeto.listaDistributivo = distributivoData.listado;
      distributivoData.listado.forEach(element => {
        this.distributivos.push(element);
      });

      objeto.listaAsignaturasSeleccionadas = objeto.listaDistributivo.map(objeto => ({
        maasCodigo: objeto.acaMallaAsignatura?.maasCodigo,
        curCodigo: objeto.acaMallaAsignatura?.curCodigo,
        curparCodigo: objeto.curparCodigo,
        maasEstado: objeto.acaMallaAsignatura?.maasEstado,
        maasHoras: objeto.acaMallaAsignatura?.maasHoras,
        maasNemonico: objeto.acaMallaAsignatura?.maasNemonico,
        reanleCodigo: objeto.acaMallaAsignatura?.reanleCodigo,
        disCodigo: objeto.disCodigo,
        acaAsignatura: objeto.acaMallaAsignatura?.acaAsignatura
      }));

      // Modificar la listaMalla directamente en lugar de crear una nueva lista
      objeto.listaAsignaturasSeleccionadas = objeto.listaAsignaturasSeleccionadas.map(item => {
        const objetoAnidadoSinOtroObjetoAnidado = { ...item.acaAsignatura };
        delete objetoAnidadoSinOtroObjetoAnidado.acaAgrupacion;
        delete objetoAnidadoSinOtroObjetoAnidado.acaAreaConocimiento;
        delete objetoAnidadoSinOtroObjetoAnidado.acaTipoAsignatura;
        delete objetoAnidadoSinOtroObjetoAnidado.acaTipoValoracion;
        return { ...item, acaAsignatura: objetoAnidadoSinOtroObjetoAnidado };
      });

      // Actualizar disCodigo en listaMalla
      objeto.listaMalla.forEach(ele => {
        const existe = objeto.listaAsignaturasSeleccionadas.find(obj => obj.maasCodigo === ele.maasCodigo);
        if (existe !== undefined) {
          ele.disCodigo = existe.disCodigo;
        }
      });

      await this.sacarDatosMateriaYApre(objeto);
    }));

    this.filtrarNombresRepetidos(this.listaGradoOfertaCombinada)
    this.filtrarJornadasRepetidos(this.listaGradoOfertaCombinada)
    this.dataSource = new MatTableDataSource(this.listaGradoOfertaCombinada);
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
    this.dataSource.paginator = this.paginator;
    this.matPaginatorIntl.itemsPerPageLabel = 'Elementos por página:';
    this.matPaginatorIntl.nextPageLabel = 'Siguiente:';
    this.matPaginatorIntl.previousPageLabel = 'Anterior:';
    //Listar los paralelos que tiene el curso;

    this.spinnerService.hide();
  }


  async sacarDatosMateriaYApre(objeto) {

    let result = this.distributivos.filter((item, index) => {
      return this.distributivos.indexOf(item) === index;
    })

    if (objeto.graCodigo >= 1 && objeto.graCodigo <= 3) {

      this.validadorGuardado = 1;

      ///realizar validación si ya hay matematicas asignadas a un profesor en un paralelo
      await this.asignacionOrdinariaService.getDistributivoPorCurparCodigo(objeto.curparCodigo).then(data => {

        let distributivoAux = data.listado;

        // Filtra los elementos de la segunda lista que no tienen el mismo ID que los de la primera lista
        const listaFiltrada = objeto.listaMalla.filter((elemento2) => !distributivoAux.some((elemento1) => elemento1.acaMallaAsignatura?.maasCodigo === elemento2.maasCodigo
          && elemento1.docCodigo !== this.docenteHispanaChild.codPersona));
        objeto.listaMalla = listaFiltrada;

      }
      )

      this.datoVisual = 1;
      await this.asignacionOrdinariaService.obtenerAsignaturaPrepaPorGrado(objeto.graCodigo).then(data => {
        this.listaPrepa = data.listado

        objeto.listaMalla.forEach(element => {

          element.acaAsignatura = this.listaPrepa;

        });

      })

      setTimeout(() => {
        objeto.listaAsignaturasSeleccionadasAnterior = objeto.listaAsignaturasSeleccionadas;
      }, 2000);




    } else {

      this.validadorGuardado = 2

      this.datoVisual = 2

      ///realizar validación si ya hay matematicas asignadas a un profesor en un paralelo
      await this.asignacionOrdinariaService.getDistributivoPorCurparCodigo(objeto.curparCodigo).then(data => {

        let distributivoAux = data.listado;
        // Filtra los elementos de la segunda lista que no tienen el mismo ID que los de la primera lista
        const listaFiltrada = objeto.listaMalla.filter((elemento2) => !distributivoAux.some((elemento1) => elemento1.acaMallaAsignatura?.maasCodigo === elemento2.maasCodigo
          && elemento1.docCodigo !== this.docenteHispanaChild.codPersona));
        objeto.listaMalla = listaFiltrada;
      }
      )

      ///realizar validación si ya hay matematicas asignadas a un profesor en un paralelo
      objeto.listaAsignaturasSeleccionadasAnterior = objeto.listaAsignaturasSeleccionadas;

    }

  }

  filtrarPorJornada(jornadaCodigo) {
    this.gradoFiltro = {};
    let listaJornada = this.listaGradoOfertaCombinada.filter(obj => obj.jorCodigo === jornadaCodigo)
    this.filtrarNombresRepetidos(listaJornada)
    // this.filtrarJornadasRepetidos(listaJornada)
    this.dataSource = new MatTableDataSource(listaJornada);
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
    this.dataSource.paginator = this.paginator;
    this.matPaginatorIntl.itemsPerPageLabel = 'Elementos por página:';
    this.matPaginatorIntl.nextPageLabel = 'Siguiente:';
    this.matPaginatorIntl.previousPageLabel = 'Anterior:';
  }
  combinarObjetos(objeto1, objeto2) {
    return { ...objeto1, ...objeto2 };
  }

  async guardarIndividual(elemento) {

    if (this.validadorGuardado === 1) {

      this.asignacionOrdinariaService.postDistributivoPorCodigoDocenteCurparCodigo(this.docenteHispanaChild.codPersona, elemento.curparCodigo).then(data => {
        let listaDistributivo = data.listado;
        if (listaDistributivo.length > 0) {

          if (elemento.listaAsignaturasSeleccionadas.length > 0) {

            elemento.listaAsignaturasSeleccionadas.forEach(element => {

              elemento.listaAsignaturasSeleccionadasAnterior.forEach(anterior => {

                if (element.maasCodigo == anterior.maasCodigo) {

                  let distributivoDTO = {
                    disCodigo: element.disCodigo,
                    acaMallaAsignatura: element.maasCodigo,
                    curparCodigo: elemento.curparCodigo,
                    disEstado: 0,
                    docCodigo: this.docenteHispanaChild.codPersona,
                    curCodigo: elemento.curCodigo,
                    reanleCodigo: elemento.reanleCodigo,
                    tutor: 0,
                    insestCodigo: elemento.insestCodigo
                  }

                  this.asignacionOrdinariaService.postGuardarDistributivo(distributivoDTO).then(data => {
                  }, error => {
                    this.mensajeService.mensajeError('', 'Error al guardar registro\n' + error);
                  })

                } else if (elemento.listaAsignaturasSeleccionadas.length === elemento.listaAsignaturasSeleccionadasAnterior) {

                  let distributivoDTO = {
                    disCodigo: anterior.disCodigo,
                    acaMallaAsignatura: anterior.maasCodigo,
                    curparCodigo: elemento.curparCodigo,
                    disEstado: 0,
                    docCodigo: this.docenteHispanaChild.codPersona,
                    curCodigo: anterior.curCodigo,
                    reanleCodigo: anterior.reanleCodigo,
                    tutor: 0,
                    insestCodigo: anterior.insestCodigo
                  }

                  this.asignacionOrdinariaService.postGuardarDistributivo(distributivoDTO).then(data => {
                  }, error => {
                    this.mensajeService.mensajeError('', 'Error al guardar registro\n' + error);
                  })

                } else {
                  let distributivoDTO = {
                    acaMallaAsignatura: element.maasCodigo,
                    curparCodigo: elemento.curparCodigo,
                    disEstado: 1,
                    docCodigo: this.docenteHispanaChild.codPersona,
                    curCodigo: element.curCodigo,
                    reanleCodigo: element.reanleCodigo,
                    tutor: 0,
                    insestCodigo: this.establecimiento
                  }
                  this.asignacionOrdinariaService.postGuardarDistributivo(distributivoDTO).then(data => {
                  }, error => {
                    this.mensajeService.mensajeError('', 'Error al guardar registro\n' + error);
                  })
                }

              });

            });

          } else {
            elemento.listaAsignaturasSeleccionadas.forEach(element => {

              let distributivoDTO = {
                acaMallaAsignatura: element.maasCodigo,
                curparCodigo: elemento.curparCodigo,
                disEstado: 1,
                docCodigo: this.docenteHispanaChild.codPersona,
                curCodigo: element.curCodigo,
                reanleCodigo: element.reanleCodigo,
                tutor: 0,
                insestCodigo: this.establecimiento
              }
              this.asignacionOrdinariaService.postGuardarDistributivo(distributivoDTO).then(data => {
              }, error => {
                this.mensajeService.mensajeError('', 'Error al guardar registro\n' + error);
              })
            });

          }
        } else {


          elemento.listaAsignaturasSeleccionadas.forEach(element => {
            let distributivoDTO = {
              acaMallaAsignatura: element.maasCodigo,
              curparCodigo: elemento.curparCodigo,
              disEstado: 1,
              docCodigo: this.docenteHispanaChild.codPersona,
              curCodigo: element.curCodigo,
              reanleCodigo: element.reanleCodigo,
              tutor: 0,
              insestCodigo: this.establecimiento
            }

            this.asignacionOrdinariaService.postGuardarDistributivo(distributivoDTO).then(data => {
            }, error => {
              this.mensajeService.mensajeError('', 'Error al guardar registro\n' + error);
            })
          });
        }

      })

    } else {

      // Utiliza filter y every para obtener objetos que no se repiten
      const listaC = elemento.listaAsignaturasSeleccionadasAnterior.filter(itemA => !elemento.listaAsignaturasSeleccionadas.some(itemB => itemA.disCodigo === itemB.disCodigo && itemA.maasCodigo === itemB.maasCodigo));
      const listaD = elemento.listaAsignaturasSeleccionadas.filter(itemB => !elemento.listaAsignaturasSeleccionadasAnterior.some(itemA => itemB.disCodigo === itemA.disCodigo && itemB.maasCodigo === itemA.maasCodigo));
      // Combina listaC y listaD para obtener todos los objetos que no se repiten en ambas listas
      const objetosNoRepetidos = [...listaC, ...listaD];
      for (const iterator of objetosNoRepetidos) {

        if (iterator.disCodigo) {
          let distributivoDTO = {
            disCodigo: iterator.disCodigo,
            acaMallaAsignatura: iterator.maasCodigo,
            curparCodigo: iterator.curparCodigo,
            disEstado: 0,
            docCodigo: this.docenteHispanaChild.codPersona,
            curCodigo: iterator.curCodigo,
            reanleCodigo: iterator.reanleCodigo,
            tutor: 0,
            insestCodigo: iterator.insestCodigo
          }

          await this.asignacionOrdinariaService.postGuardarDistributivo(distributivoDTO).then(data => {
          }, error => {
            this.mensajeService.mensajeError('', 'Error al guardar registro\n' + error);
          })
        } else {
          let distributivoDTO = {
            acaMallaAsignatura: iterator.maasCodigo,
            curparCodigo: elemento.curparCodigo,
            disEstado: 1,
            docCodigo: this.docenteHispanaChild.codPersona,
            curCodigo: iterator.curCodigo,
            reanleCodigo: iterator.reanleCodigo,
            tutor: 0,
            insestCodigo: this.establecimiento
          }

          await this.asignacionOrdinariaService.postGuardarDistributivo(distributivoDTO).then(data => {
            iterator.disCodigo = data.objeto.disCodigo;
            iterator.curparCodigo = data.objeto.curparCodigo;
            iterator.insestCodigo = data.objeto.insestCodigo;
            elemento.listaAsignaturasSeleccionadasAnterior.push(iterator)
          }, error => {
            this.mensajeService.mensajeError('', 'Error al guardar registro\n' + error);
          })
        }
      }

    }



    this.spinnerService.show();

    this.spinnerService.hide();
    this.mensajeService.mensajeCorrecto('', constante.mensajeExitoGuardar);

    if (this.spinnerService.hide) {
      this.router.navigate(["pages/asignacionDocenteOrdinaria"])
    }

  }
  compararMalla(i1, i2) {
    return i1 === undefined || i2 === undefined || i1 === null || i2 === null ? false : i1.maasCodigo === i2.maasCodigo;
  }

  volverSeleccionOferta() {
    this.deshabilitarAsignacionOutput.emit(false)
  }

  consultarEspecialidades(figprobtCodigo) {
    this.asignacionOrdinariaService.getEspecialidadPorFigproCodigo(figprobtCodigo).then(data => {
      this.listaEspecialidad = data.listado
    })
  }
  consultarJornadas() {
    this.asignacionOrdinariaService.getJornadas().then(data => {
      this.listaJornada = data.listado
    })
  }
  applyFilter(nombreGrado) {
    // const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = nombreGrado.trim().toLowerCase();
  }
  filtrarNombresRepetidos(listaJornada) {
    this.listaNombreGrados = [];
    var hash1 = {};
    this.listaNombreGrados = listaJornada.filter(function (current) {
      var exists = !hash1[current.graDescripcion];
      hash1[current.graDescripcion] = true;
      return exists;
    });
  }
  async filtrarJornadasRepetidos(listaJornada) {
    this.listaJornada = [];
    var hash1 = {};
    let jornadasRepetidas
    jornadasRepetidas = listaJornada.filter(function (current) {
      var exists = !hash1[current.jorCodigo];
      hash1[current.jorCodigo] = true;
      return exists;
    });
    for (const objeto of jornadasRepetidas) {
      await this.asignacionOrdinariaService.getJornadaPorJorCodigo(objeto.jorCodigo).then(data => {
        this.listaJornada.push(data.objeto)
      })
    }
  }
}