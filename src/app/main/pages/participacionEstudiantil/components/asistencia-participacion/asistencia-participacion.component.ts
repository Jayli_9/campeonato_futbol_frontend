import { Component, OnInit, ViewChild } from "@angular/core"
import { IPpeSubDenominacion } from "../../interfaces/IPpeSubdenominacion"
import { IofeParalelo } from "app/main/pages/calificacionOrdinaria/interfaces/IofeParalelo"
import { ParticipacionEstudiantilService } from "../../services/participacion-estudiantil.service"
import { MatSelectChange } from "@angular/material/select"
import { IPpeInscripcionSubdeno } from "../../interfaces/IPpeInscripcionSubdeno"
import { NgxSpinnerService } from "ngx-spinner"
import { MensajesService } from "app/main/pages/asignacionDocentesOrdinaria/servicios/mensajes.service"
import { MatTableDataSource } from "@angular/material/table"
import { MatPaginator, MatPaginatorIntl } from "@angular/material/paginator"
import { MatSort } from "@angular/material/sort"
import { IPpeTablaEquivalenciasPre } from "../../interfaces/IPpeTablaEquivalenciasPre"
import { GieeService } from "app/main/pages/asignacionDocentes/servicios/giee.service"
import { CatalogoService } from "app/main/pages/asignacionDocentes/servicios/catalogo.service"
import { CatAnioElectivo } from "app/main/pages/asignacionDocentes/interface/cat_anio"
import { IRegistroAsisCalifDTO } from "../../interfaces/IRegistroAsisCalifDTO"
import { IPpeRegistroAsisCalif } from "../../interfaces/IPpeRegistroAsisCalif"
import { IPpeDistributivoBuenVivir } from "../../interfaces/IPpeDistributivoBuenVivir"
import { UntypedFormControl } from "@angular/forms"

@Component({
  selector: "app-asistencia-participacion",
  templateUrl: "./asistencia-participacion.component.html",
  styleUrls: ["./asistencia-participacion.component.scss"],
})
export class AsistenciaParticipacionComponent implements OnInit {
  // Tabla
  public displayedColumns: string[] = [
    "estNombres",
    "pregasisNroHoras",
    "calificacion",
  ]
  public dataSource: any
  //variables paginación y orden
  @ViewChild(MatPaginator) paginator!: MatPaginator
  @ViewChild(MatSort) sort!: MatSort
  //Datos Institucion
  public listaInstitucion: any
  public informacionSede: any
  public codAmie: any
  public nombreInstitucion
  public informacionInstitucion
  public codInstitucion
  public codgiRegimen
  public listaRegimen
  public regimen
  public reanleCodigo: number = null
  public anioElectivoCodigo
  public selectParalelo
  // Listas
  public subDenominaciones: IPpeSubDenominacion[]
  public subDenominacionesParCodigos: number[]
  public paralelos: IofeParalelo[]
  public paralelosCurrent: IofeParalelo[]
  public estudiantes: IPpeInscripcionSubdeno[]
  public estudiantesFormato: any[]
  public equivalencias: IPpeTablaEquivalenciasPre[]
  public listaAnio: CatAnioElectivo[] = []
  private docente: any
  private distributivo: IPpeDistributivoBuenVivir[]
  // seleccionados
  public subDenominacionSelected: number
  public paraleloSelected: number

  constructor(
    private participacionEstudiantilService: ParticipacionEstudiantilService,
    private spinnerService: NgxSpinnerService,
    private mensajeService: MensajesService,
    private matPaginatorIntl: MatPaginatorIntl,
    private CatalogoService: CatalogoService,
    private GieeService: GieeService
  ) {
    this.initComponents()
    this.listarInformacion()
  }

  async ngOnInit(): Promise<any> {
    this.listaInstitucion = JSON.parse(localStorage.getItem("currentUser"))
    this.informacionSede = this.listaInstitucion.sede
    this.nombreInstitucion = this.informacionSede.descripcion
    this.codAmie = this.informacionSede.nemonico
    //buscar la institucion y regimen
    this.buscarRegiment(this.codAmie)
  }

  buscarRegiment(cod) {
    this.spinnerService.show()
    //buscar la institucion
    this.GieeService.getDatosInstitucion(cod).then(
      (data) => {
        this.informacionInstitucion = data.objeto
        for (let dato in this.informacionInstitucion) {
          this.codInstitucion = this.informacionInstitucion.insCodigo
          this.codgiRegimen = this.informacionInstitucion.regimenes[0].regCodigo
        }

        this.CatalogoService.getRegimen(this.codgiRegimen).then((data) => {
          this.listaRegimen = data.objeto
          //exportar el objeto
          this.regimen = this.listaRegimen?.regDescripcion
        })
        //listar regimenAñolectivo
        this.CatalogoService.getAnioElectivo(this.codgiRegimen).then((data) => {
          this.listaAnio = data.listado
          for (let dato of this.listaAnio) {
            if (dato.reanleTipo === "A") {
              this.reanleCodigo = dato.reanleCodigo
            }
          }
        })
      },
      (error) => {
        // alert(error);
        // alert("Usuario sin sede");
      }
    )
    this.spinnerService.hide()
  }

  async listarInformacion() {
    this.spinnerService.show()
    // await this.listarSubDenominaciones()
    await this.listarParalelos()
    await this.buscarDocente(this.listaInstitucion.identificacion)
    await this.listarSubdenominacionesPorDocente()
    await this.listarEquivalenciasPre()
    this.spinnerService.hide()
  }

  async handlerSubDenominacion(event: MatSelectChange) {
    if (!!event.value) {
      this.limpiarTabla()
      this.subDenominacionSelected = event.value
      let paralelos: any[] = this.distributivo.filter(
        (distributivo) =>
          distributivo.ppeSubdenominacion.psdenCodigo ===
          this.subDenominacionSelected
      )
      paralelos = paralelos.map((paralelo) => paralelo.parCodigo)
      this.paralelosCurrent = this.paralelos.filter((paralelo) =>
        paralelos.includes(paralelo.parCodigo)
      )
      if (!!this.subDenominacionSelected && !!this.paraleloSelected) {
        this.spinnerService.show()
        await this.buscarAlumnos(
          this.subDenominacionSelected,
          this.paraleloSelected
        )
      }
    }
  }

  async handlerParalelo(event: MatSelectChange) {
    if (!!event.value) {
      this.paraleloSelected = event.value
      if (!!this.subDenominacionSelected && !!this.paraleloSelected) {
        this.spinnerService.show()
        await this.buscarAlumnos(
          this.subDenominacionSelected,
          this.paraleloSelected
        )
      }
    }
  }

  async buscarAlumnos(psdenCodigo: number, parCodigo: number) {
    try {
      this.estudiantes = (
        await this.participacionEstudiantilService.getInscripcionesSubdenoPorSubdenoCodigoAndParCodigo(
          psdenCodigo,
          parCodigo
        )
      ).listado
      await this.cruzarLista(this.estudiantes)
    } catch (error) {
      console.log("error: ", error)
    }
  }

  async listarParalelos() {
    try {
      this.paralelos = (
        await this.participacionEstudiantilService.getParalelos()
      ).listado
    } catch (error) {
      console.log(error)
    }
  }

  async listarEquivalenciasPre() {
    try {
      this.equivalencias = (
        await this.participacionEstudiantilService.getTablaEquivalencias()
      ).listado
    } catch (error) {
      console.log(error)
    }
  }

  async listarSubDenominaciones() {
    try {
      this.subDenominaciones = (
        await this.participacionEstudiantilService.getSubDenominaciones()
      ).listado
    } catch (error) {
      console.log(error)
    }
  }

  async buscarDocente(identificacion: string) {
    try {
      this.docente = (
        await this.participacionEstudiantilService.getDocentePorCedula(
          identificacion
        )
      ).objeto
    } catch (error) {
      console.log(error)
    }
  }

  async listarSubdenominacionesPorDocente() {
    try {
      this.distributivo = (
        await this.participacionEstudiantilService.listarDistriBuenvivirPorDocCodigo(
          this.docente.codPersona
        )
      ).listado
      // Extraer la columna específica de cada objeto
      const subDenominaciones = this.distributivo.map(
        (objeto) => objeto.ppeSubdenominacion
      )
      // Filtrar los repetidos
      this.subDenominaciones = this.eliminarDuplicados(
        subDenominaciones,
        "psdenCodigo"
      )
    } catch (error) {
      console.log(error)
    }
  }

  async listarNotasRegistradas(matPresencial: number[]) {
    try {
      return (
        await this.participacionEstudiantilService.listarRegistroAsisCalifListPorMatPresencial(
          matPresencial
        )
      ).listado
    } catch (error) {
      console.log(error)
    }
  }

  async cruzarLista(estudiantes: IPpeInscripcionSubdeno[]) {
    try {
      let estudiantesLista: any[] = estudiantes.map(async (estudiante) => {
        return await this.participacionEstudiantilService.getEstudiante(
          estudiante.estCodigo
        )
      })
      this.estudiantesFormato = []
      let estData = Promise.all(estudiantesLista)
      let notas: IPpeRegistroAsisCalif[] = []
      Promise.resolve(estData).then(async (res) => {
        const matPresenciales = this.estudiantes.map((est) => {
          return est.matpreCodigo
        })
        if (matPresenciales.length !== 0) {
          notas = await this.listarNotasRegistradas(matPresenciales)
        } else {
          this.spinnerService.hide()
          this.mensajeService.mensajeAdvertencia(
            "Error",
            "No existen datos para los parámetros seleccionados."
          )
          return
        }
        this.estudiantes.forEach((estudiante) => {
          let notaTemp = notas.find(
            (nota) => nota.matpreCodigo === estudiante.matpreCodigo
          )
          this.estudiantesFormato.push({
            ...estudiante,
            estNombres: res.find(
              (est: any) => est.objeto.estCodigo === estudiante.estCodigo
            ).objeto?.estNombres,
            pregasisNroHoras: notaTemp ? notaTemp.pregasisNroHoras : "",
            calificacion: notaTemp
              ? notaTemp.ppeTablaEquivalenciasPre.ptequiCalif
              : "",
            pinsCodigo: estudiante.pinsCodigo,
            reanleCodigo: this.reanleCodigo,
            matpreCodigo: estudiante.matpreCodigo,
            pregasisFecCreacion: new Date(),
            pregasisEstado: 1,
            ppeTablaEquivalenciasPre: notaTemp
              ? notaTemp.ppeTablaEquivalenciasPre.ptequiCodigo
              : "",
            pregasisCodigo: notaTemp ? notaTemp.pregasisCodigo : null,
          })
        })
        this.validarDatosTabla(this.estudiantesFormato)
      })
    } catch (error) {}
  }

  validarDatosTabla(estudiantes: IPpeInscripcionSubdeno[]) {
    if (estudiantes.length > 0) {
      this.setDataTabla(estudiantes)
    } else {
      this.setDataTabla([])
      this.mensajeService.mensajeAdvertencia(
        "Error",
        "No existen datos para los parámetros seleccionados."
      )
    }
  }

  setDataTabla(estudiantes: IPpeInscripcionSubdeno[]) {
    this.dataSource = new MatTableDataSource(estudiantes)
    this.dataSource.paginator = this.paginator
    this.dataSource.sort = this.sort
    this.dataSource.paginator = this.paginator
    this.matPaginatorIntl.itemsPerPageLabel = "Elementos por página:"
    this.matPaginatorIntl.nextPageLabel = "Siguiente:"
    this.matPaginatorIntl.previousPageLabel = "Anterior:"
    this.spinnerService.hide()
  }

  calcularCalificacion(i: number) {
    if (
      this.estudiantesFormato[i].pregasisNroHoras >= 66 &&
      this.estudiantesFormato[i].pregasisNroHoras <= 80
    ) {
      let equivalencia: IPpeTablaEquivalenciasPre = this.equivalencias.find(
        (equivalencia) =>
          equivalencia.ptequiNroInicio <=
            this.estudiantesFormato[i].pregasisNroHoras &&
          this.estudiantesFormato[i].pregasisNroHoras <=
            equivalencia.ptequiNroFin
      )

      this.estudiantesFormato[i].calificacion = equivalencia.ptequiCalif
      this.estudiantesFormato[i].ppeTablaEquivalenciasPre =
        equivalencia.ptequiCodigo
    } else {
      this.estudiantesFormato[i].pregasisNroHoras = ""
      this.estudiantesFormato[i].calificacion = ""
      this.mensajeService.mensajeError(
        "Error",
        "La asistencia mínima es de 66 y máxima de 80"
      )
    }
  }

  async guardarAsistencia() {
    if (this.validarAsistencia()) {
      this.spinnerService.show()
      let estudiantes: IRegistroAsisCalifDTO[] = this.estudiantesFormato.map(
        (est) => {
          return {
            pregasisCodigo: est.pregasisCodigo,
            ppeInscripcionSubdeno: est.pinsCodigo,
            pregasisEstado: est.pregasisEstado,
            pregasisFecCreacion: est.pregasisFecCreacion,
            reanleCodigo: est.reanleCodigo,
            matpreCodigo: est.matpreCodigo,
            ppeTablaEquivalenciasPre: est.ppeTablaEquivalenciasPre,
            pregasisNroHoras: est.pregasisNroHoras,
          }
        }
      )
      try {
        await this.participacionEstudiantilService.guardarRegistroAsisCalifList(
          estudiantes
        )
        this.mensajeService.mensajeCorrecto(
          "Éxito",
          "Datos almacenados correctamente."
        )
        await this.buscarAlumnos(
          this.subDenominacionSelected,
          this.paraleloSelected
        )
      } catch (error) {
        this.mensajeService.mensajeError("Error", "Intente más tarde")
      }
    } else {
      this.mensajeService.mensajeError(
        "Error",
        "Ingrese todas las asistencias antes de guardar"
      )
    }
    this.spinnerService.hide()
  }

  validarAsistencia(): boolean {
    return this.estudiantesFormato.every(
      (estudiante) =>
        (estudiante.pregasisNroHoras !== "" &&
          estudiante.pregasisNroHoras !== null) ||
        (estudiante.calificacion !== "" && estudiante.calificacion !== null)
    )
  }

  inputInvalid(i: number): boolean {
    return (
      this.estudiantesFormato[i].pregasisNroHoras &&
      !/^(6[6-9]|7[0-9]|80)$/.test(this.estudiantesFormato[i].pregasisNroHoras)
    )
  }

  limpiarTabla() {
    this.estudiantes = []
    this.estudiantesFormato = []
    this.setDataTabla(this.estudiantesFormato)
    this.paraleloSelected = null
    this.paralelosCurrent = []
    this.selectParalelo.reset()
  }

  initComponents() {
    this.subDenominaciones = []
    this.paralelos = []
    this.paralelosCurrent = []
    this.estudiantes = []
    this.estudiantesFormato = []
    this.equivalencias = []
    this.subDenominacionSelected = null
    this.paraleloSelected = null
    this.docente = null
    this.distributivo = []
    this.selectParalelo = new UntypedFormControl()
  }

  eliminarDuplicados(arr: any[], campo: string) {
    const resultado = []
    const valoresUnicos = new Set()

    for (const item of arr) {
      const valorCampo = item[campo]
      if (!valoresUnicos.has(valorCampo)) {
        valoresUnicos.add(valorCampo)
        resultado.push(item)
      }
    }

    return resultado
  }
}
