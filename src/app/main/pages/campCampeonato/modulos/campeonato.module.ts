import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { CoreCommonModule } from '@core/common.module';
import { ContentHeaderModule } from 'app/layout/components/content-header/content-header.module';
import { MaterialModule } from 'app/main/shared/material/material.module';
import { CampeonatoPrincipalComponent } from '../componentes/campeonato-principal/campeonato-principal.component';
import { RUTA_CAMPEONATO } from '../rutas/campeonato.routing';
import { NgxSpinnerModule } from 'ngx-spinner';
import { MatSortModule } from '@angular/material/sort';
import { MatTableModule } from '@angular/material/table';
import { EquipoPrincipalComponent } from '../componentes/equipo-principal/equipo-principal.component';
import { MatInputModule } from '@angular/material/input';
import { FileUploadModule } from 'ng2-file-upload';
import { EstadioPrincipalComponent } from '../componentes/estadio-principal/estadio-principal.component';
import { FormEstadioComponent } from '../componentes/estadio-principal/form-estadio/form-estadio.component';
import { FormEquipoComponent } from '../componentes/equipo-principal/form-equipo/form-equipo.component';
import { TablaPrincipalComponent } from '../componentes/tabla-principal/tabla-principal.component';

@NgModule({
  declarations: [
    CampeonatoPrincipalComponent,
    EquipoPrincipalComponent,
    EstadioPrincipalComponent,
    FormEstadioComponent,
    FormEquipoComponent,
    TablaPrincipalComponent
  ],
  imports: [
    CommonModule,
    RouterModule.forChild(RUTA_CAMPEONATO),
    FormsModule,
    ReactiveFormsModule,
    MaterialModule,
    ContentHeaderModule,
    CoreCommonModule,
    NgxSpinnerModule,
    MatSortModule,
    MatTableModule,
    MatInputModule,
    FileUploadModule,
  ]
})
export class CampeonatoModule { }