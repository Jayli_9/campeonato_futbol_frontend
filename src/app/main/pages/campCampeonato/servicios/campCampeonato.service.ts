import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'environments/environment';
import { AuthenticationService } from '../../../../auth/service/authentication.service';

//exportacion interfaces
import { ResponseGenerico } from '../../asignacionDocentes/interface/response-generico';
import { ICampCampeonato } from 'app/compartidos/interfaces/ICampCampeonato';
import { ICampEquipo } from 'app/compartidos/interfaces/ICampEquipo';
import { ICampEstadio } from 'app/compartidos/interfaces/ICampEstadio';

@Injectable({
  providedIn: 'root'
})
export class campCampeonatoService {

  constructor(private _http: HttpClient, private _authService: AuthenticationService) { }  //obtener lista de distributivo;
  getCampeonatoPaginado(numPagina: number, itemsPagina: number): Promise<ResponseGenerico<ICampCampeonato>> {
    return new Promise((resolve, reject) => {
      this._http.get(`${environment.url_campeonatos}/private/listarCampeonatosPaginado/${numPagina}/${itemsPagina}`).subscribe((response: ResponseGenerico<ICampCampeonato>) => {
        resolve(response);
      }, reject);
    })
  }
  getEquipoPaginadoPorCodigoCampenato(numPagina: number, itemsPagina: number, codigoCampeonato: number): Promise<ResponseGenerico<ICampEquipo>> {
    const objetoConsulta = {
      campCodigo: codigoCampeonato,
      numeroItems: itemsPagina,
      numeroPagina: numPagina
    }
    return new Promise((resolve, reject) => {
      this._http.post(`${environment.url_campeonatos}/private/listarEquiposPaginadoPorCampCodigo`, objetoConsulta).subscribe((response: ResponseGenerico<ICampEquipo>) => {
        resolve(response);
      }, reject);
    })
  }
  getEquipoPorCodigoCampenato(codigoCampeonato: number): Promise<ResponseGenerico<ICampEquipo>> {
    
    return new Promise((resolve, reject) => {
      this._http.get(`${environment.url_campeonatos}/private/listarEquiposPorCampCodigo/${codigoCampeonato}`).subscribe((response: ResponseGenerico<ICampEquipo>) => {
        resolve(response);
      }, reject);
    })
  }
  
  postGuardarEquipo(equipoNuevo:ICampEquipo): Promise<ResponseGenerico<ICampEquipo>> {  
    return new Promise((resolve, reject) => {
      this._http.post(`${environment.url_campeonatos}/private/guardarEquipos`, equipoNuevo).subscribe((response: ResponseGenerico<ICampEquipo>) => {
        resolve(response);
      }, reject);
    })
  }
  getEstadioPaginadoPorCodigoCampenato(numPagina: number, itemsPagina: number, codigoCampeonato: number): Promise<ResponseGenerico<ICampEstadio>> {
    const objetoConsulta = {
      campCodigo: codigoCampeonato,
      numeroItems: itemsPagina,
      numeroPagina: numPagina
    }
    return new Promise((resolve, reject) => {
      this._http.post(`${environment.url_campeonatos}/private/listarEstadiosPaginadoPorCampCodigo`, objetoConsulta).subscribe((response: ResponseGenerico<ICampEstadio>) => {
        resolve(response);
      }, reject);
    })
  }
  postGuardarEstadio(estadioNuevo:ICampEstadio): Promise<ResponseGenerico<ICampEstadio>> {  
    return new Promise((resolve, reject) => {
      this._http.post(`${environment.url_campeonatos}/private/guardarEstadio`, estadioNuevo).subscribe((response: ResponseGenerico<ICampEstadio>) => {
        resolve(response);
      }, reject);
    })
  }
} 
