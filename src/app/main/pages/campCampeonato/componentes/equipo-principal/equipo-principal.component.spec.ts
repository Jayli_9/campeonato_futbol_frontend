import { ComponentFixture, TestBed } from '@angular/core/testing';

import { EquipoPrincipalComponent } from './equipo-principal.component';

describe('EquipoPrincipalComponent', () => {
  let component: EquipoPrincipalComponent;
  let fixture: ComponentFixture<EquipoPrincipalComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ EquipoPrincipalComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(EquipoPrincipalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
