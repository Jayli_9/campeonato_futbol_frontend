import { Component, EventEmitter, Input, OnInit, Output, ViewChild } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { ICampCampeonato } from 'app/compartidos/interfaces/ICampCampeonato';
import { NgxSpinnerService } from 'ngx-spinner';
import { campCampeonatoService } from '../../servicios/campCampeonato.service';
import { MatDialog } from '@angular/material/dialog';
import { FormEquipoComponent } from './form-equipo/form-equipo.component';
import { ICampEquipo } from 'app/compartidos/interfaces/ICampEquipo';
import { MensajesService } from 'app/main/pages/asignacionDocentesOrdinaria/servicios/mensajes.service';

@Component({
  selector: 'app-equipo-principal',
  templateUrl: './equipo-principal.component.html',
  styleUrls: ['./equipo-principal.component.scss']
})
export class EquipoPrincipalComponent implements OnInit {
  //recursos heredados
  @Input() campeonatoSeleccionadoChild: ICampCampeonato;
  //Outputs
  @Output() campeonatoPrincipalOutput: EventEmitter<boolean>;
  public nombresColumnas: string[] = ['numero', 'nombre', 'descripcion', 'lugar', 'logo', 'acciones'];
  dataSource: MatTableDataSource<ICampCampeonato>;
  //variables paginación y orden
  @ViewChild(MatPaginator) paginator!: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;
  //paginacion
  public listaPaginado: number[] = []
  public paginaActual: number = 0;
  public numeroItems: number = 10;
  public paginasTotales: number;
  constructor(private spinnerService: NgxSpinnerService, private campCampeonatoService: campCampeonatoService,
    public dialog: MatDialog, private mensajeService: MensajesService) {
    this.campeonatoPrincipalOutput = new EventEmitter<boolean>()
  }

  ngOnInit(): void {
    this.spinnerService.show()
    this.consultarEquipos(this.paginaActual, this.numeroItems, this.campeonatoSeleccionadoChild.campCodigo);
    // this.spinnerService.hide()
  }
  consultarEquipos(paginaActual: number, numeroItemas: number, campCodigo: number) {
    this.campCampeonatoService.getEquipoPaginadoPorCodigoCampenato(paginaActual, numeroItemas, campCodigo).then(respuesta => {
      this.paginasTotales = respuesta.totalPaginas;
      this.listaPaginado = Array.from({ length: this.paginasTotales }, (_, i) => i + 1);
      this.agregarDatosTabla(respuesta.listado);
      this.spinnerService.hide();
    },
      (error) => {
        this.spinnerService.hide()
      })
  }
  agregarDatosTabla(listaEquipos) {
    this.dataSource = new MatTableDataSource<ICampCampeonato>(listaEquipos);
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
    // this.matPaginatorIntl.itemsPerPageLabel = 'Elementos por página:';
    // this.matPaginatorIntl.nextPageLabel = 'Siguiente:';
    // this.matPaginatorIntl.previousPageLabel = 'Anterior:';
  }
  regresarCampeonatoPrincipal(regresar: boolean) {
    this.campeonatoPrincipalOutput.emit(regresar)
  }
  openDialogFormEquipo(equipoEditar: ICampEquipo): void {
    const dialogRef = this.dialog.open(FormEquipoComponent, {
      width: '600px',
      data: equipoEditar as ICampEquipo,
      disableClose: true
    });
    dialogRef.afterClosed().subscribe((result: any) => {
      if (!!result) {
        this.guardaEquipo(result)
      }
    })
  }
  async guardaEquipo(data: ICampEquipo) {
    this.spinnerService.show()
    const equipoNuevoNueva: ICampEquipo = {
      equiCodigo: data.equiCodigo,
      equiDescripcion: data.equiDescripcion,
      equiEstado: 1,
      equiLogo: data.equiLogo,
      equiLugar: data.equiLugar,
      equiNombre: data.equiNombre,
      campCampeonato: this.campeonatoSeleccionadoChild
    }
    try {
      await this.campCampeonatoService.postGuardarEquipo(equipoNuevoNueva)
      this.mensajeService.mensajeCorrecto("Éxito", "Equipo Creado")
      this.consultarEquipos(this.paginaActual, this.numeroItems, this.campeonatoSeleccionadoChild.campCodigo);
    } catch (error) {
      console.log(error)
    }
    this.spinnerService.hide()
  }
  async onPageChange(page: number): Promise<void> {
    this.paginaActual = page;
    this.consultarEquipos(this.paginaActual, this.numeroItems, this.campeonatoSeleccionadoChild.campCodigo);
  }
  async eliminarEquipo(equipoEliminado: ICampEquipo) {
    this.mensajeService.mensajeConfirmacion(equipoEliminado.equiNombre, "¿Eliminar?").then(async confirmado => {
      if (confirmado) {
        try {
          equipoEliminado.equiEstado = 0;
          await this.campCampeonatoService.postGuardarEquipo(equipoEliminado)
          this.consultarEquipos(this.paginaActual, this.numeroItems, this.campeonatoSeleccionadoChild.campCodigo);
          this.mensajeService.mensajeCorrecto("Éxito", "Equipo Eliminado")
        } catch (error) {
          console.log(error)
          this.mensajeService.mensajeError("Éxito", "Equipo No Eliminado")
        }
        this.spinnerService.hide()
      } else {
        this.spinnerService.hide()
      }
    })



  }
}
