import { Component, ElementRef, Inject, OnInit, ViewChild } from '@angular/core';
import { UntypedFormControl, UntypedFormGroup, Validators } from '@angular/forms';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { ICampCampeonato } from 'app/compartidos/interfaces/ICampCampeonato';
import { ICampEquipo } from 'app/compartidos/interfaces/ICampEquipo';
import { NgxSpinnerService } from 'ngx-spinner';
import { FileUploader, FileUploaderOptions, ParsedResponseHeaders } from 'ng2-file-upload';
import { DomSanitizer } from '@angular/platform-browser';
@Component({
  selector: 'app-form-equipo',
  templateUrl: './form-equipo.component.html',
  styleUrls: ['./form-equipo.component.scss']
})
export class FormEquipoComponent implements OnInit {
  @ViewChild('fileInput') fileInput!: ElementRef<HTMLInputElement>;
  //Forms
  public equipoForm: UntypedFormGroup
  //Variables
  public uploader: FileUploader;
  public errorTamanioArchivo: Boolean = false;
  constructor(public dialogRef: MatDialogRef<FormEquipoComponent>, private spinnerService: NgxSpinnerService,
    private sanitizer: DomSanitizer,
    @Inject(MAT_DIALOG_DATA) public equipoEditar: ICampEquipo) {

  }

  ngOnInit(): void {
    this.initEquipoForm();

  }
  initEquipoForm() {
    this.equipoForm = new UntypedFormGroup({
      equiCodigo: new UntypedFormControl(this.equipoEditar ? this.equipoEditar?.equiCodigo : null),
      equiDescripcion: new UntypedFormControl(this.equipoEditar ? this.equipoEditar?.equiDescripcion : '', [Validators.required]),
      equiEstado: new UntypedFormControl('1'),
      equiLogo: new UntypedFormControl(this.equipoEditar ? this.equipoEditar?.equiLogo : null),
      equiLugar: new UntypedFormControl(this.equipoEditar ? this.equipoEditar?.equiLugar : ''),
      equiNombre: new UntypedFormControl(this.equipoEditar ? this.equipoEditar?.equiNombre : '', [Validators.required]),
      campCampeonato: new UntypedFormControl(this.equipoEditar?.campCampeonato),
    })
    this.spinnerService.hide();
  }
  guardar() {
    if (this.equipoForm.status === "VALID") {
      const equipoNuevo: ICampEquipo = this.equipoForm.value;
      this.dialogRef.close(equipoNuevo)
    }
  }

  //////// Capturar Imagen 
  capturarImagen(archivo: any) {
    if (!archivo.target.files[0]) {
      return;
    }
    const archivoCapturado = archivo.target.files[0];
    if (archivoCapturado.size > 200 * 1024) { // "200 Mb"
      this.errorTamanioArchivo = true;
      const inputElement = this.fileInput.nativeElement;
      inputElement.value = '';
      return;
    }
    this.errorTamanioArchivo = false;
    this.toBase64(archivoCapturado).then(base64String => {
      this.equipoForm.patchValue({
        equiLogo: base64String
      });
    }).catch(error => {
    });
  }

  private toBase64(Archivo: File): Promise<string> {
    return new Promise((resolve, reject) => {
      const reader = new FileReader();
      reader.readAsDataURL(Archivo);
      reader.onload = () => {
        const base64String = reader.result as string;
        const base64 = base64String.split(',')[1]; // Obtener solo la cadena base 64
        resolve(base64);
      };
      reader.onerror = error => reject(error);
    });
  }

}
