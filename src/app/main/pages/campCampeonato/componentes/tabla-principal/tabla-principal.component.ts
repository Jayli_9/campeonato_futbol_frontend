import { Component, Input, OnInit } from '@angular/core';
import { ICampCampeonato } from 'app/compartidos/interfaces/ICampCampeonato';
import { NgxSpinnerService } from 'ngx-spinner';
import { campCampeonatoService } from '../../servicios/campCampeonato.service';
import { ICampEquipo } from 'app/compartidos/interfaces/ICampEquipo';
import { IPartido } from 'app/compartidos/interfaces/IPartido';

@Component({
  selector: 'app-tabla-principal',
  templateUrl: './tabla-principal.component.html',
  styleUrls: ['./tabla-principal.component.scss']
})
export class TablaPrincipalComponent implements OnInit {
  //recursos heredados
  @Input() campeonatoSeleccionadoChild: ICampCampeonato;
  //Listas
  public listaEquipos: ICampEquipo[] = [];
  public listaPartidos: any[] = [];
  constructor(private spinnerService: NgxSpinnerService, private campCampeonatoService: campCampeonatoService,) { }

  async ngOnInit(): Promise<void> {
    await this.consultarEquipos(this.campeonatoSeleccionadoChild.campCodigo);
    // const listaEquipos = ["Equipo A", "Equipo B", "Equipo C", "Equipo D", "Equipo E", "Equipo F", "Equipo G"];
    // const listaEquipos = ["Equipo A", "Equipo B", "Equipo C", "Equipo D","Equipo E", "Equipo F", "Equipo G", "Equipo H"];
    const cruses = this.generarCruces(this.listaEquipos);
    // const cruses = this.generarCruces(this.listaEquipos);
    const fechasImprimir = this.agruparPorFechas(cruses, this.listaEquipos, (this.listaEquipos.length), Math.trunc(this.listaEquipos.length / 2))
    this.listaPartidos = fechasImprimir;

  }
  async consultarEquipos(campCodigo: number) {
    await this.campCampeonatoService.getEquipoPorCodigoCampenato(campCodigo).then(respuesta => {
      this.listaEquipos = respuesta.listado;
    },
      (error) => {
        this.spinnerService.hide()
      })
  }
  agruparPorFechas(cruces: IPartido[], listaEquipos: ICampEquipo[], numeroFechas1: number, partidosPorFecha: number): any[][] {
    let numeroFechas = numeroFechas1;
    if (numeroFechas % 2 == 0) {
      numeroFechas = numeroFechas - 1;
    }
    const fechas: any[][] = [];
    let equiposDisponibles: Set<number> = new Set(listaEquipos.map(equipo => equipo.equiCodigo));
    let crucesDisponibles = this.shuffle([...cruces]);

    while (fechas.length < numeroFechas) {
      const fecha: any[] = [];
      let equiposFecha: Set<number> = new Set(); // Equipos asignados en esta fecha
      let crucesFecha = [...crucesDisponibles];

      for (let i = 0; i < crucesFecha.length && fecha.length < partidosPorFecha; i++) {
        const cruce = crucesFecha[i];
        if (equiposDisponibles.has(cruce.equipoLocal.equiCodigo) && equiposDisponibles.has(cruce.equipoVisitante.equiCodigo)) {
          // Verificar que los equipos del cruce no se repitan en la misma fecha
          if (!equiposFecha.has(cruce.equipoLocal.equiCodigo) && !equiposFecha.has(cruce.equipoVisitante.equiCodigo)) {
            fecha.push(cruce);
            equiposFecha.add(cruce.equipoLocal.equiCodigo);
            equiposFecha.add(cruce.equipoVisitante.equiCodigo);
            equiposDisponibles.delete(cruce.equipoLocal.equiCodigo);
            equiposDisponibles.delete(cruce.equipoVisitante.equiCodigo);
            crucesDisponibles = crucesDisponibles.filter(c => c !== cruce);
          }
        }
      }

      if (fecha.length < partidosPorFecha) {
        // No se pudieron asignar suficientes partidos para esta fecha, se reordena la lista de cruces y se reinicia el proceso
        crucesDisponibles = this.shuffle([...cruces]);
        equiposDisponibles = new Set(listaEquipos.map(equipo => equipo.equiCodigo));
        fechas.length = 0; // Vaciar fechas para empezar de nuevo
        continue;
      }

      if (fecha.length === 0) {
        // No se pudieron asignar partidos para esta fecha, se agrega una fecha vacía
        fechas.push([]);
        continue;
      }

      fechas.push(fecha);

      equiposDisponibles = new Set(listaEquipos.map(equipo => equipo.equiCodigo));
    }

    return fechas;
  }

  generarCruces(listaEquipos: ICampEquipo[]): IPartido[] {
    const cruces: IPartido[] = [];
    const totalEquipos = listaEquipos.length;
    const equiposVisitados = new Set();
    for (let i = 0; i < totalEquipos; i++) {
      for (let j = i + 1; j < totalEquipos; j++) {
        const equipoLocal = listaEquipos[i];
        const equipoVisitante = listaEquipos[j];

        // Asignar aleatoriamente local o visitante
        let local, visitante;
        if (Math.random() < 0.5) {
          local = equipoLocal;
          visitante = equipoVisitante;
        } else {
          local = equipoVisitante;
          visitante = equipoLocal;
        }

        // Verificar que el cruce no se haya generado previamente
        const cruce = `${local.equiCodigo}-${visitante.equiCodigo}`;
        if (!equiposVisitados.has(cruce)) {
          cruces.push({ equipoLocal: local, equipoVisitante: visitante });
          equiposVisitados.add(cruce);
        }
      }
    }

    return cruces;
  }
  // generarCruces1(listaEquipos: string[]): any[] {
  //   const cruces: any[] = [];
  //   const totalEquipos = listaEquipos.length;
  //   const equiposVisitados = new Set();

  //   for (let i = 0; i < totalEquipos; i++) {
  //     for (let j = i + 1; j < totalEquipos; j++) {
  //       const equipoLocal = listaEquipos[i];
  //       const equipoVisitante = listaEquipos[j];

  //       // Asignar aleatoriamente local o visitante
  //       let local, visitante;
  //       if (Math.random() < 0.5) {
  //         local = equipoLocal;
  //         visitante = equipoVisitante;
  //       } else {
  //         local = equipoVisitante;
  //         visitante = equipoLocal;
  //       }

  //       // Verificar que el cruce no se haya generado previamente
  //       const cruce = `${local}-${visitante}`;
  //       if (!equiposVisitados.has(cruce)) {
  //         cruces.push({ equipoLocal: local, equipoVisitante: visitante });
  //         equiposVisitados.add(cruce);
  //       }
  //     }
  //   }

  //   return cruces;
  // }



  // agruparPorFechas1(cruces: any[], listaEquipos: string[], numeroFechas1: number, partidosPorFecha: number): any[][] {
  //   console.log("partidosPorFecha", partidosPorFecha);

  //   let numeroFechas = numeroFechas1;
  //   console.log(numeroFechas);

  //   if (numeroFechas % 2 == 0) {
  //     numeroFechas = numeroFechas - 1;
  //   }

  //   const fechas: any[][] = [];
  //   let equiposDisponibles: Set<string> = new Set(listaEquipos);
  //   let crucesDisponibles = this.shuffle([...cruces]);

  //   while (fechas.length < numeroFechas) {
  //     const fecha: any[] = [];
  //     let equiposFecha: Set<string> = new Set(); // Equipos asignados en esta fecha
  //     let crucesFecha = [...crucesDisponibles];

  //     for (let i = 0; i < crucesFecha.length && fecha.length < partidosPorFecha; i++) {
  //       const cruce = crucesFecha[i];
  //       if (equiposDisponibles.has(cruce.equipoLocal) && equiposDisponibles.has(cruce.equipoVisitante)) {
  //         // Verificar que los equipos del cruce no se repitan en la misma fecha
  //         if (!equiposFecha.has(cruce.equipoLocal) && !equiposFecha.has(cruce.equipoVisitante)) {
  //           fecha.push(cruce);
  //           equiposFecha.add(cruce.equipoLocal);
  //           equiposFecha.add(cruce.equipoVisitante);
  //           equiposDisponibles.delete(cruce.equipoLocal);
  //           equiposDisponibles.delete(cruce.equipoVisitante);
  //           crucesDisponibles = crucesDisponibles.filter(c => c !== cruce);
  //         }
  //       }
  //     }

  //     if (fecha.length < partidosPorFecha) {
  //       // No se pudieron asignar suficientes partidos para esta fecha, se reordena la lista de cruces y se reinicia el proceso
  //       crucesDisponibles = this.shuffle([...cruces]);
  //       equiposDisponibles = new Set(listaEquipos);
  //       fechas.length = 0; // Vaciar fechas para empezar de nuevo
  //       continue;
  //     }

  //     if (fecha.length === 0) {
  //       // No se pudieron asignar partidos para esta fecha, se agrega una fecha vacía
  //       fechas.push([]);
  //       continue;
  //     }

  //     fechas.push(fecha);

  //     equiposDisponibles = new Set(listaEquipos);
  //   }

  //   return fechas;
  // }
  shuffle(array: any[]): any[] {
    const newArray = [...array];
    for (let i = newArray.length - 1; i > 0; i--) {
      const j = Math.floor(Math.random() * (i + 1));
      [newArray[i], newArray[j]] = [newArray[j], newArray[i]];
    }
    return newArray;
  }

}
