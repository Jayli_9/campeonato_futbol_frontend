import { Component, ElementRef, Inject, OnInit, ViewChild } from '@angular/core';
import { UntypedFormGroup, UntypedFormControl, Validators } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { DomSanitizer } from '@angular/platform-browser';
import { FileUploader } from 'ng2-file-upload';
import { NgxSpinnerService } from 'ngx-spinner';
import { ICampEstadio } from 'app/compartidos/interfaces/ICampEstadio';

@Component({
  selector: 'app-form-estadio',
  templateUrl: './form-estadio.component.html',
  styleUrls: ['./form-estadio.component.scss']
})
export class FormEstadioComponent implements OnInit {
  @ViewChild('fileInput') fileInput!: ElementRef<HTMLInputElement>;
  //Forms
  public estadioForm: UntypedFormGroup
  //Variables
  public uploader: FileUploader;
  public errorTamanioArchivo: Boolean = false;
  constructor(public dialogRef: MatDialogRef<FormEstadioComponent>, private spinnerService: NgxSpinnerService,
    private sanitizer: DomSanitizer,
    @Inject(MAT_DIALOG_DATA) public estadioEditar: ICampEstadio) {

  }

  ngOnInit(): void {
    this.initEstadioForm();

  }
  initEstadioForm() {
    this.estadioForm = new UntypedFormGroup({
      estaCodigo: new UntypedFormControl(this.estadioEditar ? this.estadioEditar?.estaCodigo : null),
      estaDescripcion: new UntypedFormControl(this.estadioEditar ? this.estadioEditar?.estaDescripcion : '', [Validators.required]),
      estaEstado: new UntypedFormControl('1'),
      estaNombre: new UntypedFormControl(this.estadioEditar ? this.estadioEditar?.estaNombre : '', [Validators.required]),
      campCampeonato: new UntypedFormControl(this.estadioEditar?.campCampeonato),
    })
    this.spinnerService.hide();
  }
  guardar() {
    if (this.estadioForm.status === "VALID") {
      const estadioNuevo: ICampEstadio = this.estadioForm.value;
      this.dialogRef.close(estadioNuevo)
    }
  }

}
