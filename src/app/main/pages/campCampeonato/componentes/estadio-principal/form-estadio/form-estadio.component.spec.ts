import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FormEstadioComponent } from './form-estadio.component';

describe('FormEstadioComponent', () => {
  let component: FormEstadioComponent;
  let fixture: ComponentFixture<FormEstadioComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ FormEstadioComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(FormEstadioComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
