import { ComponentFixture, TestBed } from '@angular/core/testing';

import { EstadioPrincipalComponent } from './estadio-principal.component';

describe('EstadioPrincipalComponent', () => {
  let component: EstadioPrincipalComponent;
  let fixture: ComponentFixture<EstadioPrincipalComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ EstadioPrincipalComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(EstadioPrincipalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
