import { Component, EventEmitter, Input, OnInit, Output, ViewChild } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { ICampCampeonato } from 'app/compartidos/interfaces/ICampCampeonato';
import { ICampEstadio } from 'app/compartidos/interfaces/ICampEstadio';
import { MensajesService } from 'app/main/pages/asignacionDocentesOrdinaria/servicios/mensajes.service';
import { NgxSpinnerService } from 'ngx-spinner';
import { campCampeonatoService } from '../../servicios/campCampeonato.service';
import { FormEstadioComponent } from './form-estadio/form-estadio.component';

@Component({
  selector: 'app-estadio-principal',
  templateUrl: './estadio-principal.component.html',
  styleUrls: ['./estadio-principal.component.scss']
})
export class EstadioPrincipalComponent implements OnInit {
  //recursos heredados
  @Input() campeonatoSeleccionadoChild: ICampCampeonato;
  //Outputs
  @Output() campeonatoPrincipalOutput: EventEmitter<boolean>;
  public nombresColumnas: string[] = ['numero', 'nombre', 'descripcion', 'acciones'];
  dataSource: MatTableDataSource<ICampEstadio>;
  //variables paginación y orden
  @ViewChild(MatPaginator) paginator!: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;
  //paginacion
  public listaPaginado: number[] = []
  public paginaActual: number = 0;
  public numeroItems: number = 10;
  public paginasTotales: number;
  constructor(private spinnerService: NgxSpinnerService, private campCampeonatoService: campCampeonatoService,
    public dialog: MatDialog, private mensajeService: MensajesService) {
    this.campeonatoPrincipalOutput = new EventEmitter<boolean>()
  }

  ngOnInit(): void {
    this.spinnerService.show()
    this.consultarEstadios(this.paginaActual, this.numeroItems, this.campeonatoSeleccionadoChild.campCodigo);
    this.spinnerService.hide()
  }
  consultarEstadios(paginaActual: number, numeroItemas: number, campCodigo: number) {
    this.campCampeonatoService.getEstadioPaginadoPorCodigoCampenato(paginaActual, numeroItemas, campCodigo).then(respuesta => {
      this.paginasTotales = respuesta.totalPaginas;
      this.listaPaginado = Array.from({ length: this.paginasTotales }, (_, i) => i + 1);
      this.agregarDatosTabla(respuesta.listado);
    },
      (error) => {
        this.spinnerService.hide()
      })
  }
  agregarDatosTabla(listaEstadios) {
    this.dataSource = new MatTableDataSource<ICampEstadio>(listaEstadios);
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
    // this.matPaginatorIntl.itemsPerPageLabel = 'Elementos por página:';
    // this.matPaginatorIntl.nextPageLabel = 'Siguiente:';
    // this.matPaginatorIntl.previousPageLabel = 'Anterior:';
  }
  regresarCampeonatoPrincipal(regresar: boolean) {
    this.campeonatoPrincipalOutput.emit(regresar)
  }
  openDialogFormEstadio(estadioEditar: ICampEstadio): void {
    const dialogRef = this.dialog.open(FormEstadioComponent, {
      width: '600px',
      data: estadioEditar as ICampEstadio,
      disableClose: true
    });
    dialogRef.afterClosed().subscribe((result: any) => {
      if (!!result) {
        this.guardaEstadio(result)
      }
    })
  }
  async guardaEstadio(data: ICampEstadio) {
    this.spinnerService.show()
    const estadioNuevo: ICampEstadio = {
      estaCodigo: data.estaCodigo,
      estaDescripcion: data.estaDescripcion,
      estaEstado: 1,
      estaNombre: data.estaNombre,
      campCampeonato: this.campeonatoSeleccionadoChild
    }
    try {
      await this.campCampeonatoService.postGuardarEstadio(estadioNuevo)
      this.mensajeService.mensajeCorrecto("Éxito", "Estadio Creado")
      this.consultarEstadios(this.paginaActual, this.numeroItems, this.campeonatoSeleccionadoChild.campCodigo);
    } catch (error) {
      console.log(error)
    }
    this.spinnerService.hide()
  }
  async onPageChange(page: number): Promise<void> {
    this.paginaActual = page;
    this.consultarEstadios(this.paginaActual, this.numeroItems, this.campeonatoSeleccionadoChild.campCodigo);
  }
  async eliminarEstadio(estadioEliminado: ICampEstadio) {
    this.mensajeService.mensajeConfirmacion(estadioEliminado.estaNombre, "¿Eliminar?").then(async confirmado => {
      if (confirmado) {
        try {
          estadioEliminado.estaEstado = 0;
          await this.campCampeonatoService.postGuardarEstadio(estadioEliminado)
          this.consultarEstadios(this.paginaActual, this.numeroItems, this.campeonatoSeleccionadoChild.campCodigo);
          this.mensajeService.mensajeCorrecto("Éxito", "Estadio Eliminado")
        } catch (error) {
          console.log(error)
          this.mensajeService.mensajeError("Éxito", "Estadio No Eliminado")
        }
        this.spinnerService.hide()
      } else {
        this.spinnerService.hide()
      }
    })



  }
}
