import { Component, OnInit, ViewChild } from '@angular/core';
import { campCampeonatoService } from '../../servicios/campCampeonato.service';
import { NgxSpinnerService } from 'ngx-spinner';
import { MatPaginator, MatPaginatorIntl } from '@angular/material/paginator';
import { MatSort, Sort, MatSortModule } from '@angular/material/sort';
import { MatTableDataSource, MatTableModule } from '@angular/material/table';
import { LiveAnnouncer } from '@angular/cdk/a11y';
import { ICampCampeonato } from 'app/compartidos/interfaces/ICampCampeonato';

@Component({
  selector: 'app-campeonato-principal',
  templateUrl: './campeonato-principal.component.html',
  styleUrls: ['./campeonato-principal.component.scss']
})
export class CampeonatoPrincipalComponent implements OnInit {
  public nombresColumnas: string[] = ['numero', 'nombre', 'descripcion', 'acciones'];
  dataSource: MatTableDataSource<ICampCampeonato>;
  //variables paginación y orden
  @ViewChild(MatPaginator) paginator!: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;
  //paginacion
  public numeros: number[] = []
  public paginaActual: number = 0;
  public numeroItems: number = 10;
  public paginasTotales: number;
  public activarEquipoPrincipal: boolean = false;
  public activarFechaPrincipal: boolean = false;
  public campeonatoSeleccionado: ICampCampeonato;
  // public dataSource = new MatTableDataSource(ELEMENT_DATA);
  constructor(private spinnerService: NgxSpinnerService, private campCampeonatoService: campCampeonatoService,
    private matPaginatorIntl: MatPaginatorIntl, private _liveAnnouncer: LiveAnnouncer) { }

  ngOnInit(): void {
    this.spinnerService.show();
    this.consultarCampeonatos(this.paginaActual, this.numeroItems);
  }

  consultarCampeonatos(paginaActual: number, numeroItemas: number) {
    this.campCampeonatoService.getCampeonatoPaginado(paginaActual, numeroItemas).then(respuesta => {
      this.agregarDatosTabla(respuesta.listado);
      this.spinnerService.hide();
    },
      (error) => {
        this.spinnerService.hide()
      })
  }
  agregarDatosTabla(listaCampeonatos) {
    this.dataSource = new MatTableDataSource<ICampCampeonato>(listaCampeonatos);
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
    // this.matPaginatorIntl.itemsPerPageLabel = 'Elementos por página:';
    // this.matPaginatorIntl.nextPageLabel = 'Siguiente:';
    // this.matPaginatorIntl.previousPageLabel = 'Anterior:';
  }
  abrirMenuEquipos(campeonatoSeleccionado: ICampCampeonato) {
    this.activarEquipoPrincipal = true;
    this.campeonatoSeleccionado = campeonatoSeleccionado;
  }
  abrirFechasEquipos() {
    this.activarFechaPrincipal = true;
  }
  actualizarEquipoPrincipal(event: boolean) {
    this.activarEquipoPrincipal = event;
    this.ngOnInit()
  }

}
