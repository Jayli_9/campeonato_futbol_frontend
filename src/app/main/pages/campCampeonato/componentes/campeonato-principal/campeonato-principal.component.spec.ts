import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CampeonatoPrincipalComponent } from './campeonato-principal.component';

describe('CampeonatoPrincipalComponent', () => {
  let component: CampeonatoPrincipalComponent;
  let fixture: ComponentFixture<CampeonatoPrincipalComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CampeonatoPrincipalComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CampeonatoPrincipalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
