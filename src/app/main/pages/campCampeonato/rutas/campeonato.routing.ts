import { Routes } from '@angular/router';
import { CampeonatoPrincipalComponent } from '../componentes/campeonato-principal/campeonato-principal.component';

export const RUTA_CAMPEONATO: Routes = [
  {
    path: 'campeonato',
    component: CampeonatoPrincipalComponent,
  }
];