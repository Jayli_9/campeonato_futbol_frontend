import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { environment } from "environments/environment";
import { Asistencia } from "../../asistencia-estudiantes/interfaces/asistencia";
import { AsistenciaDia } from "../interfaces/asistencia-dia";
import { RespuestaAsistenciaMatriculaDiaInterfaz } from "../interfaces/respuesta-asistencia-matricula-dia-interfaz";
import { RespuestaAsistenciaMatriculaInterfaz } from "../interfaces/respuesta-asistencia-matricula-interfaz";
import { RespuestaCursoExtraordinariaInterfaz } from "../interfaces/respuesta-curso-extraordinaria-interfaz";
import { RespuestaDocenteInterfaz } from "../interfaces/respuesta-docente-interfaz";
import { RespuestaEspecialidadInterfaz } from "../interfaces/respuesta-especialidad-interfaz";
import { RespuestaEtapaServicioEducativoInterfaz } from "../interfaces/respuesta-etapa-servicio-educativo-interfaz";
import { RespuestaGradoInterfaz } from "../interfaces/respuesta-grado-interfaz";
import { RespuestaInstEstablecimientoInterfaz } from "../interfaces/respuesta-inst-establecimiento-interfaz";
import { RespuestaInstitucionInterfaz } from "../interfaces/respuesta-institucion-interfaz";
import { RespuestaJornadaInterfaz } from "../interfaces/respuesta-jornada-interfaz";
import { RespuestaMallaAsignaturaExtraInterfaz } from "../interfaces/respuesta-malla-asignatura-extra-interfaz";
import { RespuestaMatriculaExtraordinariaInterfaz } from "../interfaces/respuesta-matricula-extraordinaria-interfaz";
import { RespuestaMesInterfaz } from "../interfaces/respuesta-mes-interfaz";
import { RespuestaModalidadRespuesta } from "../interfaces/respuesta-modalidad-interfaz";
import { RespuestaModuloInterfaz } from "../interfaces/respuesta-modulo-interfaz";
import { RespuestaRegimenAnioLectivoInterfaz } from "../interfaces/respuesta-regimen-anio-lectivo-interfaz";
import { RespuestaServicioEducativoInterfaz } from "../interfaces/respuesta-servicio-educativo-interfaz";
import { RespuestaTipoAsistenciaInterfaz } from "../interfaces/respuesta-tipo-asistencia.interfaz";
import { AsistenciaParametros } from "../interfaces/asistencia-parametros";
import { AsistenciaDiaParametros } from "../interfaces/asistencia-dia-parametros";
import { MallaAsigExtraParametro } from "../interfaces/malla-asig-extra-parametro";
import { CursoExtraParametros } from "../interfaces/curso-extra-parametros";
import { RegimenAnioLectParametros } from "../interfaces/regimen-anio-lect-parametros";
import { ServicioEducativoParametros } from "../interfaces/servicio-educativo-parametros";
import { JornadaParametros } from "../interfaces/jornada-parametros";
import { EspecialidadParametros } from "../interfaces/especialidad-parametros";
import { GradosParametros } from "../interfaces/grados-parametros";
import { ModuloParametros } from "../interfaces/modulo-parametros";
import { MatriculaExtraParametros } from "../interfaces/matricula-extra-parametros";
import { DistributivoExtraParametros } from "../interfaces/distributivo-extra-parametros";
import { RespuestaDistributivoExtraInterfaz } from "../interfaces/respuesta-distributivo-extra-interfaz";
import { RespuestaDistributivoModuloInterfaz } from "../interfaces/respuesta-distributivo-modulo-interfaz";

@Injectable({
    providedIn: 'root'
})
export class JustificarAsistenciaService {

    url_academico=environment.url_academico;
    url_oferta=environment.url_oferta;
    url_institucion=environment.url_institucion;
    url_catalogo=environment.url_catalogo;
    url_matricula=environment.url_matricula;
    url_docente=environment.url_docente;

    constructor(
        public _http:HttpClient
    ) {}

    //Servicios de academicos
    guardarAsistencia(asistencia:Asistencia) {
        let url_ws=`${this.url_academico}/private/guardarAsistencia`;
        return this._http.post(url_ws,asistencia);
    }

    obtenerListaFaltaAsistenciaPorMatCodigoYCursoExtraordinario(asistenciaParametro:AsistenciaParametros) {
        let url_ws=`${this.url_academico}/private/listarFaltasAsistenciaPorMatCodigoYCursoExtraordinario`;
        return this._http.post<RespuestaAsistenciaMatriculaInterfaz>(url_ws, asistenciaParametro);
    }

    obtenerTodasMallaAsignaturaExtraPorDocenteCurextCodigoYParalelo(mallaAsigExtraParametro:MallaAsigExtraParametro) {
        let url_ws=`${this.url_academico}/private/buscarListaMallaAsigExtraPorCurextCodigoDocenteYParalelo`;
        return this._http.post<RespuestaMallaAsignaturaExtraInterfaz>(url_ws,mallaAsigExtraParametro);
    }

    guardarAsistenciaDia(asistenciaDia:AsistenciaDia) {
        let url_ws=`${this.url_academico}/private/guardarAsistenciaDia`;
        return this._http.post(url_ws,asistenciaDia);
    }

    obtenerListaFaltasAsistenciaDiaPorMatCodigoYCursoExtraordinario(asistenciaDiaParametros:AsistenciaDiaParametros) {
        let url_ws=`${this.url_academico}/private/listarFaltasAsistenciaDiaPorMatCodigoYCursoExtraordinario`;
        return this._http.post<RespuestaAsistenciaMatriculaDiaInterfaz>(url_ws,asistenciaDiaParametros);
    }

    obtenerTodasTipoAsistenciaEstado() {
        let url_ws=`${this.url_academico}/private/listarTipoAsistenciaPorEstado`;
        return this._http.get<RespuestaTipoAsistenciaInterfaz>(url_ws);
    }

    obtenerListaMeses() {
        let url_ws=`${this.url_academico}/private/buscarMesesPorEstado`;
        return this._http.get<RespuestaMesInterfaz>(url_ws);
    }

    obtenerListaDitributivoExtraPorListaCurexListaCuparYDocente(distributivoExtraParametros:DistributivoExtraParametros) {
        let url_ws=`${this.url_academico}/private/listarDistributivoExtraPorListaCurexListaParaleloDocenteYEstado`;
        return this._http.post<RespuestaDistributivoExtraInterfaz>(url_ws, distributivoExtraParametros);
    }

    obtenerListaDitributivoModuloPorListaCurexListaCuparYDocente(distributivoExtraParametros:DistributivoExtraParametros) {
        let url_ws=`${this.url_academico}/private/listarDistributivoModuloPorListaCurexListaParaleloDocenteYEstado`;
        return this._http.post<RespuestaDistributivoModuloInterfaz>(url_ws, distributivoExtraParametros);
    }

    //servicio web de catalogos
    obtenerTodasRegimenAnioLectivoPorListaEstablecimiento(regimenAnioLectParametros:RegimenAnioLectParametros) {
        let url_ws=`${this.url_catalogo}/private/listaTodasRegionAnioLectivoPorListaEstablecimiento`;
        return this._http.post<RespuestaRegimenAnioLectivoInterfaz>(url_ws, regimenAnioLectParametros);
    }

    obtenerTodasJornadasPorCodigo(jornadaParametros:JornadaParametros) {
        let url_ws=`${this.url_catalogo}/private/listarJornadasPorListaJorCodigoYEstado`;
        return this._http.post<RespuestaJornadaInterfaz>(url_ws, jornadaParametros);
    }

    obtenerTodasModalidadesPorListaCodigos(modCodigo:any){
        let url_ws=`${this.url_catalogo}/private/listarModalidadesPorListaModCodigoYEstado/${modCodigo}`;
        return this._http.get<RespuestaModalidadRespuesta>(url_ws);
    }

    obtenerTodosServiciosEducativosPorListaCodigos(servicioEducativoParametros:ServicioEducativoParametros){
        let url_ws=`${this.url_catalogo}/private/listarServicioEducativosPorListaSereduCodigo`;
        return this._http.post<RespuestaServicioEducativoInterfaz>(url_ws,servicioEducativoParametros);
    }

    obtenerServicioEducativoPorCodigo(sereduCodigo:any) {
        let url_ws=`${this.url_catalogo}/private/buscarServicioEducativoPorCodigo/${sereduCodigo}`;
        return this._http.get<RespuestaServicioEducativoInterfaz>(url_ws);
    }

    listarLasEspecialidadesPorListaEspCodigoYEstado(especialidadParametros:EspecialidadParametros) {
        let url_ws=`${this.url_catalogo}/private/buscarEspecialidadesPorListaEspCodigoYEstado`;
        return this._http.post<RespuestaEspecialidadInterfaz>(url_ws, especialidadParametros);
    }

    obtenerTodosGradorPorListaGraCodigoYEstado(gradosParametros:GradosParametros) {
        let url_ws=`${this.url_catalogo}/private/listarGradosPorListaGraCodigoYEstado`;
        return this._http.post<RespuestaGradoInterfaz>(url_ws, gradosParametros);
    }

    obtenerRegimenAnioLectivoPorCodigo(reanleCodigo:any) {
        let url_ws=`${this.url_catalogo}/private/obtenerRegionAnioLectDTOPorCodigo/${reanleCodigo}`;
        return this._http.get<RespuestaRegimenAnioLectivoInterfaz>(url_ws);
    }

    obtenerGradorPorGraCodigo(graCodigo:any) {
        let url_ws=`${this.url_catalogo}/private/buscarCatalogoGradosPorCodigo/${graCodigo}`;
        return this._http.get<RespuestaGradoInterfaz>(url_ws);
    }
    
    //servicios web de docentes
    obtenerDocentePorIdentificacion(identificacion:any) {
        let url_ws=`${this.url_docente}/private/docenteHispanaPorIdentificacionYEstado/${identificacion}`;
        return this._http.get<RespuestaDocenteInterfaz>(url_ws);
    }

    //servicios web de ofetas
    obtenerTodosCursosExtraordinarioPorListaEstablecimiento(cursoExtraParametros:CursoExtraParametros) {
        let url_ws=`${this.url_oferta}/private/listarCursosExtraordinariosPorListaEstablecimiento`;
        return this._http.post<RespuestaCursoExtraordinariaInterfaz>(url_ws, cursoExtraParametros);
    }

    obtenerTodosCursosExtraordinarioPorRegimenYListaEstablecimiento(cursoExtraParametros:CursoExtraParametros) {
        let url_ws=`${this.url_oferta}/private/buscarCursoExtraordinarioPorRegimenAnioLectivoYlistaEstablecimiento`;
        return this._http.post<RespuestaCursoExtraordinariaInterfaz>(url_ws, cursoExtraParametros);
    }

    obtenerTodosCursosExtraordinarioListacurextCodigo(cursoExtraParametros:CursoExtraParametros) {
        let url_ws=`${this.url_oferta}/private/listarParaleloExtraordinarioPorListaCursoExtra`;
        return this._http.post<RespuestaCursoExtraordinariaInterfaz>(url_ws, cursoExtraParametros);
    }

    obtenerCursoExtraordinarioPorCurextCodigo(curextCodigo:any) {
        let url_ws=`${this.url_oferta}/private/buscarCursoExtraordinarioPorCodigo/${curextCodigo}`;
        return this._http.get<RespuestaCursoExtraordinariaInterfaz>(url_ws);
    }

    obtenerEtapaServicioEducaticoPorServicioEducativo(sereduCodigo:any) {
        let url_ws=`${this.url_oferta}/private/listarEtapasServiciosDTOPorCodigoServicioEducativo/${sereduCodigo}`;
        return this._http.get<RespuestaEtapaServicioEducativoInterfaz>(url_ws);
    }

    //servicios web de instituciones
    obtenerInstitucionPorAmie(insAmie:any) {
        let url_ws=`${this.url_institucion}/private/buscarInstitucionPorAmie/${insAmie}`;
        return this._http.get<RespuestaInstitucionInterfaz>(url_ws);
    }

    obtenerTodosEstablecimientoPorInstitucion(codigoInstitucion:any) {
        let url_ws=`${this.url_institucion}/private/listarEstablecimientosPorInstitucion/${codigoInstitucion}`;
        return this._http.get<RespuestaInstEstablecimientoInterfaz>(url_ws);
    }

    //servicios web de matriculas
    obtenerTodosModulosPorListaCodigo(moduloParametros:ModuloParametros){
        let url_ws=`${this.url_matricula}/private/listarModulosPorListaCodigoYEstado`;
        return this._http.post<RespuestaModuloInterfaz>(url_ws, moduloParametros);
    }

    obtenerModuloPorCodigo(modCodigo:any) {
        let url_ws=`${this.url_matricula}/private/obtenerModuloPorCodigo/${modCodigo}`;
        return this._http.get<RespuestaModuloInterfaz>(url_ws);
    }

    obtenerTodosEstudianteMatriculados(matriculaExtraParametros:MatriculaExtraParametros){
        let url_ws=`${this.url_matricula}/private/obtenerMatriculaExtraordinariaPorCursoExtraordParaleloYEstado`;
        return this._http.post<RespuestaMatriculaExtraordinariaInterfaz>(url_ws, matriculaExtraParametros);
    }
}