import { TestBed } from '@angular/core/testing';
import { JustificarAsistenciaService } from './justificar-asistencia.service';

describe('JustificarAsistenciaService', () => {
  let service: JustificarAsistenciaService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(JustificarAsistenciaService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});