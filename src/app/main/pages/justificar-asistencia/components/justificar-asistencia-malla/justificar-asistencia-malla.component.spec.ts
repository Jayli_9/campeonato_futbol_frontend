import { ComponentFixture, TestBed } from '@angular/core/testing';

import { JustificarAsistenciaMallaComponent } from './justificar-asistencia-malla.component';

describe('JustificarAsistenciaMallaComponent', () => {
  let component: JustificarAsistenciaMallaComponent;
  let fixture: ComponentFixture<JustificarAsistenciaMallaComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ JustificarAsistenciaMallaComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(JustificarAsistenciaMallaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});