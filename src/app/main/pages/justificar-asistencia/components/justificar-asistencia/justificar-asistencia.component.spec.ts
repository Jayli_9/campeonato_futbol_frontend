import { ComponentFixture, TestBed } from '@angular/core/testing';

import { JustificarAsistenciaComponent } from './justificar-asistencia.component';

describe('JustificarAsistenciaComponent', () => {
  let component: JustificarAsistenciaComponent;
  let fixture: ComponentFixture<JustificarAsistenciaComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ JustificarAsistenciaComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(JustificarAsistenciaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});