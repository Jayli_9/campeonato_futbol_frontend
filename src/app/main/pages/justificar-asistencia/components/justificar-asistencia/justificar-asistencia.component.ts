import { Component, OnInit, ViewChild } from "@angular/core";
import { UntypedFormBuilder, UntypedFormGroup } from "@angular/forms";
import { MatPaginator } from "@angular/material/paginator";
import { MatSort } from "@angular/material/sort";
import { MatTableDataSource } from "@angular/material/table";
import { Router } from "@angular/router";
import { NgbModal } from "@ng-bootstrap/ng-bootstrap";
import { NgxSpinnerService } from "ngx-spinner";
import Swal from "sweetalert2";
import { Asistencia } from "../../interfaces/asistencia";
import { AsistenciaDia } from "../../interfaces/asistencia-dia";
import { AsistenciaMatricula } from "../../interfaces/asistencia-matricula";
import { AsistenciaMatriculaDia } from "../../interfaces/asistencia-matricula-dia";
import { CursoExtraordinaria } from "../../interfaces/curso-extraordinaria";
import { Docente } from "../../interfaces/docente";
import { EtapaServicioEducativo } from "../../interfaces/etapa-servicio-educativo";
import { Grado } from "../../interfaces/grado";
import { MallaAsignaturaExtra } from "../../interfaces/malla-asignatura-extra";
import { MatriculaExtaordinaria } from "../../interfaces/matricula-extraordinaria";
import { Mes } from "../../interfaces/mes";
import { Modulo } from "../../interfaces/modulo";
import { RegimenAnioLectivo } from "../../interfaces/regimen-anio-lectivo";
import { RespuestaAsistenciaMatriculaDiaInterfaz } from "../../interfaces/respuesta-asistencia-matricula-dia-interfaz";
import { RespuestaAsistenciaMatriculaInterfaz } from "../../interfaces/respuesta-asistencia-matricula-interfaz";
import { RespuestaEtapaServicioEducativoInterfaz } from "../../interfaces/respuesta-etapa-servicio-educativo-interfaz";
import { RespuestaGradoInterfaz } from "../../interfaces/respuesta-grado-interfaz";
import { RespuestaMallaAsignaturaExtraInterfaz } from "../../interfaces/respuesta-malla-asignatura-extra-interfaz";
import { RespuestaMatriculaExtraordinariaInterfaz } from "../../interfaces/respuesta-matricula-extraordinaria-interfaz";
import { RespuestaMesInterfaz } from "../../interfaces/respuesta-mes-interfaz";
import { RespuestaModuloInterfaz } from "../../interfaces/respuesta-modulo-interfaz";
import { RespuestaRegimenAnioLectivoInterfaz } from "../../interfaces/respuesta-regimen-anio-lectivo-interfaz";
import { RespuestaServicioEducativoInterfaz } from "../../interfaces/respuesta-servicio-educativo-interfaz";
import { ServicioEducativo } from "../../interfaces/servicio-educativo";
import { JustificarAsistenciaService } from "../../services/justificar-asistencia.service";
import { ProgramaStore } from "../../services/programa-store";
import { AsistenciaParametros } from "../../interfaces/asistencia-parametros";
import { AsistenciaDiaParametros } from "../../interfaces/asistencia-dia-parametros";
import { MallaAsigExtraParametro } from "../../interfaces/malla-asig-extra-parametro";
import { MatriculaExtraParametros } from "../../interfaces/matricula-extra-parametros";

@Component({
    selector: 'app-justifiar-asistencia',
    templateUrl: './justificar-asistencia.component.html',
    styleUrls: ['./justificar-asistencia.component.scss']
})
export class JustificarAsistenciaComponent implements OnInit {

    public contentHeader: object;
    public frmJustificarAsistenciaCreateEdit: UntypedFormGroup;
    public submitted = false;

    listaMatriculaExtraordinario:MatriculaExtaordinaria[];
    listaMallaAsignatura:MallaAsignaturaExtra[];
    listaAsistencia:Asistencia[];
    listaAsistenciaDia:AsistenciaDia[];
    listaAsistenciaMatricula:AsistenciaMatricula[];
    listaAsistenciaMatriculaDia:AsistenciaMatriculaDia[];
    columnasAsistencia=[];
    listaMes:Mes[];

    cursoExtraordinaria:CursoExtraordinaria;
    docente:Docente;
    servicioEducativo:ServicioEducativo;
    mallaAsignatura:MallaAsignaturaExtra;
    etapaServicioEducativo:EtapaServicioEducativo;
    regimenAnioLectivo:RegimenAnioLectivo;
    modulo:Modulo;
    grado:Grado;
    mes:Mes;
    tipoNiveModulo:string;
    fechaInicio:Date;
    fechaFin:Date;
    tipoAsistencia:string = '';

    dataSource: MatTableDataSource<AsistenciaMatricula>;
    dataSourceDia: MatTableDataSource<AsistenciaMatriculaDia>;

    @ViewChild(MatPaginator) paginator: MatPaginator;
    @ViewChild(MatSort) sort: MatSort;
    constructor(private readonly router: Router,
        private readonly fb: UntypedFormBuilder,
        private modalService: NgbModal,
        public spinner: NgxSpinnerService,
        private _justificarAsistenciaService:JustificarAsistenciaService,
        private readonly programaStore: ProgramaStore
        ){
    }

    ngOnInit(): void {
        this.contentHeader = {
            headerTitle: 'Justificar Asistencia',
            actionButton: false,
            breadcrumb: {
                type: '',
                links: [
                    {
                      name: 'Inicio',
                      isLink: true,
                      link: '/pages/inicio'
                    },
                    {
                      name: 'Justificar Asistencia',
                      isLink: false
                    },
                ]
            }
        };
        this.obtenerCursoExtraordinario();
    }

    obtenerCursoExtraordinario() {
        this.cursoExtraordinaria = this.programaStore.cursoExtraordinaria;
        if (this.cursoExtraordinaria == null || this.cursoExtraordinaria == undefined) {
            this.volverJustificarAsistenciaPeriodo();
            return;
        }
        this.docente=this.programaStore.docente;
        this.spinner.show();
        this.obternerServicioEducativo();
        this.spinner.hide();
    }

    obternerServicioEducativo() {
        this._justificarAsistenciaService.obtenerServicioEducativoPorCodigo(this.cursoExtraordinaria.sereduCodigo).subscribe(
            (respuesta:RespuestaServicioEducativoInterfaz)=>{
                this.servicioEducativo = respuesta.objeto;
                this.obtenerTodasMatriculaEstudiantePorCurextCodigo();
            },
            (error:any)=>{
                this.spinner.hide();    
                Swal.fire('Ups! ocurrió un error con el servicio educativo','','error'); 
            }
        );
    }

    obtenerTodasMatriculaEstudiantePorCurextCodigo() {
        let matriculaExtraParametros:MatriculaExtraParametros = {
            curextCodigo:this.cursoExtraordinaria.curextCodigo,
            cupaexCodigo:this.cursoExtraordinaria.cupaexCodigo
        };
        this._justificarAsistenciaService.obtenerTodosEstudianteMatriculados(matriculaExtraParametros).subscribe(
            (respuesta:RespuestaMatriculaExtraordinariaInterfaz)=>{
                this.listaMatriculaExtraordinario = respuesta.listado;
                if (this.listaMatriculaExtraordinario.length == 0) {
                    Swal.fire('No existe estudiantes matricuados en este curso paralelo','','error');
                    this.volverJustificarAsistenciaPeriodo();
                    return;
                }
                
                if (this.cursoExtraordinaria.modCodigo != 0) {
                    this.obtenerRegimeAnioLectivo();
                } else if (this.cursoExtraordinaria.graCodigo != 0) {
                    this.obtenerGradoPorCodigo(this.cursoExtraordinaria.graCodigo);
                }
            },
            (error:any)=>{
                this.spinner.hide();    
                Swal.fire('Ups! ocurrió un error con la malla asignatura','','error'); 
            }
        );
    }

    obtenerRegimeAnioLectivo() {
        this._justificarAsistenciaService.obtenerRegimenAnioLectivoPorCodigo(this.cursoExtraordinaria.reanleCodigo).subscribe(
            (respuesta:RespuestaRegimenAnioLectivoInterfaz) => {
                let anio = new Date().getFullYear();
                this.regimenAnioLectivo = respuesta.objeto;
                if (this.regimenAnioLectivo.anilecAnioInicio > anio || this.regimenAnioLectivo.anilecAnioFin < anio) {
                    Swal.fire('Ups! el mes acual no esta dentro del regimen del año lectivo seleccionado','','warning'); 
                    this.volverJustificarAsistenciaPeriodo();
                } else {
                    this.obtenerModuloPorCodigo();
                }
            },
            (error:any)=>{
                this.spinner.hide();    
                Swal.fire('Ups! ocurrió un error el regimen año lectivo','','error'); 
            }
        );
    }

    obtenerModuloPorCodigo() {
        let modCodigo =  this.cursoExtraordinaria.modCodigo;
        this._justificarAsistenciaService.obtenerModuloPorCodigo(modCodigo).subscribe(
            (respuesta:RespuestaModuloInterfaz) => {
                this.modulo = respuesta.objeto;
                this.obtenerGradoPorCodigo(this.modulo.modCodigoGradoInicio);
            },
            (error:any)=>{
                this.spinner.hide();    
                Swal.fire('Ups! ocurrió un error con el modulo','','error'); 
            }
        );
    }

    obtenerGradoPorCodigo(graCodigo:any) {
        this._justificarAsistenciaService.obtenerGradorPorGraCodigo(graCodigo).subscribe(
            (respuesta:RespuestaGradoInterfaz) => {
                this.grado = respuesta.objeto;
                if (this.grado.graCodigo == 10 || this.grado.graCodigo == 11 || this.grado.graCodigo == 12 || this.grado.graCodigo == 13 ||
                    this.grado.graCodigo == 14 || this.grado.graCodigo == 15 || this.grado.graCodigo == 16 || this.grado.graCodigo == 17 || 
                    this.grado.graCodigo == 18) {
                    this.obtenerTodasMallaAsignaturasExtraCursoExtraordinariaYGrado();
                    this.obtenerListaEtapaServicioEducativoPorSevicioEducativo();
                    this.tipoAsistencia = 'POR_HORARIO';
                } else {
                    this.obtenerListaMes();
                    this.tipoAsistencia = 'POR_DIA';
                }
            },
            (error:any)=>{
                this.spinner.hide();    
                Swal.fire('Ups! ocurrió un error con el grado','','error'); 
            }
        );
    }

    obtenerListaMes(){
        this._justificarAsistenciaService.obtenerListaMeses().subscribe(
            (respuesta:RespuestaMesInterfaz) => {
                this.listaMes = respuesta.listado;
            },
            (error:any)=>{
                this.spinner.hide();    
                Swal.fire('Ups! ocurrió un error con el listado de meses','','error'); 
            }
        );
    }

    obtenerListaEtapaServicioEducativoPorSevicioEducativo() {
        this._justificarAsistenciaService.obtenerEtapaServicioEducaticoPorServicioEducativo(this.cursoExtraordinaria.sereduCodigo).subscribe(
            (respuesta:RespuestaEtapaServicioEducativoInterfaz) => {
                this.etapaServicioEducativo = respuesta.listado[0];
            },
            (error:any)=>{
                this.spinner.hide();    
                Swal.fire('Ups! ocurrió un error con el servicio educativo','','error'); 
            }
        );
    }

    obtenerTodasMallaAsignaturasExtraCursoExtraordinariaYGrado() {
        let mallaAsigExtraParametro:MallaAsigExtraParametro={
            docCodigo:this.docente.codPersona,
            curextCodigo:this.cursoExtraordinaria.curextCodigo,
            cupaexCodigo:this.cursoExtraordinaria.cupaexCodigo,
            grado:0
        }
        this._justificarAsistenciaService.obtenerTodasMallaAsignaturaExtraPorDocenteCurextCodigoYParalelo(mallaAsigExtraParametro).subscribe(
            (respuesta:RespuestaMallaAsignaturaExtraInterfaz)=>{
                this.listaMallaAsignatura = respuesta.listado;
            },
            (error:any)=>{
                this.spinner.hide();    
                Swal.fire('Ups! ocurrió un error con la malla asignatura','','error'); 
            }
        );
    }

    obtenerFaltasAsistenciaDiaEstudiantesMatriculados() {
        let matCodigo =  this.listaMatriculaExtraordinario.map((matricula) => matricula.matCodigo);
        let anio = new Date().getFullYear();
        this.listaAsistenciaMatriculaDia = null;
        this.columnasAsistencia = ['num', 'identificacion', 'apellido-nombre'];
        let asistenciaDiaParametros:AsistenciaDiaParametros = {
            listaMatCodigo:matCodigo,
            reanleCodigo:this.cursoExtraordinaria.reanleCodigo,
            curextCodigo:this.cursoExtraordinaria.curextCodigo,
            cupaexCodigo:this.cursoExtraordinaria.cupaexCodigo,
            meCodigo:this.mes.meCodigo,
            anioRegimen:anio};
        this._justificarAsistenciaService.obtenerListaFaltasAsistenciaDiaPorMatCodigoYCursoExtraordinario(asistenciaDiaParametros).subscribe(
            (respuesta:RespuestaAsistenciaMatriculaDiaInterfaz) => {
                this.spinner.hide();
                this.listaAsistenciaMatriculaDia = respuesta.listado;
                this.listaAsistenciaDia = [];
                for(let i=0; i<this.listaAsistenciaMatriculaDia.length;i++) {
                    let matriculaExta:MatriculaExtaordinaria=this.listaMatriculaExtraordinario.find((matriculaExta)=>matriculaExta.matCodigo==this.listaAsistenciaMatriculaDia[i].matCodigo);
                    this.listaAsistenciaMatriculaDia[i].estIdentificacion=matriculaExta.estIdentificacion;
                    this.listaAsistenciaMatriculaDia[i].apellidoEstudiante=matriculaExta.apellidoEstudiante;
                    this.listaAsistenciaMatriculaDia[i].nombreEstudiante=matriculaExta.nombreEstudiante;
                    if (this.listaAsistenciaMatriculaDia[i].asistenciasDias != null || this.listaAsistenciaMatriculaDia[i].asistenciasDias != undefined) {
                        for (let j = 0; j<this.listaAsistenciaMatriculaDia[i].asistenciasDias.length; j++){
                            let asistD:AsistenciaDia = this.listaAsistenciaDia.find((asistD)=>asistD.fechaNombre == this.listaAsistenciaMatriculaDia[i].asistenciasDias[j].fechaNombre);
                            if (asistD == null || asistD == undefined) {
                                this.listaAsistenciaDia.push(this.listaAsistenciaMatriculaDia[i].asistenciasDias[j]);
                            }
                            
                        }
                    }
                }
                this.listaAsistenciaDia.sort(function(fObjec, sObjec) {
                    if (fObjec.fechaNombre < sObjec.fechaNombre) {
                            return -1;
                    }
                    if (fObjec.fechaNombre > sObjec.fechaNombre) {
                            return 1;
                    }
                    return 0;
                });
                for (let i=0;i<this.listaAsistenciaMatriculaDia[0].asistenciasDias.length;i++) {
                    this.columnasAsistencia.push(this.listaAsistenciaDia[i].fechaNombre);
                }
                this.listaAsistenciaMatriculaDia.sort(function(fObjec, sObjec) {
                    if (fObjec.apellidoEstudiante != null && sObjec.apellidoEstudiante != null) {
                        if (fObjec.apellidoEstudiante < sObjec.apellidoEstudiante) {
                            return -1;
                        }
                        if (fObjec.apellidoEstudiante > sObjec.apellidoEstudiante) {
                            return 1;
                        }
                    } else {
                        if (fObjec.nombreEstudiante < sObjec.nombreEstudiante) {
                            return -1;
                        }
                        if (fObjec.nombreEstudiante > sObjec.nombreEstudiante) {
                            return 1;
                        }
                    }
                    return 0;
                });
                this.dataSourceDia = new MatTableDataSource(this.listaAsistenciaMatriculaDia);
                this.dataSourceDia.paginator = this.paginator;
                this.dataSourceDia.paginator._intl.itemsPerPageLabel="Asignaturas por página";
                this.dataSourceDia.paginator._intl.nextPageLabel="Siguiente";
                this.dataSourceDia.paginator._intl.previousPageLabel="Anterior";
                this.dataSourceDia.sort = this.sort;
            },
            (error:any)=>{
                this.spinner.hide();    
                Swal.fire('Ups! ocurrió un error con la asistencia por día','','error'); 
            }
        );
    }

    obtenerAsistenciaEstudiantesMatriculados(maasiexCodigo:any) {
        this.spinner.show();
        let matCodigo =  this.listaMatriculaExtraordinario.map((matricula) => matricula.matCodigo);
        let matCodigoLimpi = Array.from(new Set(matCodigo));
        this.columnasAsistencia = ['num', 'identificacion', 'apellido-nombre'];
        this.listaAsistenciaMatricula = null;
        this.listaAsistencia = null;

        if (this.fechaInicio == null || this.fechaInicio == undefined) {
            this.spinner.hide();    
            Swal.fire('Ups! Debe seleccionar la fecha de inicio','','warning');
            return;
        }

        if (this.fechaFin == null || this.fechaFin == undefined) {
            this.spinner.hide();
            Swal.fire('Ups! Debe seleccionar la fecha de fin','','warning');
            return;
        }

        if (this.fechaInicio > this.fechaFin) {
            this.spinner.hide();
            Swal.fire('Ups! La fecha inicio tiene que ser menor o igual a la fecha fin','','warning');
            return;
        }

        let asistenciaParametro:AsistenciaParametros = {
            listaMatCodigo:matCodigoLimpi,
            docCodigo:this.docente.codPersona,
            curextCodigo:this.cursoExtraordinaria.curextCodigo,
            cupaexCodigo:this.cursoExtraordinaria.cupaexCodigo,
            maasiexCodig:maasiexCodigo,
            reanleCodigo:this.cursoExtraordinaria.reanleCodigo,
            disextCodigo:this.mallaAsignatura.disextCodigo,
            fechaInicio:this.fechaInicio,
            fechaFin: this.fechaFin,
        };

        this._justificarAsistenciaService.obtenerListaFaltaAsistenciaPorMatCodigoYCursoExtraordinario(asistenciaParametro).subscribe(
            (respuesta:RespuestaAsistenciaMatriculaInterfaz) => {
                this.spinner.hide();
                this.listaAsistenciaMatricula = respuesta.listado;
                this.listaAsistencia = [];
                for(let i=0; i<this.listaAsistenciaMatricula.length;i++) {
                    let matriculaExta:MatriculaExtaordinaria=this.listaMatriculaExtraordinario.find((matriculaExta)=>matriculaExta.matCodigo==this.listaAsistenciaMatricula[i].matCodigo);
                    this.listaAsistenciaMatricula[i].estIdentificacion=matriculaExta.estIdentificacion;
                    this.listaAsistenciaMatricula[i].apellidoEstudiante=matriculaExta.apellidoEstudiante;
                    this.listaAsistenciaMatricula[i].nombreEstudiante=matriculaExta.nombreEstudiante;
                    if (this.listaAsistenciaMatricula[i].asistencias != null || this.listaAsistenciaMatricula[i].asistencias != undefined) {
                        for (let j = 0; j<this.listaAsistenciaMatricula[i].asistencias.length; j++){
                            let asist:Asistencia = this.listaAsistencia.find((asist)=>asist.fechaNombre == this.listaAsistenciaMatricula[i].asistencias[j].fechaNombre);
                            if (asist == null || asist == undefined) {
                                this.listaAsistencia.push(this.listaAsistenciaMatricula[i].asistencias[j]);
                            }
                            
                        }
                    }
                }

                this.listaAsistencia.sort(function(fObjec, sObjec) {
                    if (fObjec.fechaNombre < sObjec.fechaNombre) {
                            return -1;
                    }
                    if (fObjec.fechaNombre > sObjec.fechaNombre) {
                            return 1;
                    }
                    return 0;
                });

                for (let i=0;i<this.listaAsistencia.length;i++) {
                    this.columnasAsistencia.push(this.listaAsistencia[i].fechaNombre);
                }
                this.columnasAsistencia = Array.from(new Set(this.columnasAsistencia));

                this.listaAsistenciaMatricula.sort(function(fObjec, sObjec) {
                    if (fObjec.apellidoEstudiante != null && sObjec.apellidoEstudiante != null) {
                        if (fObjec.apellidoEstudiante < sObjec.apellidoEstudiante) {
                            return -1;
                        }
                        if (fObjec.apellidoEstudiante > sObjec.apellidoEstudiante) {
                            return 1;
                        }
                    } else {
                        if (fObjec.nombreEstudiante < sObjec.nombreEstudiante) {
                            return -1;
                        }
                        if (fObjec.nombreEstudiante > sObjec.nombreEstudiante) {
                            return 1;
                        }
                    }
                    return 0;
                });
                this.dataSource = new MatTableDataSource(this.listaAsistenciaMatricula);
                this.dataSource.paginator = this.paginator;
                this.dataSource.paginator._intl.itemsPerPageLabel="Asignaturas por página";
                this.dataSource.paginator._intl.nextPageLabel="Siguiente";
                this.dataSource.paginator._intl.previousPageLabel="Anterior";
                this.dataSource.sort = this.sort;
                this.spinner.hide();
            },
            (error:any)=>{
                this.spinner.hide();    
                Swal.fire('Ups! ocurrió un error las asignaturas de los estudiantes','','error'); 
            }
        );
    }

    justificarFaltaAsistencia(modalForm, asistencia:Asistencia) {
        this.submitted=false;
        this.frmJustificarAsistenciaCreateEdit = this.fb.group({
            asisCodigo:[asistencia.asisCodigo],
            horCodigo:[asistencia.horCodigo],
            tasiCodigo:[asistencia.tasiCodigo],
            hoCodigo:[asistencia.hoCodigo],
            matCodigo:[asistencia.matCodigo],
            asisFecAsistencia:[asistencia.asisFecAsistencia],
            asisAsistencia:[asistencia.asisAsistencia],
            asisJustificacion:[asistencia.asisJustificacion],
            asisObservacion:[asistencia.asisObservacion],
            dia:[asistencia.dia],
            fechaNombre:[asistencia.fechaNombre],
            horario:[asistencia.horario],
            reanleCodigo:[asistencia.reanleCodigo],
            asisEstado:[asistencia.asisEstado]
        });
        this.modalService.open(modalForm);
    }

    justificarFaltaAsistenciaDia(modalForm, asistenciaDia:AsistenciaDia) {
        this.submitted=false;
        this.frmJustificarAsistenciaCreateEdit = this.fb.group({
            asdiCodigo:[asistenciaDia.asdiCodigo],
            diaCodigo:[asistenciaDia.diaCodigo],
            meCodigo:[asistenciaDia.meCodigo],
            matCodigo:[asistenciaDia.matCodigo],
            curextCodigo:[asistenciaDia.curextCodigo],
            cupaexCodigo:[asistenciaDia.cupaexCodigo],
            asdiFecAsistencia:[asistenciaDia.asdiFecAsistencia],
            asdiAsistencia:[asistenciaDia.asdiAsistencia],
            asdiJustificacion:[asistenciaDia.asdiJustificacion],
            asdiObservacion:[asistenciaDia.asdiObservacion],
            dia:[asistenciaDia.dia],
            fechaNombre:[asistenciaDia.fechaNombre],
            reanleCodigo:[asistenciaDia.reanleCodigo]
        });
        this.modalService.open(modalForm);
    }

    guardarAsistencia(modalForm) {

        this.spinner.show();
        this.submitted = true;

        if (this.frmJustificarAsistenciaCreateEdit.invalid) {
            this.spinner.hide();
            return;
        }

        if (this.frmJustificarAsistenciaCreateEdit.get('asisJustificacion').value == false ) {
            Swal.fire('Ups! Marque la casilla de justificación.','','warning'); 
            this.spinner.hide();
            return;
        }

        if (this.frmJustificarAsistenciaCreateEdit.get('asisObservacion').value == null || this.frmJustificarAsistenciaCreateEdit.get('asisObservacion').value == '') {
            Swal.fire('Ups! Ingrese la observación.','','warning'); 
            this.spinner.hide();
            return;
        }

        let justifiarAsistencia:Asistencia = {
            asisCodigo:this.frmJustificarAsistenciaCreateEdit.get('asisCodigo').value,
            horCodigo:this.frmJustificarAsistenciaCreateEdit.get('horCodigo').value,
            tasiCodigo:this.frmJustificarAsistenciaCreateEdit.get('tasiCodigo').value,
            hoCodigo:this.frmJustificarAsistenciaCreateEdit.get('hoCodigo').value,
            matCodigo:this.frmJustificarAsistenciaCreateEdit.get('matCodigo').value,
            asisFecAsistencia:this.frmJustificarAsistenciaCreateEdit.get('asisFecAsistencia').value,
            asisAsistencia:this.frmJustificarAsistenciaCreateEdit.get('asisAsistencia').value,
            asisJustificacion:this.frmJustificarAsistenciaCreateEdit.get('asisJustificacion').value,
            asisObservacion:this.frmJustificarAsistenciaCreateEdit.get('asisObservacion').value,
            dia:this.frmJustificarAsistenciaCreateEdit.get('dia').value,
            fechaNombre:this.frmJustificarAsistenciaCreateEdit.get('fechaNombre').value,
            horario:this.frmJustificarAsistenciaCreateEdit.get('horario').value,
            reanleCodigo:this.frmJustificarAsistenciaCreateEdit.get('reanleCodigo').value,
            asisEstado:this.frmJustificarAsistenciaCreateEdit.get('asisEstado').value
        };

        this.spinner.show();
        this._justificarAsistenciaService.guardarAsistencia(justifiarAsistencia).subscribe(
            (respuesta:any) => {
                this.spinner.hide();
                Swal.fire('La justificación se guardo de manera correcta.','','success'); 
                this.obtenerAsistenciaEstudiantesMatriculados(this.mallaAsignatura.maasiexCodigo);
            },
            (error:any)=>{
                this.spinner.hide();    
                Swal.fire('Ups! ocurrió un error al guardar la justificación de la asistencia.','','error'); 
            }
        );

        modalForm.close('Accept click');
    }

    guardarAsistenciaDia(modalForm) {

        this.spinner.show();
        this.submitted = true;

        if (this.frmJustificarAsistenciaCreateEdit.invalid) {
            this.spinner.hide();
            return;
        }

        if (this.frmJustificarAsistenciaCreateEdit.get('asdiJustificacion').value == false ) {
            Swal.fire('Ups! Marque la casilla de justificación.','','warning'); 
            this.spinner.hide();
            return;
        }

        if (this.frmJustificarAsistenciaCreateEdit.get('asdiObservacion').value == null || this.frmJustificarAsistenciaCreateEdit.get('asdiObservacion').value == '') {
            Swal.fire('Ups! Ingrese la observación.','','warning'); 
            this.spinner.hide();
            return;
        }

        let justifiarAsistenciaDia:AsistenciaDia = {
            asdiCodigo:this.frmJustificarAsistenciaCreateEdit.get('asdiCodigo').value,
            diaCodigo:this.frmJustificarAsistenciaCreateEdit.get('diaCodigo').value,
            meCodigo:this.frmJustificarAsistenciaCreateEdit.get('meCodigo').value,
            matCodigo:this.frmJustificarAsistenciaCreateEdit.get('matCodigo').value,
            curextCodigo:this.frmJustificarAsistenciaCreateEdit.get('curextCodigo').value,
            cupaexCodigo:this.frmJustificarAsistenciaCreateEdit.get('cupaexCodigo').value,
            asdiFecAsistencia:this.frmJustificarAsistenciaCreateEdit.get('asdiFecAsistencia').value,
            asdiAsistencia:this.frmJustificarAsistenciaCreateEdit.get('asdiAsistencia').value,
            asdiJustificacion:this.frmJustificarAsistenciaCreateEdit.get('asdiJustificacion').value,
            asdiObservacion:this.frmJustificarAsistenciaCreateEdit.get('asdiObservacion').value,
            dia:this.frmJustificarAsistenciaCreateEdit.get('dia').value,
            fechaNombre:this.frmJustificarAsistenciaCreateEdit.get('fechaNombre').value,
            reanleCodigo:this.frmJustificarAsistenciaCreateEdit.get('reanleCodigo').value,
        };

        this.spinner.show();
        this._justificarAsistenciaService.guardarAsistenciaDia(justifiarAsistenciaDia).subscribe(
            (respuesta:any) => {
                this.spinner.hide();
                Swal.fire('La justificación se guardo de manera correcta.','','success'); 
                this.obtenerFaltasAsistenciaDiaEstudiantesMatriculados();
            },
            (error:any)=>{
                this.spinner.hide();    
                Swal.fire('Ups! ocurrió un error al guardar la justificación de la asistencia.','','error'); 
            }
        );

        modalForm.close('Accept click');
    }

    limpiarDatosConFechaInicio() {
        this.listaAsistencia = null;
        this.listaAsistenciaMatricula = null;
        this.mallaAsignatura = null;
        this.fechaFin = null;
    }

    limpiarDatosConFechaFin() {
        this.listaAsistenciaMatricula = null;
        this.mallaAsignatura = null;
    }

    volverJustificarAsistenciaPeriodo() {
        this.router.navigate(['pages/justificar-asistencia-extra']);
    }

    applyFilter(event: Event) {
        const filterValue = (event.target as HTMLInputElement).value;
        this.dataSource.filter = filterValue.trim().toLowerCase();
       
        if (this.dataSource.paginator) {
           this.dataSource.paginator.firstPage();
        }
    }

    applyFilterDia(event: Event) {
        const filterValue = (event.target as HTMLInputElement).value;
        this.dataSourceDia.filter = filterValue.trim().toLowerCase();
       
        if (this.dataSourceDia.paginator) {
           this.dataSourceDia.paginator.firstPage();
        }
     }
 
    get ReactiveFrmJustificarAsistenciaCreateEdit() {
        return this.frmJustificarAsistenciaCreateEdit.controls;
    }

}