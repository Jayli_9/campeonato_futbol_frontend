import { RespuestaGeneralInterfaz } from "app/main/shared/interfaces/respuesta-general-interfaz";
import { Modulo } from "./modulo";

export interface RespuestaModuloInterfaz extends RespuestaGeneralInterfaz {
    listado: Modulo[];
    objeto: Modulo;
}