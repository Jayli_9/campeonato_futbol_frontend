import { RespuestaGeneralInterfaz } from "app/main/shared/interfaces/respuesta-general-interfaz";
import { Mes } from "./mes";

export interface RespuestaMesInterfaz extends RespuestaGeneralInterfaz {
    listado: Mes[];
    objeto: Mes;
}