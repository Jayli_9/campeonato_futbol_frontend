import { RespuestaGeneralInterfaz } from "app/main/shared/interfaces/respuesta-general-interfaz";
import { Grado } from "./grado";

export interface RespuestaGradoInterfaz extends RespuestaGeneralInterfaz{
    listado:Grado[];
    objeto:Grado;
}