import { Asistencia } from "./asistencia";

export interface AsistenciaMatricula {
    matCodigo: number;
    estIdentificacion: string;
    nombreEstudiante: string;
    apellidoEstudiante: string;
    asistencias:Asistencia[];
}