import { RespuestaGeneralInterfaz } from "app/main/shared/interfaces/respuesta-general-interfaz";
import { CursoExtraordinaria } from "./curso-extraordinaria";

export interface RespuestaCursoExtraordinariaInterfaz extends RespuestaGeneralInterfaz {
    listado: CursoExtraordinaria[];
    objeto: CursoExtraordinaria;
}