import { RespuestaGeneralInterfaz } from "app/main/shared/interfaces/respuesta-general-interfaz";
import { MatriculaExtaordinaria } from "./matricula-extraordinaria";

export interface RespuestaMatriculaExtraordinariaInterfaz extends RespuestaGeneralInterfaz {
    listado: MatriculaExtaordinaria[];
    objeto: MatriculaExtaordinaria;
}