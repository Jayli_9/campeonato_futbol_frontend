export interface AsistenciaDia {
    asdiCodigo:number;
    diaCodigo:number;
    meCodigo:number;
    matCodigo:number;
    curextCodigo:number;
    cupaexCodigo:number;
    asdiFecAsistencia:Date;
    asdiAsistencia:boolean;
    asdiJustificacion:boolean;
    asdiObservacion:string;
    dia:string;
    fechaNombre:string;
    reanleCodigo:number;
}