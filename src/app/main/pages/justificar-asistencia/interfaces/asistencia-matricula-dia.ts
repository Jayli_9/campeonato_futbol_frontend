import { AsistenciaDia } from "./asistencia-dia";


export interface AsistenciaMatriculaDia {
    matCodigo: number;
    estIdentificacion: string;
    nombreEstudiante: string;
    apellidoEstudiante: string;
    asistenciasDias:AsistenciaDia[];
}