import { RespuestaGeneralInterfaz } from "app/main/shared/interfaces/respuesta-general-interfaz";
import { EtapaServicioEducativo } from "./etapa-servicio-educativo";

export interface RespuestaEtapaServicioEducativoInterfaz extends RespuestaGeneralInterfaz{
    listado:EtapaServicioEducativo[];
    objeto:EtapaServicioEducativo;
}