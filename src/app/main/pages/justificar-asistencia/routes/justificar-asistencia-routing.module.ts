import { Routes } from "@angular/router";
import { AuthGuard } from "app/auth/helpers/auth.guards";
import { JustificarAsistenciaMallaComponent } from "../components/justificar-asistencia-malla/justificar-asistencia-malla.component";
import { JustificarAsistenciaComponent } from "../components/justificar-asistencia/justificar-asistencia.component";

export const RUTA_JUSTIFICAR_ASISTENCIA:Routes=[
    {
        path:'justificar-asistencia',
        component:JustificarAsistenciaComponent
    },
    {
        path:'justificar-asistencia-extra',
        component:JustificarAsistenciaMallaComponent,
        canActivate:[AuthGuard]
    }
]