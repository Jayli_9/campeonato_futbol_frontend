import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { CoreCommonModule } from '@core/common.module';
import { ContentHeaderModule } from 'app/layout/components/content-header/content-header.module';
import { MaterialModule } from 'app/main/shared/material/material.module';
import { JustificarAsistenciaComponent } from '../components/justificar-asistencia/justificar-asistencia.component';
import { RUTA_JUSTIFICAR_ASISTENCIA } from '../routes/justificar-asistencia-routing.module';
import { ProgramaStore } from '../services/programa-store';
import { JustificarAsistenciaMallaComponent } from '../components/justificar-asistencia-malla/justificar-asistencia-malla.component';


@NgModule({
  declarations: [
    JustificarAsistenciaComponent,
    JustificarAsistenciaMallaComponent
  ],
  imports: [
    CommonModule,
    RouterModule.forChild(RUTA_JUSTIFICAR_ASISTENCIA),
    FormsModule,
    ReactiveFormsModule,
    MaterialModule,
    ContentHeaderModule,
    CoreCommonModule
  ],
  providers: [ProgramaStore]
})
export class JustificarAsistenciaModule { }