import { IcatAreaCiencia } from "./IcatAreaCiencia";
import { IcatEspecialidad } from "./IcatEspecialidad";
import { IcatFiguraProfesional } from "./IcatFiguraProfesional";
import { IcatNivel } from "./IcatNivel";

export interface IinsNivelPermisoInstitucionCruce {
      arecieCodigo?: number,
      espbtCodigo?: number,
      figprobcCodigo?: number,
      figprobtCodigo?: number,
      insCodigo?: number,
      jorCodigo?: number,
      nipeinCodigo?: number,
      nipeinEstado?: number,
      nipeinVigente?: string,
      nisediCodigo?: number,
      nivCodigo?: number,
      sereduCodigo?: number,
      nivel?: IcatNivel
      figuraProfesional?: IcatFiguraProfesional
      especialidad?: IcatEspecialidad
      areaCiencia?: IcatAreaCiencia
}