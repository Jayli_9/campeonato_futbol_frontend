export interface IcatPoblacionObjetivo {

      pbObjtCodigo: number,
      pbObjtDescripcion: string,
      pbObjtEstado: number,
      pbObjtFechaCreacion: string
}