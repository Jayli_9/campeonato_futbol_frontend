export interface asignaturaPrepaInterface{
       apreCodigo: number;
       apreDescripcion: string;
       apreEstado: number;
       graCodigo: number;
       apreHras: number;
       resAgrupacion: string;
}