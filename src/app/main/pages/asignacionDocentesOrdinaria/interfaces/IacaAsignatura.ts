import { IacaAgrupacion } from "./IacaAgrupacion"
import { IacaAreaConocimiento } from "./IacaAreaConocimiento"
import { IacaTipoAsignatura } from "./IacaTipoAsignatura"
import { IacaTipoValoracion } from "./IacaTipoValoracion"

export interface IacaAsignatura {
    acaAgrupacion: IacaAgrupacion,
    acaAreaConocimiento: IacaAreaConocimiento,
    acaTipoAsignatura: IacaTipoAsignatura,
    acaTipoValoracion: IacaTipoValoracion,
    asiCodigo: number,
    asiDescripcion: string,
    asiEstado: number,
    asiHoras: number,
    asiNemonico: string,
    espCodigo: number,
    graCodigo: number
}