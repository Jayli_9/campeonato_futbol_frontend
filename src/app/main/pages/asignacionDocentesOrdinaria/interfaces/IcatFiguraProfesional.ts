export interface IcatFiguraProfesional {
      arecieCodigo: number,
      figproCodigo: number,
      figproDescripcion: string,
      figproEstado: number,
      figproFechaCreacion: string
}