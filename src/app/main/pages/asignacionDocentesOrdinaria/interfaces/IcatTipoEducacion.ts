export interface IcatTipoEducacion {

      tipeduCodigo: number,
      tipeduEstado: number,
      tipeduFechaCreacion: string,
      tipeduNombre: string
}