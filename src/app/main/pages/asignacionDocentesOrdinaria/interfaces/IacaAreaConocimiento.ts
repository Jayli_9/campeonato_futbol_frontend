
export interface IacaAreaConocimiento {
    arcoCodigo: number,
    arcoDescripcion: string,
    arcoEstado: number,
    arcoNemonico: string
}