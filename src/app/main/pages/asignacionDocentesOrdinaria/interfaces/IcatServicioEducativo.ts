export interface IcatServicioEducativo {

      codigoAnioLectivo: number,
      codigoModalidad: number,
      codigoModalidadTipoEducacion: number,
      codigoPrograma: number,
      codigoRegimen: number,
      codigoTipoEducacion: number,
      impClasificacion: string,
      impDescripcion: string,
      modCodigo: number,
      modalidad: string,
      motiedCodigo: number,
      nombrePrograma: string,
      reanleCodigo: number,
      sereduCodigo: number,
      sereduEstado: number,
      sereduVigente: string,
      tipeduNombre: string
}