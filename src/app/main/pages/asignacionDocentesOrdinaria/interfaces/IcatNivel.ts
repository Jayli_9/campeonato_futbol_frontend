export interface IcatNivel {
      descripcionBilingue: string,
      nemonicoBilingue: string,
      nivCodigo: number,
      nivDescripcion: string,
      nivEstado: number,
      nivFechaCreacion: string,
      tipnivCodigo: number
}