export interface IcatTemporalidad {

      temCodigo: number,
      temDescripcion: string,
      temEstado: number
}