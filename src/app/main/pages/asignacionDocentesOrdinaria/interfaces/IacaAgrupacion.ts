
export interface IacaAgrupacion {
    agrCodigo: number,
    agrDescripcion: string,
    agrEstado: number,
    agrNemonico: string
}

