
export interface IinsNivelPermisoInstitucion {
      arecieCodigo: number,
      espbtCodigo: number,
      figprobcCodigo: number,
      figprobtCodigo: number,
      insCodigo: number,
      jorCodigo: number,
      nipeinCodigo: number,
      nipeinEstado: number,
      nipeinVigente: string,
      nisediCodigo: number,
      nivCodigo: number,
      sereduCodigo: number
}