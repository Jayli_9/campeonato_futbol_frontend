export interface IcatEspecialidad {
      codigoFiguraProfesional?: number,
      espCodigo?: number,
      espDescripcion?: string,
      espEstado?: number,
      espFechaCreacion?: string
}