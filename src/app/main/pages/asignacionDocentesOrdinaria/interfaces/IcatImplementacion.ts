export interface IcatImplementacion {

      impClasificacion: string,
      impCodigo: number,
      impDescripcion: string,
      impEstado: number
}