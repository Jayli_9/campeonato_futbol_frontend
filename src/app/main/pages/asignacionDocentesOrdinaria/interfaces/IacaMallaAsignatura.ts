import { IacaAsignatura } from "./IacaAsignatura";
import { IacaAsignaturaPrepa } from "./IacaAsignaturaPrepa";

export interface IacaMallaAsignatura {
    acaAsignatura?: IacaAsignatura,
    acaAsignaturaPrepa?: IacaAsignaturaPrepa,
    curCodigo: number,
    maasCodigo: number,
    maasEstado: number,
    maasHoras: number,
    maasNemonico: string,
    reanleCodigo: number,
    apreCodigo: number,
}