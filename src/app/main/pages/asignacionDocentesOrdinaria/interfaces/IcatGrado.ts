export interface IcatGrado {
      graCodigo: number,
      graDescripcion: string,
      graEstado: number,
      graFechaCreacion: string,
      graNemonico: string,
      nivCodigo: number
}