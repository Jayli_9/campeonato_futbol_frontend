
export interface IacaTipoAsignatura {
    tiasCodigo: number,
    tiasDescripcion: string,
    tiasEstado: number,
    tiasObligatorio: string
}