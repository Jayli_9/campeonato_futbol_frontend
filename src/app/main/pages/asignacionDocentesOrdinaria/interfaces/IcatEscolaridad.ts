export interface IcatEscolaridad {

      escCodigo: number,
      escDescripcion: string,
      escEstado: number,
      escNemonico: string
}