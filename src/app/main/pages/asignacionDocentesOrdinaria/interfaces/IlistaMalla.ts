
export interface IlistaMalla {
    acaAsignatura: {
        asiCodigo: number;
        asiDescripcion: string;
        asiEstado: number;
        asiHoras: number;
        asiNemonico: string;
        espCodigo: number;
        graCodigo: number;
    }
    curCodigo: number;
    maasCodigo: number;
    maasEstado: number;
    maasHoras: number;
    maasNemonico: string;
    reanleCodigo: number;
}

