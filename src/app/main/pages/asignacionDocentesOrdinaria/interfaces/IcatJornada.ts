export interface IcatJornada {
      jorCodigo: number,
      jorEsBilingue: number,
      jorEstado: number,
      jorFechaCreacion: string,
      jorNemonico: string,
      jorNombre: string
}