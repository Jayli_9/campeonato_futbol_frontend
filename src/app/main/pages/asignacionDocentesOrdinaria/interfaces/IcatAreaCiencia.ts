import { IcatFiguraProfesional } from "./IcatFiguraProfesional"
import { IcatNivel } from "./IcatNivel"

export interface IcatAreaCiencia {
      arecieCodigo: number,
      arecieEstado: number,
      arecieFechaCreacion: string,
      arecieVigente: string,
      catFiguraProfesional: IcatFiguraProfesional,
      catNivel: IcatNivel
}