
export interface IofeCursoParalelo {
    curCodigo: number,
    curparAforo: number,
    curparBancas: number,
    curparCodigo: number,
    curparEstado: number,
    curparFechaCreacion: string,
    curparNumEstud: number,
    idiEtnCodigo: number,
    nombreParalelo: string,
    parCodigo: number
}