export interface IacaAsignaturaPrepa {
    apreCodigo: number;
    apreDescripcion: string;
    apreEstado: number;
    apreHras: number;
    graCodigo: number;
    resAgrupacion: string;
}