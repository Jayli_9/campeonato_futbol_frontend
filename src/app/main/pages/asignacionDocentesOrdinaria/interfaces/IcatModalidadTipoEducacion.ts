export interface IcatModalidadTipoEducacion {
      escCodigo: number,
      impCodigo: number,
      modCodigo: number,
      motiedCodigo: number,
      motiedDescripcion: string,
      motiedEstado: number,
      pbojCodigo: number,
      temCodigo: number,
      tipeduCodigo: number
}