import { Routes } from '@angular/router';
import { SeleccionOfertaComponent } from '../componentes/seleccion-oferta/seleccion-oferta.component';
import { AuthGuard } from 'app/auth/helpers';

export const RUTA_ASIGNACION_ORDINARIO: Routes = [
  {
    path: 'asignacionDocenteOrdinaria',
    component: SeleccionOfertaComponent,
    canActivate:[AuthGuard]
  }
];