import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AsignacionOfertaComponent } from './asignacion-oferta.component';

describe('AsignacionOfertaComponent', () => {
  let component: AsignacionOfertaComponent;
  let fixture: ComponentFixture<AsignacionOfertaComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AsignacionOfertaComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AsignacionOfertaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
