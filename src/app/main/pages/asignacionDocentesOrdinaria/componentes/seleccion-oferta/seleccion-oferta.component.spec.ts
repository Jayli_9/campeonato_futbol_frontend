import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SeleccionOfertaComponent } from './seleccion-oferta.component';

describe('SeleccionOfertaComponent', () => {
  let component: SeleccionOfertaComponent;
  let fixture: ComponentFixture<SeleccionOfertaComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SeleccionOfertaComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SeleccionOfertaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
