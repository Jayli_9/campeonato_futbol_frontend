import { Component, ElementRef, OnInit, Output, ViewChild } from '@angular/core';
import { CatAnioElectivo } from 'app/main/pages/asignacionDocentes/interface/cat_anio';
import { CatModalidad } from 'app/main/pages/asignacionDocentes/interface/cat_modalidad';
import { CatalogoService } from 'app/main/pages/asignacionDocentes/servicios/catalogo.service';
import { GieeService } from 'app/main/pages/asignacionDocentes/servicios/giee.service';
import { MatriculaService } from 'app/main/pages/asignacionDocentes/servicios/matricula.service';
import { OfertaService } from 'app/main/pages/asignacionDocentes/servicios/oferta.service';
import { DocenteOrdinariaService } from '../../servicios/docenteOrdinaria.service';
import { IcatPoblacionObjetivo } from '../../interfaces/IcatPoblacionObjetivo';
import { IcatTemporalidad } from '../../interfaces/IcatTemporalidad';
import { IcatImplementacion } from '../../interfaces/IcatImplementacion';
import { IcatEscolaridad } from '../../interfaces/IcatEscolaridad';
import { IcatTipoEducacion } from '../../interfaces/IcatTipoEducacion';
import { IcatModalidadTipoEducacion } from '../../interfaces/IcatModalidadTipoEducacion';
import { MallaAsignaturaExtraService } from 'app/main/pages/malla-asignatura-extra/services/malla-asignatura-extra.service';
import { IcatDocenteHispana } from '../../interfaces/IcatDocenteHispana';
import { NgxSpinnerService } from "ngx-spinner"
import { FormBuilder, UntypedFormControl, FormGroup, FormsModule, ReactiveFormsModule, Validators } from '@angular/forms';
import { Observable } from 'rxjs';
import { map, startWith } from 'rxjs/operators';
import { AsyncPipe } from '@angular/common';
import { MatAutocompleteModule } from '@angular/material/autocomplete';
import { MatInputModule } from '@angular/material/input';
import { MatFormFieldModule } from '@angular/material/form-field';
import { IcatServicioEducativo } from '../../interfaces/IcatServicioEducativo';
import { IinsInstitucion } from '../../interfaces/IinsInstitucion';
import { IinsNivelPermisoInstitucion } from '../../interfaces/IinsNivelPermisoInstitucion';
import { ThemePalette } from '@angular/material/core';
import { IinsNivelPermisoInstitucionCruce } from '../../interfaces/IinsNivelPermisoInstitucionCruce';
import { MatTableDataSource } from '@angular/material/table';
import { MatPaginator, MatPaginatorIntl } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MensajesService } from '../../servicios/mensajes.service';
import { MatSelectChange } from '@angular/material/select';
import { figuraProfesional } from 'app/main/pages/asignacionDocentes/modelos/figuraProfesional';

@Component({
  selector: 'app-seleccion-oferta',
  templateUrl: './seleccion-oferta.component.html',
  styleUrls: ['./seleccion-oferta.component.scss']
})

export class SeleccionOfertaComponent implements OnInit {

  @ViewChild('selectElement') selectElement: ElementRef;

  displayedColumns: string[] = ['tipoEducacion', 'escolaridad', 'nivel', 'figuraProfesional', 'especialidad', 'accion'];
  dataSource: MatTableDataSource<any>;


  @Output() modalId;
  @Output() jorId;
  @Output() asigId;
  @Output() codEstableciminto;
  //objetos
  public contentHeader: object;
  //variables
  //variables paginación y orden
  @ViewChild(MatPaginator) paginator!: MatPaginator;
  @ViewChild(MatSort) sort!: MatSort;
  public poblacionObjetivoSeleccionado: IcatPoblacionObjetivo = null;
  public temporabilidadSeleccionado: IcatTemporalidad = null;
  public escolaridadSeleccionado: IcatEscolaridad = null;
  public implementacionSeleccionado: IcatImplementacion = null;
  public tipoEducacion: IcatTipoEducacion = null;
  public temporabilidad: IcatTemporalidad = null;
  public escolaridad: IcatEscolaridad = null;
  public implementacion: IcatImplementacion = null;
  public isFormSubmitted: boolean = true;
  public opcionBusqueda: boolean = true;
  public seleccionNivel: boolean = false;
  public reanleCodigo: number = null;
  public institucion: IinsInstitucion = null;
  public nivelCruceSeleccionado: IinsNivelPermisoInstitucionCruce;

  public modalidadTipoEducacion: IcatModalidadTipoEducacion = null;
  public docenteHispana: IcatDocenteHispana = { numIdentificacion: '' };
  public listaInstitucion: any;
  public informacionSede: any;
  public codAmie: any;
  public informacionInstitucion;
  public codInstitucion;
  public codgiRegimen;
  public listaRegimen
  public regimen;
  public anioElectivoCodigo;
  public anioElectivoDescripcion;
  public nombreInstitucion;
  //listas
  public listaCruzadaNivelPermisoInstitucion: IinsNivelPermisoInstitucionCruce[] = [];
  public listaOrdenada: IinsNivelPermisoInstitucionCruce[] = [];
  public listaNivelPermisoInstitucion: IinsNivelPermisoInstitucion[] = [];
  public listaServicioEducativo: IcatServicioEducativo[] = [];
  public listaDocenteHispana: IcatDocenteHispana[] = [];
  public listaAnio: CatAnioElectivo[] = [];
  public listaPoblacionObjetivo: IcatPoblacionObjetivo[] = [];
  public listaTemporabilidad: IcatTemporalidad[] = [];
  public listaImplementacion: IcatImplementacion[] = [];
  public listaEscolaridad: IcatEscolaridad[] = [];
  public listaTipoEducacion: IcatTipoEducacion[] = [];
  public listaModalidadTipoEducacion: IcatModalidadTipoEducacion[] = [];
  public informacionEstablecimiento: any;

  constructor(private CatalogoService: CatalogoService,
    private OfertaService: OfertaService,
    private asignacionOrdinariaService: DocenteOrdinariaService,
    private GieeService: GieeService,
    private MallaAsignaturaExtraService: MallaAsignaturaExtraService,
    private matPaginatorIntl: MatPaginatorIntl,
    private MatriculaService: MatriculaService,
    private mensajeService: MensajesService,
    private spinnerService: NgxSpinnerService) {
    this.isFormSubmitted = true;
  }


  myControl = new UntypedFormControl;
  opcionesDocentes: Observable<IcatDocenteHispana[]>;
  displayFn(docente: IcatDocenteHispana): string {
    return docente && docente.nomPersona ? docente.nomPersona : '';
  }

  private _filter(name: string): IcatDocenteHispana[] {
    const filterValue = name.toLowerCase();

    return this.listaDocenteHispana.filter(option => option.nomPersona.toLowerCase().includes(filterValue));
  }
  async ngOnInit(): Promise<void> {
    //asignacion codigo amie
    this.listaInstitucion = JSON.parse(localStorage.getItem('currentUser'));
    this.informacionSede = this.listaInstitucion.sede;
    // this.nombreInstitucion = this.informacionSede.descripcion;
    this.codAmie = this.informacionSede.nemonico;
    await this.obtenerNombreInstitucionPorAmie(this.codAmie);
    //buscar la institucion y regimen
    this.buscarRegiment(this.codAmie);
    await this.obtenerListadoDocentesHispano(this.codAmie);
    this.opcionesDocentes = this.myControl.valueChanges.pipe(
      startWith(''),
      map(value => {
        const name = typeof value === 'string' ? value : value?.name;
        return name ? this._filter(name as string) : this.listaDocenteHispana.slice();
      }),
    );

    //buscar Establecimiento de la institucion
    this.obtenerEstablecimiento(this.codAmie);
    //listar población objetivo
    this.obtenerPoblacionObjetivo();
  }
  reiniciarValores() {
    this.docenteHispana = { numIdentificacion: '' };
    this.listaCruzadaNivelPermisoInstitucion = [];
    this.listaCruzadaNivelPermisoInstitucion=[]
    // this.opcionBusqueda = false;
  }
  consultarModadidadTipoEducacion(objetoModalidadTipoEducacion) {
    this.spinnerService.show();
    if (objetoModalidadTipoEducacion === null) {
      this.temporabilidad = null;
      this.tipoEducacion = null;
      this.escolaridad = null;
      this.implementacion = null;
      this.spinnerService.hide();
      return;
    }
    this.asignacionOrdinariaService.getListaModalidadEducacionPorPbojCodigo(objetoModalidadTipoEducacion.pbObjtCodigo).then(data => {
      this.listaModalidadTipoEducacion = data.listado;
      this.modalidadTipoEducacion = this.listaModalidadTipoEducacion.find(obj => obj.motiedCodigo === 7);
      if (this.modalidadTipoEducacion === undefined) {
        this.temporabilidad = null;
        this.tipoEducacion = null;
        this.escolaridad = null;
        this.implementacion = null;
        this.spinnerService.hide();
        return;
      }
      //consultar tipo educación por codigo tipeduCodigo
      this.CatalogoService.getTipoEducacion(this.modalidadTipoEducacion.tipeduCodigo).then(data => {
        this.tipoEducacion = data.objeto;
      })
      if (this.modalidadTipoEducacion.temCodigo != null) {
        //consultar temporabilidad por codigo temCodigo
        this.asignacionOrdinariaService.getTemporabilidadTemCodigo(this.modalidadTipoEducacion.temCodigo).then(data => {
          this.temporabilidad = data.objeto;
        })
      }
      if (this.modalidadTipoEducacion.impCodigo != null) {
        //consultar implementacion por codigo impCodigo
        this.asignacionOrdinariaService.getImplementacionImpCodigo(this.modalidadTipoEducacion.impCodigo).then(data => {
          this.implementacion = data.objeto;
        })

      }
      //consultar escolaridad por codigo escCodigo
      this.asignacionOrdinariaService.getEscolaridadPorEscCodigo(this.modalidadTipoEducacion.escCodigo).then(data => {
        this.escolaridad = data.objeto;
      })


    })
    this.spinnerService.hide();
  }


  calcularAnchoSelect() {
    const opcionesLongitudes = this.listaPoblacionObjetivo.map(item => item.pbObjtDescripcion.length);
    const longitudMaxima = Math.max(...opcionesLongitudes);

    // Establecer el ancho del select basado en la longitud máxima
    const anchoMinimo = 100; // Puedes ajustar esto según tus necesidades
    const anchoCalculado = Math.max(anchoMinimo, longitudMaxima * 10); // Puedes ajustar el factor de multiplicación

    this.selectElement.nativeElement.style.width = `${anchoCalculado}px`;
  }


  //buscar los datos de la institucion y regimen
  buscarRegiment(cod) {
    this.spinnerService.show()
    //buscar la institucion
    this.GieeService.getDatosInstitucion(cod).then(data => {
      this.informacionInstitucion = data.objeto;
      for (let dato in this.informacionInstitucion) {
        this.codInstitucion = this.informacionInstitucion.insCodigo;
        this.codgiRegimen = this.informacionInstitucion.regimenes[0].regCodigo;
      }

      this.CatalogoService.getRegimen(this.codgiRegimen).then(data => {
        this.listaRegimen = data.objeto;
        //exportar el objeto
        this.regimen = this.listaRegimen?.regDescripcion
      })
      //listar regimenAñolectivo 
      this.CatalogoService.getAnioElectivo(this.codgiRegimen).then(data => {
        this.listaAnio = data.listado;
        for (let dato of this.listaAnio) {

          if (dato.reanleTipo === "A") {
            this.reanleCodigo = dato.reanleCodigo;
            this.anioElectivoCodigo = dato.anilecCodigo;
            this.CatalogoService.getAnioLectivoPorAnilecCodigo(this.anioElectivoCodigo).then(data => {
              let anioLectivo = data.objeto
              this.anioElectivoDescripcion = anioLectivo.anilecAnioInicio + " - " + anioLectivo.anilecAnioFin
            })
          }
        }
      })
    }, error => {
      // alert(error);
      // alert("Usuario sin sede");
    })
    this.spinnerService.hide()
  }

  //sacar los establecimientos de la institucion
  async obtenerEstablecimiento(cod) {
    this.informacionEstablecimiento = await this.GieeService.getEstablecimientoPorAmie(cod).then(data => data.listado);
  }
  //sacar población objetivo
  obtenerPoblacionObjetivo() {
    this.asignacionOrdinariaService.getPoblacionObjetivo().then(data => {
      this.listaPoblacionObjetivo = data.listado;
    })
  }
  //sacar población objetivo
  obtenerDocentePorIdentificacion(docenteHispana: IcatDocenteHispana) {
    try {
      this.spinnerService.show();
      if (typeof docenteHispana === "string") {
        this.listaCruzadaNivelPermisoInstitucion = []
        this.listaOrdenada = []
        this.mensajeService.mensajeAdvertencia("Advertencia", "Seleccione un docente")
        return;
      }

      if (docenteHispana?.numIdentificacion != undefined) {
        this.asignacionOrdinariaService.getDocentePorIdentificacion(docenteHispana.numIdentificacion).then(async data => {
          this.docenteHispana = data.objeto;
          if (this.docenteHispana?.codAmie !== this.codAmie) {
            this.docenteHispana = { numIdentificacion: '' }
            this.mensajeService.mensajeError('', 'Ups! No se encuentra al docente con la identificación ingresada');
            this.listaCruzadaNivelPermisoInstitucion = [];
            // this.opcionBusqueda = false;
          } else {
            await this.consultarNivelEducativo();
          }
        })
      }
    } finally {
      this.spinnerService.hide();
    }

  }
  //sacar población objetivo
  async obtenerListadoDocentesHispano(codAmie) {
    this.listaDocenteHispana = await this.asignacionOrdinariaService.getListaDocentesHispanoPorCodAmie(codAmie).then(data => data.listado);
  }
  //sacar nombre institucion
  async obtenerNombreInstitucionPorAmie(codAmie) {
    this.nombreInstitucion = await this.GieeService.getDatosInstitucion(codAmie).then(data => data.objeto);
  }
  //validar solo ingreso de numeros
  validateFormat(event) {
    let key;
    if (event.type === 'paste') {
      key = event.clipboardData.getData('text/plain');
    } else {
      key = event.keyCode;
      key = String.fromCharCode(key);
    }
    const regex = /[0-9]|\./;
    if (!regex.test(key)) {
      event.returnValue = false;
      if (event.preventDefault) {
        event.preventDefault();
      }
    }
  }

  async consultarNivelEducativo() {
    try {
      this.spinnerService.show();
      //buscar institución por Amie
      await this.asignacionOrdinariaService.getInstitucionPorAmie(this.codAmie).then(data => {
        this.institucion = data.objeto;
      })
      //buscar nivel permiso institucion
      await this.asignacionOrdinariaService.getNivelPermisoIntitucionPorInsCodigo(this.institucion.insCodigo).then(data => {
        this.listaNivelPermisoInstitucion = data.listado;
      })
      //buscar servicio educativo por reanleCodigo
      await this.asignacionOrdinariaService.getServicioEducativoPorReanleCodigo(this.reanleCodigo).then(data => {
        this.listaServicioEducativo = data.listado;
      })
      //realizar cruce de las dos listas 
      // Realizar el cruce de listas por el campo "id"

      this.listaCruzadaNivelPermisoInstitucion = this.listaNivelPermisoInstitucion
        .map(obj1 => {
          const obj2 = this.listaServicioEducativo.find(obj2 => obj2.sereduCodigo === obj1.sereduCodigo);
          if (obj2) {
            return { ...obj1, ...obj2 };
          }
          return null; // Devuelve null en lugar de obj1 si obj2 es undefined
        })
        .filter(item => item !== null); // Filtra los elementos null
      this.listaCruzadaNivelPermisoInstitucion = this.listaCruzadaNivelPermisoInstitucion.filter(obj => obj.nivCodigo !== null)
      for (const nivel of this.listaCruzadaNivelPermisoInstitucion) {
        await this.asignacionOrdinariaService.getNivelPorNivCodigo(nivel.nivCodigo).then(data => {
          nivel.nivel = data.objeto
        })
        if (nivel.figprobcCodigo != null) {
          await this.asignacionOrdinariaService.getFiguraProfesionalPorFigproCodigo(nivel.figprobcCodigo).then(async data => {
            nivel.figuraProfesional = data.objeto
          })
        }
        if (nivel.figprobtCodigo != null) {
          await this.asignacionOrdinariaService.getFiguraProfesionalPorFigproCodigo(nivel.figprobtCodigo).then(data => {
            nivel.figuraProfesional = data.objeto
          })
        }
        if (nivel.espbtCodigo != null) {
          await this.asignacionOrdinariaService.getEspecialidadPorEspbtCodigo(nivel.espbtCodigo).then(data => {
            nivel.especialidad = data.objeto
          })
        }
      }

      // Copia la lista para evitar modificar el original directamente.
      this.listaOrdenada = [...this.listaCruzadaNivelPermisoInstitucion];

      // Ordena la lista en función del código del nivel de menor a mayor.
      this.listaOrdenada.sort((a, b) => (a.nivel?.nivCodigo || 0) - (b.nivel?.nivCodigo || 0));

      this.listaCruzadaNivelPermisoInstitucion = this.listaOrdenada;

      this.dataSource = new MatTableDataSource(this.listaCruzadaNivelPermisoInstitucion);
      this.dataSource.paginator = this.paginator;
      this.dataSource.sort = this.sort;
      this.dataSource.paginator = this.paginator;
      this.matPaginatorIntl.itemsPerPageLabel = 'Elementos por página:';
      this.matPaginatorIntl.nextPageLabel = 'Siguiente:';
      this.matPaginatorIntl.previousPageLabel = 'Anterior:';
    } finally {
      this.spinnerService.hide();
    }

  }
  asignarOferta(nivelSeleccionado) {
    this.nivelCruceSeleccionado = nivelSeleccionado;
    this.seleccionNivel = true;
  }
  actualizarAsignacionOutput(event) {
    this.seleccionNivel = event;
    //this.reiniciarDatosTabla();
  }
  onInputEvent(event: any) {
    // Aquí puedes manejar el evento input como desees
    this.listaCruzadaNivelPermisoInstitucion = []
    // Puedes realizar acciones adicionales, como buscar y filtrar datos en función de lo que se escribió en el campo de entrada.
  }
  reiniciarDatosTabla() {

    this.listaCruzadaNivelPermisoInstitucion = []
  }
}
