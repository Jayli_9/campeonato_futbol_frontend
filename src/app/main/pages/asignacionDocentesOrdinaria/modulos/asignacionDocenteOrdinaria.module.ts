import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';

//importacion material
import { MaterialModule } from "app/main/pages/material/material.module";
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { SeleccionOfertaComponent } from '../componentes/seleccion-oferta/seleccion-oferta.component';
import { RUTA_ASIGNACION_ORDINARIO } from '../rutas/asignacionDocenteOrdinarioa.routing';
import { ContentHeaderModule } from 'app/layout/components/content-header/content-header.module';
import { MatAutocompleteModule } from '@angular/material/autocomplete';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { MatIconModule } from '@angular/material/icon';
import { MatDividerModule } from '@angular/material/divider';
import { AsignacionOfertaComponent } from '../componentes/seleccion-oferta/asignacion-oferta/asignacion-oferta.component';
import { MatSelectModule } from '@angular/material/select';
import { MatFormFieldControl, MatFormFieldModule } from '@angular/material/form-field';
import { MatPaginatorModule } from '@angular/material/paginator';



@NgModule({
  declarations: [
    SeleccionOfertaComponent,
    AsignacionOfertaComponent
  ],
  imports: [
    CommonModule,
    RouterModule.forChild(RUTA_ASIGNACION_ORDINARIO),
    MaterialModule,
    FormsModule,
    ReactiveFormsModule,
    MatAutocompleteModule,
    MatCheckboxModule,
    MatIconModule,
    MatDividerModule,
    MatSelectModule,
    MatFormFieldModule,
    MatPaginatorModule
    // ContentHeaderModule
  ],
  schemas: [
    CUSTOM_ELEMENTS_SCHEMA
  ],
  
})
export class asignacionDocenteOrdinariaModule { }