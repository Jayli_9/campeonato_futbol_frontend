import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'environments/environment';
import { AuthenticationService } from '../../../../auth/service/authentication.service';

//exportacion interfaces
import { IcatPoblacionObjetivo } from '../interfaces/IcatPoblacionObjetivo';
import { ResponseGenerico } from '../../asignacionDocentes/interface/response-generico';
import { modalidadTipoEducacion } from '../../asignacionDocentes/interface/modalidad-tipo-educacion';
import { IcatModalidadTipoEducacion } from '../interfaces/IcatModalidadTipoEducacion';
import { IcatDocenteHispana } from '../interfaces/IcatDocenteHispana';
import { IcatEscolaridad } from '../interfaces/IcatEscolaridad';
import { IcatImplementacion } from '../interfaces/IcatImplementacion';
import { IcatTemporalidad } from '../interfaces/IcatTemporalidad';
import { IinsInstitucion } from '../interfaces/IinsInstitucion';
import { IinsNivelPermisoInstitucion } from '../interfaces/IinsNivelPermisoInstitucion';
import { IcatServicioEducativo } from '../interfaces/IcatServicioEducativo';
import { IcatNivel } from '../interfaces/IcatNivel';
import { IcatFiguraProfesional } from '../interfaces/IcatFiguraProfesional';
import { IcatGrado } from '../interfaces/IcatGrado';
import { IofeCurso } from '../interfaces/IofeCurso';
import { IofeCursoParalelo } from '../interfaces/IofeCursoParalelo';
import { IacaMallaAsignatura } from '../interfaces/IacaMallaAsignatura';
import { IcatEspecialidad } from '../interfaces/IcatEspecialidad';
import { IcatAreaCiencia } from '../interfaces/IcatAreaCiencia';
import { IcatJornada } from '../interfaces/IcatJornada';
import { asignaturaPrepaInterface } from "../interfaces/asignaturaPrepa";

@Injectable({
  providedIn: 'root'
})
export class DocenteOrdinariaService {

  constructor(private _http: HttpClient, private _authService: AuthenticationService) { }


  //Anio electivo segun codigo
  getPoblacionObjetivo(): Promise<ResponseGenerico<IcatPoblacionObjetivo>> {
    return new Promise((resolve, reject) => {
      this._http.get(`${environment.url_catalogo}/private/listarPoblacionObjetivo`).subscribe((response: ResponseGenerico<IcatPoblacionObjetivo>) => {
        resolve(response);
      }, reject);
    })
  }
  //modalidad tipo de educacion por codigo población objetivo
  getListaModalidadEducacionPorPbojCodigo(pbojCodigo): Promise<ResponseGenerico<IcatModalidadTipoEducacion>> {
    return new Promise((resolve, reject) => {
      this._http.get(`${environment.url_catalogo}/private/listaModalidadesTipoEducacion/` + pbojCodigo).subscribe((response: ResponseGenerico<IcatModalidadTipoEducacion>) => {
        resolve(response);
      }, reject);
    })
  }
  //obtener docente hispano por identificacion
  getDocentePorIdentificacion(identificacion): Promise<ResponseGenerico<IcatDocenteHispana>> {
    return new Promise((resolve, reject) => {
      this._http.get(`${environment.url_docente}/private/docenteHispanaPorIdentificacionYEstado/` + identificacion).subscribe((response: ResponseGenerico<IcatDocenteHispana>) => {
        resolve(response);
      }, reject);
    })
  }
  //obtener docente hispano 
  getListaDocentesHispano(): Promise<ResponseGenerico<IcatDocenteHispana>> {
    return new Promise((resolve, reject) => {
      this._http.get(`${environment.url_docente}/private/listarDocentesHispana`).subscribe((response: ResponseGenerico<IcatDocenteHispana>) => {
        resolve(response);
      }, reject);
    })
  }
  //obtener docente hispano por Amie
  getListaDocentesHispanoPorCodAmie(codAmie): Promise<ResponseGenerico<IcatDocenteHispana>> {
    return new Promise((resolve, reject) => {
      this._http.get(`${environment.url_docente}/private/listarDocentesHispanaPorCodigoAmie/${codAmie}`).subscribe((response: ResponseGenerico<IcatDocenteHispana>) => {
        resolve(response);
      }, reject);
    })
  }

  //obtener escolaridad por escCodigo
  getEscolaridadPorEscCodigo(escCodigo): Promise<ResponseGenerico<IcatEscolaridad>> {
    return new Promise((resolve, reject) => {
      this._http.get(`${environment.url_catalogo}/private/buscarEscolaridadPorCodigo/` + escCodigo).subscribe((response: ResponseGenerico<IcatEscolaridad>) => {
        resolve(response);
      }, reject);
    })
  }
  //obtener implementación por impCodigo
  getImplementacionImpCodigo(impCodigo): Promise<ResponseGenerico<IcatImplementacion>> {
    return new Promise((resolve, reject) => {
      this._http.get(`${environment.url_catalogo}/private/buscarImplementacionPorCodigo/` + impCodigo).subscribe((response: ResponseGenerico<IcatImplementacion>) => {
        resolve(response);
      }, reject);
    })
  }
  //obtener temporabilidad por temCodigo
  getTemporabilidadTemCodigo(temCodigo): Promise<ResponseGenerico<IcatTemporalidad>> {
    return new Promise((resolve, reject) => {
      this._http.get(`${environment.url_catalogo}/private/buscarTemporalidadPorCodigo/` + temCodigo).subscribe((response: ResponseGenerico<IcatTemporalidad>) => {
        resolve(response);
      }, reject);
    })
  }
  //obtener temporabilidad por temCodigo
  getInstitucionPorAmie(codAmie): Promise<ResponseGenerico<IinsInstitucion>> {
    return new Promise((resolve, reject) => {
      this._http.get(`${environment.url_institucion}/private/buscarInstitucionPorAmie/` + codAmie).subscribe((response: ResponseGenerico<IinsInstitucion>) => {
        resolve(response);
      }, reject);
    })
  }
  //obtener nivel permiso institución por codigo de institucion
  getNivelPermisoIntitucionPorInsCodigo(insCodigo): Promise<ResponseGenerico<IinsNivelPermisoInstitucion>> {
    return new Promise((resolve, reject) => {
      this._http.get(`${environment.url_institucion}/private/buscarNivelPermisoPorCodigoInstitucion/` + insCodigo).subscribe((response: ResponseGenerico<IinsNivelPermisoInstitucion>) => {
        resolve(response);
      }, reject);
    })
  }
  //obtener lista de servicios educativos por reanleCodigo
  getServicioEducativoPorReanleCodigo(reanleCodigo): Promise<ResponseGenerico<IcatServicioEducativo>> {
    return new Promise((resolve, reject) => {
      this._http.get(`${environment.url_catalogo}/private/listarServicioEducativosPorRegistroAnioLectivo/` + reanleCodigo).subscribe((response: ResponseGenerico<IcatServicioEducativo>) => {
        resolve(response);
      }, reject);
    })
  }
  //obtener  nivel por nivCodigo
  getNivelPorNivCodigo(nivCodigo): Promise<ResponseGenerico<IcatNivel>> {
    return new Promise((resolve, reject) => {
      this._http.get(`${environment.url_catalogo}/private/buscarNivelPorCodigo/` + nivCodigo).subscribe((response: ResponseGenerico<IcatNivel>) => {
        resolve(response);
      }, reject);
    })
  }
  //obtener  figura profesional por codigo 
  getFiguraProfesionalPorFigproCodigo(figproCodigo): Promise<ResponseGenerico<IcatFiguraProfesional>> {
    return new Promise((resolve, reject) => {
      this._http.get(`${environment.url_catalogo}/private/buscarCatalogoFiguraProfesionalPorCodigo/` + figproCodigo).subscribe((response: ResponseGenerico<IcatFiguraProfesional>) => {
        resolve(response);
      }, reject);
    })
  }
  //obtener  area ciencia por figproCodigo 
  getAreaCienciaPorFigproCodigo(figproCodigo): Promise<ResponseGenerico<IcatAreaCiencia>> {
    return new Promise((resolve, reject) => {
      this._http.get(`${environment.url_catalogo}/private/buscarAreaCienciasPorFiguraProfesional/` + figproCodigo).subscribe((response: ResponseGenerico<IcatAreaCiencia>) => {
        resolve(response);
      }, reject);
    })
  }
  //obtener  figura profesional por codigo 
  getEspecialidadPorFigproCodigo(figproCodigo): Promise<ResponseGenerico<IcatEspecialidad>> {
    return new Promise((resolve, reject) => {
      this._http.get(`${environment.url_catalogo}/private/buscarCatalogoEspecialidadPorFiguraProfesionalCodigo/` + figproCodigo).subscribe((response: ResponseGenerico<IcatEspecialidad>) => {
        resolve(response);
      }, reject);
    })
  }
  //obtener  figura profesional por codigo 
  getEspecialidadPorEspbtCodigo(espbtCodigo): Promise<ResponseGenerico<IcatEspecialidad>> {
    return new Promise((resolve, reject) => {
      this._http.get(`${environment.url_catalogo}/private/buscarCatalogoEspecialidadPorCodigo/` + espbtCodigo).subscribe((response: ResponseGenerico<IcatEspecialidad>) => {
        resolve(response);
      }, reject);
    })
  }
  //obtener  grados por nivel
  getGradosporNivCodigo(nivCodigo): Promise<ResponseGenerico<IcatGrado>> {
    return new Promise((resolve, reject) => {
      this._http.get(`${environment.url_catalogo}/private/buscarCatalogoGradosPorNivelCodigo/` + nivCodigo).subscribe((response: ResponseGenerico<IcatGrado>) => {
        resolve(response);
      }, reject);
    })
  }
  //obtener  oferta por insestCodigo
  getOfertaPorInsestCodigo(insestCodigo): Promise<ResponseGenerico<IofeCurso>> {
    return new Promise((resolve, reject) => {
      this._http.get(`${environment.url_oferta}/private/listaCursoPorInsestCodigo/` + insestCodigo).subscribe((response: ResponseGenerico<IofeCurso>) => {
        resolve(response);
      }, reject);
    })
  }
  //obtener  oferta por insestCodigo y servicioEducativo
  getOfertaPorInsestCodigoSereduCodigo(insestCodigo, sereduCodigo): Promise<ResponseGenerico<IofeCurso>> {
    let objetoConsulta={
      codigoEstablecimiento:insestCodigo,
      codigoServicioEducativo:sereduCodigo
    }
    return new Promise((resolve, reject) => {
      this._http.post(`${environment.url_oferta}/private/listaCursoPorInsestCodigoSereduCodigo`,objetoConsulta).subscribe((response: ResponseGenerico<IofeCurso>) => {
        resolve(response);
      }, reject);
    })
  }
  //obtener  curso Paralelo por curCodigo
  getCursoParaleloPorCurCodigo(curCodigo): Promise<ResponseGenerico<IofeCursoParalelo>> {
    return new Promise((resolve, reject) => {
      this._http.get(`${environment.url_oferta}/private/listarParaleloOrdinarioPorCurso/` + curCodigo).subscribe((response: ResponseGenerico<IofeCursoParalelo>) => {
        resolve(response);
      }, reject);
    })
  }
  //obtener  Malla Asigantura por curCodigo
  getMallaAsignaturaPorCurCodigo(curCodigo): Promise<ResponseGenerico<IacaMallaAsignatura>> {
    return new Promise((resolve, reject) => {
      this._http.get(`${environment.url_academico}/private/listarMallasAsignaturaActivasPorCurso/` + curCodigo).subscribe((response: ResponseGenerico<IacaMallaAsignatura>) => {
        resolve(response);
      }, reject);
    })
  }
  //obtener  distributivo por curso paralelo
  getDistributivoPorCurparCodigo(curparCodigo): Promise<ResponseGenerico<any>> {
    return new Promise((resolve, reject) => {
      this._http.get(`${environment.url_academico}/private/listarDistributivosActivosPorCodigoCursoParalelo/` + curparCodigo).subscribe((response: ResponseGenerico<any>) => {
        resolve(response);
      }, reject);
    })
  }
  //guardar distributivo
  postGuardarDistributivo(distributivoDto): Promise<ResponseGenerico<any>> {
    return new Promise((resolve, reject) => {
      this._http.post(`${environment.url_academico}/private/guardarDistributivo`, distributivoDto).subscribe((response: ResponseGenerico<any>) => {
        resolve(response);
      }, reject);
    })
  }
  //listar distributivo por codigoDocente y curparCodigo
  postDistributivoPorCodigoDocenteCurparCodigo(docCodigo, curparCodigo): Promise<ResponseGenerico<any>> {
    let objetoConsulta={
      curparCodigo:curparCodigo,
      docCodigo:docCodigo
    }
    return new Promise((resolve, reject) => {
      this._http.post(`${environment.url_academico}/private/listarDistributivosPorDocenteYCursoParalelo`, objetoConsulta).subscribe((response: ResponseGenerico<any>) => {
        resolve(response);
      }, reject);
    })
  }
  
  //obtener  lista jornadas
  getJornadas(): Promise<ResponseGenerico<IcatJornada>> {
    return new Promise((resolve, reject) => {
      this._http.get(`${environment.url_catalogo}/private/listarJornadas`).subscribe((response: ResponseGenerico<IcatJornada>) => {
        resolve(response);
      }, reject);
    })
  }
   //obtener  jornada por código
   getJornadaPorJorCodigo(jorCodigo): Promise<ResponseGenerico<IcatJornada>> {
    return new Promise((resolve, reject) => {
      this._http.get(`${environment.url_catalogo}/private/buscarJornadaPorCodigo/` + jorCodigo).subscribe((response: ResponseGenerico<IcatJornada>) => {
        resolve(response);
      }, reject);
    })
  }


  //malla por codigo
  obtenerMallaPorCodigo(cod): Promise<ResponseGenerico<IacaMallaAsignatura>> {
    return new Promise((resolve, reject) => {
      this._http.get(`${environment.url_academico}/private/buscarMallaAsignaturaPorCodigo/` + cod).subscribe((response: ResponseGenerico<IacaMallaAsignatura>) => {
        resolve(response);
      }, reject);
    })
  }

  obtenerAsignaturaPrepaPorGrado(cod): Promise<ResponseGenerico<asignaturaPrepaInterface>>{
    return new Promise((resolve, reject) => {
      this._http.get(`${environment.url_academico}/private/buscarAsignaturaPrepaPorGrado/`+cod).subscribe((response: ResponseGenerico<asignaturaPrepaInterface>) => {
        resolve(response);
      }, reject);
    })
  }

}