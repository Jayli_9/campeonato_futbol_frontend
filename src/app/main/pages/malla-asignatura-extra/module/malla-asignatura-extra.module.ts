import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MallaAsignaturaExtraComponent } from '../components/malla-asignatura-extra/malla-asignatura-extra.component';
import { RUTA_MALLA_ASIGNATURA_EXTRA } from '../routes/malla-asignatura-extra-routing.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { CoreCommonModule } from '@core/common.module';
import { ContentHeaderModule } from 'app/layout/components/content-header/content-header.module';
import { MaterialModule } from 'app/main/shared/material/material.module';

@NgModule({
  declarations: [MallaAsignaturaExtraComponent],
  imports: [
    CommonModule,
    RouterModule.forChild(RUTA_MALLA_ASIGNATURA_EXTRA),
    FormsModule,
    ReactiveFormsModule,
    MaterialModule,
    ContentHeaderModule,
    CoreCommonModule
  ]
})
export class MallaAsignaturaExtraModule { }