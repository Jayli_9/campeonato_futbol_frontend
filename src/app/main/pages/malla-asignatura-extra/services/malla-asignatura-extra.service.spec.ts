import { TestBed } from '@angular/core/testing';

import { MallaAsignaturaExtraService } from './malla-asignatura-extra.service';

describe('AsignaturaService', () => {
  let service: MallaAsignaturaExtraService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(MallaAsignaturaExtraService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});