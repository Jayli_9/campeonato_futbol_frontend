import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { environment } from "environments/environment";
import { MallaAsignaturaExtra } from "../interfaces/malla-asignatura-extra";
import { RespuestaCursoExtraordinariaInterfaz } from "../interfaces/respuesta-curso-extraordinaria-interfaz";
import { RespuestaDocenteInterfaz } from "../interfaces/respuesta-docente-interfaz";
import { RespuestaEspecialidadInterfaz } from "../interfaces/respuesta-especialidad-interfaz";
import { RespuestaGradoInterfaz } from "../interfaces/respuesta-grado-interfaz";
import { RespuestaInstEstablecimientoInterfaz } from "../interfaces/respuesta-inst-establecimiento-interfaz";
import { RespuestaInstitucionInterfaz } from "../interfaces/respuesta-institucion-interfaz";
import { RespuestaJornadaInterfaz } from "../interfaces/respuesta-jornada-interfaz";
import { RespuestaMallaAsignaturaExtraInterfaz } from "../interfaces/respuesta-malla-asignatura-extra-interfaz";
import { RespuestaModuloInterfaz } from "../interfaces/respuesta-modulo-interfaz";
import { RespuestaRegimenAnioLectivoInterfaz } from "../interfaces/respuesta-regimen-anio-lectivo-interfaz";
import { RespuestaServicioEducativoInterfaz } from "../interfaces/respuesta-servicio-educativo-interfaz";
import { CursoExtraParametros } from "../interfaces/curso-extra-parametros";
import { RegimenAnioLectParametros } from "../interfaces/regimen-anio-lect-parametros";
import { ServicioEducativoParametros } from "../interfaces/servicio-educativo-parametros";
import { GradosParametros } from "../interfaces/grados-parametros";
import { ModuloParametros } from "../interfaces/modulo-parametros";
import { JornadaParametros } from "../interfaces/jornada-parametros";
import { MallaAsigExtraParametro } from "../interfaces/malla-asig-extra-parametro";
import { EspecialidadParamatetros } from "../interfaces/especialidad-parametros";
import { RespuestaGradoEquivalenciaInterfaz } from "../interfaces/respuesta-grado-equivalencia-interfaz";
import { ModalidadEducacionInterface } from "../interfaces/modalidad-educacion";
import { AgrupacionInterface } from "../interfaces/agrupacion";

import { servicioEducativo } from "../../asignacionDocentes/modelos/servicioEducativo";
import { ResponseGenerico } from "../interfaces/response-generico";
import { interfazServicioEducativo } from "../interfaces/servicio-datos-educativo";

@Injectable({
    providedIn: 'root'
})
export class MallaAsignaturaExtraService {

    url_academico=environment.url_academico;
    url_catalogo=environment.url_catalogo;
    url_oferta=environment.url_oferta;
    url_docente=environment.url_docente;
    url_institucion=environment.url_institucion;
    url_matricula=environment.url_matricula;

    constructor(
        public _http:HttpClient
    ) { }

    //Servicio web Academico
    guardarListaMallaAsignaturaExtra(listaMallaAsignaturaExta:MallaAsignaturaExtra[]){
        let url_ws=`${this.url_academico}/private/guardarListaMallaAsigExtra`;
        return this._http.post(url_ws,listaMallaAsignaturaExta);
    }

    obtenerListaMallaAsignaturaExtraPorCurextCodigoYGrado(mallaAsigExtraParametro:MallaAsigExtraParametro) {
        let url_ws=`${this.url_academico}/private/buscarMallaAsigExtraPorCurextCodigoYGrado`;
        return this._http.post<RespuestaMallaAsignaturaExtraInterfaz>(url_ws, mallaAsigExtraParametro);
    }

    //servicio web de catalogos
    obtenerTodasRegimenAnioLectivoPorListaEstablecimiento(regimenAnioLectParametros:RegimenAnioLectParametros) {
        let url_ws=`${this.url_catalogo}/private/listaTodasRegionAnioLectivoPorListaEstablecimiento`;
        return this._http.post<RespuestaRegimenAnioLectivoInterfaz>(url_ws,regimenAnioLectParametros);
    }

    obtenerTodosServiciosEducativosPorReanleCodigo(reanleCodigo:any) {
        let url_ws=`${this.url_catalogo}/private/listarServicioEducativosPorRegistroAnioLectivo/${reanleCodigo}`;
        return this._http.get<RespuestaServicioEducativoInterfaz>(url_ws);
    }

    obtenerServicioEducativoPorCodigoEstudianes(servicioEducativoParametros:ServicioEducativoParametros) {
        let url_ws=`${this.url_catalogo}/private/listarServicioEducativosPorListaSereduCodigo`;
        return this._http.post<RespuestaServicioEducativoInterfaz>(url_ws, servicioEducativoParametros);
    }

    obtenerTodasJornadaPorEstado(jornadaParametros:JornadaParametros) {
        let url_ws=`${this.url_catalogo}/private/listarJornadasPorListaJorCodigoYEstado`;
        return this._http.post<RespuestaJornadaInterfaz>(url_ws,jornadaParametros);
    }

    obtenerTodosGradorPorListaGraCodigoYEstado(gradosParametros:GradosParametros) {
        let url_ws=`${this.url_catalogo}/private/listarGradosPorListaGraCodigoYEstado`;
        return this._http.post<RespuestaGradoInterfaz>(url_ws,gradosParametros);
    }

    listarLasEspecialidadesPorListaEspCodigoYEstado(especialidadParamatetros:EspecialidadParamatetros) {
        let url_ws=`${this.url_catalogo}/private/buscarEspecialidadesPorListaEspCodigoYEstado`;
        return this._http.post<RespuestaEspecialidadInterfaz>(url_ws, especialidadParamatetros);
    }

    obtenerGradoEquivaleciaPorServicioEducativa(sereduCodigo:any) {
        let url_ws=`${this.url_catalogo}/private/buscarGradoPorServicioEducativo/${sereduCodigo}`;
        return this._http.get<RespuestaGradoEquivalenciaInterfaz>(url_ws);
    }

    //servicios web de ofertas
    obtenerTodosCursoExtraordinariaPorCodigos(cursoExtraParametros:CursoExtraParametros) {
        let url_ws=`${this.url_oferta}/private/buscarCursosExtraordinarioPorReanleCodigo`;
        return this._http.post<RespuestaCursoExtraordinariaInterfaz>(url_ws,cursoExtraParametros);
    }

    obtenerTodosCursosExtraordinarioPorEstablecimiento(insestCodigo:any) {
        let url_ws=`${this.url_oferta}/private/listarCursosExtraordinariosPorEstablecimiento/${insestCodigo}`;
        return this._http.get<RespuestaCursoExtraordinariaInterfaz>(url_ws);
    }

    obtenerTodosCursosExtraordinarioPorListaEstablecimiento(cursoExtraParametros:CursoExtraParametros) {
        let url_ws=`${this.url_oferta}/private/listarCursosExtraordinariosPorListaEstablecimiento`;
        return this._http.post<RespuestaCursoExtraordinariaInterfaz>(url_ws, cursoExtraParametros);
    }

    obtenerTodosCursosExtraordinarioPorGradoEstablecimientoYGrado(cursoExtraParametros:CursoExtraParametros) {
        let url_ws=`${this.url_oferta}/private/listarCursosExtraordinariosPorGradoEstablecimientoYEstado`;
        return this._http.post<RespuestaCursoExtraordinariaInterfaz>(url_ws, cursoExtraParametros);
    }

    obtenerTodosCursosExtraordinarioPorModuloEstablecimientoYGrado(cursoExtraParametros:CursoExtraParametros) {
        let url_ws=`${this.url_oferta}/private/listarCursosExtraordinariosPorModuloEstablecimientoYEstado`;
        return this._http.post<RespuestaCursoExtraordinariaInterfaz>(url_ws, cursoExtraParametros);
    }

    obtenerTodosCursosExtraordinarioPorReanleYJornada(cursoExtraParametros:CursoExtraParametros) {
        let url_ws=`${this.url_oferta}/private/buscarCursoExtraordinarioPorRegimenAnioLectivoYJornada`;
        return this._http.post<RespuestaCursoExtraordinariaInterfaz>(url_ws, cursoExtraParametros);
    }

    //servicios web de docentes
    obtenerDocentePorIdentificacion(idetificacion:any) {
        let url_ws=`${this.url_docente}/private/docenteHispanaPorIdentificacionYEstado/${idetificacion}`;
        return this._http.get<RespuestaDocenteInterfaz>(url_ws);
    }

    //servicios web de instituciones
    obtenerInstitucionPorAmie(insAmie:any) {
        let url_ws=`${this.url_institucion}/private/buscarInstitucionPorAmie/${insAmie}`;
        return this._http.get<RespuestaInstitucionInterfaz>(url_ws);
    }

    obtenerTodosEstablecimientoPorInstitucion(codigoInstitucion:any) {
        let url_ws=`${this.url_institucion}/private/listarEstablecimientosPorInstitucion/${codigoInstitucion}`;
        return this._http.get<RespuestaInstEstablecimientoInterfaz>(url_ws);
    }

    //servicios web de matricula
    obtenerModulosPorListaCodigos(moduloParametros:ModuloParametros) {
        let url_ws=`${this.url_matricula}/private/listarModulosPorListaCodigoYEstado`;
        return this._http.post<RespuestaModuloInterfaz>(url_ws, moduloParametros);
    }

    obtenerModuloPorCodigo(modCodigo) {
        let url_ws=`${this.url_matricula}/private/buscarModuloPorCodigo/${modCodigo}`;
        return this._http.get<RespuestaModuloInterfaz>(url_ws);
    }


    
    modalidadTipoEducacion(id){
        let url_ws=`${this.url_catalogo}/private/buscarModalidadTipoEducacionPorId/${id}`;
        return this._http.get<ModalidadEducacionInterface>(url_ws);
    }

    getmodalidadTipoEducacion(id): Promise<ResponseGenerico<ModalidadEducacionInterface>>{
        return new Promise((resolve, reject) => {
          this._http.get(`${environment.url_catalogo}/private/buscarModalidadTipoEducacionPorId/`+id).subscribe((response: ResponseGenerico<ModalidadEducacionInterface>) => {
            resolve(response);
          }, reject)
        })
      }

    obtenerServicioEducativoPorCodigo(id){
        let url_ws=`${this.url_catalogo}/private/buscarServicioEducativoPorCodigo/${id}`;
        return this._http.get<RespuestaServicioEducativoInterfaz>(url_ws);
    }

    SolucionServicioEducativoPorCodigo(id): Promise<ResponseGenerico<interfazServicioEducativo>>{
        return new Promise((resolve, reject) => {
          this._http.get(`${environment.url_catalogo}/private/buscarServicioEducativoPorCodigo/`+id).subscribe((response: ResponseGenerico<interfazServicioEducativo>) => {
            resolve(response);
          }, reject)
        })
      }

    listaDeAgrupaciones(): Promise<ResponseGenerico<AgrupacionInterface>>{
        return new Promise((resolve, reject) => {
          this._http.get(`${environment.url_academico}/private/listarAgrupacionesActivas`).subscribe((response: ResponseGenerico<AgrupacionInterface>) => {
            resolve(response);
          }, reject)
        })
      }
}