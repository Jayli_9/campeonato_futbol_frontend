import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MallaAsignaturaExtraComponent } from './malla-asignatura-extra.component';

describe('MallaAsignaturaExtraComponent', () => {
  let component: MallaAsignaturaExtraComponent;
  let fixture: ComponentFixture<MallaAsignaturaExtraComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ MallaAsignaturaExtraComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(MallaAsignaturaExtraComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});