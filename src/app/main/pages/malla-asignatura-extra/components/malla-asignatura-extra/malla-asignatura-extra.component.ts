import { Component, OnInit, ViewChild } from "@angular/core";
import { UntypedFormBuilder, UntypedFormGroup, Validators } from "@angular/forms";
import { MatPaginator } from "@angular/material/paginator";
import { MatSort } from "@angular/material/sort";
import { MatTableDataSource } from "@angular/material/table";
import { NgbModal } from "@ng-bootstrap/ng-bootstrap";
import { AuthenticationService } from "app/auth/service";
import { NgxSpinnerService } from "ngx-spinner";
import Swal from "sweetalert2";
import { CursoExtraordinaria } from "../../interfaces/curso-extraordinaria";
import { Docente } from "../../interfaces/docente";
import { Especialidad } from "../../interfaces/especialidad";
import { Grado } from "../../interfaces/grado";
import { InstEstablecimiento } from "../../interfaces/instEstablecimiento";
import { Institucion } from "../../interfaces/institucion";
import { Jornada } from "../../interfaces/jornada";
import { MallaAsignaturaExtra } from "../../interfaces/malla-asignatura-extra";
import { Modulo } from "../../interfaces/modulo";
import { RegimenAnioLectivo } from "../../interfaces/regimen-anio-lectivo";
import { RespuestaCursoExtraordinariaInterfaz } from "../../interfaces/respuesta-curso-extraordinaria-interfaz";
import { RespuestaEspecialidadInterfaz } from "../../interfaces/respuesta-especialidad-interfaz";
import { RespuestaGradoInterfaz } from "../../interfaces/respuesta-grado-interfaz";
import { RespuestaInstEstablecimientoInterfaz } from "../../interfaces/respuesta-inst-establecimiento-interfaz";
import { RespuestaInstitucionInterfaz } from "../../interfaces/respuesta-institucion-interfaz";
import { RespuestaJornadaInterfaz } from "../../interfaces/respuesta-jornada-interfaz";
import { RespuestaMallaAsignaturaExtraInterfaz } from "../../interfaces/respuesta-malla-asignatura-extra-interfaz";
import { RespuestaModuloInterfaz } from "../../interfaces/respuesta-modulo-interfaz";
import { RespuestaRegimenAnioLectivoInterfaz } from "../../interfaces/respuesta-regimen-anio-lectivo-interfaz";
import { RespuestaServicioEducativoInterfaz } from "../../interfaces/respuesta-servicio-educativo-interfaz";
import { ServicioEducativo } from "../../interfaces/servicio-educativo";
import { MallaAsignaturaExtraService } from "../../services/malla-asignatura-extra.service";
import { CursoExtraParametros } from "../../interfaces/curso-extra-parametros";
import { RegimenAnioLectParametros } from "../../interfaces/regimen-anio-lect-parametros";
import { ServicioEducativoParametros } from "../../interfaces/servicio-educativo-parametros";
import { GradosParametros } from "../../interfaces/grados-parametros";
import { ModuloParametros } from "../../interfaces/modulo-parametros";
import { JornadaParametros } from "../../interfaces/jornada-parametros";
import { MallaAsigExtraParametro } from "../../interfaces/malla-asig-extra-parametro";
import { EspecialidadParamatetros } from "../../interfaces/especialidad-parametros";
import { GradoEquivalencia } from "../../interfaces/grado-equivalencia";
import { RespuestaGradoEquivalenciaInterfaz } from "../../interfaces/respuesta-grado-equivalencia-interfaz";
import { ModalidadEducacionInterface } from "../../interfaces/modalidad-educacion";
import { AgrupacionInterface } from "../../interfaces/agrupacion"

@Component({
    selector: 'app-malla-asignatura-extra',
    templateUrl: './malla-asignatura-extra.component.html',
    styleUrls: ['./malla-asignatura-extra.component.scss']
})
export class MallaAsignaturaExtraComponent implements OnInit {

    public contentHeader: object;
    public frmMallaAsignaturaExtraCreateEdit: UntypedFormGroup;
    public submitted = false;

    listaMallaAsignaturaExtra: MallaAsignaturaExtra[];
    listaCursoExtraos:CursoExtraordinaria[];
    listaRegimenAnioLectivo: RegimenAnioLectivo[];
    listaCursoExtraordinaria: CursoExtraordinaria[];
    listaInstEstablecimento: InstEstablecimiento[];
    listaServicioEducativo: ServicioEducativo[];
    listaJornadas: Jornada[];
    listaJornadasCurext: Jornada[];
    listaGrados: Grado[];
    listaEspecialidad: Especialidad[] = null;
    listaModulo:Modulo[];

    //variables de modalidad educacion
    listaModalidades;
    complementoModalidad=[];
    complementoModalidadEdi=[];
    nombreServicio;
    listaServicioEducativoEdi;
    
    docente:Docente;
    institucion:Institucion;
    cursoExtraordinaria: CursoExtraordinaria;
    cursoEstraSelecionanda:CursoExtraordinaria;

    //variable de agrupacion

    listaDeAgrupacion=[];
    variableAgrupacionSelecciona=null;


    //variables de motief

    listaMotied;
    acumuladorMotied=[];

    columnasMallaAsignaruraExtra=['num', 'anio-lectivo', 'jornada', 'institucion', 'serv-educativo','grado', 'especialidad', 'estado', 'acciones'];
    columnasMallaAsiganturaExtraModal=['estado','asignatura', 'horasPres', 'horasAuton', 'horasTotal', 'nemonico'];
    dataSource: MatTableDataSource<MallaAsignaturaExtra>;
    dataSourceCursoE: MatTableDataSource<CursoExtraordinaria>;

    @ViewChild(MatPaginator) paginator: MatPaginator;
    @ViewChild(MatSort) sort: MatSort;
    constructor(private readonly fb: UntypedFormBuilder,
        private modalService: NgbModal,
        public spinner: NgxSpinnerService,
        private _mallaAsignaturaExtraService: MallaAsignaturaExtraService,
        private _authenticationService: AuthenticationService
        ){
    }
    
    ngOnInit(): void {
        this.contentHeader = {
            headerTitle: 'Malla Asignatura Extra',
            actionButton: false,
            breadcrumb: {
                type: '',
                links: [
                    {
                      name: 'Inicio',
                      isLink: true,
                      link: '/pages/inicio'
                    },
                    {
                      name: 'Malla Asignatura Extra',
                      isLink: false
                    },
                ]
            }
        };
        //this.obtenerDatosInicialesDocenteInstitucion();
        this.obtenerInstitucionPorAmie();
    }

    /*obtenerDatosInicialesDocenteInstitucion() {
        this.spinner.show();
        const currentUser = this._authenticationService.currentUserValue;
        let identificacion = currentUser.identificacion;
        this._mallaAsignaturaExtraService.obtenerDocentePorIdentificacion(identificacion).subscribe(
            (respuesta: RespuestaDocenteInterfaz)=>{
                this.docente = respuesta.objeto;
                this.obtenerInstitucionPorCodigo();
                this.listarEstablecimientoInstitucion();
                this.spinner.hide();
            },
            (error:any)=>{
                this.spinner.hide();
                Swal.fire('Ups! ocurrió un error al cargar el Docente','','error'); 
            }
        );
    }*/

    obtenerInstitucionPorAmie() {
        //let codInstitucion = this.docente.codInstitucion;
        const currentUser = this._authenticationService.currentUserValue;
        let codAmie = currentUser.sede.nemonico;
        this._mallaAsignaturaExtraService.obtenerInstitucionPorAmie(codAmie).subscribe(
            (respuesta: RespuestaInstitucionInterfaz)=>{
                this.institucion = respuesta.objeto;
                this.listarEstablecimientoInstitucion();
            },
            (error:any)=>{
                Swal.fire('Ups! ocurrió un error al cargar la Institución','','error'); 
            }
        );
    }

    listarEstablecimientoInstitucion() {
        //let codInstitucion = this.docente.codInstitucion;
        let codInstitucion = this.institucion.insCodigo;
        this._mallaAsignaturaExtraService.obtenerTodosEstablecimientoPorInstitucion(codInstitucion).subscribe(
            (respuesta: RespuestaInstEstablecimientoInterfaz)=>{
                this.listaInstEstablecimento=respuesta.listado;
                this.listarCursoExtraordinariaPorInstitucion();
            },
            (error:any)=>{
                Swal.fire('Ups! ocurrió un error al cargar la Institución','','error'); 
            }
        );
    }

    listarCursoExtraordinariaPorInstitucion() {
        let insestCodigo = this.listaInstEstablecimento.map((establecimiento) => establecimiento.insestCodigo);
        let cursoExtraParametros:CursoExtraParametros = {listaCodigoEstablecimiento:insestCodigo};
        this._mallaAsignaturaExtraService.obtenerTodosCursosExtraordinarioPorListaEstablecimiento(cursoExtraParametros).subscribe(
            (respuesta: RespuestaCursoExtraordinariaInterfaz)=>{
                this.listaCursoExtraordinaria = respuesta.listado;

                for(let i = 0; i < this.listaCursoExtraordinaria.length; i++){

                    this.obtenerNombreServicio(this.listaCursoExtraordinaria[i].sereduCodigo, i);

                }
                this.listarRegimenAnioLectivo();
                this.listaServiciosEducativosCursoExtraordinaria();
                this.obtenerListaJornadaCurext();
                this.dataSourceCursoE = new MatTableDataSource(this.listaCursoExtraordinaria);
                this.dataSourceCursoE.paginator = this.paginator;
                this.dataSourceCursoE.paginator._intl.itemsPerPageLabel="Asignaturas por página";
                this.dataSourceCursoE.paginator._intl.nextPageLabel="Siguiente";
                this.dataSourceCursoE.paginator._intl.previousPageLabel="Anterior";
                this.dataSourceCursoE.sort = this.sort;
            },
            (error:any)=>{
                this.spinner.hide();    
                Swal.fire('Ups! ocurrió un error con la lista curso extraordinario','','error'); 
            }
        );
    }


    obtenerNombreServicio(codServicio, i){
        this._mallaAsignaturaExtraService.SolucionServicioEducativoPorCodigo(codServicio).then(data => {
            let datoMotie = data.objeto;
            this.obtenerNombreMotied(datoMotie.motiedCodigo, i);
        })
    }


    obtenerNombreMotied(codigo, i){
        this._mallaAsignaturaExtraService.getmodalidadTipoEducacion(codigo).then(data => {
            this.listaMotied=data.objeto;

            this.listaCursoExtraordinaria[i].servEducativo = this.listaMotied.motiedDescripcion;

            let datos ={
                nemonico: this.listaMotied.motiedDescripcion,
                codigoServicio: codigo
            }

            this.acumuladorMotied.push(datos);
        })
    }



    obtenerListaJornadaCurext() {
        let jorCodigo = this.listaCursoExtraordinaria.map((cursoExta) =>cursoExta.jorCodigo);
        let jorCodigoLimpio = Array.from(new Set(jorCodigo));
        let jornadaParametros:JornadaParametros = {listaJorCodigo:jorCodigoLimpio};
        this._mallaAsignaturaExtraService.obtenerTodasJornadaPorEstado(jornadaParametros).subscribe(
            (respuesta: RespuestaJornadaInterfaz) =>{
                this.listaJornadasCurext = respuesta.listado;
                for (let i = 0; i < this.listaCursoExtraordinaria.length; i++) {
                    let jornada:Jornada = this.listaJornadasCurext.find((jornada) => jornada.jorCodigo == this.listaCursoExtraordinaria[i].jorCodigo);
                    this.listaCursoExtraordinaria[i].jornada = jornada.jorNombre;
                }
                
            },
            (error:any)=>{
                this.spinner.hide();
                Swal.fire('Ups! ocurrió un error al cargar las jornadas','','error'); 
            }
        );
    }

    listarRegimenAnioLectivo() {
        let listaRegAnioLec:RegimenAnioLectivo[];
        let reanleCodigo = this.listaCursoExtraordinaria.map((cursoExtra) => cursoExtra.reanleCodigo);
        let reanleCodigoLimpio = Array.from(new Set(reanleCodigo));
        let regimenAnioLectParametros:RegimenAnioLectParametros = {listaReanleCodigo:reanleCodigoLimpio};
        this._mallaAsignaturaExtraService.obtenerTodasRegimenAnioLectivoPorListaEstablecimiento(regimenAnioLectParametros).subscribe(
            (respuesta:RespuestaRegimenAnioLectivoInterfaz)=>{
                listaRegAnioLec = respuesta.listado;
                for (let i=0; i<this.listaCursoExtraordinaria.length; i++) {
                    let regAnioLec = listaRegAnioLec.find((regAnioLec)=>regAnioLec.reanleCodigo == this.listaCursoExtraordinaria[i].reanleCodigo)
                    if (regAnioLec != null && regAnioLec!=undefined) {
                        this.listaCursoExtraordinaria[i].anioLectivo = regAnioLec.regDescripcion+' '+regAnioLec.anilecAnioInicio+'-'+regAnioLec.anilecAnioFin;
                    } 
                }
            },
            (error:any)=>{
                this.spinner.hide();
                Swal.fire('Ups! ocurrió un error con los regimenes año lectivo','','error'); 
            }
        );
    }

    listaServiciosEducativosCursoExtraordinaria() {
        let listaSerEducativo:ServicioEducativo[];
        let sereduCodigo = this.listaCursoExtraordinaria.map((cursoExtra) => cursoExtra.sereduCodigo);
        let curexServicioGrado:CursoExtraordinaria[] = [];
        let curexServicioModulo:CursoExtraordinaria[] = [];
        let servicioEducativoParametros:ServicioEducativoParametros = {listaSereduCodigo:sereduCodigo};
        this._mallaAsignaturaExtraService.obtenerServicioEducativoPorCodigoEstudianes(servicioEducativoParametros).subscribe(
            (respuesta:RespuestaServicioEducativoInterfaz) => {
                listaSerEducativo = respuesta.listado;
                for(let i=0; i<this.listaCursoExtraordinaria.length; i++) {
                    let serEducativo:ServicioEducativo = listaSerEducativo.find((serEducativo)=>serEducativo.sereduCodigo==this.listaCursoExtraordinaria[i].sereduCodigo);
                    this.listaCursoExtraordinaria[i].servEducativo = serEducativo.sereduDescripcion;
                    this.listaCursoExtraordinaria[i].institucion = this.institucion.insDescripcion;
                    this.listaCursoExtraordinaria[i].modalidad = serEducativo.modalidad;
                    if (this.listaCursoExtraordinaria[i].graCodigo != 0) {
                        curexServicioGrado.push(this.listaCursoExtraordinaria[i]);
                    } else if (this.listaCursoExtraordinaria[i].modCodigo != 0){
                        curexServicioModulo.push(this.listaCursoExtraordinaria[i]);
                    }
                }
                this.listarGradoPorCursoExtraordinario(curexServicioGrado);
                this.listarModuloPorCursoExtraordinario(curexServicioModulo);
            },
            (error:any)=>{
                this.spinner.hide();
                Swal.fire('Ups! ocurrió un error con los servicio educativo','','error'); 
            }
        );
    }

    listarGradoPorCursoExtraordinario(curexServicioGrado:CursoExtraordinaria[]) {
        let listaGra:Grado[];
        let graCodigo = curexServicioGrado.map((grado)=>grado.graCodigo);
        let graCodigoLim = Array.from(new Set(graCodigo));
        let gradosParametros:GradosParametros = {listaGraCodigo:graCodigoLim};
        this._mallaAsignaturaExtraService.obtenerTodosGradorPorListaGraCodigoYEstado(gradosParametros).subscribe(
            (respuesta:RespuestaGradoInterfaz) =>{
                listaGra = respuesta.listado;
                for (let i=0; i<this.listaCursoExtraordinaria.length; i++) {
                    if (this.listaCursoExtraordinaria[i].graCodigo != 0) {
                        let gradoSerEdu:Grado = listaGra.find((gradoSerEdu)=>gradoSerEdu.graCodigo == this.listaCursoExtraordinaria[i].graCodigo);
                        this.listaCursoExtraordinaria[i].grado = gradoSerEdu.graDescripcion;
                    }
                }
            },
            (error:any)=>{
                this.spinner.hide();
                Swal.fire('Ups! ocurrió un error con los grados','','error'); 
            }
        );
    }

    listarModuloPorCursoExtraordinario(curexServicioModulo:CursoExtraordinaria[]) {
        let listaMod:Modulo[];
        let modCodigo = curexServicioModulo.map((modulo)=>modulo.modCodigo);
        let modCodigoLim = Array.from(new Set(modCodigo));
        let moduloParametros:ModuloParametros = {listaModCodigo:modCodigoLim};
        this._mallaAsignaturaExtraService.obtenerModulosPorListaCodigos(moduloParametros).subscribe(
            (respuesta:RespuestaModuloInterfaz) => {
                listaMod = respuesta.listado;
                for (let i=0; i<this.listaCursoExtraordinaria.length; i++) {
                    if (this.listaCursoExtraordinaria[i].modCodigo != 0) {
                        let moduloSerEd:Modulo = listaMod.find((modulo)=>modulo.modCodigo == this.listaCursoExtraordinaria[i].modCodigo);
                        if (moduloSerEd != null && moduloSerEd != undefined) {
                            this.listaCursoExtraordinaria[i].grado = moduloSerEd.modDescripcion;
                        }
                    }
                }
            },
            (error:any)=>{
                this.spinner.hide();
                Swal.fire('Ups! ocurrió un error con los modulos','','error'); 
            }
        );
    }

    limpiarCamposEstablecimiento(insestCodigo:any){
        this.frmMallaAsignaturaExtraCreateEdit.get('sereduCodigo').reset();
        this.frmMallaAsignaturaExtraCreateEdit.get('modCodigo').reset();
        this.frmMallaAsignaturaExtraCreateEdit.get('jorCodigo').reset();
        this.frmMallaAsignaturaExtraCreateEdit.get('graCodigo').reset();
        this.frmMallaAsignaturaExtraCreateEdit.get('espCodigo').reset();
        this.variableAgrupacionSelecciona=null;
        this.listaDeAgrupacion=[];
        this.listaGrados = null;
        
        this.listaModulo = null;
        this.listaMallaAsignaturaExtra = null;
        this.buscarListasCargarCombos(insestCodigo);
    }

    buscarListasCargarCombos(insestCodigo:any) {
        this.spinner.show();
        this._mallaAsignaturaExtraService.obtenerTodosCursosExtraordinarioPorEstablecimiento(insestCodigo).subscribe(
            (respuesta: RespuestaCursoExtraordinariaInterfaz) => {
                this.listaCursoExtraos = respuesta.listado;
                this.listarTodasRegimenAnioLectivoPorEstablecimiento();
                this.listarJornadasPorEstado();
                this.spinner.hide();
            },
            (error:any) => {
                this.spinner.hide();
                Swal.fire('Ups! ocurrió no se pudo obtener los datos','','error');
            }
        );
    }

    buscarListasCargarCombosEditar(cursoExt: CursoExtraordinaria) {
        this.spinner.show();
        this._mallaAsignaturaExtraService.obtenerTodosCursosExtraordinarioPorEstablecimiento(cursoExt.insestCodigo).subscribe(
            (respuesta: RespuestaCursoExtraordinariaInterfaz) => {
                this.listaCursoExtraos = respuesta.listado;
                this.listarJornadasPorEstado();
                this.listaMallaAsignaturaExtra = null;
                if (cursoExt.graCodigo != 0) { 
                    if (cursoExt.graCodigo == 4 ||  cursoExt.graCodigo == 5 || cursoExt.graCodigo == 6 || cursoExt.graCodigo == 7 || cursoExt.graCodigo == 8 || cursoExt.graCodigo == 9) {
                        Swal.fire(`No es necesario la malla de ${cursoExt.grado}` ,'','warning');
                    } else {
                        this.listarMallaAsignaturaExtraEditar(cursoExt.graCodigo, cursoExt);
                    }
                }
                
                
                
                
                if (cursoExt.modCodigo != 0) {
                    this.obtenerModuloPorCodigo(cursoExt);
                }

                this.listarServicioEducativoPorReanleCodigoEdi(cursoExt.sereduCodigo);

                /**
                 * Se desactiva ya que solo utilizacion modulo y grado
                 * if (cursoExt.graCodigo == 0 && cursoExt.modCodigo == 0) {
                    this.listaGradoEqivalenciaEditar(cursoExt);
                }
                 */

                
                this.spinner.hide();
            },
            (error:any) => {
                this.spinner.hide();
                Swal.fire('Ups! ocurrió no se pudo obtener los datos','','error');
            }
        );
    }


    buscarNemonicoServicioEditar(cod, sereCodigo){
        this._mallaAsignaturaExtraService.modalidadTipoEducacion(cod).subscribe((respuesta)=>{
            this.listaModalidades=respuesta;
            let objeto=this.listaModalidades.objeto;

            let datos ={
                nombreServicio: objeto.motiedDescripcion,
                codigoServicio: sereCodigo
            }

            this.complementoModalidadEdi.push(datos);

            this.nombreServicio=objeto.motiedDescripcion;
        
        }
        );
    }
    
    obtenerModuloPorCodigo(cursoExt: CursoExtraordinaria) {
        let modulo:Modulo;
        this._mallaAsignaturaExtraService.obtenerModuloPorCodigo(cursoExt.modCodigo).subscribe(
            (respuesta:RespuestaModuloInterfaz) => {
                modulo = respuesta.objeto;
                if ((modulo.modCodigoGradoInicio == 4 && modulo.modCodigoGradoFin == 5) || (modulo.modCodigoGradoInicio == 6 && modulo.modCodigoGradoFin == 7) || (modulo.modCodigoGradoInicio == 8 && modulo.modCodigoGradoFin == 9)) {
                    Swal.fire(`No es necesario la malla de ${modulo.modDescripcion}` ,'','warning');
                } else {
                    this.listarMallaAsignaturaExtraEditar(cursoExt.graCodigo, cursoExt);
                }
            },
            (error:any) => {
                this.spinner.hide();
                Swal.fire('Ups! ocurrió no se pudo obtener el modulo','','error');
            }
        );
    }

    listarTodasRegimenAnioLectivoPorEstablecimiento() {
        this.listaRegimenAnioLectivo =  null;
        let reanleCodigo = this.listaCursoExtraos.map((cursoExta) =>cursoExta.reanleCodigo);
        let reanleCodigoLimpio = Array.from(new Set(reanleCodigo));
        let regimenAnioLectParametros:RegimenAnioLectParametros = {listaReanleCodigo:reanleCodigoLimpio};
        this._mallaAsignaturaExtraService.obtenerTodasRegimenAnioLectivoPorListaEstablecimiento(regimenAnioLectParametros).subscribe(
            (respuesta: RespuestaRegimenAnioLectivoInterfaz) => {
                this.listaRegimenAnioLectivo = respuesta.listado; 
            },
            (error:any)=>{
                this.spinner.hide();
                Swal.fire('Ups! ocurrió un error al cargar los regimen con los años lectivos','','error'); 
            }
        );
    }

    limpiarCampos(reanleCodigo:any) {
        this.frmMallaAsignaturaExtraCreateEdit.get('sereduCodigo').reset();
        this.frmMallaAsignaturaExtraCreateEdit.get('modCodigo').reset();
        this.frmMallaAsignaturaExtraCreateEdit.get('jorCodigo').reset();
        this.frmMallaAsignaturaExtraCreateEdit.get('graCodigo').reset();
        this.frmMallaAsignaturaExtraCreateEdit.get('espCodigo').reset();
        this.variableAgrupacionSelecciona=null;
        this.listaDeAgrupacion =[];
        this.listaGrados = null;
        this.listaModulo = null;
        this.listaMallaAsignaturaExtra = null;
    }

    limpiarCamposPorJornada(jorCodigo:any) {
        this.frmMallaAsignaturaExtraCreateEdit.get('sereduCodigo').reset();
        this.frmMallaAsignaturaExtraCreateEdit.get('modCodigo').reset();
        this.frmMallaAsignaturaExtraCreateEdit.get('graCodigo').reset();
        this.frmMallaAsignaturaExtraCreateEdit.get('espCodigo').reset();
        this.variableAgrupacionSelecciona=null;
        this.listaDeAgrupacion = [];
        this.listaGrados = null;
        this.listaModulo = null;
        this.listaMallaAsignaturaExtra = null;
        this.obtenerCursoExtraPorReanleYJornada(jorCodigo)
    }

    obtenerCursoExtraPorReanleYJornada(jorCodigo:any) {
        let listaCursoExtraJor:CursoExtraordinaria[];
        let cursoExtraParametros:CursoExtraParametros = {reanleCodigo:this.frmMallaAsignaturaExtraCreateEdit.get('reanleCodigo').value, jorCodigo:jorCodigo};
        this._mallaAsignaturaExtraService.obtenerTodosCursosExtraordinarioPorReanleYJornada(cursoExtraParametros).subscribe(
            (respuesta: RespuestaCursoExtraordinariaInterfaz) => {
                listaCursoExtraJor = respuesta.listado;
                let listaSerCodigo = listaCursoExtraJor.map((cursoExtra) => cursoExtra.sereduCodigo);
                this.complementoModalidad=[];
                this.listarServicioEducativoPorReanleCodigo(listaSerCodigo);
            },
            (error:any)=>{
                this.spinner.hide();
                Swal.fire('Ups! ocurrió un error al otener Curso Extraordinario','','error'); 
            }
        );
    }

    listarServicioEducativoPorReanleCodigo(listaSerCodigo:number[]) {
        this.spinner.show();
        let servicioEducativoParametros:ServicioEducativoParametros = {listaSereduCodigo:listaSerCodigo};
        this._mallaAsignaturaExtraService.obtenerServicioEducativoPorCodigoEstudianes(servicioEducativoParametros).subscribe(
            (respuesta: RespuestaServicioEducativoInterfaz) => {
                this.spinner.hide();
                this.listaServicioEducativo = respuesta.listado;

                for(let i = 0; i < this.listaServicioEducativo.length; i++){
                    let codigo = this.listaServicioEducativo[i].motiedCodigo;
                    let codigoServicio = this.listaServicioEducativo[i].sereduCodigo;
                    this.buscarNombreServicio(codigo, codigoServicio);
                }
            },
            (error:any)=>{
                this.spinner.hide();
                Swal.fire('Ups! ocurrió un error los servicios Educativos','','error'); 
            }
        );
    }

    listarServicioEducativoPorReanleCodigoEdi(listaSerCodigo) {
        this._mallaAsignaturaExtraService.obtenerServicioEducativoPorCodigo(listaSerCodigo).subscribe(
            (respuesta: RespuestaServicioEducativoInterfaz) => {

                this.listaServicioEducativoEdi = respuesta.objeto;

                let codigo = this.listaServicioEducativoEdi.motiedCodigo;
                    let codigoServicio = this.listaServicioEducativoEdi.sereduCodigo;

                    this.buscarNemonicoServicioEditar(codigo, codigoServicio);

            }
        );
    }


    buscarNombreServicio(cod, sereCodigo){
        this._mallaAsignaturaExtraService.modalidadTipoEducacion(cod).subscribe((respuesta)=>{
            this.listaModalidades=respuesta;
            let objeto=this.listaModalidades.objeto;

            let datos ={
                nombreServicio: objeto.motiedDescripcion,
                codigoServicio: sereCodigo
            }

            this.complementoModalidad.push(datos);
        
        }
        );
    }

    listarJornadasPorEstado() {
        this.listaJornadas = null;
        let jorCodigo = this.listaCursoExtraos.map((cursoExta) =>cursoExta.jorCodigo);
        let jorCodigoLimpio = Array.from(new Set(jorCodigo));
        let jornadaParametros:JornadaParametros = {listaJorCodigo:jorCodigoLimpio};
        this._mallaAsignaturaExtraService.obtenerTodasJornadaPorEstado(jornadaParametros).subscribe(
            (respuesta: RespuestaJornadaInterfaz) =>{
                this.listaJornadas = respuesta.listado;
                let jornadaCodigo = this.frmMallaAsignaturaExtraCreateEdit.get('jorCodigo').value;
                let jornada:Jornada = this.listaJornadas.find((jornada) => jornada.jorCodigo == jornadaCodigo);
                if (this.frmMallaAsignaturaExtraCreateEdit.get('jornada') != null) {
                    this.frmMallaAsignaturaExtraCreateEdit.get('jornada').setValue(jornada.jorNombre);
                }
                
            },
            (error:any)=>{
                this.spinner.hide();
                Swal.fire('Ups! ocurrió un error al cargar las jornadas','','error'); 
            }
        );
    }

    cargarCombosGradoModulo (sereduCodigo:any) {
        this.spinner.show();
        let listaCursoExtGra:CursoExtraordinaria[] = this.listaCursoExtraos.filter((cursoExt)=>cursoExt.sereduCodigo==sereduCodigo && cursoExt.graCodigo != 0);
        let listaCursoExtMod:CursoExtraordinaria[] = this.listaCursoExtraos.filter((cursoExt)=>cursoExt.sereduCodigo==sereduCodigo && cursoExt.modCodigo != 0);
        this.listaGrados = null;
        this.listaModulo = null;
        this.listaMallaAsignaturaExtra = null;
        this.frmMallaAsignaturaExtraCreateEdit.get('modCodigo').reset();
        this.frmMallaAsignaturaExtraCreateEdit.get('graCodigo').reset();
        this.frmMallaAsignaturaExtraCreateEdit.get('espCodigo').reset();
        if (listaCursoExtGra != null && listaCursoExtGra != undefined && listaCursoExtGra.length != 0) {
            this.listarGradoPorEstado(listaCursoExtGra);
        } else if (listaCursoExtMod != null && listaCursoExtMod != undefined && listaCursoExtMod.length != 0) {
            this.listarModulos(listaCursoExtMod);
        }
        this.spinner.hide();
    }

    listarGradoPorEstado(listaCursoExt:CursoExtraordinaria[]) {
        let graCodigo = listaCursoExt.map((cursoExta) =>cursoExta.graCodigo);
        let graCodigoLim = Array.from(new Set(graCodigo));
        let gradosParametros:GradosParametros = {listaGraCodigo:graCodigoLim};
        this._mallaAsignaturaExtraService.obtenerTodosGradorPorListaGraCodigoYEstado(gradosParametros).subscribe(
            (respuesta:RespuestaGradoInterfaz) =>{
                this.listaGrados = null;
                this.listaModulo = null;
                this.listaGrados = respuesta.listado;
                this.cargarComboDeAgrupacion();
            },
            (error:any)=>{
                this.spinner.hide();
                Swal.fire('Ups! ocurrió un error al cargar los grados','','error'); 
            }
        );
    }


    cargarComboDeAgrupacion(){
        this._mallaAsignaturaExtraService.listaDeAgrupaciones().then(
            (respuesta) => {
                this.listaDeAgrupacion=respuesta.listado;
            },
            (error: any)=> {
                this.spinner.hide();
                Swal.fire()
            }
        )
    }

    listarModulos(listaCursoExt:CursoExtraordinaria[]){
        let modCodigo = listaCursoExt.map((cursoExta) =>cursoExta.modCodigo);
        let modCodigoLim = Array.from(new Set(modCodigo));
        let moduloParametros:ModuloParametros = {listaModCodigo:modCodigoLim};
        this._mallaAsignaturaExtraService.obtenerModulosPorListaCodigos(moduloParametros).subscribe(
            (respuesta:RespuestaModuloInterfaz) =>{
                this.listaModulo = null;
                this.listaGrados = null; 
                this.listaModulo = respuesta.listado;
                this.cargarComboDeAgrupacion();
            },
            (error:any)=>{
                this.spinner.hide();
                Swal.fire('Ups! ocurrió un error al cargar los modulos','','error'); 
            }
        );
    }

    /**
     * Se desactiva por que no se usa el grado equivalencia, se maneja solo modulos y grados
     * 
     * listaGradoEqivalencia() {
        let sereduCodigo = this.frmMallaAsignaturaExtraCreateEdit.get('sereduCodigo').value;
        let gradoEquivalencia:GradoEquivalencia;
        this._mallaAsignaturaExtraService.obtenerGradoEquivaleciaPorServicioEducativa(sereduCodigo).subscribe(
            (respuesta:RespuestaGradoEquivalenciaInterfaz) => {
                gradoEquivalencia = respuesta.listado[0];
                if (gradoEquivalencia.graequGradoInicio >= 10) {
                    this.obtenerListaCursoExtraordinariaPorGradoCodigoYEstablecimiento(gradoEquivalencia.graequGradoInicio);
                } 
            },
            (error:any)=>{
                this.spinner.hide();
                Swal.fire('Ups! ocurrió un error al cargar el grado equivalecia','','error'); 
            }
        );
    }
     */
    
    /**
     * Se desactiva por que no se usa el grado equivalencia, se maneja solo modulos y grados
     * listaGradoEqivalenciaEditar(cursoExt: CursoExtraordinaria) {
        let gradoEquivalencia:GradoEquivalencia;
        this._mallaAsignaturaExtraService.obtenerGradoEquivaleciaPorServicioEducativa(cursoExt.sereduCodigo).subscribe(
            (respuesta:RespuestaGradoEquivalenciaInterfaz) => {
                gradoEquivalencia = respuesta.listado[0];
                if (gradoEquivalencia != null || gradoEquivalencia != undefined) {
                    if ((gradoEquivalencia.graequGradoInicio == 4 && gradoEquivalencia.graequGradoFin == 5) || (gradoEquivalencia.graequGradoInicio == 6 && gradoEquivalencia.graequGradoFin == 9)) {
                        Swal.fire(`No es necesario la malla de ${gradoEquivalencia.graequDescripcion}` ,'','warning');
                    } else {
                        this.listarMallaAsignaturaExtraEditar(gradoEquivalencia.graequGradoInicio, cursoExt);
                    }  
                } else {
                    Swal.fire(`No existe datos` ,'','warning');
                }
            },
            (error:any)=>{
                Swal.fire('Ups! ocurrió al cargar los datos del grado equivalencia','','error'); 
            }
        );
    }
     */

    

    obtenerListaCursoExtraordinariaPorGradoCodigoYEstablecimiento(codGrado:any) {

        if(this.variableAgrupacionSelecciona != "0"){
        this.spinner.hide();
        let insestCodigo = this.frmMallaAsignaturaExtraCreateEdit.get('insestCodigo').value;
        let listaCursoExts:CursoExtraordinaria[];
        let cursoExtraParametros:CursoExtraParametros = {graCodigo: codGrado, insestCodigo:insestCodigo};
        this._mallaAsignaturaExtraService.obtenerTodosCursosExtraordinarioPorGradoEstablecimientoYGrado(cursoExtraParametros).subscribe(
            (respuesta:RespuestaCursoExtraordinariaInterfaz)=>{
                listaCursoExts = respuesta.listado;
                this.listaMallaAsignaturaExtra = null;
                this.listaTodasLasEspecialidadesPorListaEspCodigoYGrado(listaCursoExts, codGrado);
                this.spinner.hide();
            },
            (error:any)=>{
                Swal.fire('Ups! ocurrió al cargar los datos de las especialidades','','error'); 
            }
        );
    }   
    }

    obtenerListaCursoExtraordinarioPorModuloYEstablecimiento(modCodigo:any) {
        let modulo:Modulo = this.listaModulo.find((modulo)=>modulo.modCodigo==modCodigo);
        if ((modulo.modCodigoGradoInicio == 4 && modulo.modCodigoGradoFin == 5) || (modulo.modCodigoGradoInicio == 6 && modulo.modCodigoGradoFin == 7) || (modulo.modCodigoGradoInicio == 8 && modulo.modCodigoGradoFin == 9)) {
            Swal.fire(`No es necesario la malla de ${modulo.modDescripcion}` ,'','warning');
            return;
        }
        if (modulo.modCodigoGradoInicio != 13 && modulo.modCodigoGradoInicio != 14 && modulo.modCodigoGradoInicio != 15 && modulo.modCodigoGradoInicio != 19 && modulo.modCodigoGradoInicio != 20) {
            this.listarCursoExtraordinaria(modulo.modCodigo);
        } else {
            let insestCodigo = this.frmMallaAsignaturaExtraCreateEdit.get('insestCodigo').value;
            let listaCursoExts:CursoExtraordinaria[];
            let cursoExtraParametros:CursoExtraParametros = {modCodigo:modCodigo,insestCodigo:insestCodigo};
            this._mallaAsignaturaExtraService.obtenerTodosCursosExtraordinarioPorModuloEstablecimientoYGrado(cursoExtraParametros).subscribe(
                (respuesta:RespuestaCursoExtraordinariaInterfaz)=>{
                    listaCursoExts = respuesta.listado;
                    this.listaTodasLasEspecialidadesPorListaEspCodigoYEspecialidad(listaCursoExts, modulo.modCodigo,  modulo.modCodigoGradoInicio);
                    this.spinner.hide();
                },
                (error:any)=>{
                    Swal.fire('Ups! ocurrió al cargar los datos de las especialidades','','error'); 
                }
            );
        }
    }

    listaTodasLasEspecialidadesPorListaEspCodigoYGrado(listaCursoExtraos:CursoExtraordinaria[], codGrado:any){
        let espCodigo = listaCursoExtraos.map((cursoExta) =>cursoExta.espCodigo);
        this.listaEspecialidad = null;
        if (espCodigo.length != 0) {
            let especialidadParamatetros:EspecialidadParamatetros = {listaEspCodigo: espCodigo};
            this._mallaAsignaturaExtraService.listarLasEspecialidadesPorListaEspCodigoYEstado(especialidadParamatetros).subscribe(
                (respuesta:RespuestaEspecialidadInterfaz)=>{
                    this.listaEspecialidad = respuesta.listado;
                    this.listarCursoExtraordinaria(codGrado);   
                },
                (error:any)=>{
                    this.spinner.hide();
                    Swal.fire('Ups! ocurrió un error al cargar Especialidades','','error'); 
                }
            );
        } else {
            this.listarCursoExtraordinaria(codGrado);
        }
    }

    listaTodasLasEspecialidadesPorListaEspCodigoYEspecialidad(listaCursoExtraos:CursoExtraordinaria[], modCodigo:any, codGrado:any) {
        let espCodigo = listaCursoExtraos.map((cursoExta) =>cursoExta.espCodigo);
        this.listaEspecialidad = null;
        if (espCodigo.length != 0) {
            let especialidadParamatetros:EspecialidadParamatetros = {listaEspCodigo:espCodigo}; 
            this._mallaAsignaturaExtraService.listarLasEspecialidadesPorListaEspCodigoYEstado(especialidadParamatetros).subscribe(
                (respuesta:RespuestaEspecialidadInterfaz)=>{
                    this.listaEspecialidad = respuesta.listado;
                    this.listarCursoExtraordinaria(modCodigo);   
                },
                (error:any)=>{
                    this.spinner.hide();
                    Swal.fire('Ups! ocurrió un error al cargar Especialidades','','error'); 
                }
            );
        } else {
            if (this.listaGrados !=  null) {
                this.listarCursoExtraordinaria(codGrado);
            }
            if (this.listaModulo != null) {
                this.listarCursoExtraordinaria(modCodigo);
            }
            
        }
    }

    listarCursoExtraordinaria(codigo:any) {
        let reanleCodigo = this.frmMallaAsignaturaExtraCreateEdit.get('reanleCodigo').value;
        let insestCodigo = this.frmMallaAsignaturaExtraCreateEdit.get('insestCodigo').value;
        let sereduCodigo = this.frmMallaAsignaturaExtraCreateEdit.get('sereduCodigo').value;
        let codGrado = 0;
        if (this.listaGrados !=  null) {
            codGrado = codigo;
        }
        let modCodigo = 0;
        if (this.listaModulo != null) {
            modCodigo = codigo;
        }
        let jorCodigo = this.frmMallaAsignaturaExtraCreateEdit.get('jorCodigo').value;
        let espCodigo = 0;
        if (this.listaEspecialidad != null) {
            espCodigo = this.frmMallaAsignaturaExtraCreateEdit.get('espCodigo').value;
        }
        this.cursoExtraordinaria = null;
        let cursoExtraParametros:CursoExtraParametros = {
            reanleCodigo:reanleCodigo,
            insestCodigo:insestCodigo,
            sereduCodigo:sereduCodigo,
            modCodigo:modCodigo,
            jorCodigo:jorCodigo,
            graCodigo:codGrado,
            espCodigo:espCodigo
        };
        this._mallaAsignaturaExtraService.obtenerTodosCursoExtraordinariaPorCodigos(cursoExtraParametros).subscribe(
            (respuesta:RespuestaCursoExtraordinariaInterfaz)=>{
                this.cursoExtraordinaria=respuesta.objeto;
                this.listarMallaAsignaturaExtra(codGrado);
            },
            (error:any)=>{
                this.spinner.hide();
                Swal.fire('Ups! ocurrió un error el curso extraordinario','','error'); 
            }
        );
    }

    listarCursoExtraordinariaEsp(espCodigo:any) {
        let modCodigo = 0;
        if (this.frmMallaAsignaturaExtraCreateEdit.get('modCodigo').value != null) {
            modCodigo = this.frmMallaAsignaturaExtraCreateEdit.get('modCodigo').value;
        }

        let graCodigo = 0;
        if (this.frmMallaAsignaturaExtraCreateEdit.get('graCodigo').value != null) {
            graCodigo = this.frmMallaAsignaturaExtraCreateEdit.get('graCodigo').value;
        }

        let cursoExtraParametros:CursoExtraParametros = {
            reanleCodigo:this.frmMallaAsignaturaExtraCreateEdit.get('reanleCodigo').value,
            insestCodigo:this.frmMallaAsignaturaExtraCreateEdit.get('insestCodigo').value,
            sereduCodigo:this.frmMallaAsignaturaExtraCreateEdit.get('sereduCodigo').value,
            modCodigo:modCodigo,
            jorCodigo:this.frmMallaAsignaturaExtraCreateEdit.get('jorCodigo').value,
            graCodigo:graCodigo,
            espCodigo:espCodigo
        };
        this._mallaAsignaturaExtraService.obtenerTodosCursoExtraordinariaPorCodigos(cursoExtraParametros).subscribe(
            (respuesta:RespuestaCursoExtraordinariaInterfaz)=>{
                this.cursoExtraordinaria=respuesta.objeto;
                this.listarMallaAsignaturaExtra(cursoExtraParametros.graCodigo);
            },
            (error:any)=>{
                this.spinner.hide();
                Swal.fire('Ups! ocurrió un error el curso extraordinario','','error'); 
            }
        );
    }

    listarMallaAsignaturaExtra(codGrado:any) {
        if (this.cursoExtraordinaria != null) {
            let curextCodigo = this.cursoExtraordinaria.curextCodigo;
            let mallaAsigExtraParametro:MallaAsigExtraParametro  = {
                curextCodigo: curextCodigo,
                grado: codGrado
            }
            this._mallaAsignaturaExtraService.obtenerListaMallaAsignaturaExtraPorCurextCodigoYGrado(mallaAsigExtraParametro).subscribe(
                (respuesta: RespuestaMallaAsignaturaExtraInterfaz)=>{
                    this.listaMallaAsignaturaExtra = respuesta.listado;
                    this.dataSource = new MatTableDataSource(this.listaMallaAsignaturaExtra);
                    for (let i = 0; i < this.listaMallaAsignaturaExtra.length; i++) {
                        if (this.listaMallaAsignaturaExtra[i].maasiexEstado == 1) {
                            this.listaMallaAsignaturaExtra[i].estadoCHB = true;
                        } else if (this.listaMallaAsignaturaExtra[i].maasiexEstado == 0) {
                            this.listaMallaAsignaturaExtra[i].estadoCHB = false;
                        }
                    }
                    this.dataSource.sort = this.sort;
                },
                (error:any)=>{
                    this.spinner.hide();
                    Swal.fire('Ups! ocurrió un error con la malla asignatura extra','','error'); 
                }
            );
        }
    }

    listarMallaAsignaturaExtraEditar(codGrado:any, cursoExt: CursoExtraordinaria) {
        let curextCodigo = cursoExt.curextCodigo;
        let mallaAsigExtraParametro:MallaAsigExtraParametro  = {
            curextCodigo: curextCodigo,
            grado: codGrado
        }
        this._mallaAsignaturaExtraService.obtenerListaMallaAsignaturaExtraPorCurextCodigoYGrado(mallaAsigExtraParametro).subscribe(
            (respuesta: RespuestaMallaAsignaturaExtraInterfaz)=>{
                this.listaMallaAsignaturaExtra = respuesta.listado;
                this.dataSource = new MatTableDataSource(this.listaMallaAsignaturaExtra);
                for (let i = 0; i < this.listaMallaAsignaturaExtra.length; i++) {
                    if (this.listaMallaAsignaturaExtra[i].maasiexEstado == 1) {
                        this.listaMallaAsignaturaExtra[i].estadoCHB = true;
                    } else if (this.listaMallaAsignaturaExtra[i].maasiexEstado == 0) {
                        this.listaMallaAsignaturaExtra[i].estadoCHB = false;
                    }
                }
                this.dataSource.sort = this.sort;
            },
            (error:any)=>{
                this.spinner.hide();
                Swal.fire('Ups! ocurrió un error con la malla asignatura extra','','error'); 
            }
        );
    }
    
    listarEspecialidadPorListaCodigo() {
        let listaEspe:Especialidad[];
        let espCodigoConCeros = this.listaCursoExtraordinaria.map((curext) => curext.espCodigo);
        let espCodigo = espCodigoConCeros.filter((espCodigo)=>espCodigo!=0);
        let espCodigoLimpio = Array.from(new Set(espCodigo));
        if(espCodigoLimpio.length>0) {
            let especialidadParamatetros:EspecialidadParamatetros = {listaEspCodigo:espCodigoLimpio}; 
            this._mallaAsignaturaExtraService.listarLasEspecialidadesPorListaEspCodigoYEstado(especialidadParamatetros).subscribe(
                (respuesta:RespuestaEspecialidadInterfaz)=>{
                    listaEspe = respuesta.listado;
                    for(let i=0; i<this.listaCursoExtraordinaria.length; i++){
                        let espec:Especialidad=listaEspe.find((espec)=>espec.espCodigo==this.listaCursoExtraordinaria[i].espCodigo);
                        this.listaCursoExtraordinaria[i].especialidad = espec.espDescripcion;
                    }
                }
            );
        }  
    }

    cambiarEstadoMallaHoraExtra(mAsigExtra:MallaAsignaturaExtra) {
        if (mAsigExtra.estadoCHB) {
            mAsigExtra.maasiexEstado = 1;
        } else {
            mAsigExtra.maasiexEstado = 0;
        }
    }

    calcularHorasTotalAsignatura(mAsigExtra:MallaAsignaturaExtra) {
        if (mAsigExtra.maasiexHorasPre == 0) {
            Swal.fire('Necesita ingresar las horas presenciales','','success'); 
            return;
        }
        let total = mAsigExtra.maasiexHorasPre + mAsigExtra.maasiexHorasAuto;
        mAsigExtra.maasiexHraTotal = total;
    }

    nuevaMallaAsignaturaExtra(modalForm) {
        this.listaDeAgrupacion=[];
        this.variableAgrupacionSelecciona=null;
        this.submitted=false;
        this.listaMallaAsignaturaExtra = null;
        this.listaGrados = null;
        this.listaModulo = null; 
        this.frmMallaAsignaturaExtraCreateEdit = this.fb.group({
            reanleCodigo: [null, [Validators.required]],
            insCodigo: [this.institucion.insCodigo],
            insestCodigo: [null, [Validators.required]],
            sereduCodigo: [null, [Validators.required]],
            modCodigo: [null, [Validators.required]],
            jorCodigo: [null, [Validators.required]],
            graCodigo: [null, [Validators.required]],
            espCodigo: [null]
        });
        this.modalService.open(modalForm, {size: 'lg'});
    }

    guardarMallaAsignaturaExtra(modalForm) {
        for (let i = 0; i < this.listaMallaAsignaturaExtra.length; i++) {
            if (this.listaMallaAsignaturaExtra[i].maasiexNemonico == null) {
                Swal.fire('Necesita ingresar el nemónico todas las asignaturas','','error');
                return;
            }
            if (this.listaMallaAsignaturaExtra[i].maasiexHorasPre == 0) {
                Swal.fire('Necesita ingresar las horas presenciales en todas las asignaturas','','error');
                return;
            }
            if (this.listaMallaAsignaturaExtra[i].maasiexHorasAuto == 0) {
                Swal.fire('Necesita ingresar las horas autonomas en todas las asignaturas','','error');
                return;
            }
            this.listaMallaAsignaturaExtra[i].curextCodigo = this.cursoExtraordinaria.curextCodigo;
            this.listaMallaAsignaturaExtra[i].reanleCodigo = this.cursoExtraordinaria.reanleCodigo;
        }

        this.spinner.show();
        this._mallaAsignaturaExtraService.guardarListaMallaAsignaturaExtra(this.listaMallaAsignaturaExtra).subscribe(
            (respuesta:any)=>{
                Swal.fire(`Malla asignatura extra creada!`,'','success');
                this.spinner.hide();
                
            },
            (error:any)=>{
                Swal.fire(`No se pudo crear la Malla asignatura extra`,'','error');
                this.spinner.hide();
                
            },
        );
        modalForm.close('Accept click');
    }

    editarMallaAsignaturaExtra(modalForm, cursoExt: CursoExtraordinaria) {
        this.cursoEstraSelecionanda = cursoExt;
        let establecimiento: InstEstablecimiento = this.listaInstEstablecimento.find((establecimiento) => establecimiento.insestCodigo == cursoExt.insestCodigo);
        if (cursoExt.espCodigo == 0) {
            this.frmMallaAsignaturaExtraCreateEdit = this.fb.group({
                reanleCodigo: [cursoExt.reanleCodigo],
                reanleNombre: [cursoExt.anioLectivo],
                insCodigo: [this.institucion.insCodigo],
                insestCodigo: [cursoExt.insestCodigo],
                establecmiento: [establecimiento.nombreTipo],
                sereduCodigo: [cursoExt.sereduCodigo],
                sevicio:[cursoExt.servEducativo],
                modCodigo: [cursoExt.modCodigo],
                jorCodigo: [cursoExt.jorCodigo],
                jornada: [''],
                graCodigo: [cursoExt.graCodigo],
                nombreGrado: [cursoExt.grado]
            });
        } else {
            this.frmMallaAsignaturaExtraCreateEdit = this.fb.group({
                reanleCodigo: [cursoExt.reanleCodigo],
                reanleNombre: [cursoExt.anioLectivo],
                insCodigo: [this.institucion.insCodigo],
                insestCodigo: [cursoExt.insestCodigo],
                establecmiento: [establecimiento.nombreTipo],
                sereduCodigo: [cursoExt.sereduCodigo],
                sevicio:[cursoExt.servEducativo],
                modCodigo: [cursoExt.modCodigo],
                jorCodigo: [cursoExt.jorCodigo],
                jornada: [''],
                graCodigo: [cursoExt.graCodigo],
                nombreGrado: [cursoExt.grado],
                espCodigo: [cursoExt.espCodigo],
                nombreEsp: [cursoExt.especialidad]
            });
        }
        this.buscarListasCargarCombosEditar(cursoExt);
        this.cursoExtraordinaria = cursoExt;
        this.modalService.open(modalForm, {size: 'lg'});
    }

    applyFilter(event: Event) {
        const filterValue = (event.target as HTMLInputElement).value;
        this.dataSource.filter = filterValue.trim().toLowerCase();
        if (this.dataSource.paginator) {
          this.dataSource.paginator.firstPage();
        }
    }

    applyFilterCursoEx(event: Event) {
        const filterValue = (event.target as HTMLInputElement).value;
        this.dataSourceCursoE.filter = filterValue.trim().toLowerCase();
        if (this.dataSourceCursoE.paginator) {
          this.dataSourceCursoE.paginator.firstPage();
        }
    }

    get ReactiveFrmMallaAsignaturaExtraCreateEdit() {
        return this.frmMallaAsignaturaExtraCreateEdit.controls;
    }

}