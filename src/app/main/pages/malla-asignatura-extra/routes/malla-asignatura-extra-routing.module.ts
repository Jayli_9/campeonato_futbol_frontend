import { Routes } from "@angular/router";
import { AuthGuard } from "app/auth/helpers";
import { MallaAsignaturaExtraComponent } from "../components/malla-asignatura-extra/malla-asignatura-extra.component";


export const RUTA_MALLA_ASIGNATURA_EXTRA:Routes=[
    {
        path:'malla-asignatura-extra',
        component:MallaAsignaturaExtraComponent
    }
]