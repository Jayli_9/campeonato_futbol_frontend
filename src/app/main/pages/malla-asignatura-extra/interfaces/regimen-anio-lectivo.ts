export interface RegimenAnioLectivo {
    reanleCodigo?: number;
	regCodigo: number;
	anilecCodigo: number;
	anilecAnioInicio: number;
	anilecAnioFin: number;
	regDescripcion: string;
}