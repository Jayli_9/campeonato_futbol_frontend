export interface Asignatura {
    asiCodigo: number;
	asiDescripcion: string;
	asiEstado: number;
	asiHoras: number;
	asiNemonico: string;
	espCodigo?: number;
	graCodigo: number;
	acaAgrupacion: number;
	acaAreaConocimiento: number;
	acaTipoAsignatura: number;
	acaTipoValoracion: number;
}