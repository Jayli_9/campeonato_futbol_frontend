export interface AgrupacionInterface{
       agrCodigo: number;
       agrDescripcion: string;
       agrEstado: number;
       agrNemonico: string;
}