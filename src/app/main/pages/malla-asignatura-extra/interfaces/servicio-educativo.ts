export interface ServicioEducativo {
    sereduCodigo:number;
    sereduDescripcion: string;
    sereduIntensivo:number;
    modalidad?:string;
    motiedCodigo: number;
}