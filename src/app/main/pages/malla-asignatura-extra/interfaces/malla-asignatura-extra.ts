export interface MallaAsignaturaExtra {
    maasiexCodigo?: number;
    asiCodigo: number;
    asiDescripcion: string;
    tivaCodigo?:number;
    maasiexNemonico: String;
    maasiexEstado: number;
    maasiexHorasPre: number;
    maasiexHorasAuto: number;
    maasiexHraTotal: number;
    curextCodigo: number;
    reanleCodigo: number;
    estadoCHB?:boolean;
}
