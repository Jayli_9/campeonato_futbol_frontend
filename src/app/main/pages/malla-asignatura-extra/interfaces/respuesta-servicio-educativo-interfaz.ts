import { RespuestaGeneralInterfaz } from "app/main/shared/interfaces/respuesta-general-interfaz";
import { ServicioEducativo } from "./servicio-educativo";

export interface RespuestaServicioEducativoInterfaz extends RespuestaGeneralInterfaz {
    listado: ServicioEducativo[];
    objeto: ServicioEducativo;
}