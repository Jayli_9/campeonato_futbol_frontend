export interface CursoExtraordinaria {
    curextCodigo: number;
    espCodigo: number;
    graCodigo: number;
    insestCodigo: number;
    jorCodigo: number;
    reanleCodigo: number;
    sereduCodigo: number;
    modCodigo: number;
    curextEstado: number;
    anioLectivo: string;
    institucion: string;
    servEducativo: string;
    grado: string;
    especialidad: string;
    modalidad:string;
    nisediCodigo:number;
    jornada:string;
}