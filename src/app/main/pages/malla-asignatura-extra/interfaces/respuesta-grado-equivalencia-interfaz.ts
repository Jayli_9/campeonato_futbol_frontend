import { RespuestaGeneralInterfaz } from "app/main/shared/interfaces/respuesta-general-interfaz";
import { GradoEquivalencia } from "./grado-equivalencia";

export interface RespuestaGradoEquivalenciaInterfaz extends RespuestaGeneralInterfaz {
    listado: GradoEquivalencia[];
    objeto: GradoEquivalencia;
}