export interface ModalidadEducacionInterface{
    motiedCodigo: number;
    motiedDescripcion: string;
    motiedEstado: number;
    modCodigo: number;
    tipeduCodigo: number;
}