import { Routes } from "@angular/router";
import { AuthGuard } from "app/auth/helpers/auth.guards";
import { AgrupacionComponent } from "../components/agrupacion/agrupacion.component";

export const RUTA_AGRUPACION:Routes=[
    {
        path:'agrupacion',
        component:AgrupacionComponent,
        canActivate:[AuthGuard]
    }
]