import { RespuestaGeneralInterfaz } from "app/main/shared/interfaces/respuesta-general-interfaz";
import { Agrupacion } from "./agrupacion";

export interface RespuestaAgrupacionInterfaz extends RespuestaGeneralInterfaz{
    listado: Agrupacion[];
    objeto: Agrupacion;
}
