import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AgrupacionComponent } from './agrupacion.component';

describe('AgrupacionComponent', () => {
  let component: AgrupacionComponent;
  let fixture: ComponentFixture<AgrupacionComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AgrupacionComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AgrupacionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
