import { Component, OnInit, ViewChild } from '@angular/core';
import { UntypedFormBuilder, UntypedFormGroup, Validators } from '@angular/forms';
import {MatPaginator} from '@angular/material/paginator';
import {MatSort} from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import Swal from 'sweetalert2';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { NgxSpinnerService } from 'ngx-spinner';
import { Agrupacion } from '../../interfaces/agrupacion';
import { AgrupacionService } from '../../services/agrupacion.service';
import { RespuestaAgrupacionInterfaz } from '../../interfaces/respuesta-agrupacion-interfaz';

@Component({
  selector: 'app-agrupacion',
  templateUrl: './agrupacion.component.html',
  styleUrls: ['./agrupacion.component.scss']
})
export class AgrupacionComponent implements OnInit {

  public contentHeader: object;
  public frmAgrupacionCreateEdit: UntypedFormGroup;
  public submitted = false;
  listaAgrupacion:Agrupacion[]=null;
  columnasAgrupacion=['num','descripcion','nemonico','estado','acciones'];
  dataSource: MatTableDataSource<Agrupacion>;

  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;
  constructor(
    private readonly fb: UntypedFormBuilder,
    private modalService: NgbModal,
    public spinner: NgxSpinnerService,
    private _agrupacionService:AgrupacionService
  ) { }

  ngOnInit(): void {
    this.contentHeader = {
      headerTitle: 'Agrupación',
      actionButton: false,
      breadcrumb: {
        type: '',
        links: [
          {
            name: 'Inicio',
            isLink: true,
            link: '/pages/inicio'
          },
          {
            name: 'Agrupacion',
            isLink: false
          },
        ]
      }
    };
    this.listarTodasLasAgrupaciones();
  }

  listarTodasLasAgrupaciones(){
    this.spinner.show();
    this._agrupacionService.listarTodasLasAgrupaciones().subscribe(
      (respuesta:RespuestaAgrupacionInterfaz)=>{
        this.listaAgrupacion=respuesta.listado;
        this.dataSource = new MatTableDataSource(this.listaAgrupacion);
        this.dataSource.paginator = this.paginator;
        this.dataSource.paginator._intl.itemsPerPageLabel="Agrupaciones por página";
        this.dataSource.paginator._intl.nextPageLabel="Siguiente";
        this.dataSource.paginator._intl.previousPageLabel="Anterior";
        this.dataSource.sort = this.sort;
        this.spinner.hide();
      },
      (error:any)=>{
        this.spinner.hide();
       Swal.fire('Ups! ocurrió un error al cargar Agrupaciones','','error'); 
      }
    );
  }
  nuevaAgrupacion(modalForm){
    this.submitted=false;
    this.frmAgrupacionCreateEdit=this.fb.group({
      descripcion: ['', [Validators.required]],
      nemonico: ['', [Validators.required,Validators.maxLength(10)]]
    });
    this.modalService.open(modalForm);
  }
  editAgrupacion(modalForm,agrupacionEdit:Agrupacion){
    this.submitted=false;
    this.frmAgrupacionCreateEdit=this.fb.group({
      codigo:[agrupacionEdit.agrCodigo],
      descripcion: [agrupacionEdit.agrDescripcion, [Validators.required]],
      nemonico: [agrupacionEdit.agrNemonico, [Validators.required,Validators.maxLength(10)]],
      estado:[agrupacionEdit.agrEstado]
    });
    this.modalService.open(modalForm);
  }

  guardarAgrupacion(modalForm){
    this.spinner.show();
    this.submitted = true;
    if (this.frmAgrupacionCreateEdit.invalid) {
      this.spinner.hide();
      return;
    }
    let agrupacionNueva:Agrupacion={
      agrDescripcion:this.frmAgrupacionCreateEdit.get("descripcion").value,
      agrNemonico:this.frmAgrupacionCreateEdit.get("nemonico").value,
      agrEstado:1
    };
    this._agrupacionService.guardarActualizarAgrupacion(agrupacionNueva).subscribe(
      (respuesta:any)=>{
          Swal.fire(`Agrupación Creada!`,'','success');
          this.spinner.hide();
          this.listarTodasLasAgrupaciones();
          
      },
      (error:any)=>{
        Swal.fire(`No se pudo crear la Agrupación `,'','error'); 
        this.spinner.hide();
        this.listarTodasLasAgrupaciones();
      }
    );
    modalForm.close('Accept click');  
  }

  activarInactivar(agrupacionActualizar:Agrupacion){
    this.spinner.show();
    if (agrupacionActualizar.agrEstado==1) {
      this._agrupacionService.inactivarAgrupacion(agrupacionActualizar.agrCodigo).subscribe(
        (respuesta:any)=>{
          this.spinner.hide();
          Swal.fire(' Agrupación Inactivada','','success'); 
          this.listarTodasLasAgrupaciones();
        },
        (error:any)=>{
          this.spinner.hide();
          Swal.fire('Ups! ocurrió un error','','error'); 
          this.listarTodasLasAgrupaciones();
        }
      );
    } else {
      agrupacionActualizar.agrEstado=1;
      this._agrupacionService.guardarActualizarAgrupacion(agrupacionActualizar).subscribe(
        (respuesta:any)=>{
          this.spinner.hide();
          Swal.fire(' Agrupación Activada','','success'); 
          this.listarTodasLasAgrupaciones();
        },
        (error:any)=>{
          this.spinner.hide();
          Swal.fire('Ups! ocurrió un error','','error'); 
          this.listarTodasLasAgrupaciones();
        }
      );
    }
    
}
actualizarAgrupacion(modalForm){
  this.spinner.show();
    this.submitted = true;
    // stop here if form is invalid
    if (this.frmAgrupacionCreateEdit.invalid) {
      this.spinner.hide();
      return;
    }
    let agrupacionGuardar:Agrupacion={
      agrCodigo:this.frmAgrupacionCreateEdit.get("codigo").value,
      agrDescripcion:this.frmAgrupacionCreateEdit.get("descripcion").value,
      agrNemonico:this.frmAgrupacionCreateEdit.get("nemonico").value,
      agrEstado:this.frmAgrupacionCreateEdit.get("estado").value,
    };
    this._agrupacionService.guardarActualizarAgrupacion(agrupacionGuardar).subscribe(
      (respuesta:any)=>{
          Swal.fire(`Agrupación Actualizada!`,'','success'); 
          this.spinner.hide();
          this.listarTodasLasAgrupaciones();
      },
      (error:any)=>{
        Swal.fire(`No se pudo guardar la Agrupación`,'','error'); 
        this.spinner.hide();
        this.listarTodasLasAgrupaciones();
      }
    );
    modalForm.close('Accept click');
}
applyFilter(event: Event) {
  const filterValue = (event.target as HTMLInputElement).value;
  this.dataSource.filter = filterValue.trim().toLowerCase();

  if (this.dataSource.paginator) {
    this.dataSource.paginator.firstPage();
  }
}
 // getter for easy access to form fields
 get ReactiveFrmAgrupacionCreateEdit() {
  return this.frmAgrupacionCreateEdit.controls;
 }
}
