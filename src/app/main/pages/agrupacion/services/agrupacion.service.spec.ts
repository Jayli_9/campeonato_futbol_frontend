import { TestBed } from '@angular/core/testing';

import { AgrupacionService } from './agrupacion.service';

describe('AgrupacionService', () => {
  let service: AgrupacionService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(AgrupacionService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
