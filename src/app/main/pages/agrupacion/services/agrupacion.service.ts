import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'environments/environment';
import { Agrupacion } from '../interfaces/agrupacion';
import { RespuestaAgrupacionInterfaz } from '../interfaces/respuesta-agrupacion-interfaz';

@Injectable({
  providedIn: 'root'
})
export class AgrupacionService {

  url_academico=environment.url_academico;
  constructor(
    public _http:HttpClient
  ) { }
  guardarActualizarAgrupacion(agrupacion:Agrupacion){
    let url_ws=`${this.url_academico}/private/guardarAgrupacion`;
    return this._http.post(url_ws,agrupacion);
  }

  listarTodasLasAgrupaciones(){
    let url_ws=`${this.url_academico}/private/listarTodasLasAgrupaciones`;
    return this._http.get<RespuestaAgrupacionInterfaz>(url_ws);
  }

  listarAgrupacionesActivas(){
    let url_ws=`${this.url_academico}/private/listarAgrupacionesActivas`;
    return this._http.get<RespuestaAgrupacionInterfaz>(url_ws);
  }

  buscarAgrupacionPorCodigo(codigo:number){
    let url_ws=`${this.url_academico}/private/buscarAgrupacionPorCodigo/${codigo}`;
    return this._http.get<RespuestaAgrupacionInterfaz>(url_ws);
  }
  
  inactivarAgrupacion(codigo:number){
    let url_ws=`${this.url_academico}/private/eliminarAgrupacionPorId/${codigo}`;
    return this._http.delete(url_ws);
  }
}
