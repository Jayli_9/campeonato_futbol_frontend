import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AgrupacionComponent } from '../components/agrupacion/agrupacion.component';
import { RUTA_AGRUPACION } from '../routes/agrupacion-routing.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { CoreCommonModule } from '@core/common.module';
import { ContentHeaderModule } from 'app/layout/components/content-header/content-header.module';
import { MaterialModule } from 'app/main/shared/material/material.module';



@NgModule({
  declarations: [AgrupacionComponent],
  imports: [
    CommonModule,
    RouterModule.forChild(RUTA_AGRUPACION),
    FormsModule,
    ReactiveFormsModule,
    MaterialModule,
    ContentHeaderModule,
    CoreCommonModule
  ]
})
export class AgrupacionModule { }
