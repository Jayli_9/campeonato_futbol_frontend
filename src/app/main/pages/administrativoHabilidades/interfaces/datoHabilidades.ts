export interface datoHabilidadesInterface{
    codigoHabilidad: number;
    nombreHabilidad: string;
    codigoCatHabilidad: number;
    nombreCatHabilidad: string;
}