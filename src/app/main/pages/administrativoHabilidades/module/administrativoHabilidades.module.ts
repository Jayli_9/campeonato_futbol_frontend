import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RUTA_ADMINISTRATIVOHABILIDAD } from "../routes/administrativoHabilidades-routing.module";
import { PrincipalComponent } from "../components/principal/principal.component";
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { CoreCommonModule } from '@core/common.module';
import { ContentHeaderModule } from 'app/layout/components/content-header/content-header.module';
import { MaterialModule } from 'app/main/shared/material/material.module';
import { HttpClientModule } from '@angular/common/http';
import { ProgramaStoreService } from "../services/programa-store.service"; 

@NgModule({
    declarations: [PrincipalComponent],
    imports: [
      CommonModule,
      RouterModule.forChild(RUTA_ADMINISTRATIVOHABILIDAD),
      FormsModule,
      ReactiveFormsModule,
      MaterialModule,
      ContentHeaderModule,
      HttpClientModule,
      CoreCommonModule
    ],
    providers: [ProgramaStoreService]
  })

  export class administrativoHabilidadesModule {
    
  }