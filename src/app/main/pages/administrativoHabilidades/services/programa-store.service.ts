import { Injectable } from '@angular/core';
import { datoHabilidadesInterface } from "../interfaces/datoHabilidades";

@Injectable()
export class ProgramaStoreService {

    private datoHabilidad: datoHabilidadesInterface[] = [];
    private habCodigo: number;
    private catHabCodigo: number;
    private nombreHabilidad: string;



    get gethabCodigo(){
        return this.habCodigo;
    }

    set sethabCodigo(habCodigo){
        this.habCodigo = habCodigo;
    }

    get getcatHabCodigo(){
        return this.catHabCodigo;
    }

    set setcatHabCodigo(catHabCodigo){
        this.catHabCodigo = catHabCodigo;
    }

    get getnombreHabilidad(){
        return this.nombreHabilidad;
    }

    set setnombreHabilidad(nombreHabilidad){
        this.nombreHabilidad = nombreHabilidad;
    }


    get guardarDatoHabilidad(){
        return this.datoHabilidad;
    }

    set guardarDatoHabilidad(datoHabilidad){
        this.datoHabilidad = datoHabilidad;
    }


}
