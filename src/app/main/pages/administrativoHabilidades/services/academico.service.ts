import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { environment } from 'environments/environment';
import { ResponseGenerico } from '../interfaces/response-generico';
import { habilidadesInterface } from '../interfaces/habilidades';
import { catHabilidadesInterface } from '../interfaces/catHabilidades';

@Injectable({
  providedIn: 'root'
})
export class AcademicoService {

  private readonly URL_REST = environment.url_academico;

constructor(private _http: HttpClient) { }


listarHabilidades(): Promise<ResponseGenerico<habilidadesInterface>>{
  return new Promise((resolve, reject) => {
    this._http.get(`${environment.url_academico}/private/listarHabilidades`).subscribe((response: ResponseGenerico<habilidadesInterface>) => {
      resolve(response);
    }, reject);
  })
}

buscarCatHabilidadPorCodigo(cod): Promise<ResponseGenerico<catHabilidadesInterface>>{
  return new Promise((resolve, reject) => {
    this._http.get(`${environment.url_academico}/private/buscarCatHabilidadesPorCodigo/` + cod).subscribe((response: ResponseGenerico<catHabilidadesInterface>) => {
      resolve(response);
    }, reject);
  })
}

listarCatHabilidades(): Promise<ResponseGenerico<catHabilidadesInterface>>{
  return new Promise((resolve, reject) => {
    this._http.get(`${environment.url_academico}/private/listaCatHabilidades`).subscribe((response: ResponseGenerico<catHabilidadesInterface>) => {
      resolve(response);
    }, reject);
  })
}





eliminarHabilidad(cod): Promise<ResponseGenerico<habilidadesInterface>>{
  return new Promise((resolve, reject) => {
    this._http.delete(`${environment.url_academico}/private/eliminarHabilidad/`+cod).subscribe((response: ResponseGenerico<habilidadesInterface>) => {
      resolve(response);
    }, reject);
  })
}


guardarHabilidad(parametro: any){
  return this._http.post<habilidadesInterface>(`${environment.url_academico}/private/registrarHabilidad`, parametro);
}



actualizarHabilidad(parametro: any){
  return this._http.post<habilidadesInterface>(`${environment.url_academico}/private/actualizarHabilidad`, parametro);
}





}
