import { Routes } from "@angular/router";
import { AuthGuard } from "app/auth/helpers/auth.guards";
import { PrincipalComponent } from "../components/principal/principal.component";

export const RUTA_ADMINISTRATIVOHABILIDAD:Routes=[
    {
        path:'administrativoHabilidad',
        component:PrincipalComponent,
        canActivate:[AuthGuard]
    }
]