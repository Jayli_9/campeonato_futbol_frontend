import { Component, EventEmitter, Input, OnInit, Output, ViewChild  } from '@angular/core';
import { User } from 'app/auth/models';
import { AcademicoService } from "../../services/academico.service";
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import Swal from 'sweetalert2/dist/sweetalert2.js';
import { Router } from '@angular/router';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import {Location} from '@angular/common';

@Component({
  selector: 'app-modal-insercion-habilidades',
  templateUrl: './modal-insercion-habilidades.component.html',
  styleUrls: ['./modal-insercion-habilidades.component.scss']
})
export class ModalInsercionHabilidadesComponent implements OnInit {

  @Input() fromParent;
  @Output() cambiosGuardados = new EventEmitter<void>();

  //lista de CatHabilidades
  listaCatHabilidades = [];
  codigoCategoria;
  nombreHabilidad;

  constructor(
    private readonly adecademicoService: AcademicoService,
    public activeModal: NgbActiveModal
    ) { }

  ngOnInit(){

    this.adecademicoService.listarCatHabilidades().then(data => {
      let lista = data.listado;

      for(let i = 0; i < lista.length; i++){
        if(lista[i].cthEstado == 1){
          this.listaCatHabilidades.push(lista[i]);
        }       
      }

    })

  }


  closeModal(sendData) {
    let existeCurso=false;

    if(sendData==='ok'){

      if(this.codigoCategoria != null && this.nombreHabilidad != null){


        Swal.fire({
          title: 'Confirmar',
          text: '¿Desea Guardar la información?',
          icon: 'question',
          showCancelButton: true,
          confirmButtonColor: '#3085d6',
          cancelButtonColor: '#d33',
          confirmButtonText: 'Guardar',
          cancelButtonText: 'Cancelar'
        }).then((result) => {
          if (result.isConfirmed) {

            let nombre = this.nombreHabilidad.toUpperCase()

            let dato = {
                 "cthCodigo": parseInt(this.codigoCategoria),
                 "habDescipcion": nombre,
                 "habPredeterminado": 0,
                 "habEstado": 1
            }

            this.adecademicoService.guardarHabilidad(dato).subscribe({
              next: (Response)=>{
                Swal.fire({
                  title: "Datos Guardados",
                  text: "La Habilidad ha sido guardada",
                  icon: "success"
                });

                this.activeModal.close(this.fromParent);
                this.cambiosGuardados.emit();
              },
              error: (error) =>{
                Swal.fire({
                  icon: 'error',
                  title: 'Atención!',
                  text: 'Error en guardar los datos'
                })
              }
            })
            
          }
        });


      }else{

        Swal.fire({
          icon: 'error',
          title: 'Atención!',
          text: 'Debe llenar todos los campos'
        })
        
      }

    }else if(sendData==='cancel'){
      this.activeModal.close(this.fromParent);
    }else if(sendData==='dismiss'){
      this.activeModal.close(this.fromParent);
    }
  }

}
