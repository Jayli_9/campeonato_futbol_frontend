import { Component, OnInit, ViewChild  } from '@angular/core';
import { User } from 'app/auth/models';
import { AcademicoService } from "../../services/academico.service";
import { ProgramaStoreService } from "../../services/programa-store.service";
import Swal from 'sweetalert2/dist/sweetalert2.js';
import { Router } from '@angular/router';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import {Location} from '@angular/common';
import { ModalInsercionHabilidadesComponent } from "../modal-insercion-habilidades/modal-insercion-habilidades.component";
import { EditarHabilidadesComponent } from "../editar-habilidades/editar-habilidades.component";


@Component({
  selector: 'app-principal',
  templateUrl: './principal.component.html',
  styleUrls: ['./principal.component.scss']
})
export class PrincipalComponent implements OnInit {

  @ViewChild(ModalInsercionHabilidadesComponent) insercion: ModalInsercionHabilidadesComponent;

  contentHeader: object;

  //datos de las habilidades
  habilidadesTotal = [];
  page = 1;
  pageSize = 4;
  datoEvent;


  //datos del filtro
  selectorAsignatura;

  //datos de categorias
  categoriaSeleccionada;
  listaCategorias = [];

  constructor(
    private readonly adecademicoService: AcademicoService,
    private modalService: NgbModal,
    private readonly programaStore: ProgramaStoreService,
    ) {

   }

  ngOnInit(){
    this.contentHeader = {
      headerTitle: 'Administrar Habilidades',
      actionButton: false,
      breadcrumb: {
          type: '',
          links: [
              {
                name: 'Inicio',
                isLink: true,
                link: '/'
              },
              {
                name: 'administrativoHabilidad',
                isLink: false
              }
          ]
      }
    };

    this.adecademicoService.listarCatHabilidades().then(data => {
      let lista = data.listado;

      for(let i = 0; i < lista.length; i++){
        if(lista[i].cthEstado == 1){
          this.listaCategorias.push(lista[i]);
        }       
      }

    })
    

  }


  seleccionarCategoria(event){
    this.datoEvent = event;
    this.habilidadesTotal = [];

    this.adecademicoService.listarHabilidades().then(data => {
      let listaHabidades = data.listado;
      for(let i = 0; i < listaHabidades.length; i++){
        setTimeout(() => {
          if(listaHabidades[i].cthCodigo == event && listaHabidades[i].habEstado === 1){
            this.adecademicoService.buscarCatHabilidadPorCodigo(listaHabidades[i].cthCodigo).then(data => {
              let catHabilidades = data.objeto;
              let Datos = {
                codigoHabilidad: listaHabidades[i].habCodigo,
                nombreHabilidad: listaHabidades[i].habDescipcion,
                codigoCatHabilidad: catHabilidades.cthCodigo,
                nombreCatHabilidad: catHabilidades.cthDescripcion
              }
              this.habilidadesTotal.push(Datos);
            })
          }
        }, 1000);
      }
    })

  }



  openModalHabilidad(){
    const dialog = this.modalService.open(ModalInsercionHabilidadesComponent,{
      scrollable: true,
      size: 'lg',
      windowClass: 'myCustomModalClass',
      // keyboard: false,
      //backdrop: 'static'
    });

    dialog.result.then((result) => {
      this.habilidadesTotal = [];

    this.adecademicoService.listarHabilidades().then(data => {
      let listaHabidades = data.listado;
      for(let i = 0; i < listaHabidades.length; i++){
        setTimeout(() => {
          if(listaHabidades[i].cthCodigo == this.datoEvent && listaHabidades[i].habEstado === 1){
            this.adecademicoService.buscarCatHabilidadPorCodigo(listaHabidades[i].cthCodigo).then(data => {
              let catHabilidades = data.objeto;
              let Datos = {
                codigoHabilidad: listaHabidades[i].habCodigo,
                nombreHabilidad: listaHabidades[i].habDescipcion,
                codigoCatHabilidad: catHabilidades.cthCodigo,
                nombreCatHabilidad: catHabilidades.cthDescripcion
              }
              this.habilidadesTotal.push(Datos);
            })
          }
        }, 1000);
      }
    })
    })
  }


  abrirEditar(est: any){
    this.programaStore.guardarDatoHabilidad = est;
    this.programaStore.setcatHabCodigo= est.codigoCatHabilidad;
    this.programaStore.sethabCodigo = est.codigoHabilidad;
    this.programaStore.setnombreHabilidad = est.nombreHabilidad;

    const dialog = this.modalService.open(EditarHabilidadesComponent,{
      scrollable: true,
      size: 'lg',
      windowClass: 'myCustomModalClass',
      // keyboard: false,
      //backdrop: 'static'
    });

    dialog.result.then((result) => {
      this.habilidadesTotal = [];

    this.adecademicoService.listarHabilidades().then(data => {
      let listaHabidades = data.listado;
      for(let i = 0; i < listaHabidades.length; i++){
        setTimeout(() => {
          if(listaHabidades[i].cthCodigo == this.datoEvent && listaHabidades[i].habEstado == 1){
            this.adecademicoService.buscarCatHabilidadPorCodigo(listaHabidades[i].cthCodigo).then(data => {
              let catHabilidades = data.objeto;
              let Datos = {
                codigoHabilidad: listaHabidades[i].habCodigo,
                nombreHabilidad: listaHabidades[i].habDescipcion,
                codigoCatHabilidad: catHabilidades.cthCodigo,
                nombreCatHabilidad: catHabilidades.cthDescripcion
              }
              this.habilidadesTotal.push(Datos);
            })
          }
        }, 1000);
      }
    })
    })


  }


  openEliminarModal(est: any) {
    Swal.fire({
      title: '¿Está seguro que desea eliminar esta Habilidad?',
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Si',
      cancelButtonText: 'No'
    }).then((result) => {
      if(result.isConfirmed){

        setTimeout(() => {
          this.adecademicoService.eliminarHabilidad(est.codigoHabilidad).then(data => {

            Swal.fire({
              title: 'Habilidad eliminada',
              text: "Datos guardados",
              icon: 'success',
              confirmButtonColor:  '#3085d6',
              confirmButtonText: 'ok'
            })


    this.habilidadesTotal = [];

    this.adecademicoService.listarHabilidades().then(data => {
      let listaHabidades = data.listado;
      for(let i = 0; i < listaHabidades.length; i++){
        setTimeout(() => {
          if(listaHabidades[i].cthCodigo == this.datoEvent && listaHabidades[i].habEstado == 1){
            this.adecademicoService.buscarCatHabilidadPorCodigo(listaHabidades[i].cthCodigo).then(data => {
              let catHabilidades = data.objeto;
              let Datos = {
                codigoHabilidad: listaHabidades[i].habCodigo,
                nombreHabilidad: listaHabidades[i].habDescipcion,
                codigoCatHabilidad: catHabilidades.cthCodigo,
                nombreCatHabilidad: catHabilidades.cthDescripcion
              }
              this.habilidadesTotal.push(Datos);
            })
          }
        }, 1000);
      }
    })

          })
        }, 1500);

      }
    })
  }



  refreshServicios() {
    this.habilidadesTotal=this.habilidadesTotal
      .map((servicio, i) => ({sereduCodigo: i + 1, ...servicio}))
      .slice((this.page - 1) * this.pageSize, (this.page - 1) * this.pageSize + this.pageSize);
  }
}
