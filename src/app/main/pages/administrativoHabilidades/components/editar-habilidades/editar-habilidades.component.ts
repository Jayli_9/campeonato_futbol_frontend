import { Component, EventEmitter, Input, OnInit, Output, ViewChild  } from '@angular/core';
import { User } from 'app/auth/models';
import { AcademicoService } from "../../services/academico.service";
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import Swal from 'sweetalert2/dist/sweetalert2.js';
import { Router } from '@angular/router';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import {Location} from '@angular/common';
import { ProgramaStoreService } from "../../services/programa-store.service";
import { datoHabilidadesInterface } from "../../interfaces/datoHabilidades";

@Component({
  selector: 'app-editar-habilidades',
  templateUrl: './editar-habilidades.component.html',
  styleUrls: ['./editar-habilidades.component.scss']
})
export class EditarHabilidadesComponent implements OnInit {

  @Input() fromParent;
  @Output() cambiosGuardados = new EventEmitter<void>();

  //lista de CatHabilidades
  objetosHabilidad: datoHabilidadesInterface[] = [];
  listaCatHabilidades = [];
  codigoCategoria;
  nombreHabilidad;
  codigoHabilidad;


  constructor(
    private readonly adecademicoService: AcademicoService,
    public activeModal: NgbActiveModal,
    private readonly programaStore: ProgramaStoreService
  ) { 
    this.objetosHabilidad = programaStore.guardarDatoHabilidad;
    this.nombreHabilidad = programaStore.getnombreHabilidad;
    this.codigoCategoria = programaStore.getcatHabCodigo;
    this.codigoHabilidad = programaStore.gethabCodigo;
  }

  ngOnInit() {

    this.adecademicoService.listarCatHabilidades().then(data => {
      let lista = data.listado;

      for(let i = 0; i < lista.length; i++){
        if(lista[i].cthEstado == 1){
          this.listaCatHabilidades.push(lista[i]);
        }       
      }

    })

  }


  closeModal(sendData) {
    let existeCurso=false;

    if(sendData==='ok'){

      if(this.codigoCategoria != null && this.nombreHabilidad != null){


        Swal.fire({
          title: 'Confirmar',
          text: '¿Desea actualizar la información?',
          icon: 'question',
          showCancelButton: true,
          confirmButtonColor: '#3085d6',
          cancelButtonColor: '#d33',
          confirmButtonText: 'Guardar',
          cancelButtonText: 'Cancelar'
        }).then((result) => {
          if (result.isConfirmed) {

            let nombre = this.nombreHabilidad.toUpperCase();

            let dato = {
                 "habCodigo": this.codigoHabilidad,
                 "cthCodigo": parseInt(this.codigoCategoria),
                 "habDescipcion": nombre,
                 "habEstado": 1,
                 "habPredeterminado": 0
            }

            this.adecademicoService.actualizarHabilidad(dato).subscribe({
              next: (Response)=>{
                Swal.fire({
                  title: "Datos Guardados",
                  text: "La Habilidad ha sido guardada",
                  icon: "success"
                });

                this.activeModal.close(this.fromParent);
                this.cambiosGuardados.emit();
              },
              error: (error) =>{
                Swal.fire({
                  icon: 'error',
                  title: 'Atención!',
                  text: 'Error en guardar los datos'
                })
              }
            })
            
          }
        });


      }

    }else if(sendData==='cancel'){
      this.activeModal.close(this.fromParent);
    }else if(sendData==='dismiss'){
      this.activeModal.close(this.fromParent);
    }
  }

}
