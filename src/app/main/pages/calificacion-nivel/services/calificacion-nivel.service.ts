import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { environment } from "environments/environment";
import { CalificacionNivel } from "../interfaces/calificacion-nivel";
import { RespuestaCalificacionNivelInterfaz } from "../interfaces/respuesta-calificacion-nivel-interfaz";
import { RespuestaCursoExtraordinariaInterfaz } from "../interfaces/respuesta-curso-extraordinaria-interfaz";
import { RespuestaGradoInterfaz } from "../interfaces/respuesta-grado-interfaz";
import { RespuestaInstEstablecimientoInterfaz } from "../interfaces/respuesta-inst-establecimiento-interfaz";
import { RespuestaInstitucionInterfaz } from "../interfaces/respuesta-institucion-interfaz";
import { RespuestaModeloCalificacionInterfaz } from "../interfaces/respuesta-modelo-calificacion-interfaz";
import { RespuestaNivelInterfaz } from "../interfaces/respuesta-nivel-interfaz";
import { RespuestaServicioEducativoInterfaz } from "../interfaces/respuesta-servicio-educativo-interfaz";
import { CursoExtraParametros } from "../interfaces/curso-extra-parametros";
import { GradosParametros } from "../interfaces/grados-parametros";
import { NivelParametros } from "../interfaces/nivel-parametros";

@Injectable({
    providedIn: 'root'
})
export class CalificacionNivelService {

    url_academico=environment.url_academico;
    url_catalogo=environment.url_catalogo;
    url_institucion=environment.url_institucion;
    url_oferta=environment.url_oferta;

    constructor(
        public _http:HttpClient
    ) { }

    //Servicios de Academicos

    guardarCalificacionNivel(calificacionNivel:CalificacionNivel) {
        let url_ws=`${this.url_academico}/private/guardarCalificacionNivel`;
        return this._http.post(url_ws,calificacionNivel);
    }

    activarInactivarCalificacionNivel(codigo:any) {
        let url_ws=`${this.url_academico}/private/eliminarCalificacionNivel/${codigo}`;
        return this._http.delete(url_ws);
    }

    obtenerListaCalificaionNivel(){
        let url_ws=`${this.url_academico}/private/listarTodosCalificacionNivel`;
        return this._http.get<RespuestaCalificacionNivelInterfaz>(url_ws);
    }

    obtenerListaModeloCalificacionPorEstado() {
        let url_ws=`${this.url_academico}/private/listarTodasLosModelosCalificacionesPorEstado`;
        return this._http.get<RespuestaModeloCalificacionInterfaz>(url_ws);
    }

    //Servicios de Catalogos

    obtenerTodosServiciosEducativosPorReanleCodigo(sereduCodigo:any) {
        let url_ws=`${this.url_catalogo}/private/listarServicioEducativosPorListaSereduCodigo/${sereduCodigo}`;
        return this._http.get<RespuestaServicioEducativoInterfaz>(url_ws);
    }

    obtenerTodosGradorPorListaGraCodigoYEstado(gradosParametros:GradosParametros) {
        let url_ws=`${this.url_catalogo}/private/listarGradosPorListaGraCodigoYEstado`;
        return this._http.post<RespuestaGradoInterfaz>(url_ws, gradosParametros);
    }

    obtenerTodosNivelePorNivCodigoYEstado(nivelParametros:NivelParametros) {
        let url_ws=`${this.url_catalogo}/private/listarNivelPorListaNivCodigoYEstado`;
        return this._http.post<RespuestaNivelInterfaz>(url_ws, nivelParametros);
    }

    //Servicios Institucion
    obtenerInstitucionPorAmie(insAmie:any) {
        let url_ws=`${this.url_institucion}/private/buscarInstitucionPorAmie/${insAmie}`;
        return this._http.get<RespuestaInstitucionInterfaz>(url_ws);
    }

    obtenerTodosEstablecimientoPorInstitucion(codigoInstitucion:any) {
        let url_ws=`${this.url_institucion}/private/listarEstablecimientosPorInstitucion/${codigoInstitucion}`;
        return this._http.get<RespuestaInstEstablecimientoInterfaz>(url_ws);
    }

    //Servicios de Ofertas
    obtenerTodosCursosExtraordinarioPorListaEstablecimiento(cursoExtraParametros:CursoExtraParametros) {
        let url_ws=`${this.url_oferta}/private/listarCursosExtraordinariosPorListaEstablecimiento`;
        return this._http.post<RespuestaCursoExtraordinariaInterfaz>(url_ws,cursoExtraParametros);
    }
    
}