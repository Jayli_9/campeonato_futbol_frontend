import { TestBed } from '@angular/core/testing';
import { CalificacionNivelService } from './calificacion-nivel.service';

describe('CalificacionNivelService', () => {
  let service: CalificacionNivelService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(CalificacionNivelService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
  
});