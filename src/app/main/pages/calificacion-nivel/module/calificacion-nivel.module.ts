import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { CoreCommonModule } from '@core/common.module';
import { ContentHeaderModule } from 'app/layout/components/content-header/content-header.module';
import { MaterialModule } from 'app/main/shared/material/material.module';
import { CalificacionNivelComponent } from '../components/calificacion-nivel/calificacion-nivel.component';
import { RUTA_CALIFICACION_NIVEL } from '../routes/calificacion-nivel-routing.module';


@NgModule({
  declarations: [CalificacionNivelComponent],
  imports: [
    CommonModule,
    RouterModule.forChild(RUTA_CALIFICACION_NIVEL),
    FormsModule,
    ReactiveFormsModule,
    MaterialModule,
    ContentHeaderModule,
    CoreCommonModule
  ]
})
export class CalificacionNivelModule {}