import { Component, OnInit, ViewChild } from "@angular/core";
import { UntypedFormBuilder, UntypedFormGroup, Validators } from "@angular/forms";
import { MatPaginator } from "@angular/material/paginator";
import { MatSort } from "@angular/material/sort";
import { MatTableDataSource } from "@angular/material/table";
import { NgbModal } from "@ng-bootstrap/ng-bootstrap";
import { AuthenticationService } from "app/auth/service";
import { NgxSpinnerService } from "ngx-spinner";
import Swal from "sweetalert2";
import { CalificacionNivel } from "../../interfaces/calificacion-nivel";
import { Grado } from "../../interfaces/grado";
import { InstEstablecimiento } from "../../interfaces/instEstablecimiento";
import { Institucion } from "../../interfaces/institucion";
import { ModeloCalificacion } from "../../interfaces/modelo-calificacion";
import { Nivel } from "../../interfaces/nivel";
import { RespuestaCalificacionNivelInterfaz } from "../../interfaces/respuesta-calificacion-nivel-interfaz";
import { RespuestaCursoExtraordinariaInterfaz } from "../../interfaces/respuesta-curso-extraordinaria-interfaz";
import { RespuestaInstEstablecimientoInterfaz } from "../../interfaces/respuesta-inst-establecimiento-interfaz";
import { RespuestaInstitucionInterfaz } from "../../interfaces/respuesta-institucion-interfaz";
import { RespuestaModeloCalificacionInterfaz } from "../../interfaces/respuesta-modelo-calificacion-interfaz";
import { RespuestaNivelInterfaz } from "../../interfaces/respuesta-nivel-interfaz";
import { CalificacionNivelService } from "../../services/calificacion-nivel.service";
import { CursoExtraParametros } from "../../interfaces/curso-extra-parametros";
import { GradosParametros } from "../../interfaces/grados-parametros";
import { NivelParametros } from "../../interfaces/nivel-parametros";
import { CursoExtraordinaria } from "../../interfaces/curso-extraordinaria";
import { RespuestaGradoInterfaz } from "../../interfaces/respuesta-grado-interfaz";

@Component({
    selector: 'app-calificacion-nivel',
    templateUrl: './calificacion-nivel.component.html',
    styleUrls: ['./calificacion-nivel.component.scss']
})
export class CalificacionNivelComponent implements OnInit {

    public contentHeader: object;
    public frmCalificacionNivelCreateEdit: UntypedFormGroup;
    public submitted = false;
    
    listaCalificacionNivel:CalificacionNivel[];
    listaNivel:Nivel[];
    listaModeloCalificacion:ModeloCalificacion[];
    listaEstablecimiento:InstEstablecimiento[];
    listaCursoExtraordinaria:CursoExtraordinaria[];
    listaGrado:Grado[];

    institucion:Institucion;

    columnasCalificacionNivel=['num', 'mod-calificacion', 'nivel', 'estado', 'acciones'];
    dataSource: MatTableDataSource<CalificacionNivel>;

    @ViewChild(MatPaginator) paginator: MatPaginator;
    @ViewChild(MatSort) sort: MatSort;
    constructor(private readonly fb: UntypedFormBuilder,
        private modalService: NgbModal,
        public spinner: NgxSpinnerService,
        private _calificacionNivelService: CalificacionNivelService,
        private _authenticationService: AuthenticationService
        ){
    }

    ngOnInit(): void {
        this.contentHeader = {
            headerTitle: 'Calificación Nivel',
            actionButton: false,
            breadcrumb: {
                type: '',
                links: [
                    {
                      name: 'Inicio',
                      isLink: true,
                      link: '/pages/inicio'
                    },
                    {
                      name: 'Calificación Nivel',
                      isLink: false
                    },
                ]
            }
          };
          this.buscarInstitucionPorAmie();
    }

    buscarInstitucionPorAmie() {
        this.spinner.show();
        const currentUser = this._authenticationService.currentUserValue;
        let codAmie = currentUser.sede.nemonico;
        this._calificacionNivelService.obtenerInstitucionPorAmie(codAmie).subscribe(
            (respuesta: RespuestaInstitucionInterfaz)=>{
                this.institucion = respuesta.objeto;
                this.listarEstablecimientoInstitucion();
                this.listarModeloCalificacionPorEstado();
                this.listarTodaCalificacionNivel();
                this.spinner.hide();
            },
            (error:any)=>{
                this.spinner.hide();
                Swal.fire('Ups! ocurrió un error al cargar la Institución','','error'); 
            }
        );
    }

    listarModeloCalificacionPorEstado() {
        this._calificacionNivelService.obtenerListaModeloCalificacionPorEstado().subscribe(
            (respuesta: RespuestaModeloCalificacionInterfaz)=>{
                this.listaModeloCalificacion=respuesta.listado;
            },
            (error:any)=>{
                this.spinner.hide();
                Swal.fire('Ups! ocurrió un error al cargar los Modelos de Calificación','','error'); 
            }
        );
    }

    listarTodaCalificacionNivel(){
        this._calificacionNivelService.obtenerListaCalificaionNivel().subscribe(
            (respuesta: RespuestaCalificacionNivelInterfaz)=>{
                this.listaCalificacionNivel=respuesta.listado;
                this.obtenerListaNivelPorLitaCalificacionNivel();
                this.dataSource = new MatTableDataSource(this.listaCalificacionNivel);
                this.dataSource.paginator = this.paginator;
                this.dataSource.paginator._intl.itemsPerPageLabel="Asignaturas por página";
                this.dataSource.paginator._intl.nextPageLabel="Siguiente";
                this.dataSource.paginator._intl.previousPageLabel="Anterior";
                this.dataSource.sort = this.sort;
            },
            (error:any)=>{
                this.spinner.hide();
                Swal.fire('Ups! ocurrió un error al cargar los Modelos de Calificación','','error'); 
            }
        );
    } 

    obtenerListaNivelPorLitaCalificacionNivel() {
        let nivCodigo = this.listaCalificacionNivel.map((califNiv) => califNiv.nivCodigo);
        if (nivCodigo.length != 0) {
            let nivCodigoLimpio = Array.from(new Set(nivCodigo));
            let nivelParametros:NivelParametros = {listaNivCodigo:nivCodigo};
            this._calificacionNivelService.obtenerTodosNivelePorNivCodigoYEstado(nivelParametros).subscribe(
                (respuesta: RespuestaNivelInterfaz)=>{
                    this.listaNivel = respuesta.listado;
                    for (let i = 0; i<this.listaCalificacionNivel.length; i++) {
                        let nivelObtenido:Nivel = this.listaNivel.find((nivObt)=>nivObt.nivCodigo == this.listaCalificacionNivel[i].nivCodigo);
                        this.listaCalificacionNivel[i].nivDescripcion = nivelObtenido.nivDescripcion;
                    }
                },
                (error:any)=>{
                    this.spinner.hide();
                    Swal.fire('Ups! ocurrió un error con la lista los niveles','','error'); 
                }
            );
        }
    }

    listarEstablecimientoInstitucion() {
        let codInstitucion = this.institucion.insCodigo;
        this._calificacionNivelService.obtenerTodosEstablecimientoPorInstitucion(codInstitucion).subscribe(
            (respuesta: RespuestaInstEstablecimientoInterfaz)=>{
                this.listaEstablecimiento=respuesta.listado;
                this.listarCursoExtraordinariaPorInstitucion();
            },
            (error:any)=>{
                this.spinner.hide();
                Swal.fire('Ups! ocurrió un error al cargar la Institución','','error'); 
            }
        );
    }

    listarCursoExtraordinariaPorInstitucion() {
        let insestCodigo = this.listaEstablecimiento.map((establecimiento) => establecimiento.insestCodigo);
        let cursoExtraParametros:CursoExtraParametros = {listaCodigoEstablecimiento:insestCodigo};
        this._calificacionNivelService.obtenerTodosCursosExtraordinarioPorListaEstablecimiento(cursoExtraParametros).subscribe(
            (respuesta: RespuestaCursoExtraordinariaInterfaz)=>{
                this.listaCursoExtraordinaria = respuesta.listado;
                this.obtenerGradosConCursoExtraordinaria();
            },
            (error:any)=>{
                this.spinner.hide();    
                Swal.fire('Ups! ocurrió un error con la lista curso extraordinario','','error'); 
            }
        );
    }

    obtenerGradosConCursoExtraordinaria() {
        let graCodigo = this.listaCursoExtraordinaria.map((cursoExtra) => cursoExtra.graCodigo);
        let graCodigoLimpio = Array.from(new Set(graCodigo));
        let gradosParametros:GradosParametros = {listaGraCodigo:graCodigoLimpio};
        this._calificacionNivelService.obtenerTodosGradorPorListaGraCodigoYEstado(gradosParametros).subscribe(
            (respuesta: RespuestaGradoInterfaz)=>{
                this.listaGrado = respuesta.listado;
                this.obtenerNivelePorListaCodigoNivelYEstado();
            },
            (error:any)=>{
                this.spinner.hide();
                Swal.fire('Ups! ocurrió un error al obtener la lista de los grados','','error'); 
            }
        );
    }
    
    obtenerNivelePorListaCodigoNivelYEstado() {
        let nivCodigo = this.listaGrado.map((grado)=>grado.nivCodigo);
        let nivelParametros:NivelParametros = {listaNivCodigo:nivCodigo};
        this._calificacionNivelService.obtenerTodosNivelePorNivCodigoYEstado(nivelParametros).subscribe(
            (respuesta: RespuestaNivelInterfaz)=>{
                this.listaNivel = respuesta.listado;
            },
            (error:any)=>{
                this.spinner.hide();
                Swal.fire('Ups! ocurrió un error con la lista los niveles','','error'); 
            }
        );
    }

    nuevaCalificacionNivel(modalForm) {
        this.submitted=false;
        this.frmCalificacionNivelCreateEdit = this.fb.group({
            calCodigo: ['', [Validators.required]],
            nivCodigo: ['', [Validators.required]]
        });
        this.modalService.open(modalForm);
    }

    editarCalificionNivel(modalForm, calificacionNivel:CalificacionNivel) {

        if (calificacionNivel.calnivEstado==0) {
            Swal.fire(`Debe activar el nivel de calificación.!`,'','success');
            return;
        }

        this.submitted=false;
        this.frmCalificacionNivelCreateEdit = this.fb.group({
            calnivCodigo: calificacionNivel.calnivCodigo,
            calCodigo: calificacionNivel.calCodigo,
            nivCodigo: calificacionNivel.nivCodigo,
            calnivEstado: calificacionNivel.calnivEstado
        });
        this.modalService.open(modalForm);
    }

    guardarCalificacionNivel(modalForm) {
        this.spinner.show();
        this.submitted = true;
        if (this.frmCalificacionNivelCreateEdit.invalid) {
            this.spinner.hide();
            return;
        }
        let nuevaCalificacionNivel:CalificacionNivel ={
            calCodigo: this.frmCalificacionNivelCreateEdit.get('calCodigo').value,
            nivCodigo: this.frmCalificacionNivelCreateEdit.get('nivCodigo').value,
            calnivEstado:1
        };
        this._calificacionNivelService.guardarCalificacionNivel(nuevaCalificacionNivel).subscribe(
            (respuesta:any)=>{
                Swal.fire(`Calificación nivel Creada!`,'','success');
                this.listarTodaCalificacionNivel()
                this.spinner.hide();
            },
            (error:any)=>{
                this.spinner.hide();
                Swal.fire(`No se pudo crear la calificación nivel `,'','error');   
            }
        );
        modalForm.close('Accept click');
    }

    actualizarCalificacionNivel(modalForm) {
        this.spinner.show();
        this.submitted = true;
        if (this.frmCalificacionNivelCreateEdit.invalid) {
            this.spinner.hide();
            return;
        }
        let nuevaCalificacionNivel:CalificacionNivel ={
            calnivCodigo: this.frmCalificacionNivelCreateEdit.get('calnivCodigo').value,
            calCodigo: this.frmCalificacionNivelCreateEdit.get('calCodigo').value,
            nivCodigo: this.frmCalificacionNivelCreateEdit.get('nivCodigo').value,
            calnivEstado:1
        };
        this._calificacionNivelService.guardarCalificacionNivel(nuevaCalificacionNivel).subscribe(
            (respuesta:any)=>{
                Swal.fire(`Calificación nivel Actualizada!`,'','success');
                this.listarTodaCalificacionNivel()
                this.spinner.hide();
            },
            (error:any)=>{
                this.spinner.hide();
                Swal.fire(`No se pudo actualizar la calificación nivel `,'','error');   
            }
        );
        modalForm.close('Accept click');
    }

    activarInactivar(calificacionNivel:CalificacionNivel) {
        this.spinner.show();
        if (calificacionNivel.calnivEstado == 1) {
            this._calificacionNivelService.activarInactivarCalificacionNivel(calificacionNivel.calnivCodigo).subscribe(
                (respuesta:any)=>{
                    Swal.fire(`Calificación nivel Inactiva!`,'','success');
                    this.listarTodaCalificacionNivel()
                    this.spinner.hide();
                },
                (error:any)=>{
                    this.spinner.hide();
                    Swal.fire(`No se pudo inactivar la calificación nivel `,'','error');   
                }
            );
        } else {
            calificacionNivel.calnivEstado = 1;
            this._calificacionNivelService.guardarCalificacionNivel(calificacionNivel).subscribe(
                (respuesta:any)=>{
                    Swal.fire(`Calificación nivel activa!`,'','success');
                    this.listarTodaCalificacionNivel()
                    this.spinner.hide();
                },
                (error:any)=>{
                    this.spinner.hide();
                    Swal.fire(`No se pudo activar la calificación nivel `,'','error');   
                }
            );
        }
    }

    applyFilter(event: Event) {
        const filterValue = (event.target as HTMLInputElement).value;
        this.dataSource.filter = filterValue.trim().toLowerCase();
      
        if (this.dataSource.paginator) {
          this.dataSource.paginator.firstPage();
        }
    }

    get ReactiveFrmCalificacionNivelCreateEdit() {
        return this.frmCalificacionNivelCreateEdit.controls;
    }
    
}