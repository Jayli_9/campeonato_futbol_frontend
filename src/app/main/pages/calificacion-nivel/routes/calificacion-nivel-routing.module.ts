import { Routes } from "@angular/router";
import { AuthGuard } from "app/auth/helpers/auth.guards";
import { CalificacionNivelComponent } from "../components/calificacion-nivel/calificacion-nivel.component";

export const RUTA_CALIFICACION_NIVEL:Routes=[
    {
        path:'calificacion-nivel',
        component:CalificacionNivelComponent
    }
]