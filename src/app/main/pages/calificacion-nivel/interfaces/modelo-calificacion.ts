export interface ModeloCalificacion {
    calCodigo?: number;
    calDescripcion: string;
    calEstado: number;
    calNemonico: string;
    calPorcentaje: number;
}