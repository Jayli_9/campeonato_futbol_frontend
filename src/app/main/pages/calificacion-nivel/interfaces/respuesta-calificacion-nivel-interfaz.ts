import { RespuestaGeneralInterfaz } from "app/main/shared/interfaces/respuesta-general-interfaz";
import { CalificacionNivel } from "./calificacion-nivel";

export interface RespuestaCalificacionNivelInterfaz extends RespuestaGeneralInterfaz {
    listado: CalificacionNivel[];
    objeto: CalificacionNivel;
}