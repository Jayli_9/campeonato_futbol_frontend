export interface CalificacionNivel {
    calnivCodigo?:number;
    calCodigo:number;
    nivCodigo:number;
    calnivEstado:number;
    calDescripcion?:string;
    nivDescripcion?:string;
}