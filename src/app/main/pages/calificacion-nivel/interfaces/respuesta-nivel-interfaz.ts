import { RespuestaGeneralInterfaz } from "app/main/shared/interfaces/respuesta-general-interfaz";
import { Nivel } from "./nivel";

export interface RespuestaNivelInterfaz extends RespuestaGeneralInterfaz {
    listado:Nivel[];
    objeto:Nivel;
}