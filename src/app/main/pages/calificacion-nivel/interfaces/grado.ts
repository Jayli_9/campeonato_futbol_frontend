export interface Grado {
    graCodigo?: number;
    nivCodigo: number;
    graDescripcion: String;
    graEstado: number;
    graNemonico: string;
}