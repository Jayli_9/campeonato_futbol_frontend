import { RespuestaGeneralInterfaz } from "app/main/shared/interfaces/respuesta-general-interfaz";
import { ModeloCalificacion } from "./modelo-calificacion";

export interface RespuestaModeloCalificacionInterfaz extends RespuestaGeneralInterfaz {
    listado: ModeloCalificacion[];
    objeto: ModeloCalificacion;
}