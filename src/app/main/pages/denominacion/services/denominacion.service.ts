import { HttpClient } from "@angular/common/http"
import { Injectable } from "@angular/core"
import { environment } from "environments/environment"
import { Denominacion } from "../interfaces/Denominacion"
import { ResponseGenerico } from "../../reporteEstudiantes/interfaces/response-generico"
import { IPpeParticipacion } from "../../participacionEstudiantil/interfaces/IPpeParticipacion"
import { DenominacionDTO } from "../interfaces/DenominacionDTO"

@Injectable({
  providedIn: "root",
})
export class DenominacionService {
  url_academico = environment.url_academico
  constructor(public _http: HttpClient) {}

  guardarDenominacion(
    denominacion: DenominacionDTO
  ): Promise<ResponseGenerico<Denominacion>> {
    return new Promise((resolve, reject) => {
      this._http
        .post(
          `${environment.url_academico}/private/guardarDenominacion`,
          denominacion
        )
        .subscribe((response: ResponseGenerico<Denominacion>) => {
          resolve(response)
        }, reject)
    })
  }

  listarTodasLasDenominaciones() {
    let url_ws = `${this.url_academico}/private/listarDenominacion`
    return this._http.get<any>(url_ws)
  }

  activarInactivarDenominacion(codigo: number): Promise<any> {
    return new Promise((resolve, reject) => {
      this._http
        .get(
          `${environment.url_academico}/private/cambiarEstadoDenominacionPorId/${codigo}`
        )
        .subscribe((response: any) => {
          resolve(response)
        }, reject)
    })
  }

  listarParticipacion(): Promise<ResponseGenerico<IPpeParticipacion>> {
    return new Promise((resolve, reject) => {
      this._http
        .get(`${environment.url_academico}/private/listarParticipaciones`)
        .subscribe((response: ResponseGenerico<IPpeParticipacion>) => {
          resolve(response)
        }, reject)
    })
  }
}
