import { Routes } from "@angular/router"
import { AuthGuard } from "app/auth/helpers/auth.guards"
import { DenominacionComponent } from "../components/denominacion/denominacion.component"

export const RUTA_DENOMINACION: Routes = [
  {
    path: "creacion-denominacion",
    component: DenominacionComponent,
    canActivate: [AuthGuard],
  },
]
