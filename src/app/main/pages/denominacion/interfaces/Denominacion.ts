import { IPpeParticipacion } from "../../participacionEstudiantil/interfaces/IPpeParticipacion";

export interface Denominacion {
    pdenoCodigo?: number;
    pdenoDescripcion: string;
    pdenoEstado: number;
    ppeParticipacion: IPpeParticipacion;

}