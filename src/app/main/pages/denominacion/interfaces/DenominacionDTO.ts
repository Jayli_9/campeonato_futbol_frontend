export interface DenominacionDTO {
    pdenoCodigo?: number;
    pdenoDescripcion: string;
    pdenoEstado: number;
    ppeParticipacion: number;
}