import { NgModule } from "@angular/core"
import { CommonModule } from "@angular/common"
import { DenominacionComponent } from "../components/denominacion/denominacion.component"
import { RUTA_DENOMINACION } from "../routes/denominacion-routing.module"
import { FormsModule, ReactiveFormsModule } from "@angular/forms"
import { RouterModule } from "@angular/router"
import { CoreCommonModule } from "@core/common.module"
import { ContentHeaderModule } from "app/layout/components/content-header/content-header.module"
import { MaterialModule } from "app/main/shared/material/material.module"
import { MatSelectModule } from "@angular/material/select"
import { MatDialogModule } from "@angular/material/dialog"
import { ModalDenominacionNuevoComponent } from "../components/denominacion/modal-denominacion-nuevo/modal-denominacion-nuevo.component"
import { ModalDenominacionEditarComponent } from "../components/denominacion/modal-denominacion-editar/modal-denominacion-editar.component"

@NgModule({
  declarations: [
    DenominacionComponent,
    ModalDenominacionNuevoComponent,
    ModalDenominacionEditarComponent,
  ],
  imports: [
    CommonModule,
    RouterModule.forChild(RUTA_DENOMINACION),
    FormsModule,
    ReactiveFormsModule,
    MaterialModule,
    ContentHeaderModule,
    CoreCommonModule,
    MatSelectModule,
    MatDialogModule,
  ],
})
export class DenominacionModule {}
