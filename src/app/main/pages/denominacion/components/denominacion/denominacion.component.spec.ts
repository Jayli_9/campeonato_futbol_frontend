import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DenominacionComponent } from './denominacion.component';

describe('DenominacionComponent', () => {
  let component: DenominacionComponent;
  let fixture: ComponentFixture<DenominacionComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DenominacionComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DenominacionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
