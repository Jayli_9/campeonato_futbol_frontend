import { Component, Inject, OnInit } from "@angular/core"
import { UntypedFormControl, UntypedFormGroup, Validators } from "@angular/forms"
import { MAT_DIALOG_DATA, MatDialogRef } from "@angular/material/dialog"
import { MatSelectChange } from "@angular/material/select"
import { IPpeParticipacion } from "app/main/pages/participacionEstudiantil/interfaces/IPpeParticipacion"
import { DenominacionService } from "../../../services/denominacion.service"

@Component({
  selector: "app-modal-denominacion-editar",
  templateUrl: "./modal-denominacion-editar.component.html",
  styleUrls: ["./modal-denominacion-editar.component.scss"],
})
export class ModalDenominacionEditarComponent {
  public denominacionForm: UntypedFormGroup
  public participacion: IPpeParticipacion[]

  constructor(
    public dialogRef: MatDialogRef<ModalDenominacionEditarComponent>,
    private denominacionService: DenominacionService,
    @Inject(MAT_DIALOG_DATA) public data: any
  ) {
    this.initComponents()
    this.listarInformacion()
  }

  async listarInformacion() {
    await this.listarParticipacion()
  }

  async listarParticipacion() {
    try {
      this.participacion = (
        await this.denominacionService.listarParticipacion()
      ).listado
    } catch (error) {
      console.log(error)
    }
  }

  editar() {
    if (this.denominacionForm.status === "VALID") {
      this.dialogRef.close({
        ...this.denominacionForm.value,
        pdenoCodigo: this.data.pdenoCodigo
      })
    }
  }

  initComponents() {
    this.participacion = []
    this.denominacionForm = new UntypedFormGroup({
      descripcion: new UntypedFormControl(this.data.pdenoDescripcion, [Validators.required]),
      participacion: new UntypedFormControl(this.data.ppeParticipacion.pparCodigo, [Validators.required]),
    })
  }

  resetForm(){
    this.denominacionForm.get("descripcion").setValue(null)
    this.denominacionForm.get("participacion").setValue(null)
    this.denominacionForm.get("descripcion").markAsUntouched()
    this.denominacionForm.get("participacion").markAsUntouched()
  }

  getErrorMessage(field: string) {
    return this.denominacionForm.get(field).hasError("required")
      ? "Campo requerido"
      : this.denominacionForm.get(field).hasError("email")
      ? "Correo inválido"
      : this.denominacionForm.get(field).hasError("maxlength")
      ? "longitud incorrecta"
      : this.denominacionForm.get(field).hasError("minlength")
      ? "longitud incorrecta"
      : ""
  }
}
