import { Component, OnInit, ViewChild } from "@angular/core"
import { FormBuilder, UntypedFormGroup, Validators } from "@angular/forms"
import { MatPaginator } from "@angular/material/paginator"
import { MatSort } from "@angular/material/sort"
import { MatTableDataSource } from "@angular/material/table"
import { NgxSpinnerService } from "ngx-spinner"
import { DenominacionService } from "../../services/denominacion.service"
import { Denominacion } from "../../interfaces/Denominacion"
import { IPpeParticipacion } from "app/main/pages/participacionEstudiantil/interfaces/IPpeParticipacion"
import { MatDialog } from "@angular/material/dialog"
import { ModalDenominacionNuevoComponent } from "./modal-denominacion-nuevo/modal-denominacion-nuevo.component"
import { MensajesService } from "app/main/pages/asignacionDocentesOrdinaria/servicios/mensajes.service"
import { DenominacionDTO } from "../../interfaces/DenominacionDTO"
import { ModalDenominacionEditarComponent } from "./modal-denominacion-editar/modal-denominacion-editar.component"

@Component({
  selector: "app-denominacion",
  templateUrl: "./denominacion.component.html",
  styleUrls: ["./denominacion.component.scss"],
})
export class DenominacionComponent implements OnInit {
  public contentHeader: object
  public frmDenominacionCreateEdit: UntypedFormGroup
  public submitted = false
  listaDenominacion: Denominacion[] = null
  listaParticipacion: IPpeParticipacion[] = []
  columnasDenominacion = [
    "num",
    "descripcion",
    "participacion",
    "estado",
    "acciones",
  ]
  dataSource: MatTableDataSource<Denominacion>

  @ViewChild(MatPaginator) paginator: MatPaginator
  @ViewChild(MatSort) sort: MatSort
  constructor(
    public spinner: NgxSpinnerService,
    private _denominacionService: DenominacionService,
    private mensajeService: MensajesService,
    private dialog: MatDialog
  ) {}

  ngOnInit(): void {
    this.contentHeader = {
      headerTitle: "Denominación",
      actionButton: false,
      breadcrumb: {
        type: "",
        links: [
          {
            name: "Inicio",
            isLink: true,
            link: "/pages/inicio",
          },
          {
            name: "Denominacion",
            isLink: false,
          },
        ],
      },
    }
    this.listarTodasLasDenominaciones()
  }

  listarTodasLasDenominaciones() {
    this.spinner.show()
    this._denominacionService.listarTodasLasDenominaciones().subscribe(
      (respuesta: any) => {
        this.listaDenominacion = respuesta.listado
        this.dataSource = new MatTableDataSource(this.listaDenominacion)
        this.dataSource.paginator = this.paginator
        this.dataSource.paginator._intl.itemsPerPageLabel =
          "Agrupaciones por página"
        this.dataSource.paginator._intl.nextPageLabel = "Siguiente"
        this.dataSource.paginator._intl.previousPageLabel = "Anterior"
        this.dataSource.sort = this.sort
        this.spinner.hide()
      },
      (error: any) => {
        this.spinner.hide()
        this.mensajeService.mensajeError(
          "Error",
          "Ups! ocurrió un error al cargar Denominaciones"
        )
      }
    )
  }

  nuevaDenominacion() {
    const dialogRef = this.dialog.open(ModalDenominacionNuevoComponent, {
      width: "600px",
    })
    dialogRef.afterClosed().subscribe((result: any) => {
      if (!!result) {
        this.guardarDenominacion(result)
      }
    })
  }

  editDenominacion(denominacion: Denominacion) {
    const dialogRef = this.dialog.open(ModalDenominacionEditarComponent, {
      width: "600px",
      data: denominacion,
    })
    dialogRef.afterClosed().subscribe((result: any) => {
      if (!!result) {
        this.guardarDenominacion(result)
      }
    })
  }

  async guardarDenominacion(data: any) {
    this.spinner.show()
    let denominacionNueva: DenominacionDTO = {
      pdenoCodigo: data.pdenoCodigo,
      pdenoDescripcion: data.descripcion,
      ppeParticipacion: data.participacion,
      pdenoEstado: 1,
    }
    try {
      await this._denominacionService.guardarDenominacion(denominacionNueva)
      this.mensajeService.mensajeCorrecto("Éxito", "Denominación creada")
      this.listarTodasLasDenominaciones()
    } catch (error) {
      console.log(error)
    }
    this.spinner.hide()
  }

  async activarInactivar(denominacion: Denominacion) {
    try {
      await this._denominacionService.activarInactivarDenominacion(
        denominacion.pdenoCodigo
      )
      this.listarTodasLasDenominaciones()
      this.mensajeService.mensajeCorrecto("Éxito", "Estado cambiado")
    } catch (error) {
      this.mensajeService.mensajeError("Error", "Estado no cambiado")
    }
  }

  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value
    this.dataSource.filter = filterValue.trim().toLowerCase()

    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage()
    }
  }
  // getter for easy access to form fields
  get ReactiveFrmAgrupacionCreateEdit() {
    return this.frmDenominacionCreateEdit.controls
  }
}
