import { RespuestaGeneralInterfaz } from "app/main/shared/interfaces/respuesta-general-interfaz";
import { ModeloEvaluacion } from "./modelo-evaluacion";

export interface RespuestaModeloEvaluacionInterfaz extends RespuestaGeneralInterfaz {
    listado: ModeloEvaluacion[];
    objeto: ModeloEvaluacion;
}