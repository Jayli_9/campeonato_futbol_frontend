export interface ModeloCalificacion {
    calCodigo?: number;
    calDescripcion: string;
    calEstado: number;
    calNemonico: string;
    calPorcentaje: number;
    calMaximo: number;
    calMinimo: number;
    reanleCodigo: number;
    sereduCodigo: number;
    modgenCodigo: number;
    nroParcial:number;
    tipoNemonioCalificacionEnum: string;
    nemonicoCodigo: number;
    regDescripcion?: string;
    anilecAnioInicio?:number;
    anilecAnioFin?:number;
    sereduDescripcion?: string;
}