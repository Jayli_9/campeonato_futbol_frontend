export interface ServicioEducativo {
    sereduCodigo:number;
    sereduDescripcion: string;
    reanleCodigo:number;
    motiedCodigo:number;
    sereduNemonico:string;
    modalidad:string;
    tipoEducacion:string;
}