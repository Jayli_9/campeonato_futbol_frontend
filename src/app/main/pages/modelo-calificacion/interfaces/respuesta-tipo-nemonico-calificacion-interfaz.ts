import { RespuestaGeneralInterfaz } from "app/main/shared/interfaces/respuesta-general-interfaz";
import { TipoNemonioCalificacion } from "./tipo-nemonico-calificacion";

export interface RespuestaTipoNemonioCalificacionInterfaz extends RespuestaGeneralInterfaz {
    listado: TipoNemonioCalificacion[];
    objeto: TipoNemonioCalificacion;
}