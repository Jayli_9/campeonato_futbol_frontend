export interface TipoNemonioCalificacion {
    codigo:number;
    nemonioCalificacionEnum: string;
    nombre: string;
    nemonico: string;
}