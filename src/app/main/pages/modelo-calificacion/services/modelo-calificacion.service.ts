import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { environment } from "environments/environment";
import { ModeloCalificacion } from "../interfaces/modelo-calificacion";
import { RespuestaCursoExtraordinariaInterfaz } from "../interfaces/respuesta-curso-extraordinaria-interfaz";
import { RespuestaInstEstablecimientoInterfaz } from "../interfaces/respuesta-inst-establecimiento-interfaz";
import { RespuestaInstitucionInterfaz } from "../interfaces/respuesta-institucion-interfaz";
import { RespuestaModeloCalificacionInterfaz } from "../interfaces/respuesta-modelo-calificacion-interfaz";
import { RespuestaModeloEvaluacionGeneralInterfaz } from "../interfaces/respuesta-modelo-evaluacion-general-interfaz";
import { RespuestaModeloEvaluacionInterfaz } from "../interfaces/respuesta-modelo-evaluacion-interfaz";
import { RespuestaRegimenAnioLectivoInterfaz } from "../interfaces/respuesta-regimen-anio-lectivo-interfaz";
import { RespuestaServicioEducativoInterfaz } from "../interfaces/respuesta-servicio-educativo-interfaz";
import { RespuestaTipoNemonioCalificacionInterfaz } from "../interfaces/respuesta-tipo-nemonico-calificacion-interfaz";
import { CursoExtraParametros } from "../interfaces/curso-extra-parametros";
import { RegimenAnioLectParametros } from "../interfaces/regimen-anio-lect-parametros";
import { ServicioEducativoParametros } from "../interfaces/servicio-educativo-parametros";
import { ModEvaGenParametros } from "../interfaces/mod-eva-gen-parametros";

@Injectable({
    providedIn: 'root'
})
export class ModeloCalificacionService {

    url_academico=environment.url_academico;
    url_catalogo=environment.url_catalogo;
    url_institucion=environment.url_institucion;
    url_oferta=environment.url_oferta;

    constructor(
        public _http:HttpClient
    ) { }

    //servicos de academicos
    guardarModeloCalificacion(modeloCalificacion: ModeloCalificacion){
        let url_ws=`${this.url_academico}/private/guardarModeloCalificacion`;
        return this._http.post(url_ws,modeloCalificacion);
    }

    obtenerTodosModelosCalificacion() {
        let url_ws=`${this.url_academico}/private/listarTodasLosModelosCalificaciones`;
        return this._http.get<RespuestaModeloCalificacionInterfaz>(url_ws);
    }

    obtenerModelosEvaluacionGeneralPorLitaReganlecYEstado(modEvaGenParametros:ModEvaGenParametros) {
        let url_ws=`${this.url_academico}/private/listarModevaGenPorListaReganlecYEstado`;
        return this._http.post<RespuestaModeloEvaluacionGeneralInterfaz>(url_ws,modEvaGenParametros);
    }

    activarDesactivarModelosCalificacion(codigo:any) {
        let url_ws=`${this.url_academico}/private/eliminarModeloCalificacion/${codigo}`;
        return this._http.delete(url_ws);
    }

    obterListaTipoNemonicoCalifiacion() {
        let url_ws=`${this.url_academico}/private/listaTipoNemonicoCalificacion`;
        return this._http.get<RespuestaTipoNemonioCalificacionInterfaz>(url_ws);
    }

    //Servicio de catalogos
    obtenerTodasRegimenAnioLectivoPorListaEstablecimiento(regimenAnioLectParametros:RegimenAnioLectParametros) {
        let url_ws=`${this.url_catalogo}/private/listaTodasRegionAnioLectivoPorListaEstablecimiento`;
        return this._http.post<RespuestaRegimenAnioLectivoInterfaz>(url_ws, regimenAnioLectParametros);
    }
    
    obtenerTodosServiciosEducativosPorReanleCodigo(reanleCodigo:any) {
        let url_ws=`${this.url_catalogo}/private/listarServicioEducativosPorRegistroAnioLectivo/${reanleCodigo}`;
        return this._http.get<RespuestaServicioEducativoInterfaz>(url_ws);
    }

    obtenerTodosServiciosEducativosPorListaSereduCodigo(servicioEducativoParametros:ServicioEducativoParametros) {
        let url_ws=`${this.url_catalogo}/private/listarServicioEducativosPorListaSereduCodigo`;
        return this._http.post<RespuestaServicioEducativoInterfaz>(url_ws, servicioEducativoParametros);
    }

    //servicios web de ofertas
    obtenerTodosCursosExtraordinarioPorListaEstablecimiento(cursoExtraParametros:CursoExtraParametros) {
        let url_ws=`${this.url_oferta}/private/listarCursosExtraordinariosPorListaEstablecimiento`;
        return this._http.post<RespuestaCursoExtraordinariaInterfaz>(url_ws, cursoExtraParametros);
    }

    //servicios web de instituciones
    obtenerInstitucionPorAmie(insAmie:any) {
        let url_ws=`${this.url_institucion}/private/buscarInstitucionPorAmie/${insAmie}`;
        return this._http.get<RespuestaInstitucionInterfaz>(url_ws);
    }

    obtenerTodosEstablecimientoPorInstitucion(codigoInstitucion:any) {
        let url_ws=`${this.url_institucion}/private/listarEstablecimientosPorInstitucion/${codigoInstitucion}`;
        return this._http.get<RespuestaInstEstablecimientoInterfaz>(url_ws);
    }
}