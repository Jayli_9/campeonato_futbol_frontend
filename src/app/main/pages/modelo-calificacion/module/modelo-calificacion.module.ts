import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { CoreCommonModule } from '@core/common.module';
import { ContentHeaderModule } from 'app/layout/components/content-header/content-header.module';
import { MaterialModule } from 'app/main/shared/material/material.module';
import { ModeloCalificacionComponent } from '../components/modelo-calificacion/modelo-calificacion.component';
import { RUTA_MODELO_CALIFICACION } from '../routes/modelo-calificacion-routing.module';


@NgModule({
  declarations: [ModeloCalificacionComponent],
  imports: [
    CommonModule,
    RouterModule.forChild(RUTA_MODELO_CALIFICACION),
    FormsModule,
    ReactiveFormsModule,
    MaterialModule,
    ContentHeaderModule,
    CoreCommonModule
  ]
})
export class ModeloCalificacionModule { }