import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ModeloCalificacionComponent } from './modelo-calificacion.component';

describe('ModeloCalificacionComponent', () => {
  let component: ModeloCalificacionComponent;
  let fixture: ComponentFixture<ModeloCalificacionComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ModeloCalificacionComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ModeloCalificacionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});