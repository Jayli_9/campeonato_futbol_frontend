import { Component, OnInit, ViewChild } from "@angular/core";
import { UntypedFormBuilder, UntypedFormGroup, Validators } from "@angular/forms";
import { MatPaginator } from "@angular/material/paginator";
import { MatSort } from "@angular/material/sort";
import { MatTableDataSource } from "@angular/material/table";
import { NgbModal } from "@ng-bootstrap/ng-bootstrap";
import { AuthenticationService } from "app/auth/service";
import { NgxSpinnerService } from "ngx-spinner";
import Swal from "sweetalert2";
import { CursoExtraordinaria } from "../../interfaces/curso-extraordinaria";
import { InstEstablecimiento } from "../../interfaces/instEstablecimiento";
import { Institucion } from "../../interfaces/institucion";
import { ModeloCalificacion } from "../../interfaces/modelo-calificacion";
import { ModeloEvaluacionGeneral } from "../../interfaces/modelo-evaluacion-general";
import { RegimenAnioLectivo } from "../../interfaces/regimen-anio-lectivo";
import { RespuestaCursoExtraordinariaInterfaz } from "../../interfaces/respuesta-curso-extraordinaria-interfaz";
import { RespuestaInstEstablecimientoInterfaz } from "../../interfaces/respuesta-inst-establecimiento-interfaz";
import { RespuestaInstitucionInterfaz } from "../../interfaces/respuesta-institucion-interfaz";
import { RespuestaModeloCalificacionInterfaz } from "../../interfaces/respuesta-modelo-calificacion-interfaz";
import { RespuestaModeloEvaluacionGeneralInterfaz } from "../../interfaces/respuesta-modelo-evaluacion-general-interfaz";
import { RespuestaRegimenAnioLectivoInterfaz } from "../../interfaces/respuesta-regimen-anio-lectivo-interfaz";
import { RespuestaServicioEducativoInterfaz } from "../../interfaces/respuesta-servicio-educativo-interfaz";
import { RespuestaTipoNemonioCalificacionInterfaz } from "../../interfaces/respuesta-tipo-nemonico-calificacion-interfaz";
import { ServicioEducativo } from "../../interfaces/servicio-educativo";
import { TipoNemonioCalificacion } from "../../interfaces/tipo-nemonico-calificacion";
import { ModeloCalificacionService } from "../../services/modelo-calificacion.service";
import { CursoExtraParametros } from "../../interfaces/curso-extra-parametros";
import { RegimenAnioLectParametros } from "../../interfaces/regimen-anio-lect-parametros";
import { ServicioEducativoParametros } from "../../interfaces/servicio-educativo-parametros";
import { ModEvaGenParametros } from "../../interfaces/mod-eva-gen-parametros";
import { CatalogoService } from "app/main/pages/asignacionDocentes/servicios/catalogo.service";
import { EvaluacionService } from "app/main/pages/evaluacion-estudiantes/services/evaluacion.service";

@Component({
    selector: 'app-modelo-calificacion',
    templateUrl: './modelo-calificacion.component.html',
    styleUrls: ['./modelo-calificacion.component.scss']
})
export class ModeloCalificacionComponent implements OnInit {

    public contentHeader: object;
    public frmModeloCalificacionCreateEdit: UntypedFormGroup;
    public submitted = false;

    listModeloCalificacion: ModeloCalificacion[];
    listaModeloEvaluacionGeneral: ModeloEvaluacionGeneral[];
    listaRegimenAnioLectivo: RegimenAnioLectivo[];
    listaServiciosEducativos: ServicioEducativo[];
    listaInstEstablecimento: InstEstablecimiento[];
    listaCursoExtraordinaria: CursoExtraordinaria[];
    listaTipoNemonicoCalificacion: TipoNemonioCalificacion[];

    institucion: Institucion;

    columnasCalificacionModeloEvaluacion = ['num', 'mod-eva', 'numero-cal', 'nemonico', 'porcen', 'anio-lect', 'serv-edu', 'estado', 'acciones'];
    dataSource: MatTableDataSource<ModeloCalificacion>;

    @ViewChild(MatPaginator) paginator: MatPaginator;
    @ViewChild(MatSort) sort: MatSort;

    constructor(private readonly fb: UntypedFormBuilder,
        private modalService: NgbModal,
        public spinner: NgxSpinnerService,
        private _modeloCalificacionService: ModeloCalificacionService,
        private _evaluacionService: EvaluacionService,
        private _catalogoService: CatalogoService,
        private _authenticationService: AuthenticationService
    ) {
    }

    ngOnInit(): void {
        this.contentHeader = {
            headerTitle: 'Modelo Calificación',
            actionButton: false,
            breadcrumb: {
                type: '',
                links: [
                    {
                        name: 'Inicio',
                        isLink: true,
                        link: '/pages/inicio'
                    },
                    {
                        name: 'Modelo Calificación',
                        isLink: false
                    },
                ]
            }
        };
        this.listarModeloCalificacion();
    }

    listarModeloCalificacion() {
        this.spinner.show();
        this._modeloCalificacionService.obtenerTodosModelosCalificacion().subscribe(
            (respuesta: RespuestaModeloCalificacionInterfaz) => {
                this.listModeloCalificacion = respuesta.listado;
                this.obtenerInstitucionPorAmie();
                this.obtenerDescripcionRegimenAnioLectivo();
                this.obtenerDescripcionServicioEducativo();
                this.listarTipoNemonicoCalificacion();
                this.dataSource = new MatTableDataSource(this.listModeloCalificacion);
                this.dataSource.paginator = this.paginator;
                this.dataSource.paginator._intl.itemsPerPageLabel = "Asignaturas por página";
                this.dataSource.paginator._intl.nextPageLabel = "Siguiente";
                this.dataSource.paginator._intl.previousPageLabel = "Anterior";
                this.dataSource.sort = this.sort;
                this.spinner.hide();
            },
            (error: any) => {
                this.spinner.hide();
                Swal.fire('Ups! ocurrió un error al cargar los Modelos de Calificaciones', '', 'error');
            }
        );
    }

    obtenerInstitucionPorAmie() {
        //let codInstitucion = this.docente.codInstitucion;
        const currentUser = this._authenticationService.currentUserValue;
        let codAmie = currentUser.sede.nemonico;
        this._modeloCalificacionService.obtenerInstitucionPorAmie(codAmie).subscribe(
            (respuesta: RespuestaInstitucionInterfaz) => {
                this.institucion = respuesta.objeto;
                this.listarEstablecimientoInstitucion();
            },
            (error: any) => {
                Swal.fire('Ups! ocurrió un error al cargar la Institución', '', 'error');
            }
        );
    }

    listarEstablecimientoInstitucion() {
        //let codInstitucion = this.docente.codInstitucion;
        let codInstitucion = this.institucion.insCodigo;
        this._modeloCalificacionService.obtenerTodosEstablecimientoPorInstitucion(codInstitucion).subscribe(
            (respuesta: RespuestaInstEstablecimientoInterfaz) => {
                this.listaInstEstablecimento = respuesta.listado;
                this.listarCursoExtaordinarioPorListaEstablecimiento();
            },
            (error: any) => {
                Swal.fire('Ups! ocurrió un error al cargar los establecimientos de la institución', '', 'error');
            }
        );
    }

    listarCursoExtaordinarioPorListaEstablecimiento() {
        let insestCodigo = this.listaInstEstablecimento.map((estable) => estable.insestCodigo);
        let cursoExtraParametros: CursoExtraParametros = { listaCodigoEstablecimiento: insestCodigo };
        this._modeloCalificacionService.obtenerTodosCursosExtraordinarioPorListaEstablecimiento(cursoExtraParametros).subscribe(
            (respuesta: RespuestaCursoExtraordinariaInterfaz) => {
                this.listaCursoExtraordinaria = respuesta.listado;
                this.listarRegimenAnioLectivo();
            },
            (error: any) => {
                Swal.fire('Ups! ocurrió un error al cargar los establecimiento de la institución', '', 'error');
            }
        );
    }

    listarRegimenAnioLectivo() {
        let reanleCodigo = this.listaCursoExtraordinaria.map((cursoExtrao) => cursoExtrao.reanleCodigo);
        let reanleCodigoLimpio: number[] = Array.from(new Set(reanleCodigo));
        let regimenAnioLectParametros: RegimenAnioLectParametros = { listaReanleCodigo: reanleCodigoLimpio };
        this._modeloCalificacionService.obtenerTodasRegimenAnioLectivoPorListaEstablecimiento(regimenAnioLectParametros).subscribe(
            (respuesta: RespuestaRegimenAnioLectivoInterfaz) => {
                this.listaRegimenAnioLectivo = respuesta.listado;
                this.listarModeloEvaluacionGeneral();
            },
            (error: any) => {
                Swal.fire('Ups! ocurrió un error al cargar los regimen del año lectivo', '', 'error');
            }
        );
    }

    limpiarServicioEducativo(reanleCodigo: number) {
        this.listaServiciosEducativos = null;
        this.frmModeloCalificacionCreateEdit.get('sereduCodigo').reset();
        this.listarServicioEducativosPorReanleCodigo(reanleCodigo);
    }

    listarServicioEducativosPorReanleCodigo(reanleCodigo: number) {

        this._modeloCalificacionService.obtenerTodosServiciosEducativosPorReanleCodigo(reanleCodigo).subscribe(
            (respuesta: RespuestaServicioEducativoInterfaz) => {
                this.listaServiciosEducativos = respuesta.listado;
                for (const servicioEdu of this.listaServiciosEducativos) {
                    ///buscar nombre de servicio educativo descripcion
                    this._catalogoService.getListaModalidadEducacion(servicioEdu.motiedCodigo).then(data => {
                        let modalidadTipoEducacion: any = data['objeto']
                        servicioEdu.sereduDescripcion = modalidadTipoEducacion.motiedDescripcion;
                    })
                    ///fin buscar nombre de servicio educativo descripcion
                }

            },
            (error: any) => {
                Swal.fire('Ups! ocurrió un error al cargar los servicios educativos', '', 'error');
            }
        );
    }

    listarModeloEvaluacionGeneral() {
        let reanleCodigo = this.listaRegimenAnioLectivo.map((regimenAnioLec) => regimenAnioLec.reanleCodigo);
        let modEvaGenParametros: ModEvaGenParametros = { listaReanleCodigo: reanleCodigo };
        this._modeloCalificacionService.obtenerModelosEvaluacionGeneralPorLitaReganlecYEstado(modEvaGenParametros).subscribe(
            (respuesta: RespuestaModeloEvaluacionGeneralInterfaz) => {
                this.listaModeloEvaluacionGeneral = respuesta.listado;
            },
            (error: any) => {
                Swal.fire('Ups! ocurrió un error al cargar los modelos de evaluación', '', 'error');
            }
        );
    }

    obtenerDescripcionRegimenAnioLectivo() {
        let reanleCodigo = this.listModeloCalificacion.map((modCalificacion) => modCalificacion.reanleCodigo);
        let reanleCodigoLimpio: number[] = Array.from(new Set(reanleCodigo));
        let listRegAnioLec: RegimenAnioLectivo[];
        let regimenAnioLectParametros: RegimenAnioLectParametros = { listaReanleCodigo: reanleCodigoLimpio };
        this._modeloCalificacionService.obtenerTodasRegimenAnioLectivoPorListaEstablecimiento(regimenAnioLectParametros).subscribe(
            (respuesta: RespuestaRegimenAnioLectivoInterfaz) => {
                listRegAnioLec = respuesta.listado;
                for (let i = 0; i < this.listModeloCalificacion.length; i++) {
                    let regAnioLec: RegimenAnioLectivo = listRegAnioLec.find((regAL) => regAL.reanleCodigo == this.listModeloCalificacion[i].reanleCodigo);
                    this.listModeloCalificacion[i].regDescripcion = regAnioLec.regDescripcion;
                    this.listModeloCalificacion[i].anilecAnioInicio = regAnioLec.anilecAnioInicio;
                    this.listModeloCalificacion[i].anilecAnioFin = regAnioLec.anilecAnioFin;
                }
            },
            (error: any) => {
                Swal.fire('Ups! ocurrió un error al cargar los regimen del año lectivo', '', 'error');
            }
        );
    }

    obtenerDescripcionServicioEducativo() {
        let sereduCodigo = this.listModeloCalificacion.map((modCalificacion) => modCalificacion.sereduCodigo);
        let sereduCodigoLimpio = Array.from(new Set(sereduCodigo));
        let listSerEduc: ServicioEducativo[];
        let servicioEducativoParametros: ServicioEducativoParametros = { listaSereduCodigo: sereduCodigoLimpio };
        this._modeloCalificacionService.obtenerTodosServiciosEducativosPorListaSereduCodigo(servicioEducativoParametros).subscribe(
            (respuesta: RespuestaServicioEducativoInterfaz) => {
                listSerEduc = respuesta.listado;
                for (let i = 0; i < this.listModeloCalificacion.length; i++) {
                    let serEducativo: ServicioEducativo = listSerEduc.find((serEdu) => serEdu.sereduCodigo == this.listModeloCalificacion[i].sereduCodigo);
                    this.listModeloCalificacion[i].sereduDescripcion = serEducativo.sereduDescripcion;
                }
            }
            ,
            (error: any) => {
                Swal.fire('Ups! ocurrió un error al cargar los servicios educativos', '', 'error');
            }
        );
    }

    listarTipoNemonicoCalificacion() {
        this._modeloCalificacionService.obterListaTipoNemonicoCalifiacion().subscribe(
            (respuesta: RespuestaTipoNemonioCalificacionInterfaz) => {
                this.listaTipoNemonicoCalificacion = respuesta.listado;
            }
            ,
            (error: any) => {
                Swal.fire('Ups! ocurrió un error al cargar los servicios educativos', '', 'error');
            }
        );
    }

    crearNemonico(tipCalificacionEnum: any) {
        let modgenCodigo = this.frmModeloCalificacionCreateEdit.get('modgenCodigo').value;
        if (modgenCodigo == null) {
            this.refrescarValuesPorNemonico();
            Swal.fire('Escoja el modelo de evaluación', '', 'error');
            return;
        }
        let calDescripcion = this.frmModeloCalificacionCreateEdit.get('calDescripcion').value;
        if (calDescripcion == null || calDescripcion.trim == '') {
            this.refrescarValuesPorNemonico();
            Swal.fire('Ingrese en el nombre', '', 'error');
            return;
        }

        let sereduCodigo = this.frmModeloCalificacionCreateEdit.get('sereduCodigo').value;
        if (sereduCodigo == null) {
            this.refrescarValuesPorNemonico();
            Swal.fire('Escoja el servicio educativo', '', 'error');
            return;
        }

        let modevaGen: ModeloEvaluacionGeneral = this.listaModeloEvaluacionGeneral.find((modeloEvaluacion) => modeloEvaluacion.modgenCodigo == modgenCodigo);

        let servicioEducativo: ServicioEducativo = this.listaServiciosEducativos.find((servicioEducativo) => servicioEducativo.sereduCodigo == sereduCodigo);

        let nroParcial = this.frmModeloCalificacionCreateEdit.get('nroParcial').value;

        let tipoCalificacion: TipoNemonioCalificacion = this.listaTipoNemonicoCalificacion.find((tipoCalificacion) => tipoCalificacion.codigo == tipCalificacionEnum);
        let calNemonico = '';
        if (tipoCalificacion.nemonico != 'SUP' && tipoCalificacion.nemonico != 'REM' && tipoCalificacion.nemonico != 'GRA' && tipoCalificacion.nemonico != 'PANUALORD') {
            calNemonico = tipoCalificacion.nemonico + nroParcial + modevaGen.moevaNemonico + modevaGen.modgenCodigo + servicioEducativo.sereduNemonico;
        } else {
            calNemonico = tipoCalificacion.nemonico;
        }

        this.frmModeloCalificacionCreateEdit.get('calNemonico').setValue(calNemonico);
    }

    refrescarValuesPorNemonico() {
        let nombre: string = this.frmModeloCalificacionCreateEdit.get('calDescripcion').value;
        nombre = nombre.toUpperCase();
        this.frmModeloCalificacionCreateEdit.get('calDescripcion').setValue(nombre);
        this.frmModeloCalificacionCreateEdit.get('calNemonico').reset();
        this.frmModeloCalificacionCreateEdit.get('calPorcentaje').reset();
        this.frmModeloCalificacionCreateEdit.get('nemonicoCodigo').reset();
        this.frmModeloCalificacionCreateEdit.get('nroParcial').reset();
    }

    refrescarComponentes() {
        this.frmModeloCalificacionCreateEdit.get('calDescripcion').reset();
        this.frmModeloCalificacionCreateEdit.get('calNemonico').reset();
        this.frmModeloCalificacionCreateEdit.get('calPorcentaje').reset();
        this.frmModeloCalificacionCreateEdit.get('reanleCodigo').reset();
        this.frmModeloCalificacionCreateEdit.get('sereduCodigo').reset();
        this.frmModeloCalificacionCreateEdit.get('nemonicoCodigo').reset();
        this.frmModeloCalificacionCreateEdit.get('nroParcial').reset();
    }

    nuevoModeloCalificacion(modalForm) {
        this.submitted = false;
        this.frmModeloCalificacionCreateEdit = this.fb.group({
            calDescripcion: ['', [Validators.required]],
            calNemonico: ['', [Validators.required]],
            calPorcentaje: ['', [Validators.required]],
            reanleCodigo: [null, [Validators.required]],
            sereduCodigo: [null, [Validators.required]],
            modgenCodigo: [null, [Validators.required]],
            nroParcial: [null, [Validators.required]],
            nemonicoCodigo: [null, [Validators.required]],
            calMinimo: ['', [Validators.required]],
            calMaximo: ['', [Validators.required]]
        });
        this.modalService.open(modalForm);
    }

    editarModeloCalificacion(modalForm, modeloCalificacion: ModeloCalificacion) {
        if (modeloCalificacion.calEstado == 0) {
            Swal.fire(`Debe activar el modelo de calificación.!`, '', 'success');
            return;
        }

        this.submitted = false;
        this.frmModeloCalificacionCreateEdit = this.fb.group({
            calCodigo: [modeloCalificacion.calCodigo],
            calDescripcion: [modeloCalificacion.calDescripcion],
            calNemonico: [modeloCalificacion.calNemonico],
            calPorcentaje: [modeloCalificacion.calPorcentaje],
            reanleCodigo: [modeloCalificacion.reanleCodigo],
            sereduCodigo: [modeloCalificacion.sereduCodigo],
            modgenCodigo: [modeloCalificacion.modgenCodigo],
            nroParcial: [modeloCalificacion.nroParcial],
            nemonicoCodigo: [modeloCalificacion.nemonicoCodigo],
            calMinimo: [modeloCalificacion.calMinimo],
            calMaximo: [modeloCalificacion.calMaximo]
        });
        this.listarServicioEducativosPorReanleCodigo(modeloCalificacion.reanleCodigo);
        this.modalService.open(modalForm);
    }

    guardarModeloCalificacion(modalForm) {
        this.spinner.show();
        this.submitted = true;

        if (this.frmModeloCalificacionCreateEdit.invalid) {
            this.spinner.hide();
            return;
        }

        let codigoNemonio = this.frmModeloCalificacionCreateEdit.get('nemonicoCodigo').value;
        let tipoCalifiacionEnum: TipoNemonioCalificacion = this.listaTipoNemonicoCalificacion.find((tipoCalifiacion) => tipoCalifiacion.codigo == codigoNemonio);

        let modeloCalificacion: ModeloCalificacion = {
            calDescripcion: this.frmModeloCalificacionCreateEdit.get('calDescripcion').value,
            calEstado: 1,
            calNemonico: this.frmModeloCalificacionCreateEdit.get('calNemonico').value,
            calPorcentaje: this.frmModeloCalificacionCreateEdit.get('calPorcentaje').value,
            reanleCodigo: this.frmModeloCalificacionCreateEdit.get('reanleCodigo').value,
            sereduCodigo: this.frmModeloCalificacionCreateEdit.get('sereduCodigo').value,
            modgenCodigo: this.frmModeloCalificacionCreateEdit.get('modgenCodigo').value,
            nroParcial: this.frmModeloCalificacionCreateEdit.get('nroParcial').value,
            tipoNemonioCalificacionEnum: tipoCalifiacionEnum.nemonioCalificacionEnum,
            nemonicoCodigo: this.frmModeloCalificacionCreateEdit.get('nemonicoCodigo').value,
            calMaximo: this.frmModeloCalificacionCreateEdit.get('calMaximo').value,
            calMinimo: this.frmModeloCalificacionCreateEdit.get('calMinimo').value,
        }

        this._modeloCalificacionService.guardarModeloCalificacion(modeloCalificacion).subscribe(
            (respuesta: any) => {
                Swal.fire(`Modelo Calificación Creada!`, '', 'success');
                this.spinner.hide();
                this.listarModeloCalificacion();
            },
            (error: any) => {
                Swal.fire(`No se pudo crear el Modelo Calificación `, '', 'error');
                this.spinner.hide();
                this.listarModeloCalificacion();
            }
        );
        modalForm.close('Accept click');
    }

    actualizarModeloCalificacion(modalForm) {
        this.spinner.show();
        this.submitted = true;

        if (this.frmModeloCalificacionCreateEdit.invalid) {
            this.spinner.hide();
            return;
        }

        let codigoNemonio = this.frmModeloCalificacionCreateEdit.get('nemonicoCodigo').value;
        let nemonicoCalifiacionEnum: TipoNemonioCalificacion = this.listaTipoNemonicoCalificacion.find((tipoCalifiacion) => tipoCalifiacion.codigo == codigoNemonio);

        let modeloCalificacion: ModeloCalificacion = {
            calCodigo: this.frmModeloCalificacionCreateEdit.get('calCodigo').value,
            calDescripcion: this.frmModeloCalificacionCreateEdit.get('calDescripcion').value,
            calEstado: 1,
            calNemonico: this.frmModeloCalificacionCreateEdit.get('calNemonico').value,
            calPorcentaje: this.frmModeloCalificacionCreateEdit.get('calPorcentaje').value,
            reanleCodigo: this.frmModeloCalificacionCreateEdit.get('reanleCodigo').value,
            sereduCodigo: this.frmModeloCalificacionCreateEdit.get('sereduCodigo').value,
            modgenCodigo: this.frmModeloCalificacionCreateEdit.get('modgenCodigo').value,
            nroParcial: this.frmModeloCalificacionCreateEdit.get('nroParcial').value,
            tipoNemonioCalificacionEnum: nemonicoCalifiacionEnum.nemonioCalificacionEnum,
            nemonicoCodigo: this.frmModeloCalificacionCreateEdit.get('nemonicoCodigo').value,
            calMaximo: this.frmModeloCalificacionCreateEdit.get('calMaximo').value,
            calMinimo: this.frmModeloCalificacionCreateEdit.get('calMinimo').value,
        }

        this._modeloCalificacionService.guardarModeloCalificacion(modeloCalificacion).subscribe(
            (respuesta: any) => {
                Swal.fire(`Modelo Calificación Actualizada!`, '', 'success');
                this.spinner.hide();
                this.listarModeloCalificacion();
            },
            (error: any) => {
                Swal.fire(`No se pudo Actualizar el Modelo Calificación `, '', 'error');
                this.spinner.hide();
                this.listarModeloCalificacion();
            }
        );
        modalForm.close('Accept click');
    }

    activarInactivar(modeloCalificacion: ModeloCalificacion) {
        this.spinner.show();
        if (modeloCalificacion.calEstado == 1) {
            this._modeloCalificacionService.activarDesactivarModelosCalificacion(modeloCalificacion.calCodigo).subscribe(
                (respuesta: any) => {
                    this.spinner.hide();
                    Swal.fire('Modelo Calificación Inactivada!', '', 'success');
                    this.listarModeloCalificacion();
                },
                (error: any) => {
                    this.spinner.hide();
                    Swal.fire('Ups! ocurrió un error', '', 'error');
                    this.listarModeloCalificacion();
                }
            );
        } else {
            modeloCalificacion.calEstado = 1;
            this._modeloCalificacionService.guardarModeloCalificacion(modeloCalificacion).subscribe(
                (respuesta: any) => {
                    this.spinner.hide();
                    Swal.fire('Modelo Calificación activada!', '', 'success');
                    this.listarModeloCalificacion();
                },
                (error: any) => {
                    this.spinner.hide();
                    Swal.fire('Ups! ocurrió un error', '', 'error');
                    this.listarModeloCalificacion();
                }
            );
        }
    }

    applyFilter(event: Event) {
        const filterValue = (event.target as HTMLInputElement).value;
        this.dataSource.filter = filterValue.trim().toLowerCase();

        if (this.dataSource.paginator) {
            this.dataSource.paginator.firstPage();
        }
    }

    get ReactiveFrmModeloCalificacionCreateEdit() {
        return this.frmModeloCalificacionCreateEdit.controls;
    }

}