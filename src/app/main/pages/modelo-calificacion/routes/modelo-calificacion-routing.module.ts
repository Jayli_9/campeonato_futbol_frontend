import { Routes } from "@angular/router";
import { AuthGuard } from "app/auth/helpers/auth.guards";
import { ModeloCalificacionComponent } from "../components/modelo-calificacion/modelo-calificacion.component";


export const RUTA_MODELO_CALIFICACION:Routes=[
    {
        path:'modelo-calificacion',
        component:ModeloCalificacionComponent
    }
]