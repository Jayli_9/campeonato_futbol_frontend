import { Routes } from "@angular/router";
import { AuthGuard } from "app/auth/helpers/auth.guards";
import { PrincipalComponent } from "../components/principal/principal.component";

export const RUTA_ADMINISTRATIVOCATHABILIDAD:Routes=[
    {
        path:'administrativoCatHabilidad',
        component:PrincipalComponent,
        canActivate:[AuthGuard]
    }
]