import { Injectable } from '@angular/core';
import { catHabilidadesInterface } from "../interfaces/catHabilidades";

@Injectable({
  providedIn: 'root'
})
export class ProgramaStoreService {

  private datoCatHabilidades: catHabilidadesInterface[] = [];

  private nombreCategoria: string;

  private codigoCategoria: number;

  get getcodigoCategoria(){
    return this.codigoCategoria;
  }

  set setcodigoCategoria(codigoCategoria){
    this.codigoCategoria = codigoCategoria;
  }

  get getnombreCategoria(){
    return this.nombreCategoria;
  }

  set setnombreCategoria(nombreCategoria){
    this.nombreCategoria = nombreCategoria;
  }

  get getdatoCatHabilidades(){
    return this.datoCatHabilidades;
  }

  set setdatoCatHabilidades(datoCatHabilidades){
    this.datoCatHabilidades = datoCatHabilidades;
  }

}
