import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { environment } from 'environments/environment';
import { ResponseGenerico } from '../interfaces/response-generico';
import { catHabilidadesInterface } from "../interfaces/catHabilidades";

@Injectable({
  providedIn: 'root'
})
export class AcademicoService {

  private readonly URL_REST = environment.url_academico;

constructor(private _http: HttpClient) { }

listarCatHabilidades(): Promise<ResponseGenerico<catHabilidadesInterface>>{
  return new Promise((resolve, reject) => {
    this._http.get(`${environment.url_academico}/private/listaCatHabilidades`).subscribe((response: ResponseGenerico<catHabilidadesInterface>) => {
      resolve(response);
    }, reject);
  })
}


guardarCatHabilidad(parametro: any){
  return this._http.post<catHabilidadesInterface>(`${environment.url_academico}/private/registrarCatHabilidad`, parametro);
}

editarCatHabilidad(parametro: any){
  return this._http.post<catHabilidadesInterface>(`${environment.url_academico}/private/actualizarCatHabilidad`, parametro);
}



eliminarCatHabilidad(cod): Promise<ResponseGenerico<catHabilidadesInterface>>{
  return new Promise((resolve, reject) => {
    this._http.delete(`${environment.url_academico}/private/eliminarCatHabilidad/`+cod).subscribe((response: ResponseGenerico<catHabilidadesInterface>) => {
      resolve(response);
    }, reject);
  })
}





}
