import { Component, OnInit } from '@angular/core';
import { AcademicoService } from "../../services/academico.service";
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { ModalCatHabilidadesComponent } from "../modal-cat-habilidades/modal-cat-habilidades.component";
import { EditarCatHabilidadesComponent } from "../editar-cat-habilidades/editar-cat-habilidades.component";
import { ProgramaStoreService } from "../../services/programa-store.service";
import Swal from 'sweetalert2';

@Component({
  selector: 'app-principal',
  templateUrl: './principal.component.html',
  styleUrls: ['./principal.component.scss']
})
export class PrincipalComponent implements OnInit {

  contentHeader: object;

  page = 1;
  pageSize = 4;
  datoEvent;

  //datos de categoria
  listaCategoria = [];

  constructor(
    private readonly academicoServicio: AcademicoService,
    private modalService: NgbModal,
    private readonly programStore: ProgramaStoreService
  ) { }

  ngOnInit() {
    this.contentHeader = {
      headerTitle: 'Administrar Catalogo Habilidades',
      actionButton: false,
      breadcrumb: {
          type: '',
          links: [
              {
                name: 'Inicio',
                isLink: true,
                link: '/'
              },
              {
                name: 'administrativoCatHabilidad',
                isLink: false
              }
          ]
      }
    };


    this.academicoServicio.listarCatHabilidades().then(data => {
      let datos = data.listado;

      for(let i = 0; i < datos.length; i++){

        if(datos[i].cthEstado == 1){

          this.listaCategoria.push(datos[i])

        }

      }

    })


  }


  openModalHabilidad(){
    const dialog = this.modalService.open(ModalCatHabilidadesComponent,{
      scrollable: true,
      size: 'lg',
      windowClass: 'myCustomModalClass',
      // keyboard: false,
      //backdrop: 'static'
    });

    dialog.result.then((result) => {
      setTimeout(() => {
        this.listaCategoria = [];
        this.academicoServicio.listarCatHabilidades().then(data => {
          let datos = data.listado;
    
          for(let i = 0; i < datos.length; i++){
    
            if(datos[i].cthEstado == 1){
    
              this.listaCategoria.push(datos[i])
    
            }
    
          }
    
        })
      }, 1000);

      
    })
  }


  abrirEditar(est: any){
    this.programStore.setnombreCategoria=est.cthDescripcion;
    this.programStore.setcodigoCategoria=est.cthCodigo;

    const dialog = this.modalService.open(EditarCatHabilidadesComponent, {
      scrollable: true,
      size: 'lg',
      windowClass: 'myCustomModalClass',
      // keyboard: false,
      //backdrop: 'static'
    });

    dialog.result.then((result) => {
      setTimeout(() => {
        this.listaCategoria = [];
        this.academicoServicio.listarCatHabilidades().then(data => {
          let datos = data.listado;
    
          for(let i = 0; i < datos.length; i++){
    
            if(datos[i].cthEstado == 1){
    
              this.listaCategoria.push(datos[i])
    
            }
    
          }
    
        })
      }, 1000);

      
    })

  }

  
  openEliminarModal(est: any){

    
    Swal.fire({
      title: '¿Está seguro que desea eliminar esta Categoria?',
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Si',
      cancelButtonText: 'No'
    }).then((result) => {
      if(result.isConfirmed){
        this.academicoServicio.eliminarCatHabilidad(est.cthCodigo).then(data => {

          Swal.fire({
            title: 'Categoria eliminada',
            text: "Datos guardados",
            icon: 'success',
            confirmButtonColor:  '#3085d6',
            confirmButtonText: 'ok'
          })

          setTimeout(() => {
            this.listaCategoria = [];
            this.academicoServicio.listarCatHabilidades().then(data => {
              let datos = data.listado;
        
              for(let i = 0; i < datos.length; i++){
        
                if(datos[i].cthEstado == 1){
        
                  this.listaCategoria.push(datos[i])
        
                }
        
              }
        
            })
          }, 1000);

        })
      }
    })
  }

}
