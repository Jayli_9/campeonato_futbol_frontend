import { Component, EventEmitter, Input, OnInit, Output, ViewChild  } from '@angular/core';
import { User } from 'app/auth/models';
import { AcademicoService } from "../../services/academico.service";
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import Swal from 'sweetalert2/dist/sweetalert2.js';
import { Router } from '@angular/router';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import {Location} from '@angular/common';
import { ProgramaStoreService } from "../../services/programa-store.service";

@Component({
  selector: 'app-editar-cat-habilidades',
  templateUrl: './editar-cat-habilidades.component.html',
  styleUrls: ['./editar-cat-habilidades.component.scss']
})
export class EditarCatHabilidadesComponent implements OnInit {

  @Input() fromParent;
  @Output() cambiosGuardados = new EventEmitter<void>();

  //datos del formulario
  nombreCategoria;
  codigoCategoria;


  constructor(
    private readonly adecademicoService: AcademicoService,
    public activeModal: NgbActiveModal,
    private readonly programStore: ProgramaStoreService
  ) {
    this.nombreCategoria = this.programStore.getnombreCategoria;
    this.codigoCategoria = this.programStore.getcodigoCategoria;
   }

  ngOnInit(){
  }

  closeModal(sendData) {
    let existeCurso=false;

    if(sendData==='ok'){

      if(this.nombreCategoria != null){

        Swal.fire({
          title: 'Confirmar',
          text: '¿Desea actualizar la información?',
          icon: 'question',
          showCancelButton: true,
          confirmButtonColor: '#3085d6',
          cancelButtonColor: '#d33',
          confirmButtonText: 'Guardar',
          cancelButtonText: 'Cancelar'
        }).then((result) => {
          if (result.isConfirmed) {

            let nombre = this.nombreCategoria.toUpperCase()

            let dato = {
              "cthCodigo": this.codigoCategoria,
              "cthDescripcion": nombre,
               "cthEstado": 1
            }

            this.adecademicoService.editarCatHabilidad(dato).subscribe({
              next: (Response)=>{
                Swal.fire({
                  title: "Datos Actualizados",
                  text: "La Categoria Habilidad ha sido guardada",
                  icon: "success"
                });

                this.activeModal.close(this.fromParent);
                this.cambiosGuardados.emit();
              },
              error: (error) =>{
                Swal.fire({
                  icon: 'error',
                  title: 'Atención!',
                  text: 'Error en guardar los datos'
                })
              }
            })
            
          }
        });

        }else{
          Swal.fire({
            icon: 'error',
            title: 'Atención!',
            text: 'Debe llenar todos los campos'
          })
        }

    }else if(sendData==='cancel'){
      this.activeModal.close(this.fromParent);
    }else if(sendData==='dismiss'){
      this.activeModal.close(this.fromParent);
    }
  }

}
