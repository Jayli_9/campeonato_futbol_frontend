import { Routes } from "@angular/router";
import { AuthGuard } from "app/auth/helpers/auth.guards";
import { ModeloEvaluacionComponent } from "../components/modelo-evaluacion/modelo-evaluacion.component";

export const RUTA_MODELO_EVALUACION:Routes=[
    {
        path:'modelo-evaluacion',
        component:ModeloEvaluacionComponent
    }
]