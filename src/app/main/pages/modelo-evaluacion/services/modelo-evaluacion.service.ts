import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { environment } from "environments/environment";
import { ModeloEvaluacion } from "../interfaces/modelo-evaluacion";
import { RespuestaModeloEvaluacionInterfaz } from "../interfaces/respuesta-modelo-evaluacion-interfaz";

@Injectable({
    providedIn: 'root'
})
export class ModeloEvaluacionService {

    url_academico=environment.url_academico;

    constructor(
        public _http:HttpClient
    ) { }

    guardarModeloEvaluacion(modeloEvaluacion: ModeloEvaluacion){
        let url_ws=`${this.url_academico}/private/guardarModeloEvaluacion`;
        return this._http.post(url_ws,modeloEvaluacion);
    }

    obtenerTodosModelosEvaluacion() {
        let url_ws=`${this.url_academico}/private/listarTodasLosModelosEvaluacion`;
        return this._http.get<RespuestaModeloEvaluacionInterfaz>(url_ws);
    }

    activarDesactivarModelosEvaluacion(codigo:any) {
        let url_ws=`${this.url_academico}/private/eliminarModelosEvaluacion/${codigo}`;
        return this._http.delete(url_ws);
    }
}