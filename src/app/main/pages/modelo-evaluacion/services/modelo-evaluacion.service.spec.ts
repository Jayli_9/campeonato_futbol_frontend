import { TestBed } from '@angular/core/testing';
import { ModeloEvaluacionService } from './modelo-evaluacion.service';

describe('ModeloEvaluacionService', () => {
  let service: ModeloEvaluacionService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(ModeloEvaluacionService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});