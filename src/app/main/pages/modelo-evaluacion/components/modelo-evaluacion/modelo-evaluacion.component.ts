import { Component, OnInit, ViewChild } from "@angular/core";
import { UntypedFormBuilder, UntypedFormGroup, Validators } from "@angular/forms";
import { MatPaginator } from "@angular/material/paginator";
import { MatSort } from "@angular/material/sort";
import { MatTableDataSource } from "@angular/material/table";
import { NgbModal } from "@ng-bootstrap/ng-bootstrap";
import { NgxSpinnerService } from "ngx-spinner";
import Swal from "sweetalert2";
import { ModeloEvaluacion } from "../../interfaces/modelo-evaluacion";
import { RespuestaModeloEvaluacionInterfaz } from "../../interfaces/respuesta-modelo-evaluacion-interfaz";
import { ModeloEvaluacionService } from "../../services/modelo-evaluacion.service";

@Component({
    selector: 'app-modelo-evaluacion',
    templateUrl: './modelo-evaluacion.component.html',
    styleUrls: ['./modelo-evaluacion.component.scss']
})
export class ModeloEvaluacionComponent implements OnInit {

    public contentHeader: object;
    public frmModeloEvaluacionCreateEdit: UntypedFormGroup;
    public submitted = false;

    listaModeloEvaluacion:ModeloEvaluacion[];

    columnasModeloEvaluacion=['num', 'descripcion', 'nemonico', 'estado', 'acciones'];
    dataSource: MatTableDataSource<ModeloEvaluacion>;

    @ViewChild(MatPaginator) paginator: MatPaginator;
    @ViewChild(MatSort) sort: MatSort;
    constructor(private readonly fb: UntypedFormBuilder,
        private modalService: NgbModal,
        public spinner: NgxSpinnerService,
        private _modeloEvaluacionService: ModeloEvaluacionService
        ){
    }

    ngOnInit(): void {
        this.contentHeader = {
            headerTitle: 'Modelo Evaluación',
            actionButton: false,
            breadcrumb: {
                type: '',
                links: [
                    {
                      name: 'Inicio',
                      isLink: true,
                      link: '/pages/inicio'
                    },
                    {
                      name: 'Modelo Evaluación',
                      isLink: false
                    },
                ]
            }
          };
          this.listarModeloEvaluacion();
    }

    listarModeloEvaluacion() {
        this.spinner.show();
        this.listaModeloEvaluacion = null;
        this._modeloEvaluacionService.obtenerTodosModelosEvaluacion().subscribe(
            (respuesta:RespuestaModeloEvaluacionInterfaz)=>{
                this.listaModeloEvaluacion = respuesta.listado;
                this.dataSource = new MatTableDataSource(this.listaModeloEvaluacion);
                this.dataSource.paginator = this.paginator;
                this.dataSource.paginator._intl.itemsPerPageLabel="Asignaturas por página";
                this.dataSource.paginator._intl.nextPageLabel="Siguiente";
                this.dataSource.paginator._intl.previousPageLabel="Anterior";
                this.dataSource.sort = this.sort;
                this.spinner.hide();
            },
            (error:any)=>{
                this.spinner.hide();
                Swal.fire('Ups! ocurrió un error','','error'); 
            }
        );
    }

    crearNemonico() {
        let moevDescripcion = this.frmModeloEvaluacionCreateEdit.get('moevDescripcion').value;
        moevDescripcion = moevDescripcion.toUpperCase();
        this.frmModeloEvaluacionCreateEdit.get('moevDescripcion').setValue(moevDescripcion);
        this.frmModeloEvaluacionCreateEdit.get('moevNemonico').setValue(moevDescripcion.substr(0,3));
    }

    nuevoModeloEvaluacion(modalForm){
        this.submitted=false;
        this.frmModeloEvaluacionCreateEdit = this.fb.group({
            moevDescripcion:['', [Validators.required]],
            moevNemonico:['', [Validators.required]]
        });
        this.modalService.open(modalForm);
    }

    editarModeloEvaluacion(modalForm, modeloEvaluacion:ModeloEvaluacion) {
        if (modeloEvaluacion.moevEstado==0) {
            Swal.fire(`Debe activar el modelo de evaluación!`,'','success');
            return;
        }

        this.submitted=false;
        this.frmModeloEvaluacionCreateEdit = this.fb.group({
            moevCodigo:modeloEvaluacion.moevCodigo,
            moevDescripcion:[modeloEvaluacion.moevDescripcion,[Validators.required]],
            moevEstado:[modeloEvaluacion.moevEstado],
            moevNemonico:[modeloEvaluacion.moevNemonico,[Validators.required]]
        });
        this.modalService.open(modalForm);
    }

    guardarModeloEvaluacion(modalForm) {
        this.spinner.show();
        this.submitted = true;

        if (this.frmModeloEvaluacionCreateEdit.invalid) {
            this.spinner.hide();
            return;
        }

        let nuevoModeloEvaluacion:ModeloEvaluacion = {
            moevDescripcion:this.frmModeloEvaluacionCreateEdit.get('moevDescripcion').value,
            moevEstado:1,
            moevNemonico:this.frmModeloEvaluacionCreateEdit.get('moevNemonico').value,
        };

        this._modeloEvaluacionService.guardarModeloEvaluacion(nuevoModeloEvaluacion).subscribe(
            (respuesta:any)=>{
                Swal.fire(`Modelo Evaluación Creada!`,'','success');
                this.spinner.hide();
                this.listarModeloEvaluacion();
            },
            (error:any)=>{
                Swal.fire(`No se pudo crear el Modelo de Evaluación `,'','error');
                this.spinner.hide();
                this.listarModeloEvaluacion();
            }
        );
        modalForm.close('Accept click');
    }

    actualizarModeloEvaluacion(modalForm) {
        this.spinner.show();
        this.submitted = true;

        if (this.frmModeloEvaluacionCreateEdit.invalid) {
            this.spinner.hide();
            return;
        }

        let modeloEvaluacionNuevo:ModeloEvaluacion = {
            moevCodigo:this.frmModeloEvaluacionCreateEdit.get('moevCodigo').value,
            moevDescripcion:this.frmModeloEvaluacionCreateEdit.get('moevDescripcion').value,
            moevEstado:this.frmModeloEvaluacionCreateEdit.get('moevEstado').value,
            moevNemonico:this.frmModeloEvaluacionCreateEdit.get('moevNemonico').value,
        };

        this._modeloEvaluacionService.guardarModeloEvaluacion(modeloEvaluacionNuevo).subscribe(
            (respuesta:any)=>{
                Swal.fire(`Modelo Evaluación actualizada!`,'','success');
                this.spinner.hide();
                this.listarModeloEvaluacion();
            },
            (error:any)=>{
                Swal.fire(`No se pudo actualizar el Modelo de Evaluación `,'','error');
                this.spinner.hide();
                this.listarModeloEvaluacion();
            }
        );
        modalForm.close('Accept click');
    }

    activarInactivar(modeloEvaluacion:ModeloEvaluacion){
        this.spinner.show();
        if (modeloEvaluacion.moevEstado == 1) {
            this._modeloEvaluacionService.activarDesactivarModelosEvaluacion(modeloEvaluacion.moevCodigo).subscribe(
                (respuesta:any)=>{
                    Swal.fire(`Modelo Evaluación Inactivada!`,'','success');
                    this.spinner.hide();
                    this.listarModeloEvaluacion();
                },
                (error:any)=>{
                    Swal.fire(`Ups! ocurrió un error`,'','error');
                    this.spinner.hide();
                    this.listarModeloEvaluacion();
                }
            );
        } else {
            modeloEvaluacion.moevEstado = 1;
            this._modeloEvaluacionService.guardarModeloEvaluacion(modeloEvaluacion).subscribe(
                (respuesta:any)=>{
                    Swal.fire(`Modelo Evaluación Activada!`,'','success');
                    this.spinner.hide();
                    this.listarModeloEvaluacion();
                },
                (error:any)=>{
                    Swal.fire(`No se pudo activar el Modelo Evaluación `,'','error');
                    this.spinner.hide();
                    this.listarModeloEvaluacion();
                }
            );
        }
    }

    applyFilter(event: Event) {
        const filterValue = (event.target as HTMLInputElement).value;
        this.dataSource.filter = filterValue.trim().toLowerCase();
      
        if (this.dataSource.paginator) {
          this.dataSource.paginator.firstPage();
        }
    }

    get ReactiveFrmModeloEvaluacionCreateEdit() {
        return this.frmModeloEvaluacionCreateEdit.controls;
    }

}