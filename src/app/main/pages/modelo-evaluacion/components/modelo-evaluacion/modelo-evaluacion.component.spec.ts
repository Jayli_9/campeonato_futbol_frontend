import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ModeloEvaluacionComponent } from './modelo-evaluacion.component';

describe('ModeloEvaluacionComponent', () => {
  let component: ModeloEvaluacionComponent;
  let fixture: ComponentFixture<ModeloEvaluacionComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ModeloEvaluacionComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ModeloEvaluacionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});