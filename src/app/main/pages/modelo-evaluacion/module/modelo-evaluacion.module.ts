import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { CoreCommonModule } from '@core/common.module';
import { ContentHeaderModule } from 'app/layout/components/content-header/content-header.module';
import { MaterialModule } from 'app/main/shared/material/material.module';
import { ModeloEvaluacionComponent } from '../components/modelo-evaluacion/modelo-evaluacion.component';
import { RUTA_MODELO_EVALUACION } from '../routes/modelo-evaluacion-routing.module';

@NgModule({
  declarations: [ModeloEvaluacionComponent],
  imports: [
    CommonModule,
    RouterModule.forChild(RUTA_MODELO_EVALUACION),
    FormsModule,
    ReactiveFormsModule,
    MaterialModule,
    ContentHeaderModule,
    CoreCommonModule
  ]
})
export class ModeloEvaluacionModule { }