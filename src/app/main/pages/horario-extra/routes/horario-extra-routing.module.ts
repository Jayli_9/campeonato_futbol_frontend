import { Routes } from "@angular/router";
import { AuthGuard } from "app/auth/helpers/auth.guards";
import { HorarioCursoExtraComponent } from "../components/horario-curso-extra/horario-curso-extra.component";
import { HorarioExtraComponent } from "../components/horario-extra/horario-extra.component";

export const RUTA_HORARIO_EXTRA:Routes=[
    {
        path:'horario-extra',
        component:HorarioExtraComponent
    },
    {
        path:'horarioExtraordinario',
        component:HorarioCursoExtraComponent,
        canActivate:[AuthGuard]
    }
]