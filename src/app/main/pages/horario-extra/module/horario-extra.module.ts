import { NgModule } from "@angular/core";
import { HorarioCursoExtraComponent } from "../components/horario-curso-extra/horario-curso-extra.component";
import { HorarioExtraComponent } from "../components/horario-extra/horario-extra.component";
import { CommonModule } from "@angular/common";
import { RouterModule } from "@angular/router";
import { RUTA_HORARIO_EXTRA } from "../routes/horario-extra-routing.module";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { MaterialModule } from "app/main/shared/material/material.module";
import { ContentHeaderModule } from "app/layout/components/content-header/content-header.module";
import { CoreCommonModule } from "@core/common.module";
import { ProgramaStore } from "../services/programa-store";

@NgModule({
    declarations: [
        HorarioCursoExtraComponent,
        HorarioExtraComponent
    ],
    imports: [
      CommonModule,
      RouterModule.forChild(RUTA_HORARIO_EXTRA),
      FormsModule,
      ReactiveFormsModule,
      MaterialModule,
      ContentHeaderModule,
      CoreCommonModule
    ],
    providers: [ProgramaStore]
  })
  export class HorarioExtraModule { }