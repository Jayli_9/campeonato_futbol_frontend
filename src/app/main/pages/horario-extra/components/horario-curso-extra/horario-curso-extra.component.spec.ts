import { ComponentFixture, TestBed } from '@angular/core/testing';
import { HorarioCursoExtraComponent } from './horario-curso-extra.component';

describe('HorarioCursoExtraComponent', () => {
  let component: HorarioCursoExtraComponent;
  let fixture: ComponentFixture<HorarioCursoExtraComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ HorarioCursoExtraComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(HorarioCursoExtraComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});