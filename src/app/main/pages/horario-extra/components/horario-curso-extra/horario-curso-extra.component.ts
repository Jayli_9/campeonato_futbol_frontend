import { Component, OnInit, ViewChild } from "@angular/core";
import { MatPaginator } from "@angular/material/paginator";
import { MatSort } from "@angular/material/sort";
import { Router } from "@angular/router";
import { NgbModal } from "@ng-bootstrap/ng-bootstrap";
import { AuthenticationService } from "app/auth/service";
import { NgxSpinnerService } from "ngx-spinner";
import { ProgramaStore } from "../../services/programa-store";
import { CursoExtraordinaria } from "../../interfaces/curso-extraordinaria";
import { RegimenAnioLectivo } from "../../interfaces/regimen-anio-lectivo";
import { InstEstablecimiento } from "../../interfaces/instEstablecimiento";
import { ServicioEducativo } from "../../interfaces/servicio-educativo";
import { Jornada } from "../../interfaces/jornada";
import { Especialidad } from "../../interfaces/especialidad";
import { Modulo } from "../../interfaces/modulo";
import { Grado } from "../../interfaces/grado";
import { Institucion } from "../../interfaces/institucion";
import { Docente } from "../../interfaces/docente";
import { MatTableDataSource } from "@angular/material/table";
import { HorarioExtraService } from "../../services/horario-extra.service";
import Swal from "sweetalert2";
import { RespuestaEspecialidadInterfaz } from "../../interfaces/respuesta-especialidad-interfaz";
import { RespuestaJornadaInterfaz } from "../../interfaces/respuesta-jornada-interfaz";
import { RespuestaModuloInterfaz } from "../../interfaces/respuesta-modulo-interfaz";
import { RespuestaGradoInterfaz } from "../../interfaces/respuesta-grado-interfaz";
import { RespuestaServicioEducativoInterfaz } from "../../interfaces/respuesta-servicio-educativo-interfaz";
import { RespuestaCursoExtraordinariaInterfaz } from "../../interfaces/respuesta-curso-extraordinaria-interfaz";
import { RespuestaRegimenAnioLectivoInterfaz } from "../../interfaces/respuesta-regimen-anio-lectivo-interfaz";
import { RespuestaDocenteInterfaz } from "../../interfaces/respuesta-docente-interfaz";
import { RespuestaInstEstablecimientoInterfaz } from "../../interfaces/respuesta-inst-establecimiento-interfaz";
import { RespuestaInstitucionInterfaz } from "../../interfaces/respuesta-institucion-interfaz";
import { CursoExtraParametros } from "../../interfaces/curso-extra-parametros";
import { RegimenAnioLectParametros } from "../../interfaces/regimen-anio-lect-parametros";
import { ServicioEducativoParametros } from "../../interfaces/servicio-educativo-parametros";
import { JornadaParametros } from "../../interfaces/jornada-parametros";
import { EspecialidadParametros } from "../../interfaces/especialidad-parametros";
import { GradosParametros } from "../../interfaces/grados-parametros";
import { ModuloParametros } from "../../interfaces/modulo-parametros";
import { DistributivoExtra } from "../../interfaces/distributivo-extra";
import { DistributivoExtraParametros } from "../../interfaces/distributivo-extra-parametros";
import { RespuestaDistributivoExtraInterfaz } from "../../interfaces/respuesta-distributivo-extra-interfaz";
import { DistributivoModulo } from "../../interfaces/distributivo-modulo";
import { RespuestaDistributivoModuloInterfaz } from "../../interfaces/respuesta-distributivo-modulo-interfaz";

@Component({
    selector: 'horario-curso-extra',
    templateUrl: './horario-curso-extra.component.html',
    styleUrls: ['./horario-curso-extra.component.scss']
})
export class HorarioCursoExtraComponent implements OnInit {

    public contentHeader: object;
    public submitted = false;

    listaCursoExtraordinarioBusqueda:CursoExtraordinaria[];
    listaCursoExtraordinarioParalelo:CursoExtraordinaria[];
    listaCursoExtraordinario:CursoExtraordinaria[] = [];
    listaRegimenAnioLectivo:RegimenAnioLectivo[];
    listaInstEstablecimento: InstEstablecimiento[];
    listaServiciosEducativos:ServicioEducativo[];
    listaDistributivoExtra:DistributivoExtra[];
    listaDistributivoModulo:DistributivoModulo[];
    listaJornada:Jornada[];
    listaEspecialidad:Especialidad[];
    listaModulo:Modulo[];
    listaGrado:Grado[];

    institucion:Institucion;
    regimenAnioLectivo:RegimenAnioLectivo;
    docente:Docente;

    columnasModuloGrado=['num', 'tipo-educacion', 'modalidad', 'jornada', 'servicio-educativo', 'modulo-grado', 'especialidad', 'paralelo', 'acciones'];
    dataSource: MatTableDataSource<CursoExtraordinaria>;

    @ViewChild(MatPaginator) paginator: MatPaginator;
    @ViewChild(MatSort) sort: MatSort;

    constructor(private readonly router: Router,
        private modalService: NgbModal,
        public spinner: NgxSpinnerService,
        private _horarioExtraService:HorarioExtraService,
        private _authenticationService: AuthenticationService,
        private readonly programaStore: ProgramaStore
      ){
    }

    ngOnInit(): void {
        this.contentHeader = {
            headerTitle: 'Horario', 
            actionButton: false,
            breadcrumb: {
                type: '',
                links: [
                    {
                        name: 'Inicio',
                        isLink: true,
                        link: '/pages/inicio'
                    },
                    {
                        name: 'Horario',
                        isLink: false
                    },
                ]
            }
        };
        this.obtenerInstitucionPorAmie();
    }

    obtenerInstitucionPorAmie() {
        const currentUser = this._authenticationService.currentUserValue;
        let codAmie = currentUser.sede.nemonico;
        this._horarioExtraService.obtenerInstitucionPorAmie(codAmie).subscribe(
            (respuesta: RespuestaInstitucionInterfaz)=>{
                this.institucion = respuesta.objeto;
                this.listarEstablecimientoInstitucion();
                this.obtenerDocente();
                this.spinner.hide();
            },
            (error:any)=>{
                Swal.fire('Ups! ocurrió un error al cargar la Institución','','error'); 
            }
        );
    }
    
    listarEstablecimientoInstitucion() {
        let codInstitucion = this.institucion.insCodigo;
        this._horarioExtraService.obtenerTodosEstablecimientoPorInstitucion(codInstitucion).subscribe(
            (respuesta: RespuestaInstEstablecimientoInterfaz)=>{
                this.listaInstEstablecimento=respuesta.listado;
                this.buscarListasCargarCombosRegionAnioLec();
            },
            (error:any)=>{
                this.spinner.hide();
                Swal.fire('Ups! ocurrió un error al cargar la Institución','','error'); 
            }
        );
    }
    
    obtenerDocente() {
        const currentUser = this._authenticationService.currentUserValue;
        let identificacion = currentUser.identificacion;
        this._horarioExtraService.obtenerDocentePorIdentificacion(identificacion).subscribe(
            (respuesta:RespuestaDocenteInterfaz) => {
                this.docente=respuesta.objeto;
            },
            (error:any)=>{
                this.spinner.hide();
                Swal.fire('Ups! ocurrió un error al obtener al docente','','error'); 
            }
        );
    }
    
    buscarListasCargarCombosRegionAnioLec() {
        let insestCodigo = this.listaInstEstablecimento.map((establecimiento) => establecimiento.insestCodigo);
        let insestCodigoLimpio = Array.from(new Set(insestCodigo));
        let listaCursoExtraos:CursoExtraordinaria[];
        let cursoExtraParametros:CursoExtraParametros = {listaCodigoEstablecimiento:insestCodigoLimpio};
        this._horarioExtraService.obtenerTodosCursosExtraordinarioPorListaEstablecimiento(cursoExtraParametros).subscribe(
            (respuesta: RespuestaCursoExtraordinariaInterfaz) => {
                listaCursoExtraos = respuesta.listado;
                this.listarTodasRegimenAnioLectivoPorEstablecimiento(listaCursoExtraos);
                this.spinner.hide();
            },
            (error:any) => {
                this.spinner.hide();
                Swal.fire('Ups! ocurrió no se pudo obtener los datos','','error');
            }
        );
    }
    
    listarTodasRegimenAnioLectivoPorEstablecimiento(listaCursoExtraos:CursoExtraordinaria[]) {
        this.listaRegimenAnioLectivo =  null;
        let reanleCodigo = listaCursoExtraos.map((cursoExta) =>cursoExta.reanleCodigo);
        let reanleCodigoLimpio = Array.from(new Set(reanleCodigo));
        let regimenAnioLectParametros:RegimenAnioLectParametros = {listaReanleCodigo:reanleCodigoLimpio};
        this._horarioExtraService.obtenerTodasRegimenAnioLectivoPorListaEstablecimiento(regimenAnioLectParametros).subscribe(
            (respuesta: RespuestaRegimenAnioLectivoInterfaz) => {
                this.listaRegimenAnioLectivo = respuesta.listado; 
            },
            (error:any)=>{
                this.spinner.hide();
                Swal.fire('Ups! ocurrió un error al cargar los regimen con los años lectivos','','error'); 
            }
        );
    }
    
    listarCursoExtraordinariaPorInstitucion(reanleCodigo:any) {        
        let insestCodigo = this.listaInstEstablecimento.map((establecimiento) => establecimiento.insestCodigo);
        let cursoExtraParametros:CursoExtraParametros = {reanleCodigo:reanleCodigo,listaInsestCodigo:insestCodigo};
        this._horarioExtraService.obtenerTodosCursosExtraordinarioPorRegimenYListaEstablecimiento(cursoExtraParametros).subscribe(
            (respuesta: RespuestaCursoExtraordinariaInterfaz)=>{
                this.listaCursoExtraordinarioBusqueda = respuesta.listado;
                this.obtenerParaleloPorListaDeCursoExtra();
            },
            (error:any)=>{
                this.spinner.hide();    
                Swal.fire('Ups! ocurrió un error con la lista curso extraordinario','','error'); 
            }
        );
    }
    
    obtenerParaleloPorListaDeCursoExtra() {
        let curextCodigo = this.listaCursoExtraordinarioBusqueda.map((cursoExtra) => cursoExtra.curextCodigo);
        let cursoExtraParametros:CursoExtraParametros = {listaCurextCodigo:curextCodigo};
        this._horarioExtraService.obtenerTodosCursosExtraordinarioListacurextCodigo(cursoExtraParametros).subscribe(
            (respuesta: RespuestaCursoExtraordinariaInterfaz)=>{
                this.listaCursoExtraordinarioParalelo=respuesta.listado;
                this.obtenerListaDistributivoModulo();
            },
            (error:any)=>{
                this.spinner.hide();    
                Swal.fire('Ups! ocurrió un error con al obtener el paralelo','','error'); 
            }
        );
    }

    obtenerListaDistributivoModulo() {
        let curextCodigo = this.listaCursoExtraordinarioParalelo.map((cursoExtraPa) => cursoExtraPa.curextCodigo);
        let cupaexCodigo = this.listaCursoExtraordinarioParalelo.map((cursoExtraPa) => cursoExtraPa.cupaexCodigo);
        let curextCodigoLimpio = Array.from(new Set(curextCodigo));
        let cupaexCodigoLimpio = Array.from(new Set(cupaexCodigo));
        this.listaCursoExtraordinario = [];
        let distributivoExtraParametros:DistributivoExtraParametros={
            listaCurextCodigo:curextCodigoLimpio,
            listaCupaexCodigo:cupaexCodigoLimpio,
            docCodigo:this.docente.codPersona
        }
        this._horarioExtraService.obtenerListaDitributivoModuloPorListaCurexListaCuparYDocente(distributivoExtraParametros).subscribe(
            (respuesta: RespuestaDistributivoModuloInterfaz)=>{
                this.listaDistributivoModulo = respuesta.listado;
                for (let i = 0; i < this.listaDistributivoModulo.length; i++) {
                    let cursoExtraordinariaDistrib:CursoExtraordinaria = this.listaCursoExtraordinarioParalelo.find((cursoExtreDistiv) => cursoExtreDistiv.curextCodigo == this.listaDistributivoModulo[i].curextCodigo && cursoExtreDistiv.cupaexCodigo == this.listaDistributivoModulo[i].cupaextCodigo);
                    this.listaCursoExtraordinario.push(cursoExtraordinariaDistrib);
                }
                this.obtenerListaDistributivoExtra();
            },
            (error:any)=>{
                this.spinner.hide();    
                Swal.fire('Error al obtener los datos del distributivo modulo','','error'); 
            }
        );

    }

    obtenerListaDistributivoExtra() {
        let curextCodigo = this.listaCursoExtraordinarioParalelo.map((cursoExtraPa) => cursoExtraPa.curextCodigo);
        let cupaexCodigo = this.listaCursoExtraordinarioParalelo.map((cursoExtraPa) => cursoExtraPa.cupaexCodigo);
        let curextCodigoLimpio = Array.from(new Set(curextCodigo));
        let cupaexCodigoLimpio = Array.from(new Set(cupaexCodigo));
        let distributivoExtraParametros:DistributivoExtraParametros={
            listaCurextCodigo:curextCodigoLimpio,
            listaCupaexCodigo:cupaexCodigoLimpio,
            docCodigo:this.docente.codPersona
        }
        this._horarioExtraService.obtenerListaDitributivoExtraPorListaCurexListaCuparYDocente(distributivoExtraParametros).subscribe(
            (respuesta: RespuestaDistributivoExtraInterfaz)=>{
                this.listaDistributivoExtra = respuesta.listado;
                for (let i = 0; i < this.listaDistributivoExtra.length; i++) {
                    let cursoExtraordinariaDistrib:CursoExtraordinaria = this.listaCursoExtraordinarioParalelo.find((cursoExtreDistiv) => cursoExtreDistiv.curextCodigo == this.listaDistributivoExtra[i].curextCodigo && cursoExtreDistiv.cupaexCodigo == this.listaDistributivoExtra[i].cupaexCodigo);
                    this.listaCursoExtraordinario.push(cursoExtraordinariaDistrib);
                }
                this.listaCursoExtraordinario = Array.from(new Set(this.listaCursoExtraordinario));
                this.obtenerListaServiciosEducativosPorListaDeCodigos();
                this.obtenerListaGradoPorListaDeServiciosEducativos();
                this.obtenerListaModuloPorListaDeServiciosEducativos();
                this.obtenerListaJornadasPorListaDeCodigos();
                this.obtenerListaEspecialidadPorListaDeCodigos();
                this.dataSource = new MatTableDataSource(this.listaCursoExtraordinario);
                this.dataSource.paginator = this.paginator;
                this.dataSource.paginator._intl.itemsPerPageLabel="Asignaturas por página";
                this.dataSource.paginator._intl.nextPageLabel="Siguiente";
                this.dataSource.paginator._intl.previousPageLabel="Anterior";
                this.dataSource.sort = this.sort;
            },
            (error:any)=>{
                this.spinner.hide();    
                Swal.fire('Error al obtener los datos del distributivo extra','','error'); 
            }
        );
        
    }
    
    obtenerListaServiciosEducativosPorListaDeCodigos() {
        let sereduCodigo = this.listaCursoExtraordinarioBusqueda.map((cursoExtra) => cursoExtra.sereduCodigo);
        let sereduCodigoLimpio = Array.from(new Set(sereduCodigo));
        let servicioEducativoParametros:ServicioEducativoParametros = {listaSereduCodigo:sereduCodigoLimpio};
        this._horarioExtraService.obtenerTodosServiciosEducativosPorListaCodigos(servicioEducativoParametros).subscribe(
              (respuesta:RespuestaServicioEducativoInterfaz)=>{
                this.listaServiciosEducativos = respuesta.listado;              
                for (let i=0; i<this.listaCursoExtraordinario.length; i++) {
                    let servicioEdu:ServicioEducativo= this.listaServiciosEducativos.find((servicioEdu) => servicioEdu.sereduCodigo==this.listaCursoExtraordinario[i].sereduCodigo);
                    this.listaCursoExtraordinario[i].servEducativo = servicioEdu.sereduDescripcion;
                    this.listaCursoExtraordinario[i].tipoEducacion = servicioEdu.tipeduNombre;
                    this.listaCursoExtraordinario[i].modalidad = servicioEdu.modalidad;
                }
                this.obtenerListaGradoPorListaDeServiciosEducativos();
                this.obtenerListaModuloPorListaDeServiciosEducativos();
            },
            (error:any)=>{
                this.spinner.hide();    
                Swal.fire('Ups! ocurrió un error con al obtener los servicios educativos','','error'); 
            }
        );
    }
    
    obtenerListaGradoPorListaDeServiciosEducativos() {
        let listaCurexGra = this.listaCursoExtraordinario.filter((grado)=>grado.graCodigo != 0);
        let graCodigo =listaCurexGra.map((grado)=>grado.graCodigo);
        let graCodigoLimpio = Array.from(new Set(graCodigo));
        if (graCodigo.length > 0) {
            let gradosParametros:GradosParametros =  {listaGraCodigo:graCodigoLimpio};
            this._horarioExtraService.obtenerTodosGradorPorListaGraCodigoYEstado(gradosParametros).subscribe(
                (respuesta:RespuestaGradoInterfaz)=>{
                    this.listaGrado = respuesta.listado;
                    for (let i=0; i<this.listaCursoExtraordinario.length; i++) {
                        let grado:Grado= this.listaGrado.find((grado) => grado.graCodigo==this.listaCursoExtraordinario[i].graCodigo);
                        if (grado != null || grado!=undefined) {
                            this.listaCursoExtraordinario[i].gradoModulo = grado.graDescripcion;
                            this.listaCursoExtraordinario[i].graCodigo = grado.graCodigo;
                        }
                    }
                },
                (error:any)=>{
                    this.spinner.hide();    
                    Swal.fire('Ups! ocurrió un error con al obtener los grados','','error'); 
                }
            );
        }
    }
    
    obtenerListaModuloPorListaDeServiciosEducativos() {
        let listaCurexMod = this.listaCursoExtraordinario.filter((modulo) => modulo.modCodigo != 0);
        let modCodigo = listaCurexMod.map((modulo) => modulo.modCodigo);
        let modCodigoLimpio = Array.from(new Set(modCodigo));
        if (modCodigoLimpio.length > 0) {
            let moduloParametros:ModuloParametros = {listaModCodigo:modCodigoLimpio};
            this._horarioExtraService.obtenerTodosModulosPorListaCodigo(moduloParametros).subscribe(
                (respuesta:RespuestaModuloInterfaz)=>{
                    this.listaModulo = respuesta.listado;
                    for (let i=0; i<this.listaCursoExtraordinario.length; i++) {
                        let modulo:Modulo= this.listaModulo.find((modulo) => modulo.sereduCodigo==this.listaCursoExtraordinario[i].sereduCodigo);
                        if (modulo != null || modulo!=undefined) {
                            this.listaCursoExtraordinario[i].gradoModulo = modulo.modDescripcion;
                            this.listaCursoExtraordinario[i].moduloCodigo = modulo.modCodigo;
                        }
                    }
                },
                (error:any)=>{
                    this.spinner.hide();    
                    Swal.fire('Ups! ocurrió un error con al obtener los modulos','','error'); 
                }
            );
        }
    }
    
    obtenerListaJornadasPorListaDeCodigos() {
        let jorCodigo = this.listaCursoExtraordinarioBusqueda.map((cursoExtra) => cursoExtra.jorCodigo);
        let jorCodigoLimpio = Array.from(new Set(jorCodigo));
        let jornadaParametros:JornadaParametros = {listaJorCodigo:jorCodigoLimpio};
        this._horarioExtraService.obtenerTodasJornadasPorCodigo(jornadaParametros).subscribe(
            (respuesta:RespuestaJornadaInterfaz)=>{
                this.listaJornada = respuesta.listado;
                for (let i=0; i<this.listaCursoExtraordinario.length; i++) {
                    let jornada:Jornada = this.listaJornada.find((jornada) => jornada.jorCodigo == this.listaCursoExtraordinario[i].jorCodigo);
                    this.listaCursoExtraordinario[i].jornada = jornada.jorNombre;
                }
            },
            (error:any)=>{
                this.spinner.hide();    
                Swal.fire('Ups! ocurrió un error con al obtener las jornadas','','error'); 
            }
        );
    }
    
    obtenerListaEspecialidadPorListaDeCodigos() {
        let espCodigo = this.listaCursoExtraordinarioBusqueda.map((cursoExtra) => cursoExtra.espCodigo);
        let espCodigoLimpio = Array.from(new Set(espCodigo));
        let especialidadParametros:EspecialidadParametros = {listaEspCodigo:espCodigoLimpio};
        this._horarioExtraService.listarLasEspecialidadesPorListaEspCodigoYEstado(especialidadParametros).subscribe(
            (respuesta:RespuestaEspecialidadInterfaz)=>{
                this.listaEspecialidad = respuesta.listado;
                for (let i=0; i<this.listaCursoExtraordinario.length; i++) {
                    let especialidad:Especialidad = this.listaEspecialidad.find((servicioEducativo) => servicioEducativo.espCodigo == this.listaCursoExtraordinario[i].espCodigo);
                    if (especialidad != null && especialidad != undefined) {
                        this.listaCursoExtraordinario[i].especialidad = especialidad.espDescripcion;
                    }
                      
                }
            },
            (error:any)=>{
                this.spinner.hide();    
                Swal.fire('Ups! ocurrió un error con al obtener las especialidades','','error'); 
            }
        );
    }
    
    abrirCrearHorarios(cursoExtraordinaria:CursoExtraordinaria) {
        this.programaStore.cursoExtraordinaria = cursoExtraordinaria;
        this.programaStore.docente = this.docente;
        this.router.navigate(['pages/horario-extra']);
    }
    
    applyFilter(event: Event) {
        const filterValue = (event.target as HTMLInputElement).value;
        this.dataSource.filter = filterValue.trim().toLowerCase();
         
        if (this.dataSource.paginator) {
            this.dataSource.paginator.firstPage();
        }
    }

}