import { ComponentFixture, TestBed } from '@angular/core/testing';
import { HorarioExtraComponent } from './horario-extra.component';

describe('HorarioExtraComponent', () => {
  let component: HorarioExtraComponent;
  let fixture: ComponentFixture<HorarioExtraComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ HorarioExtraComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(HorarioExtraComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});