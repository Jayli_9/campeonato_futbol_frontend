import { Component, OnInit, ViewChild } from "@angular/core";
import { UntypedFormBuilder } from "@angular/forms";
import { MatPaginator } from "@angular/material/paginator";
import { MatSort } from "@angular/material/sort";
import { Router } from "@angular/router";
import { NgbModal } from "@ng-bootstrap/ng-bootstrap";
import { NgxSpinnerService } from "ngx-spinner";
import { ProgramaStore } from "../../services/programa-store";
import { CursoExtraordinaria } from "../../interfaces/curso-extraordinaria";
import { Docente } from "../../interfaces/docente";
import { MallaAsignaturaExtra } from "../../interfaces/malla-asignatura-extra";
import { HorarioExtraService } from "../../services/horario-extra.service";
import { ServicioEducativo } from "../../interfaces/servicio-educativo";
import Swal from "sweetalert2";
import { RespuestaServicioEducativoInterfaz } from "../../interfaces/respuesta-servicio-educativo-interfaz";
import { RespuestaMallaAsignaturaExtraInterfaz } from "../../interfaces/respuesta-malla-asignatura-extra-interfaz";
import { HorarioExtraParametro } from "../../interfaces/horario-extra-parametro";
import { Dia } from "../../interfaces/dia";
import { HorarioExtraDia } from "../../interfaces/horario-extra-dia";
import { HorarioExtra } from "../../interfaces/horario-extra";
import { RespuestaHorarioExtraInterfaz } from "../../interfaces/respuesta-horario-extra-interfaz";
import { MatTableDataSource } from "@angular/material/table";
import { DocenteParametro } from "../../interfaces/docente-parametro";
import { RespuestaDocenteInterfaz } from "../../interfaces/respuesta-docente-interfaz";
import { MallaAsigExtraParametro } from "../../interfaces/malla-asig-extra-parametro";

@Component({
    selector: 'horario-extra',
    templateUrl: './horario-extra.component.html',
    styleUrls: ['./horario-extra.component.scss']
})
export class HorarioExtraComponent implements OnInit {

    public contentHeader: object;

    listaMallaAsignatura:MallaAsignaturaExtra[];
    listaDia:Dia[];
    listaHorarioExtra:HorarioExtra[];
    listaDocentes:Docente[];

    cursoExtraordinaria:CursoExtraordinaria;
    docente:Docente;
    servicioEducativo:ServicioEducativo;

    columnasHorario=[];

    dataSource: MatTableDataSource<HorarioExtra>;

    @ViewChild(MatPaginator) paginator: MatPaginator;
    @ViewChild(MatSort) sort: MatSort;
    constructor(private readonly router: Router,
        private readonly fb: UntypedFormBuilder,
        private modalService: NgbModal,
        public spinner: NgxSpinnerService,
        private _horarioExtraService:HorarioExtraService,
        private readonly programaStore: ProgramaStore
        ){
    }

    ngOnInit(): void {
        this.contentHeader = {
            headerTitle: 'Horario Extraordinario',
            actionButton: false,
            breadcrumb: {
                type: '',
                links: [
                    {
                      name: 'Inicio',
                      isLink: true,
                      link: '/pages/inicio'
                    },
                    {
                      name: 'Horario Extraordinario',
                      isLink: false
                    },
                ]
            }
        };
        this.obtenerCursoExtraordinario();
    }

    obtenerCursoExtraordinario() {
        this.cursoExtraordinaria = this.programaStore.cursoExtraordinaria;
        if (this.cursoExtraordinaria == null || this.cursoExtraordinaria == undefined) {
            this.volverHorarioCursoExtra();
            return;
        }
        this.docente=this.programaStore.docente;
        this.spinner.show();
        this.obternerServicioEducativo();
        this.spinner.hide();
    }

    obternerServicioEducativo() {
        this._horarioExtraService.obtenerServicioEducativoPorCodigo(this.cursoExtraordinaria.sereduCodigo).subscribe(
            (respuesta:RespuestaServicioEducativoInterfaz)=>{
                this.servicioEducativo = respuesta.objeto;
                this.obtenerTodasMallaAsignaturasExtraCursoExtraordinariaYGrado();
            },
            (error:any)=>{
                this.spinner.hide();    
                Swal.fire('Ups! ocurrió un error con el servicio educativo','','error'); 
            }
        );
    }

    obtenerTodasMallaAsignaturasExtraCursoExtraordinariaYGrado() {
        let mallaAsigExtraParametro:MallaAsigExtraParametro={
            docCodigo:0,
            curextCodigo:this.cursoExtraordinaria.curextCodigo,
            cupaexCodigo:this.cursoExtraordinaria.cupaexCodigo,
            grado:0
        }

        this._horarioExtraService.obtenerTodasMallaAsignaturaExtraPorCurextCodigoYParalelo(mallaAsigExtraParametro).subscribe(
            (respuesta:RespuestaMallaAsignaturaExtraInterfaz)=>{
                this.listaMallaAsignatura = respuesta.listado;
                this.obtenerDocentePorListaCodigo();
            },
            (error:any)=>{
                this.spinner.hide();    
                Swal.fire('Ups! ocurrió un error con la malla asignatura','','error'); 
            }
        );
    }

    obtenerDocentePorListaCodigo() {
        let docCodigo = this.listaMallaAsignatura.map((docente)=>docente.docCodigo);
        let docCodigoLimpio = Array.from(new Set(docCodigo));
        let docenteParamatro:DocenteParametro = {listaCodPersona:docCodigoLimpio};
        this._horarioExtraService.obtenerDocentePorListaCodigo(docenteParamatro).subscribe(
            (respuesta:RespuestaDocenteInterfaz) =>{
                this.listaDocentes = respuesta.listado;
                this.obtenerTodosHorarioPorCursoExtra();
            },
            (error:any)=>{
                this.spinner.hide();    
                Swal.fire('Ups! ocurrió un error al obtener los docentes','','error'); 
            }
        );
    }

    obtenerTodosHorarioPorCursoExtra() {
        let horarioExtraParametro:HorarioExtraParametro = {
            curexCodigo:this.cursoExtraordinaria.curextCodigo,
            cupaexCodigo:this.cursoExtraordinaria.cupaexCodigo,
            insestCodigo:this.cursoExtraordinaria.insestCodigo
        };
        this.listaHorarioExtra = null;
        this.columnasHorario = ['num','horario'];
        this._horarioExtraService.obtenerListaHorarioExtraPorCurextCuparInsest(horarioExtraParametro).subscribe(
            (respuesta:RespuestaHorarioExtraInterfaz) =>{
                this.listaHorarioExtra = respuesta.listado;
                this.listaDia = this.listaHorarioExtra[0].listaDia;
                for (let i=0; i<this.listaDia.length; i++) {
                    this.columnasHorario.push(this.listaDia[i].diaNemonico);
                }
                for (let i=0; i<this.listaHorarioExtra.length; i++) {
                    for (let j=0; j<this.listaDia.length; j++) {
                        let hExtDia:HorarioExtraDia = this.listaHorarioExtra[i].listaHorarioExtraDia.find((horarioExtraDia) => horarioExtraDia.diaCodigo == this.listaDia[j].diaCodigo);
                        if (hExtDia == null || hExtDia == undefined) {
                            let hExtDiaNuevo:HorarioExtraDia = {
                                horCodigo:null,
                                diaCodigo:this.listaDia[j].diaCodigo,
                                disextCodigo:null,
                                insestCodigo:null,
                                curexCodigo:null,
                                jorCodigo:null,
                                graCodigo:null,
                                horHorInicio:null,
                                horHorFin:null,
                                horNroHoras:null,
                                horEstado:null,
                                cupaexCodigo:null,
                                editar:'NO_EDITAR'
                            }
                            this.listaHorarioExtra[i].listaHorarioExtraDia.push(hExtDiaNuevo);
                        }
                    }
                    this.listaHorarioExtra[i].editar='NO_EDITAR';
                    let listaHExtDia:HorarioExtraDia[] = this.listaHorarioExtra[i].listaHorarioExtraDia;
                    for (let j = 0; j<listaHExtDia.length; j++) {
                        if (listaHExtDia[j].disextCodigo != null && listaHExtDia[j].disextCodigo != undefined) {
                            let distributivo:MallaAsignaturaExtra = this.listaMallaAsignatura.find((distributivo)=>distributivo.disextCodigo==listaHExtDia[j].disextCodigo);
                            let docent:Docente = this.listaDocentes.find((docent)=>docent.codPersona == distributivo.docCodigo);
                            if (docent != null && docent != undefined){
                                listaHExtDia[j].nombreDocente = docent.nomPersona;
                            }
                            listaHExtDia[j].descripcionAsigna = distributivo.asiDescripcion;
                        }
                        listaHExtDia[j].editar = 'NO_EDITAR';
                    }
                    
                }
                this.columnasHorario.push('accion');
                this.dataSource = new MatTableDataSource(this.listaHorarioExtra);
                this.dataSource.paginator = this.paginator;
                this.dataSource.paginator._intl.itemsPerPageLabel="Asignaturas por página";
                this.dataSource.paginator._intl.nextPageLabel="Siguiente";
                this.dataSource.paginator._intl.previousPageLabel="Anterior";
                this.dataSource.sort = this.sort;
            },
            (error:any)=>{
                this.spinner.hide();    
                Swal.fire('Ups! ocurrió un error al obtener los horarios','','error'); 
            }
        );
    }

    nuevoHorario() {
        let listaHorExtraDia:HorarioExtraDia[] = [];
        for (let i=0; i<this.listaDia.length; i++) {
            let hExtDiaNuevo:HorarioExtraDia = {
                horCodigo:null,
                diaCodigo:this.listaDia[i].diaCodigo,
                disextCodigo:null,
                insestCodigo:null,
                curexCodigo:null,
                jorCodigo:null,
                graCodigo:null,
                horHorInicio:null,
                horHorFin:null,
                horNroHoras:null,
                horEstado:null,
                cupaexCodigo:null,
                editar:'NO_EDITAR'
            }
            listaHorExtraDia.push(hExtDiaNuevo);   
        }
        let horarioExtra:HorarioExtra = {
            horHorInicio:null,
            horHorFin:null,
            editar:'NO_EDITAR',
            listaHorarioExtraDia:listaHorExtraDia
        }
        this.dataSource.data.push(horarioExtra);
        this.dataSource.filter = "";
    }

    guardarHorarioExtra(horExtra:HorarioExtra){
        this.spinner.show();
        let minutosInicio:number = Number(horExtra.horHorInicio.toString().split(":")[1]);
        let horasInicio:number = Number(horExtra.horHorInicio.toString().split(":")[0]);
        let minutosFin:number = Number(horExtra.horHorFin.toString().split(":")[1]);
        let horasFin:number = Number(horExtra.horHorFin.toString().split(":")[0]);
        let totalHorasInicio = horasInicio+(minutosInicio/60);
        let totalHorasFin = horasFin+(minutosFin/60);
        let numHoras = totalHorasFin - totalHorasInicio;
        numHoras = Math.round((numHoras + Number.EPSILON)*100)/100;
        for (let i=0; i<horExtra.listaHorarioExtraDia.length;i++) {
            horExtra.listaHorarioExtraDia[i].graCodigo = this.cursoExtraordinaria.graCodigo;
            horExtra.listaHorarioExtraDia[i].insestCodigo = this.cursoExtraordinaria.insestCodigo;
            horExtra.listaHorarioExtraDia[i].curexCodigo = this.cursoExtraordinaria.curextCodigo;
            horExtra.listaHorarioExtraDia[i].cupaexCodigo = this.cursoExtraordinaria.cupaexCodigo;
            horExtra.listaHorarioExtraDia[i].jorCodigo = this.cursoExtraordinaria.jorCodigo;
            horExtra.listaHorarioExtraDia[i].horNroHoras = numHoras;
            horExtra.listaHorarioExtraDia[i].horEstado = 1;
        }
        this._horarioExtraService.guardarListaHorarioExtra(horExtra).subscribe(
            (respuesta:any) =>{
                this.spinner.hide();
                this.obtenerTodosHorarioPorCursoExtra();
                Swal.fire(`Horarios grabadas!`,'','success');
            },
            (error:any)=>{
                this.spinner.hide();
                this.obtenerTodosHorarioPorCursoExtra();   
                Swal.fire('Ups! no se pudo guardar el horario','','error'); 
            }
        );
    }

    guardarListaHorarioExtra() {
        this.spinner.show();
        for (let i=0;i<this.listaHorarioExtra.length;i++) {
            let minutosInicio:number = Number(this.listaHorarioExtra[i].horHorInicio.toString().split(":")[1]);
            let horasInicio:number = Number(this.listaHorarioExtra[i].horHorInicio.toString().split(":")[0]);
            let minutosFin:number = Number(this.listaHorarioExtra[i].horHorFin.toString().split(":")[1]);
            let horasFin:number = Number(this.listaHorarioExtra[i].horHorFin.toString().split(":")[0]);
            let totalHorasInicio = horasInicio+(minutosInicio/60);
            let totalHorasFin = horasFin+(minutosFin/60);
            let numHoras = totalHorasFin - totalHorasInicio;
            numHoras = Math.round((numHoras + Number.EPSILON)*100)/100;
            for (let j=0; j<this.listaHorarioExtra[i].listaHorarioExtraDia.length;j++) {
                this.listaHorarioExtra[i].listaHorarioExtraDia[j].graCodigo = this.cursoExtraordinaria.graCodigo;
                this.listaHorarioExtra[i].listaHorarioExtraDia[j].insestCodigo = this.cursoExtraordinaria.insestCodigo;
                this.listaHorarioExtra[i].listaHorarioExtraDia[j].curexCodigo = this.cursoExtraordinaria.curextCodigo;
                this.listaHorarioExtra[i].listaHorarioExtraDia[j].cupaexCodigo = this.cursoExtraordinaria.cupaexCodigo;
                this.listaHorarioExtra[i].listaHorarioExtraDia[j].jorCodigo = this.cursoExtraordinaria.jorCodigo;
                this.listaHorarioExtra[i].listaHorarioExtraDia[j].horNroHoras = numHoras;
                this.listaHorarioExtra[i].listaHorarioExtraDia[j].horEstado = 1;
            }
        }
        this._horarioExtraService.guardarListaHorarioExtraDia(this.listaHorarioExtra).subscribe(
            (respuesta:any) =>{
                this.spinner.hide();
                this.obtenerTodosHorarioPorCursoExtra();
                Swal.fire(`Horarios grabadas!`,'','success');
            },
            (error:any)=>{
                this.spinner.hide();
                this.obtenerTodosHorarioPorCursoExtra();  
                Swal.fire('Ups! no se pudo guardar el horario','','error'); 
            }
        );
    }

    editarAsignaturaHorario(horarioExtraDia:HorarioExtraDia) {
        if (horarioExtraDia.editar=='EDITAR') {
            horarioExtraDia.editar='NO_EDITAR';
        } else if (horarioExtraDia.editar=='NO_EDITAR'){
            horarioExtraDia.editar='EDITAR';
        }
        
    }

    editarHorario(horarioExtra:HorarioExtra) {
        if (horarioExtra.editar=='EDITAR') {
            horarioExtra.editar='NO_EDITAR';
        } else if (horarioExtra.editar=='NO_EDITAR'){
            horarioExtra.editar='EDITAR';
        }   
    }

    validarHorario(horarioExtra:HorarioExtra){
        if (horarioExtra.horHorFin < horarioExtra.horHorInicio) {
            horarioExtra.horHorInicio = null;
            horarioExtra.horHorFin = null;
            Swal.fire('Ups! la hora fin no puede ser menor a la hora inicio','','warning');
            return;
        }

        if (horarioExtra.horHorFin == horarioExtra.horHorInicio) {
            horarioExtra.horHorInicio = null;
            horarioExtra.horHorFin = null;
            Swal.fire('Ups! la hora inicio no puede ser igual a la hora fin','','warning');
            return;
        }

        if (horarioExtra.horHorFin != null || horarioExtra.horHorFin != undefined) {
            horarioExtra.editar = 'NO_EDITAR';
        }
    }

    validarMallaSeleccionada(horaInicio:any, horaFin:any, horarioExtraDia:HorarioExtraDia) {

        if (horaFin < horaInicio) {
            horarioExtraDia.disextCodigo = null;
            Swal.fire('Ups! la hora fin no puede ser menor a la hora inicio','','warning');
            return;
        }

        if (horaFin == horaInicio) {
            horarioExtraDia.disextCodigo = null;
            Swal.fire('Ups! la hora inicio no puede ser igual a la hora fin','','warning');
            return;
        }

        let horarioExtraParametro:HorarioExtraParametro = {
            curexCodigo:this.cursoExtraordinaria.curextCodigo,
            diaCodigo:horarioExtraDia.diaCodigo,
            disextCodigo:horarioExtraDia.disextCodigo,
            insestCodigo:this.cursoExtraordinaria.insestCodigo,
            horHorInicio:horaInicio,
            horHorFin:horaFin
        };

        let buscarHorarioExtra:HorarioExtra[];
        
        this._horarioExtraService.obtenerListaHorarioExtraPorCurextDiaDisextHorario(horarioExtraParametro).subscribe(
            (respuesta:RespuestaHorarioExtraInterfaz) => {
                buscarHorarioExtra = respuesta.listado;
                if (buscarHorarioExtra.length == 0) {
                    if (horarioExtraDia.disextCodigo != null && horarioExtraDia.disextCodigo != undefined) {
                        let distributivo:MallaAsignaturaExtra = this.listaMallaAsignatura.find((distributivo)=>distributivo.disextCodigo==horarioExtraDia.disextCodigo);
                        let docent:Docente = this.listaDocentes.find((docent)=>docent.codPersona == distributivo.docCodigo);
                        if (docent != null && docent != undefined){
                            horarioExtraDia.nombreDocente = docent.nomPersona;
                            horarioExtraDia.editar = 'NO_EDITAR';
                        }
                        horarioExtraDia.descripcionAsigna = distributivo.asiDescripcion;
                    }
                } else  {
                    horarioExtraDia.disextCodigo = null;
                    Swal.fire('Ups! el harario ya esta ocupado en otro grado o paralelo','','warning');
                }
            },
            (error:any)=>{
                this.spinner.hide();    
                Swal.fire('Ups! ocurrió un error al obtener los docentes','','error'); 
            }
        );
        
    }

    volverHorarioCursoExtra() {
        this.router.navigate(['pages/horarioExtraordinario']);
    }
 
    applyFilter(event: Event) {
        const filterValue = (event.target as HTMLInputElement).value;
        this.dataSource.filter = filterValue.trim().toLowerCase();
         
        if (this.dataSource.paginator) {
            this.dataSource.paginator.firstPage();
        }
    }

}