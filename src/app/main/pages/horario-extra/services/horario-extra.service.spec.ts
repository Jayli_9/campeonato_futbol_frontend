import { TestBed } from '@angular/core/testing';
import { HorarioExtraService } from './horario-extra.service';

describe('HorarioExtraService', () => {
  let service: HorarioExtraService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(HorarioExtraService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});