import { RespuestaGeneralInterfaz } from "app/main/shared/interfaces/respuesta-general-interfaz";
import { HorarioExtra } from "./horario-extra";

export interface RespuestaHorarioExtraInterfaz extends RespuestaGeneralInterfaz {
    listado: HorarioExtra[];
    objeto: HorarioExtra;
}