import { Time } from "@angular/common";
import { HorarioExtraDia } from "./horario-extra-dia";
import { Dia } from "./dia";

export interface HorarioExtra {
    horHorInicio:Time;
    horHorFin:Time;
    editar?:string;
    listaHorarioExtraDia:HorarioExtraDia[];
    listaDia?:Dia[];
}