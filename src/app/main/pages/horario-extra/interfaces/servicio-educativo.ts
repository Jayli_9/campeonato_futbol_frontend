export interface ServicioEducativo {
    sereduCodigo:number;
    sereduDescripcion:string;
    sereduIntensivo:number;
    tipeduNombre?:string;
    modalidad?:string;
}