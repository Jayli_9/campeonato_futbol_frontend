export interface Especialidad {
    espCodigo: number;
    espDescripcion: string;
    espEstado: number;
    espFechaCreacion: Date;
    codigoFiguraProfesional: number;
}