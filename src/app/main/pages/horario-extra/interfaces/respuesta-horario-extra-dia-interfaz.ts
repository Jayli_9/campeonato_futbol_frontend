import { RespuestaGeneralInterfaz } from "app/main/shared/interfaces/respuesta-general-interfaz";
import { HorarioExtraDia } from "./horario-extra-dia";

export interface RespuestaHorarioExtraDiaInterfaz extends RespuestaGeneralInterfaz {
    listado: HorarioExtraDia[];
    objeto: HorarioExtraDia;
}