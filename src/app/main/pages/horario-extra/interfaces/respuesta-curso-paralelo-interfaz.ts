import { RespuestaGeneralInterfaz } from "app/main/shared/interfaces/respuesta-general-interfaz";
import { CursoParalelo } from "./curso-paralelo";

export interface RespuestaCursoParaleloInterfaz extends RespuestaGeneralInterfaz {
    listado: CursoParalelo[];
    objeto: CursoParalelo;
}