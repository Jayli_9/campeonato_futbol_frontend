export interface CursoExtraordinaria {
    curextCodigo: number;
    espCodigo: number;
    graCodigo: number;
    insestCodigo: number;
    jorCodigo: number;
    reanleCodigo: number;
    sereduCodigo: number;
    modCodigo: number;
    cupaexCodigo:number;
    curextEstado: number;
    tipoEducacion?:string;
    parCodigo:number;
    parDescripcion?:string;
    modalidad?:string;
    jornada?:string;
    servEducativo?: string;
    gradoModulo?: string;
    moduloCodigo?: number;
    especialidad?: string;
}