export interface Modulo {
    modCodigo: number;
    sereduCodigo: number;
    modDescripcion: string;
    modCodigoGradoInicio: number;
    modDescripcionGradoInicio:string;
    modCodigoGradoFin: number;
    modDescripcionGradoFin:string;
}