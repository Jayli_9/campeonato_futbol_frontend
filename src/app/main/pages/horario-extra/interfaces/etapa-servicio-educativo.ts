export interface EtapaServicioEducativo {
    etseedCodigo:number;
    etaCodigo:number;
    etseedEstado:number;
    etseedFechaInicio:Date;
    etseedFechaFin:Date;
    etseedNemonico:string;
    sereduCodigo:number;
}