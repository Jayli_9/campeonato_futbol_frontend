import { RespuestaGeneralInterfaz } from "app/main/shared/interfaces/respuesta-general-interfaz";
import { Institucion } from "./institucion";

export interface RespuestaInstitucionInterfaz extends RespuestaGeneralInterfaz {
    listado: Institucion[];
    objeto: Institucion;
}