export interface Dia {
    diaCodigo:number;
    diaDescripcion:string;
    diaNemonico:string;
    diaEstado:string;
}