import { RespuestaGeneralInterfaz } from "app/main/shared/interfaces/respuesta-general-interfaz";
import { Dia } from "./dia";

export interface RespuestaDiaInterfaz extends RespuestaGeneralInterfaz {
    listado: Dia[];
    objeto: Dia;
}