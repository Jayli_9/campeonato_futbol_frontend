export interface DistributivoExtra {
    disextCodigo:number;
    curextCodigo:number;
    cupaexCodigo:number;
    docCodigo:number;
    maasiexCodigo:number;
    espCodigo:number;
    disextEstado:number;
}