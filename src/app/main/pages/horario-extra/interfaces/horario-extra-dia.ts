import { Time } from "@angular/common";

export interface HorarioExtraDia {
    horCodigo:number;
    diaCodigo:number;
    disextCodigo:number;
    insestCodigo:number;
    curexCodigo:number;
    jorCodigo:number;
    graCodigo:number;
    horHorInicio:Time;
    horHorFin:Time;
    horNroHoras:number;
    horEstado:number;
    cupaexCodigo:number;
    nombreDocente?:string;
    descripcionAsigna?:string;
    editar?:string;
}