export interface Institucion {
    insCodigo: number;
    insDescripcion: string;
    insEstado: number;
}