import { Time } from "@angular/common";

export interface HorarioExtraParametro {
    diaCodigo?:number;
    curexCodigo:number;
    cupaexCodigo?:number;
    insestCodigo:number;
    disextCodigo?:number;
    horHorInicio?:Time;
    horHorFin?:Time;
}