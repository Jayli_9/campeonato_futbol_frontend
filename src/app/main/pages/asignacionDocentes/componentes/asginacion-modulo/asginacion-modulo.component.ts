import { Component, OnInit, ViewChildren, Input} from '@angular/core';
import { DocenteService } from "app/main/pages/asignacionDocentes/servicios/docente.service";
import { SeleccionDatosOfertaComponent } from "../seleccion-datos-oferta/seleccion-datos-oferta.component";
import {MatDialog} from '@angular/material/dialog';

//importacion de servicios
import { GieeService } from "app/main/pages/asignacionDocentes/servicios/giee.service";
import { OfertaService } from "app/main/pages/asignacionDocentes/servicios/oferta.service";
import { CatalogoService } from "app/main/pages/asignacionDocentes/servicios/catalogo.service";
import { AcademicoService } from "app/main/pages/asignacionDocentes/servicios/academico.service";

//importacion de interface
import { CatMallaExtra } from '../../interface/cat_mallaExtraordinaria';
import { CatServicioEducativo } from '../../interface/cat_servicioEducativo';
import { CatAsignatura } from '../../interface/cat_asignatura';
import { FormGroup, UntypedFormBuilder, FormControl, Validators } from '@angular/forms';
import { CatParalelo } from '../../interface/cat_paraleloExtra';
import { MatriculaService } from '../../servicios/matricula.service';
import { CatModulo } from '../../interface/acaModulo';
import { CatGradoEquivalencia } from '../../interface/acaGradoEquivalencia';
import Swal from 'sweetalert2';
import { CatAsignacionModalidad } from '../../interface/cat_asignacionModalidad';


@Component({
  selector: 'app-asginacion-modulo',
  templateUrl: './asginacion-modulo.component.html',
  styleUrls: ['./asginacion-modulo.component.scss']
})
export class AsginacionModuloComponent implements OnInit {

  //promesas
  especialidadPromise: Promise<CatMallaExtra[]>;

  @Input() modalId ;
  @Input() jorId;
  @Input() asigId;
  @Input() codEstableciminto;

  //Datos del componente padre
  @ViewChildren(SeleccionDatosOfertaComponent) padre: SeleccionDatosOfertaComponent;

  //paralelos
  listaParalelos;
  datosParalelos: CatParalelo[]=[];

  datosParalelosIntensivoModalidad;
  datosParalelosIntensivoServicio;
  datosParalelosModalidad;
  datosParalelosServicio;


  datoParalelo: number ;

  //modelo para guardar la asignacion
  asignacionModelo;
  estadoGuardado:number = 1;


   //listado de amie
   listaInstitucion;
   informacionSede;
   informacionInstitucion;
   codAmie;
   codInstitucion;
   inst;

   //listado de curex
  listaCursoExtraordinario;
  ofertasFiltradas=[];
  codCurex: CatMallaExtra[]=[];

  modal;
  jornada;
  tipoAsignacion;
  modulo1= false;
  modulo2= false;
  modulo3= false;

  //variables de malla 
    numberMalla;
    listaMalla;
    nombreMalla: CatMallaExtra[]=[];

  //variable asignatura
    listaAsignatura: CatAsignatura[]=[];
    nombreAsignatura: CatAsignatura[]=[];
    numberAsignatura: number=0;


  //varibles para formulario
  listaServicioEducativo: CatServicioEducativo[]=[];
  nombreServicio: CatServicioEducativo[]=[];
  numberServicioEducativo;
  serivicoEducativo;
  servicioModulo;

  //varibles para docentes
  listaDocentes: Object;
  nombreDocente;
  numeroIdentificacion;
  codigoDocente;
  cedulaDocente;


  //variables de los servicios
  listaServicioEdu;


  //variables establecimientos
  listaEstablecimientos;
  codEstablecimiento;


  //variables de equivalencia de servicio
  listaEquivalencia;
  intensividad=[];
  equivalenciasAcumuladasIntensivas: CatGradoEquivalencia[]=[];
  equivalenciasAcumuladas: CatGradoEquivalencia[]=[];


  listaServiciosEquivalencia;


  //variables de modulos
  listaModulos;
  moduloPorCodigoIntensivo: CatModulo[]=[];
  modulosPorCodigo: CatModulo[]=[];

  listaServiciosModulos;
  mmodCodigo;


  //variables de datos por servicio selecciondo
  listaDeOfertaModalidadIntensiva: CatModulo[]=[];
  ListaDeOfertaModalidad: CatModulo[]=[];
  listaDeOfertaServicioIntensiva: CatGradoEquivalencia[]=[];
  listaDeOfertaServicio: CatGradoEquivalencia[]=[];

  //variables de datos por servicio seleccionado Alfa
  listaDeOfertaModalidadIntensivaAlfa: CatModulo[]=[];
  ListaDeOfertaModalidadAlfa: CatModulo[]=[];
  listaDeOfertaServiciosIntensivaAlfa: CatGradoEquivalencia[]=[];
  listaDeOfertaServiciosAlfa: CatGradoEquivalencia[]=[];

  //sacarModaliodad
  modalidadServicio=[];
  curextCodigo;
  servicioEducativo;


  //sacarServicios
  servicioEquivalencia=[];


  //variables
  modalidad;


  //obtener id seleccionado
  seleccionadoId;
  check;


  //agregar datos
  seleccionOfertas=[];


  //malla y asignacionMalla
  listaAsigMalla;
  listaMallaExtra;


  //variable curext seleccionados
  curextArray=[];
  modulos=[];
  servicioDatos=[];


  //dato a guardar
  guardarDato;

  //acumularoGrados
  paralelosAcumulador=[];

  //acumuladorI
  acumuladorI=0;

  //ObjetodeAnioElectivo
  aniosElectivos;


  //datos de malla modulo
  listaModuloMalla=[];

  ofertaSemiPresencial=[];
  ofertaPresencial=[];


  //variable para guardar

  autorizadoGuardar: string = "B";


  //variable modulo
  listaModalidadEducacion;


  //variable de distributivo modulo
  datosAsignadosModulo: CatAsignacionModalidad[]=[];

  buscarDocente() {
    this.servicioDocente.getDocenteCedula(this.cedulaDocente).then(data => {
      this.listaDocentes = data.objeto;
      if(this.listaDocentes!=null || this.listaDocentes!=undefined){

        for (let dato in this.listaDocentes){

          if(this.listaDocentes['stsEstadoAccPersona']=="A"){
            this.nombreDocente = this.listaDocentes['nomPersona'];
            this.numeroIdentificacion = this.listaDocentes['numIdentificacion'];
            this.codigoDocente = this.listaDocentes['codPersona'];
          }
        }

        Swal.fire({
          icon: 'success',
          title: 'Atención!',
          text: 'Docente encontrado y cargado!'
        })

      }else{

        Swal.fire({
          icon: 'error',
          title: 'Atención!',
          text: 'Cedula no corresponde a ningún Docente!'
        })

      }

    }, error => {
      alert(error);
    })
  }

  onCambioServicio(event){

    this.paralelosAcumulador=[];
    this.curextArray=[];
    this.modulos=[];

    this.datosParalelosIntensivoModalidad=[];
    this.datosParalelosModalidad=[];
    this.datosParalelosIntensivoServicio=[];
    this.datosParalelosServicio=[];
    
    this.datosParalelos=[];
    
    this.listaDeOfertaModalidadIntensivaAlfa=[];
    this.ListaDeOfertaModalidadAlfa=[];

    this.listaDeOfertaServiciosIntensivaAlfa=[];
    this.listaDeOfertaServiciosAlfa=[];

    this.listaDeOfertaModalidadIntensiva=[];
    this.ListaDeOfertaModalidad=[];

    this.listaDeOfertaServicio=[];
    this.listaDeOfertaServicioIntensiva=[];

    this.seleccionOfertas=[];


    this.modulosPorCodigo = this.modulosPorCodigo.filter((item, index) => (this.modulosPorCodigo.indexOf(item) === index));
    this.moduloPorCodigoIntensivo = this.moduloPorCodigoIntensivo.filter((item, index) => (this.moduloPorCodigoIntensivo.indexOf(item) === index));
    this.equivalenciasAcumuladas = this.equivalenciasAcumuladas.filter((item, index) => (this.equivalenciasAcumuladas.indexOf(item) === index));
    this.equivalenciasAcumuladasIntensivas = this.equivalenciasAcumuladasIntensivas.filter((item, index) => (this.equivalenciasAcumuladasIntensivas.indexOf(item) === index));



    if(event == 1){
      for(let i = 0; i < this.moduloPorCodigoIntensivo.length; i ++){
        if(this.moduloPorCodigoIntensivo[i].modCodigoGradoInicio==4){
            this.listaDeOfertaModalidadIntensivaAlfa.push(this.moduloPorCodigoIntensivo[i]);
            this.listaDeOfertaModalidadIntensivaAlfa.sort();
        }
      }

      for(let i = 0; i < this.modulosPorCodigo.length; i ++){
        if(this.modulosPorCodigo[i].modCodigoGradoInicio==4){
            this.ListaDeOfertaModalidadAlfa.push(this.modulosPorCodigo[i]);
            this.ListaDeOfertaModalidadAlfa.sort();
        }
    }

      for(let i = 0; i< this.equivalenciasAcumuladasIntensivas.length; i++){
        if(this.equivalenciasAcumuladasIntensivas[i].graequGradoInicio==4){
          this.listaDeOfertaServiciosIntensivaAlfa.push(this.equivalenciasAcumuladasIntensivas[i]);
          this.listaDeOfertaServiciosIntensivaAlfa.sort();
        }
      }

      for(let i = 0; i< this.equivalenciasAcumuladas.length; i++){
        if(this.equivalenciasAcumuladas[i].graequGradoInicio==4){
          this.listaDeOfertaServiciosAlfa.push(this.equivalenciasAcumuladas[i]);
          this.listaDeOfertaServiciosAlfa.sort();
        }
      }


    }else if(event == 2){

      for(let i = 0; i < this.moduloPorCodigoIntensivo.length; i ++){
        if(this.moduloPorCodigoIntensivo[i].modCodigoGradoInicio >= 5 && this.moduloPorCodigoIntensivo[i].modCodigoGradoInicio < 9){
            this.listaDeOfertaModalidadIntensiva.push(this.moduloPorCodigoIntensivo[i]);
            this.listaDeOfertaModalidadIntensiva.sort();
         }
      }

      for(let i = 0; i < this.modulosPorCodigo.length; i ++){
        if(this.modulosPorCodigo[i].modCodigoGradoInicio >= 5 && this.modulosPorCodigo[i].modCodigoGradoInicio < 9){
            this.ListaDeOfertaModalidad.push(this.modulosPorCodigo[i]);
            this.ListaDeOfertaModalidad.sort();
          }
      }

      for(let i = 0; i < this.equivalenciasAcumuladasIntensivas.length; i ++){
        if(this.equivalenciasAcumuladasIntensivas[i].graequGradoInicio >= 5 && this.equivalenciasAcumuladasIntensivas[i].graequGradoInicio < 9){
          this.listaDeOfertaServicioIntensiva.push(this.equivalenciasAcumuladasIntensivas[i]);
          this.listaDeOfertaServicioIntensiva.sort();
        }
      }

      for(let i = 0; i < this.equivalenciasAcumuladas.length; i ++){
        if(this.equivalenciasAcumuladas[i].graequGradoInicio >= 5 && this.equivalenciasAcumuladas[i].graequGradoInicio < 9){
          this.listaDeOfertaServicio.push(this.equivalenciasAcumuladas[i]);
          this.listaDeOfertaServicio.sort();
        }
      }

    }

  }


  agregar(cod){

    for(let i = 0; i < this.modalidadServicio.length; i++){

      if(cod==this.modalidadServicio[i].Modulo){

        let curext=this.modalidadServicio[i].curext;

        let modu = this.modalidadServicio[i].Modulo;

        if(this.intensividad[i]==this.modalidadServicio[i].servicio){
          this.obtenerParalelosIntensivoModulo(curext, cod, modu);
        }else{
          this.obtenerParalelosModulos(curext, cod, modu);
        }

      }

    }

    for(let i = 0; i < this.servicioEquivalencia.length; i ++){

      if(cod==this.servicioEquivalencia[i].servicio){

        let curext = this.servicioEquivalencia[i].curext;

        this.curextArray.push(curext);

        if(this.intensividad[i]==this.servicioEquivalencia[i].servicio){
          this.obtenerParalelosIntensivoServicio(curext, cod);
        }else{
          this.obtenerParalelosServicios(curext, cod);
        }

      }

    }

    this.seleccionOfertas.push(cod);
  }

  quitar(cod){

    for(let i = 0; i < this.modalidadServicio.length; i++){

      if(cod==this.modalidadServicio[i].Modulo){

        let curext=this.modalidadServicio[i].curext;

        let modu = this.modalidadServicio[i].Modulo;

        this.modulos.splice(this.modulos.indexOf(modu), 1);

        this.curextArray.splice(this.curextArray.indexOf(curext), 1);

        if(this.intensividad[i]==this.modalidadServicio[i].servicio){
          this.quitarParaleloIntensivoModulo(curext, cod, modu);
        }else{
          this.quitarParaleloModulos(curext, cod, modu);
        }

      }
    }

    for(let i = 0; i < this.servicioEquivalencia.length; i ++){

      if(cod==this.servicioEquivalencia[i].servicio){

        let curext = this.servicioEquivalencia[i].curext;

        this.curextArray.splice(this.curextArray.indexOf(curext), 1);

        if(this.intensividad[i]==this.servicioEquivalencia[i].servicio){
          this.quitarParaleloIntensivoServicio(curext, cod);
        }else{
          this.quitarParaleloServicio(curext, cod);
        }

      }

    }
    this.seleccionOfertas.splice(this.seleccionOfertas.indexOf(cod), 1);
  }


  onCambioModalidad(event){
    for(let i = 0; i < this.modalidadServicio.length; i++){
      if(this.modalidadServicio[i].Modulo==event){
        this.curextCodigo=this.modalidadServicio[i].curext;
        this.servicioEducativo=this.modalidadServicio[i].servicio;
      }
    }
  }


  constructor
  (
    private servicioDocente: DocenteService,
    private GieeService: GieeService,
    private OfertaService: OfertaService,
    private CatalogoService: CatalogoService,
    private AcademicoService: AcademicoService,
    private formBuilder: UntypedFormBuilder,
    private matricula: MatriculaService
  ) {
   }

  ngOnInit() {

    this.AcademicoService.asignacionesDistributivasModulo().then(data => {
      this.datosAsignadosModulo=data.listado;
    }, error => {
      alert(error);
    })

    this.modalidadServicio=[];

    this.listaDeOfertaModalidadIntensivaAlfa=[];
    this.ListaDeOfertaModalidadAlfa=[];

    this.listaDeOfertaServicio=[];
    this.listaDeOfertaServicioIntensiva=[];

    this.listaDeOfertaModalidadIntensiva=[];
    this.ListaDeOfertaModalidad=[];

    this.listaDeOfertaServiciosIntensivaAlfa=[];
    this.listaDeOfertaServiciosAlfa=[];


    this.listaInstitucion=JSON.parse(localStorage.getItem('currentUser'));
    this.informacionSede=this.listaInstitucion.sede;
    this.codAmie=this.informacionSede.nemonico;
    this.obtenerInstitucion();

    //busca curso Extraordinario
    this.OfertaService.getCursoExtraordinario(this.codEstableciminto).then(data => {
      this.listaCursoExtraordinario=data.listado;
      this.listaCursoExtraordinario.forEach(element => {

        if(element.curextCuposDisponibles>0){
          if(element.jorCodigo == this.jorId){
            if(element.graCodigo == 0){

              this.CatalogoService.getElectivoSegunAnioPorCodigo(element.reanleCodigo).then(data => {
                this.aniosElectivos=data.objeto;
                if(this.aniosElectivos.reanleTipo == "A"){

                  this.CatalogoService.getServicioEducativoPorCodigo(element.sereduCodigo).then(data => {
                    this.listaServicioEdu=data.objeto;


                    this.CatalogoService.getListaModalidadEducacion(this.listaServicioEdu.motiedCodigo).then(data => {
                      this.listaModalidadEducacion= data.objeto;

                      if(this.modalId == 2 ){

                        if(this.listaModalidadEducacion.modCodigo == 2){
                          this.ofertaPresencial.push(this.listaServicioEdu.sereduCodigo);
  
                          if(this.listaModalidadEducacion.temCodigo == 1){
                            this.intensividad.push(this.listaServicioEdu.sereduCodigo);
                        }
  
                        this.ofertasFiltradas.push(element);
                      }
  
  
                      }else if(this.modalId == 3){
                        if(this.listaModalidadEducacion.modCodigo == 3){
                          this.ofertaSemiPresencial.push(this.listaServicioEdu.sereduCodigo);
  
                          if(this.listaServicioEdu.sereduIntensivo==1){
                            this.intensividad.push(this.listaServicioEdu.sereduCodigo);
                        }
  
                        this.ofertasFiltradas.push(element);
                      }
                      }



                    })


                    

                    }, error => {
                      alert(error);
                   });
                }
              })

            }
          }
        }
      });

    }, error => {
  
    alert(error);
    });

    setTimeout(() => {
      this.obtenerEquivalenciaServicios(this.ofertasFiltradas);
    }, 1000);

    setTimeout(() => {
      this.obtenerModulos(this.ofertasFiltradas);
    }, 1000);
    

  }


  obtenerEquivalenciaServicios(ofertas){

    setTimeout(()=>{

      for(let i = 0; i < ofertas.length; i++){
        if(ofertas[i].modCodigo == 0){
      
          this.obtenerEquivalenciaPorServicio(ofertas[i].sereduCodigo, this.ofertaSemiPresencial, this.ofertaPresencial, this.intensividad);
  
          this.listaServiciosEquivalencia={
            servicio: ofertas[i].sereduCodigo,
            curext: ofertas[i].curextCodigo
          };
  
          this.servicioEquivalencia.push(this.listaServiciosEquivalencia);
  
        }
      }

    }, 800);

  }

  obtenerModulos(ofertas){

    setTimeout(()=>{

      for(let i = 0; i < ofertas.length; i++){
        if(ofertas[i].modCodigo != 0){
      
          this.obternerModuloPorCodigo(ofertas[i].modCodigo, this.ofertaSemiPresencial, this.ofertaPresencial, this.intensividad);
  
          this.listaServiciosModulos={
            Modulo: ofertas[i].modCodigo,
            servicio: ofertas[i].sereduCodigo,
            curext: ofertas[i].curextCodigo
           };
  
           this.modalidadServicio.push(this.listaServiciosModulos);
        }
      }

    }, 800)

  }


  obternerModuloPorCodigo(cod, ofertaSemi, ofertaPresencial, intensividad){

    this.matricula.getModuloPorCodigo(cod).then(data => {
      this.listaModulos = data.objeto;

        if(this.modalId==3){
            for(let a = 0; a < ofertaSemi.length; a ++){
                if(ofertaSemi[a] == this.listaModulos.sereduCodigo){

                  if(intensividad.length > 0){

                    for(let i = 0; i < intensividad.length; i++){

                      if(intensividad[i] == this.listaModulos.sereduCodigo){
                        this.moduloPorCodigoIntensivo.push(this.listaModulos);
                      }else{
                        this.modulosPorCodigo.push(this.listaModulos);
                      }

                    }
                  }else{
                    this.modulosPorCodigo.push(this.listaModulos);
                  }
                }
              }

        }else if(this.modalId==2){


            for(let a = 0; a < ofertaPresencial.length; a ++){
                if(ofertaPresencial[a] == this.listaModulos.sereduCodigo){

                  if(intensividad.length > 0){

                    for(let i = 0; i < intensividad.length; i++){

                      if(intensividad[i] == this.listaModulos.sereduCodigo){
                        this.moduloPorCodigoIntensivo.push(this.listaModulos);
                      }else{
                        this.modulosPorCodigo.push(this.listaModulos);
                      }

                    }

                  }else{
                    this.modulosPorCodigo.push(this.listaModulos);
                  }
  
                }
              }

        }
    }, error => {
      alert(error);
    });

  }


  obtenerEquivalenciaPorServicio(cod, oferSemi, ofertaPresencial, intensivo){
    this.CatalogoService.getEquivalenciaPorServicioEducativo(cod).then(data => {
        this.listaEquivalencia=data.listado;
        this.listaEquivalencia.forEach(element => {

          if(this.modalId==3){
              for(let a = 0; a < oferSemi.length; a ++){
                  if(oferSemi[a] == element.sereduCodigo){

                    if(intensivo.length > 0){

                      for(let i = 0; i < intensivo.length; i++){
                        if(intensivo[i]==element.sereduCodigo){                    
                          this.equivalenciasAcumuladasIntensivas.push(element);
                      }else{
                          this.equivalenciasAcumuladas.push(element);
                      }
                      }

                    }else{
                      this.equivalenciasAcumuladas.push(element);
                    }


                  }
                }

          }else if(this.modalId==2){

              for(let a = 0; a < ofertaPresencial.length; a ++){
                  if(ofertaPresencial[a] == element.sereduCodigo){

                    if(intensivo.length>0){

                      for(let i = 0; i < intensivo.length; i++){

                        if(intensivo[i]==element.sereduCodigo){
                          this.equivalenciasAcumuladasIntensivas.push(element);
                        }else{
                          this.equivalenciasAcumuladas.push(element);
                        }

                      }

                    }

                  }
              }

          }
        })
    })
  }
  

  //obtener Asignatura Extraordinaria


  //obtener Institucion Extraordinaria

  obtenerInstitucion(){
    //buscar la institucion
    this.GieeService.getDatosInstitucion(this.codAmie).then(data => {
      this.informacionInstitucion=data.objeto;
      for(let dato in this.informacionInstitucion){
        this.codInstitucion=this.informacionInstitucion.insCodigo;
      }
      
    }, error => {
      alert(error);
      alert("Usuario sin sede");
    })
  }



obtenerEspecialidad(cod){
  this.numberAsignatura=0;
  if(this.numberAsignatura==cod){
  }else{
    this.numberAsignatura= cod;
    this.AcademicoService.getAsignaturaCodigo(this.numberAsignatura).then(data => {
      this.listaAsignatura.push(data.objeto);
    }, error  => {
      alert(error);
    }); 
  }
}

asginar(){

  if(this.modalId==null || this.jorId==null || this.asigId==null || this.serivicoEducativo==null || this.numeroIdentificacion==null || this.nombreDocente==null){
    Swal.fire({
      icon: 'error',
      title: 'Alerta!',
      text: 'Todos los datos deben ser llenados!'
    })

  }else{

    let datosLimpios = this.paralelosAcumulador.filter((item, index) => (this.paralelosAcumulador.indexOf(item) === index));

    for(let i = 0; i < datosLimpios.length; i++){

      this.verificacionGuardado(datosLimpios[i], this.datosAsignadosModulo);

      setTimeout(() => {
        this.AcademicoService.getMallaModalidad(datosLimpios[i].curext).then(data => {
          this.listaAsigMalla=data.listado;
          if(this.listaAsigMalla.length!=0){
  
            this.listaAsigMalla.forEach(element => {
              this.mmodCodigo = element.mmodCodigo;
            });
  
            setTimeout(() => {
              if(this.autorizadoGuardar=="A"){
  
                let objetoAsignacion={
                  "mmodCodigo": this.mmodCodigo,
                  "curextCodigo": datosLimpios[i].curext,
                  "cupaextCodigo": datosLimpios[i].cupaext,
                  "modCodigo": datosLimpios[i].modulo,
                  "docCodigo": parseInt(this.codigoDocente),
                  "dismoEstado": 1
                }
      
                this.AcademicoService.guardarAsignacionModulo(objetoAsignacion).subscribe({
                  next: (Response) => {
      
                    if(i < this.curextArray.length){
                      Swal.fire({
                        title: 'Docente asignado',
                        text: "Datos registrados y guardados",
                        icon: 'success',
                        confirmButtonColor:  '#3085d6',
                        confirmButtonText: 'ok'
                      }).then((result) => {
                        if(result.isConfirmed){
                          location.reload();
                        }
                      })
                    }
      
                    },
                    error: (error) =>{
      
                      Swal.fire({
                        icon: 'error',
                        title: 'Atención!',
                        text: 'Docente no asignado'
                      })
    
                    }
                  })
              }else{
                Swal.fire({
                  icon: 'error',
                  title: 'Atención!',
                  text: 'Oferta ya asignada'
                })
              }
            }, 1000);
            
  
          }else{
            Swal.fire({
              icon: 'error',
              title: 'Atención!',
              text: 'Oferta no tiene una malla asignada'
            })
          }
        })
      }, 1000);


      

    }
  }

 }


 
 verificacionGuardado(datosLimpios, data){

  if(data.length==0){
    this.autorizadoGuardar="A";
    }else{

      data.forEach(element => {
        if(element.cupaextCodigo == datosLimpios.cupaext && element.curextCodigo == datosLimpios.curext){
          this.autorizadoGuardar="B";
        }else{
          this.autorizadoGuardar="A";
        }
    })

    }
 }

 quitarParaleloIntensivoModulo(cod, codigoModulo, modu){
  this.OfertaService.getParaleloExtra(cod).then(data => {
    if(data.listado.length == 0){
    }else{
      this.listaParalelos = data.listado;
      this.listaParalelos.forEach(element => {

        let datos={
          codDiferencia : codigoModulo,
          curext: cod,
          datosParalelo: element,
          modulo: modu
        }

        this.datosParalelosIntensivoModalidad.splice(this.datosParalelosIntensivoModalidad.indexOf(datos), 1);
      });
    }
  }, error => {
    alert(error);
  });
 }


 agregarCurso(curext){

  this.AcademicoService.getMallaModalidad(curext).then(data => {

    let datosMalla = data.listado;
    datosMalla.forEach(element => {
      this.listaModuloMalla.push(element);
    })

  }, error =>{
    alert(error);
  })

 }

 quitarCurso(curext){
  
   this.AcademicoService.getMallaModalidad(curext).then(data => {
    let datosMalla = data.listado;
    datosMalla.forEach(element => {
      this.listaModuloMalla.splice(this.listaModuloMalla.indexOf(element), 1);
    });
   })
   
 }


 agregarMalla(cod, curext, modulo, malla){

  let acumuladorDatos ={
    cupaext: cod,
    curext: curext,
    modulo: modulo,
    malla: malla
  }

  this.paralelosAcumulador.push(acumuladorDatos);
 }


 quitarMalla(cod, curext, modulo, malla){
  let acumuladorDatos ={
    cupaext: cod,
    curext: curext,
    modulo: modulo,
    malla: malla
  }

  this.paralelosAcumulador.splice(this.paralelosAcumulador.indexOf(acumuladorDatos), 1);
 }

 quitarParaleloIntensivoServicio(cod, codigoModulo){
  this.OfertaService.getParaleloExtra(cod).then(data => {
    if(data.listado.length == 0){
    }else{
      this.listaParalelos = data.listado;
      this.listaParalelos.forEach(element => {

        let datos={
          codDiferencia : codigoModulo,
          curext: cod,
          datosParalelo: element,
          modulo: 0
        }

        this.datosParalelosIntensivoServicio.splice(this.datosParalelosIntensivoServicio.indexOf(datos), 1);

      });
    }
  }, error => {
    alert(error);
  });
 }

 quitarParaleloModulos(cod, codigoModulo, modu){
  this.OfertaService.getParaleloExtra(cod).then(data => {
    if(data.listado.length == 0){
    }else{
      this.listaParalelos = data.listado;
      this.listaParalelos.forEach(element => {

        let datos={
          codDiferencia : codigoModulo,
          curext: cod,
          datosParalelo: element,
          modulo: modu
        }


        this.datosParalelosModalidad.splice(this.datosParalelosModalidad.indexOf(datos), 1);

      });
    }
  }, error => {
    alert(error);
  });
 }


 quitarParaleloServicio(cod, codigoModulo){
  this.OfertaService.getParaleloExtra(cod).then(data => {
    if(data.listado.length == 0){
    }else{
      this.listaParalelos = data.listado;
      this.listaParalelos.forEach(element => {

        let datos={
          codDiferencia : codigoModulo,
          curext: cod,
          datosParalelo: element,
          modulo: 0
        }

        this.datosParalelosServicio.splice(this.datosParalelosServicio.indexOf(datos), 1);

      });
    }
  }, error => {
    alert(error);
  });
 }

  obtenerParalelosIntensivoModulo(cod, codigoModulo, modu){

    this.OfertaService.getParaleloExtra(cod).then(data => {
      if(data.listado.length == 0){
      }else{
        this.listaParalelos = data.listado;
        this.listaParalelos.forEach(element => {

          let datos={
            codDiferencia : codigoModulo,
            curext: cod,
            datosParalelo: element,
            modulo: modu
          }
          this.datosParalelosIntensivoModalidad.push(datos);

          this.datosParalelosIntensivoModalidad = this.datosParalelosIntensivoModalidad.filter((item, index) => (this.datosParalelosIntensivoModalidad.indexOf(item) === index));

        });
      }
    }, error => {
      alert(error);
    });

  }

  obtenerParalelosIntensivoServicio(cod, codigoModulo){
    this.OfertaService.getParaleloExtra(cod).then(data => {
      if(data.listado.length == 0){
      }else{
        this.listaParalelos = data.listado;
        this.listaParalelos.forEach(element => {

          let datos = {
            codDiferencia : codigoModulo,
            curext: cod,
            datosParalelo: element,
            modulo: 0
          }
          this.datosParalelosIntensivoServicio.push(datos);

          this.datosParalelosIntensivoServicio = this.datosParalelosIntensivoServicio.filter((item, index) => (this.datosParalelosIntensivoServicio.indexOf(item) === index));


        });
      }
    }, error => {
      alert(error);
    });
  }

  obtenerParalelosModulos(cod, codigoModulo, modu){

    this.OfertaService.getParaleloExtra(cod).then(data => {
      if(data.listado.length == 0){
      }else{
        this.listaParalelos = data.listado;
        this.listaParalelos.forEach(element => {

          let datos = {
            codDiferencia : codigoModulo,
            curext: cod,
            datosParalelo: element,
            modulo: modu
          }
          this.datosParalelosModalidad.push(datos);

          this.datosParalelosModalidad = this.datosParalelosModalidad.filter((item, index) => (this.datosParalelosModalidad.indexOf(item) === index));

        });
      }
    }, error => {
      alert(error);
    });


  }

  obtenerParalelosServicios(cod, codigoModulo){

    this.OfertaService.getParaleloExtra(cod).then(data => {
      if(data.listado.length == 0){
      }else{
        this.listaParalelos = data.listado;
        this.listaParalelos.forEach(element => {

          let datos = {
            codDiferencia : codigoModulo,
            curext: cod,
            datosParalelo: element,
            modulo: 0
          }

          this.datosParalelosServicio.push(datos);

          this.datosParalelosServicio = this.datosParalelosServicio.filter((item, index) => (this.datosParalelosServicio.indexOf(item) === index));
        });
      }
    }, error => {
      alert(error);
    });


  }


  //formularios

  validateFormat(event) {
    let key;
    if (event.type === 'paste') {
      key = event.clipboardData.getData('text/plain');
    } else {
      key = event.keyCode;
      key = String.fromCharCode(key);
    }
    const regex = /[0-9]|\./;
     if (!regex.test(key)) {
      event.returnValue = false;
       if (event.preventDefault) {
        event.preventDefault();
       }
     }
    }





}
