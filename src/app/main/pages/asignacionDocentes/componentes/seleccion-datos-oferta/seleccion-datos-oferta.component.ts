import { Component, OnInit, Output } from '@angular/core';
import { Router } from '@angular/router';
import { CatalogoService } from "app/main/pages/asignacionDocentes/servicios/catalogo.service";
import { OfertaService } from "app/main/pages/asignacionDocentes/servicios/oferta.service";
import { GieeService } from "app/main/pages/asignacionDocentes/servicios/giee.service";
import { MatriculaService } from "app/main/pages/asignacionDocentes/servicios/matricula.service";
import { CatModalidad } from '../../interface/cat_modalidad';
import { CatJoranada } from '../../interface/cat_jornada';
import { CatModu } from '../../interface/cat_modulo';
import { CatServicioEducativo } from '../../interface/cat_servicioEducativo';

@Component({
  selector: 'app-seleccion-datos-oferta',
  templateUrl: './seleccion-datos-oferta.component.html',
  styleUrls: ['./seleccion-datos-oferta.component.scss']
})
export class SeleccionDatosOfertaComponent implements OnInit {
  public contentHeader: object;
  //datos del regimen
  listaRegimen;
  listaAnio;
  anioElectivo;
  codgiRegimen;
  regimen;

  @Output() modalId ;
  @Output() jorId;
  @Output() asigId;
  @Output() codEstableciminto;

  //servicio Educativo
  codigosServicioEducativo;

  //lista de variables modulo
  numberModulos;
  listaModulos;
  datosModulos: CatModu[]=[];
  
  //listado de amie
  listaInstitucion;
  informacionSede;
  informacionInstitucion;
  codAmie;
  codInstitucion;
  inst;

  //listado de curex
  listaCursoExtraordinario;

  //Ejemplo lista de jornada
  objetoJornada: CatJoranada[]=[];
  todasJornadas: CatJoranada[]=[];
  codJornada: 0;
  jornada: 1;
  listaJornada;
  listaJornadaPresencial;
  ListaJornadaSemiPresencial;
  nombreJornada: CatJoranada[]=[];
  jornadasPresenciales: CatJoranada[]=[];
  jornadasSemiPresenciales: CatJoranada[]=[];
  jornada2: CatJoranada[]=[];
  
  acumuladorPresencial=[];
  acumuladorSemiPresencial=[];

  //ejemplo listado de modalidad
  codModalidad: 0;
  listaModalidad;
  nombreModalidad: CatModalidad[]=[];
  listaDatosModalidad;
  numberModalidad=null;
  numberJornada=null;


  //modalidad
  modalidad: number;

  //asignatura
  asignatura;

  //tipo de asignacion
  asignaciones;


  //variables de establecimiento
  informacionEstablecimiento;
  codEstablecimiento;



  //establecimiento
  establecimiento;
  listaEstablecimiento
  codInstitucionObtenida;
  nombreInstitucion


  //acumulador de servicio educativos nivel permiso
  nivelesEducativosCodServicio=[];
  acumuladorMotied=[];
  acumuladorModCodigo=[];
  acumuladorModalidad=[];
  limpioModalidad=[];


  //datos para modulo y por curso
  aniosElectivos;
  servicioModulo;
  numberServicioEducativo;
  //datos 
  numberCurso;
  //varibles para formulario
  listaServicioEducativo: CatServicioEducativo[]=[];
  nombreServicio: CatServicioEducativo[]=[];
  listaServicioEdu;
  listaModalidadEducacion;


  //codigo Ins
  insCodigo;

  moduloPremisa=1;
  gradoPremisa=1;


  onCambioDeModalidad(event){
    this.modalidad=event;
    this.jornada=null
    this.modalId=event;
    this.asignaciones=null;
  }

  onCambioAsginacion(event){
    this.asigId=event;
  }


  onCambioJornada(event){
    this.jorId=event;
    this.asignaciones=null;

    this.OfertaService.getCursoExtraordinario(this.codEstableciminto).then(data => {
      this.listaCursoExtraordinario=data.listado;
      this.listaCursoExtraordinario.forEach(element => {        
        if(element.curextCuposDisponibles>0){
          if(element.jorCodigo == event){
              this.CatalogoService.getElectivoSegunAnioPorCodigo(element.reanleCodigo).then(data => {
                this.aniosElectivos=data.objeto;
                if(this.aniosElectivos.reanleTipo == "A"){

                  //console.log(element);

                  setTimeout(() => {
                    if(element.modCodigo != 0){
                      this.moduloPremisa=0;
                    }
  
                    if(element.graCodigo != 0){
                      this.gradoPremisa=0;
                    }
                  }, 1000);
                  
                }
              })
          }
        }
      });
    }, error => {
  
    alert(error);
    });

  }

  constructor(
    private _router: Router,
    private CatalogoService: CatalogoService,
    private OfertaService: OfertaService,
    private GieeService: GieeService,
    private MatriculaService: MatriculaService,) { }

  ngOnInit() {

    this.contentHeader = {
      headerTitle: 'Asignación Docentes', 
      actionButton: false,
      breadcrumb: {
          type: '',
          links: [
              {
                  name: 'Inicio',
                  isLink: true,
                  link: '/pages/inicio'
              },
              {
                  name: 'Asignación Docente',
                  isLink: false
              },
          ]
      }
  };



    //servicio implementacion y codigo de amie

    //asignacion codigo amie
    this.listaInstitucion=JSON.parse(localStorage.getItem('currentUser'));
    this.informacionSede=this.listaInstitucion.sede;
    this.nombreInstitucion=this.informacionSede.descripcion;
    this.codAmie=this.informacionSede.nemonico;

    //buscar la institucion y regimen
    this.buscarRegiment(this.codAmie);

    //buscar Establecimiento de la institucion
    this.obtenerEstablecimiento(this.codAmie);


    setTimeout(() => {
      this.sacarPermisoInstitucion(this.insCodigo);
    }, 1000);

    //servicio implementado en modalidad


       //listado de modalidades
       this.CatalogoService.getModalidades().then(data => {
        this.listaModalidad = data.listado;
       }, error => {
        alert(error);
       })

       //comprobar Oferta por curso o por modulo existente


  }


  //sacar los establecimientos de la institucion
  obtenerEstablecimiento(cod){
    this.GieeService.getEstablecimientoPorAmie(cod).then(data => {
      this.informacionEstablecimiento=data.listado;
      for(let i = 0; i < this.informacionEstablecimiento.length; i++){
        let agrupacionInstitucion = this.informacionEstablecimiento[i].insInstitucion;
        this.insCodigo = agrupacionInstitucion.insCodigo;
      }
     
  })
  }

  
  sacarPermisoInstitucion(cod){
    this.GieeService.getPermisoNivelInstitucionPorInsCodigo(cod).then(data => {
      let listadoIntituciones = data.listado;
      for(let i = 0; i < listadoIntituciones.length; i++){
        this.nivelesEducativosCodServicio.push(listadoIntituciones[i].sereduCodigo)
      }

      let LimpiezaCodigoServiciosNivel = Array.from(new Set(this.nivelesEducativosCodServicio));
      setTimeout(() => {
        this.sacarServiciosEducativosInstituciones(LimpiezaCodigoServiciosNivel);
      }, 1000);

    })

  }


  sacarServiciosEducativosInstituciones(acumulador){
    for(let i = 0; i < acumulador.length; i++){
      this.CatalogoService.getServicioEducativoPorCodigo(acumulador[i]).then(data => {
        let objetoServiciosEducativos = data.objeto;

        this.CatalogoService.getElectivoSegunAnioPorCodigo(objetoServiciosEducativos.reanleCodigo).then(data => {
          let objetosAnioElectivo = data.objeto;

          if(objetosAnioElectivo.reanleTipo=="A"){
            this.acumuladorMotied.push(objetoServiciosEducativos.motiedCodigo);
          }

        })
      })
    }
    
    setTimeout(() => {
      this.sacarModalidadesMotied(this.acumuladorMotied);
    }, 1000);

    
  }


  sacarModalidadesMotied(codigosMotied){
    let LimpiezaMotied = Array.from(new Set(codigosMotied));
    for(let i = 0; i < LimpiezaMotied.length; i++){
      this.CatalogoService.buscarIdModalidad(LimpiezaMotied[i]).then(data => {
        let objetoMotied = data.objeto;
        this.acumuladorModCodigo.push(objetoMotied.modCodigo);
      })
    }

    

    setTimeout(() => {
      this.buscarModCodigo(this.acumuladorModCodigo);
    }, 1000);
  }


  buscarModCodigo(modusCodigo){
    let LimpiezaMod = Array.from(new Set(modusCodigo));
    for(let i = 0; i < LimpiezaMod.length; i++){
      this.CatalogoService.getModalidadesCodigo(LimpiezaMod[i]).then(data => {
        let datosModalidad = data.objeto;
        this.acumuladorModalidad.push(datosModalidad);
      })
    }

    setTimeout(() => {
      this.limpioModalidad = Array.from(new Set(this.acumuladorModalidad));
    }, 1000);
  }



  onCambioEstablecimiento(event){
    this.moduloPremisa=1;
    this.gradoPremisa=1;
    this.codEstableciminto=event;
    this.jornadasPresenciales=[];
    this.jornadasSemiPresenciales=[];
    this.modalidad=0;
    this.jornada=null
    this.modalId=null;
    this.asignaciones=null;
    this.obtenerCurso(event);

  }


  //obtener los cursos de los establecimiento
  obtenerCurso(cod){
    this.acumuladorPresencial=[];
    this.acumuladorSemiPresencial=[];
    //busca curso Extraordinario
    this.OfertaService.getCursoExtraordinario(cod).then(data => {
      this.listaCursoExtraordinario=data.listado;

      this.listaCursoExtraordinario.forEach(element => {



        if(element.curextCuposDisponibles>0){
          if(element.jorCodigo == 1){
            this.acumuladorPresencial.push(1);
            this.acumuladorSemiPresencial.push(1);
          }else if(element.jorCodigo == 2){
            this.acumuladorSemiPresencial.push(2);
          }else if(element.jorCodigo == 3){
            this.acumuladorSemiPresencial.push(3);
            this.acumuladorPresencial.push(3);
          }
        }
      });




      this.jornadas(this.acumuladorPresencial, this.acumuladorSemiPresencial);

    }, error => {
  
    alert(error);
    })
    
  }


  //limpiar datos y sacar las jornadas
  jornadas(cod1, cod2){

    let limpioPresencial = Array.from(new Set(cod1));
    let LimpioSemiPresencial = Array.from(new Set(cod2));

    if(limpioPresencial.length > 0){

      for(let i = 0; i < limpioPresencial.length; i ++){
        this.CatalogoService.getJornadaPorCodigo(limpioPresencial[i]).then(data => {
          this.listaJornadaPresencial=data.objeto;
          this.jornadasPresenciales.push(this.listaJornadaPresencial);
        }, error => {
          alert(error);
        })
      }

    }


    if(LimpioSemiPresencial.length > 0){

      for(let i = 0; i < LimpioSemiPresencial.length; i ++){
        this.CatalogoService.getJornadaPorCodigo(LimpioSemiPresencial[i]).then(data => {
          this.ListaJornadaSemiPresencial=data.objeto;
          this.jornadasSemiPresenciales.push(this.ListaJornadaSemiPresencial);
        }, error => {
          alert(error);
        })
      }

    }

  }




  //buscar los datos de la institucion y regimen
  buscarRegiment(cod){
    //buscar la institucion
    this.GieeService.getDatosInstitucion(cod).then(data => {
      this.informacionInstitucion=data.objeto;
      
      for(let dato in this.informacionInstitucion){
        this.codInstitucion=this.informacionInstitucion.insCodigo;
        this.codgiRegimen=this.informacionInstitucion.regimenes[0].regCodigo;
      }

      this.CatalogoService.getRegimen(this.codgiRegimen).then(data => {
        this.listaRegimen = data.objeto;
        //exportar el objeto
        for(let dato in this.listaRegimen){
          this.regimen = this.listaRegimen.regDescripcion
        }
      })

      this.CatalogoService.getAnioElectivo(this.codgiRegimen).then(data => {
        this.listaAnio=data.listado;
        for(let dato in this.listaAnio){
          this.anioElectivo = this.listaAnio.descripcionAnioLectivo;
        }
      })

      //busca curso Extraordinario
    this.OfertaService.getCursoExtraordinario(this.codInstitucion).then(data => {
      this.listaCursoExtraordinario=data.listado;
      for (let i = 0; i < this.listaCursoExtraordinario.length; i ++){
        //servicio implementado en modalidad
        this.listaModalidad[i]=this.listaCursoExtraordinario[i]["sereduCodigo"];
      }

      for (let i = 0; i < this.listaCursoExtraordinario.length; i ++){
        //servicio implementado en jornada
        this.objetoJornada[i]=this.listaCursoExtraordinario[i]["jorCodigo"];
      }

      this.numberModalidad=0;
      this.numberJornada=0;

      //buscar datos modulo
      this.asignarModulo(this.listaModalidad);

    }, error => {
      alert(error);
    })
    }, error => {
      alert(error);
      alert("Usuario sin sede");
    })

  }

  asignarModalidades(cod){
    //sacar la lista de modalidades
    for (let i = 0; i < cod.length; i ++){
      if(cod[i]==this.numberModalidad){
      }else{
        this.numberModalidad==cod[i];
        //sacar modalidades del curso
       this.CatalogoService.getModalidadesCodigo(this.numberModalidad).then(data => {
        this.listaDatosModalidad=data.objeto;
          this.nombreModalidad.push(this.listaDatosModalidad);
       }, error => {
          alert(error);
       })
      }
    }
  }


  asignarModulo(cod){
    let limpioArray = Array.from(new Set(cod));
    this.numberModulos=0;
    //sacar la lista modulos
    for(let i = 0; i < limpioArray.length; i++){
        this.numberModulos==limpioArray[i];
        //sacar los datos o listado de datos
        this.MatriculaService.getmodulosPorServicio(this.numberModulos).then(data => {
          this.listaModulos=data.listado;
          this.listaModulos.forEach(element => {
            this.datosModulos.push(element);
          });
        }, error => {
          alert(error);
        });
    }
  }




}