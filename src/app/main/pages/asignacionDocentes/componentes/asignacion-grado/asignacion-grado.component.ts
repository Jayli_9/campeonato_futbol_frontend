import { Component, OnInit, Input, ViewChildren} from '@angular/core';
import { Router } from '@angular/router';
import { DocenteService } from "app/main/pages/asignacionDocentes/servicios/docente.service";
import { CatalogoService } from "app/main/pages/asignacionDocentes/servicios/catalogo.service";
import { AcademicoService } from "app/main/pages/asignacionDocentes/servicios/academico.service";

import { GieeService } from '../../servicios/giee.service';
import { OfertaService } from '../../servicios/oferta.service';
import { CatMallaExtra } from '../../interface/cat_mallaExtraordinaria';
import { CatServicioEducativo } from '../../interface/cat_servicioEducativo';
import { CatAsignatura } from '../../interface/cat_asignatura';
import { CatParalelo } from '../../interface/cat_paraleloExtra';
import { SeleccionDatosOfertaComponent } from '../seleccion-datos-oferta/seleccion-datos-oferta.component';
import { CatGradoEquivalencia } from '../../interface/acaGradoEquivalencia';
import { MatriculaService } from '../../servicios/matricula.service';
import { CatModulo } from '../../interface/acaModulo';
import { CatGradosCursos } from '../../interface/cat_gradosCursos';
import { CatMallaAsigExtra } from '../../interface/cat_mallaAsig';
import Swal from 'sweetalert2';
import { CatFusionMateria } from '../../interface/acaResultadoCurextCupaex';
import { CatAsignacion } from '../../interface/cat_asignacion';
import { modalidadTipoEducacion } from '../../interface/modalidad-tipo-educacion';

@Component({
  selector: 'app-asignacion-grado',
  templateUrl: './asignacion-grado.component.html',
  styleUrls: ['./asignacion-grado.component.scss']
})
export class AsignacionGradoComponent implements OnInit {

  @Input() modalId ;
  @Input() jorId;
  @Input() asigId;
  @Input() codEstableciminto;

  //Datos del componente padre
  @ViewChildren(SeleccionDatosOfertaComponent) padre: SeleccionDatosOfertaComponent;

  //variables para materias
  numberMateria: number;
  variableAsignar=[];

  //Asignar materia
  materiaAsignar;

  //Grado de institucion
  octavo;
  noveno;
  decimo;
  materiaOctavo;
  materiaNoveno;
  materiaDecimo;

   //listado de amie
   listaInstitucion;
   informacionSede;
   informacionInstitucion;
   codAmie;
   codInstitucion;
   inst;

  //varibles para formulario
  serivicoEducativo;

  //varibles para docentes
  listaDocentes;
  nombreDocente;
  numeroIdentificacion: number;
  codigoDocente;
  cedulaDocente;

  //radios
      //Especializacion
      especializacion;

      //intensivo / no intensivo
      intensividad;

  //figuras profesionales/ materias
      listaFigurasProfesionales;
      datoFigura;
      codFiguraSeleccionada: 0;
      listaMaterias;
      codMateriaSeleccionada: number = 0;


  //lista de malla extraordinaria
   listaMallaExtra;
   codMallaExtra;


   //Variable de niveles de educacion
   intensivoBasica;
   intensivaBachillerato;
   nointensivoBasica;
   nointensivoBachillerato;

   //listado de curex
  listaCursoExtraordinario;
  codCurex: CatMallaExtra[]=[];


  //lista de paralelo
  listaParalelos;
  datosParalelos: CatParalelo[]=[];

  //varibles para formulario
  listaServicioEducativo: CatServicioEducativo[]=[];
  nombreServicio: CatServicioEducativo[]=[];
  numberServicioEducativo;
  servicioModulo;

  //variables de malla 
  numberMalla;
  listaMalla;
  nombreMalla: CatMallaExtra[]=[];

  //variable asignatura
  listaAsignatura: CatAsignatura[]=[];
  nombreAsignatura: CatAsignatura[]=[];
  numberAsignatura: number=0;


  //paralelos
  datoParalelo: number;

  //datos 
  numberCurso;


  //establecimiento
  listaEstablecimientos;
  codEstablecimiento;


  //variables de los servicios
  listaServicioEdu;
  servicioIntensivo=[];

  //variables de los modulos
  listaModulos;
  moduloPorCodigoIntensivo: CatModulo[]=[];
  modulosPorCodigo: CatModulo[]=[];
  listaServiciosModulos;
  acumuladorPorModulo=[];

  //variables de los grados
  ListaCursos;
  acumularCursos: CatGradosCursos[]=[];
  listaServicioGrado;
  acumuladorPorGrado=[];

  //vairbales para las ofertas de servicio
  listaEquivalencia;
  equivalenciasAcumuladasIntensivas: CatGradoEquivalencia[]=[];
  equivalenciasAcumuladas: CatGradoEquivalencia[]=[];
  listaServiciosEquivalencia;
  acumuladorPorServicio=[];

  //lista de EGB seleccionado segun el servicio
  gradoEGB: CatGradosCursos[]=[];
  moduloEGB: CatModulo[]=[];
  moduloEGBIntensivo: CatModulo[]=[];
  servicioEGB: CatGradoEquivalencia[]=[];
  servicioEGBIntensivo: CatGradoEquivalencia[]=[];


  //variables de seleccion
  gradosSeleccion;
  moduloSeleccion;
  servicioSeleccion;


  //variables seleccionados
  seleccionOfertas=[];


  //listaAsigMalla
  listaAsigMalla;

  //aniosElectivos
  aniosElectivos;
  ofertasConFiltros=[];

  ofertaSemiPresencial=[];
  ofertaPresencial=[];

  
  datosParalelosIntensivoServicio=[];
  datosParalelosServicio=[];
  datosParalelosGrados: CatParalelo[]=[];


  //malla Asig 

  mallaAsigModulo: CatMallaAsigExtra[]=[];
  mallaServicio: CatMallaAsigExtra[]=[];
  mallaGrado: CatMallaAsigExtra[]=[];


  //datos paralelos Intensivo
  datosParalelosIntensivoGrados=[];
  curextArray=[];


  //variables para identificarlos
  acumuladorSeleccion=[];
  resultadoMaterias;
  acumuladorMaterias=[];
  materiasMostrar=[];


  //datos para guardar

  GuardarDatos=[];


  //datos
  element;
  tecnicoCheked=false;
  cienciasCheked=false;


  //autorizacion para guardar
  autorizacionGuardar: string = "B";

  //variable de distributivo modulo
  datosAsignadosModulo: CatAsignacion[]=[];

  datosRepetidos: CatAsignacion[]=[];

  datosCumuladorRepetidos=[];


  //variables de modalidad tipo de educacion
  listaModalidadEducacion;



      //busqueda de datos de docentes
      buscarDocente() {
        this.servicioDocente.getDocenteCedula(this.cedulaDocente).then(data => {
          this.listaDocentes = data.objeto;

          if(this.listaDocentes!=null || this.listaDocentes!=undefined){
            for (let dato in this.listaDocentes) {
              if(this.listaDocentes['stsEstadoAccPersona']=="A"){
                this.nombreDocente = this.listaDocentes['nomPersona'];
                this.numeroIdentificacion = this.listaDocentes['numIdentificacion'];
                this.codigoDocente = this.listaDocentes['codPersona'];
              }
            }
    
            Swal.fire({
              icon: 'success',
              title: 'Atención!',
              text: 'Docente encontrado y cargado!'
            })

          }else{

            Swal.fire({
              icon: 'error',
              title: 'Atención!',
              text: 'Cedula no corresponde a ningún maestro!'
            })

          }

        }, error => {
          alert(error);
        })
      }


  //cambio Tecnico
  onCambioTecnico(event){
    this.especializacion=event;


    this.GuardarDatos=[];

    this.datosParalelos=[];

    this.gradoEGB=[];
    this.moduloEGB=[];
    this.moduloEGBIntensivo=[];
    this.servicioEGB=[];
    this.servicioEGBIntensivo=[];
    this.seleccionOfertas=[];

    this.mallaServicio=[];
    this.materiasMostrar=[];
    this.mallaGrado=[];

    this.datosParalelosGrados=[];
    this.datosParalelosIntensivoServicio=[];
    this.datosParalelosServicio=[];

    this.especializacion=event;

    if(this.serivicoEducativo == "4"){

      if(event=="2" && this.intensividad=="2"){
        for(let i = 0; i < this.acumularCursos.length; i++){
          if(this.acumularCursos[i].graCodigo >= 13 && this.acumularCursos[i].graCodigo <= 15) {
            this.gradoEGB.push(this.acumularCursos[i]);
            this.gradoEGB.sort();
          }
        }

        for(let i = 0; i < this.equivalenciasAcumuladas.length; i++){
          if(this.equivalenciasAcumuladas[i].graequGradoInicio >= 13 && this.equivalenciasAcumuladas[i].graequGradoInicio <= 15) {
            this.servicioEGB.push(this.equivalenciasAcumuladas[i]);
            this.servicioEGB.sort();
          }
        }


      }else if(event=="1" && this.intensividad=="2"){

        for(let i = 0; i < this.acumularCursos.length; i++){
          if(this.acumularCursos[i].graCodigo >= 16 && this.acumularCursos[i].graCodigo <= 18) {
            this.gradoEGB.push(this.acumularCursos[i]);
            this.gradoEGB.sort();
          }
        }

        for(let i = 0; i < this.equivalenciasAcumuladas.length; i++){
          if(this.equivalenciasAcumuladas[i].graequGradoInicio >= 16 && this.equivalenciasAcumuladas[i].graequGradoInicio <= 18) {
            this.servicioEGB.push(this.equivalenciasAcumuladas[i]);
            this.servicioEGB.sort();
          }
        }
        
      }else if(event=="2" && this.intensividad=="1"){
        for(let i = 0; i < this.equivalenciasAcumuladasIntensivas.length; i++){
          if(this.equivalenciasAcumuladasIntensivas[i].graequGradoInicio >= 13 && this.equivalenciasAcumuladasIntensivas[i].graequGradoInicio <= 15) {
            this.servicioEGBIntensivo.push(this.equivalenciasAcumuladasIntensivas[i]);
            this.servicioEGBIntensivo.sort();
          }
        }
      }else if(event=="1" && this.intensividad=="1"){
        for(let i = 0; i < this.equivalenciasAcumuladasIntensivas.length; i++){
          if(this.equivalenciasAcumuladasIntensivas[i].graequGradoInicio >= 16 && this.equivalenciasAcumuladasIntensivas[i].graequGradoInicio <= 18) {
            this.servicioEGBIntensivo.push(this.equivalenciasAcumuladasIntensivas[i]);
            this.servicioEGBIntensivo.sort();
          }
        }
      }

    }
  }


  agregarMateria(maasiexCodigo, codservicio, codparelelo, nombreMateria){

    for(let i = 0; i < this.acumuladorPorServicio.length; i++){

      if(this.acumuladorPorServicio[i].servicio == codservicio){

        let curext = this.acumuladorPorServicio[i].curext;

        let DatosGuardar = {
          cupaexCodigo: codparelelo,
          codServicio: codservicio,
          curext: curext,
          maasiexCodigo: maasiexCodigo,
          especialidad: 0,
          nombreMateria: nombreMateria
        }

        this.GuardarDatos.push(DatosGuardar);

        this.GuardarDatos = this.GuardarDatos.filter((item, index) => (this.GuardarDatos.indexOf(item) === index));

      }
      
    }

    for(let i = 0; i < this.acumuladorPorGrado.length; i++){
      
      if(this.acumuladorPorGrado[i].Grado == codservicio){

        let curext = this.acumuladorPorGrado[i].curext;

        let DatosGuardar = {
          cupaexCodigo: codparelelo,
          codServicio: codservicio,
          curext: curext,
          maasiexCodigo: maasiexCodigo,
          especialidad: this.acumuladorPorGrado[i].especialidad,
          nombreMateria: nombreMateria
        }

        this.GuardarDatos.push(DatosGuardar);
        this.GuardarDatos = this.GuardarDatos.filter((item, index) => (this.GuardarDatos.indexOf(item) === index));
      }

    }
    
  }

  quitarMateria(maasiexCodigo, codservicio, codparelelo, nombreMateria){

    for(let i = 0; i < this.acumuladorPorServicio.length; i++){

      if(this.acumuladorPorServicio[i].servicio == codservicio){

        let curext = this.acumuladorPorServicio[i].curext;

        let DatosGuardar = {
          cupaexCodigo: codparelelo,
          codServicio: codservicio,
          curext: curext,
          maasiexCodigo: maasiexCodigo,
          especialidad: 0,
          nombreMateria: nombreMateria
        }

        this.GuardarDatos.splice(this.GuardarDatos.indexOf(DatosGuardar), 1);

      }
    }

    for(let i = 0; i < this.acumuladorPorGrado.length; i++){
      
      if(this.acumuladorPorGrado[i].Grado == codservicio){

        let curext = this.acumuladorPorGrado[i].curext;

        let DatosGuardar = {
          cupaexCodigo: codparelelo,
          codServicio: codservicio,
          curext: curext,
          maasiexCodigo: maasiexCodigo,
          especialidad: this.acumuladorPorGrado[i].especialidad,
          nombreMateria: nombreMateria
        }

        this.GuardarDatos.splice(this.GuardarDatos.indexOf(DatosGuardar), 1);

      }
      
    }

  }


  agregarCurso(cod, codGrado){

    this.acumuladorMaterias=[];

    for(let i = 0; i < this.acumuladorPorServicio.length; i++){
      if(codGrado == this.acumuladorPorServicio[i].servicio){

        let buscador = {
          "cupaexCodigo": cod,
          "curextCodigo": this.acumuladorPorServicio[i].curext
        }


        this.AcademicoService.buscarMallaPorCursoYParalelo(buscador).subscribe((respuesta: CatFusionMateria)=>{
          this.resultadoMaterias=respuesta;

          for(let y = 0; y < this.resultadoMaterias.listado.length; y++){
            this.acumuladorMaterias.push(this.resultadoMaterias.listado[y]);
          }
        })


      }
    }

    for(let i = 0; i < this.acumuladorPorGrado.length; i ++){
      if(codGrado ==this.acumuladorPorGrado[i].Grado){

        let buscador ={
          "cupaexCodigo": cod,
          "curextCodigo": this.acumuladorPorGrado[i].curext
        }

        this.AcademicoService.buscarMallaPorCursoYParalelo(buscador).subscribe((respuesta: CatFusionMateria)=>{
          this.resultadoMaterias=respuesta;

          for(let y = 0; y < this.resultadoMaterias.listado.length; y++){
            this.acumuladorMaterias.push(this.resultadoMaterias.listado[y]);
          }


        });

      }
    }

    let MateriasDatos = {
      "codParalelo": cod,
      "codServicio": codGrado,
      "datosMaterias": this.acumuladorMaterias
    }

    this.materiasMostrar.push(MateriasDatos);

  }

  quitarCurso(cod, codGrado){

    for(let i = 0; i < this.acumuladorPorServicio.length; i++){
      if(codGrado == this.acumuladorPorServicio[i].servicio){

        let buscador ={
          "cupaexCodigo": cod,
          "curextCodigo": this.acumuladorPorServicio[i].curext
        }

        this.AcademicoService.buscarMallaPorCursoYParalelo(buscador).subscribe((respuesta: CatFusionMateria)=>{
          this.resultadoMaterias=respuesta;

          for(let y = 0; y < this.resultadoMaterias.listado.length; y++){
            this.acumuladorMaterias.push(this.resultadoMaterias.listado[y]);
          }

        })

      }
    }


    for(let i = 0; i < this.acumuladorPorGrado.length; i++){
      if(codGrado ==this.acumuladorPorGrado[i].Grado){

        let buscador ={
          "cupaexCodigo": cod,
          "curextCodigo": this.acumuladorPorGrado[i].curext
        }

        this.AcademicoService.buscarMallaPorCursoYParalelo(buscador).subscribe((respuesta: CatFusionMateria)=>{
          this.resultadoMaterias=respuesta;

          for(let y =0; y < this.resultadoMaterias.listado.length; y++){
            this.acumuladorMaterias.push(this.resultadoMaterias.listado[y]);
          }

        })
      }
    }


    let MateriasDatos = {
      codParalelo: cod,
      codServicio: codGrado,
      datosMaterias: this.acumuladorMaterias
    }

    this.materiasMostrar.splice(this.materiasMostrar.indexOf(MateriasDatos), 1);


  }


  agregar(cod){

    this.seleccionOfertas.push(cod); 

    for(let y = 0; y < this.acumuladorPorServicio.length; y++){
      if(cod==this.acumuladorPorServicio[y].servicio){

        let curext=this.acumuladorPorServicio[y].curext;

        this.curextArray.push(curext);


        if(this.servicioIntensivo.length > 0){
          for(let i = 0; i < this.servicioIntensivo.length; i++){
            if(this.servicioIntensivo[i] == this.acumuladorPorServicio[y].servicio){
              this.obtenerParalelosIntensivoServicio(curext, cod);
            }
          }
        }else{
          this.obtenerParalelosServicios(curext, cod);
        }

        this.sacarMallaAsignaturaServicio(curext);
      }
    }

    for(let y = 0; y < this.acumuladorPorGrado.length; y++){

      if(cod==this.acumuladorPorGrado[y].Grado){
        
        let curext=this.acumuladorPorGrado[y].curext;

        this.curextArray.push(curext);

        this.obtenerParalelosIntensivoGrados(curext, cod);

        this.sacarMallaAsignaturaGrado(curext);

      }

    }

  }

  quitar(cod){

    this.seleccionOfertas.splice(this.seleccionOfertas.indexOf(cod), 1);

    for(let y = 0; y < this.acumuladorPorServicio.length; y++){
      if(cod == this.acumuladorPorServicio[y].servicio){
        let curext=this.acumuladorPorServicio[y].curext;
        this.curextArray.splice(this.curextArray.indexOf(curext), 1);

        if(this.servicioIntensivo.length > 0){

          for(let i = 0; i < this.servicioIntensivo.length; i++){
            if(this.servicioIntensivo[i] == this.acumuladorPorServicio[y].servicio){
              this.quitarParalelosServiciosIntensivo(curext);
            }
          }

        }else{
          this.quitarParalelosServicios(curext);
        }

      }

     
    }

    for(let y = 0; y < this.acumuladorPorGrado.length; y++){
      if(cod == this.acumuladorPorGrado[y].Grado){
        let curext=this.acumuladorPorGrado[y].curext;
        this.curextArray.splice(this.curextArray.indexOf(curext), 1);
        this.quitarParalelosGrados(curext);
      }
    }

    

  }

  sacarMallaAsignaturaServicio(curext){
    this.AcademicoService.getMallaAsignacion(curext).then(data => {
      this.listaAsigMalla=data.listado;
      this.mallaServicio.push(this.listaAsigMalla);
    }, error => {
      alert(error);
    });

  }

  sacarMallaAsignaturaGrado(curext){

    this.AcademicoService.getMallaAsignacion(curext).then(data => {
      this.listaAsigMalla=data.listado;
      this.mallaGrado.push(this.listaAsigMalla);
    }, error => {
      alert(error);
    });
  }


  //Cambio de servicio Educativo
  onCambioServicio(event){

    this.intensividad="0"
    this.datosParalelos=[];
    this.especializacion="0";

    this.gradoEGB=[];
    this.moduloEGB=[];
    this.moduloEGBIntensivo=[];
    this.servicioEGB=[];
    this.servicioEGBIntensivo=[];
    this.seleccionOfertas=[];

    this.datosParalelosGrados=[];
    this.datosParalelosIntensivoServicio=[];
    this.datosParalelosServicio=[];


    this.acumularCursos = this.acumularCursos.filter((item, index) => (this.acumularCursos.indexOf(item) === index));
    this.equivalenciasAcumuladas = this.equivalenciasAcumuladas.filter((item, index) => (this.equivalenciasAcumuladas.indexOf(item) === index));
    this.equivalenciasAcumuladasIntensivas = this.equivalenciasAcumuladasIntensivas.filter((item, index) => (this.equivalenciasAcumuladasIntensivas.indexOf(item) === index));

    this.serivicoEducativo=event;
  }

  //Cambio Intensividad
  onCambioIntensivo(event){

    this.especializacion="0";

    this.GuardarDatos=[];

    this.datosParalelos=[];

    this.mallaServicio=[];
    this.materiasMostrar=[];
    this.mallaGrado=[];


    this.gradoEGB=[];
    this.moduloEGB=[];
    this.moduloEGBIntensivo=[];
    this.servicioEGB=[];
    this.servicioEGBIntensivo=[];
    this.seleccionOfertas=[];


    this.datosParalelosIntensivoGrados=[];



    this.datosParalelosGrados=[];
    this.datosParalelosIntensivoServicio=[];
    this.datosParalelosServicio=[];

    this.intensividad=event;

    if(this.serivicoEducativo=="3"){

      if(event=="2"){
        for(let i = 0; i < this.acumularCursos.length; i++){
          if(this.acumularCursos[i].graCodigo >= 10 && this.acumularCursos[i].graCodigo <= 12) {
            this.gradoEGB.push(this.acumularCursos[i]);
          }
        }
      }else if(event=="1"){

        for(let i = 0; i < this.equivalenciasAcumuladas.length; i++){
          if(this.equivalenciasAcumuladas[i].graequGradoInicio >= 10 && this.equivalenciasAcumuladas[i].graequGradoInicio <= 12) {
            this.servicioEGB.push(this.equivalenciasAcumuladas[i]);
          }
        }
  
        for(let i = 0; i < this.equivalenciasAcumuladasIntensivas.length; i++){
          if(this.equivalenciasAcumuladasIntensivas[i].graequGradoInicio >= 10 && this.equivalenciasAcumuladasIntensivas[i].graequGradoInicio <= 12){
            this.servicioEGBIntensivo.push(this.equivalenciasAcumuladasIntensivas[i]);
          }
        }
        
      }

    }
  }


  //cambio de figura profesional
  onCambioAsignatura(event){

    this.codMateriaSeleccionada=null;
    this.datoFigura=event;
    this.CatalogoService.getEspecialidadPorFiguraProf(this.datoFigura).then(data => {
      this.listaMaterias = data.listado;
    }, error => {
      alert(error);
    })

  }


  quitarParalelosGrados(cod){
    this.OfertaService.getParaleloExtra(cod).then(data => {
      if(data.listado.length == 0){
      }else{
        this.listaParalelos = data.listado;
        this.listaParalelos.forEach(element => {

          this.datosParalelosIntensivoGrados.splice(this.datosParalelosIntensivoGrados.indexOf(element), 1);
          this.acumuladorSeleccion.splice(this.acumuladorSeleccion.indexOf(element.graCodigo), 1);

        });
      }
    })
  }


  obtenerParalelosIntensivoGrados(cod, codServicio){

    this.OfertaService.getParaleloExtra(cod).then(data => {
      if(data.listado.length == 0){
      }else{
        this.listaParalelos = data.listado;

        this.listaParalelos.forEach(element => {
          let datosSeleccion={
            curextSeleccion: cod,
            ServicioSeleccion: codServicio,
            cupaextSeleccion: element
          }

          this.datosParalelosIntensivoGrados.push(datosSeleccion);

          this.datosParalelosIntensivoGrados = this.datosParalelosIntensivoGrados.filter((item, index) => (this.datosParalelosIntensivoGrados.indexOf(item) === index));
          
        });
      }
    }, error => {
      alert(error);
    });

  }


  quitarParalelosServicios(cod){
    this.OfertaService.getParaleloExtra(cod).then(data => {
      if(data.listado.length  == 0){
      }else{
        this.listaParalelos = data.listado;
        this.listaParalelos.forEach(element => {
          this.datosParalelosServicio.splice(this.datosParalelosIntensivoServicio.indexOf(element), 1);
          this.acumuladorSeleccion.splice(this.acumuladorSeleccion.indexOf(element.sereduCodigo), 1);
        });
      }
    })
  }

  quitarParalelosServiciosIntensivo(cod){
    this.OfertaService.getParaleloExtra(cod).then(data => {
      if(data.listado.length  == 0){
      }else{
        this.listaParalelos = data.listado;
        this.listaParalelos.forEach(element => {
          this.datosParalelosIntensivoServicio.splice(this.datosParalelosIntensivoServicio.indexOf(element), 1);
          this.acumuladorSeleccion.splice(this.acumuladorSeleccion.indexOf(element.sereduCodigo), 1);
        });
      }
    })
  }

  obtenerParalelosIntensivoServicio(cod, codServicio){
    this.OfertaService.getParaleloExtra(cod).then(data => {
      if(data.listado.length == 0){
      }else{
        this.listaParalelos = data.listado;
        this.listaParalelos.forEach(element => {

          let datosSeleccion={
            curextSeleccion: cod,
            ServicioSeleccion: codServicio,
            cupaextSeleccion: element
          }

          this.datosParalelosIntensivoServicio.push(datosSeleccion);

          this.datosParalelosIntensivoServicio = this.datosParalelosIntensivoServicio.filter((item, index) => (this.datosParalelosIntensivoServicio.indexOf(item) === index));
        });
      }
    }, error => {
      alert(error);
    });
  }

  obtenerParalelosServicios(cod, codServicio){

    this.OfertaService.getParaleloExtra(cod).then(data => {
      if(data.listado.length == 0){
      }else{
        this.listaParalelos = data.listado;
        this.listaParalelos.forEach(element => {

          let datosSeleccion={
            curextSeleccion: cod,
            ServicioSeleccion: codServicio,
            cupaextSeleccion: element
          }

          this.datosParalelosServicio.push(datosSeleccion);

          this.datosParalelosServicio = this.datosParalelosServicio.filter((item, index) => (this.datosParalelosServicio.indexOf(item) === index));

        });
      }
    }, error => {
      alert(error);
    });


  }


  

  constructor(
    private servicioDocente: DocenteService,
    private _router: Router,
    private CatalogoService: CatalogoService,
    private AcademicoService: AcademicoService,
    private GieeService: GieeService,
    private OfertaService: OfertaService,
    private matricula: MatriculaService

    ) { }

  ngOnInit() {
    
    //servicio de figuras profesionales
    this.CatalogoService.getFiguraProfesional().then(data => {
      this.listaFigurasProfesionales = data.listado;
    }, error => {
      alert(error);
    })


    this.AcademicoService.asignacionesDistributivas().then(data => {
      this.datosAsignadosModulo=data.listado;
    }, error => {
      alert(error);
    })



    this.listaInstitucion=JSON.parse(localStorage.getItem('currentUser'));
    this.informacionSede=this.listaInstitucion.sede;
    this.codAmie=this.informacionSede.nemonico;
    this.obtenerInstitucion();

    this.OfertaService.getCursoExtraordinario(this.codEstableciminto).then(data => {
      this.listaCursoExtraordinario=data.listado;

      this.numberServicioEducativo==0;
      this.numberCurso=0;

      
      this.listaCursoExtraordinario.forEach(element => {        
        if(element.curextCuposDisponibles>0){
          if(element.jorCodigo == this.jorId){
            if(element.modCodigo==0){

              this.CatalogoService.getElectivoSegunAnioPorCodigo(element.reanleCodigo).then(data => {
                this.aniosElectivos=data.objeto;
                if(this.aniosElectivos.reanleTipo == "A"){

                  this.CatalogoService.getServicioEducativoPorCodigo(element.sereduCodigo).then(data => {
                    this.listaServicioEdu=data.objeto;

                    let codigoServicio = this.listaServicioEdu.sereduCodigo;

                    this.CatalogoService.getListaModalidadEducacion(this.listaServicioEdu.motiedCodigo).then(data => {
                      this.listaModalidadEducacion = data.objeto;

                      if(this.modalId == 2 ){
                        
                        if(this.listaModalidadEducacion.modCodigo == 2){
                          if(this.listaModalidadEducacion.temCodigo == 1){
                            this.servicioIntensivo.push(codigoServicio);
                          }
                          this.ofertaPresencial.push(codigoServicio);
                          this.ofertasConFiltros.push(element);
                        }

                      }else if(this.modalId == 3){

                          if(this.listaModalidadEducacion.modCodigo == 3){
                            if(this.listaModalidadEducacion.temCodigo == 1){
                              this.servicioIntensivo.push(codigoServicio);
                            }
                            this.ofertaSemiPresencial.push(codigoServicio);
                            this.ofertasConFiltros.push(element);
                          }                        
                
                      }


                    })

                    

                    /**
                     * comentado los filtros de clasificacion Extraordinaria Presencial, extraordinaria semi presencial y filtro de intensivo y no intensivo
                     */
                    
              
                    }, error => {
                      alert(error);
                   })

                }
              })
            }
          }
        }
      });
    }, error => {
  
    alert(error);
    });

    setTimeout(() => {
      this.obtenerOfertaCursos(this.ofertasConFiltros);
    }, 1000);

    setTimeout(() => {
      this.obtenerEquivalenciaServicios(this.ofertasConFiltros);
    }, 1000);

  }


  obtenerInstitucion(){

    //buscar la institucion
    this.GieeService.getDatosInstitucion(this.codAmie).then(data => {
      this.informacionInstitucion=data.objeto;
      for(let dato in this.informacionInstitucion){
        this.codInstitucion=this.informacionInstitucion.insCodigo;
      }
    }, error => {
      alert(error);
      alert("Usuario sin sede");
    })

  }


  obtenerEquivalenciaServicios(ofertas){

    setTimeout(()=>{
      for(let i = 0; i < ofertas.length; i++){
        if(ofertas[i].graCodigo == 0){
      
          this.obtenerEquivalenciaPorServicio(ofertas[i].sereduCodigo, this.ofertaSemiPresencial, this.ofertaPresencial, this.servicioIntensivo);
  
          this.listaServiciosEquivalencia={
            servicio: ofertas[i].sereduCodigo,
            curext: ofertas[i].curextCodigo
          };

          this.acumuladorPorServicio.push(this.listaServiciosEquivalencia);

        }
      }
    }, 800);

  }


  obtenerOfertaCursos(ofertas){

    setTimeout(()=> {
      for(let i = 0; i < ofertas.length; i++){
        if(ofertas[i].graCodigo!=0){

          this.obtenerGrados(ofertas[i].graCodigo, ofertas[i].sereduCodigo, this.ofertaSemiPresencial, this.ofertaPresencial);

          this.listaServicioGrado={
            Grado: ofertas[i].graCodigo,
            servicio: ofertas[i].sereduCodigo,
            curext: ofertas[i].curextCodigo,
            especialidad: ofertas[i].espCodigo
        };

        this.acumuladorPorGrado.push(this.listaServicioGrado);

        }
      }
    }, 800);

  }


  obtenerGrados(cod, codServicio, ofertaSemi, ofertaPresencial){

    this.CatalogoService.getGradosPorCurso(cod).then(data => {
      this.ListaCursos=data.objeto;

      if(this.modalId==3){

        for(let i = 0; i < ofertaSemi.length; i++){
          if(ofertaSemi[i]==codServicio){
            this.acumularCursos.push(this.ListaCursos);
          }
        }

      }else if(this.modalId==2){

        for(let i = 0; i < ofertaPresencial.length; i++){
          if(ofertaPresencial[i]==codServicio){
            this.acumularCursos.push(this.ListaCursos);
          }
        }

      }
      }, error => {
        alert(error);
      })

  }

  obtenerEquivalenciaPorServicio(cod, oferSemi, ofertaPresencial, intensivo){
    this.CatalogoService.getEquivalenciaPorServicioEducativo(cod).then(data => {
        this.listaEquivalencia=data.listado;
        this.listaEquivalencia.forEach(element => {

          if(this.modalId==3){
              for(let a = 0; a < oferSemi.length; a ++){
                  if(oferSemi[a] == element.sereduCodigo){

                    if(intensivo.length > 0){
                      for(let i = 0; i < intensivo.length; i++){
                        if(intensivo[i]==element.sereduCodigo){                    
                          this.equivalenciasAcumuladasIntensivas.push(element);
                          }else{
                          this.equivalenciasAcumuladas.push(element);
                          }
                      }
                    }else{
                      this.equivalenciasAcumuladas.push(element);
                    }
                  }
                }

          }else if(this.modalId==2){

            for(let a = 0; a < ofertaPresencial.length; a ++){
              if(ofertaPresencial[a] == element.sereduCodigo){
                if(intensivo.length > 0){
                  for(let i = 0; i < intensivo.length; i++){
                    if(intensivo[i]==element.sereduCodigo){                    
                      this.equivalenciasAcumuladasIntensivas.push(element);
                      }else{
                      this.equivalenciasAcumuladas.push(element);
                      }
                  }
                }else{
                  this.equivalenciasAcumuladas.push(element);
                }
              }
            }

          }
        })
    })
  }

   //obtener la malla

   obtenerMalla(cod){
    this.numberMalla=0;
    this.numberAsignatura=0;
    for(let i = 0; i < cod.length; i ++){
      if(this.numberMalla==cod[i]){
      }else{
        this.numberMalla=cod[i];
        //sacar la malla por codigo
        this.AcademicoService.getMallaExtraordinaria(this.numberMalla).then(data =>{
          this.listaMalla=data.listado;
          this.listaMalla.forEach(element => {
            if(element.curextCodigo==this.numberAsignatura){

            }else{
              this.obtenerEspecialidad(element.curextCodigo);
            }
          });
        }, error =>{
          alert(error);
        })
      }
    }
  }

  obtenerEspecialidad(cod) {
    this.numberAsignatura=0;
    if(this.numberAsignatura==cod){
  
    }else{
      this.numberAsignatura= cod;
      this.AcademicoService.getAsignaturaCodigo(this.numberAsignatura).then(data => {
        this.listaAsignatura.push(data.objeto);
      }, error  => {
        alert(error);
      }) 
    }
  }


  asignar(){
    if(this.numeroIdentificacion == null || this.nombreDocente == null || this.GuardarDatos.length==0){
      
      Swal.fire({
        icon: 'error',
        title: 'Atención!',
        text: 'Todos los datos deben ser llenados!'
      })
    }else{
      let datosLimpios = this.GuardarDatos.filter((item, index) => (this.GuardarDatos.indexOf(item) === index));
      for(let i = 0; i < datosLimpios.length; i++){

        if(this.datosAsignadosModulo.length==0){
          this.autorizacionGuardar="A";
          this.guardarTodo(datosLimpios[i], i, this.autorizacionGuardar);
        }else{

          this.AcademicoService.getBuscarRepetidosGrado(datosLimpios[i].cupaexCodigo, datosLimpios[i].curext, datosLimpios[i].maasiexCodigo).then(list => {
            this.datosRepetidos = list.listado;

            if(this.datosRepetidos.length==0){
              this.autorizacionGuardar="A";
              this.guardarTodo(datosLimpios[i], i, this.autorizacionGuardar);
            }else{
              this.autorizacionGuardar="B";
              this.guardarTodo(datosLimpios[i], i, this.autorizacionGuardar);
            }
          })
    
        }

        
        }
    }
  }


   guardarTodo(datosLimpios, i, autorizacion){

    setTimeout(() => {
      if(autorizacion=="A"){
        let dato = i;

      let objetoAsignacion = {
        "cupaexCodigo": datosLimpios.cupaexCodigo,
        "curextCodigo": datosLimpios.curext,
        "disextEstado": 1,
        "docCodigo": parseInt(this.codigoDocente),
        "espCodigo": datosLimpios.especialidad,
        "maasiexCodigo": datosLimpios.maasiexCodigo
      }


      this.AcademicoService.guardarAsignacion(objetoAsignacion).subscribe({
        next: (Response)=>{

          dato=dato+1;
          
          if(dato >= this.GuardarDatos.length){

            Swal.fire({
              title: 'Docente asignado',
              text: "Datos registrados y guardados",
              icon: 'success',
              confirmButtonColor:  '#3085d6',
              confirmButtonText: 'ok'
            }).then((result) => {
              if(result.isConfirmed){
                location.reload();
              }
            })

          }

        },
        error: (error) =>{

          Swal.fire({
            icon: 'error',
            title: 'Atención!',
            text: 'Error en guardar los datos'
          })

        }
      })

      }else{
        Swal.fire({
          icon: 'error',
          title: 'Atención!',
          text: 'Oferta ya asignada'
        })
      }



    }, 2000)
   }


  asignarMateria(event){

    if(this.variableAsignar.length == 0){
      this.variableAsignar.push(event);
    }else{
      this.numberMateria=this.variableAsignar.length;
      for(let i = 0; i < this.variableAsignar.length; i++){
        if(this.variableAsignar[i] != event && this.numberMateria - 1 == i){
          this.variableAsignar.push(event);
          this.numberMateria=this.variableAsignar.length;
          break;
        }else if(this.variableAsignar[i] == event && this.variableAsignar[this.variableAsignar.length - 1] == event){
          this.variableAsignar.splice(i, 1);
        }
      }
    }

  }

   //formularios

   validateFormat(event) {
    let key;
    if (event.type === 'paste') {
      key = event.clipboardData.getData('text/plain');
    } else {
      key = event.keyCode;
      key = String.fromCharCode(key);
    }
    const regex = /[0-9]|\./;
     if (!regex.test(key)) {
      event.returnValue = false;
       if (event.preventDefault) {
        event.preventDefault();
       }
     }
    }
    
}


