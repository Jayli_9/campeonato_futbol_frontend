import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { Ruta_Seleccion_Oferta } from "../rutas/asignacionDocente.routing";
import { SeleccionDatosOfertaComponent } from "../componentes/seleccion-datos-oferta/seleccion-datos-oferta.component";

//importacion material
import { MaterialModule } from "app/main/pages/material/material.module";
import { AsignacionGradoComponent } from '../componentes/asignacion-grado/asignacion-grado.component';
import { AsginacionModuloComponent } from "../componentes/asginacion-modulo/asginacion-modulo.component";
import { FormsModule, ReactiveFormsModule } from '@angular/forms';



@NgModule({
  declarations: [
    SeleccionDatosOfertaComponent,
    AsignacionGradoComponent,
    AsginacionModuloComponent
  ],
  imports: [
    CommonModule,
    RouterModule.forChild(Ruta_Seleccion_Oferta),
    MaterialModule,
    FormsModule,
    ReactiveFormsModule
  ],
  schemas: [
    CUSTOM_ELEMENTS_SCHEMA
  ]
  
})
export class asignacionDocenteModule { }