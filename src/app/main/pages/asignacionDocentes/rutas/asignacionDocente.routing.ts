import { Routes } from '@angular/router';
import { SeleccionDatosOfertaComponent } from "../componentes/seleccion-datos-oferta/seleccion-datos-oferta.component";

export const Ruta_Seleccion_Oferta: Routes = [
  {
    path: 'asignacionDocente',
    component: SeleccionDatosOfertaComponent,
  }
];