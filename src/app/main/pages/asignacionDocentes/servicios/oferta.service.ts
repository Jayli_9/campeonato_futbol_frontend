import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'environments/environment';
import { AuthenticationService } from './../../../../auth/service/authentication.service';


//export interface
import { ResponseGenerico } from "../interface/response-generico";
import { CatCursoExtraordinario } from '../interface/cat_ cursoExtraordinario';
import { CatParalelo } from '../interface/cat_paraleloExtra';

@Injectable({
  providedIn: 'root'
})
export class OfertaService {

constructor(private _http: HttpClient, private _authService: AuthenticationService) { }


//lista de curso extraordinario
getCursoExtraordinario(cod): Promise<ResponseGenerico<CatCursoExtraordinario>> {
  return new Promise((resolve, reject) => {
    this._http.get(`${environment.url_oferta}/private/listarCursosExtraordinariosPorEstablecimiento/`+ cod).subscribe((response: ResponseGenerico<CatCursoExtraordinario>) => {
      resolve(response);
    }, reject);
  })
}

//lista de paralelos
getParaleloExtra(cod): Promise<ResponseGenerico<CatParalelo>> {
  return new Promise((resolve, reject) => {
    this._http.get(`${environment.url_oferta}/private/listarParaleloExtraordinarioPorCurso/`+ cod).subscribe((response: ResponseGenerico<CatParalelo>) => {
      resolve(response);
    }, reject);
  })
}


}
