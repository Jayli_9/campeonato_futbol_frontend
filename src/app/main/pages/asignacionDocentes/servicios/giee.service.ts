import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'environments/environment';
import { AuthenticationService } from './../../../../auth/service/authentication.service';

// exportar interface
import { ResponseGenerico } from '../interface/response-generico';
import { CatInstitucionEducativa } from '../interface/cat_institucionEducativa';
import { CatEstablecimiento } from '../interface/cat_establecimiento';
import { NivelPermisoIntitucionInterface } from '../interface/nivel-Institucion';

@Injectable({
  providedIn: 'root'
})
export class GieeService {

constructor(private _http: HttpClient, private _authService: AuthenticationService) { }


//buscar institucion por amie
getDatosInstitucion(cod: string): Promise<ResponseGenerico<CatInstitucionEducativa>>{
  return new Promise((resolve, reject) => {
    this._http.get(`${environment.url_institucion}/private/buscarInstitucionPorAmie/`+ cod).subscribe((response: ResponseGenerico<CatInstitucionEducativa>) => {
      resolve(response);
    }, reject);
  })
}

getEstablecimientoPorAmie(cod): Promise<ResponseGenerico<CatEstablecimiento>>{
  return new Promise((resolve, reject) => {
this._http.get(`${environment.url_institucion}/private/listarEstablecimientosPorAmie/`+ cod).subscribe((response: ResponseGenerico<CatEstablecimiento>) => {
   resolve(response);
 }, reject);
})
}


//permiso de nivel institucion
getPermisoNivelInstitucionPorInsCodigo(cod): Promise<ResponseGenerico<NivelPermisoIntitucionInterface>>{
  return new Promise((resolve, reject) => {
this._http.get(`${environment.url_institucion}/private/buscarNivelPermisoInstitucionPorCodigoInstitucion/`+ cod).subscribe((response: ResponseGenerico<NivelPermisoIntitucionInterface>) => {
   resolve(response);
 }, reject);
})
}

}