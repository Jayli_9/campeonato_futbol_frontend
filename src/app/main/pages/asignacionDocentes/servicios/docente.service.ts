import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'environments/environment';
import { AuthenticationService } from './../../../../auth/service/authentication.service';
import { ResponseGenerico } from "../interface/response-generico";
import { CatBusquedaDocentes } from "../interface/cat_busquedaDocentes"; 

@Injectable({
  providedIn: 'root'
})
export class DocenteService {

constructor(private _http: HttpClient, private _authService: AuthenticationService) { }

//busqueda de docentes por cedula
getDocenteCedula(cod): Promise<ResponseGenerico<CatBusquedaDocentes>>{
  return new Promise((resolve, reject) => {
    this._http.get(`${environment.url_docente}/private/docenteHispanaPorIdentificacionYEstado/`+ cod , {headers: {
      Authorization: this._authService.currentUserValue.token
    }}).subscribe((response: ResponseGenerico<CatBusquedaDocentes>) => {
      resolve(response);
    }, reject);
  });
}


}
