import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'environments/environment';
import { AuthenticationService } from './../../../../auth/service/authentication.service';

//exportables
import { CatMallaExtra } from "../interface/cat_mallaExtraordinaria";
import { ResponseGenerico } from "../interface/response-generico";
import { CatAsignatura } from "../interface/cat_asignatura";
import { CatAsignacion } from '../interface/cat_asignacion';
import { CatMallaModalidad } from '../interface/cat_mallaModalidad';
import { CatAsignacionModalidad } from '../interface/cat_asignacionModalidad';
import { CatMallaAsigExtra } from '../interface/cat_mallaAsig';
import { CatFusionMateria } from '../interface/acaResultadoCurextCupaex';
import { rejects } from 'assert';

@Injectable({
  providedIn: 'root'
})
export class AcademicoService {

constructor(private _http: HttpClient, private _authService: AuthenticationService) {}


//listar asignacion Distributivo
asignacionesDistributivas(): Promise<ResponseGenerico<CatAsignacion>>{
  return new Promise((resolve, reject) => {
    this._http.get(`${environment.url_academico}/private/listarDistributivoExtra`).subscribe((response: ResponseGenerico<CatAsignacion>) => {
      resolve(response);
    }, reject)
  })
}

//listar Asignacion Modulo
asignacionesDistributivasModulo(): Promise<ResponseGenerico<CatAsignacionModalidad>>{
  return new Promise((resolve, reject) => {
    this._http.get(`${environment.url_academico}/private/listarDistributivoModulo`).subscribe((response: ResponseGenerico<CatAsignacionModalidad>) => {
      resolve(response);
    }, reject)
  })
}

//servicios de materias malla extraordinaria
getMallaExtraordinaria(cod : number): Promise<ResponseGenerico<CatMallaExtra>>{
  return new Promise((resolve, reject) => {
    this._http.get(`${environment.url_academico}/private/buscarMallaAsigExtraPorCurextCodigo/`+ cod).subscribe((response: ResponseGenerico<CatMallaExtra>) => {
      resolve(response);
    }, reject);
  })
}

//servicios de materias malla Modalidad
getMallaModalidad(cod): Promise<ResponseGenerico<CatMallaModalidad>>{
  return new Promise((resolve, reject) => {
    this._http.get(`${environment.url_academico}/private/buscarMallaModalidadPorCurext/`+ cod).subscribe((response: ResponseGenerico<CatMallaModalidad>) => {
      resolve(response);
    }, reject);
  })
}


//servicio de asignatura por coodigo
getAsignaturaCodigo(cod): Promise<ResponseGenerico<CatAsignatura>>{
  return new Promise((resolve, reject) => {
    this._http.get(`${environment.url_academico}/private/buscarMallaAsignaturaPorCodigo/`+ cod).subscribe((response: ResponseGenerico<CatAsignatura>) => {
      resolve(response);
    }, reject);
  })
}


//servicio de asignatura por coodigo
getMallaAsignacion(cod): Promise<ResponseGenerico<CatMallaAsigExtra>>{
  return new Promise((resolve, reject) => {
    this._http.get(`${environment.url_academico}/private/buscarMallaAsigExtraPorCurextCodigo/`+ cod).subscribe((response: ResponseGenerico<CatMallaAsigExtra>) => {
      resolve(response);
    }, reject);
  })
}


//validacion de docente

getCodDocenteExtra(cod): Promise<ResponseGenerico<CatAsignacion>>{
  return new Promise((resolve, reject) => {
    this._http.get(`${environment.url_academico}/private/buscarDistributivoExtraPorDocente/`+ cod).subscribe((response: ResponseGenerico<CatAsignacion>) => {
      resolve(response);
    }, reject)
  })
}

getCodDocenteModulo(cod): Promise<ResponseGenerico<CatAsignacionModalidad>>{
  return new Promise((resolve, reject) => {
    this._http.get(`${environment.url_academico}/private/buscarDistributivoModuloPorDocente/`+ cod).subscribe((response: ResponseGenerico<CatAsignacionModalidad>) => {
      resolve(response);
    }, reject)
  })
}


//validacion de curext

getCurextExtra(cod): Promise<ResponseGenerico<CatAsignacion>>{
  return new Promise((resolve, reject) => {
    this._http.get(`${environment.url_academico}/private/buscarDistributivoExtraPorCurext/`+ cod).subscribe((response: ResponseGenerico<CatAsignacion>) => {
      resolve(response);
    }, reject)
  })
}

getCurextModulo(cod): Promise<ResponseGenerico<CatAsignacionModalidad>>{
  return new Promise((resolve, reject) => {
    this._http.get(`${environment.url_academico}/private/buscarDistributivoModuloPorCurext/`+ cod).subscribe((response: ResponseGenerico<CatAsignacionModalidad>) => {
      resolve(response);
    },)
  })
}
//validacion de cupaext

getCupaextExtra(cod): Promise<ResponseGenerico<CatAsignacion>>{
  return new Promise((resolve, reject) => {
    this._http.get(`${environment.url_academico}/private/buscarDistributivoExtraPorCupaex/`+ cod).subscribe((response: ResponseGenerico<CatAsignacion>) => {
      resolve(response);
    }, reject)
  })
}

getCupaextModulo(cod): Promise<ResponseGenerico<CatAsignacionModalidad>>{
  return new Promise((resolve, reject) => {
    this._http.get(`${environment.url_academico}/private/buscarDistributivoModuloPorCupaext/`+ cod).subscribe((response: ResponseGenerico<CatAsignacionModalidad>) => {
      resolve(response);
    }, reject)
  })
}


//filtro de repetidos

//por grado
getBuscarRepetidosGrado(cupaex, curext, malla): Promise<ResponseGenerico<CatAsignacion>>{
  return new Promise((resolve, reject) => {
    this._http.get(`${environment.url_academico}/private/listarDistributivoExtraPorCupaexPorCurextPorMateria/`+cupaex+`/`+curext+`/`+malla).subscribe((response: ResponseGenerico<CatAsignacion>) => {
      resolve(response);
    }, reject)
  })
}

//por modulo

getBuscarRepetidosModalidad(cupaex, curext, malla): Promise<ResponseGenerico<CatAsignacionModalidad>>{
  return new Promise((resolve, reject) => {
    this._http.get(`${environment.url_academico}/private/buscarDistributivoModuloPorCursoPorParaleloPorMateria/`+cupaex+`/`+curext+`/`+malla).subscribe((response: ResponseGenerico<CatAsignacionModalidad>) => {
      resolve(response);
    }, reject)
  })
}


//buscar asignatura por cupaex y curext
buscarMallaPorCursoYParalelo(parametro: any){
  return this._http.post<CatFusionMateria>(`${environment.url_academico}/private/buscarMallaAsigExtraPorCurextCodigoYGrado`, parametro);
}


//servicios de asignacion

guardarAsignacion(parametro: any) {
  return this._http.post<CatAsignacion>(`${environment.url_academico}/private/guardarDistributivoExtra`, parametro);
}

//Servicios de Asignacion Modalidad

guardarAsignacionModulo(parametro: any){
  return this._http.post<CatAsignacionModalidad>(`${environment.url_academico}/private/guardarDistributivoModulo`, parametro);
}

}