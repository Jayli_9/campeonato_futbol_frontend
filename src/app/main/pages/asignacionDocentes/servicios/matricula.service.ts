import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'environments/environment';
import { AuthenticationService } from './../../../../auth/service/authentication.service';

//exportables
import { ResponseGenerico } from "../interface/response-generico";
import { CatModu } from '../interface/cat_modulo';
import { CatModulo } from '../interface/acaModulo';

@Injectable({
  providedIn: 'root'
})
export class MatriculaService {

constructor(private _http: HttpClient, private _authService: AuthenticationService) { }

//servicios de modulo por inicio y fin 
getmodulosOfertas(cod, ced): Promise<ResponseGenerico<CatModu>>{
  return new Promise((resolve, reject) => {
    this._http.get(`${environment.url_matricula}/private/buscarModuloPorCodigoDeGradoInicioFin/`+ cod +"-"+ ced).subscribe((response: ResponseGenerico<CatModu>) => {
      resolve(response);
    }, reject);
  })
}


//servicio de moculo por codigo servicio educativo
getmodulosPorServicio(cod): Promise<ResponseGenerico<CatModu>>{
  return new Promise((resolve, reject) => {
    this._http.get(`${environment.url_matricula}/private/listarModulosPorServicioEducativo/`+ cod).subscribe((response: ResponseGenerico<CatModu>) => {
      resolve(response);
    }, reject);
  })
}

   //Lista de modalidad segun servicio

   getModuloPorCodigo(cod): Promise<ResponseGenerico<CatModulo>>{
    return new Promise((resolve, reject) => {
this._http.get(`${environment.url_matricula}/private/buscarModuloPorCodigo/`+ cod).subscribe((response: ResponseGenerico<CatModulo>) => {
     resolve(response);
   }, reject);
})
}


}
