import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'environments/environment';
import { AuthenticationService } from './../../../../auth/service/authentication.service';

//exportacion interfaces
import { ResponseGenerico } from "../interface/response-generico";
import { CatJoranada } from "../interface/cat_jornada"; 
import { CatModalidad } from "../interface/cat_modalidad";
import { CatFiguraProfesional } from '../interface/cat_figuraProfesional';
import { CatEspecialidad } from '../interface/cat_especialidad';
import { CatInstitucionEducativa } from "../interface/cat_institucionEducativa";
import { CatServicioEducativo } from '../interface/cat_servicioEducativo';
import { Catregimen } from '../interface/cat_regimen';
import { CatAnioElectivo } from '../interface/cat_anio';
import { CatGradoEquivalencia } from '../interface/acaGradoEquivalencia';
import { CatGradosCursos } from '../interface/cat_gradosCursos';
import { modalidadTipoEducacion } from '../interface/modalidad-tipo-educacion';
import { ModalidadNuevaInterface } from '../interface/modalidadesNueva';
import { CatAnioLectivo } from '../interface/cat_anio_lectivo';

@Injectable({
  providedIn: 'root'
})
export class CatalogoService {

constructor(private _http: HttpClient, private _authService: AuthenticationService) { }

//Servicio de las jornadas
    //Listado de jornadas
getJornadas(): Promise<ResponseGenerico<CatJoranada>> {
  return new Promise((resolve, reject) => {
    this._http.get(`${environment.url_catalogo}/private/listarJornadas`).subscribe((response: ResponseGenerico<CatJoranada>) => {
      resolve(response);
    }, reject);
  })
}

   

    //lista de jornadas por codigo
  getJornadaPorCodigo(cod): Promise<ResponseGenerico<CatJoranada>> {
    return new Promise((resolve, reject) => {
      this._http.get(`${environment.url_catalogo}/private/buscarJornadaPorCodigo/`+ cod).subscribe((response: ResponseGenerico<CatJoranada>) => {
        resolve(response);
      }, reject);
    })
  }

//servicio de modalidad

  //listado de moralidades
getModalidades(): Promise<ResponseGenerico<CatModalidad>>{
  return new Promise((resolve, reject) => {
    this._http.get(`${environment.url_catalogo}/private/listarModalidades`).subscribe((response: ResponseGenerico<CatModalidad>) => {
      resolve(response);
    }, reject);
  })
}

   //lista de modalidades por codigo

   getModalidadesCodigo(cod): Promise<ResponseGenerico<CatModalidad>>{
    return new Promise((resolve, reject) => {
      this._http.get(`${environment.url_catalogo}/private/buscaroModalidadPorCodigo/` + cod).subscribe((response: ResponseGenerico<CatModalidad>) => {
        resolve(response);
      }, reject);
    })
  }


  

//servicio de figura profesional
getFiguraProfesional(): Promise<ResponseGenerico<CatFiguraProfesional>>{
  return new Promise((resolve, reject) => {
    this._http.get(`${environment.url_catalogo}/private/listarCatalogoFiguraProfesionales`).subscribe((response: ResponseGenerico<CatFiguraProfesional>) => {
      resolve(response);
    }, reject);
  })
}

//especialidad por figura profesional
getEspecialidadPorFiguraProf(cod): Promise<ResponseGenerico<CatEspecialidad>>{
  return new Promise((resolve, reject) => {
    this._http.get(`${environment.url_catalogo}/private/buscarCatalogoEspecialidadPorFiguraProfesionalCodigo/`+ cod).subscribe((response: ResponseGenerico<CatEspecialidad>) => {
      resolve(response);
    }, reject);
  })
}


//lista de servicio educativo

  //lista de servicio Educativo por codigo
  getServicioEducativo(): Promise<ResponseGenerico<CatServicioEducativo>>{
    return new Promise((resolve, reject) => {
      this._http.get(`${environment.url_catalogo}/private/listarServicioEducativos`).subscribe((response: ResponseGenerico<CatServicioEducativo>) => {
        resolve(response);
      }, reject);
    })
  }


  //busqueda de servicio Educativo por codigo
  getServicioEducativoPorCodigo(cod): Promise<ResponseGenerico<CatServicioEducativo>>{
    return new Promise((resolve, reject) => {
      this._http.get(`${environment.url_catalogo}/private/buscarServicioEducativoPorCodigo/`+ cod).subscribe((response: ResponseGenerico<CatServicioEducativo>) => {
        resolve(response);
      }, reject);
    })
  }

  //Regimen
  getRegimen(cod: number): Promise<ResponseGenerico<Catregimen>>{
    return new Promise((resolve, reject) => {
      this._http.get(`${environment.url_catalogo}/private/buscarRegimenPorCodigo/`+ cod).subscribe((response: ResponseGenerico<Catregimen>) => {
        resolve(response);
      }, reject);
    })
  }

  //Servicio Anio Electivo
  getAnioElectivo(cod: number): Promise<ResponseGenerico<CatAnioElectivo>>{
    return new Promise((resolve, reject) => {
      this._http.get(`${environment.url_catalogo}/private/listarRegimenAnioLectivoPorCodigoRegimen/`+ cod).subscribe((response: ResponseGenerico<CatAnioElectivo>) => {
        resolve(response);
      }, reject);
    })
  }


  //listar el modulo por servicio Educativo
  getEquivalenciaPorServicioEducativo(cod): Promise<ResponseGenerico<CatGradoEquivalencia>>{
    return new Promise((resolve, reject) => {
this._http.get(`${environment.url_catalogo}/private/buscarGradoPorServicioEducativo/`+ cod).subscribe((response: ResponseGenerico<CatGradoEquivalencia>) => {
     resolve(response);
   }, reject);
})
}


//Obtener Cuso por codigo de grados

getGradosPorCurso(cod): Promise<ResponseGenerico<CatGradosCursos>>{
  return new Promise((resolve, reject) => {
this._http.get(`${environment.url_catalogo}/private/buscarCatalogoGradosPorCodigo/`+ cod).subscribe((response: ResponseGenerico<CatGradosCursos>) => {
   resolve(response);
 }, reject);
})
}

//Anio electivo segun codigo
getElectivoSegunAnioPorCodigo(cod): Promise<ResponseGenerico<CatAnioElectivo>>{
  return new Promise((resolve, reject) => {
this._http.get(`${environment.url_catalogo}/private/buscarRegimenAnioLectivoPorCodigo/`+ cod).subscribe((response: ResponseGenerico<CatAnioElectivo>) => {
   resolve(response);
 }, reject);
})
}
//Anio electivo segun codigo
getAnioLectivoPorAnilecCodigo(cod): Promise<ResponseGenerico<CatAnioLectivo>>{
  return new Promise((resolve, reject) => {
this._http.get(`${environment.url_catalogo}/private/buscarAnioLectivoPorCodigo/`+ cod).subscribe((response: ResponseGenerico<CatAnioLectivo>) => {
   resolve(response);
 }, reject);
})
}


//modalidad tipo de educacion

getListaModalidadEducacion(cod): Promise<ResponseGenerico<modalidadTipoEducacion>>{
  return new Promise((resolve, reject) => {
    this._http.get(`${environment.url_catalogo}/private/buscarModalidadTipoEducacionPorId/`+ cod).subscribe((response: ResponseGenerico<modalidadTipoEducacion>) => {
      resolve(response);
    }, reject);
  })
}
// tipo de educacion

getTipoEducacion(cod): Promise<ResponseGenerico<any>>{
  return new Promise((resolve, reject) => {
    this._http.get(`${environment.url_catalogo}/private/buscarTipoEducacionPorCodigo/`+ cod).subscribe((response: ResponseGenerico<any>) => {
      resolve(response);
    }, reject);
  })
}

buscarIdModalidad(cod): Promise<ResponseGenerico<ModalidadNuevaInterface>>{
  return new Promise((resolve, reject) => {
    this._http.get(`${environment.url_catalogo}/private/buscarModalidadTipoEducacionPorId/`+ cod).subscribe((response: ResponseGenerico<ModalidadNuevaInterface>) => {
      resolve(response);
    }, reject);
  })
}
//Anio electivo segun codigo
getPoblacionObjetivo(): Promise<ResponseGenerico<CatAnioLectivo>>{
  return new Promise((resolve, reject) => {
this._http.get(`${environment.url_catalogo}/private/listarPoblacionObjetivo`).subscribe((response: ResponseGenerico<CatAnioLectivo>) => {
   resolve(response);
 }, reject);
})
}


}