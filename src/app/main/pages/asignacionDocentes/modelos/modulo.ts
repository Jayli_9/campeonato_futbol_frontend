import {get, set} from "lodash-es";
import { CatModu } from "../interface/cat_modulo";

export class modulo implements CatModu{

    constructor (data){
        set(this, 'data', data);
    }

    get modCodigo(): number{
        return get(this, 'data.modCodigo');
    }

    get modCodigoGradoFin(): number{
        return get(this, 'data.modCodigoGradoFin');
    }

    get modCodigoGradoInicio(): number{
        return get(this, 'data.modCodigoGradoInicio');
    }

    get modDescripcion(): String{
        return get(this, 'data.modDescripcion');
    }

    get modDescripcionGradoFin(): String{
        return get(this, 'data.modDescripcionGradoFin');
    }

    get modDescripcionGradoInicio(): String{
        return get(this, 'data.modDescripcionGradoInicio');
    }

    get modEstado(): number{
        return get(this, 'data.modEstado');
    }

    get sereduCodigo(): number{
        return get(this, 'data.sereduCodigo');
    }

    get matRegistroInscritos(): []{
        return get(this, 'data.matRegistroInscritos');
    }
}