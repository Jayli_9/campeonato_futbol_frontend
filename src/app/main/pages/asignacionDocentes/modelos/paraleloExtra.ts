import {get, set} from "lodash-es";
import { CatParalelo } from "../interface/cat_paraleloExtra";

export class paraleloExtra implements CatParalelo{

    constructor (data){
        set(this, 'data', data);
    }

    get curCodigo(): number{
        return get(this, 'data.curCodigo');
    }

    get curparAforo(): number{
        return get(this, 'data.curparAforo');
    }

    get curparBancas(): number{
        return get(this, 'data.curparBancas');
    }

    get curparCodigo(): number{
        return get(this, 'data.curparCodigo');
    }

    get curparEstado(): number{
        return get(this, 'data.curparEstado');
    }

    get curparFechaCreacion(): Date{
        return get(this, 'data.curparFechaCreacion');
    }

    get curparNumEstud(): number{
        return get(this, 'data.curparNumEstud');
    }

    get idiEtnCodigo(): number{
        return get(this, 'data.idiEtnCodigo');
    }

    get nombreParalelo(): String{
        return get(this, 'data.nombreParalelo');
    }

    get parCodigo(): number{
        return get(this, 'data.parCodigo');
    }
}