import {get, set} from "lodash-es";
import { acaTipoAsignaturaI } from "../interface/acaTipoAsginaturaI";

export class modTipoAsignatura implements acaTipoAsignaturaI{

    constructor (data){
        set(this, 'data', data);
    }

    get tiasCodigo(): number {
        return get(this, 'data.tiasCodigo');
    }

    get tiasDescripcion(): String {
        return get(this, 'data.tiasDescripcion');
    }

    get tiasEstado(): number {
        return get(this, 'data.tiasEstado');
    }

    get tiasObligatorio(): String {
        return get(this, 'data.tiasObligatorio');
    }
}