import {get, set} from "lodash-es";
import { CatParaleloExtraordinario } from "../interface/cat_cursoParaleloExtraordinario";

export class cursoparaleloExtraordinario implements CatParaleloExtraordinario{
    constructor (data){
        set(this, 'data', data);
    }

    get codigoCurso(): number{
        return get(this, 'data.codigoCurso');
    }

    get codigoParalelo(): number{
        return get(this, 'data.codigoParalelo');
    }

    get cupaexAforo(): number{
        return get(this, 'data.cupaexAforo');
    }

    get cupaexBancas(): number{
        return get(this, 'data.cupaexBancas');
    }

    get cupaexCodigo(): number{
        return get(this, 'data.cupaexCodigo');
    }

    get cupaexEstado(): number{
        return get(this, 'data.cupaexEstado');
    }

    get cupaexFechaCreacion(): Date{
        return get(this, 'data.cupaexFechaCreacion');
    }

    get cupaexNumEstud(): number{
        return get(this, 'data.cupaexNumEstud');
    }

    get nombreParalelo(): String{
        return get(this, 'data.nombreParalelo');
    }
}