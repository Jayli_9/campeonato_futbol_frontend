import {get, set} from "lodash-es";
import { CatServicioEducativo } from "../interface/cat_servicioEducativo";

export class servicioEducativo implements CatServicioEducativo{

    constructor(data){
        set(this, 'data', data);
    }

    get sereduCodigo(): number {
        return get(this, 'data.sereduCodigo');
    }

    get reanleCodigo(): number {
        return get(this, 'data.reanleCodigo');
    }

    get sereduDescripcion(): String {
        return get(this, 'data.sereduDescripcion');
    }

    get sereduEstado(): number {
        return get(this, 'data.sereduEstado');
    }

    get sereduFechaFin(): Date {
        return get(this, 'data.sereduFechaFin');
    }

    get sereduFechaInicio(): Date {
        return get(this, 'data.sereduFechaInicio');
    }

    get sereduIntensivo(): number {
        return get(this, 'data.sereduIntensivo');
    }

    get sereduVigente(): String {
        return get(this, 'data.sereduVigente');
    }

    get sereduClasificacion(): String {
        return get(this, 'data.sereduClasificacion');
    }

    get sereduNemonico(): String {
        return get(this, 'data.sereduNemonico');
    }

    get codigoRegimen(): number {
        return get(this, 'data.codigoRegimen');
    }

    get codigoAnioLectivo(): number {
        return get(this, 'data.codigoAnioLectivo');
    }

    get codigoModalidadTipoEducacion(): number {
        return get(this, 'data.codigoModalidadTipoEducacion');
    }

    get motiedCodigo():number {
        return get(this, 'data.motiedCodigo');
    }

    get codigoModalidad(): number {
        return get(this, 'data.codigoModalidad');
    }

    get codigoTipoEducacion(): number {
        return get(this, 'data.codigoTipoEducacion');
    }

    get codigoPrograma(): number {
        return get(this, 'data.codigoPrograma');
    }
}