import {get, set} from "lodash-es";
import { acaAgrupacionI } from "../interface/acaAgrupacionI";

export class modAgrupacion implements acaAgrupacionI{

    constructor (data){
        set(this, 'data', data);
    }

    get agrCodigo(): number {
        return get(this, 'data.agrCodigo');
    }

    get agrDescripcion(): String {
        return get(this, 'data.agrDescripcion');
    }

    get agrEstado(): number {
        return get(this, 'data.agrEstado');
    }

    get agrNemonico(): String {
        return get(this, 'data.agrNemonico');
    }
}
