import {get, set} from "lodash-es";
import { CatAnioElectivo } from "../interface/cat_anio";

export class anio implements CatAnioElectivo{
    constructor(data){
        set(this, 'data', data);
    }

    get anilecCodigo(): number {
        return get(this, 'data.anilecCodigo');
    }

    get descripcionAnioLectivo(): String {
        return get(this, 'data.descripcionAnioLectivo');
    }

    get reanleCodigo(): number {
        return get(this, 'data.reanleCodigo');
    }

    get reanleEstado(): number {
        return get(this, 'data.reanleEstado');
    }

    get reanleFechaCreacion(): Date {
        return get(this, 'data.reanleFechaCreacion');
    }

    get reanleTipo(): String {
        return get(this, 'data.reanleTipo');
    }

    get regCodigo(): number{
        return get(this, 'data.regCodigo');
    }

}