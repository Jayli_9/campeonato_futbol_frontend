import {get, set} from "lodash-es";
import { CatMallaExtra } from "../interface/cat_mallaExtraordinaria";

export class mallaExtraordinaria implements CatMallaExtra{

    constructor (data){
        set(this, 'data', data);
    }

    get asiCodigo(): number{
        return get(this, 'data.asiCodigo');
    }

    get curextCodigo(): number{
        return get(this, 'data.curextCodigo');
    }

    get maasiexCodigo(): number{
        return get(this, 'data.maasiexCodigo');
    }

    get maasiexEstado(): number{
        return get(this, 'data.maasiexEstado');
    }

    get maasiexHorasAuto(): number{
        return get(this, 'data.maasiexHorasAuto');
    }

    get maasiexHorasPre(): number{
        return get(this, 'data.maasiexHorasPre');
    }

    get maasiexHraTotal(): number{
        return get(this, 'data.maasiexHraTotal');
    }

    get maasiexNemonico(): String{
        return get(this, 'data.maasiexNemonico');
    }

    get reanleCodigo(): number{
        return get(this, 'data.reanleCodigo');
    }
}