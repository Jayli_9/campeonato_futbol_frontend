import {get, set} from "lodash-es";
import { CatCurrentUser } from "../interface/cat_currentUser";

export class currentUser implements CatCurrentUser{
    constructor (data){
        set(this, 'data', data);
    }

    get accesoConcedido(): true{
        return get(this, 'data.accesoConcedido');
    }

    get cedula(): number{
        return get(this, 'data.cedula');
    }

    get codigoUsuario(): number{
        return get(this, 'data.codigoUsuario');
    }

    get identificacion(): String{
        return get(this, 'data.identificacion');
    }

    get nombre(): String{
        return get(this, 'data.nombre');
    }

    get observacion(): String{
        return get(this, 'data.observacion');
    }

    get sede(): object{
        return get(this, 'data.sede');
    }

    get token(): String{
        return get(this, 'data.token');
    }
    
}