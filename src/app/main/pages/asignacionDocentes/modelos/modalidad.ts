import {get, set} from "lodash-es";
import { CatModalidad } from "../interface/cat_modalidad";

export class modalidad implements CatModalidad {

    constructor (data){
        set(this, 'data', data);
    }

    get modCodigo(): number{
        return get(this, 'data.modCodigo');
    }

    get modNombre(): String{
        return get(this, 'data.modNombre');
    }

    get modEstado(): number{
        return get(this, 'data.modEstado');
    }

    get modFechaCreacion(): Date{
        return get(this, 'data.modFechaCreacion');
    }
}