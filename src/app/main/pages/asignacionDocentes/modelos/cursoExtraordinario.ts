import {get, set} from "lodash-es";
import { CatCursoExtraordinario } from "../interface/cat_ cursoExtraordinario";

export class cursoExtraordinario implements CatCursoExtraordinario{
    constructor(data){
        set(this, 'data', data);
    }

    get curextAforo():number {
        return get(this, 'data.curextAforo');
    }

    get curextCodigo(): number{
        return get(this, 'data.curextCodigo');
    }

    get curextCuposDisponibles(): number{
        return get(this, 'data.curextCuposDisponibles');
    }

    get curextEstado(): number {
        return get(this, 'data.curextEstado');
    }

    get curextFechaCreacion(): Date {
        return get(this, 'data.curextFechaCreacion');
    }

    get espCodigo(): number{
        return get(this, 'data.espCodigo');
    }

    get graCodigo(): number{
        return get(this, 'data.graCodigo');
    }

    get insestCodigo(): number{
        return get(this, 'data.insestCodigo');
    }

    get jorCodigo(): number {
        return get(this, 'data.jorCodigo');
    }

    get modCodigo(): number {
        return get(this, 'data.modCodigo');
    }

    get reanleCodigo(): number {
        return get(this, 'data.reanleCodigo');
    }

    get sereduCodigo(): number {
        return get(this, 'data.sereduCodigo');
    }
}

