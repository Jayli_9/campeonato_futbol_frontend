import {get, set} from "lodash-es";
import { acaTipoValoracionI } from "../interface/acaTipoValoracion";

export class modTipoValoracion implements acaTipoValoracionI{

    constructor (data){
        set(this, 'data', data);
    }

    get tivaCodigo(): number {
        return get(this, 'data.tivaCodigo');
    }

    get tivaDescripcion(): String {
        return get(this, 'data.tivaDescripcion');
    }

    get tivaEstado(): number {
        return get(this, 'data.tivaEstado');
    }
}