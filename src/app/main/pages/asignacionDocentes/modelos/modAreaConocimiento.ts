import {get, set} from "lodash-es";
import { acaAreaConocimientoI } from "../interface/acaAreaConocimientoI";

export class modAreaConocimiento implements acaAreaConocimientoI{
    constructor (data){
        set(this, 'data', data);
    }

    get arcoCodigo(): number {
        return get(this, 'data.arcoCodigo');
    }

    get arcoDescripcion(): String {
        return get(this, 'data.arcoDescripcion');
    }

    get arcoEstado(): number {
        return get(this, 'data.arcoEstado');
    }

    get arcoNemonico(): String {
        return get(this, 'data.arcoNemonico');
    }
}