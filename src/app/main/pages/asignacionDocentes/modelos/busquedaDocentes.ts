import {get, set} from "lodash-es";
import { CatBusquedaDocentes } from "../interface/cat_busquedaDocentes";

export class busquedaDocentes implements CatBusquedaDocentes{
    constructor (data){
        set(this, 'data', data);
    }

    get numIdentificacion(): String{
        return get(this, 'data.numIdentificacion');
    }

    get nomPersona(): String{
        return get(this, 'data.nomPersona');
    }

    get denominacionUltimaAcc(): String{
        return get(this, 'data.denominacionUltimaAcc');
    }

    get institucionUltimaAcc(): String{
        return get(this, 'data.institucionUltimaAcc');
    }

    get experienciaPublica(): number{
        return get(this, 'data.experienciaPublica');
    }

    get experienciaPublicaLetras(): String{
        return get(this, 'data.experienciaPublicaLetras');
    }

    get experienciaPrivada(): number{
        return get(this, 'data.experienciaPrivada');
    }

    get experienciaPrivadaLetras(): String{
        return get(this, 'data.experienciaPrivadaLetras');
    }

    get experienciaTotal(): number{
        return get(this, 'data.experienciaTotal');
    }

    get experienciaTotalLetras(): String{
        return get(this, 'data.experienciaTotalLetras');
    }







}