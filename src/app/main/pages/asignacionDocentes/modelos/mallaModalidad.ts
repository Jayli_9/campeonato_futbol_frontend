import {get, set} from "lodash-es";
import { CatMallaModalidad } from "../interface/cat_mallaModalidad";

export class mallaModalidad implements CatMallaModalidad{

    constructor (data){
        set(this, 'data', data);
    }

    get mmodCodigo(): number{
        return get(this, 'data.mmodCodigo');
    }

    get unialfCodigo(): number{
        return get(this, 'data.unialfCodigo');
    }

    get mmodNemonico(): String{
        return get(this, 'data.mmodNemonico');
    }

    get mmodHoras(): number{
        return get(this, 'data.mmodHoras');
    }

    get mmodEstado(): number{
        return get(this, 'data.mmodEstado');
    }

    get reanleCodigo(): number{
        return get(this, 'data.reanleCodigo');
    }
}