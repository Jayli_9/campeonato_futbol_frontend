import {get, set} from "lodash-es";
import { CatInstitucionEducativa } from "../interface/cat_institucionEducativa";

export class institucionEducativa implements CatInstitucionEducativa{
    constructor (data){
        set(this, 'data', data);
    }

    get insCodigo(): number{
        return get(this, 'data.insCodigo');
    }

    get insDescripcion(): String{
        return get(this, 'data.insDescripcion');
    }

    get insEstado(): number{
        return get(this, 'data.insEstado');
    }

    get insAmie(): String{
        return get(this, 'data.insAmie');
    }

    get insLogo(): number{
        return get(this, 'data.insLogo');
    }

    get insEslogan(): number{
        return get(this, 'data.insEslogan');
    }

    get insNumeroPermiso(): String{
        return get(this, 'data.insNumeroPermiso');
    }

    get disCodigoAdm(): number{
        return get(this, 'data.disCodigoAdm');
    }

    get zonCodigoAdm(): number{
        return get(this, 'data.zonCodigoAdm');
    }

    get cirCodigoAdm(): number{
        return get(this, 'data.cirCodigoAdm');
    }

    get insNumResolucion(): String{
        return get(this, 'data.insNumResolucion');
    }

    get insRutaArchivo(): String{
        return get(this, 'data.insRutaArchivo');
    }

    get sosCodigo(): number{
        return get(this, 'data.sosCodigo');
    }

    get insInstitucionRegimens(): object{
        return get(this, 'data.insInstitucionRegimens');
    }

    get inDescripcion(): String{
        return get(this, 'data.inDescripcion');
    }


}