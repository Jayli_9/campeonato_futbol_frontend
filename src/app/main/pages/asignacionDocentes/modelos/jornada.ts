import {get, set} from "lodash-es";
import { CatJoranada } from "../interface/cat_jornada";

export class joranada implements CatJoranada{

    constructor (data){
        set(this, 'data', data);
    }

    get jorCodigo(): number{
        return get(this, 'data.jorCodigo');
    }

    get jorEstado(): number{
        return get(this, 'data.jorEstado');
    }

    get jorFechaCreacion(): Date{
        return get(this, 'data.jorFechaCreacion');
    }

    get jorNombre(): String{
        return get(this, 'data.jorNombre');
    }


}