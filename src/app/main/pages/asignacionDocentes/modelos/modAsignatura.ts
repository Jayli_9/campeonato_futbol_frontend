import {get, set} from "lodash-es";
import { acaAgrupacionI } from "../interface/acaAgrupacionI";
import { acaAreaConocimientoI } from "../interface/acaAreaConocimientoI";
import { acaAsignaturaI } from "../interface/acaAsginaturaI";
import { acaTipoAsignaturaI } from "../interface/acaTipoAsginaturaI";
import { acaTipoValoracionI } from "../interface/acaTipoValoracion";

export class modAsignatura implements acaAsignaturaI{

    constructor (data){
        set(this, 'data', data);
    }

    get asiCodigo(): number {
        return get(this, 'data.asiCodigo');
    }

    get asiDescripcion(): String {
        return get(this, 'data.asiDescripcion');
    }

    get asiHoras(): number {
        return get(this, 'data.asiHoras');
    }

    get asiEstado(): number {
        return get(this, 'data.asiEstado');
    }

    get asiNemonico(): String {
        return get(this, 'data.asiNemonico');
    }

    get acaAgrupacion(): acaAgrupacionI {
        return get(this, 'data.acaAgrupacion');
    }

    get acaAreaConocimiento(): acaAreaConocimientoI {
        return get(this, 'data.acaAreaConocimiento');
    }

    get acaTipoAsignatura(): acaTipoAsignaturaI {
        return get(this, 'data.acaTipoAsignatura');
    }

    get acaTipoValoracion(): acaTipoValoracionI {
        return get(this, 'data.acaTipoValoracion');
    }


}