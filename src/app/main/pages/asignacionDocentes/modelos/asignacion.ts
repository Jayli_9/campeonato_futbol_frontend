import {get, set} from "lodash-es";
import { CatAsignacion } from "../interface/cat_asignacion";

export class asignacion implements CatAsignacion{
    constructor(data){
        set(this, 'data', data);
    }

    get cupaexCodigo(): number {
        return get(this, 'data.cupaexCodigo');
    }

    get curextCodigo(): number {
        return get(this, 'data.curextCodigo');
    }
    get disextCodigo(): number {
        return get(this, 'data.disextCodigo');
    }
    get disextEstado(): number {
        return get(this, 'data.disextEstado');
    }
    get docCodigo(): number {
        return get(this, 'data.docCodigo');
    }
    get espCodigo(): number {
        return get(this, 'data.espCodigo');
    }
    get maasiexCodigo(): number {
        return get(this, 'data.maasiexCodigo');
    }


}