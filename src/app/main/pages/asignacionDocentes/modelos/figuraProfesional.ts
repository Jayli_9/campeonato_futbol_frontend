import {get, set} from "lodash-es";
import { CatFiguraProfesional } from "../interface/cat_figuraProfesional";

export class figuraProfesional implements CatFiguraProfesional{

    constructor (data){
        set(this, 'data', data);

    //     figproCodigo: number;
    // figproDescripcion: String;
    // figproEstado: number;
    // figproFechaCreacion: Date;
    }

    get figproCodigo(): number{
        return get(this, 'data.figproCodigo');
    }

    get figproDescripcion(): String{
        return get(this, 'data.figproDescripcion');
    }

    get figproEstado(): number{
        return get(this, 'data.figproEstado');
    }

    get figproFechaCreacion(): Date{
        return get(this, 'data.figproFechaCreacion');
    }



}