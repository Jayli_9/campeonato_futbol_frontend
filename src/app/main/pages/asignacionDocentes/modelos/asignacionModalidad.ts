import {get, set} from "lodash-es";
import { CatAsignacionModalidad } from "../interface/cat_asignacionModalidad";

export class asignacionModalidad implements CatAsignacionModalidad{
    constructor(data){
        set(this, 'data', data);
    }

    get dismoCodigo(): number{
        return get(this, 'data.dismoCodigo');
    }

    get mmodCodigo(): number {
        return get(this, 'data.mmodCodigo');
    }

    get curextCodigo(): number {
        return get(this, 'data.curextCodigo');
    }

    get cupaextCodigo(): number {
        return get(this, 'data.cupaextCodigo');
    }

    get modCodigo(): number {
        return get(this, 'data.modCodigo');
    }

    get docCodigo(): number {
        return get(this, 'data.docCodigo');
    }

    get dismoEstado(): number {
        return get(this, 'data.dismoEstado');
    }
}