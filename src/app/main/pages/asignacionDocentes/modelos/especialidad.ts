import {get, set} from "lodash-es";
import { CatEspecialidad } from "../interface/cat_especialidad";

export class especialidad implements CatEspecialidad {

    constructor (data){
        set(this, 'data', data);
    }

    get codigoFiguraProfesional(): number{
        return get(this, 'data.codigoFiguraProfesional');
    }

    get espCodigo(): number{
        return get(this, 'data.espCodigo');
    }

    get espDescripcion(): String{
        return get(this, 'data.espDescripcion');
    }

    get espEstado(): number{
        return get(this, 'data.espEstado');
    }

    get espFechaCreacion(): Date{
        return get(this, 'data.espFechaCreacion');
    }

    
    


}