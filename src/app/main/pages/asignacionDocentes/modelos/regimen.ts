import {get, set} from "lodash-es";
import { Catregimen } from "../interface/cat_regimen";

export class regimen implements Catregimen {

    constructor(data){
        set(this, 'data', data);
    }

    get regCodigo(): number {
        return get(this, 'data.regCodigo');
    }

    get regDescripcion(): String {
        return get(this, 'data.regDescripcion');
    }

    get regEstado(): number {
        return get(this, 'data.regEstado');
    }

    get regFechaCreacion(): Date {
        return get(this, 'data.regFechaCreacion');
    }
}