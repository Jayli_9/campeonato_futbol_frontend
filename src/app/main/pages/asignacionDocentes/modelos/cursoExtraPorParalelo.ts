import {get, set} from "lodash-es";
import { CatCursoExtra } from "../interface/cat_cursoExtraPorParalelo";

export class cursoExtraPorParalelo implements CatCursoExtra{
    constructor (data){
        set(this, 'data', data);
    }

    get curparCodigo(): number{
        return get(this, 'data.curparCodigo');
    }

    get curparAforo(): number{
        return get(this, 'data.curparAforo');
    }

    get curparBancas(): number{
        return get(this, 'data.curparBancas');
    }

    get curparEstado(): number{
        return get(this, 'data.curparEstado');
    }

    get curparFechaCreacion(): Date{
        return get(this, 'data.curparFechaCreacion');
    }

    get idiEtnCodigo(): number{
        return get(this, 'data.idiEtnCodigo');
    }

    get curparNumEstud(): number{
        return get(this, 'data.curparNumEstud');
    }

    get curCodigo(): number{
        return get(this, 'data.curCodigo');
    }

    get parCodigo(): number{
        return get(this, 'data.parCodigo');
    }

    get nombreParalelo(): number{
        return get(this, 'data.nombreParalelo');
    }
}