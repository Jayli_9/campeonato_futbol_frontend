import {get, set} from "lodash-es";
import { acaAsignaturaI } from "../interface/acaAsginaturaI";
import { CatAsignatura } from "../interface/cat_asignatura";

export class asignatura implements CatAsignatura{
    constructor(data){
        set(this, 'data', data);
    }

    get maasCodigo():number {
        return get(this, 'data.maasCodigo');
    }

    get curCodigo():number {
        return get(this, 'data.curCodigo');
    }

    get maasEstado():number {
        return get(this, 'data.maasEstado');
    }

    get maasHoras():number {
        return get(this, 'data.maasHoras');
    }

    get maasNemonico(): String  {
        return get(this, 'data.maasNemonico');
    }

    get reanleCodigo():number {
        return get(this, 'data.reanleCodigo');
    }

    get acaAsignatura(): acaAsignaturaI {
        return get(this, 'data.acaAsignatura');
    }

}