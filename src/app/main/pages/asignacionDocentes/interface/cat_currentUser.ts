export interface CatCurrentUser{
    accesoConcedido: true;
    cedula: number;
    codigoUsuario: number;
    identificacion: String;
    nombre: String;
    observacion: String;
    sede: object
    token: String;
}