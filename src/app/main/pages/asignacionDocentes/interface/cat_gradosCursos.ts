export interface CatGradosCursos {
    graCodigo: number;
    nivCodigo: number;
    graDescripcion: String;
    graEstado: number;
    graNemonico: String;
    graFechaCreacion: Date;
}