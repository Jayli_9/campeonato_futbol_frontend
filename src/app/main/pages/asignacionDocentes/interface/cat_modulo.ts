export interface CatModu{
            modCodigo: number;
            modCodigoGradoFin: number;
            modCodigoGradoInicio: number;
            modDescripcion: String;
            modDescripcionGradoFin: String;
            modDescripcionGradoInicio: String;
            modEstado: number;
            sereduCodigo: number;
            matRegistroInscritos: [];
}