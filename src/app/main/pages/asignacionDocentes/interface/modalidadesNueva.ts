export interface ModalidadNuevaInterface{
     motiedCodigo: number;
     motiedDescripcion: string;
     motiedEstado: number;
     modCodigo: number;
     tipeduCodigo: number;
     temCodigo: number;
}