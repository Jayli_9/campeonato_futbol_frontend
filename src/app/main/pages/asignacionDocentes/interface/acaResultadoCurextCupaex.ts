export interface CatFusionMateria{
       maasiexCodigo: number;
       disextCodigo: number;
       asiCodigo: number;
       asiDescripcion: String;
       tivaCodigo: number;
       maasiexNemonico: String;
       maasiexEstado: number;
       maasiexHorasPre: number;
       maasiexHorasAuto: number;
       maasiexHraTotal: number;
       curextCodigo: number;
       reanleCodigo: number;
       estadoCHB: number;
       docCodigo: number;
}