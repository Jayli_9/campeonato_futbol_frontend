export interface CatBusquedaDocentes {
    numIdentificacion: String;
    nomPersona: String;
    denominacionUltimaAcc: String;
    institucionUltimaAcc: String;
    experienciaPublica: number;
    experienciaPublicaLetras: String;
    experienciaPrivada: number;
    experienciaPrivadaLetras: String;
    experienciaTotal: number;
    experienciaTotalLetras: String;
}