export interface CatInstitucionEducativa {
         insCodigo: number;
         insDescripcion: String;
        insEstado: number;
        insAmie: String;
        insLogo: number;
        insEslogan: number;
        insNumeroPermiso: String;
        disCodigoAdm: number;
        zonCodigoAdm: number;
        cirCodigoAdm: number;
        insNumResolucion: String;
        insRutaArchivo: String;
        sosCodigo: number;
        insInstitucionRegimens: object;
        inDescripcion: String;
}