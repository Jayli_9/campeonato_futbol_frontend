export interface CatAsignacion{
      cupaexCodigo: number;
      curextCodigo: number;
      disextCodigo: number;
      disextEstado: number;
      docCodigo: number;
      espCodigo: number;
      maasiexCodigo: number;
}