export interface CatParaleloExtraordinario {
      codigoCurso: number;
      codigoParalelo: number;
      cupaexAforo: number;
      cupaexBancas: number;
      cupaexCodigo: number;
      cupaexEstado: number;
      cupaexFechaCreacion: Date;
      cupaexNumEstud: number;
      nombreParalelo: String;
}