export interface CatFiguraProfesional{
    figproCodigo: number;
    figproDescripcion: String;
    figproEstado: number;
    figproFechaCreacion: Date;
}