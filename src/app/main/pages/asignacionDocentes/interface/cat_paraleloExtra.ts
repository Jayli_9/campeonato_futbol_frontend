export interface CatParalelo {
    curCodigo: number;
    curparAforo: number;
    curparBancas: number;
    curparCodigo: number;
    curparEstado: number;
    curparFechaCreacion: Date;
    curparNumEstud: number;
    idiEtnCodigo: number;
    nombreParalelo: String;
    parCodigo: number;
}