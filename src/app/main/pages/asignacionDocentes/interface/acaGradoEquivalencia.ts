export interface CatGradoEquivalencia{
    graequCodigo: number;
    graequGradoInicio: number;
    graequGradoFin: number;
    graequEstado: number;
    sereduCodigo: number;
    graequDescripcion: String;
}