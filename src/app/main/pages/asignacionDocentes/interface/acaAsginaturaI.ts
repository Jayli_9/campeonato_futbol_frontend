import { acaAgrupacionI } from "./acaAgrupacionI";
import { acaAreaConocimientoI } from "./acaAreaConocimientoI";
import { acaTipoAsignaturaI } from "./acaTipoAsginaturaI";
import { acaTipoValoracionI } from "./acaTipoValoracion";

export interface acaAsignaturaI{
        asiCodigo: number;
        asiDescripcion: String;
        asiHoras: number;
        asiEstado: number;
        asiNemonico: String;
        acaAgrupacion: acaAgrupacionI;
        acaAreaConocimiento: acaAreaConocimientoI;
        acaTipoAsignatura: acaTipoAsignaturaI;
        acaTipoValoracion: acaTipoValoracionI;
}