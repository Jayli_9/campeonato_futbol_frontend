export interface CatMallaExtra{
      asiCodigo: number;
      curextCodigo: number;
      maasiexCodigo: number;
      maasiexEstado: number;
      maasiexHorasAuto: number;
      maasiexHorasPre: number;
      maasiexHraTotal: number;
      maasiexNemonico: String;
      reanleCodigo: number;
}