export interface CatMallaAsigExtra{
    maasiexCodigo: number;
    acaAsignatura: number;
    maasiexNemonico: string;
    maasiexEstado: number;
    maasiexHorasPre: number;
    maasiexHorasAuto: number;
    maasiexHraTotal: number;
    curextCodigo: number;
    reanleCodigo: number;
}