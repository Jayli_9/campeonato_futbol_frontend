export interface CatCursoExtraordinario{
      curextAforo: number;
      curextCodigo: number;
      curextCuposDisponibles: number;
      curextEstado: number;
      curextFechaCreacion: Date;
      espCodigo: number;
      graCodigo: number;
      insestCodigo: number;
      jorCodigo: number;
      modCodigo: number;
      reanleCodigo: number;
      sereduCodigo: number;
}