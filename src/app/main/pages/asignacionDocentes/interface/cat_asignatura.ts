import { acaAsignaturaI } from "./acaAsginaturaI";

export interface CatAsignatura{
     maasCodigo: number;
     curCodigo: number;
     maasEstado: number;
     maasHoras: number;
     maasNemonico: String;
     reanleCodigo: number;
     acaAsignatura: acaAsignaturaI;
}