export interface Catregimen{
      regCodigo: number;
      regDescripcion: String;
      regEstado: number;
      regFechaCreacion: Date;
}