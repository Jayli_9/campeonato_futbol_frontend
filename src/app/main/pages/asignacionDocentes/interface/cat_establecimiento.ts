export interface CatEstablecimiento{
    cirCodigo: number;
    insInstitucion: {
        cirCodigoAdm: number;
        disCodigoAdm: number;
        inDescripcion: String;
        insAmie: String;
        insCodigo: number;
        insDescripcion: String;
        insEslogan: String;
        insEstado: number;
        insLogo: String;
        insNumResolucion: String;
        insNumeroPermiso: String;
        insRutaArchivo: String;
        regimenes: [
          {
            insCodigo: number;
            insRegEstado: number;
            insregCodigo: number;
            regCodigo: number;
          }
        ],
        sosCodigo: number;
        zonCodigoAdm: number;
      },
      insTipoInstitucion: {
        tipinsCodigo: number;
        tipinsDescripcion: String;
        tipinsEstado: number;
      },
      insestCodigo: number;
      insestCoordenadaX: String;
      insestCoordenadaY: String;
      insestDireccion: String;
      insestEstado: number;
      insestGeocodigo: String;
      nombreTipo: String;
      parCodigo: number;
      disparCodigo:number;
}