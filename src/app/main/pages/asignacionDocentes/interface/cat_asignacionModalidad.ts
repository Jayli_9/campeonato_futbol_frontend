export interface CatAsignacionModalidad{
        dismoCodigo: number;
        mmodCodigo: number;
        curextCodigo: number;
        cupaextCodigo: number;
        modCodigo: number;
        docCodigo: number;
        dismoEstado: number;
}