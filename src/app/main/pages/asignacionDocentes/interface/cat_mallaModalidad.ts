export interface CatMallaModalidad{
            mmodCodigo: number;
            mmodNemonico: String;
            mmodHoras: number;
            mmodEstado: number;
            reanleCodigo: number;
            curextCodigo: number;
}