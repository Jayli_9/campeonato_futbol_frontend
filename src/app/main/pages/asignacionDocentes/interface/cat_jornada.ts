export interface CatJoranada{
    jorCodigo: number;
    jorNombre: String;
    jorEstado: number;
    jorFechaCreacion: Date;
}