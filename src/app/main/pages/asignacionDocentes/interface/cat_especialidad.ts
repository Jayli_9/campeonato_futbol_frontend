export interface CatEspecialidad{
    codigoFiguraProfesional: number;
    espCodigo: number;
    espDescripcion: String;
    espEstado: number;
    espFechaCreacion: Date;
}