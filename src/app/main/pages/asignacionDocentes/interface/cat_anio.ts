export interface CatAnioElectivo{
      anilecCodigo: number;
      descripcionAnioLectivo: String;
      reanleCodigo: number;
      reanleEstado: number;
      reanleFechaCreacion: Date;
      reanleTipo: String;
      regCodigo: number;
}