export interface CatModalidad{
    modCodigo: number;
    modNombre: String;
    modEstado: number;
    modFechaCreacion: Date;
}