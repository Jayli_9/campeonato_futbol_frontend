import {Component, OnInit, ViewChild} from '@angular/core';
import {EjeConstitutivo} from '../../interfaces/eje-constitutivo';
import {UntypedFormBuilder, UntypedFormGroup, Validators} from '@angular/forms';
import {MatTableDataSource} from '@angular/material/table';
import {MatPaginator} from '@angular/material/paginator';
import {MatSort} from '@angular/material/sort';
import {NgbModal} from '@ng-bootstrap/ng-bootstrap';
import {NgxSpinnerService} from 'ngx-spinner';
import Swal from 'sweetalert2';
import {RespuestaEjeConstitutivo} from '../../interfaces/respuesta-eje-constitutivo';
import {EjeConstitutivoService} from '../../services/eje-constitutivo.service';


@Component({
    selector: 'app-eje-constitutivo',
    templateUrl: './eje-constitutivo.component.html',
    styleUrls: ['./eje-constitutivo.component.scss']
})
export class EjeConstitutivoComponent implements OnInit {
    public contentHeader: object;
    public frmEjeCreateEdit: UntypedFormGroup;
    public submitted = false;
    listaEjes: EjeConstitutivo[] = null;
    columnasEjes = ['codigo', 'descripcion', 'estado', 'acciones'];
    dataSource: MatTableDataSource<EjeConstitutivo>;

    @ViewChild(MatPaginator) paginator: MatPaginator;
    @ViewChild(MatSort) sort: MatSort;

    constructor(
        private readonly fb: UntypedFormBuilder,
        private modalService: NgbModal,
        public spinner: NgxSpinnerService,
        private _ejeService: EjeConstitutivoService
    ) {
    }

    ngOnInit(): void {
        this.contentHeader = {
            headerTitle: 'Eje Constitutivo',
            actionButton: false,
            breadcrumb: {
                type: '',
                links: [
                    {
                        name: 'Inicio',
                        isLink: true,
                        link: '/pages/inicio'
                    },
                    {
                        name: 'Eje Constitutivo',
                        isLink: false
                    },
                ]
            }
        };
        this.listarTodosLosEjes();
    }

    listarTodosLosEjes() {
        this.spinner.show();
        this._ejeService.listarTodasLosEjesConstitutivos().subscribe(
            (respuesta: RespuestaEjeConstitutivo) => {
                this.listaEjes = respuesta.listado;
                this.dataSource = new MatTableDataSource(this.listaEjes);
                this.dataSource.paginator = this.paginator;
                this.dataSource.paginator._intl.itemsPerPageLabel = 'Ejes por página';
                this.dataSource.paginator._intl.nextPageLabel = 'Siguiente';
                this.dataSource.paginator._intl.previousPageLabel = 'Anterior';
                this.dataSource.sort = this.sort;
                this.spinner.hide();
            },
            (error: any) => {
                this.spinner.hide();
                Swal.fire('Ups! ocurrió un error al cargar Ejes', '', 'error');
            }
        );
    }

    nuevoEje(modalForm) {
        this.submitted = false;
        this.frmEjeCreateEdit = this.fb.group({
            descripcion: ['', [Validators.required,Validators.maxLength(20)]],
        });
        this.modalService.open(modalForm);
    }

    editEje(modalForm, ejeEdit: EjeConstitutivo) {
        this.submitted = false;
        this.frmEjeCreateEdit = this.fb.group({
            codigo: [ejeEdit.ejeCodigo],
            descripcion: [ejeEdit.ejeDescripcion, [Validators.required]],
            estado: [ejeEdit.ejeEstado]
        });
        this.modalService.open(modalForm);
    }

    guardarEje(modalForm) {

        this.spinner.show();
        this.submitted = true;
        if (this.frmEjeCreateEdit.invalid) {
            this.spinner.hide();
            return;
        }
        let ejeNuevo: EjeConstitutivo = {
            ejeDescripcion: this.frmEjeCreateEdit.get('descripcion').value,
            ejeEstado: 1
        };
        this._ejeService.guardarActualizarEjeConstitutivo(ejeNuevo).subscribe(
            (respuesta: any) => {
                Swal.fire(`Eje Constitutivo Creado!`, '', 'success');
                this.spinner.hide();
                this.listarTodosLosEjes();

            },
            (error: any) => {
                Swal.fire(`No se pudo crear el Eje Constitutivo`, '', 'error');
                this.spinner.hide();
                this.listarTodosLosEjes();
            }
        );
        modalForm.close('Accept click');
    }

    activarInactivar(ejeActualizar: EjeConstitutivo) {
        this.spinner.show();
        if (ejeActualizar.ejeEstado == 1) {
            this._ejeService.inactivarEje(ejeActualizar.ejeCodigo).subscribe(
                (respuesta: any) => {
                    this.spinner.hide();
                    Swal.fire(' Eje Inactivado', '', 'success');
                    this.listarTodosLosEjes();
                },
                (error: any) => {
                    this.spinner.hide();
                    Swal.fire('Ups! ocurrió un error', '', 'error');
                    this.listarTodosLosEjes();
                }
            );
        } else {
            ejeActualizar.ejeEstado = 1;
            this._ejeService.guardarActualizarEjeConstitutivo(ejeActualizar).subscribe(
                (respuesta: any) => {
                    this.spinner.hide();
                    Swal.fire(' Eje Activado', '', 'success');
                    this.listarTodosLosEjes();
                },
                (error: any) => {
                    this.spinner.hide();
                    Swal.fire('Ups! ocurrió un error', '', 'error');
                    this.listarTodosLosEjes();
                }
            );
        }

    }

    actualizarEje(modalForm) {
        this.spinner.show();
        this.submitted = true;
        // stop here if form is invalid
        if (this.frmEjeCreateEdit.invalid) {
            this.spinner.hide();
            return;
        }
        let ejeGuardar: EjeConstitutivo = {
            ejeCodigo: this.frmEjeCreateEdit.get('codigo').value,
            ejeDescripcion: this.frmEjeCreateEdit.get('descripcion').value,
            ejeEstado: this.frmEjeCreateEdit.get('estado').value,
        };
        this._ejeService.guardarActualizarEjeConstitutivo(ejeGuardar).subscribe(
            (respuesta: any) => {
                Swal.fire(`Eje Constitutivo Actualizado!`, '', 'success');
                this.spinner.hide();
                this.listarTodosLosEjes();
            },
            (error: any) => {
                Swal.fire(`No se pudo guardar el Eje Constitutivo`, '', 'error');
                this.spinner.hide();
                this.listarTodosLosEjes();
            }
        );
        modalForm.close('Accept click');
    }

    applyFilter(event: Event) {
        const filterValue = (event.target as HTMLInputElement).value;
        this.dataSource.filter = filterValue.trim().toLowerCase();

        if (this.dataSource.paginator) {
            this.dataSource.paginator.firstPage();
        }
    }

    get ReactiveFrmEjeCreateEdit() {
        return this.frmEjeCreateEdit.controls;
    }

}
