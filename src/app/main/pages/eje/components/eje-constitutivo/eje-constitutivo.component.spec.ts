import { ComponentFixture, TestBed } from '@angular/core/testing';

import { EjeConstitutivoComponent } from './eje-constitutivo.component';

describe('EjeAcademicoComponent', () => {
  let component: EjeConstitutivoComponent;
  let fixture: ComponentFixture<EjeConstitutivoComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ EjeConstitutivoComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(EjeConstitutivoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
