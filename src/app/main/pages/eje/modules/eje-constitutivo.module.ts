import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {RouterModule} from '@angular/router';
import {RUTA_EJE} from '../routes/eje-routing/eje-routing.module';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {MaterialModule} from '../../../shared/material/material.module';
import {ContentHeaderModule} from '../../../../layout/components/content-header/content-header.module';
import {CoreCommonModule} from '../../../../../@core/common.module';
import {EjeConstitutivoComponent} from '../components/eje-constitutivo/eje-constitutivo.component';



@NgModule({
  declarations: [
      EjeConstitutivoComponent
  ],
  imports: [
    CommonModule,
    RouterModule.forChild(RUTA_EJE),
    FormsModule,
    ReactiveFormsModule,
    MaterialModule,
    ContentHeaderModule,
    CoreCommonModule

  ]
})
export class EjeConstitutivoModule { }
