import {RespuestaGeneralInterfaz} from '../../../shared/interfaces/respuesta-general-interfaz';
import {EjeConstitutivo} from './eje-constitutivo';

export interface RespuestaEjeConstitutivo extends RespuestaGeneralInterfaz{
    listado: EjeConstitutivo[];
    objeto: EjeConstitutivo;
}
