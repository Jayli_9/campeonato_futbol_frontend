import { TestBed } from '@angular/core/testing';

import { EjeConstitutivoService } from './eje-constitutivo.service';

describe('EjeConstitutivoService', () => {
  let service: EjeConstitutivoService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(EjeConstitutivoService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
