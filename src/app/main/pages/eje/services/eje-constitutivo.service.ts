import {Injectable} from '@angular/core';
import {environment} from '../../../../../environments/environment';
import {HttpClient} from '@angular/common/http';
import {EjeConstitutivo} from '../interfaces/eje-constitutivo';
import {RespuestaAreaConocimientoInterfaz} from '../../area-conocimiento/interfaces/respuesta-area-conocimiento-interfaz';
import {RespuestaEjeConstitutivo} from '../interfaces/respuesta-eje-constitutivo';

@Injectable({
    providedIn: 'root'
})
export class EjeConstitutivoService {
    url_academico = environment.url_academico;

    constructor(
        public _http: HttpClient
    ) {
    }

    guardarActualizarEjeConstitutivo(ejeConstitutivo: EjeConstitutivo) {
        let url_ws = `${this.url_academico}/private/guardarEje`;
        return this._http.post(url_ws, ejeConstitutivo);
    }

    listarTodasLosEjesConstitutivos() {
        let url_ws = `${this.url_academico}/private/listarTodosLosEjes`;
        return this._http.get<RespuestaEjeConstitutivo>(url_ws);
    }

    listarEjesConstitutivosActivos() {
        let url_ws = `${this.url_academico}/private/listarEjesActivos`;
        return this._http.get<RespuestaEjeConstitutivo>(url_ws);
    }

    buscarEjeConstitutivoPorCodigo(codigo: number) {
        let url_ws = `${this.url_academico}/private/buscarEjePorCodigo/${codigo}`;
        return this._http.get<RespuestaEjeConstitutivo>(url_ws);
    }

    inactivarEje(codigo: number) {
        let url_ws = `${this.url_academico}/private/eliminarEjePorId/${codigo}`;
        return this._http.delete(url_ws);
    }
}
