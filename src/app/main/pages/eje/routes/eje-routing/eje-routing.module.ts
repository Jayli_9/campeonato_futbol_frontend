import {Routes} from '@angular/router';
import {AuthGuard} from '../../../../../auth/helpers';
import {EjeConstitutivoComponent} from '../../components/eje-constitutivo/eje-constitutivo.component';

export const RUTA_EJE: Routes = [
    {
        path: 'eje-constitutivo',
        component: EjeConstitutivoComponent,
        canActivate: [AuthGuard]
    }
];
