import { Routes } from "@angular/router";
import { AuthGuard } from "app/auth/helpers/auth.guards";
import { ReporteNivelComponent } from "../componentes/reporte-nivel/reporte-nivel.component";

export const RUTA_REPORTE_NIVEL:Routes=[
    {
        path:'reporteBoletasTutor',
        component:ReporteNivelComponent
    }
]