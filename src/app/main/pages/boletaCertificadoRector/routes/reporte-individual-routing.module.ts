import { Routes } from "@angular/router";
import { AuthGuard } from "app/auth/helpers/auth.guards";
import { ReporteIndividualComponent } from "../componentes/reporte-individual/reporte-individual.component";

export const RUTA_REPORTE_INDIVIDUAL:Routes=[
    {
        path:'certificado-promocion-rector',
        component:ReporteIndividualComponent,
        canActivate:[AuthGuard]
    }
]