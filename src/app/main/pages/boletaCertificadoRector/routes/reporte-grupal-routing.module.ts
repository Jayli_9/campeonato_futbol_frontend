import { Routes } from "@angular/router";
import { AuthGuard } from "app/auth/helpers/auth.guards";
import { ReporteGrupalComponent } from "../componentes/reporte-grupal/reporte-grupal.component";


export const RUTA_REPORTE_GRUPAL:Routes=[
    {
        path:'reporteGrupal',
        component:ReporteGrupalComponent
    }
]