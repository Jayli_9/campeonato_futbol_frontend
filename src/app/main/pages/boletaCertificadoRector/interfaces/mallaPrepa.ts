export interface mallaPrepaInterface{
     maasCodigo: number;
     curCodigo: number;
     maasEstado: number;
     maasHoras: number;
     maasNemonico: number;
     reanleCodigo: number;
     insestCodigo: number;
     acaAsignatura: null,
     acaAsignaturaPrepa: {
       apreCodigo: number;
       apreDescripcion: string;
       apreEstado: number;
       graCodigo: number;
       apreHras: number;
       resAgrupacion: string;
    }
}