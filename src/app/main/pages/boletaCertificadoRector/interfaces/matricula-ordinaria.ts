import { estudianteInterface } from "../../asistenciaOrdinaria/interfaces/estudiante";
import { matriculaEstudianteInterface } from "../../asistenciaOrdinaria/interfaces/matriculaEstudiante";

export interface MatriculaOrdinaria {
    matCodigo:number;
    curCodigo:number;
    curparCodigo:number;
    reanleCodigo:number;
    matTipoProceso:number;
    estudiante:matriculaEstudianteInterface;
}
