export interface Provincia {
    proCodigo:number;
    proDescripcion:string;
    proEstado:number;
}
