import { MatriculaOrdinaria } from "./matricula-ordinaria";

export interface ParametrosCertificado {
    codigoAmie:string;
    institucion:string;
    anioLectivo:string;
    regimen:string;
    modalidad:string;
    jornada:string;
    matricula:MatriculaOrdinaria;
    gradoActual:string;
    gradoSiguiente:string;
    promedio:string;
    fecha:string;
    promedioLetras:string;
    provincia:string;
}
