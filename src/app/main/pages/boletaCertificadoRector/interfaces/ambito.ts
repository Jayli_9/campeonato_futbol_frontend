export interface ambitoInterface{
    ambCodigo: number;
    apreCodigo: number;
    ambDescripcion: string;
    ambEstado: number;
}