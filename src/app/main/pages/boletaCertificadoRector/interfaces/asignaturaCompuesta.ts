export interface asignaturaCompuestaInterface{
       asiCodigo: number;
       asiDescripcion: string;
       asiHoras: number;
       asiEstado: number;
       asiNemonico: number;
       espCodigo: number;
       graCodigo: number;
       acaAgrupacion: {
         agrCodigo: number;
         agrDescripcion: string;
         agrEstado: number;
         agrNemonico: string;
      },
       acaAreaConocimiento: {
         arcoCodigo: number;
         arcoDescripcion: string;
         arcoEstado: number;
         arcoNemonico: string;
      },
       acaTipoAsignatura: {
         tiasCodigo: number;
         tiasDescripcion: string;
         tiasEstado: number;
         tiasObligatorio: number;
      },
      acaTipoValoracion: {
         tivaCodigo: number;
         tivaDescripcion: string;
         tivaEstado: number;
      }
}