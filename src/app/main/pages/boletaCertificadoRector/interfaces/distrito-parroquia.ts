import { Canton } from "./canton";

export interface DistritoParroquia {
    disparCodigo:number;
    disparDescripcion:string;
    disparEstado:number;
    codigoParroquia?:number;
    nombreParroquia?:string;
    codigoCanton?:number;
    nombreCanton?:string;
    codigoProvincia?:number;
    nombreProvincia?:string;
}
