export interface calificacionMogenInterface{
     calCodigo: number;
     calDescripcion: string;
     calEstado: number;
     calNemonico: string;
     calPorcentaje: number;
     reanleCodigo: number;
     sereduCodigo: number;
     nroParcial: number;
     tipoNemonioCalificacionEnum: string;
     modevaGen: {
       modgenCodigo: number;
       acaModeloEvaluacion: {
         moevCodigo: number;
         moevDescripcion: string;
         moevEstado: number;
         moevNemonico: string;
      },
       modgenDescripcion: string;
       modgenNemonico: string;
       modgenEstado: number;
       reanleCodigo: number;
       modgenNivel: number;
    },
     calMinimo: number;
     calMaximo: number;
}