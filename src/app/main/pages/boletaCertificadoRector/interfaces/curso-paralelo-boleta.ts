export interface CursoParaleloBoleta {
    codigoEstablecimiento:number;
    codigoReanle:number;
    codigoGrado:number;
    codigoJornada:number;
    codigoEspecialidad:number;
}
