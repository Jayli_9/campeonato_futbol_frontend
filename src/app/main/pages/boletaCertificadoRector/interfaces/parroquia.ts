import { Canton } from "./canton";

export interface Parroquia {
    parCodigo:number;
    parDescripcion:string;
    parEstado:number;
    catCanton:Canton;
}
