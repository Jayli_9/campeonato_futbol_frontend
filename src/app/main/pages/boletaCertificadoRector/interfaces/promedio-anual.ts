export interface PromedioAnualInterface{
     rproCodigo:number;
	matCodigo:number;
	curparCodigo:number;
	rproPromedio:string;
	rproAprobado:string;
	rproEstado:number;
}