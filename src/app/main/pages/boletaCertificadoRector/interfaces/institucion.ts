import { InstitucionRegimen } from "./institucion-regimen";

export interface Institucion {
    insCodigo: number;
    insDescripcion: string;
    insEstado: number;
    insAmie:string;
    regimenes:InstitucionRegimen[];
}