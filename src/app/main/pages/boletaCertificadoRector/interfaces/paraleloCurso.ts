export interface curParaleloInterface {
    curparCodigo: number;
    curparAforo: number;
    curparBancas: number;
    curparEstado: number;
    curparFechaCreacion: Date;
    idiEtnCodigo: number;
    curparNumEstud: number;
    curCodigo: number;
    parCodigo: number;
    nombreParalelo: string;
}