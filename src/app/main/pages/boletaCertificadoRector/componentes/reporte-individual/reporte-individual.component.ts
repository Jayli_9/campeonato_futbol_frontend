import { Component, OnInit, ViewChild } from '@angular/core';
import { UntypedFormBuilder, UntypedFormGroup, Validators } from '@angular/forms';
import { MatDialog } from '@angular/material/dialog';
import { MatPaginator } from '@angular/material/paginator';
import { MatTableDataSource } from '@angular/material/table';
import { User } from 'app/auth/models/user';
import { CatEstablecimiento } from 'app/main/pages/asignacionDocentes/interface/cat_establecimiento';
import { CatRegimenAnioLectivo } from 'app/main/shared/interfaces/cat-regimen-anio-lectivo';
import { ResponseGenerico } from '../../interfaces/response-generico';
import { GieeService } from '../../services/giee.service';
import { CatalogoService } from '../../services/catalogo.service';
import { CatNivel } from '../../interfaces/cat-nivel';
import { CatJoranada } from 'app/main/pages/asignacionDocentes/interface/cat_jornada';
import { CatGradosCursos } from 'app/main/pages/asignacionDocentes/interface/cat_gradosCursos';
import { Institucion } from '../../interfaces/institucion';
import { NivelPermisoIntitucionInterface } from 'app/main/pages/asignacionDocentes/interface/nivel-Institucion';
import { OfertaService } from '../../services/oferta.service';
import { CursoParalelo } from 'app/main/pages/asistencia-estudiantes/interfaces/curso-paralelo';
import { MatriculaOrdinaria } from '../../interfaces/matricula-ordinaria';
import { MatriculaService } from '../../services/matricula.service';
import { NgxSpinnerService } from 'ngx-spinner';
import { PdfCertificadoService } from '../../services/pdf-certificado.service';
import { CatModalidad } from 'app/main/pages/asignacionDocentes/interface/cat_modalidad';
import { AcademicoService } from '../../services/academico.service';
import { PromedioAnualInterface } from '../../interfaces/promedio-anual';
import { CatEspecialidad } from 'app/main/pages/asignacionDocentes/interface/cat_especialidad';
import { cursoInterface } from '../../interfaces/curso';
import { orderBy } from 'lodash-es';
import { Parroquia } from '../../interfaces/parroquia';
import { DistritoParroquia } from '../../interfaces/distrito-parroquia';
import Swal from 'sweetalert2';
import pdfMake from 'pdfmake/build/pdfmake';
import pdfFonts from 'pdfmake/build/vfs_fonts';
import { notaDestrezaInterface } from '../../interfaces/nota-Destreza';
import { notaAsignaturaInterface } from '../../interfaces/nota-Asignatura';
import { forkJoin } from 'rxjs';
pdfMake.vfs = pdfFonts.pdfMake.vfs;

@Component({
  selector: 'app-reporte-individual',
  templateUrl: './reporte-individual.component.html',
  styleUrls: ['./reporte-individual.component.scss']
})
export class ReporteIndividualComponent implements OnInit {

  columns: string[] = ['nombre','boleta', 'certificado'];
  currentUser: User;
  dataSource = new MatTableDataSource<Object>();
  @ViewChild(MatPaginator) paginator: MatPaginator;

  public frmCertificado: UntypedFormGroup;
  public contentHeader: object;
  institucion:Institucion;
  listaEstablecimiento:CatEstablecimiento[]=[];
  listaRegimenesActual:CatRegimenAnioLectivo[]=[];
  listaNiveles:CatNivel[]=[];
  listaJornadas: CatJoranada[]=[];
  listaGrados: CatGradosCursos[]=[];
  listaTodosGrados: CatGradosCursos[]=[];
  listaModalidades: CatModalidad[]=[];
  listaPermisosNivel: NivelPermisoIntitucionInterface[]=[];
  listaParalelos: CursoParalelo[]=[];
  listaMatriculas: MatriculaOrdinaria[]=[];
  cargando: boolean = false;
  promedio:PromedioAnualInterface;
  listaEspecialidades: CatEspecialidad[]=[];
  listaCursos: cursoInterface[]=[];
  distritoParroquia:DistritoParroquia;


  listaMaterias: notaAsignaturaInterface[]=[];
  listaMateriasPrepa: notaDestrezaInterface[]=[];
  Amie;

  //datos de institucion
  nombreInstitucion: string;
  grado;
  paralelo;
  nombreEspecialidad; 
  nombreDocente: string;

  //PDF
  nombreAñoElectivo;
  nombreGrado;
  nombreJornada;
  nombreParalelo;


  //filtros para prepa
  acumuladorAmbitoDestreza = [];

  //datos para pdf
  nombreDestreza;
  objetoDatosRecolectados =[];
  objetoDatosRecolectadosN =[];
  datosRelacionados = [];
  datosRelacionadosB = [];
  notaPrimerTrimestre;
  notaSegundoTrimestre;
  notaTercerTrimestre;
  promedioTrimestre;
  supletorio;
  promedioAnual;
  promedioAnualTotal;
  notaPonderacion;
  promedioFinal;

  descripcion1;
  descripcion2;
  descripcion3;
  listaDescripcion = [];

  constructor(private readonly fb: UntypedFormBuilder,
    private dialog: MatDialog,
    private readonly gieeService: GieeService,
            readonly catalogoService: CatalogoService,
            readonly ofertaService: OfertaService,
            readonly matriculaService:MatriculaService,
            public spinner: NgxSpinnerService,
            private pdfCertificadoService: PdfCertificadoService,
            readonly academicoService: AcademicoService) {
      this.currentUser = JSON.parse(localStorage.getItem('currentUser'));
     }

  ngOnInit(): void {
    this.initForm();
    this.buscarInstitucionPorAmie();
    this.listarTodosGrados();
    this.listarModalidad();
  }

  initForm() {
    this.frmCertificado = this.fb.group({
      codigoEstablecimiento: [0, [Validators.required]],
      codigoRegimen: [0, [Validators.required]],
      codigoNivel: [0, [Validators.required]],
      codigoJornada: [0, [Validators.required]],
      codigoGrado: [0, [Validators.required]],
      codigoParalelo: [0, [Validators.required]],
      codigoEspecialidad: [0]
    });
  }

  get registerFormControl() {
    return this.frmCertificado.controls;
  }

  listarTodosGrados(){
    this.catalogoService.listarTodosGrados().subscribe(
      (respuesta: ResponseGenerico<CatGradosCursos>) => {
        this.listaTodosGrados = respuesta.listado;
      })
  }

  listarModalidad(){
    this.catalogoService.listarModalidad().subscribe(
      (respuesta: ResponseGenerico<CatModalidad>) => {
        this.listaModalidades = respuesta.listado;
      })
  }

  buscarInstitucionPorAmie() {
    this.spinner.show();
    this.Amie = this.currentUser.sede.nemonico;
    this.gieeService.buscarInstitucionPorAmie(this.currentUser.sede.nemonico).subscribe(
      (respuesta: ResponseGenerico<Institucion>) => {
        this.institucion = respuesta.objeto;
        this.nombreInstitucion = this.institucion.insDescripcion;
        this.listarEstablecimientosPorAmie();
      })
  }

  listarEstablecimientosPorAmie() {
    this.gieeService.listarEstablecimientosPorAmie(this.currentUser.sede.nemonico).subscribe(
      (respuesta: ResponseGenerico<CatEstablecimiento>) => {
        this.listaEstablecimiento = respuesta.listado;
        this.spinner.hide();
      })
  }

  buscarDistritoParroquiaPorCodigo(codigoDistritoParroquia:number){
    this.catalogoService.buscarDistritoParroquiaPorCodigo(codigoDistritoParroquia).subscribe(
      (respuesta: ResponseGenerico<DistritoParroquia>) => {
        this.distritoParroquia = respuesta.objeto;
      })
  }

  seleccionarEstablecimiento(){
    let itemEstablecimiento = this.listaEstablecimiento.find((item) => item.insestCodigo == this.frmCertificado.get('codigoEstablecimiento').value);
    this.buscarDistritoParroquiaPorCodigo(itemEstablecimiento.disparCodigo)
    let regCodigos: number[] = this.institucion.regimenes.map(regimen => regimen.regCodigo);
    this.listarRegimenAnioLectivoPorTipo(regCodigos);
  };

  listarRegimenAnioLectivoPorTipo(idRegimen:number[]){
    this.spinner.show();
    this.catalogoService.listarRegimenAnioLectivoPorCodigoRegimen(idRegimen).subscribe(
      (respuesta: ResponseGenerico<CatRegimenAnioLectivo>) => {
        this.listaRegimenesActual = respuesta.listado;
        this.listaRegimenesActual.sort((a, b) => a.reanleCodigo - b.reanleCodigo);
        this.spinner.hide();
      })
  }

  seleccionarRegimen(){
    this.buscarNivelesPermiso();

    try{
      const codigoRegimen = this.frmCertificado.get('codigoRegimen').value;
      const lista = this.listaRegimenesActual.filter(element => element.reanleCodigo === parseInt(codigoRegimen));
    
      lista.forEach(element => {
      this.nombreAñoElectivo = element.descripcionAnioLectivo;
    });
    }catch{

    }

  };

  buscarNivelesPermiso(){
    this.gieeService.buscarNivelPermisoPorCodigoInstitucion(this.institucion.insCodigo).subscribe(
      (respuesta: ResponseGenerico<NivelPermisoIntitucionInterface>) => {
        this.listaPermisosNivel = respuesta.listado;
        this.listarNiveles();
      })
  }

  listarNiveles() {
    this.catalogoService.listarNiveles().subscribe(
        (respuesta: ResponseGenerico<CatNivel>) => {
            this.listaNiveles = respuesta.listado;
            let listaNivelesFiltrados = this.listaPermisosNivel
                .map(element => this.listaNiveles.find(item => item.nivCodigo === element.nivCodigo))
                .filter((nivel, index, array) => array.findIndex(item => item?.nivCodigo === nivel?.nivCodigo) === index);
            this.listaNiveles = listaNivelesFiltrados;
            this.listaNiveles.sort((a, b) => a.nivCodigo - b.nivCodigo);
        });
  }

  seleccionarNivel(){
    this.nombreEspecialidad = undefined;
    this.listaGrados=[];
    this.listaJornadas=[];
    this.listaEspecialidades=[];
    this.listaParalelos=[];
    this.frmCertificado.get('codigoGrado').setValue(0);
    this.frmCertificado.get('codigoEspecialidad').setValue(0);
    this.frmCertificado.get('codigoJornada').setValue(0);
    this.frmCertificado.get('codigoParalelo').setValue(0);
    this.listarJornadas();
    this.listarGrados();
  };

  listarJornadas(){
    const filtroJornadas = {
      1: [1, 1],
      2: [2, 2],
      3: [3, 3],
      4: [1, 3],
      5: [1, 2],
      6: [2, 3],
      7: [1, 2, 3]};
    this.catalogoService.listarJornadas().subscribe(
      (respuesta: ResponseGenerico<CatJoranada>) => {
        this.listaJornadas = respuesta.listado;
        let foundItem = this.listaPermisosNivel.find((item) => item.nivCodigo == this.frmCertificado.get('codigoNivel').value);
        if (foundItem.jorCodigo in filtroJornadas) {
          let codigosFiltro = filtroJornadas[foundItem.jorCodigo];
          this.listaJornadas = this.listaJornadas.filter(item => codigosFiltro.includes(item.jorCodigo));
        }
      })
  }

  seleccionarJornada(){
    this.listaParalelos=[];
    this.listaMatriculas=[];
     this.dataSource= new MatTableDataSource<MatriculaOrdinaria>(this.listaMatriculas);
    this.frmCertificado.get('codigoParalelo').setValue(0);
    this.listarParaleloGrados();

    try{
      const lista = this.listaJornadas.filter(element => element.jorCodigo === parseInt(this.frmCertificado.get('codigoJornada').value));

      lista.forEach(element => {
        this.nombreJornada = element.jorNombre;
      });

    }catch{

    }
  };

  listarGrados(){
    this.catalogoService.listarGradosPorNivel(this.frmCertificado.get('codigoNivel').value).subscribe(
      (respuesta: ResponseGenerico<CatGradosCursos>) => {
        this.listaGrados = respuesta.listado;
      })
  }

  seleccionarGrado(){ 
    this.frmCertificado.get('codigoJornada').setValue(0);
    this.frmCertificado.get('codigoParalelo').setValue(0);
    this.listaEspecialidades=[];
    this.listaMatriculas=[];
     this.dataSource= new MatTableDataSource<MatriculaOrdinaria>(this.listaMatriculas);
    if(this.frmCertificado.get('codigoNivel').value==8){
      this.spinner.show();
      this.listarCursoBachillerato();
    }

    try{
      const lista = this.listaGrados.filter(elemet => elemet.graCodigo === parseInt(this.frmCertificado.get('codigoGrado').value))

      lista.forEach(element => {
        this.nombreGrado = element.graDescripcion;
      });
    } catch{

    }

  };

  listarCursoBachillerato(){
    let establecimientos=[]
    establecimientos.push(this.frmCertificado.get('codigoEstablecimiento').value)
    let parametro = {
      reanleCodigo: this.frmCertificado.get('codigoRegimen').value,
      insestCodigo: establecimientos,
      modCodigo: 0,
      graCodigo: this.frmCertificado.get('codigoGrado').value
    }
    this.ofertaService.buscarCursoPorCodRegAnioLectivoGradoEstablecimiento(parametro).subscribe(
      (respuesta: ResponseGenerico<cursoInterface>) => {
        let codigosEspecializado=[];
        this.listaCursos = respuesta.listado;
        this.listaCursos.forEach((curso) => {
          codigosEspecializado.push(curso.espCodigo);
      });
      this.listarEspecialidades(codigosEspecializado);
      })
  }

  listarEspecialidades(codigoEspecialidades:number[]){
    let parametro = {
      listaEspCodigo: codigoEspecialidades,
    }
    this.catalogoService.listarEspecialidades(parametro).subscribe(
      (respuesta: ResponseGenerico<CatEspecialidad>) => {
        this.listaEspecialidades = respuesta.listado;
        this.spinner.hide();
      })
  }

  seleccionarEspecialidad(){
    this.frmCertificado.get('codigoJornada').setValue(0);
    this.frmCertificado.get('codigoParalelo').setValue(0);
    this.listaMatriculas=[];
     this.dataSource= new MatTableDataSource<MatriculaOrdinaria>(this.listaMatriculas);

     try{
      const lista = this.listaEspecialidades.filter(obj => obj.espCodigo === parseInt(this.frmCertificado.get('codigoEspecialidad').value))

      lista.forEach(element => {
        this.nombreEspecialidad = element.espDescripcion;
        if(typeof this.nombreEspecialidad === 'undefined'){
          this.nombreEspecialidad = " "
        }
      });
     } catch{

     }

  }

  listarParaleloGrados(){
    this.spinner.show();
    let parametro={
      codigoEstablecimiento:this.frmCertificado.get('codigoEstablecimiento').value,
      codigoReanle:this.frmCertificado.get('codigoRegimen').value,
      codigoGrado:this.frmCertificado.get('codigoGrado').value,
      codigoJornada:this.frmCertificado.get('codigoJornada').value,
      codigoEspecialidad:this.frmCertificado.get('codigoEspecialidad').value?this.frmCertificado.get('codigoEspecialidad').value:null
    }
    this.ofertaService.listarParalelosCertificadoPromocion(parametro).subscribe(
      (respuesta: ResponseGenerico<CursoParalelo>) => {
        this.listaParalelos = respuesta.listado;
        this.listaParalelos.sort((a, b) => a.parCodigo - b.parCodigo);
        this.spinner.hide();
      })
  }

  seleccionarParalelo(){
    this.listarEstudiantesParalelo();

    try{
      const lista = this.listaParalelos.filter(element => element.curparCodigo === parseInt(this.frmCertificado.get('codigoParalelo').value))

      lista.forEach(element => {
        this.nombreParalelo = element.nombreParalelo;
      });
    }catch{

    }

  };

  listarEstudiantesParalelo(){
    this.spinner.show();
    this.matriculaService.listaMatriculasPorRegimenAnioLectivoYParalelo(this.frmCertificado.get('codigoRegimen').value,this.frmCertificado.get('codigoParalelo').value).subscribe(
      (respuesta: ResponseGenerico<MatriculaOrdinaria>) => {
        this.listaMatriculas = respuesta.listado;
        this.listaMatriculas = orderBy(this.listaMatriculas, ['estudiante.estNombres'], ['asc']);
        this.dataSource= new MatTableDataSource<MatriculaOrdinaria>(this.listaMatriculas);
        this.dataSource.paginator = this.paginator;
        this.spinner.hide();
      })
  }

  obtenerValoresUnicos(data, columna) {
    const valoresUnicos = new Set();
    // Iterar sobre cada objeto en el arreglo
    data.forEach(objeto => {
      // Agregar el valor de la columna deseada al conjunto
      valoresUnicos.add(objeto[columna]);
    });
    // Convertir el conjunto a un arreglo si es necesario
    return Array.from(valoresUnicos);
  }

  async crearFiltroNotas(element, listaMaterias, objMetodoDestreza){

    try{
  
      let unique = {};
    const datos = objMetodoDestreza.filter(obj => {
      const key = obj.nombreAmbito + '|' + obj.nombreDestreza;
      if (!unique[key]) {
          unique[key] = true;
          return true;
      }
      return false;
  });
  
    listaMaterias.forEach(materia => {
      materia.notas.forEach(notas => {
        datos.forEach(eleccion => {       
          if(notas.nombreDestreza === eleccion.nombreDestreza && notas.nombreAmbito === eleccion.nombreAmbito){
            if(notas.identificador === "TRIMESTRE 1"){
              if(notas.nota != null && notas.nota != 0){ 
                eleccion.nota1 = notas.nota;
              }else{
                eleccion.nota1 = "NE";
              }
            }else if(notas.identificador === "TRIMESTRE 2"){
              if(notas.nota != null && notas.nota != 0){
                eleccion.nota2 = notas.nota;
              }else{
                eleccion.nota2 = "NE";
              }
            }else if(notas.identificador == "TRIMESTRE 3"){
              if(notas.nota != null && notas.nota != 0){
                eleccion.nota3 = notas.nota;
              }else{
                eleccion.nota3 = "NE";
              }
            }
          } 
        });
      });
    });
  
  
    datos.forEach(eleccion => {
      if (!eleccion.hasOwnProperty('nota1') || eleccion.nota1 === null || eleccion.nota1 === undefined) {
        eleccion.nota1 = "NE";
    }
    
    if (!eleccion.hasOwnProperty('nota2') || eleccion.nota2 === null || eleccion.nota2 === undefined) {
        eleccion.nota2 = "NE";
    }
    
    if (!eleccion.hasOwnProperty('nota3') || eleccion.nota3 === null || eleccion.nota3 === undefined) {
        eleccion.nota3 = "NE";
    }
    });
  
    await this.crearPDFPrepa(element, datos.sort());
  
    } catch{
      
    }
  }

  async crearFiltroAmbitoDestreza(element, listaMaterias){
    try{
      this.acumuladorAmbitoDestreza = [];
      listaMaterias.forEach(materia => {
        materia.notas.forEach(nota => {
          let datos = {
            nombreAmbito: nota.nombreAmbito,
            nombreDestreza: nota.nombreDestreza
          }
          this.acumuladorAmbitoDestreza.push(datos);
        });
      });
  
      await this.crearFiltroNotas(element, listaMaterias, this.acumuladorAmbitoDestreza);
    }catch{
  
    }
  }

  async sacarFiltrosPrepa(element, listasMaterias){
    try{
      const conjunto = new Set<string>();
    const datosSinRepetidos = listasMaterias.filter((objeto) => {
      const clave = `${objeto.codigoDestreza}-${objeto.codigoAmbito}-${objeto.identificador}`;
      if (conjunto.has(clave)) {
          return false;
      }
      conjunto.add(clave);
      return true;
  });
    this.spinner.hide();
    const valoresUnicos = this.obtenerValoresUnicos(datosSinRepetidos, 'nombreAmbito');
    const notasMaterias = valoresUnicos.map(obj => {
      return {
          nombreMateria: obj,
          notas: listasMaterias.filter(elemento => elemento.nombreAmbito === obj)
      }
    })
    await this.crearFiltroAmbitoDestreza(element, notasMaterias);

    } catch{

    }
    

  }

  async sacarDataPrepa(element, listaMaterias) {
    try {
        this.objetoDatosRecolectados = [];
        for (const notaSistema of listaMaterias) {
            const datosJSON = JSON.parse(notaSistema.nodeCalNotaDes);
            const realmCodigo = parseInt(this.frmCertificado.get('codigoRegimen').value);
            
            const modelo = await this.academicoService.buscarCalificacionPorMogenCodigo(datosJSON.modelo).toPromise();
            const listaModelo = modelo.listado;
            
            for (const modeloData of listaModelo) {
                if (modeloData.reanleCodigo === realmCodigo) {
                    const ambitoData = await this.academicoService.buscarAmbitoPorCodigo(datosJSON.ambito).toPromise();
                    const ambitos = ambitoData.objeto;
                    
                    const respuesta = await this.academicoService.buscarDestrezasPorCodigo(datosJSON.ambito).toPromise();
                    const listaDestreza = respuesta.listado;
                    
                    for (const jsonData of datosJSON.destrezas) {
                        const seleccionDestreza = listaDestreza.filter(obj => obj.desCodigo === jsonData.desCodigo);
                        for (const destre of seleccionDestreza) {
                            const datosGurdado = {
                                nombreAmbito: ambitos.ambDescripcion,
                                nombreDestreza: destre.desDescripcion,
                                codigoDestreza: jsonData.desCodigo,
                                codigoAmbito: ambitos.ambCodigo,
                                nota: jsonData.resEscala,
                                codigoMatricula: element.matCodigo,
                                identificador: modeloData.calDescripcion
                            };
                            this.objetoDatosRecolectados.push(datosGurdado);

                        }
                    }
                }
            }
        }
        await this.sacarFiltrosPrepa(element, this.objetoDatosRecolectados);
    } catch (error) {
        console.error(error);
    }
}


async sacarData(element, listaMaterias){
  try {
    this.objetoDatosRecolectados = [];
    this.objetoDatosRecolectadosN = [];
    this.spinner.show();
    let Grados = listaMaterias.filter(codigo => codigo.acaMallaAsignatura.acaAsignatura.acaTipoAsignatura.tiasObligatorio === "S");
    let GradosA = listaMaterias.filter(codigo => codigo.acaMallaAsignatura.acaAsignatura.acaTipoAsignatura.tiasObligatorio === "N");
    for (const element of Grados) {
        const nombreMateria = element.acaMallaAsignatura.acaAsignatura.asiDescripcion;
        const datosJSON = JSON.parse(element.noasCalNot);
        for (const jsonData of datosJSON) {
            const respuesta = await this.academicoService.buscarCalificacionPorMogenCodigo(jsonData.modgenCodigo).toPromise();
            let realmCodigo = parseInt(this.frmCertificado.get('codigoRegimen').value);
            let lista = respuesta.listado;
            lista.forEach(calificacion => {
                if(calificacion.reanleCodigo === realmCodigo){
                    jsonData.listaCalificacion.forEach(calificaciones => {
                        let datosGurdado={
                            nombreMateria: nombreMateria,
                            codigoCalificacion: calificacion.calCodigo,
                            nota: calificaciones.noasCalNot,
                            codigoMatricula: element.matCodigo,
                            identificador: calificacion.calDescripcion
                        }
                        this.objetoDatosRecolectados.push(datosGurdado);
                    });
                }
            });
        }
    }
    for (const element of GradosA) {
      const nombreMateria = element.acaMallaAsignatura.acaAsignatura.asiDescripcion;
      const datosJSON = JSON.parse(element.noasCalNot);
      for (const jsonData of datosJSON) {
          const respuesta = await this.academicoService.buscarCalificacionPorMogenCodigo(jsonData.modgenCodigo).toPromise();
          let realmCodigo = parseInt(this.frmCertificado.get('codigoRegimen').value);
          let lista = respuesta.listado;
          lista.forEach(calificacion => {
              if(calificacion.reanleCodigo === realmCodigo){
                  jsonData.listaCalificacion.forEach(calificaciones => {
                      let datosGurdado={
                          nombreMateria: nombreMateria,
                          codigoCalificacion: calificacion.calCodigo,
                          nota: calificaciones.noasCalNot,
                          codigoMatricula: element.matCodigo,
                          identificador: calificacion.calDescripcion
                      }
                      this.objetoDatosRecolectadosN.push(datosGurdado);
                  });
              }
          });
      }
  }
  
  await this.sacarDatosFiltrados(this.objetoDatosRecolectados, this.objetoDatosRecolectadosN, element);
} catch (error) {
    console.error("Error al buscar notas de asignatura:", error);
}
}

async sacarDatosFiltrados(listaData, listaDataN, element){
  this.spinner.hide();
  const valoresUnicos = this.obtenerValoresUnicos(listaData, 'nombreMateria');

  const valoresUnicosB = this.obtenerValoresUnicos(listaDataN, 'nombreMateria');

    const notasMateriasB = valoresUnicosB.map(obj => {
      return {
          nombreMateria: obj,
          notas: listaDataN.filter(elemento => elemento.nombreMateria === obj)
      }
    })

  const notasMaterias = valoresUnicos.map(obj => {
    return {
        nombreMateria: obj,
        notas: listaData.filter(elemento => elemento.nombreMateria === obj)
    }
  })

  if(parseInt(this.frmCertificado.get('codigoGrado').value) >= 4 && parseInt(this.frmCertificado.get('codigoGrado').value) <= 6){
    if(parseInt(this.frmCertificado.get('codigoGrado').value) === 6){
      this.notasElemental2(notasMaterias, notasMateriasB ,element);
    }else{
      this.notasElemental(notasMaterias, notasMateriasB ,element);
    }
    
  }else if(parseInt(this.frmCertificado.get('codigoGrado').value) >=7 && parseInt(this.frmCertificado.get('codigoGrado').value) <= 9){
    //media
    this.notasMedia(notasMaterias, notasMateriasB, element);
  }else if(parseInt(this.frmCertificado.get('codigoGrado').value) >= 10){
    //bachillerato
    this.notaBachillerato(notasMaterias, notasMateriasB ,element);
  }
}

async notasElemental2(notasMaterias, notasMateriasB, element) {
  try {
    const datosRelacionados = [];
    const datosRelacionadosB = [];
    const listaDescripcion = [];

    const obtenerNota = (nota) => nota ? nota : 0;

    const procesarNotas = (datosSistema, datosRelacionados) => {
      datosSistema.notas.forEach(nota => {
        switch (nota.identificador) {
          case "TRIMESTRE 1":
            this.notaPrimerTrimestre = obtenerNota(nota.nota);
            break;
          case "TRIMESTRE 2":
            this.notaSegundoTrimestre = obtenerNota(nota.nota);
            break;
          case "TRIMESTRE 3":
            this.notaTercerTrimestre = obtenerNota(nota.nota);
            break;
          case "PROMEDIO TRIMESTRE":
            this.promedioTrimestre = obtenerNota(nota.nota);
            break;
          case "EVALUACION FINAL":
            this.promedioAnual = obtenerNota(nota.nota);
            break;
            case "PROMEDIO FINAL":
              this.promedioFinal = obtenerNota(nota.nota);
            break;
          default:
            break;
        }
      });

      const datosAlmacenados = {
        nombreMateria: datosSistema.nombreMateria,
        nota1: this.notaPrimerTrimestre,
        nota2: this.notaSegundoTrimestre,
        nota3: this.notaTercerTrimestre,
        promedioTrimestre: this.promedioTrimestre,
        evaluacionFinal: this.promedioAnual,
        promedioFinal: this.promedioFinal
      };

      datosRelacionados.push(datosAlmacenados);
    };

    for (const datosSistema of notasMaterias) {
      procesarNotas(datosSistema, datosRelacionados);
    }

    for (const datosSistema of notasMateriasB) {
      procesarNotas(datosSistema, datosRelacionadosB);

      if (datosSistema.nombreMateria === "ACOMPAÑAMIENTO INTEGRAL EN EL AULA") {
        const respuestas = await forkJoin([
          this.academicoService.buscarPonderacionPorNota(this.notaPrimerTrimestre),
          this.academicoService.buscarPonderacionPorNota(this.notaSegundoTrimestre),
          this.academicoService.buscarPonderacionPorNota(this.notaTercerTrimestre)
        ]).toPromise();

        const descripciones = respuestas.map(respuesta => respuesta.objeto.pocoObs);

        const datosDescripcion = {
          nombreMateria: datosSistema.nombreMateria,
          descripcion1: descripciones[0],
          descripcion2: descripciones[1],
          descripcion3: descripciones[2]
        };

        listaDescripcion.push(datosDescripcion);
      }
    }

    await this.crearPDFElemental2(element, datosRelacionados, datosRelacionadosB, listaDescripcion);
  } catch (error) {
    console.error("Ocurrió un error:", error);
  }
}

async notasElemental(notasMaterias, notasMateriasB, element) {
  try {
    const datosRelacionados = [];
    const datosRelacionadosB = [];
    const listaDescripcion = [];

    const obtenerNota = (nota) => nota ? nota : 0;

    const procesarNotas = (datosSistema, datosRelacionados) => {
      datosSistema.notas.forEach(nota => {
        switch (nota.identificador) {
          case "TRIMESTRE 1":
            this.notaPrimerTrimestre = obtenerNota(nota.nota);
            break;
          case "TRIMESTRE 2":
            this.notaSegundoTrimestre = obtenerNota(nota.nota);
            break;
          case "TRIMESTRE 3":
            this.notaTercerTrimestre = obtenerNota(nota.nota);
            break;
          case "PROMEDIO TRIMESTRE":
            this.promedioTrimestre = obtenerNota(nota.nota);
            break;
          case "SUPLETORIO":
            this.supletorio = obtenerNota(nota.nota);
            break;
          case "EVALUACION FINAL":
            this.promedioAnual = obtenerNota(nota.nota);
            break;
          default:
            break;
        }
      });

      const datosAlmacenados = {
        nombreMateria: datosSistema.nombreMateria,
        nota1: this.notaPrimerTrimestre,
        nota2: this.notaSegundoTrimestre,
        nota3: this.notaTercerTrimestre,
        promedioTrimestre: this.promedioTrimestre
      };

      datosRelacionados.push(datosAlmacenados);
    };

    for (const datosSistema of notasMaterias) {
      procesarNotas(datosSistema, datosRelacionados);
    }

    for (const datosSistema of notasMateriasB) {
      procesarNotas(datosSistema, datosRelacionadosB);

      if (datosSistema.nombreMateria === "ACOMPAÑAMIENTO INTEGRAL EN EL AULA") {
        const respuestas = await forkJoin([
          this.academicoService.buscarPonderacionPorNota(this.notaPrimerTrimestre),
          this.academicoService.buscarPonderacionPorNota(this.notaSegundoTrimestre),
          this.academicoService.buscarPonderacionPorNota(this.notaTercerTrimestre)
        ]).toPromise();

        respuestas.forEach((respuesta, index) => {
          const obj = respuesta.objeto;
          if (index === 0) {
            this.descripcion1 = obj.pocoObs;
          } else if (index === 1) {
            this.descripcion2 = obj.pocoObs;
          } else if (index === 2) {
            this.descripcion3 = obj.pocoObs;
          }
        });

        const datos = {
          nombreMateria: datosSistema.nombreMateria,
          descripcion1: this.descripcion1,
          descripcion2: this.descripcion2,
          descripcion3: this.descripcion3
        };

        listaDescripcion.push(datos);
      }
    }

    await this.crearPDFElemental(element, datosRelacionados, datosRelacionadosB, listaDescripcion);

  } catch (error) {
    console.error("Ocurrió un error:", error);
  }
}

async notasMedia(notasMaterias, notasMateriasB, element) {
  try {
    const datosRelacionados = [];
    const datosRelacionadosB = [];
    const listaDescripcion = [];

    const obtenerNota = (nota) => nota ? nota : 0;

    const procesarNotas = async (datosSistema, datosRelacionados, listaDescripcion) => {
      datosSistema.notas.forEach(async (nota) => {
        switch (nota.identificador) {
          case "TRIMESTRE 1":
            this.notaPrimerTrimestre = obtenerNota(nota.nota);
            break;
          case "TRIMESTRE 2":
            this.notaSegundoTrimestre = obtenerNota(nota.nota);
            break;
          case "TRIMESTRE 3":
            this.notaTercerTrimestre = obtenerNota(nota.nota);
            break;
          case "PROMEDIO TRIMESTRE":
            this.promedioTrimestre = obtenerNota(nota.nota);
            break;
          case "SUPLETORIO":
            this.supletorio = obtenerNota(nota.nota);
            break;
          case "EVALUACION FINAL":
            this.promedioAnual = obtenerNota(nota.nota);
            break;
            case "NOTA PONDERADA":
            this.notaPonderacion = obtenerNota(nota.nota);
            break;
            case "PROMEDIO FINAL":
            this.promedioFinal = obtenerNota(nota.nota);
            break;
          default:
            break;
        }
      });

      const datosAlmacenados = {
        nombreMateria: datosSistema.nombreMateria,
        nota1: this.notaPrimerTrimestre,
        nota2: this.notaSegundoTrimestre,
        nota3: this.notaTercerTrimestre,
        promedioTrimestral: this.promedioTrimestre,
        supletorio: this.supletorio,
        promedioAnual: this.promedioAnual,
        ponderacion: this.notaPonderacion,
        promedioFinal: this.promedioFinal
      };

      datosRelacionados.push(datosAlmacenados);

      if (datosSistema.nombreMateria === "ACOMPAÑAMIENTO INTEGRAL EN EL AULA") {
        const respuestas = await forkJoin([
          this.academicoService.buscarPonderacionPorNota(this.notaPrimerTrimestre),
          this.academicoService.buscarPonderacionPorNota(this.notaSegundoTrimestre),
          this.academicoService.buscarPonderacionPorNota(this.notaTercerTrimestre)
        ]).toPromise();

        const descripciones = respuestas.map(respuesta => respuesta.objeto?respuesta.objeto.pocoObs:"");

        const datosDescripcion = {
          nombreMateria: datosSistema.nombreMateria,
          descripcion1: descripciones[0],
          descripcion2: descripciones[1],
          descripcion3: descripciones[2]
        };

        listaDescripcion.push(datosDescripcion);
      }
    };

    for (const datosSistema of notasMaterias) {
      await procesarNotas(datosSistema, datosRelacionados, listaDescripcion);
    }

    for (const datosSistema of notasMateriasB) {
      await procesarNotas(datosSistema, datosRelacionadosB, listaDescripcion);
    }

    await this.crearPDFMedia(element, datosRelacionados, datosRelacionadosB, listaDescripcion);
  } catch (error) {
    console.error("Ocurrió un error:", error);
  }
}

async notaBachillerato(notasMaterias, notasMateriasB, element) {
  try {
    const datosRelacionados = [];
    const datosRelacionadosB = [];
    const listaDescripcion = [];

    const obtenerNota = (nota) => nota ? nota : 0;

    const procesarNotas = async (datosSistema, datosRelacionados, listaDescripcion) => {
      datosSistema.notas.forEach(async (nota) => {
        switch (nota.identificador) {
          case "TRIMESTRE 1":
            this.notaPrimerTrimestre = obtenerNota(nota.nota);
            break;
          case "TRIMESTRE 2":
            this.notaSegundoTrimestre = obtenerNota(nota.nota);
            break;
          case "TRIMESTRE 3":
            this.notaTercerTrimestre = obtenerNota(nota.nota);
            break;
          case "PROMEDIO TRIMESTRE":
            this.promedioTrimestre = obtenerNota(nota.nota);
            break;
          case "SUPLETORIO":
            this.supletorio = obtenerNota(nota.nota);
            break;
          case "PROMEDIO FINAL":
            this.promedioAnual = obtenerNota(nota.nota);
            break;
            case "EVALUACION FINAL":
            this.promedioFinal = obtenerNota(nota.nota);
            break;
          default:
            break;
        }
      });

      const datosAlmacenados = {
        nombreMateria: datosSistema.nombreMateria,
        nota1: this.notaPrimerTrimestre,
        nota2: this.notaSegundoTrimestre,
        nota3: this.notaTercerTrimestre,
        promedioTrimestral: this.promedioTrimestre,
        supletorio: this.supletorio,
        promedioAnual: this.promedioAnual,
        promedioFinal: this.promedioFinal
      };

      datosRelacionados.push(datosAlmacenados);

      if (datosSistema.nombreMateria === "ACOMPAÑAMIENTO INTEGRAL EN EL AULA") {
        const respuestas = await forkJoin([
          this.academicoService.buscarPonderacionPorNota(this.notaPrimerTrimestre),
          this.academicoService.buscarPonderacionPorNota(this.notaSegundoTrimestre),
          this.academicoService.buscarPonderacionPorNota(this.notaTercerTrimestre)
        ]).toPromise();

        const descripciones = respuestas.map(respuesta => respuesta.objeto?respuesta.objeto.pocoObs:"");

        const datosDescripcion = {
          nombreMateria: datosSistema.nombreMateria,
          descripcion1: descripciones[0],
          descripcion2: descripciones[1],
          descripcion3: descripciones[2]
        };

        listaDescripcion.push(datosDescripcion);
      }
    };

    for (const datosSistema of notasMaterias) {
      await procesarNotas(datosSistema, datosRelacionados, listaDescripcion);
    }

    for (const datosSistema of notasMateriasB) {
      await procesarNotas(datosSistema, datosRelacionadosB, listaDescripcion);
    }
    await this.crearPDFBachillerato(element, datosRelacionados, datosRelacionadosB, listaDescripcion);
  } catch (error) {
    console.error("Ocurrió un error:", error);
  }
}


async crearPDFPrepa(element, notasMaterias){
  const pdfDefinition = {
    content: [
      {
        columns: [
          {
            image: await this.getBase64ImageFromURL("assets/images/ico/logoUnido.png"),
            width: 80,
            alignment: 'center',
          },
          {
            stack: [
              { text:  this.nombreInstitucion, style: 'tituloPequeno' },
              { text: "AMIE: " + this.Amie, style: 'tituloPequeno'},
              { text: "AÑO LECTIVO: "+ this.nombreAñoElectivo, style: 'tituloLargo'},
              { text: "JORNADA: "+this.nombreJornada + "        GRADO: "+this.nombreGrado+ "  PARALELO: "+this.nombreParalelo,  style: 'tituloLargo'}
            ],
            margin: [0, 20, 0, -90],
            width: '*',
          },
        ],
      },
      { text: 'Nombre Estudiante: '+element.estudiante.estNombres, style: 'subtitulo' }, // Subtítulo agregado aquí
      {
        table: {
          headerRows: 1,
          widths: [90, 285, 30, 30, 30],
          body: [
            [
              { text: 'AMBITO', rowSpan: 2, alignment: 'center'},
              { text: 'DESTREZAS', rowSpan: 2, alignment: 'center'},
              { text: 'TRIMESTRES', colSpan: 3, alignment: 'center'},
              { text: '', alignment: 'center'},
              { text: '', alignment: 'center'},
            ],
            [
              { text: '', margin: [0, 0, -80, 0]},
              { text: '', margin: [0, 0, -80, 0]},
              { text: 'I', margin: [0, 0, -80, 0]},
              { text: 'II', margin: [0, 0, -80, 0]},
              { text: 'III', margin: [0, 0, -80, 0]},
              
            ],
            ...notasMaterias.map(item => [item.nombreAmbito, item.nombreDestreza, item.nota1, item.nota2, item.nota3])
          ],
        },
        style: 'datosTabla', // Aplica el estilo a la tabla
      },
    ],
    styles: {
      titulo: {
        fontSize: 16,
        bold: true,
      },
      tituloPequeno: {
        fontSize: 10,
        bold: true,
        alignment: 'center',
        lineHeight: 1.5, // Ajusta el espaciado entre líneas
      },
      tituloLargo: {
        fontSize: 8,
        alignment: 'center',
        bold: false,
        lineHeight: 1.5, // Ajusta el espaciado entre líneas
      },
      subtitulo: {
        fontSize: 10,
        bold: true,
        alignment: 'left',
        margin: [0, 15, 0, 5], // Ajusta el margen del subtitulo
      },
      datosTabla: {
        fontSize: 8,
        alignment: 'center',
        margin: [0, 20, 0, 0],
        fillColor: '#F2F2F2',
      },
      TablaFirma: {
        margin: [50, 120, 0, 0],
        },
        firmaRector: {
          fontSize: 7,
          bold: true,
          margin: [100, 0, 0, 0] 
          }
    },
  };

  // Espacio para la firma del rector y línea
  pdfDefinition.content.push({
    text: '______________________________',
    style: 'TablaFirma',
});

pdfDefinition.content.push({
  text: 'MÁXIMA AUTORIDAD',
  style: 'firmaRector'
});

  const pdf = pdfMake.createPdf(pdfDefinition);
  pdf.open();
  
  
}

async crearPDFElemental2(element, notasMaterias, notasMateriasB, listaDescripcion) {
  const header = {
      image: await this.getBase64ImageFromURL("assets/images/ico/logoUnido.png"),
      width: 80,
      alignment: 'center',
  };

  const stack = [
      { text: this.nombreInstitucion, style: 'tituloPequeno' },
      { text: "AMIE: " + this.Amie, style: 'tituloPequeno' },
      { text: "AÑO LECTIVO: " + this.nombreAñoElectivo, style: 'tituloLargo' },
      { text: "JORNADA: " + this.nombreJornada + "        GRADO: " + this.nombreGrado + "     PARALELO:" + this.nombreParalelo, style: 'tituloLargo' }
  ];

  const pdfDefinition = {
      content: [
          {
              columns: [
                  header,
                  {
                      stack: stack,
                      margin: [0, 20, 0, -90],
                      width: '*',
                  },
              ],
          },
          { text: 'Nombre Estudiante: ' + element.estudiante.estNombres, style: 'subtitulo' },
          {
              table: {
                  headerRows: 1,
                  widths: [150, 50, 50, 50, 50, 50, 50],
                  body: [
                      [
                          { text: 'ASIGNATURA', rowSpan: 2, alignment: 'center' },
                          { text: 'TRIMESTRES', colSpan: 3, alignment: 'center' },
                          { text: '' },
                          { text: '' },
                          { text: 'PROMEDIO TRIMESTRE', rowSpan: 2, alignment: 'center' },
                          { text: 'EVALUACION FINAL', rowSpan: 2, alignment: 'center' },
                          { text: 'NOTA FINAL', rowSpan: 2, alignment: 'center' }
                      ],
                      [
                          { text: '', margin: [0, 0, -80, 0] },
                          { text: 'I', margin: [0, 0, -80, 0]},
                          { text: 'II', margin: [0, 0, -80, 0] },
                          { text: 'III', margin: [0, 0, -80, 0]},
                          { text: '', margin: [0, 0, -80, 0] },
                          { text: '', margin: [0, 0, -80, 0] },
                          { text: '', margin: [0, 0, -80, 0] }
                      ],
                      ...notasMaterias.map(item => [item.nombreMateria, item.nota1, item.nota2, item.nota3, item.promedioTrimestre, item.evaluacionFinal, item.promedioFinal]),
                      [
                          { text: "PROMEDIO ANUAL", colSpan: 6, alignment: 'center' },
                          {},
                          {},
                          {},
                          {},
                          {},
                          { text: this.promedioAnualTotal }
                      ],
                  ],
              },
              style: 'datosTabla',
          },
      ],
      styles: {
          titulo: {
              fontSize: 16,
              bold: true,
          },
          tituloPequeno: {
              fontSize: 10,
              bold: true,
              alignment: 'center',
              lineHeight: 1.5,
          },
          tituloLargo: {
              fontSize: 8,
              alignment: 'center',
              bold: false,
              lineHeight: 1.5,
          },
          subtitulo: {
              fontSize: 10,
              bold: true,
              alignment: 'left',
              margin: [0, 15, 0, 5],
          },
          datosTabla: {
              fontSize: 8,
              alignment: 'center',
              margin: [0, 20, 0, 0],
              fillColor: '#F2F2F2',
          },
          segundaTabla: {
              fontSize: 8,
              alignment: 'center',
              margin: [0, 5, 0, 5],
              fillColor: '#F2F2F2',
          },
          TablaFirma: {
            margin: [50, 120, 0, 0],
            },
            firmaRector: {
              fontSize: 7,
              bold: true,
              margin: [100, 0, 0, 0] 
              }
      },
  };

  if (notasMateriasB.length > 0) {
      pdfDefinition.content.push({
          table: {
              headerRows: 1,
              widths: [150, 50, 50, 50, 50, 50, 50],
              body: [
                  ...notasMateriasB.map(item => [item.nombreMateria, item.nota1, item.nota2, item.nota3, item.promedioTrimestre, item.evaluacionFinal, item.promedioFinal])
              ],
          },
          style: 'segundaTabla',
      });
  }

  if (listaDescripcion.length > 0) {
      pdfDefinition.content.push({
          table: {
              headerRows: 1,
              widths: [150, 60, 60, 60],
              body: [
                  ...listaDescripcion.map(item => ['EVALUACIÓN COMPORTAMENTAL', item.descripcion1, item.descripcion2, item.descripcion3])
              ],
          },
          style: 'segundaTabla',
      });
  }

   // Espacio para la firma del rector y línea
   pdfDefinition.content.push({
    text: '______________________________',
    style: 'TablaFirma',
});

pdfDefinition.content.push({
  text: 'MÁXIMA AUTORIDAD',
  style: 'firmaRector'
});


  const pdf = pdfMake.createPdf(pdfDefinition);
  pdf.open();
}

async crearPDFElemental(element, notasMaterias, notasMateriasB, listaDescripcion) {
  const header = {
      image: await this.getBase64ImageFromURL("assets/images/ico/logoUnido.png"),
      width: 80,
      alignment: 'center',
  };

  const stack = [
      { text: this.nombreInstitucion, style: 'tituloPequeno' },
      { text: "AMIE: " + this.Amie, style: 'tituloPequeno' },
      { text: "AÑO LECTIVO: " + this.nombreAñoElectivo, style: 'tituloLargo' },
      { text: "JORNADA: " + this.nombreJornada + "        GRADO: " + this.nombreGrado + "    PARALELO: " + this.nombreParalelo, style: 'tituloLargo' }
  ];

  const pdfDefinition = {
      content: [
          {
              columns: [
                  header,
                  {
                      stack: stack,
                      margin: [0, 20, 0, -90],
                      width: '*',
                  },
              ],
          },
          { text: 'Nombre Estudiante: ' + element.estudiante.estNombres, style: 'subtitulo' },
          {
              table: {
                  headerRows: 1,
                  widths: [150, 80, 80, 80, 80],
                  body: [
                      [
                          { text: 'ASIGNATURA', rowSpan: 2, alignment: 'center' },
                          { text: 'TRIMESTRES', colSpan: 3, alignment: 'center' },
                          { text: '' },
                          { text: '' },
                          { text: 'PROMEDIO TRIMESTRE', rowSpan: 2, alignment: 'center' }
                      ],
                      [
                          { text: '', margin: [0, 0, -80, 0]},
                          { text: 'I', margin: [0, 0, -80, 0]},
                          { text: 'II', margin: [0, 0, -80, 0] },
                          { text: 'III', margin: [0, 0, -80, 0] },
                          { text: '', margin: [0, 0, -80, 0]}
                      ],
                      ...notasMaterias.map(item => [item.nombreMateria, item.nota1, item.nota2, item.nota3, item.promedioTrimestre]),
                      [
                          { text: "PROMEDIO ANUAL", colSpan: 4, alignment: 'center' },
                          {},
                          {},
                          {},
                          { text: this.promedioAnualTotal }
                      ],
                  ],
              },
              style: 'datosTabla',
          },
      ],
      styles: {
          titulo: {
              fontSize: 16,
              bold: true,
          },
          tituloPequeno: {
              fontSize: 10,
              bold: true,
              alignment: 'center',
              lineHeight: 1.5,
          },
          centeredCell: {
            alignment: 'center',
          }
          ,
          tituloLargo: {
              fontSize: 8,
              alignment: 'center',
              bold: false,
              lineHeight: 1.5,
          },
          subtitulo: {
              fontSize: 10,
              bold: true,
              alignment: 'left',
              margin: [0, 15, 0, 5],
          },
          datosTabla: {
              fontSize: 8,
              alignment: 'center',
              margin: [0, 20, 0, 0],
              fillColor: '#F2F2F2',
          },
          segundaTabla: {
              fontSize: 8,
              alignment: 'center',
              margin: [0, 5, 0, 5],
              fillColor: '#F2F2F2',
          },
          TablaFirma: {
          margin: [50, 120, 0, 0],
          },
          firmaRector: {
            fontSize: 7,
            bold: true,
            margin: [100, 0, 0, 0] 
            }
      },
  };

  if (listaDescripcion.length > 0) {
      pdfDefinition.content.push({
          table: {
              headerRows: 1,
              widths: [150, 80, 80, 80, 80],
              body: [
                  ...notasMateriasB.map(item => [item.nombreMateria, item.nota1, item.nota2, item.nota3, item.promedioTrimestre])
              ],
          },
          style: 'segundaTabla',
      });

      pdfDefinition.content.push({
        table: {
            headerRows: 1,
            widths: [150, 80, 80, 80],
            body: [
                ...listaDescripcion.map(item => ['EVALUACIÓN COMPORTAMENTAL', item.descripcion1, item.descripcion2, item.descripcion3])
            ],
        },
        style: 'segundaTabla',
    });


    // Espacio para la firma del rector y línea
  pdfDefinition.content.push({
    text: '______________________________',
    style: 'TablaFirma',
});

pdfDefinition.content.push({
  text: 'MÁXIMA AUTORIDAD',
  style: 'firmaRector'
});


  }

  const pdf = pdfMake.createPdf(pdfDefinition);
  pdf.open();
}

async crearPDFMedia(element, notasMaterias, notasMateriasB, listaDescripcion) {

  const header = {
    image: await this.getBase64ImageFromURL("assets/images/ico/logoUnido.png"),
    width: 80,
    alignment: 'center',
};

const stack = [
    { text: this.nombreInstitucion, style: 'tituloPequeno' },
    { text: "AMIE: " + this.Amie, style: 'tituloPequeno' },
    { text: "AÑO LECTIVO: " + this.nombreAñoElectivo, style: 'tituloLargo' },
    { text: "JORNADA: " + this.nombreJornada + "        GRADO: " + this.nombreGrado + "    PARALELO: " + this.nombreParalelo, style: 'tituloLargo' }
];

  const pdfDefinition = {
      content: [
          {
              columns: [
                  header,
                  {
                    stack: stack,
                    margin: [0, 20, 0, -90],
                    width: '*',
                  },
              ],
          },
          { text: 'Nombre Estudiante: ' + element.estudiante.estNombres, style: 'subtitulo' },
          {
              table: {
                  headerRows: 1,
                  widths: [150, 37, 37, 37, 37, 37, 37, 37, 37],
                  body: [
                      [
                          { text: 'ASIGNATURA', rowSpan: 2, alignment: 'center' },
                          { text: 'TRIMESTRES', colSpan: 3, alignment: 'center' },
                          { text: '' },
                          { text: '' },
                          { text: 'PROMEDIO TRIMESTRE', rowSpan: 2, alignment: 'center' },
                          { text: 'EVALUACIÓN FINAL', rowSpan: 2, alignment: 'center' },
                          { text: 'NOTA FINAL', rowSpan: 2, alignment: 'center' },
                          { text: 'SUPLETORIO', rowSpan: 2, alignment: 'center' },
                          { text: 'NOTA PONDERADA', rowSpan: 2, alignment: 'center' }
                      ],
                      [
                          { text: '', margin: [0, 0, -80, 0]},
                          { text: 'I', margin: [0, 0, -80, 0]},
                          { text: 'II', margin: [0, 0, -80, 0]},
                          { text: 'III', margin: [0, 0, -80, 0]},
                          { text: '', margin: [0, 0, -80, 0]},
                          { text: '', margin: [0, 0, -80, 0]},
                          { text: '', margin: [0, 0, -80, 0]},
                          { text: '', margin: [0, 0, -80, 0]},
                          { text: '', margin: [0, 0, -80, 0]}
                      ],
                      ...notasMaterias.map(item => [item.nombreMateria, item.nota1, item.nota2, item.nota3, item.promedioTrimestral, item.promedioAnual, item.promedioFinal, item.supletorio ,item.ponderacion]),
                      [
                          { text: "PROMEDIO ANUAL", colSpan: 8, alignment: 'center' },
                          {},
                          {},
                          {},
                          {},
                          {},
                          {},
                          {},
                          { text: this.promedioAnualTotal }
                      ],
                  ],
              },
              style: 'datosTabla',
          },
      ],
      styles: {
          titulo: {
              fontSize: 16,
              bold: true,
          },
          tituloPequeno: {
              fontSize: 10,
              bold: true,
              alignment: 'center',
              lineHeight: 1.5,
          },
          tituloLargo: {
              fontSize: 8,
              alignment: 'center',
              bold: false,
              lineHeight: 1.5,
          },
          subtitulo: {
              fontSize: 10,
              bold: true,
              alignment: 'left',
              margin: [0, 15, 0, 5],
          },
          datosTabla: {
              fontSize: 6,
              alignment: 'center',
              margin: [0, 5, 0, 5],
              fillColor: '#F2F2F2',
          },
          segundaTabla: {
              fontSize: 6,
              alignment: 'center',
              margin: [0, 5, 0, 5],
              fillColor: '#F2F2F2',
          },
          TablaFirma: {
            margin: [50, 120, 0, 0],
            },
            firmaRector: {
              fontSize: 7,
              bold: true,
              margin: [100, 0, 0, 0] 
              }
      },
  };

  if (notasMateriasB.length > 0) {
      pdfDefinition.content.push({
          table: {
              headerRows: 1,
              widths: [150, 37, 37, 37, 37, 37, 37, 37, 37],
              body: [
                  ...notasMateriasB.map(item => [item.nombreMateria, item.nota1, item.nota2, item.nota3, item.promedioTrimestral, item.promedioAnual, item.promedioFinal, item.supletorio ,item.ponderacion])
              ],
          },
          style: 'segundaTabla',
      });
  }

  if (listaDescripcion.length > 0) {
      pdfDefinition.content.push({
          table: {
              headerRows: 1,
              widths: [200, 80, 80, 80],
              body: [
                  ...listaDescripcion.map(item => ['EVALUACIÓN COMPORTAMENTAL', item.descripcion1, item.descripcion2, item.descripcion3])
              ],
          },
          style: 'segundaTabla',
      });
  }

  // Espacio para la firma del rector y línea
  pdfDefinition.content.push({
    text: '______________________________',
    style: 'TablaFirma',
});

pdfDefinition.content.push({
  text: 'MÁXIMA AUTORIDAD',
  style: 'firmaRector'
});

  const pdf = pdfMake.createPdf(pdfDefinition);
  pdf.open();
}

async crearPDFBachillerato(element, notasMaterias, notasMateriasB, listaDescripcion) {

  const header = {
      image: await this.getBase64ImageFromURL("assets/images/ico/logoUnido.png"),
      width: 80,
      alignment: 'center',
  };

  const especialidadText = this.nombreEspecialidad !== undefined ? " ESPECIALIDAD: " + this.nombreEspecialidad : "";

  const stack = [
      { text:  this.nombreInstitucion, style: 'tituloPequeno' },
      { text: "AMIE: " + this.Amie, style: 'tituloPequeno' },
      { text: "AÑO LECTIVO: " + this.nombreAñoElectivo, style: 'tituloLargo' },
      { text: "JORNADA: " + this.nombreJornada + "     GRADO: " + this.nombreGrado + "  PARALELO: " + this.nombreParalelo + " "+ especialidadText, style: 'tituloLargo' }
  ];

  const pdfDefinition = {
      content: [
          {
              columns: [
                  header,
                  {
                      stack: stack,
                      margin: [0, 20, 0, -90],
                      width: '*',
                  },
              ],
          },
          { text: 'Nombre Estudiante: ' + element.estudiante.estNombres, style: 'subtitulo' },
          {
              table: {
                  headerRows: 1,
                  widths: [100, 45, 45, 45, 45, 45, 45, 45],
                  body: [
                      [
                          { text: 'ASIGNATURA', rowSpan: 2, alignment: 'center' },
                          { text: 'TRIMESTRES', colSpan: 3, alignment: 'center' },
                          { text: '' },
                          { text: '' },
                          { text: 'PROMEDIO TRIMESTRE', rowSpan: 2, alignment: 'center' },
                          { text: 'EVALUACIÓN FINAL', rowSpan: 2, alignment: 'center' },
                          { text: 'SUPLETORIO', rowSpan: 2, alignment: 'center' },
                          { text: 'PROMEDIO ANUAL', rowSpan: 2, alignment: 'center' }
                      ],
                      [
                        { text: '', margin: [0, 0, -80, 0] },
                        { text: 'I', margin: [0, 0, -80, 0] },
                        { text: 'II', margin: [0, 0, -80, 0] },
                        { text: 'III', margin: [0, 0, -80, 0] },
                        { text: '', margin: [0, 0, -80, 0] },
                        { text: '', margin: [0, 0, -80, 0] },
                        { text: '', margin: [0, 0, -80, 0] },
                        { text: '', margin: [0, 0, -80, 0] }
                      ],
                      ...notasMaterias.map(item => [item.nombreMateria, item.nota1, item.nota2, item.nota3, item.promedioTrimestral, item.promedioFinal ,item.supletorio, item.promedioAnual]),
                      [
                          { text: "PROMEDIO ANUAL", colSpan: 7, alignment: 'center' },
                          {},
                          {},
                          {},
                          {},
                          {},
                          {},
                          { text: this.promedioAnualTotal }
                      ],
                  ],
              },
              style: 'datosTabla',
          },
      ],
      styles: {
          titulo: {
              fontSize: 16,
              bold: true,
          },
          tituloPequeno: {
              fontSize: 10,
              bold: true,
              alignment: 'center',
              lineHeight: 1.5,
          },
          tituloLargo: {
              fontSize: 8,
              alignment: 'center',
              bold: false,
              lineHeight: 1.5,
          },
          subtitulo: {
              fontSize: 10,
              bold: true,
              alignment: 'left',
              margin: [0, 15, 0, 5],
          },
          datosTabla: {
              fontSize: 6,
              alignment: 'center',
              margin: [0, 20, 0, 0],
              fillColor: '#F2F2F2',
          },
          segundaTabla: {
              fontSize: 7,
              alignment: 'center',
              margin: [0, 5, 0, 5],
              fillColor: '#F2F2F2',
          },
          TablaFirma: {
            margin: [50, 120, 0, 0],
            },
            firmaRector: {
              fontSize: 7,
              bold: true,
              margin: [100, 0, 0, 0] 
              }
      },
  };

  if (notasMateriasB.length > 0) {
      pdfDefinition.content.push({
          table: {
              headerRows: 1,
              widths: [100, 45, 45, 45, 45, 45, 45, 45],
              body: [
                  ...notasMateriasB.map(item => [item.nombreMateria, item.nota1, item.nota2, item.nota3, item.promedioTrimestral, item.promedioFinal ,item.supletorio, item.promedioAnual])
              ],
          },
          style: 'segundaTabla',
      });
  }

  if (listaDescripcion.length > 0) {
      pdfDefinition.content.push({
          table: {
              headerRows: 1,
              widths: [100, 100, 100, 100],
              body: [
                  ...listaDescripcion.map(item => ['EVALUACIÓN COMPORTAMENTAL', item.descripcion1, item.descripcion2, item.descripcion3])
              ],
          },
          style: 'segundaTabla',
      });
  }

  // Espacio para la firma del rector y línea
  pdfDefinition.content.push({
    text: '______________________________',
    style: 'TablaFirma',
});

pdfDefinition.content.push({
  text: 'MÁXIMA AUTORIDAD',
  style: 'firmaRector'
});

  const pdf = pdfMake.createPdf(pdfDefinition);
  pdf.open();
}


getBase64ImageFromURL(url) {
  return new Promise((resolve, reject) => {
    var img = new Image();
    img.setAttribute("crossOrigin", "anonymous");

    img.onload = () => {
      var canvas = document.createElement("canvas");
      canvas.width = img.width;
      canvas.height = img.height;

      var ctx = canvas.getContext("2d");
      ctx.drawImage(img, 0, 0);

      var dataURL = canvas.toDataURL("image/png");

      resolve(dataURL);
    };

    img.onerror = error => {
      reject(error);
    };

    img.src = url;
  });
}

  async generarBoletaCalificacion(element:any){
    if(this.frmCertificado.get('codigoGrado').value >= 1 && this.frmCertificado.get('codigoGrado').value  <= 3){
      try{
        const respuesta: ResponseGenerico<notaDestrezaInterface> = await this.academicoService.buscarNotaDestrezaPorMatricula(element.matCodigo).toPromise();
      this.listaMateriasPrepa = respuesta.listado
      }catch (error) {
        console.error("Error al buscar notas de asignatura:", error);
      }

      if(this.listaMateriasPrepa.length === 0){
        Swal.fire({
          icon: "error",
          title: "Atención",
          text: "El estudiante no tiene materias calificadas"
        });
      }else{
        await this.sacarDataPrepa(element, this.listaMateriasPrepa);
      }
      
    }else{
      try {
        await this.sacarPromedioAnual(element.matCodigo)
        const respuesta: ResponseGenerico<notaAsignaturaInterface> = await this.academicoService.buscarNotaAsignaturaPorMatricula(element.matCodigo).toPromise();
        this.listaMaterias = respuesta.listado;
      } catch (error) {
        console.error("Error al buscar notas de asignatura:", error);
      }

      if(this.listaMaterias.length === 0){
        Swal.fire({
          icon: "error",
          title: "Atención",
          text: "El estudiante no tiene materias calificadas"
        });
      }else{
        await this.sacarData(element, this.listaMaterias);
      }
      

    }

  }

  async sacarPromedioAnual(codigo){
    this.academicoService.buscarPromedioPorCodigoMatricula(codigo).subscribe(
      (respuesta: ResponseGenerico<PromedioAnualInterface>) => {
        let lista = respuesta.listado;

        if(lista.length>0){
          lista.forEach(element => {
            this.promedioAnualTotal = element.rproPromedio;
          });
        }else{
          this.promedioAnualTotal = 0
       }
      })
  }

  generarCertificadoPromocion(matricula:any){
    this.academicoService.buscarPromedioPorCodigoMatricula(matricula.matCodigo).subscribe(
      (respuesta: ResponseGenerico<PromedioAnualInterface>) => {
        respuesta.listado;
    let regimenAnioLectivo=this.listaRegimenesActual.find(item => item.reanleCodigo == this.frmCertificado.get('codigoRegimen').value);
    let ultimoEspacioIndex = regimenAnioLectivo.descripcionAnioLectivo.lastIndexOf(" "); 
    let regimen = regimenAnioLectivo.descripcionAnioLectivo.substring(0, ultimoEspacioIndex);
    let anioLectivo = regimenAnioLectivo.descripcionAnioLectivo.substring(ultimoEspacioIndex + 1); 
    let paralelo=this.listaParalelos.find(item => item.curparCodigo == this.frmCertificado.get('codigoParalelo').value)
    let certificado={
      codigoAmie:this.institucion.insAmie,
      institucion:this.institucion.insDescripcion,
      anioLectivo:anioLectivo,
      regimen:regimen,
      modalidad:this.listaModalidades.find(item => item.modCodigo == paralelo.codModalidad)?.modNombre?.toString() || '',
      jornada:this.listaJornadas.find(item => item.jorCodigo == this.frmCertificado.get('codigoJornada').value)?.jorNombre?.toString() || '',
      matricula:matricula,
      gradoActual:this.listaGrados.find(item => item.graCodigo == this.frmCertificado.get('codigoGrado').value)?.graDescripcion?.toString() || '',
      gradoSiguiente:this.listaTodosGrados.find(item => item.graCodigo == (parseInt(this.frmCertificado.get('codigoGrado').value,10))+1)?.graDescripcion?.toString() || '',
      promedio:respuesta.listado.length>0?respuesta.listado[0].rproPromedio:'',
      fecha:this.fechaActualDias(),
      promedioLetras:'',
      provincia:this.distritoParroquia.nombreProvincia
    }
    this.pdfCertificadoService.generatePdf(certificado);
  });
  }

  fechaActualDias(){
    const meses = [
      "enero", "febrero", "marzo", "abril", "mayo", "junio",
      "julio", "agosto", "septiembre", "octubre", "noviembre", "diciembre"
    ];
    const dias = [
      "domingo", "lunes", "martes", "miércoles", "jueves", "viernes", "sábado"
    ];
    const fechaActual = new Date();
    const diaSemana = dias[fechaActual.getDay()];
    const diaMes = fechaActual.getDate();
    const mes = meses[fechaActual.getMonth()];
    const año = fechaActual.getFullYear();
    let fechaEnLetras = ` ${diaMes} de ${mes} de ${año}`;
    return fechaEnLetras;
  }

  buscarPromedio(matricula:number){
    this.academicoService.buscarPromedioPorCodigoMatricula(matricula).subscribe(
      (respuesta: ResponseGenerico<PromedioAnualInterface>) => {
        this.promedio = respuesta.objeto;
      })
  }

  convertirDecimalALetras(numero: string): string {
    const numerosALetras: Record<number, string> = {
        0: 'cero', 1: 'uno', 2: 'dos', 3: 'tres',
        4: 'cuatro', 5: 'cinco', 6: 'seis', 7: 'siete',
        8: 'ocho', 9: 'nueve', 10: 'diez', 11: 'once',
        12: 'doce', 13: 'trece', 14: 'catorce', 15: 'quince',
        20: 'veinte', 30: 'treinta', 40: 'cuarenta', 50: 'cincuenta',
        60: 'sesenta', 70: 'setenta', 80: 'ochenta', 90: 'noventa',
    };

    const unidades = ['', 'un', 'dos', 'tres', 'cuatro', 'cinco', 'seis', 'siete', 'ocho', 'nueve'];
    const decenas = ['diez', 'veinti', 'treinta', 'cuarenta', 'cincuenta', 'sesenta', 'setenta', 'ochenta', 'noventa'];

    const partes = numero.split('.');
    const entero = parseInt(partes[0]);
    const decimal = parseInt(partes[1] || '0');

    let enteroEnLetras = '';
    let decimalEnLetras = '';

    if (entero === 0) {
        enteroEnLetras = 'cero';
    } else {
        if (entero >= 1000 && entero < 2000) {
            enteroEnLetras += 'mil ';
            enteroEnLetras += convertirCientos(entero % 1000);
        } else if (entero >= 2000) {
            enteroEnLetras += convertirMillones(Math.floor(entero / 1000000)) + ' millones ';
            enteroEnLetras += convertirMiles(entero % 1000000);
        } else {
            enteroEnLetras += convertirMiles(entero);
        }
    }

    if (decimal > 0) {
        decimalEnLetras = ' punto ';
       /* if (decimal < 10) {
            decimalEnLetras += 'cero ';
        }*/
        decimalEnLetras += convertirCientos(decimal);
    }

    return enteroEnLetras + decimalEnLetras;

    function convertirCientos(num: number): string {
        let letras = '';
        const centenas = Math.floor(num / 100);
        const decenas = Math.floor((num % 100) / 10);
        const unidades = num % 10;

        if (centenas > 0) {
            if (centenas === 1 && decenas === 0 && unidades === 0) {
                letras += 'cien';
            } else {
                letras += numerosALetras[centenas] + 'cientos';
            }
        }

        if (decenas > 0 || unidades > 0) {
            if (centenas > 0) {
                letras += ' ';
            }
            if (decenas === 1) {
                letras += numerosALetras[10 + unidades];
            } else if (decenas > 1) {
                letras += decenas === 2 ? 'veinti' : numerosALetras[decenas * 10];
                if (unidades > 0) {
                    letras += ' y ';
                }
            }
            if (unidades > 0 && decenas !== 1) {
                letras += unidades === 1 && decenas !== 2 ? 'un' : numerosALetras[unidades];
            }
        }

        return letras;
    }

    function convertirMiles(num: number): string {
        let letras = '';
        const miles = Math.floor(num / 1000);
        const resto = num % 1000;

        if (miles > 0) {
            if (miles === 1 && resto === 0) {
                letras += 'mil';
            } else {
                letras += convertirCientos(miles) + ' mil';
            }
            if (resto > 0) {
                letras += ' ';
            }
        }

        letras += convertirCientos(resto);
        return letras;
    }

    function convertirMillones(num: number): string {
        let letras = '';
        const millones = Math.floor(num / 1000000);
        const resto = num % 1000000;

        if (millones > 0) {
            if (millones === 1 && resto === 0) {
                letras += 'un millón';
            } else {
                letras += convertirCientos(millones) + ' millones';
            }
            if (resto > 0) {
                letras += ' ';
            }
        }

        return letras;
    }
}

}