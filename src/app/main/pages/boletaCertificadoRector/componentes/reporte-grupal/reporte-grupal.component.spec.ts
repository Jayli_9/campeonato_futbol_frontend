import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ReporteGrupalComponent } from './reporte-grupal.component';

describe('ReporteGrupalComponent', () => {
  let component: ReporteGrupalComponent;
  let fixture: ComponentFixture<ReporteGrupalComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ReporteGrupalComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ReporteGrupalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
