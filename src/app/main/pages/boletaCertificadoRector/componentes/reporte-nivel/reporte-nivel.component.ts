import { Component, OnInit, ViewChild } from '@angular/core';
import { UntypedFormBuilder, UntypedFormGroup, Validators } from '@angular/forms';
import { MatDialog } from '@angular/material/dialog';
import { MatPaginator } from '@angular/material/paginator';
import { MatTableDataSource } from '@angular/material/table';
import { User } from 'app/auth/models/user';
import { CatEstablecimiento } from 'app/main/pages/asignacionDocentes/interface/cat_establecimiento';
import { CatRegimenAnioLectivo } from 'app/main/shared/interfaces/cat-regimen-anio-lectivo';
import { ResponseGenerico } from '../../interfaces/response-generico';
import { GieeService } from '../../services/giee.service';
import { CatalogoService } from '../../services/catalogo.service';
import { CatNivel } from '../../interfaces/cat-nivel';
import { regimenInterface } from "../../interfaces/regimen";
import { CatJoranada } from 'app/main/pages/asignacionDocentes/interface/cat_jornada';
import { CatGradosCursos } from 'app/main/pages/asignacionDocentes/interface/cat_gradosCursos';
import { Institucion } from '../../interfaces/institucion';
import { cursoInterface } from "../../interfaces/curso";
import { gradoInterface } from "../../interfaces/grado";
import { NivelPermisoIntitucionInterface } from 'app/main/pages/asignacionDocentes/interface/nivel-Institucion';
import { OfertaService } from '../../services/oferta.service';
import { CursoParalelo } from 'app/main/pages/asistencia-estudiantes/interfaces/curso-paralelo';
import { DocenteService } from "../../services/docente.service";
import { AcademicoService } from "../../services/academico.service";
import Swal from 'sweetalert2/dist/sweetalert2.js';
import { orderBy } from 'lodash-es';
import { curParaleloInterface } from '../../interfaces/paraleloCurso';
import { paraleloInterface } from '../../interfaces/paralelo';
import { NgxSpinnerService } from 'ngx-spinner';
import { MatriculaService } from '../../services/matricula.service';
import { MatriculaOrdinaria } from '../../interfaces/matricula-ordinaria';
import { notaAsignaturaInterface } from "../../interfaces/nota-Asignatura";
import pdfMake from 'pdfmake/build/pdfmake';
import pdfFonts from 'pdfmake/build/vfs_fonts';
import { calificacionMogenInterface } from '../../interfaces/calificacionMogen';
import { PromedioAnualInterface } from '../../interfaces/promedio-anual';
import { notaDestrezaInterface } from "../../interfaces/nota-Destreza";
import { mallaPrepaInterface } from "../../interfaces/mallaPrepa";
import { asignaturaCompuestaInterface } from "../../interfaces/asignaturaCompuesta";
import { ambitoInterface } from "../../interfaces/ambito";
import { destrezaInterface } from '../../interfaces/destreza';
import { forkJoin } from 'rxjs';
pdfMake.vfs = pdfFonts.pdfMake.vfs;

@Component({
  selector: 'app-reporte-nivel',
  templateUrl: './reporte-nivel.component.html',
  styleUrls: ['./reporte-nivel.component.scss']
})
export class ReporteNivelComponent implements OnInit {
  columns: string[] = ['nombre','boleta'];
  currentUser: User;
  dataSource = new MatTableDataSource<Object>();
  @ViewChild(MatPaginator) paginator: MatPaginator;

  //datos de institucion
  nombreInstitucion: string;
  grado;
  paralelo;
  nombreDocente: string;

  //datos para tutores
  codigoTutor: number;
  codigosRealm = [];
  arrayComparativo = [];
  listaGradosTutor: gradoInterface[]=[];
  listaGradosTutorFiltro: gradoInterface[]=[];
  listaGradosData: gradoInterface[]=[];
  codigoNivel:number;
  listaNiveles: CatNivel[]=[];
  listaNivelesFiltro:  CatNivel[]=[];
  listaJornadas: CatJoranada[]=[];
  listaJornadaFiltro: CatJoranada[]=[];
  listaCodigoParalelos =[];
  listaParaleFiltro=[];
  codigoGrado;


  public frmCertificado: UntypedFormGroup;
  contentHeader: object;
  institucion:Institucion;
  listaEstablecimiento:CatEstablecimiento[]=[];
  listaRegimenesActual:CatRegimenAnioLectivo[]=[];
  
  listaGrados: CatGradosCursos[]=[];
  listaPermisosNivel: NivelPermisoIntitucionInterface[]=[];
  listaParalelos: CursoParalelo[]=[];
  listaMatriculas: MatriculaOrdinaria[]=[];
  listaMaterias: notaAsignaturaInterface[]=[];
  listaMateriasPrepa: notaDestrezaInterface[]=[];
  cargando: boolean = false;
  codigoJornada;
  codigoParalelo;
  codigoRegimen;
  Amie;

  //PDF
  nombreAñoElectivo;
  nombreGrado;
  nombreJornada;
  nombreParalelo;


  //filtros para prepa
  acumuladorAmbitoDestreza = [];

  //datos para pdf
  nombreDestreza;
  objetoDatosRecolectados =[];
  objetoDatosRecolectadosN =[];
  datosRelacionados = [];
  datosRelacionadosB = [];
  notaPrimerTrimestre;
  notaSegundoTrimestre;
  notaTercerTrimestre;
  promedioTrimestre;
  supletorio;
  promedioAnual;
  promedioAnualTotal;
  notaPonderacion;


  descripcion1;
  descripcion2;
  descripcion3;
  listaDescripcion = [];



  constructor(private readonly fb: UntypedFormBuilder,
    private dialog: MatDialog,
    private readonly gieeService: GieeService,
            readonly catalogoService: CatalogoService,
            readonly ofertaService: OfertaService,
            readonly matriculaService: MatriculaService,
            readonly docenteService: DocenteService,
            public spinner: NgxSpinnerService,
            readonly academicoService: AcademicoService) {
              this.currentUser = JSON.parse(localStorage.getItem('currentUser'));
            }
            
            async buscarPorDocenteIdentificador(cod){
              this.docenteService.buscarDocentePorIdentificacion(cod).then(data =>{
                let lista = data.objeto;

                if (lista === null || typeof lista === 'undefined') {
                  Swal.fire({
                    icon: "error",
                    title: "Atención",
                    text: "Cedula de docente no valida"
                  });
                 }else{
                  this.codigoTutor = lista.codPersona;
                  this.validarTutor(this.codigoTutor);
                 }

               
              })
            }

            async validarTutor(cod){
              this.academicoService.buscarTutorPorCodigoDocente(cod).then(data => {
                let validacionTutor = data.listado;
                if (!validacionTutor || validacionTutor.length === 0) {
                  Swal.fire({
                    icon: "error",
                    title: "Atención",
                    text: "Este docente no es tutor"
                  });
                  } else {
                    validacionTutor.forEach(element => {
                      if(element.tuorEstado === 1){
                        let datos = {
                          codigoGrados: element.curCodigo,
                          codigoParalelo: element.curparCodigo
                        }
                        this.listaCodigoParalelos.push(datos);
                        this.codigosRealm.push(element.reanleCodigo);
                        this.sacarCursoTutor(element.curCodigo);
                      }
                    });
                  // La lista contiene elementos
                  this.buscarInstitucionPorAmie();
                 }
              })
            }

            async sacarCursoTutor(element){
              this.ofertaService.buscarCursoPorCodigo(element).subscribe(
                (respuesta: ResponseGenerico<cursoInterface>) => {
                  let listado = respuesta.objeto;
                  this.sacarGradoTutor(listado.graCodigo, listado.curCodigo);
                  this.sacarJornadas(listado.jorCodigo);
                })
            }

            async sacarGradoTutor(element, obj){
              this.catalogoService.buscarGradoPorCodigo(element).subscribe(
                (respuesta: ResponseGenerico<gradoInterface>) => {
                  let listado = respuesta.objeto;
                  this.listaGradosTutor.push(listado);
                  setTimeout(() => {
                    this.SacarNiveles(listado.nivCodigo);
                  }, 600);

                  this.listaGradosTutorFiltro = this.listaGradosTutor.filter((item, index, self) => {
                    const foundIndex = self.findIndex((el) => (
                      el.graDescripcion === item.graDescripcion
                    ));
                    return index === foundIndex;
                  });

                  setTimeout(() => {
                    this.listaCodigoParalelos.forEach(par => {
                      if(par.codigoGrados === obj){
                        par.gradoSeleccionado = listado.graCodigo;
                      }
                    });
                  }, 2000);
                })
            }


            async SacarNiveles(cod){
              this.catalogoService.buscarNivelesPorCodigo(cod).subscribe(
                (respuesta: ResponseGenerico<CatNivel>) => {
                  let listado = respuesta.objeto;
                  this.listaNiveles.push(listado);
                  this.listaNivelesFiltro = this.listaNiveles.filter((item, index, self) => {
                    const foundIndex = self.findIndex((el) => (
                      el.nivDescripcion === item.nivDescripcion
                    ));
                    return index === foundIndex;
                  });
                })
            }

            async sacarJornadas(cod){
              this.catalogoService.buscarJornadasPorCodigo(cod).subscribe(
                (respuesta: ResponseGenerico<CatJoranada>) => {
                  let listado = respuesta.objeto;
                  this.listaJornadas.push(listado);
                  this.listaJornadaFiltro = this.listaJornadas.filter((item, index, self) => {
                    const foundIndex = self.findIndex((el) => (
                      el.jorNombre === item.jorNombre
                    ));
                    return index === foundIndex;
                  });
                })
            }



  ngOnInit(): void{

    let dato =this.currentUser.identificacion;
    this.buscarPorDocenteIdentificador(dato);

    this.nombreDocente = this.currentUser.nombre

    this.initForm();
    

    this.contentHeader = {
      headerTitle: 'Reporte Boleta Tutores',
      actionButton: false,
      breadcrumb: {
          type: '',
          links: [
              {
                name: 'Inicio',
                isLink: true,
                link: '/'
              },
              {
                name: 'reporteBoletasTutor',
                isLink: false
              }
          ]
      }
    };
    
  }

  initForm() {
    this.frmCertificado = this.fb.group({
      codigoEstablecimiento: [0, [Validators.required]],
      codigoRegimen: [0, [Validators.required]],
      codigoNivel: [0, [Validators.required]],
      codigoJornada: [0, [Validators.required]],
      codigoGrado: [0, [Validators.required]],
      codigoParalelo: [0, [Validators.required]]
    });
  }

  get registerFormControl() {
    return this.frmCertificado.controls;
  }

  buscarInstitucionPorAmie() {
    this.cargando = true;
    this.Amie = this.currentUser.sede.nemonico;
    this.gieeService.buscarInstitucionPorAmie(this.currentUser.sede.nemonico).subscribe(
      (respuesta: ResponseGenerico<Institucion>) => {
        this.institucion = respuesta.objeto;
        this.nombreInstitucion = this.institucion.insDescripcion;
        this.listarEstablecimientosPorAmie();
      })
  }

  listarEstablecimientosPorAmie() {
    this.gieeService.listarEstablecimientosPorAmie(this.currentUser.sede.nemonico).subscribe(
      (respuesta: ResponseGenerico<CatEstablecimiento>) => {
        this.listaEstablecimiento = respuesta.listado;
        setTimeout(() => {
          this.cargando = false;
        }, 500);
      })
  }

  seleccionarEstablecimiento(){
    let regCodigos: number[] = this.institucion.regimenes.map(regimen => regimen.regCodigo);

    let result = this.codigosRealm.filter((item,index)=>{
      return this.codigosRealm.indexOf(item) === index;
    })

    try{

      result.forEach(element => {
        this.catalogoService.listarRegimenPorAnioElectivo(element).subscribe(
          (respuesta: ResponseGenerico<regimenInterface>) => {
            let lista = respuesta.objeto;
            this.arrayComparativo.push(lista);
          }
        )
      });

    } finally{

      setTimeout(() => {
        let datos: number[] = this.arrayComparativo.map(comparativo => comparativo.regCodigo);
        this.listarRegimenAnioLectivoPorTipo(datos);
      }, 1500);

    }
    
    
  };


  listarRegimenAnioLectivoPorTipo(idRegimen){
    this.cargando = true;
    this.catalogoService.listarRegimenAnioLectivoPorCodigoRegimen(idRegimen).subscribe(
      (respuesta: ResponseGenerico<CatRegimenAnioLectivo>) => {
        let listado = respuesta.listado;

        listado.forEach(element => {
          if(element.reanleTipo == "A"){
            this.listaRegimenesActual.push(element);
          }
        });

        setTimeout(() => {
          this.cargando = false;
        }, 500);
      })
  }

  seleccionarRegimen(event){
    //this.buscarNivelesPermiso();

    this.listaRegimenesActual.forEach(element => {
      this.nombreAñoElectivo = element.descripcionAnioLectivo;
    });

    this.codigoRegimen = parseInt(event);
    
  };

  buscarNivelesPermiso(){
    this.gieeService.buscarNivelPermisoPorCodigoInstitucion(this.institucion.insCodigo).subscribe(
      (respuesta: ResponseGenerico<NivelPermisoIntitucionInterface>) => {
        this.listaPermisosNivel = respuesta.listado;
        this.listarNiveles();
      })
  }

  listarNiveles() {
    this.catalogoService.listarNiveles().subscribe(
        (respuesta: ResponseGenerico<CatNivel>) => {
            this.listaNiveles = respuesta.listado;
            let listaNivelesFiltrados = this.listaPermisosNivel
                .map(element => this.listaNiveles.find(item => item.nivCodigo === element.nivCodigo))
                .filter((nivel, index, array) => array.findIndex(item => item?.nivCodigo === nivel?.nivCodigo) === index);
            this.listaNiveles = listaNivelesFiltrados;
        });
  }


  getListaGrupos(): any[] {
    return this.listaGradosTutorFiltro.filter(codigo => codigo.nivCodigo === this.codigoNivel);
}

  seleccionarNivel(event){
    this.listaGradosData = null;
    this.frmCertificado.get('codigoGrado').setValue(null);
    this.frmCertificado.get('codigoJornada').setValue(null);
    this.codigoNivel = parseInt(event);
    this.grado = "0";
    this.listaGradosData = [];

    this.listaGradosData = this.getListaGrupos();
  };

  seleccionarJornada(event){
    //this.listarGrados();
    //this.listarParaleloGrados();
    let JornadaSeleccion = this.listaJornadaFiltro.filter(codigo => codigo.jorCodigo === parseInt(event));

    JornadaSeleccion.forEach(element => {
      this.nombreJornada = element.jorNombre;
    }); 

    this.SacarCurParalelos();
  };

  seleccionarGrado(event){

    this.listaMatriculas=[];
     this.dataSource= new MatTableDataSource<MatriculaOrdinaria>(this.listaMatriculas);
     
    let Grados = this.listaGradosTutorFiltro.filter(codigo => codigo.graCodigo === parseInt(event));
    Grados.forEach(element => {
      this.nombreGrado = element.graDescripcion;
    });
    this.listaParaleFiltro = [];
    this.getListaParalelo();

    this.codigoGrado = parseInt(event);
   };

  listarParaleloGrados(){
    let parametro={
      codigoEstablecimiento:this.frmCertificado.get('codigoEstablecimiento').value,
      codigoReanle:this.frmCertificado.get('codigoRegimen').value,
      codigoGrado:this.frmCertificado.get('codigoGrado').value,
      codigoJornada:this.frmCertificado.get('codigoJornada').value,
      codigoEspecialidad:null
    }
    this.ofertaService.listarParalelosCertificadoPromocion(parametro).subscribe(
      (respuesta: ResponseGenerico<CursoParalelo>) => {
        this.listaParalelos = respuesta.listado;
      })
  }

  getListaParalelo(): any[] {
    return this.listaCodigoParalelos.filter(codigo => codigo.gradoSeleccionado === this.codigoGrado);
}

  async SacarCurParalelos(){
    this.getListaParalelo().forEach(element => {;
      this.ofertaService.buscarCursoParaleloPorCodigo(element.codigoParalelo).subscribe(
        (respuesta: ResponseGenerico<curParaleloInterface>) => {
          let listado = respuesta.objeto;
          this.sacarParalelos(listado.parCodigo, listado.curparCodigo);
        })
    });
  }

  async sacarParalelos(cod, cuparCodigo){
    try{
        this.ofertaService.buscarParaleloPorCodigo(cod).subscribe(
            (respuesta: ResponseGenerico<paraleloInterface>) => {
                let listado = respuesta.objeto;
                let paraleloConCodigo = {
                    paralelo: listado,
                    cuparCodigo: cuparCodigo
                };
                this.listaParaleFiltro.push(paraleloConCodigo);
            }
        );
    } finally{
        // Código que desees ejecutar después de la suscripción
    }
}


  seleccionarParalelo(event){
    this.listaParaleFiltro.forEach(element => {
      if(parseInt(event) === element.cuparCodigo){
        this.codigoParalelo = parseInt(event);
        this.nombreParalelo = element.paralelo.parDescripcion;
      }
    });
    
    if(parseInt(event) != 0){
      this.sacarEstudiantes();
    }
  };

  async sacarEstudiantes(){
    this.spinner.show();
    let realmCodigo = parseInt(this.frmCertificado.get('codigoRegimen').value);
    this.matriculaService.listaMatriculasPorRegimenAnioLectivoYParalelo(realmCodigo,this.codigoParalelo).subscribe(
      (respuesta: ResponseGenerico<MatriculaOrdinaria>) => {
        this.listaMatriculas = respuesta.listado;
        this.listaMatriculas = orderBy(this.listaMatriculas, ['estudiante.estNombres'], ['asc']);
        this.dataSource= new MatTableDataSource<MatriculaOrdinaria>(this.listaMatriculas);
        this.dataSource.paginator = this.paginator;
        this.spinner.hide();
      })
  }


   obtenerValoresUnicos(data, columna) {
    const valoresUnicos = new Set();
    // Iterar sobre cada objeto en el arreglo
    data.forEach(objeto => {
      // Agregar el valor de la columna deseada al conjunto
      valoresUnicos.add(objeto[columna]);
    });
    // Convertir el conjunto a un arreglo si es necesario
    return Array.from(valoresUnicos);
  }
  
  async sacarDatosFiltrados(listaData, listaDataN, element){
    this.spinner.hide();
    const valoresUnicos = this.obtenerValoresUnicos(listaData, 'nombreMateria');
  
    const valoresUnicosB = this.obtenerValoresUnicos(listaDataN, 'nombreMateria');
  
      const notasMateriasB = valoresUnicosB.map(obj => {
        return {
            nombreMateria: obj,
            notas: listaData.filter(elemento => elemento.nombreMateria === obj)
        }
      })
    const notasMaterias = valoresUnicos.map(obj => {
      return {
          nombreMateria: obj,
          notas: listaData.filter(elemento => elemento.nombreMateria === obj)
      }
    })
    
    if(parseInt(this.frmCertificado.get('codigoGrado').value) >= 4 && parseInt(this.frmCertificado.get('codigoGrado').value) <= 6){
      //elemental
      if(parseInt(this.frmCertificado.get('codigoGrado').value) === 6){
        this.notasElemental2(notasMaterias, notasMateriasB ,element);
      }else{
        this.notasElemental(notasMaterias, notasMateriasB ,element);
      }
      
    }else if(parseInt(this.frmCertificado.get('codigoGrado').value) >=7 && parseInt(this.frmCertificado.get('codigoGrado').value) <= 9){
      //media
      this.notasMedia(notasMaterias, notasMateriasB, element);
    }else if(parseInt(this.frmCertificado.get('codigoGrado').value) >= 10){
      //bachillerato
      this.notaBachillerato(notasMaterias, notasMateriasB ,element);
    }
  }


  async notasElemental2(notasMaterias, notasMateriasB, element) {
    try {
      const datosRelacionados = [];
      const datosRelacionadosB = [];
      const listaDescripcion = [];
  
      const obtenerNota = (nota) => nota ? nota : 0;
  
      const procesarNotas = (datosSistema, datosRelacionados) => {
        datosSistema.notas.forEach(nota => {
          switch (nota.identificador) {
            case "TRIMESTRE 1":
              this.notaPrimerTrimestre = obtenerNota(nota.nota);
              break;
            case "TRIMESTRE 2":
              this.notaSegundoTrimestre = obtenerNota(nota.nota);
              break;
            case "TRIMESTRE 3":
              this.notaTercerTrimestre = obtenerNota(nota.nota);
              break;
            case "PROMEDIO TRIMESTRE":
              this.promedioTrimestre = obtenerNota(nota.nota);
              break;
            case "EVALUACION FINAL":
              this.promedioAnual = obtenerNota(nota.nota);
              break;
            default:
              break;
          }
        });
  
        const datosAlmacenados = {
          nombreMateria: datosSistema.nombreMateria,
          nota1: this.notaPrimerTrimestre,
          nota2: this.notaSegundoTrimestre,
          nota3: this.notaTercerTrimestre,
          promedioTrimestre: this.promedioTrimestre,
          evaluacionFinal: this.promedioAnual
        };
  
        datosRelacionados.push(datosAlmacenados);
      };
  
      for (const datosSistema of notasMaterias) {
        procesarNotas(datosSistema, datosRelacionados);
      }
  
      for (const datosSistema of notasMateriasB) {
        procesarNotas(datosSistema, datosRelacionadosB);
  
        if (datosSistema.nombreMateria === "ACOMPAÑAMIENTO INTEGRAL EN EL AULA") {
          const respuestas = await forkJoin([
            this.academicoService.buscarPonderacionPorNota(this.notaPrimerTrimestre),
            this.academicoService.buscarPonderacionPorNota(this.notaSegundoTrimestre),
            this.academicoService.buscarPonderacionPorNota(this.notaTercerTrimestre)
          ]).toPromise();
  
          const descripciones = respuestas.map(respuesta => respuesta.objeto.pocoObs);
  
          const datosDescripcion = {
            nombreMateria: datosSistema.nombreMateria,
            descripcion1: descripciones[0],
            descripcion2: descripciones[1],
            descripcion3: descripciones[2]
          };
  
          listaDescripcion.push(datosDescripcion);
        }
      }
  
      await this.crearPDFElemental2(element, datosRelacionados, datosRelacionadosB, listaDescripcion);
    } catch (error) {
      console.error("Ocurrió un error:", error);
    }
  }
  
  async notasElemental(notasMaterias, notasMateriasB, element) {
    try {
      const datosRelacionados = [];
      const datosRelacionadosB = [];
      const listaDescripcion = [];
  
      const obtenerNota = (nota) => nota ? nota : 0;
  
      const procesarNotas = (datosSistema, datosRelacionados) => {
        datosSistema.notas.forEach(nota => {
          switch (nota.identificador) {
            case "TRIMESTRE 1":
              this.notaPrimerTrimestre = obtenerNota(nota.nota);
              break;
            case "TRIMESTRE 2":
              this.notaSegundoTrimestre = obtenerNota(nota.nota);
              break;
            case "TRIMESTRE 3":
              this.notaTercerTrimestre = obtenerNota(nota.nota);
              break;
            case "PROMEDIO TRIMESTRE":
              this.promedioTrimestre = obtenerNota(nota.nota);
              break;
            case "SUPLETORIO":
              this.supletorio = obtenerNota(nota.nota);
              break;
            case "EVALUACION FINAL":
              this.promedioAnual = obtenerNota(nota.nota);
              break;
            default:
              break;
          }
        });
  
        const datosAlmacenados = {
          nombreMateria: datosSistema.nombreMateria,
          nota1: this.notaPrimerTrimestre,
          nota2: this.notaSegundoTrimestre,
          nota3: this.notaTercerTrimestre,
          promedioTrimestre: this.promedioTrimestre
        };
  
        datosRelacionados.push(datosAlmacenados);
      };
  
      for (const datosSistema of notasMaterias) {
        procesarNotas(datosSistema, datosRelacionados);
      }
  
      for (const datosSistema of notasMateriasB) {
        procesarNotas(datosSistema, datosRelacionadosB);
  
        if (datosSistema.nombreMateria === "ACOMPAÑAMIENTO INTEGRAL EN EL AULA") {
          const respuestas = await forkJoin([
            this.academicoService.buscarPonderacionPorNota(this.notaPrimerTrimestre),
            this.academicoService.buscarPonderacionPorNota(this.notaSegundoTrimestre),
            this.academicoService.buscarPonderacionPorNota(this.notaTercerTrimestre)
          ]).toPromise();
  
          respuestas.forEach((respuesta, index) => {
            const obj = respuesta.objeto;
            if (index === 0) {
              this.descripcion1 = obj.pocoObs;
            } else if (index === 1) {
              this.descripcion2 = obj.pocoObs;
            } else if (index === 2) {
              this.descripcion3 = obj.pocoObs;
            }
          });
  
          const datos = {
            nombreMateria: datosSistema.nombreMateria,
            descripcion1: this.descripcion1,
            descripcion2: this.descripcion2,
            descripcion3: this.descripcion3
          };
  
          listaDescripcion.push(datos);
        }
      }
  
      await this.crearPDFElemental(element, datosRelacionados, datosRelacionadosB, listaDescripcion);
  
    } catch (error) {
      console.error("Ocurrió un error:", error);
    }
  }
  
  async notasMedia(notasMaterias, notasMateriasB, element) {
    try {
      const datosRelacionados = [];
      const datosRelacionadosB = [];
      const listaDescripcion = [];
  
      const obtenerNota = (nota) => nota ? nota : 0;
  
      const procesarNotas = async (datosSistema, datosRelacionados, listaDescripcion) => {
        datosSistema.notas.forEach(async (nota) => {
          switch (nota.identificador) {
            case "TRIMESTRE 1":
              this.notaPrimerTrimestre = obtenerNota(nota.nota);
              break;
            case "TRIMESTRE 2":
              this.notaSegundoTrimestre = obtenerNota(nota.nota);
              break;
            case "TRIMESTRE 3":
              this.notaTercerTrimestre = obtenerNota(nota.nota);
              break;
            case "PROMEDIO TRIMESTRE":
              this.promedioTrimestre = obtenerNota(nota.nota);
              break;
            case "SUPLETORIO":
              this.supletorio = obtenerNota(nota.nota);
              break;
            case "EVALUACION FINAL":
              this.promedioAnual = obtenerNota(nota.nota);
              break;
            default:
              break;
          }
        });
  
        const datosAlmacenados = {
          nombreMateria: datosSistema.nombreMateria,
          nota1: this.notaPrimerTrimestre,
          nota2: this.notaSegundoTrimestre,
          nota3: this.notaTercerTrimestre,
          promedioTrimestral: this.promedioTrimestre,
          supletorio: this.supletorio,
          promedioAnual: this.promedioAnual
        };
  
        datosRelacionados.push(datosAlmacenados);
  
        if (datosSistema.nombreMateria === "ACOMPAÑAMIENTO INTEGRAL EN EL AULA") {
          const respuestas = await forkJoin([
            this.academicoService.buscarPonderacionPorNota(this.notaPrimerTrimestre),
            this.academicoService.buscarPonderacionPorNota(this.notaSegundoTrimestre),
            this.academicoService.buscarPonderacionPorNota(this.notaTercerTrimestre)
          ]).toPromise();
  
          const descripciones = respuestas.map(respuesta => respuesta.objeto.pocoObs);
  
          const datosDescripcion = {
            nombreMateria: datosSistema.nombreMateria,
            descripcion1: descripciones[0],
            descripcion2: descripciones[1],
            descripcion3: descripciones[2]
          };
  
          listaDescripcion.push(datosDescripcion);
        }
      };
  
      for (const datosSistema of notasMaterias) {
        await procesarNotas(datosSistema, datosRelacionados, listaDescripcion);
      }
  
      for (const datosSistema of notasMateriasB) {
        await procesarNotas(datosSistema, datosRelacionadosB, listaDescripcion);
      }
  
      await this.crearPDFMedia(element, datosRelacionados, datosRelacionadosB, listaDescripcion);
    } catch (error) {
      console.error("Ocurrió un error:", error);
    }
  }
  
  async notaBachillerato(notasMaterias, notasMateriasB, element) {
    try {
      const datosRelacionados = [];
      const datosRelacionadosB = [];
      const listaDescripcion = [];
  
      const obtenerNota = (nota) => nota ? nota : 0;
  
      const procesarNotas = async (datosSistema, datosRelacionados, listaDescripcion) => {
        datosSistema.notas.forEach(async (nota) => {
          switch (nota.identificador) {
            case "TRIMESTRE 1":
              this.notaPrimerTrimestre = obtenerNota(nota.nota);
              break;
            case "TRIMESTRE 2":
              this.notaSegundoTrimestre = obtenerNota(nota.nota);
              break;
            case "TRIMESTRE 3":
              this.notaTercerTrimestre = obtenerNota(nota.nota);
              break;
            case "PROMEDIO TRIMESTRE":
              this.promedioTrimestre = obtenerNota(nota.nota);
              break;
            case "SUPLETORIO":
              this.supletorio = obtenerNota(nota.nota);
              break;
            case "EVALUACION FINAL":
              this.promedioAnual = obtenerNota(nota.nota);
              break;
            default:
              break;
          }
        });
  
        const datosAlmacenados = {
          nombreMateria: datosSistema.nombreMateria,
          nota1: this.notaPrimerTrimestre,
          nota2: this.notaSegundoTrimestre,
          nota3: this.notaTercerTrimestre,
          promedioTrimestral: this.promedioTrimestre,
          supletorio: this.supletorio,
          promedioAnual: this.promedioAnual
        };
  
        datosRelacionados.push(datosAlmacenados);
  
        if (datosSistema.nombreMateria === "ACOMPAÑAMIENTO INTEGRAL EN EL AULA") {
          const respuestas = await forkJoin([
            this.academicoService.buscarPonderacionPorNota(this.notaPrimerTrimestre),
            this.academicoService.buscarPonderacionPorNota(this.notaSegundoTrimestre),
            this.academicoService.buscarPonderacionPorNota(this.notaTercerTrimestre)
          ]).toPromise();
  
          const descripciones = respuestas.map(respuesta => respuesta.objeto.pocoObs);
  
          const datosDescripcion = {
            nombreMateria: datosSistema.nombreMateria,
            descripcion1: descripciones[0],
            descripcion2: descripciones[1],
            descripcion3: descripciones[2]
          };
  
          listaDescripcion.push(datosDescripcion);
        }
      };
  
      for (const datosSistema of notasMaterias) {
        await procesarNotas(datosSistema, datosRelacionados, listaDescripcion);
      }
  
      for (const datosSistema of notasMateriasB) {
        await procesarNotas(datosSistema, datosRelacionadosB, listaDescripcion);
      }
  
      await this.crearPDFBachillerato(element, datosRelacionados, datosRelacionadosB, listaDescripcion);
    } catch (error) {
      console.error("Ocurrió un error:", error);
    }
  }


  async crearPDFPrepa(element, notasMaterias){
    const pdfDefinition = {
      content: [
        {
          columns: [
            {
              image: await this.getBase64ImageFromURL("../../assets/images/ico/logoUnido.png"),
              width: 80,
              alignment: 'center',
            },
            {
              stack: [
                { text: "UNIDAD EDUCATIVA FISCAL: " + this.nombreInstitucion, style: 'tituloPequeno' },
                { text: "CÓDIGO AMIE: " + this.Amie, style: 'tituloPequeno'},
                { text: "AÑO LECTIVO: "+ this.nombreAñoElectivo, style: 'tituloLargo'},
                { text: "JORNADA: "+this.nombreJornada + "        GRADO PARALELO: "+this.nombreGrado+ " "+this.nombreParalelo,  style: 'tituloLargo'}
              ],
              margin: [0, 20, 0, -90],
              width: '*',
            },
          ],
        },
        { text: 'Nombre Estudiante: '+element.estudiante.estNombres, style: 'subtitulo' }, // Subtítulo agregado aquí
        {
          table: {
            headerRows: 1,
            widths: [90, 250, 30, 30, 30],
            body: [
              [
                { text: 'AMBITO', rowSpan: 2, alignment: 'center'},
                { text: 'DESTREZAS', rowSpan: 2, alignment: 'center'},
                { text: 'TRIMESTRES', colSpan: 3, alignment: 'center'},
                { text: '', alignment: 'center'},
                { text: '', alignment: 'center'},
              ],
              [
                { text: '', alignment: 'center'},
                { text: '', alignment: 'center'},
                { text: 'I', alignment: 'center'},
                { text: 'II', alignment: 'center'},
                { text: 'III', alignment: 'center'},
                
              ],
              ...notasMaterias.map(item => [item.nombreAmbito, item.nombreDestreza, item.nota1, item.nota2, item.nota3])
            ],
          },
          style: 'datosTabla', // Aplica el estilo a la tabla
        },
      ],
      styles: {
        titulo: {
          fontSize: 16,
          bold: true,
        },
        tituloPequeno: {
          fontSize: 10,
          bold: true,
          alignment: 'center',
          lineHeight: 1.5, // Ajusta el espaciado entre líneas
        },
        tituloLargo: {
          fontSize: 8,
          alignment: 'center',
          bold: false,
          lineHeight: 1.5, // Ajusta el espaciado entre líneas
        },
        subtitulo: {
          fontSize: 10,
          bold: true,
          alignment: 'left',
          margin: [0, 40, 0, 5], // Ajusta el margen del subtitulo
        },
        datosTabla: {
          fontSize: 8,
          alignment: 'center',
          margin: [0, 20, 0, 0],
          fillColor: '#F2F2F2',
        },
      },
    };
  
    const pdf = pdfMake.createPdf(pdfDefinition);
    pdf.open();
    
    
  }
  
  async crearPDFElemental2(element, notasMaterias, notasMateriasB, listaDescripcion) {
    const header = {
        image: await this.getBase64ImageFromURL("../../assets/images/ico/logoUnido.png"),
        width: 80,
        alignment: 'center',
    };
  
    const stack = [
        { text: "UNIDAD EDUCATIVA FISCAL: " + this.nombreInstitucion, style: 'tituloPequeno' },
        { text: "CÓDIGO AMIE: " + this.Amie, style: 'tituloPequeno' },
        { text: "AÑO LECTIVO: " + this.nombreAñoElectivo, style: 'tituloLargo' },
        { text: "JORNADA: " + this.nombreJornada + "        GRADO PARALELO: " + this.nombreGrado + " " + this.nombreParalelo, style: 'tituloLargo' }
    ];
  
    const pdfDefinition = {
        content: [
            {
                columns: [
                    header,
                    {
                        stack: stack,
                        margin: [0, 20, 0, -90],
                        width: '*',
                    },
                ],
            },
            { text: 'Nombre Estudiante: ' + element.estudiante.estNombres, style: 'subtitulo' },
            {
                table: {
                    headerRows: 1,
                    widths: [150, 60, 60, 60, 60, 60],
                    body: [
                        [
                            { text: 'ASIGNATURA', rowSpan: 2, alignment: 'center' },
                            { text: 'TRIMESTRES', colSpan: 3, alignment: 'center' },
                            { text: '' },
                            { text: '' },
                            { text: 'PROMEDIO TRIMESTRE', rowSpan: 2, alignment: 'center' },
                            { text: 'EVALUACION FINAL', rowSpan: 2, alignment: 'center' }
                        ],
                        [
                            { text: '' },
                            { text: 'I', alignment: 'center' },
                            { text: 'II', alignment: 'center' },
                            { text: 'III', alignment: 'center' },
                            { text: '', alignment: 'center' },
                            { text: '', alignment: 'center' }
                        ],
                        ...notasMaterias.map(item => [item.nombreMateria, item.nota1, item.nota2, item.nota3, item.promedioTrimestre, item.evaluacionFinal]),
                        [
                            { text: "PROMEDIO ANUAL", colSpan: 5, alignment: 'center' },
                            {},
                            {},
                            {},
                            {},
                            { text: this.promedioAnualTotal }
                        ],
                    ],
                },
                style: 'datosTabla',
            },
        ],
        styles: {
            titulo: {
                fontSize: 16,
                bold: true,
            },
            tituloPequeno: {
                fontSize: 10,
                bold: true,
                alignment: 'center',
                lineHeight: 1.5,
            },
            tituloLargo: {
                fontSize: 8,
                alignment: 'center',
                bold: false,
                lineHeight: 1.5,
            },
            subtitulo: {
                fontSize: 10,
                bold: true,
                alignment: 'left',
                margin: [0, 40, 0, 5],
            },
            datosTabla: {
                fontSize: 8,
                alignment: 'center',
                margin: [0, 20, 0, 0],
                fillColor: '#F2F2F2',
            },
            segundaTabla: {
                fontSize: 8,
                alignment: 'center',
                margin: [0, 5, 0, 5],
                fillColor: '#F2F2F2',
            },
        },
    };
  
    if (notasMateriasB.length > 0) {
        pdfDefinition.content.push({
            table: {
                headerRows: 1,
                widths: [150, 60, 60, 60, 60, 60],
                body: [
                    ...notasMateriasB.map(item => [item.nombreMateria, item.nota1, item.nota2, item.nota3, item.promedioTrimestre, item.evaluacionFinal])
                ],
            },
            style: 'segundaTabla',
        });
    }
  
    if (listaDescripcion.length > 0) {
        pdfDefinition.content.push({
            table: {
                headerRows: 1,
                widths: [150, 60, 60, 60],
                body: [
                    ...listaDescripcion.map(item => [item.nombreMateria, item.descripcion1, item.descripcion2, item.descripcion3])
                ],
            },
            style: 'segundaTabla',
        });
    }
  
    const pdf = pdfMake.createPdf(pdfDefinition);
    pdf.open();
  }
  
  async crearPDFElemental(element, notasMaterias, notasMateriasB, listaDescripcion) {
    const header = {
        image: await this.getBase64ImageFromURL("../../assets/images/ico/logoUnido.png"),
        width: 80,
        alignment: 'center',
    };
  
    const stack = [
        { text: "UNIDAD EDUCATIVA FISCAL: " + this.nombreInstitucion, style: 'tituloPequeno' },
        { text: "CÓDIGO AMIE: " + this.Amie, style: 'tituloPequeno' },
        { text: "AÑO LECTIVO: " + this.nombreAñoElectivo, style: 'tituloLargo' },
        { text: "JORNADA: " + this.nombreJornada + "        GRADO PARALELO: " + this.nombreGrado + " " + this.nombreParalelo, style: 'tituloLargo' }
    ];
  
    const pdfDefinition = {
        content: [
            {
                columns: [
                    header,
                    {
                        stack: stack,
                        margin: [0, 20, 0, -90],
                        width: '*',
                    },
                ],
            },
            { text: 'Nombre Estudiante: ' + element.estudiante.estNombres, style: 'subtitulo' },
            {
                table: {
                    headerRows: 1,
                    widths: [150, 80, 80, 80, 80],
                    body: [
                        [
                            { text: 'ASIGNATURA', rowSpan: 2, alignment: 'center' },
                            { text: 'TRIMESTRES', colSpan: 3, alignment: 'center' },
                            { text: '' },
                            { text: '' },
                            { text: 'PROMEDIO TRIMESTRE', rowSpan: 2, alignment: 'center' }
                        ],
                        [
                            { text: '', alignment: 'center'},
                            { text: 'I', alignment: 'center' },
                            { text: 'II', alignment: 'center' },
                            { text: 'III', alignment: 'center' },
                            { text: '', alignment: 'center'}
                        ],
                        ...notasMaterias.map(item => [item.nombreMateria, item.nota1, item.nota2, item.nota3, item.promedioTrimestre]),
                        [
                            { text: "PROMEDIO ANUAL", colSpan: 4, alignment: 'center' },
                            {},
                            {},
                            {},
                            { text: this.promedioAnualTotal }
                        ],
                    ],
                },
                style: 'datosTabla',
            },
        ],
        styles: {
            titulo: {
                fontSize: 16,
                bold: true,
            },
            tituloPequeno: {
                fontSize: 10,
                bold: true,
                alignment: 'center',
                lineHeight: 1.5,
            },
            tituloLargo: {
                fontSize: 8,
                alignment: 'center',
                bold: false,
                lineHeight: 1.5,
            },
            subtitulo: {
                fontSize: 10,
                bold: true,
                alignment: 'left',
                margin: [0, 40, 0, 5],
            },
            datosTabla: {
                fontSize: 8,
                alignment: 'center',
                margin: [0, 20, 0, 0],
                fillColor: '#F2F2F2',
            },
            segundaTabla: {
                fontSize: 8,
                alignment: 'center',
                margin: [0, 5, 0, 5],
                fillColor: '#F2F2F2',
            },
        },
    };
  
    if (listaDescripcion.length > 0) {
        pdfDefinition.content.push({
            table: {
                headerRows: 1,
                widths: [150, 80, 80, 80, 80],
                body: [
                    ...notasMateriasB.map(item => [item.nombreMateria, item.nota1, item.nota2, item.nota3, item.promedioTrimestre])
                ],
            },
            style: 'segundaTabla',
        });
  
        pdfDefinition.content.push({
          table: {
              headerRows: 1,
              widths: [150, 80, 80, 80],
              body: [
                  ...listaDescripcion.map(item => [item.nombreMateria, item.descripcion1, item.descripcion2, item.descripcion3])
              ],
          },
          style: 'segundaTabla',
      });
  
    }
  
    const pdf = pdfMake.createPdf(pdfDefinition);
    pdf.open();
  }
  
  async crearPDFMedia(element, notasMaterias, notasMateriasB, listaDescripcion) {
    const header = {
        image: await this.getBase64ImageFromURL("../../assets/images/ico/logoUnido.png"),
        width: 80,
        alignment: 'center',
    };
  
    const stack = [
        { text: "UNIDAD EDUCATIVA FISCAL: " + this.nombreInstitucion, style: 'tituloPequeno' },
        { text: "CÓDIGO AMIE: " + this.Amie, style: 'tituloPequeno' },
        { text: "AÑO LECTIVO: " + this.nombreAñoElectivo, style: 'tituloLargo' },
        { text: "JORNADA: " + this.nombreJornada + "        GRADO PARALELO: " + this.nombreGrado + " " + this.nombreParalelo, style: 'tituloLargo' }
    ];
  
    const pdfDefinition = {
        content: [
            {
                columns: [
                    header,
                    {
                        stack: stack,
                        margin: [0, 20, 0, -90],
                        width: '*',
                    },
                ],
            },
            { text: 'Nombre Estudiante: ' + element.estudiante.estNombres, style: 'subtitulo' },
            {
                table: {
                    headerRows: 1,
                    widths: [200, 80, 80, 80, 80, 80, 80],
                    body: [
                        [
                            { text: 'ASIGNATURA', rowSpan: 2, alignment: 'center' },
                            { text: 'TRIMESTRES', colSpan: 3, alignment: 'center' },
                            { text: '' },
                            { text: '' },
                            { text: 'PROMEDIO TRIMESTRE', rowSpan: 2, alignment: 'center' },
                            { text: 'SUPLETORIO', rowSpan: 2, alignment: 'center' },
                            { text: 'PROMEDIO ANUAL', rowSpan: 2, alignment: 'center' }
                        ],
                        [
                            { text: '', alignment: 'center'},
                            { text: 'I', alignment: 'center'},
                            { text: 'II', alignment: 'center'},
                            { text: 'III', alignment: 'center'},
                            { text: '', alignment: 'center'},
                            { text: '', alignment: 'center'},
                            { text: '', alignment: 'center'}
                        ],
                        ...notasMaterias.map(item => [item.nombreMateria, item.nota1, item.nota2, item.nota3, item.promedioTrimestral, item.supletorio, item.promedioAnual]),
                        [
                            { text: "PROMEDIO ANUAL", colSpan: 6, alignment: 'center' },
                            {},
                            {},
                            {},
                            {},
                            {},
                            { text: this.promedioAnualTotal }
                        ],
                    ],
                },
                style: 'datosTabla',
            },
        ],
        styles: {
            titulo: {
                fontSize: 16,
                bold: true,
            },
            tituloPequeno: {
                fontSize: 10,
                bold: true,
                alignment: 'center',
                lineHeight: 1.5,
            },
            tituloLargo: {
                fontSize: 8,
                alignment: 'center',
                bold: false,
                lineHeight: 1.5,
            },
            subtitulo: {
                fontSize: 10,
                bold: true,
                alignment: 'left',
                margin: [0, 40, 0, 5],
            },
            datosTabla: {
                fontSize: 8,
                alignment: 'center',
                margin: [0, 5, 0, 5],
                fillColor: '#F2F2F2',
            },
            segundaTabla: {
                fontSize: 8,
                alignment: 'center',
                margin: [0, 5, 0, 5],
                fillColor: '#F2F2F2',
            },
        },
    };
  
    if (notasMateriasB.length > 0) {
        pdfDefinition.content.push({
            table: {
                headerRows: 1,
                widths: [200, 80, 80, 80, 80, 80, 80],
                body: [
                    ...notasMateriasB.map(item => [item.nombreMateria, item.nota1, item.nota2, item.nota3, item.promedioTrimestral, item.supletorio, item.promedioAnual])
                ],
            },
            style: 'segundaTabla',
        });
    }
  
    if (listaDescripcion.length > 0) {
        pdfDefinition.content.push({
            table: {
                headerRows: 1,
                widths: [200, 80, 80, 80],
                body: [
                    ...listaDescripcion.map(item => [item.nombreMateria, item.descripcion1, item.descripcion2, item.descripcion3])
                ],
            },
            style: 'segundaTabla',
        });
    }
  
    const pdf = pdfMake.createPdf(pdfDefinition);
    pdf.open();
  }
  
  async crearPDFBachillerato(element, notasMaterias, notasMateriasB, listaDescripcion) {
    const header = {
        image: await this.getBase64ImageFromURL("../../assets/images/ico/logoUnido.png"),
        width: 80,
        alignment: 'center',
    };
  
    const stack = [
        { text: "UNIDAD EDUCATIVA FISCAL: " + this.nombreInstitucion, style: 'tituloPequeno' },
        { text: "CÓDIGO AMIE: " + this.Amie, style: 'tituloPequeno' },
        { text: "AÑO LECTIVO: " + this.nombreAñoElectivo, style: 'tituloLargo' },
        { text: "JORNADA: " + this.nombreJornada + "        GRADO PARALELO: " + this.nombreGrado + " " + this.nombreParalelo, style: 'tituloLargo' }
    ];
  
    const pdfDefinition = {
        content: [
            {
                columns: [
                    header,
                    {
                        stack: stack,
                        margin: [0, 20, 0, -90],
                        width: '*',
                    },
                ],
            },
            { text: 'Nombre Estudiante: ' + element.estudiante.estNombres, style: 'subtitulo' },
            {
                table: {
                    headerRows: 1,
                    widths: [100, 60, 60, 60, 60, 60, 60],
                    body: [
                        [
                            { text: 'ASIGNATURA', rowSpan: 2, alignment: 'center' },
                            { text: 'TRIMESTRES', colSpan: 3, alignment: 'center' },
                            { text: '' },
                            { text: '' },
                            { text: 'PROMEDIO TRIMESTRE', rowSpan: 2, alignment: 'center' },
                            { text: 'SUPLETORIO', rowSpan: 2, alignment: 'center' },
                            { text: 'PROMEDIO ANUAL', rowSpan: 2, alignment: 'center' }
                        ],
                        [
                          { text: '', alignment: 'center' },
                          { text: 'I', alignment: 'center' },
                          { text: 'II', alignment: 'center' },
                          { text: 'III', alignment: 'center' },
                          { text: '', alignment: 'center' },
                          { text: '', alignment: 'center' },
                          { text: '', alignment: 'center' }
                        ],
                        ...notasMaterias.map(item => [item.nombreMateria, item.nota1, item.nota2, item.nota3, item.promedioTrimestral, item.supletorio, item.promedioAnual]),
                        [
                            { text: "PROMEDIO ANUAL", colSpan: 6, alignment: 'center' },
                            {},
                            {},
                            {},
                            {},
                            {},
                            { text: this.promedioAnualTotal }
                        ],
                    ],
                },
                style: 'datosTabla',
            },
        ],
        styles: {
            titulo: {
                fontSize: 16,
                bold: true,
            },
            tituloPequeno: {
                fontSize: 10,
                bold: true,
                alignment: 'center',
                lineHeight: 1.5,
            },
            tituloLargo: {
                fontSize: 8,
                alignment: 'center',
                bold: false,
                lineHeight: 1.5,
            },
            subtitulo: {
                fontSize: 10,
                bold: true,
                alignment: 'left',
                margin: [0, 40, 0, 5],
            },
            datosTabla: {
                fontSize: 8,
                alignment: 'center',
                margin: [0, 20, 0, 0],
                fillColor: '#F2F2F2',
            },
            segundaTabla: {
                fontSize: 8,
                alignment: 'center',
                margin: [0, 5, 0, 5],
                fillColor: '#F2F2F2',
            }
        },
    };
  
    if (notasMateriasB.length > 0) {
        pdfDefinition.content.push({
            table: {
                headerRows: 1,
                widths: [100, 60, 60, 60, 60, 60, 60],
                body: [
                    ...notasMateriasB.map(item => [item.nombreMateria, item.nota1, item.nota2, item.nota3, item.promedioTrimestral, item.supletorio, item.promedioAnual])
                ],
            },
            style: 'segundaTabla',
        });
    }
  
    if (listaDescripcion.length > 0) {
        pdfDefinition.content.push({
            table: {
                headerRows: 1,
                widths: [100, 60, 60, 60],
                body: [
                    ...listaDescripcion.map(item => [item.nombreMateria, item.descripcion1, item.descripcion2, item.descripcion3])
                ],
            },
            style: 'segundaTabla',
        });
    }
  
    const pdf = pdfMake.createPdf(pdfDefinition);
    pdf.open();
  }



  async sacarData(element, listaMaterias){
    try {
      this.objetoDatosRecolectados = [];
      this.objetoDatosRecolectadosN = [];
      this.spinner.show();
      let Grados = listaMaterias.filter(codigo => codigo.acaMallaAsignatura.acaAsignatura.acaTipoAsignatura.tiasObligatorio === "S");
      let GradosA = listaMaterias.filter(codigo => codigo.acaMallaAsignatura.acaAsignatura.acaTipoAsignatura.tiasObligatorio === "N");
      for (const element of Grados) {
          const nombreMateria = element.acaMallaAsignatura.acaAsignatura.asiDescripcion;
          const datosJSON = JSON.parse(element.noasCalNot);
          for (const jsonData of datosJSON) {
              const respuesta = await this.academicoService.buscarCalificacionPorMogenCodigo(jsonData.modgenCodigo).toPromise();
              let realmCodigo = parseInt(this.frmCertificado.get('codigoRegimen').value);
              let lista = respuesta.listado;
              lista.forEach(calificacion => {
                  if(calificacion.reanleCodigo === realmCodigo){
                      jsonData.listaCalificacion.forEach(calificaciones => {
                          let datosGurdado={
                              nombreMateria: nombreMateria,
                              codigoCalificacion: calificacion.calCodigo,
                              nota: calificaciones.noasCalNot,
                              codigoMatricula: element.matCodigo,
                              identificador: calificacion.calDescripcion
                          }
                          this.objetoDatosRecolectados.push(datosGurdado);
                      });
                  }
              });
          }
      }
  
      for (const element of GradosA) {
        const nombreMateria = element.acaMallaAsignatura.acaAsignatura.asiDescripcion;
        const datosJSON = JSON.parse(element.noasCalNot);
        for (const jsonData of datosJSON) {
            const respuesta = await this.academicoService.buscarCalificacionPorMogenCodigo(jsonData.modgenCodigo).toPromise();
            let realmCodigo = parseInt(this.frmCertificado.get('codigoRegimen').value);
            let lista = respuesta.listado;
            lista.forEach(calificacion => {
                if(calificacion.reanleCodigo === realmCodigo){
                    jsonData.listaCalificacion.forEach(calificaciones => {
                        let datosGurdado={
                            nombreMateria: nombreMateria,
                            codigoCalificacion: calificacion.calCodigo,
                            nota: calificaciones.noasCalNot,
                            codigoMatricula: element.matCodigo,
                            identificador: calificacion.calDescripcion
                        }
                        this.objetoDatosRecolectadosN.push(datosGurdado);
                    });
                }
            });
        }
    }
    
    await this.sacarDatosFiltrados(this.objetoDatosRecolectados, this.objetoDatosRecolectadosN, element);
  } catch (error) {
      console.error("Error al buscar notas de asignatura:", error);
  }
  }


  async sacarPromedioAnual(codigo){
    this.academicoService.buscarPromedioPorCodigoMatricula(codigo).subscribe(
      (respuesta: ResponseGenerico<PromedioAnualInterface>) => {
        let lista = respuesta.listado;

        if(lista.length>0){
          lista.forEach(element => {
            this.promedioAnualTotal = element.rproPromedio;
          });
        }else{
          this.promedioAnualTotal = 0
       }
      })
  }

  comparar(a, b) {
    if (a.codigoAmbito < b.codigoAmbito) {
        return -1;
    }
    if (a.codigoAmbito > b.codigoAmbito) {
        return 1;
    }
    return 0;
}

async eliminarRepetidos(array) {
  const hash = {};
  return array.filter(item => {
      const key = `${item.codigoDestreza}-${item.codigoAmbito}-${item.identificador}`;
      if (!hash[key]) {
          hash[key] = true;
          return true;
      }
      return false;
  });
}


async crearFiltroNotas(element, listaMaterias, objMetodoDestreza){

  try{

    let unique = {};
  const datos = objMetodoDestreza.filter(obj => {
    const key = obj.nombreAmbito + '|' + obj.nombreDestreza;
    if (!unique[key]) {
        unique[key] = true;
        return true;
    }
    return false;
});

  listaMaterias.forEach(materia => {
    materia.notas.forEach(notas => {
      datos.forEach(eleccion => {       
        if(notas.nombreDestreza === eleccion.nombreDestreza && notas.nombreAmbito === eleccion.nombreAmbito){
          if(notas.identificador === "TRIMESTRE 1"){
            if(notas.nota != null && notas.nota != 0){ 
              eleccion.nota1 = notas.nota;
            }else{
              eleccion.nota1 = "NE";
            }
          }else if(notas.identificador === "TRIMESTRE 2"){
            if(notas.nota != null && notas.nota != 0){
              eleccion.nota2 = notas.nota;
            }else{
              eleccion.nota2 = "NE";
            }
          }else if(notas.identificador == "TRIMESTRE 3"){
            if(notas.nota != null && notas.nota != 0){
              eleccion.nota3 = notas.nota;
            }else{
              eleccion.nota3 = "NE";
            }
          }
        } 
      });
    });
  });


  datos.forEach(eleccion => {
    if (!eleccion.hasOwnProperty('nota1') || eleccion.nota1 === null || eleccion.nota1 === undefined) {
      eleccion.nota1 = "NE";
  }
  
  if (!eleccion.hasOwnProperty('nota2') || eleccion.nota2 === null || eleccion.nota2 === undefined) {
      eleccion.nota2 = "NE";
  }
  
  if (!eleccion.hasOwnProperty('nota3') || eleccion.nota3 === null || eleccion.nota3 === undefined) {
      eleccion.nota3 = "NE";
  }
  });

  await this.crearPDFPrepa(element, datos.sort());

  } catch{
    
  }
}

async crearFiltroAmbitoDestreza(element, listaMaterias){
  try{
    this.acumuladorAmbitoDestreza = [];
    listaMaterias.forEach(materia => {
      materia.notas.forEach(nota => {
        let datos = {
          nombreAmbito: nota.nombreAmbito,
          nombreDestreza: nota.nombreDestreza
        }
        this.acumuladorAmbitoDestreza.push(datos);
      });
    });

    await this.crearFiltroNotas(element, listaMaterias, this.acumuladorAmbitoDestreza);
  }catch{

  }
}

async sacarFiltrosPrepa(element, listasMaterias){
  try{
    const conjunto = new Set<string>();
  const datosSinRepetidos = listasMaterias.filter((objeto) => {
    const clave = `${objeto.codigoDestreza}-${objeto.codigoAmbito}-${objeto.identificador}`;
    if (conjunto.has(clave)) {
        return false;
    }
    conjunto.add(clave);
    return true;
});
  this.spinner.hide();
  const valoresUnicos = this.obtenerValoresUnicos(datosSinRepetidos, 'nombreAmbito');
  const notasMaterias = valoresUnicos.map(obj => {
    return {
        nombreMateria: obj,
        notas: listasMaterias.filter(elemento => elemento.nombreAmbito === obj)
    }
  })
  await this.crearFiltroAmbitoDestreza(element, notasMaterias);

  } catch{

  }
  

}

async sacarDataPrepa(element, listaMaterias) {
  try {
      this.objetoDatosRecolectados = [];
      for (const notaSistema of listaMaterias) {
          const datosJSON = JSON.parse(notaSistema.nodeCalNotaDes);
          const realmCodigo = parseInt(this.frmCertificado.get('codigoRegimen').value);
          
          const modelo = await this.academicoService.buscarCalificacionPorMogenCodigo(datosJSON.modelo).toPromise();
          const listaModelo = modelo.listado;
          
          for (const modeloData of listaModelo) {
              if (modeloData.reanleCodigo === realmCodigo) {
                  const ambitoData = await this.academicoService.buscarAmbitoPorCodigo(datosJSON.ambito).toPromise();
                  const ambitos = ambitoData.objeto;
                  
                  const respuesta = await this.academicoService.buscarDestrezasPorCodigo(datosJSON.ambito).toPromise();
                  const listaDestreza = respuesta.listado;
                  
                  for (const jsonData of datosJSON.destrezas) {
                      const seleccionDestreza = listaDestreza.filter(obj => obj.desCodigo === jsonData.desCodigo);
                      for (const destre of seleccionDestreza) {
                          const datosGurdado = {
                              nombreAmbito: ambitos.ambDescripcion,
                              nombreDestreza: destre.desDescripcion,
                              codigoDestreza: jsonData.desCodigo,
                              codigoAmbito: ambitos.ambCodigo,
                              nota: jsonData.resEscala,
                              codigoMatricula: element.matCodigo,
                              identificador: modeloData.calDescripcion
                          };
                          this.objetoDatosRecolectados.push(datosGurdado);

                      }
                  }
              }
          }
      }
      await this.sacarFiltrosPrepa(element, this.objetoDatosRecolectados);
  } catch (error) {
      console.error(error);
  }
}

  async generarBoletaCalificacion(element){
    if(this.codigoGrado >= 1 && this.codigoGrado <= 3){
      try{
        const respuesta: ResponseGenerico<notaDestrezaInterface> = await this.academicoService.buscarNotaDestrezaPorMatricula(element.matCodigo).toPromise();
      this.listaMateriasPrepa = respuesta.listado
      }catch (error) {
        console.error("Error al buscar notas de asignatura:", error);
      }

      if(this.listaMateriasPrepa.length === 0){
        Swal.fire({
          icon: "error",
          title: "Atención",
          text: "El estudiante no tiene materias calificadas"
        });
      }else{
        await this.sacarDataPrepa(element, this.listaMateriasPrepa);
      }
      
    }else{

      try {
        await this.sacarPromedioAnual(element.matCodigo)
        const respuesta: ResponseGenerico<notaAsignaturaInterface> = await this.academicoService.buscarNotaAsignaturaPorMatricula(element.matCodigo).toPromise();
        this.listaMaterias = respuesta.listado;
      } catch (error) {
        console.error("Error al buscar notas de asignatura:", error);
      }

      if(this.listaMaterias.length === 0){
        Swal.fire({
          icon: "error",
          title: "Atención",
          text: "El estudiante no tiene materias calificadas"
        });
      }else{
        await this.sacarData(element, this.listaMaterias);
      }
      

    }

  }


  getBase64ImageFromURL(url) {
    return new Promise((resolve, reject) => {
      var img = new Image();
      img.setAttribute("crossOrigin", "anonymous");
  
      img.onload = () => {
        var canvas = document.createElement("canvas");
        canvas.width = img.width;
        canvas.height = img.height;
  
        var ctx = canvas.getContext("2d");
        ctx.drawImage(img, 0, 0);
  
        var dataURL = canvas.toDataURL("image/png");
  
        resolve(dataURL);
      };
  
      img.onerror = error => {
        reject(error);
      };
  
      img.src = url;
    });
  }

}
