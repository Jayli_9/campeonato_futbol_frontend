import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

import { HttpClient } from '@angular/common/http';
import { environment } from 'environments/environment';
import { ResponseGenerico } from '../interfaces/response-generico';
import { MatriculaOrdinaria } from '../interfaces/matricula-ordinaria';


@Injectable({
  providedIn: 'root'
})
export class MatriculaService {

  private readonly URL_REST = environment.url_matricula;

  constructor(public _http:HttpClient) { }

  listaMatriculasPorRegimenAnioLectivoYParalelo(reanleCodigo:number,paraleloCodigo:number): Observable<any> | undefined {
    let url_ws=`${this.URL_REST}/private/listaMatriculasPorRegimenAnioLectivoYParalelo/${reanleCodigo}/${paraleloCodigo}`;
  return this._http.get<ResponseGenerico<MatriculaOrdinaria>>(url_ws);
  }
 
}