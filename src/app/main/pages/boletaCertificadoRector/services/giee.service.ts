import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

import { HttpClient } from '@angular/common/http';
import { environment } from 'environments/environment';
import { CatEstablecimiento } from '../../asignacionDocentes/interface/cat_establecimiento';
import { ResponseGenerico } from '../interfaces/response-generico';
import { NivelPermisoIntitucionInterface } from '../../asignacionDocentes/interface/nivel-Institucion';


@Injectable({
  providedIn: 'root'
})
export class GieeService {

  private readonly URL_REST = environment.url_institucion;

  constructor(public _http:HttpClient) { }

  listarEstablecimientosPorAmie(amie:any): Observable<any> | undefined {
    let url_ws=`${this.URL_REST}/private/listarEstablecimientosPorAmie/${amie}`;
  return this._http.get<ResponseGenerico<CatEstablecimiento>>(url_ws);
  }

  buscarInstitucionPorAmie(amie:any): Observable<any> | undefined {
    let url_ws=`${this.URL_REST}/private/buscarInstitucionPorAmie/${amie}`;
  return this._http.get<ResponseGenerico<CatEstablecimiento>>(url_ws);
  }

  buscarNivelPermisoPorCodigoInstitucion(codigoInstitucion:number): Observable<any> | undefined {
    let url_ws=`${this.URL_REST}/private/buscarNivelPermisoPorCodigoInstitucion/${codigoInstitucion}`;
  return this._http.get<ResponseGenerico<NivelPermisoIntitucionInterface>>(url_ws);
  }
  
}