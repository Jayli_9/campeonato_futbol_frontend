import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import pdfMake from 'pdfmake/build/pdfmake';
import pdfFonts from 'pdfmake/build/vfs_fonts';
import QRCode from 'qrcode';
import { ParametrosCertificado } from '../interfaces/parametros-certificado';

pdfMake.vfs = pdfFonts.pdfMake.vfs;

@Injectable({
  providedIn: 'root'
})
export class PdfCertificadoService {

  constructor(private http: HttpClient) {}

  async generatePdf(parametrosCertificado: ParametrosCertificado): Promise<void> {
    const codigoAmie = 'CÓDIGO AMIE';
    const anoLectivo = 'AÑO LECTIVO';
    const regimen = 'RÉGIMEN';

    const documentDefinition = {
      
      content: [
        { image: await this.getBase64ImageFromURL("assets/images/banner/logo_ministerio.png"),
        width: 550,
        alignment: 'center',
        margin: [0, -20, 0, 10]},
        {
          absolutePosition: { x: 0, y:200 },
          width: 595.28, 
          height: 500, 
          image: await this.getBase64ImageFromURL("assets/images/backgrounds/escudo.png"),
          opacity: 0.8,
        },
        {
          table: {
            widths: ['33.33%', '33.33%', '33.33%'],
            body: [
              [
                {
                  text: [
                    { text: `${codigoAmie}: `, style: 'bold' },
                    { text: parametrosCertificado.codigoAmie, style: 'normal' }
                  ],
                  style: 'subtitle',
                  alignment: 'left'
                },
                {
                  text: [
                    { text: `${anoLectivo}: `, style: 'bold' },
                    { text: parametrosCertificado.anioLectivo, style: 'normal' }
                  ],
                  style: 'subtitle',
                  alignment: 'left'
                },
                {
                  text: [
                    { text: `${regimen}: `, style: 'bold' },
                    { text: parametrosCertificado.regimen, style: 'normal' }
                  ],
                  style: 'subtitle',
                  alignment: 'left'
                }
              ]
            ]
          },
          layout: 'noBorders'
        },
        {
          table: {
            widths: ['33.33%', '33.33%', '33.33%'],
            body: [
              [
                {
                  text: [
                    { text: "MODALIDAD: ", style: 'bold' },
                    { text: parametrosCertificado.modalidad, style: 'normal' }
                  ],
                  style: 'subtitle',
                  alignment: 'left'
                },
                {
                  text: [
                    { text: '', style: 'bold' },
                    { text: '', style: 'normal' }
                  ],
                  style: 'subtitle',
                  alignment: 'left'
                },
                {
                  text: [
                    { text: "JORNADA: ", style: 'bold' },
                    { text: parametrosCertificado.jornada, style: 'normal' }
                  ],
                  style: 'subtitle',
                  alignment: 'left'
                }
              ]
            ]
          },
          layout: 'noBorders'
        },
        { text: 'CERTIFICADO DE PROMOCIÓN', style: 'title', alignment: 'center', margin: [0, 20, 0, 20] },
        { text: 'La Máxima Autoridad de la Institución Educativa:', style: 'text', alignment: 'left', margin: [0, 10, 0, 20] },
        { text: parametrosCertificado.institucion, style: 'amie', alignment: 'center' },
        { text: 'De conformidad con lo prescrito en el Art. 187, del Reglamento General a la Ley Orgánica de Educación Intercultural, certifica que el/la estudiante:', style: 'text', alignment: 'left', margin: [0, 20, 0, 20] },
        { text: parametrosCertificado.matricula.estudiante.estNombres, style: 'amie', alignment: 'center', margin: [0, 30, 0, 0] },
        { text: 'Documento de Identidad Nro: '+parametrosCertificado.matricula.estudiante.estIdentificacion, style: 'bold', alignment: 'center', margin: [0, 0, 0, 20] },
        { text: "DE: "+parametrosCertificado.gradoActual, style: 'normal' , alignment: 'left', margin: [0, 20, 0, 20]},
        { text: 'ES PROMOVIDO(A) A:', style: 'text', alignment: 'center', margin: [0, 20, 0, 20] },
        { text: parametrosCertificado.gradoSiguiente, style: 'bold', alignment: 'center', margin: [0, 0, 0, 0] },
        { text: 'PROMEDIO OBTENIDO: '+ parametrosCertificado.promedio, style: 'text', alignment: 'center', margin: [0, 0, 0, 20] },//+' ('+ parametrosCertificado.promedioLetras +')', style: 'text', alignment: 'center', margin: [0, 0, 0, 20] },
        { text: 'Para constancia de lo actuado, suscriben la presente Certificación: el/la Máxima Autoridad de la Institución Educativa.', style: 'text', alignment: 'left', margin: [0, 20, 0, 20] },
        { text: 'Dado y firmado en: '+parametrosCertificado.provincia+', '+parametrosCertificado.fecha, style: 'text', alignment: 'left', margin: [0, 10, 0, 70] },
        { text: '..................................................', style: 'text', alignment: 'center', margin: [0, 0, 0,0] },
        { text: 'Máxima Autoridad', style: 'text', alignment: 'center', margin: [0, 0, 0, 20] },
        {
          absolutePosition: { x: 0, y: 792 - 100 }, 
          image: await this.generateQRCode("Nombre:"+parametrosCertificado.matricula.estudiante.estNombres+",CI:"+parametrosCertificado.matricula.estudiante.estIdentificacion+",Matricula:"+parametrosCertificado.matricula.matCodigo), 
          width: 100, 
          alignment: 'right', 
          margin: [0, 0, 0, 0] 
        },
       
      ],
      styles: {
        title: {
          fontSize: 24,
          bold: true
        },
        bold: {
          bold: true
        },
        normal: {
          bold: false
        },
        subtitle: {
          fontSize: 10,
          margin: [0, 5, 0, 5]
        },
        amie: {
          fontSize: 14,
          bold: true,
          margin: [0, 0, 0, 0]
        },
        text: {
          fontSize: 11,
          bold: false,
          margin: [0, 0, 0, 0]
        }
      }
    };

    pdfMake.createPdf(documentDefinition).open();
  }

  getBase64ImageFromURL(url) {
    return new Promise((resolve, reject) => {
      var img = new Image();
      img.setAttribute("crossOrigin", "anonymous");
      img.onload = () => {
        var canvas = document.createElement("canvas");
        canvas.width = img.width;
        canvas.height = img.height;
        var ctx = canvas.getContext("2d");
        ctx.drawImage(img, 0, 0);
        var dataURL = canvas.toDataURL("image/png");
        resolve(dataURL);
      };
      img.onerror = error => {
        reject(error);
      };
      img.src = url;
    });
  }

  // Función para generar el código QR como una imagen en base64
 generateQRCode(data: string) {
  return new Promise((resolve, reject) => {
    try {
      const qrCodeDataURL = QRCode.toDataURL(data);
      resolve(qrCodeDataURL); 
    } catch (error) {
      reject(error);
    }
  });
}
  
}