import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { environment } from 'environments/environment';
import { ResponseGenerico } from '../interfaces/response-generico';
import { tutorOrdinariaInterface } from "../interfaces/tutor";
import { PromedioAnualInterface } from '../interfaces/promedio-anual';
import { notaAsignaturaInterface } from "../interfaces/nota-Asignatura";
import { calificacionMogenInterface } from "../interfaces/calificacionMogen";
import { notaDestrezaInterface } from "../interfaces/nota-Destreza";
import { mallaPrepaInterface } from "../interfaces/mallaPrepa";
import { asignaturaCompuestaInterface } from "../interfaces/asignaturaCompuesta";
import { ambitoInterface } from "../interfaces/ambito";
import { destrezaInterface } from "../interfaces/destreza";
import { ponderacionComportamientoInterface } from '../interfaces/ponderacionComportamiento';

@Injectable({
  providedIn: 'root'
})
export class AcademicoService {

  private readonly URL_REST = environment.url_academico;

constructor(private _http: HttpClient) { }


buscarTutorPorCodigoDocente(cod): Promise<ResponseGenerico<tutorOrdinariaInterface>>{
  return new Promise((resolve, reject) => {
    this._http.get(`${environment.url_academico}/private/buscarTutorOrdinariaPorDocente/`+cod).subscribe((response: ResponseGenerico<tutorOrdinariaInterface>) => {
      resolve(response);
    }, reject);
  })
}

buscarPromedioPorCodigoMatricula(codigoMatricula:number): Observable<any> | undefined {
  let url_ws=`${this.URL_REST}/private/buscarResProAnualPorMatCodigo/${codigoMatricula}`;
  return this._http.get<ResponseGenerico<PromedioAnualInterface>>(url_ws);
}


buscarNotaAsignaturaPorMatricula(codigoMatricula:number): Observable<any> | undefined {
  let url_ws=`${this.URL_REST}/private/buscarNotaAsignaturaPorMatricula/${codigoMatricula}`;
  return this._http.get<ResponseGenerico<notaAsignaturaInterface>>(url_ws);
}


buscarCalificacionPorMogenCodigo(mogenCodigo:number): Observable<any> | undefined {
  let url_ws=`${this.URL_REST}/private/listarCalificacionPorEstadoAndModgenCodigo/${mogenCodigo}`;
  return this._http.get<ResponseGenerico<calificacionMogenInterface>>(url_ws);
}


buscarNotaDestrezaPorMatricula(matCodigo: number): Observable<any> | undefined {
  let url_ws=`${this.URL_REST}/private/buscarNotasDestrezasPorMatricula/${matCodigo}`;
  return this._http.get<ResponseGenerico<notaDestrezaInterface>>(url_ws);
}


buscarMallaPrepaPorCodigo(maasCodigo: number): Observable<any> | undefined {
  let url_ws=`${this.URL_REST}/private/buscarMallaAsignaturaPorCodigo/${maasCodigo}`;
  return this._http.get<ResponseGenerico<mallaPrepaInterface>>(url_ws);
}

buscarAsignaturaPorCodigo(asiCodigo: number): Observable<any> | undefined {
  let url_ws=`${this.URL_REST}/private/buscarAsignaturaPorCodigo/${asiCodigo}`;
  return this._http.get<ResponseGenerico<asignaturaCompuestaInterface>>(url_ws);
}

buscarDestrezasPorCodigo(ambCodigo: number): Observable<any> | undefined {
  let url_ws=`${this.URL_REST}/private/listarDestrezasPorAmbCodigo/${ambCodigo}`;
  return this._http.get<ResponseGenerico<destrezaInterface>>(url_ws);
}


buscarAmbitoPorCodigo(ambCodigo: number): Observable<any> | undefined {
  let url_ws=`${this.URL_REST}/private/ListarPorCodigo/${ambCodigo}`;
  return this._http.get<ResponseGenerico<ambitoInterface>>(url_ws);
}

buscarPonderacionPorNota(pocoMin: string):  Observable<any> | undefined {
  let url_ws=`${this.URL_REST}/private/buscarComportamientoPonderacionPorNota/${pocoMin}`;
  return this._http.get<ResponseGenerico<ponderacionComportamientoInterface>>(url_ws);
}

}