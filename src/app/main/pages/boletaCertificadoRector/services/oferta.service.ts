import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

import { HttpClient } from '@angular/common/http';
import { environment } from 'environments/environment';
import { ResponseGenerico } from '../interfaces/response-generico';
import { CursoParalelo } from 'app/main/pages/asistencia-estudiantes/interfaces/curso-paralelo';
import { CursoParaleloBoleta } from '../interfaces/curso-paralelo-boleta';
import {cursoInterface} from '../interfaces/curso';
import { CursoExtraParametroInterface } from '../interfaces/curso-extra-parametro';
import { curParaleloInterface } from '../interfaces/paraleloCurso';
import { paraleloInterface } from "../interfaces/paralelo";


@Injectable({
  providedIn: 'root'
})
export class OfertaService {

  private readonly URL_REST = environment.url_oferta;

  constructor(public _http:HttpClient) { }

  listarParalelosCertificadoPromocion(parametros:CursoParaleloBoleta): Observable<any> | undefined {
    let url_ws=`${this.URL_REST}/private/listarParalelosCertificadoPromocion`;
  return this._http.post<ResponseGenerico<CursoParalelo>>(url_ws,parametros);
  }


  buscarCursoPorCodigo(parametro): Observable<any> | undefined {
    let url_ws=`${this.URL_REST}/private/buscarCursoPorCodigo/${parametro}`;
  return this._http.get<ResponseGenerico<cursoInterface>>(url_ws);
  }

  buscarCursoPorCodRegAnioLectivoGradoEstablecimiento(parametro:CursoExtraParametroInterface): Observable<any> | undefined {
    let url_ws=`${this.URL_REST}/private/buscarCursoPorCodRegAnioLectivoGradoEstablecimiento`;
    return this._http.post<ResponseGenerico<CursoParalelo>>(url_ws,parametro);
  }

  buscarCursoParaleloPorCodigo(parametro): Observable<any> | undefined {
    let url_ws=`${this.URL_REST}/private/buscarCursoParaleloPorCodigo/${parametro}`;
  return this._http.get<ResponseGenerico<curParaleloInterface>>(url_ws);
  }

  buscarParaleloPorCodigo(parametro): Observable<any> | undefined {
    let url_ws=`${this.URL_REST}/private/buscarParaleloPorCodigo/${parametro}`;
  return this._http.get<ResponseGenerico<paraleloInterface>>(url_ws);
  }
 
}