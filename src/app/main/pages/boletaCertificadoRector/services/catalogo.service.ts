import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { environment } from 'environments/environment';
import { CatGradosCursos } from '../../asignacionDocentes/interface/cat_gradosCursos';
import { ResponseGenerico } from '../interfaces/response-generico';
import { CatNivel } from '../interfaces/cat-nivel';
import { CatJoranada } from '../../asignacionDocentes/interface/cat_jornada';
import { CatModalidad } from '../../asignacionDocentes/interface/cat_modalidad';
import { regimenInterface } from "../interfaces/regimen";
import {gradoInterface} from "../interfaces/grado"
import { nivelInterface } from "../interfaces/niveles";
import { CatEspecialidad } from '../../asignacionDocentes/interface/cat_especialidad';
import { EspecialidadParametroInterface } from '../interfaces/especialidad-parametro';
import { Parroquia } from '../interfaces/parroquia';
import { DistritoParroquia } from '../interfaces/distrito-parroquia';


@Injectable({
  providedIn: 'root'
})
export class CatalogoService {

  private readonly URL_REST = environment.url_catalogo;

  constructor(public _http:HttpClient) { }

  listarGradosPorNivel(nivCodigo: number): Observable<any> | undefined {
    let url_ws=`${this.URL_REST}/private/buscarCatalogoGradosPorNivelCodigo/${nivCodigo}`;
    return this._http.get<ResponseGenerico<CatGradosCursos>>(url_ws);
  }


  buscarNivelesPorCodigo(cod): Observable<any> | undefined {
    let url_ws=`${this.URL_REST}/private/buscarNivelPorCodigo/${cod}`;
    return this._http.get<ResponseGenerico<nivelInterface>>(url_ws);
  }

  listarRegimenAnioLectivoPorCodigoRegimen(regCodigo: number[]): Observable<any> | undefined {
    let url_ws=`${this.URL_REST}/private/listarRegimenAnioLectivoPorRegimen/${regCodigo}`;
    return this._http.get<ResponseGenerico<CatGradosCursos>>(url_ws);
  }

  listarNiveles(): Observable<any> | undefined {
    let url_ws=`${this.URL_REST}/private/listarNiveles`;
    return this._http.get<ResponseGenerico<CatNivel>>(url_ws);
  }

  listarJornadas(): Observable<any> | undefined {
    let url_ws=`${this.URL_REST}/private/listarJornadas`;
    return this._http.get<ResponseGenerico<CatJoranada>>(url_ws);
  }

  listarTodosGrados(): Observable<any> | undefined {
    let url_ws=`${this.URL_REST}/private/listarTodosLosGrados`;
    return this._http.get<ResponseGenerico<CatGradosCursos>>(url_ws);
  }

  listarModalidad(): Observable<any> | undefined {
    let url_ws=`${this.URL_REST}/private/listarModalidades`;
    return this._http.get<ResponseGenerico<CatModalidad>>(url_ws);
  }

  buscarJornadasPorCodigo(cod): Observable<any> | undefined {
    let url_ws=`${this.URL_REST}/private/buscarJornadaPorCodigo/${cod}`;
    return this._http.get<ResponseGenerico<CatJoranada>>(url_ws);
  }


  listarRegimenPorAnioElectivo(cod): Observable<any> | undefined {
    let url_ws=`${this.URL_REST}/private/buscarRegimenAnioLectivoPorCodigo/${cod}`;
    return this._http.get<ResponseGenerico<regimenInterface>>(url_ws);
  }

  buscarGradoPorCodigo(cod): Observable<any> | undefined {
    let url_ws=`${this.URL_REST}/private/buscarCatalogoGradosPorCodigo/${cod}`;
    return this._http.get<ResponseGenerico<gradoInterface>>(url_ws);
  }

  listarEspecialidades(listaEspCodigo:EspecialidadParametroInterface): Observable<any> | undefined {
    let url_ws=`${this.URL_REST}/private/buscarEspecialidadesPorListaEspCodigoYEstado`;
  return this._http.post<ResponseGenerico<CatEspecialidad>>(url_ws,listaEspCodigo);
  }

  buscarDistritoParroquiaPorCodigo(codigoDistritoParroquia:number): Observable<any> | undefined {
    let url_ws=`${this.URL_REST}/private/buscarDistritoParroquiaPorCodigo/${codigoDistritoParroquia}`;
    return this._http.get<ResponseGenerico<DistritoParroquia>>(url_ws);
  }

}