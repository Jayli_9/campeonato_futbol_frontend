import { LOCALE_ID, NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgbActiveModal, NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { CoreCommonModule } from '@core/common.module';
import { ContentHeaderModule } from 'app/layout/components/content-header/content-header.module';
import { NgxDatatableModule } from '@swimlane/ngx-datatable';
import { CardSnippetModule } from '@core/components/card-snippet/card-snippet.module';
import { NgSelectModule } from '@ng-select/ng-select';
import { MatTabsModule } from '@angular/material/tabs';
import { MatIconModule } from '@angular/material/icon';
import { CoreTouchspinModule } from '@core/components/core-touchspin/core-touchspin.module';
import { Ng2FlatpickrModule } from 'ng2-flatpickr';
import localeEs from '@angular/common/locales/es';
import { registerLocaleData } from '@angular/common';
import { MatTableModule } from '@angular/material/table';
import { MatPaginatorModule } from '@angular/material/paginator';
import { SweetAlert2Module } from '@sweetalert2/ngx-sweetalert2';
import { MatDialogModule } from '@angular/material/dialog';
import { MatSnackBarModule } from '@angular/material/snack-bar';
import { MatButtonModule } from '@angular/material/button';
import { RUTA_REPORTE_INDIVIDUAL } from '../routes/reporte-individual-routing.module';
import { RUTA_REPORTE_NIVEL } from "../routes/reporte-nivel-routing.module";
import { ReporteIndividualComponent } from '../componentes/reporte-individual/reporte-individual.component';
import { ReporteNivelComponent } from "../componentes/reporte-nivel/reporte-nivel.component";
registerLocaleData(localeEs, 'es');

@NgModule({
    declarations: [
        ReporteIndividualComponent,
        ReporteNivelComponent
    ],
    imports: [
        CommonModule,
        FormsModule,
        ReactiveFormsModule,
        RouterModule.forChild(RUTA_REPORTE_INDIVIDUAL),
        RouterModule.forChild(RUTA_REPORTE_NIVEL),
        NgbModule,
        CoreCommonModule,
        ContentHeaderModule,
        NgxDatatableModule,
        CardSnippetModule,
        NgSelectModule,
        MatTabsModule,
        MatIconModule,
        CoreTouchspinModule,
        Ng2FlatpickrModule,
        MatTableModule,
        MatPaginatorModule,
        SweetAlert2Module.forRoot(),
        MatDialogModule,
        MatSnackBarModule,
        MatButtonModule
    ],
    bootstrap: [ReporteIndividualComponent, ReporteNivelComponent],
    exports: [
        ReporteIndividualComponent,
        ReporteNivelComponent
    ],
    providers: [{ provide: LOCALE_ID, useValue: 'es' }]
})
export class BoletaCertificadoModule { }