import { Component, OnInit } from '@angular/core';
import { User } from 'app/auth/models';
import { AcademicoService } from '../../services/academico.service';
import { objetoAlmacenadoInterface } from "../../interfaces/objetoAlmacenados";
import Swal from 'sweetalert2/dist/sweetalert2.js'
import { GieeService } from "../../services/giee.service";
import { CatalogoService } from "../../services/catalogo.service";


@Component({
  selector: 'app-principal',
  templateUrl: './principal.component.html',
  styleUrls: ['./principal.component.scss']
})
export class PrincipalComponent implements OnInit {
  contentHeader: object;

  //datos institucion
  currentUser: User;
  datosInstitucion = [];
  nombreInstitucion;
  nemonico;
  amie;


  //sacar datos de establecimiento y anio electivo
  insestCodigo;
  reanleCodigo;
  insCodigo;

  acumuladorServicios= [];
  acumuladorReanleCodigo= [];





  //variables de listado que vamos a usar
  ListarTodasHabilidades;

  listaHabilidadCognitiva=[];

  listaHabilidadSociales=[];

  listaHabilidadEmocionales=[];


  datosAlmacenamientoCognitiva: objetoAlmacenadoInterface[]=[];
  datosAlmacenamientoSocial: objetoAlmacenadoInterface[]=[];
  datosAlmacenamientoEmocional: objetoAlmacenadoInterface[]=[];


  datosServicioLimpio = [];
  datosRealmLimpio = [];



  constructor(
    private readonly adecademicoService: AcademicoService,
    private readonly gieeService: GieeService,
    private readonly catalogoService: CatalogoService
  ) { 
    this.currentUser = JSON.parse(localStorage.getItem("currentUser"))
  }


  ngOnInit(){

    this.datosInstitucion.push(this.currentUser.sede);

    this.amie = this.datosInstitucion[0].nemonico;

    setTimeout(() => {
      this.sacarEstablecimiento(this.amie);
    }, 1500);

    

    this.nombreInstitucion = this.datosInstitucion[0].descripcion;
    this.nemonico = this.datosInstitucion[0].nemonico;
    
    this.contentHeader = {
      headerTitle: 'Habilidades & Criterios',
      actionButton: false,
      breadcrumb: {
          type: '',
          links: [
              {
                name: 'Inicio',
                isLink: true,
                link: '/'
              },
              {
                name: 'acomIntegralAula',
                isLink: false
              }
          ]
      }
    };

    this.adecademicoService.listarHabilidades().then(data => {
      this.ListarTodasHabilidades = data.listado;

      this.ListarTodasHabilidades.forEach(element => {
        if(element.cthCodigo == 1){
          
          if(element.habCodigo == 1 || element.habCodigo == 2 || element.habCodigo == 4 ){
            let datos = {
              codigo: element.habCodigo,
              descripcionHabilidades: element.habDescipcion,
              seleccion: true,
              datoBloque: 1
            }

            this.listaHabilidadCognitiva.push(datos);

          }else{

            let datos = {
              codigo: element.habCodigo,
              descripcionHabilidades: element.habDescipcion,
              seleccion: false,
              datoBloque: 0
            }

            this.listaHabilidadCognitiva.push(datos);

          }

        }else if(element.cthCodigo == 2){

          if(element.habCodigo == 11){

            let datos = {
              codigo: element.habCodigo,
              descripcionHabilidades: element.habDescipcion,
              seleccion: true,
              datoBloque: 1
            }

            this.listaHabilidadSociales.push(datos);

          }else{

            let datos = {
              codigo: element.habCodigo,
              descripcionHabilidades: element.habDescipcion,
              seleccion: false,
              datoBloque: 0
            }

            this.listaHabilidadSociales.push(datos);

          }
          

        }else if(element.cthCodigo == 3){

          let datos = {
            codigo: element.habCodigo,
            descripcionHabilidades: element.habDescipcion,
            seleccion: false,
            datoBloque: 0
          }

          this.listaHabilidadEmocionales.push(datos);

        }
      });
    })
  }


  sacarEstablecimiento(cod){

    this.gieeService.buscarEstablecimientoPorAmie(cod).then(data => {
      let listado = data.listado;

      listado.forEach(element => {
        this.insestCodigo = element.insestCodigo;
        let agrupacionInstitucion = element.insInstitucion;
        this.insCodigo = agrupacionInstitucion.insCodigo;
        this.nivelesPermisoPorinsCodigo(this.insCodigo);
      });
    })
    

  }


  nivelesPermisoPorinsCodigo(cod){
    this.gieeService.getPermisoNivelInstitucionPorInsCodigo(cod).then(data => {
      let listado = data.listado;

      listado.forEach(element => {
        this.buscarServicioEducativoPorCodigo(element.sereduCodigo);
      });

    })
  }

  buscarServicioEducativoPorCodigo(cod){
    this.acumuladorServicios.push(cod);

    this.catalogoService.buscarServicioEducativo(cod).then(data => {
      let listaServicioEducativo = data.objeto;

      this.acumuladorReanleCodigo.push(listaServicioEducativo.reanleCodigo);

    })
  }





  SeleccionCognitiva(objeto){

    // Si el objeto se selecciona, deselecciona los demás
    if (objeto.seleccion) {
      this.listaHabilidadCognitiva.forEach((o) => {
        if (o.codigo !== objeto.codigo && o.datoBloque != 1) {
          o.seleccion = false;
        }
      });

      // Agrega el objeto a la lista de objetos seleccionados
      this.datosAlmacenamientoCognitiva = [objeto];
    } else {
      // Si el objeto se deselecciona, quítalo de la lista de objetos seleccionados
      this.datosAlmacenamientoCognitiva = this.datosAlmacenamientoCognitiva.filter(
        (o) => o.codigo !== objeto.codigo
      );
    }

  }


  SeleccionSocial(objeto: objetoAlmacenadoInterface){
    // Si el objeto se selecciona, verifica si ya hay 3 seleccionados

    if (objeto.seleccion) {
      if (this.datosAlmacenamientoSocial.length < 3) {
        this.datosAlmacenamientoSocial.push(objeto);
      } else {

        setTimeout(() => {

          Swal.fire({
            icon: "error",
            title: "Alerta",
            text: "Solo puedes escoger 3 habilidades sociales"
          });

          objeto.seleccion = false

        }, 500);

      }
    } else {
      // Si el objeto se deselecciona, quítalo de la lista de objetos seleccionados
      this.datosAlmacenamientoSocial = this.datosAlmacenamientoSocial.filter(
        (o) => o.codigo !== objeto.codigo
      );
    }

   }


   seleccionEmocional(objeto){

    // Si el objeto se selecciona, deselecciona los demás
    if (objeto.seleccion) {
      this.listaHabilidadEmocionales.forEach((o) => {
        if (o.codigo !== objeto.codigo && o.datoBloque != 1) {
          o.seleccion = false;
        }
      });

      // Agrega el objeto a la lista de objetos seleccionados
      this.datosAlmacenamientoEmocional = [objeto];
    } else {
      // Si el objeto se deselecciona, quítalo de la lista de objetos seleccionados
      this.datosAlmacenamientoEmocional = this.datosAlmacenamientoEmocional.filter(
        (o) => o.codigo !== objeto.codigo
      );
    }

   }


  guardarHabilidades(){

    Swal.fire({
      title: 'Confirmar',
      text: '¿Desea Guardar la información?',
      icon: 'question',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Guardar',
      cancelButtonText: 'Cancelar'
    }).then((result) => {
      if (result.isConfirmed) {

        let datos = {
          codigo: 1,
          descripcionHabilidades: "Dato Por defecto",
          seleccion: true,
          datoBloque: 1
        }

        let datos2 = {
          codigo: 2,
          descripcionHabilidades: "Dato Por defecto",
          seleccion: true,
          datoBloque: 1
        }

        let datos3 = {
          codigo: 4,
          descripcionHabilidades: "Dato Por defecto",
          seleccion: true,
          datoBloque: 1
        }

        let datos4 = {
          codigo: 11,
          descripcionHabilidades: "Dato Por defecto",
          seleccion: true,
          datoBloque: 1
        }

        this.datosAlmacenamientoCognitiva.push(datos);
        this.datosAlmacenamientoCognitiva.push(datos2);
        this.datosAlmacenamientoCognitiva.push(datos3);
        this.datosAlmacenamientoSocial.push(datos4);


        setTimeout(() => {
          this.validadorGuardado(this.insestCodigo);
        }, 1500);

      }
    });


  }


  async validadorGuardado(insestCodigo){
    try{
      const data = await this.adecademicoService.buscarHabilidadesInstPorInstitucion(insestCodigo);
    const datos = data.listado

    if(datos.length === 9){
      Swal.fire({
        icon: "error",
        title: "Atención",
        text: "La institución ya tiene habilidades registradas"
      });
      setTimeout(() => {
        location.reload();
      }, 2000);
    }else{
      await this.guardarInformacionCognitiva();
    }
    } catch {

    }
    
  }







  guardarInformacionCognitiva(){

     this.datosServicioLimpio.push(limpiarObjeto(this.acumuladorServicios));

     this.datosRealmLimpio.push(limpiarObjeto(this.acumuladorReanleCodigo));


      for(let i = 0; i < this.datosAlmacenamientoCognitiva.length; i++){
        for(let j = 0; j < this.datosServicioLimpio.length; j++){

          

          let datos = {
            "habCodigo": this.datosAlmacenamientoCognitiva[i].codigo,
            "hinEstado": 1,
            "insestCodigo": this.insestCodigo,
            "reanleCodigo": this.acumuladorReanleCodigo[j],
            "sereduCodigo": this.acumuladorServicios[j]
          }


          this.adecademicoService.guardarHabilidades(datos).subscribe({
            next: (response) => {

              let validor = i+1;

              
              if(validor === this.datosAlmacenamientoCognitiva.length){
                this.guardarInformacionSocial();
              }else{

              }
            },
            error: (error) => {
              Swal.fire({
                icon: "error",
                title: "Advertencia",
                text: "Los datos no se pudieron guardar"
              });
            }

          })

        }
  
      }
  }


  guardarInformacionSocial(){
    for(let i = 0; i < this.datosAlmacenamientoSocial.length; i++){
      for(let j = 0; j < this.datosServicioLimpio.length; j++){
        let datos = {
          "habCodigo": this.datosAlmacenamientoSocial[i].codigo,
          "hinEstado": 1,
          "insestCodigo": this.insestCodigo,
          "reanleCodigo": this.acumuladorReanleCodigo[j],
          "sereduCodigo": this.acumuladorServicios[j]
        }


        this.adecademicoService.guardarHabilidades(datos).subscribe({
          next: (response) => {

            let validor = i+1;

            
            if(validor === this.datosAlmacenamientoSocial.length){
              this.guardarInformacionEmocional();
            }else{

            }
          },
          error: (error) => {
            Swal.fire({
              icon: "error",
              title: "Advertencia",
              text: "Los datos no se pudieron guardar"
            });
          }

        })
      }
    }
  }


  guardarInformacionEmocional(){
    for(let i = 0; i < this.datosAlmacenamientoEmocional.length; i++){
      for(let j = 0; j < this.datosServicioLimpio.length; j++){

        let datos = {
          "habCodigo": this.datosAlmacenamientoEmocional[i].codigo,
          "hinEstado": 1,
          "insestCodigo": this.insestCodigo,
          "reanleCodigo": this.acumuladorReanleCodigo[j],
          "sereduCodigo": this.acumuladorServicios[j]
        }

        this.adecademicoService.guardarHabilidades(datos).subscribe({
          next: (response) => {

            let validor = i+1;

            
            if(validor === this.datosAlmacenamientoEmocional.length){

              setTimeout(() => {
                Swal.fire({
                  title: "Datos Guardados",
                  text: "Todos los datos han sido guardados correctamente",
                  icon: "success"
                });
              }, 1000);


              setTimeout(() => {
                location.reload();
              }, 1500);
              
            }else{

            }
          },
          error: (error) => {
            Swal.fire({
              icon: "error",
              title: "Advertencia",
              text: "Los datos no se pudieron guardar"
            });
          }
        })
      }
    }
  }


  


}


function limpiarObjeto(objeto) {
  // Creamos un conjunto para almacenar claves únicas
  let clavesUnicas = new Set();

  // Filtramos las claves del objeto y eliminamos duplicados
  let clavesFiltradas = Object.keys(objeto).filter(function (clave) {
    if (!clavesUnicas.has(clave)) {
      clavesUnicas.add(clave);
      return true;
    }
    return false;
  });

  // Creamos un nuevo objeto con las claves filtradas
  let objetoFiltrado = {};
  clavesFiltradas.forEach(function (clave) {
    objetoFiltrado[clave] = objeto[clave];
  });

  return objetoFiltrado;
}