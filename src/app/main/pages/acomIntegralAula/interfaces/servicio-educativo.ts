export interface CatServicioEducativo{
    sereduCodigo: number;
    reanleCodigo: number;
    sereduDescripcion: String;
    sereduEstado: number;
    sereduFechaFin: Date;
    sereduFechaInicio: Date;
    sereduIntensivo: number;
    motiedCodigo:number;
    sereduVigente: String;
    sereduClasificacion: String;
    sereduNemonico: String;
    codigoRegimen: number;
    codigoAnioLectivo: number;
    codigoModalidadTipoEducacion: number;
    codigoModalidad: number;
    codigoTipoEducacion: number;
    codigoPrograma: number;
    modCodigo: number
}