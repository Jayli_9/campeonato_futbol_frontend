export interface habilidadesInterface{

    habCodigo: number;

    cthCodigo: number;

    habDescipcion: String;

    habPredeterminado: number;
    
    habEstado: number;

}