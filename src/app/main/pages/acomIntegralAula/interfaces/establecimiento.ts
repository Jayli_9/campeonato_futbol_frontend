export interface extableciminetoInterface{
       insestCodigo: number;
       insInstitucion: {
         insCodigo: number;
         insDescripcion:  String;
         insEstado: number;
         insAmie: String;
         insLogo: null;
         insEslogan: null;
         insNumeroPermiso: String;
         insNumResolucion: String;
         insRutaArchivo: String;
         sosCodigo: number;
         denCodigo: number;
         jurCodigo: number;
         insFechaCreacion: number;
         insDireccion: null;
         tipinsCodigo: number;
         insFechaResolucion: null;
         regimenes: null;
         inDescripcion: String;
      },
       insestEstado: number;
       insestDireccion: String;
       insestCoordenadaX: String;
       insestCoordenadaY: String;
       parCodigo: number;
       disCodigo: number;
       insestGeocodigo: String;
       insTipoEstablecimiento: {
         tipEstCodigo: number;
         tipEstDescripcion: String;
         tipEstEstado: number;
         tipEstFechaCreacion: Date;
      },
       insestFechaCreacion: Date;
       nombreTipo: String;
}