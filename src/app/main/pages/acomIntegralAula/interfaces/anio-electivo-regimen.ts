export interface anioElectivoRegimenInterface{
     reanleCodigo: number;
     reanleEstado: number;
     reanleFechaCreacion: Date;
     reanleTipo: String;
     regCodigo: number;
     anilecCodigo: number;
     descripcionAnioLectivo: String;
}