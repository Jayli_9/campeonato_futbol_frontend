import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { environment } from 'environments/environment';
import { ResponseGenerico } from '../interfaces/response-generico';
import { extableciminetoInterface } from "../interfaces/establecimiento";
import { NivelPermisoIntitucionInterface } from "../interfaces/nivelPermisoInstitucion";

@Injectable({
  providedIn: 'root'
})
export class GieeService {

  private readonly URL_REST = environment.url_institucion;
  
constructor(private _http: HttpClient) { }

buscarEstablecimientoPorAmie(cod): Promise<ResponseGenerico<extableciminetoInterface>>{
  return new Promise((resolve, reject) => {
    this._http.get(`${environment.url_institucion}/private/listarEstablecimientosPorAmie/`+cod).subscribe((response: ResponseGenerico<extableciminetoInterface>) => {
      resolve(response);
    }, reject);
  })
}

//permiso de nivel institucion
getPermisoNivelInstitucionPorInsCodigo(cod): Promise<ResponseGenerico<NivelPermisoIntitucionInterface>>{
  return new Promise((resolve, reject) => {
this._http.get(`${environment.url_institucion}/private/buscarNivelPermisoPorCodigoInstitucion/`+ cod).subscribe((response: ResponseGenerico<NivelPermisoIntitucionInterface>) => {
   resolve(response);
 }, reject);
})
}

}
