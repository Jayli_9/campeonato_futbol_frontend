import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { environment } from 'environments/environment';
import { ResponseGenerico } from '../interfaces/response-generico';
import { habilidadesInterface } from "../interfaces/habilidades";
import { habInstInterface } from "../interfaces/habInst"; 

@Injectable({
  providedIn: 'root'
})
export class AcademicoService {

  private readonly URL_REST = environment.url_academico;

  constructor(private _http: HttpClient) {}


  buscarHabilidadesPorCatHabilidades(cod): Promise<ResponseGenerico<habilidadesInterface>>{
    return new Promise((resolve, reject) => {
      this._http.get(`${environment.url_academico}/private/buscarHabilidadesPorCodigoCatHabilidad/`+cod).subscribe((response: ResponseGenerico<habilidadesInterface>) => {
        resolve(response);
      }, reject);
    })
  }


  listarHabilidades(): Promise<ResponseGenerico<habilidadesInterface>>{
    return new Promise((resolve, reject) => {
      this._http.get(`${environment.url_academico}/private/listarHabilidades`).subscribe((response: ResponseGenerico<habilidadesInterface>) => {
        resolve(response);
      }, reject);
    })
  }

  guardarHabilidades(parametro: any){
    return this._http.post<habInstInterface>(`${environment.url_academico}/private/registroHabInst`, parametro);
  }


  buscarHabilidadesInstPorInstitucion(cod): Promise<ResponseGenerico<habInstInterface>>{
    return new Promise((resolve, reject) => {
      this._http.get(`${environment.url_academico}/private/buscarHabInstPorInsestCodgio/`+cod).subscribe((response: ResponseGenerico<habInstInterface>) => {
        resolve(response);
      }, reject);
    })
  }
  
  

}
