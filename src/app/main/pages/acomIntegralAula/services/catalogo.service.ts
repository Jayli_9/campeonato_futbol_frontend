import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { environment } from 'environments/environment';
import { ResponseGenerico } from '../interfaces/response-generico';
import { CatServicioEducativo } from "../interfaces/servicio-educativo";

@Injectable({
  providedIn: 'root'
})
export class CatalogoService {

  private readonly URL_REST = environment.url_catalogo;

  constructor(private _http: HttpClient) {}

  buscarServicioEducativo(cod): Promise<ResponseGenerico<CatServicioEducativo>>{
    return new Promise((resolve, reject) => {
      this._http.get(`${environment.url_catalogo}/private/buscarServicioEducativoPorCodigo/`+cod).subscribe((response: ResponseGenerico<CatServicioEducativo>) => {
        resolve(response);
      }, reject);
    })
  }

}
