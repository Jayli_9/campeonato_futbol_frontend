import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { NgSelectModule } from '@ng-select/ng-select';
import { CoreCommonModule } from '@core/common.module';
import { ContentHeaderModule } from 'app/layout/components/content-header/content-header.module';
import { Ng2FlatpickrModule } from 'ng2-flatpickr';
import { PrincipalModule } from './principal/principal.module';
import { AuthenticationModule } from './authentication/authentication.module';
import { MiscellaneousModule } from './miscellaneous/miscellaneous.module';
import { AreaConocimientoModule } from './area-conocimiento/module/area-conocimiento.module';
import { MaterialModule } from '../shared/material/material.module';
import { AgrupacionModule } from './agrupacion/module/agrupacion.module';
import { TipoValoracionModule } from './tipo-valoracion/module/tipo-valoracion.module';
import { TipoAsignaturaModule } from './tipo-asignatura/module/tipo-asignatura.module';
import { AsignaturaModule } from './asignatura/module/asignatura.module';
import { MallaAsignaturaExtraModule } from './malla-asignatura-extra/module/malla-asignatura-extra.module';
import { ModeloEvaluacionModule } from './modelo-evaluacion/module/modelo-evaluacion.module';
import { ModeloCalificacionModule } from './modelo-calificacion/module/modelo-calificacion.module';
import { EvaluacionModule } from './evaluacion-estudiantes/module/evaluacion.module';
import { AsistenciaModule } from './asistencia-estudiantes/module/asistencia.module';
import { JustificarAsistenciaModule } from './justificar-asistencia/module/justificar-asistencia.module';
import { CalificacionNivelModule } from './calificacion-nivel/module/calificacion-nivel.module';
import { ModeloEvaluacionGeneralModule } from './modelo-evaluacion-general/module/modelo-evaluacion-general.module';
import { HorarioExtraModule } from './horario-extra/module/horario-extra.module';
import { asignacionDocenteModule } from './asignacionDocentes/modulos/asignacionDocente.module';
import { asignacionDocenteOrdinariaModule } from './asignacionDocentesOrdinaria/modulos/asignacionDocenteOrdinaria.module';
import { calificacionOrdinariaModule } from './calificacionOrdinaria/modulos/calificacionOrdinaria.module';
import { ReportePlanesEstudianteModule } from './reporteEstudiantes/module/reportePlanesEstudio.module';
import { acomIntegralAulaModule } from './acomIntegralAula/module/acomintegralAula.module';
import { administrativoHabilidadesModule } from './administrativoHabilidades/module/administrativoHabilidades.module';
import {
    ModalInsercionHabilidadesComponent
} from './administrativoHabilidades/components/modal-insercion-habilidades/modal-insercion-habilidades.component';
import { EditarHabilidadesComponent } from './administrativoHabilidades/components/editar-habilidades/editar-habilidades.component';
import {
    EditarCatHabilidadesComponent
} from './administrativoCatHabilidades/components/editar-cat-habilidades/editar-cat-habilidades.component';
import {
    ModalCatHabilidadesComponent
} from './administrativoCatHabilidades/components/modal-cat-habilidades/modal-cat-habilidades.component';
import { administrativoCatHabilidadesModule } from '../pages/administrativoCatHabilidades/module/administrativoCatHabilidades.module';
import { EdicionCriteriosComponent } from './administrativoCriterios/components/edicion-criterios/edicion-criterios.component';
import { ModalCriteriosComponent } from './administrativoCriterios/components/modal-criterios/modal-criterios.component';
import { administrativoCriteriosModule } from './administrativoCriterios/module/administrativoCriterios.module';
import { ModeloReporteDocentes } from './reporteDocente/module/reporte-docente-module';
import { Ng2CompleterModule } from 'ng2-completer';
import { asignacionPrepaModule } from './asignacionPrepa/modulos/asignacionPrepa.module';
import { ReporteAsignacionModule } from './reporteAsignatura/modulo/modelo-reporteAsignacion.module';
import { ReporteMallaModule } from './reporteMalla/module/reporteMalla.module';
import { asistenciaOrdinariaModule } from './asistenciaOrdinaria/modulos/asistenciaOrdinaria.module';
import { asignacionTutorModule } from './asignacionTutor/modulos/asignacionTutor.module';
import { ParticipacionEstudiantilModule } from './participacionEstudiantil/module/participacion-estudiantil.module';
import { EjeConstitutivoModule } from './eje/modules/eje-constitutivo.module';
import { EjeGradoModule } from './eje-grado/modules/eje-grado/eje-grado.module';
import { NotaOvpComponent } from './calificacionOrdinaria/componentes/calificacion-principal/nota-ovp/nota-ovp.component';
import { NgxSpinnerModule } from 'ngx-spinner';
import { ModaltutorComponent } from './asignacionTutor/componentes/asignacion-tutores/modaltutor/modaltutor.component';
import { DenominacionModule } from './denominacion/module/denominacion.module';
import { TipoJustificacionComponent } from "./asistenciaOrdinaria/componentes/asistencia-principal/justificacion/tipoJustificacion/tipoJustificacion.component";
import { SubDenominacionModule } from './sub-denominacion/module/sub-denominacion.module';
import { BoletaCertificadoModule } from './boletaCertificadoRector/module/boletaCertificadoRector.module';
import { CampeonatoModule } from './campCampeonato/modulos/campeonato.module';
import { MatSortModule } from '@angular/material/sort';
import { TablaPrincipalComponent } from './campCampeonato/componentes/tabla-principal/tabla-principal.component';


@NgModule({
    declarations: [
        ModalInsercionHabilidadesComponent,
        EditarHabilidadesComponent,
        EditarCatHabilidadesComponent,
        ModalCatHabilidadesComponent,
        EdicionCriteriosComponent,
        ModalCriteriosComponent,
        ModaltutorComponent,
        TipoJustificacionComponent,
        
    ],
    imports: [
        CommonModule,
        CoreCommonModule,
        ContentHeaderModule,
        NgbModule,
        NgSelectModule,
        FormsModule,
        AuthenticationModule,
        MiscellaneousModule,
        Ng2FlatpickrModule,
        MaterialModule,
        PrincipalModule,
        AreaConocimientoModule,
        AgrupacionModule,
        TipoValoracionModule,
        TipoAsignaturaModule,
        AsignaturaModule,
        MallaAsignaturaExtraModule,
        ModeloEvaluacionModule,
        ModeloEvaluacionGeneralModule,
        ModeloCalificacionModule,
        EvaluacionModule,
        AsistenciaModule,
        JustificarAsistenciaModule,
        CalificacionNivelModule,
        HorarioExtraModule,
        asignacionDocenteModule,
        asignacionDocenteOrdinariaModule,
        calificacionOrdinariaModule,
        ReportePlanesEstudianteModule,
        acomIntegralAulaModule,
        administrativoHabilidadesModule,
        administrativoCatHabilidadesModule,
        administrativoCriteriosModule,
        ModeloReporteDocentes,
        Ng2CompleterModule,
        asignacionPrepaModule,
        ReporteAsignacionModule,
        ReporteMallaModule,
        asistenciaOrdinariaModule,
        asignacionTutorModule,
        ParticipacionEstudiantilModule,
        EjeConstitutivoModule,
        EjeGradoModule,
        CampeonatoModule,
        NgxSpinnerModule,
        DenominacionModule,
        SubDenominacionModule,
        BoletaCertificadoModule,
        NgxSpinnerModule,
        MatSortModule

    ],
    exports: [

    ],

    providers: []
})
export class PagesModule {
}
