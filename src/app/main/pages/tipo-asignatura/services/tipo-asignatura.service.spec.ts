import { TestBed } from '@angular/core/testing';

import { TipoAsignaturaService } from './tipo-asignatura.service';

describe('TipoAsignaturaService', () => {
  let service: TipoAsignaturaService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(TipoAsignaturaService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
