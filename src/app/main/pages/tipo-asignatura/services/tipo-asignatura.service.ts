import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'environments/environment';
import { RespuestaTipoAsignaturaInterfaz } from '../interfaces/respuesta-tipo-asignatura-interfaz';
import { TipoAsignatura } from '../interfaces/tipo-asignatura';

@Injectable({
  providedIn: 'root'
})
export class TipoAsignaturaService {

  url_academico=environment.url_academico;
  constructor(
    public _http:HttpClient
  ) { }
  guardarActualizarTipoAsignatura(tipoAsignatura:TipoAsignatura){
    let url_ws=`${this.url_academico}/private/guardarTipoAsignatura`;
    return this._http.post(url_ws,tipoAsignatura);
  }

  listarTodosLosTiposDeAsignatura(){
    let url_ws=`${this.url_academico}/private/listarTodosLosTiposDeAsignatura`;
    return this._http.get<RespuestaTipoAsignaturaInterfaz>(url_ws);
  }

  listarTiposAsignaturaActivos(){
    let url_ws=`${this.url_academico}/private/listarTiposAsignaturaActivos`;
    return this._http.get<RespuestaTipoAsignaturaInterfaz>(url_ws);
  }

  buscarTipoAsignaturaPorCodigo(codigo:number){
    let url_ws=`${this.url_academico}/private/buscarTipoAsignaturaPorCodigo/${codigo}`;
    return this._http.get<RespuestaTipoAsignaturaInterfaz>(url_ws);
  }
  
  inactivarTipoAsignatura(codigo:number){
    let url_ws=`${this.url_academico}/private/eliminarTipoAsignaturaPorId/${codigo}`;
    return this._http.delete(url_ws);
  }
}
