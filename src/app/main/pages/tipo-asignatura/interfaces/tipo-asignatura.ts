export interface TipoAsignatura {
    tiasCodigo?: number;
    tiasDescripcion: string;
    tiasEstado: number;
    tiasObligatorio: string;
}
