import { Component, OnInit, ViewChild } from '@angular/core';
import { UntypedFormBuilder, UntypedFormGroup, Validators } from '@angular/forms';
import {MatPaginator} from '@angular/material/paginator';
import {MatSort} from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import Swal from 'sweetalert2';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { NgxSpinnerService } from 'ngx-spinner';
import { TipoAsignatura } from '../../interfaces/tipo-asignatura';
import { TipoAsignaturaService } from '../../services/tipo-asignatura.service';
import { RespuestaTipoAsignaturaInterfaz } from '../../interfaces/respuesta-tipo-asignatura-interfaz';

@Component({
  selector: 'app-tipo-asignatura',
  templateUrl: './tipo-asignatura.component.html',
  styleUrls: ['./tipo-asignatura.component.scss']
})
export class TipoAsignaturaComponent implements OnInit {
  public contentHeader: object;
  public frmTipoAsignaturaCreateEdit: UntypedFormGroup;
  public submitted = false;
  listaTipoAsignatura:TipoAsignatura[]=null;
  columnasTipoAsignatura=['num','descripcion','obligatorio','estado','acciones'];
  dataSource: MatTableDataSource<TipoAsignatura>;

  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;
  constructor(
    private readonly fb: UntypedFormBuilder,
    private modalService: NgbModal,
    public spinner: NgxSpinnerService,
    private _tipoAsignaturaService:TipoAsignaturaService
  ) { }

  ngOnInit(): void {
    this.contentHeader = {
      headerTitle: 'Tipo Asignatura',
      actionButton: false,
      breadcrumb: {
        type: '',
        links: [
          {
            name: 'Inicio',
            isLink: true,
            link: '/pages/inicio'
          },
          {
            name: 'Tipo Asignatura',
            isLink: false
          },
        ]
      }
    };
    this.listarTodosLosTipoAsignatura();
  }
  listarTodosLosTipoAsignatura(){
    this.spinner.show();
    this._tipoAsignaturaService.listarTodosLosTiposDeAsignatura().subscribe(
      (respuesta:RespuestaTipoAsignaturaInterfaz)=>{
        this.listaTipoAsignatura=respuesta.listado;
        this.dataSource = new MatTableDataSource(this.listaTipoAsignatura);
        this.dataSource.paginator = this.paginator;
        this.dataSource.paginator._intl.itemsPerPageLabel="Tipos Asignatura por página";
        this.dataSource.paginator._intl.nextPageLabel="Siguiente";
        this.dataSource.paginator._intl.previousPageLabel="Anterior";
        this.dataSource.sort = this.sort;
        this.spinner.hide();
      },
      (error:any)=>{
        this.spinner.hide();
       Swal.fire('Ups! ocurrió un error al cargar Tipo Asignatura','','error'); 
      }
    );
  }
  nuevoTipoAsignatura(modalForm){
    this.submitted=false;
    this.frmTipoAsignaturaCreateEdit=this.fb.group({
      descripcion: ['', [Validators.required]],
      obligatorio: [false]
    });
    this.modalService.open(modalForm);
  }
  editTipoAsignatura(modalForm,tipoAsignaturaEdit:TipoAsignatura){
    this.submitted=false;
    this.frmTipoAsignaturaCreateEdit=this.fb.group({
      codigo:[tipoAsignaturaEdit.tiasCodigo],
      descripcion: [tipoAsignaturaEdit.tiasDescripcion, [Validators.required]],
      obligatorio: [tipoAsignaturaEdit.tiasObligatorio=="S" ?true:false],
      estado:[tipoAsignaturaEdit.tiasEstado]
    });
    this.modalService.open(modalForm);
  }

  guardarTipoAsignatura(modalForm){
    this.spinner.show();
    this.submitted = true;
    if (this.frmTipoAsignaturaCreateEdit.invalid) {
      this.spinner.hide();
      return;
    }
    let tipoAsignaturaNueva:TipoAsignatura={
      tiasDescripcion:this.frmTipoAsignaturaCreateEdit.get("descripcion").value,
      tiasObligatorio:this.frmTipoAsignaturaCreateEdit.get("obligatorio").value==true?"S":"N",
      tiasEstado:1
    };
    this._tipoAsignaturaService.guardarActualizarTipoAsignatura(tipoAsignaturaNueva).subscribe(
      (respuesta:any)=>{
          Swal.fire(`Tipo Asignatura Creada!`,'','success');
          this.spinner.hide();
          this.listarTodosLosTipoAsignatura();
          
      },
      (error:any)=>{
        Swal.fire(`No se pudo crear el Tipo Asignatura `,'','error'); 
        this.spinner.hide();
        this.listarTodosLosTipoAsignatura();
      }
    );
    modalForm.close('Accept click');  
  }

  activarInactivar(tipoAsignaturaActualizar:TipoAsignatura){
    this.spinner.show();
    if (tipoAsignaturaActualizar.tiasEstado==1) {
      this._tipoAsignaturaService.inactivarTipoAsignatura(tipoAsignaturaActualizar.tiasCodigo).subscribe(
        (respuesta:any)=>{
          this.spinner.hide();
          Swal.fire(' Tipo Asignatura Inactivada','','success'); 
          this.listarTodosLosTipoAsignatura();
        },
        (error:any)=>{
          this.spinner.hide();
          Swal.fire('Ups! ocurrió un error','','error'); 
          this.listarTodosLosTipoAsignatura();
        }
      );
    } else {
      tipoAsignaturaActualizar.tiasEstado=1;
      this._tipoAsignaturaService.guardarActualizarTipoAsignatura(tipoAsignaturaActualizar).subscribe(
        (respuesta:any)=>{
          this.spinner.hide();
          Swal.fire(' Tipo Asignatura Activada','','success'); 
          this.listarTodosLosTipoAsignatura();
        },
        (error:any)=>{
          this.spinner.hide();
          Swal.fire('Ups! ocurrió un error','','error'); 
          this.listarTodosLosTipoAsignatura();
        }
      );
    }
    
}
actualizarTipoAsignatura(modalForm){
  this.spinner.show();
    this.submitted = true;
    // stop here if form is invalid
    if (this.frmTipoAsignaturaCreateEdit.invalid) {
      this.spinner.hide();
      return;
    }
    let tipoAsignaturaGuardar:TipoAsignatura={
      tiasCodigo:this.frmTipoAsignaturaCreateEdit.get("codigo").value,
      tiasDescripcion:this.frmTipoAsignaturaCreateEdit.get("descripcion").value,
      tiasObligatorio:this.frmTipoAsignaturaCreateEdit.get("obligatorio").value==true?"S":"N",
      tiasEstado:this.frmTipoAsignaturaCreateEdit.get("estado").value,
    };
    this._tipoAsignaturaService.guardarActualizarTipoAsignatura(tipoAsignaturaGuardar).subscribe(
      (respuesta:any)=>{
          Swal.fire(`Tipo Asignatura Actualizada!`,'','success'); 
          this.spinner.hide();
          this.listarTodosLosTipoAsignatura();
      },
      (error:any)=>{
        Swal.fire(`No se pudo guardar el Tipo Asignatura`,'','error'); 
        this.spinner.hide();
        this.listarTodosLosTipoAsignatura();
      }
    );
    modalForm.close('Accept click');
}

applyFilter(event: Event) {
  const filterValue = (event.target as HTMLInputElement).value;
  this.dataSource.filter = filterValue.trim().toLowerCase();

  if (this.dataSource.paginator) {
    this.dataSource.paginator.firstPage();
  }
}
 // getter for easy access to form fields
 get ReactiveFrmTipoAsignaturaCreateEdit() {
  return this.frmTipoAsignaturaCreateEdit.controls;
 }
}
