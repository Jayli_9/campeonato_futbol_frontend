import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TipoAsignaturaComponent } from './tipo-asignatura.component';

describe('TipoAsignaturaComponent', () => {
  let component: TipoAsignaturaComponent;
  let fixture: ComponentFixture<TipoAsignaturaComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ TipoAsignaturaComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(TipoAsignaturaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
