import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RUTA_TIPO_ASIGNATURA } from '../routes/tipo-asignatura-routing.module';
import { RouterModule } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CoreCommonModule } from '@core/common.module';
import { ContentHeaderModule } from 'app/layout/components/content-header/content-header.module';
import { MaterialModule } from 'app/main/shared/material/material.module';
import { TipoAsignaturaComponent } from '../components/tipo-asignatura/tipo-asignatura.component';



@NgModule({
  declarations: [TipoAsignaturaComponent],
  imports: [
    CommonModule,
    RouterModule.forChild(RUTA_TIPO_ASIGNATURA),
    FormsModule,
    ReactiveFormsModule,
    MaterialModule,
    ContentHeaderModule,
    CoreCommonModule
  ]
})
export class TipoAsignaturaModule { }
