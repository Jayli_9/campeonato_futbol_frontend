import { Routes } from "@angular/router";
import { AuthGuard } from "app/auth/helpers/auth.guards";
import { TipoAsignaturaComponent } from "../components/tipo-asignatura/tipo-asignatura.component";

export const RUTA_TIPO_ASIGNATURA:Routes=[
    {
        path:'tipo-asignatura',
        component:TipoAsignaturaComponent,
        canActivate:[AuthGuard]
    }
]