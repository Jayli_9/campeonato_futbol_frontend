import { NgModule } from "@angular/core"
import { CommonModule } from "@angular/common"
import { FormsModule, ReactiveFormsModule } from "@angular/forms"
import { RouterModule } from "@angular/router"
import { CoreCommonModule } from "@core/common.module"
import { ContentHeaderModule } from "app/layout/components/content-header/content-header.module"
import { MaterialModule } from "app/main/shared/material/material.module"
import { MatSelectModule } from "@angular/material/select"
import { MatDialogModule } from "@angular/material/dialog"
import { SubDenominacionComponent } from "../components/sub-denominacion/sub-denominacion.component"
import { ModalSubDenominacionNuevoComponent } from "../components/sub-denominacion/modal-sub-denominacion-nuevo/modal-sub-denominacion-nuevo.component"
import { ModalSubDenominacionEditarComponent } from "../components/sub-denominacion/modal-sub-denominacion-editar/modal-sub-denominacion-editar.component"
import { RUTA_SUB_DENOMINACION } from "../routes/sub-denominacion-routing.module"

@NgModule({
  declarations: [
    SubDenominacionComponent,
    ModalSubDenominacionNuevoComponent,
    ModalSubDenominacionEditarComponent,
  ],
  imports: [
    CommonModule,
    RouterModule.forChild(RUTA_SUB_DENOMINACION),
    FormsModule,
    ReactiveFormsModule,
    MaterialModule,
    ContentHeaderModule,
    CoreCommonModule,
    MatSelectModule,
    MatDialogModule,
  ],
})
export class SubDenominacionModule {}
