import { Component, Inject, OnInit } from "@angular/core"
import { UntypedFormControl, UntypedFormGroup, Validators } from "@angular/forms"
import { MAT_DIALOG_DATA, MatDialogRef } from "@angular/material/dialog"
import { SubDenominacionService } from "../../../services/sub-denominacion.service"
import { IPpeDenominacion } from "app/main/pages/participacionEstudiantil/interfaces/IPpeDenominacion"

@Component({
  selector: "app-modal-sub-denominacion-nuevo",
  templateUrl: "./modal-sub-denominacion-nuevo.component.html",
  styleUrls: ["./modal-sub-denominacion-nuevo.component.scss"],
})
export class ModalSubDenominacionNuevoComponent {
  public subDenominacionForm: UntypedFormGroup
  public denominacion: IPpeDenominacion[]

  constructor(
    public dialogRef: MatDialogRef<ModalSubDenominacionNuevoComponent>,
    private subDenominacionService: SubDenominacionService,
    @Inject(MAT_DIALOG_DATA) public inscripcion: any
  ) {
    this.initComponents()
    this.listarInformacion()
  }

  async listarInformacion() {
    await this.listarDenominacion()
  }

  async listarDenominacion() {
    try {
      this.denominacion = (
        await this.subDenominacionService.listarDenominacion()
      ).listado
    } catch (error) {
      console.log(error)
    }
  }

  guardar() {
    if (this.subDenominacionForm.status === "VALID") {
      this.dialogRef.close(this.subDenominacionForm.value)
    }
  }

  initComponents() {
    this.denominacion = []
    this.subDenominacionForm = new UntypedFormGroup({
      descripcion: new UntypedFormControl("", [Validators.required]),
      denominacion: new UntypedFormControl("", [Validators.required]),
    })
  }

  resetForm(){
    this.subDenominacionForm.get("descripcion").setValue(null)
    this.subDenominacionForm.get("denominacion").setValue(null)
    this.subDenominacionForm.get("descripcion").markAsUntouched()
    this.subDenominacionForm.get("denominacion").markAsUntouched()
  }

  getErrorMessage(field: string) {
    return this.subDenominacionForm.get(field).hasError("required")
      ? "Campo requerido"
      : this.subDenominacionForm.get(field).hasError("email")
      ? "Correo inválido"
      : this.subDenominacionForm.get(field).hasError("maxlength")
      ? "longitud incorrecta"
      : this.subDenominacionForm.get(field).hasError("minlength")
      ? "longitud incorrecta"
      : ""
  }
}
