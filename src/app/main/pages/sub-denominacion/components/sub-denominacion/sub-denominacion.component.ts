import { Component, OnInit, ViewChild } from "@angular/core"
import { FormBuilder, UntypedFormGroup, Validators } from "@angular/forms"
import { MatPaginator } from "@angular/material/paginator"
import { MatSort } from "@angular/material/sort"
import { MatTableDataSource } from "@angular/material/table"
import { NgxSpinnerService } from "ngx-spinner"
import { SubDenominacionService } from "../../services/sub-denominacion.service"
import { MatDialog } from "@angular/material/dialog"
import { MensajesService } from "app/main/pages/asignacionDocentesOrdinaria/servicios/mensajes.service"
import { SubDenominacion } from "../../interfaces/subDenominacion"
import { ModalSubDenominacionNuevoComponent } from "./modal-sub-denominacion-nuevo/modal-sub-denominacion-nuevo.component"
import { ModalSubDenominacionEditarComponent } from "./modal-sub-denominacion-editar/modal-sub-denominacion-editar.component"
import { SubDenominacionDTO } from "../../interfaces/subDenominacionDTO"
import { IPpeSubDenominacion } from "app/main/pages/participacionEstudiantil/interfaces/IPpeSubdenominacion"
import { IPpeDenominacion } from "app/main/pages/participacionEstudiantil/interfaces/IPpeDenominacion"

@Component({
  selector: "app-sub-denominacion",
  templateUrl: "./sub-denominacion.component.html",
  styleUrls: ["./sub-denominacion.component.scss"],
})
export class SubDenominacionComponent implements OnInit {
  public contentHeader: object
  public frmSubDenominacionCreateEdit: UntypedFormGroup
  public submitted = false
  listaSubDenominacion: IPpeSubDenominacion[] = null
  listaDenominacion: IPpeDenominacion[] = []
  columnasSubDenominacion = [
    "num",
    "descripcion",
    "denominacion",
    "estado",
    "acciones",
  ]
  dataSource: MatTableDataSource<IPpeSubDenominacion>

  @ViewChild(MatPaginator) paginator: MatPaginator
  @ViewChild(MatSort) sort: MatSort
  constructor(
    public spinner: NgxSpinnerService,
    private _subdenominacionService: SubDenominacionService,
    private mensajeService: MensajesService,
    private dialog: MatDialog
  ) {}

  ngOnInit(): void {
    this.contentHeader = {
      headerTitle: "Sub Denominación",
      actionButton: false,
      breadcrumb: {
        type: "",
        links: [
          {
            name: "Inicio",
            isLink: true,
            link: "/pages/inicio",
          },
          {
            name: "SubDenominacion",
            isLink: false,
          },
        ],
      },
    }
    this.listarTodasLasSubDenominaciones()
  }

  listarTodasLasSubDenominaciones() {
    this.spinner.show()
    this._subdenominacionService.listarTodasLasSubDenominaciones().subscribe(
      (respuesta: any) => {
        this.listaSubDenominacion = respuesta.listado
        this.dataSource = new MatTableDataSource(this.listaSubDenominacion)
        this.dataSource.paginator = this.paginator
        this.dataSource.paginator._intl.itemsPerPageLabel =
          "Agrupaciones por página"
        this.dataSource.paginator._intl.nextPageLabel = "Siguiente"
        this.dataSource.paginator._intl.previousPageLabel = "Anterior"
        this.dataSource.sort = this.sort
        this.spinner.hide()
      },
      (error: any) => {
        this.spinner.hide()
        this.mensajeService.mensajeError(
          "Error",
          "Ups! ocurrió un error al cargar Denominaciones"
        )
      }
    )
  }

  nuevaSubDenominacion() {
    const dialogRef = this.dialog.open(ModalSubDenominacionNuevoComponent, {
      width: "600px",
    })
    dialogRef.afterClosed().subscribe((result: any) => {
      if (!!result) {
        this.guardarSubDenominacion(result)
      }
    })
  }

  editSubDenominacion(subDenominacion: SubDenominacion) {
    const dialogRef = this.dialog.open(ModalSubDenominacionEditarComponent, {
      width: "600px",
      data: subDenominacion,
    })
    dialogRef.afterClosed().subscribe((result: any) => {
      if (!!result) {
        this.guardarSubDenominacion(result)
      }
    })
  }

  async guardarSubDenominacion(data: any) {
    this.spinner.show()
    console.log("data");
    console.log(data);
    
    let subDenominacionNueva: SubDenominacionDTO = {
      psdenCodigo: data.psdenCodigo,
      psdenDescripcion: data.descripcion,
      ppeDenominacion: data.denominacion,
      psdenEstado: 1,
    }
    try {
      await this._subdenominacionService.guardarSubDenominacion(subDenominacionNueva)
      this.mensajeService.mensajeCorrecto("Éxito", "Sub Denominación creada")
      this.listarTodasLasSubDenominaciones()
    } catch (error) {
      console.log(error)
    }
    this.spinner.hide()
  }

  async activarInactivar(subDenominacion: SubDenominacion) {
    try {
      await this._subdenominacionService.activarInactivarSubDenominacion(
        subDenominacion.psdenCodigo
      )
      this.listarTodasLasSubDenominaciones()
      this.mensajeService.mensajeCorrecto("Éxito", "Estado cambiado")
    } catch (error) {
      this.mensajeService.mensajeError("Error", "Estado cambiado")
    }
  }

  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value
    this.dataSource.filter = filterValue.trim().toLowerCase()

    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage()
    }
  }
  // getter for easy access to form fields
  get ReactiveFrmAgrupacionCreateEdit() {
    return this.frmSubDenominacionCreateEdit.controls
  }
}
