import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SubDenominacionComponent } from './sub-denominacion.component';

describe('DenominacionComponent', () => {
  let component: SubDenominacionComponent;
  let fixture: ComponentFixture<SubDenominacionComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SubDenominacionComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SubDenominacionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
