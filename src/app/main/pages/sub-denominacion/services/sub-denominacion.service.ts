import { HttpClient } from "@angular/common/http"
import { Injectable } from "@angular/core"
import { environment } from "environments/environment"
import { SubDenominacion } from "../interfaces/subDenominacion"
import { ResponseGenerico } from "../../reporteEstudiantes/interfaces/response-generico"
import { IPpeParticipacion } from "../../participacionEstudiantil/interfaces/IPpeParticipacion"
import { SubDenominacionDTO } from "../interfaces/subDenominacionDTO"
import { IPpeDenominacion } from "../../participacionEstudiantil/interfaces/IPpeDenominacion"

@Injectable({
  providedIn: "root",
})
export class SubDenominacionService {
  url_academico = environment.url_academico
  constructor(public _http: HttpClient) {}

  guardarSubDenominacion(
    subDenominacion: SubDenominacionDTO
  ): Promise<ResponseGenerico<SubDenominacion>> {
    return new Promise((resolve, reject) => {
      this._http
        .post(
          `${environment.url_academico}/private/guardarSubdenominacion`,
          subDenominacion
        )
        .subscribe((response: ResponseGenerico<SubDenominacion>) => {
          resolve(response)
        }, reject)
    })
  }

  listarTodasLasSubDenominaciones() {
    let url_ws = `${this.url_academico}/private/listarSubdenominacion`
    return this._http.get<any>(url_ws)
  }

  activarInactivarSubDenominacion(codigo: number): Promise<any> {
    return new Promise((resolve, reject) => {
      this._http
        .get(
          `${environment.url_academico}/private/cambiarEstadoSubDenominacionPorId/${codigo}`
        )
        .subscribe((response: any) => {
          resolve(response)
        }, reject)
    })
  }

  listarDenominacion(): Promise<ResponseGenerico<IPpeDenominacion>> {
    return new Promise((resolve, reject) => {
      this._http
        .get(`${environment.url_academico}/private/listarDenominacion`)
        .subscribe((response: ResponseGenerico<IPpeDenominacion>) => {
          resolve(response)
        }, reject)
    })
  }
}
