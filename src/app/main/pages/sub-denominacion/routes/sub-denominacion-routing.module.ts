import { Routes } from "@angular/router"
import { AuthGuard } from "app/auth/helpers/auth.guards"
import { SubDenominacionComponent } from "../components/sub-denominacion/sub-denominacion.component"

export const RUTA_SUB_DENOMINACION: Routes = [
  {
    path: "registro_subdenominacion",
    component: SubDenominacionComponent,
    canActivate: [AuthGuard],
  },
]
