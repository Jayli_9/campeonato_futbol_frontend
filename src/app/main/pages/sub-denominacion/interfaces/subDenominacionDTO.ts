export interface SubDenominacionDTO {
    psdenCodigo?: number;
    psdenDescripcion: string;
    psdenEstado: number;
    ppeDenominacion: number;
}