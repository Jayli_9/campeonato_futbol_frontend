import { Denominacion } from "../../denominacion/interfaces/Denominacion";

export interface SubDenominacion {
    psdenCodigo?: number;
    psdenDescripcion: string;
    psdenEstado: number;
    ppeDenominacion: Denominacion;

}