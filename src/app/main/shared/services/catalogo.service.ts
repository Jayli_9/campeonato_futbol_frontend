import {Injectable} from '@angular/core';
import {RespuestaRegimenAnioLectivoInterfaz} from '../interfaces/respuesta-regimen-anio-lectivo-interfaz';
import {RespuestaRegimenInterfaz} from '../interfaces/respuesta-regimen-interfaz';
import {HttpClient} from '@angular/common/http';
import {environment} from '../../../../environments/environment';
import {RespuestaAnioLectivoInterfaz} from '../interfaces/respuesta-anio-lectivo-interfaz';
import {RespuestaGradoInterfaz} from '../interfaces/respuesta-grado-interfaz';
import { ResponseGenerico } from 'app/main/pages/malla-asignatura-extra/interfaces/response-generico';
import { IcatGrado } from 'app/main/pages/asignacionDocentesOrdinaria/interfaces/IcatGrado';
import { CatPoblacionObjetivo } from '../interfaces/cat-poblacion-objetivo';

@Injectable({
    providedIn: 'root'
})
export class CatalogoService {
    url_catalogo = environment.url_catalogo;

    constructor(
        public _http: HttpClient) {
    }

    /*ANIO LECTIVO -------------------------------*/
    buscarAnioLectivo(codigo: number) {
        let url_ws = `${this.url_catalogo}/private/buscarAnioLectivoPorCodigo/${codigo}`;
        return this._http.get<RespuestaAnioLectivoInterfaz>(url_ws);
    }

    /*--------------------------------------------*/

    /*GRADO---------------------------------------*/
    buscarGradosNivel(codigo: number) {
        let url_ws = `${this.url_catalogo}/private/buscarCatalogoGradosPorNivelCodigo/${codigo}`;
        return this._http.get<RespuestaGradoInterfaz>(url_ws);
    }

    buscarGradoPorCodigo(codigo: number) {
        let url_ws = `${this.url_catalogo}/private/buscarCatalogoGradosPorCodigo/${codigo}`;
        return this._http.get<RespuestaGradoInterfaz>(url_ws);
    }

    /*--------------------------------------------*/

    /*REGIMEN - ANIO LECTIVO-------------------------*/
    listarRegimenAnioLectivos() {
        let url_ws = `${this.url_catalogo}/private/listarRegimenAnioLectivo`;
        return this._http.get<RespuestaRegimenAnioLectivoInterfaz>(url_ws);
    }

    /*--------------------------------------------*/

    /*REGIMEN--------------------------------------*/
    listarRegimenTipo() {
        let url_ws = `${this.url_catalogo}/private/listarRegimenTipo`;
        return this._http.get<RespuestaRegimenInterfaz>(url_ws);
    }

    /*--------------------------------------------*/


    listarGrados(): Promise<ResponseGenerico<IcatGrado>> {
        return new Promise((resolve, reject) => {
          this._http
            .get(
              `${environment.url_catalogo}/private/listarCatalogoGrados`
            )
            .subscribe((response: ResponseGenerico<IcatGrado>) => {
              resolve(response)
            }, reject)
        })
      }


      listarPoblacionObjetivo(): Promise<ResponseGenerico<CatPoblacionObjetivo>> {
        return new Promise((resolve, reject) => {
          this._http
            .get(
              `${environment.url_catalogo}/private/listarPoblacionObjetivo`
            )
            .subscribe((response: ResponseGenerico<CatPoblacionObjetivo>) => {
              resolve(response)
            }, reject)
        })
      }

}
