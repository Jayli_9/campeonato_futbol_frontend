import {RespuestaGeneralInterfaz} from './respuesta-general-interfaz';
import {CatRegimenAnioLectivo} from './cat-regimen-anio-lectivo';
export interface RespuestaRegimenAnioLectivoInterfaz extends RespuestaGeneralInterfaz{

    listado: CatRegimenAnioLectivo[];
    objeto: CatRegimenAnioLectivo;
}
