export interface CatPoblacionObjetivo {
    pbObjtCodigo: number;
    pbObjtDescripcion: string;
    pbObjtEstado: number;
    pbObjtFechaCreacion: Date;
}