import {RespuestaGeneralInterfaz} from './respuesta-general-interfaz';
import {CatGrado} from './cat-grado';

export interface RespuestaGradoInterfaz extends RespuestaGeneralInterfaz {

    listado: CatGrado[];
    objeto: CatGrado;
}
