export interface CatAnioLectivo {
    anilecAnioFin:number;
    anilecAnioInicio:number;
    anilecCodigo:number;
    anilecEstado:number;
    anilecFechaCreacion:Date;
}

