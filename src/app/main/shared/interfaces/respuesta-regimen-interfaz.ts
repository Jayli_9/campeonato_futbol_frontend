import {RespuestaGeneralInterfaz} from './respuesta-general-interfaz';
import {CatRegimen} from './cat-regimen';

export interface RespuestaRegimenInterfaz extends RespuestaGeneralInterfaz {

    listado: CatRegimen[];
    objeto: CatRegimen;
}
