import {CatRegimenAnioLectivo} from './cat-regimen-anio-lectivo';

export interface CatRegimen {
    regAnioLecActual:CatRegimenAnioLectivo;
    regAnioLecSiguiente	:CatRegimenAnioLectivo;
    regCodigo:number;
    regDescripcion:string;
    regEstado:number;
    regFechaCreacion:Date;

}
