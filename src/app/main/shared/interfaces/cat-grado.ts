export interface CatGrado {
    graCodigo:number;
    graDescripcion:string;
    graEstado:number;
    graFechaCreacion:string;
    graNemonico:string;
    nivCodigo:number;
}
