import {CatAnioLectivo} from './cat-anio-lectivo';

export interface RespuestaAnioLectivoInterfaz {
    listado: CatAnioLectivo[];
    objeto: CatAnioLectivo;
}
