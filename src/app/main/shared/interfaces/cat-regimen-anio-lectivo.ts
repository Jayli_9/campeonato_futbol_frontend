import {CatAnioLectivo} from './cat-anio-lectivo';

export interface CatRegimenAnioLectivo {
    anilecCodigo:number;
    descripcionAnioLectivo:string;
    reanleCodigo:number;
    reanleEstado:number;
    reanleFechaCreacion:Date;
    reanleTipo:string;
    regCodigo:number;
}
