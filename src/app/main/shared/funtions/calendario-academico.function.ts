import {CalendarioAsistencia} from '../../pages/asistenciaOrdinaria/interfaces/calendarioAsistencia';
import {eachDayOfInterval, format, getDay, getMonth, getWeekOfMonth, getYear, parse, parseISO, startOfMonth} from 'date-fns';
import {es} from 'date-fns/locale';

export function construirCalendario(fechaInicial: string, fechaFinal: string): CalendarioAsistencia {
    const anios: CalendarioAsistencia['anios'] = {};

    const fInicial = parseISO(fechaInicial);
    const fFinal = parseISO(fechaFinal);

    const diasEntreFechas = eachDayOfInterval({start: fInicial, end: fFinal});

    let semanaActual = 1;
    let primerDiaSemana = startOfMonth(fInicial);

    diasEntreFechas.forEach(dia => {
        const diaSemana = getDay(dia);
        const mesActual = getMonth(dia);

        // Omitimos fines de semana
        if (diaSemana === 0 || diaSemana === 6) {
            return;
        }

        if (diaSemana === 1 || mesActual !== getMonth(primerDiaSemana)) {
            semanaActual = getWeekOfMonth(dia);
            primerDiaSemana = dia;
        }

        const anio = getYear(dia);
        const mesNombre = capitalize(format(dia, 'MMMM', {locale: es}));
        const numeroMes = getMonth(dia) + 1;
        const diaNombre = format(dia, 'EEEE', {locale: es});

        anios[anio] ??= {};
        anios[anio][mesNombre] ??= {numeroMes, semanas: {}};
        anios[anio][mesNombre].semanas[semanaActual] ??= [];

        anios[anio][mesNombre].semanas[semanaActual].push({
            diaSemana: parseInt(format(dia, 'd')),
            nombreDia: diaNombre,
            fecha: dia
        });
    });

    return {
        fechaInicial: fInicial,
        fechaFinal: fFinal,
        anios: anios
    };
}
function capitalize(word: string): string {
    return word.charAt(0).toUpperCase() + word.slice(1);
}

export function obtenerNumeroMes(nombreMes: string): number {
    const fecha = parse(`01 ${nombreMes} 2000`, 'dd MMMM yyyy', new Date(), {locale: es});
    const numeroMes = getMonth(fecha);
    return numeroMes + 1;
}

