import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { BehaviorSubject, Observable, of } from 'rxjs';
import { map } from 'rxjs/operators';

import { environment } from 'environments/environment';
import { User } from 'app/auth/models';
import { ToastrService } from 'ngx-toastr';
import { Rol } from '../models/rol.model';
import { currentUserConstante } from 'app/compartidos/constantes/currentUserConstante';
import { menuConstante } from 'app/compartidos/constantes/menuConstante';


@Injectable({ providedIn: 'root' })
export class AuthenticationService {
  //public
  public currentUser: Observable<User>;

  //private
  private currentUserSubject: BehaviorSubject<User>;

  private codigoUsuario: number;
  public token: string;

  /**
   *
   * @param {HttpClient} _http
   * @param {ToastrService} _toastrService
   */
  constructor(private _http: HttpClient, private _toastrService: ToastrService) {
    this.currentUserSubject = new BehaviorSubject<User>(JSON.parse(localStorage.getItem('currentUser')));
    this.currentUser = this.currentUserSubject.asObservable();
  }

  // getter: currentUserValue
  public get currentUserValue(): User {
    return this.currentUserSubject.value;
  }

  /**
   * User login
   *
   * @param usuario
   * @param password
   * @returns user
   */
  login(usuario: string, password: string) {

    const usuarioLogueo = {
      identificacion: usuario,
      clave: password,
    };

    return this._http
      .post<any>(`${environment.url_seguridades}/public/loginAplicacion`, usuarioLogueo)
      .pipe(
        map(user => {
          let usario=currentUserConstante.currentUser;
          // login successful if there's a jwt token in the response
          if (user && user.token) {
            usario.token=user.token;
            // store user details and jwt token in local storage to keep user logged in between page refreshes
            localStorage.setItem('currentUser', JSON.stringify(usario));
            this.codigoUsuario=usario.codigoUsuario;
            this.token=usario.token;
            // notify
            this.currentUserSubject.next(usario);
          }

          return usario;
        })
      );
    // Valor quemado para simular una respuesta exitosa

    // Almacenar el usuario en el almacenamiento local
    // localStorage.setItem('currentUser', JSON.stringify(currentUserConstante.currentUser));

    // // Establecer el código de usuario y token en la clase actual
    // this.codigoUsuario = currentUserConstante.currentUser.codigoUsuario;
    // this.token = currentUserConstante.currentUser.token;

    // // Emitir el usuario actualizado
    // this.currentUserSubject.next(currentUserConstante.currentUser);

    // // Devolver el usuario quemado como observable
    // return of(currentUserConstante.currentUser);
    // // return currentUserConstante.currentUser;
  }
  obtenerMenu() {
    // const JsonEnvio = {
    //   codigoUsuario: this.codigoUsuario,
    //   prefijoAplicacion: environment.prefijoApp
    // };
    // return this._http
    //   .post<Rol[]>(`${environment.url_seguridades}/private/obtenerRolesMenu`, JsonEnvio)
    //   .pipe(
    //     map(menu => {
    //       localStorage.setItem('menuJson', JSON.stringify(menu));
    //       return menu;
    //     })
    //   );
    localStorage.setItem('menuJson', JSON.stringify(menuConstante.menuJson));
    return of(menuConstante.menuJson);
  }

  /**
   * User logout
   *
   */
  logout() {
    // remove user from local storage to log user out
    localStorage.removeItem('currentUser');
    localStorage.removeItem('menuJson');
    // notify
    this.currentUserSubject.next(null);
  }
}
