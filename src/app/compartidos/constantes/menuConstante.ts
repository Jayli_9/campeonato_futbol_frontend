export class menuConstante {
    public static menuJson = [{
        "codigo": 9654, "aplicacion": { "codigo": 742, "prefijo": "ACD", "nombre": "CAMPEONATO APP", "descripcion": "SISTEMA PARA ORGANIZAR CAMPEONATOS", "url": "/acd", "tipo": "1" }, "nombre": "ADMINISTRADOR", "descripcion": "ADMINISTRADOR", "estado": "A", "estadoActivo": null, "asignado": null, "enumRolSeguridad": null, "asignadoAdministrador": null,
        "menu": [{
            "codigo": 6335, "nombre": "ADMIN JAYLI", "descripcion": "ADMIN DESCRIPCIÓN", "url": "/pages", "nivel": 1, "codigoRecursoPadre": null,
            "recursosHijos": [{ "codigo": 6341, "nombre": "CAMPEONATO", "descripcion": "CAMPEONATO DESCRIPCION", "url": "/pages/campeonato", "nivel": 2, "codigoRecursoPadre": 6335, "recursosHijos": [] }]
        }]
    }]
}