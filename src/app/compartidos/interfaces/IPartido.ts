export interface IPartido {
    equipoLocal: string;
    equipoVisitante: string;
}