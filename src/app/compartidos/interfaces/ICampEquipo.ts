import { ICampCampeonato } from "./ICampCampeonato";

export interface ICampEquipo {
    campCampeonato: ICampCampeonato,
    equiCodigo: number,
    equiDescripcion: string,
    equiEstado: number,
    equiLogo: string,
    equiLugar: string,
    equiNombre: string
}