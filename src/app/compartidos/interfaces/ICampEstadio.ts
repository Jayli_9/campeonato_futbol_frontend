import { ICampCampeonato } from "./ICampCampeonato";

export interface ICampEstadio {
    campCampeonato: ICampCampeonato,
    estaCodigo: number,
    estaDescripcion: string,
    estaEstado: number,
    estaNombre: string
}