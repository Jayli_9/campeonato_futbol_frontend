
export interface ICampCampeonato {
    campCodigo: number,
    campDescripcion: string,
    campEstado: number,
    campNombre: string
}