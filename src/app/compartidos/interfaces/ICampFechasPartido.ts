import { IPartido } from "./IPartido";

export interface ICampFechasPartido {
    fechCodigo?: number,
    fechDescripcion?: string,
    fechEstado?: number,
    partidos?: IPartido[],
}