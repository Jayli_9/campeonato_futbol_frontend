export const environment = {
  production: true,
  hmr: false,
  prefijoApp:"ACD",
  url_seguridades: 'https://desacademico.educacion.gob.ec/seguridades-servidor',
  url_academico:'https://desacademico.educacion.gob.ec/academico-servidor',
  url_catalogo:'https://desacademico.educacion.gob.ec/catalogo-servidor',
  url_oferta:'https://desacademico.educacion.gob.ec/oferta-servidor',
  url_docente:'https://desacademico.educacion.gob.ec/docentesApp-servidor',  
  url_institucion:'https://desacademico.educacion.gob.ec/giee-servidor',
  url_matricula:'https://desacademico.educacion.gob.ec/matricula-servidor',
};
