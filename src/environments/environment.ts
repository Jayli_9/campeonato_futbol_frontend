// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  hmr: false,
  prefijoApp:"ACD",
  url_seguridades: 'http://10.200.10.15/seguridades-servidor',
  url_academico:'http://10.200.10.15/academico-servidor',
  url_catalogo:'http://10.200.10.15/catalogo-servidor',
  url_oferta:'http://10.200.10.15/oferta-servidor',
  url_docente:'http://10.200.10.15/docentesApp-servidor',
  url_institucion:'http://10.200.10.15/giee-servidor',
  url_campeonatos:'http://localhost:8080/campeonato-back',
  url_matricula:'http://10.200.10.15/matricula-servidor'
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/plugins/zone-error';  // Included with Angular CLI.
